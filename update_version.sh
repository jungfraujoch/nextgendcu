#!/bin/bash

#
# Copyright (2019-2024) Paul Scherrer Institute
#

VERSION=$(<VERSION)

# Regular expression for semantic versioning with optional pre-release and build metadata
regex="^([0-9]+)\.([0-9]+)\.([0-9]+)(-[A-Za-z0-9.\-]+)?(\+[A-Za-z0-9.\-]+)?$"

if [[ $VERSION =~ $regex ]]; then
    major="${BASH_REMATCH[1]}"
    minor="${BASH_REMATCH[2]}"
    patch="${BASH_REMATCH[3]}"
    prerelease="${BASH_REMATCH[4]}"
    build="${BASH_REMATCH[5]}"

    echo "Major version: $major"
    echo "Minor version: $minor"
    echo "Patch version: $patch"
    [ -n "$prerelease" ] && echo "Pre-release version: ${prerelease:1}"  # Remove the leading '-'
    [ -n "$build" ] && echo "Build metadata: ${build:1}"  # Remove the leading '+'
else
    echo "Invalid semantic version: $semver"
    exit
fi

OPENAPI_VERSION=7.8.0
wget https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/${OPENAPI_VERSION}/openapi-generator-cli-${OPENAPI_VERSION}.jar -O openapi-generator-cli.jar

SRC=' version:.*'
DST=" version: $VERSION"
sed -i -e "s/$SRC/$DST/" broker/jfjoch_api.yaml
sed -i -e "s/$SRC/$DST/" writer/writer_api.yaml


git rm broker/gen/model/*.cpp broker/gen/model/*.h frontend/src/openapi/models/*.ts docs/python_client/* docs/python_client/docs/*

java -jar openapi-generator-cli.jar generate -i broker/jfjoch_api.yaml -o python-client/ -g python --git-host=git.psi.ch --git-repo-id jungfraujoch --git-user-id jungfraujoch --additional-properties=packageName=jfjoch_client,packageVersion=$VERSION
java -jar openapi-generator-cli.jar generate -i broker/jfjoch_api.yaml -o broker/gen -g cpp-pistache-server
java -jar openapi-generator-cli.jar generate -i writer/writer_api.yaml -o writer/gen -g cpp-pistache-server

cd frontend
sed -i s/\".*\"/\"$VERSION\"/g src/version.ts

npm install
npm run openapi
npm run redocly4broker

cd ..

mkdir -p docs/python_client/docs
cp python-client/README.md docs/python_client
cp python-client/docs/*.md docs/python_client/docs

git add broker/gen/model/*.cpp broker/gen/model/*.h frontend/src/openapi/models/*.ts docs/python_client/*.md docs/python_client/docs/*.md

sed -i "s,release =.*,release = \'$VERSION\'," docs/conf.py
sed -i "s,\"version\":.*,\"version\": \"$VERSION\"\,," frontend/package.json

cd fpga

SRC="define JFJOCH_FPGA_MAJOR_VERSION 8'd.*"
DST="define JFJOCH_FPGA_MAJOR_VERSION 8'd${major}"
sed -i "s/$SRC/$DST/" hdl/action_config.v

SRC="define JFJOCH_FPGA_MINOR_VERSION 8'd.*"
DST="define JFJOCH_FPGA_MINOR_VERSION 8'd${minor}"
sed -i "s/$SRC/$DST/" hdl/action_config.v

SRC="define JFJOCH_FPGA_PATCH_VERSION 8'd.*"
DST="define JFJOCH_FPGA_PATCH_VERSION 8'd${patch}"
sed -i "s/$SRC/$DST/" hdl/action_config.v

SRC="define JFJOCH_FPGA_PRERELEASE 64'h.*"
if [ -n "$prerelease" ]; then
  result=$(printf '%.7s' "${prerelease:1}" | rev | xxd -p -u)
  DST="define JFJOCH_FPGA_PRERELEASE 64'h$result"
else
  DST="define JFJOCH_FPGA_PRERELEASE 64'h0"
fi

sed -i "s/$SRC/$DST/" hdl/action_config.v


cd pcie_driver

sed -i s,MODULE_VERSION\(\".*,MODULE_VERSION\(\"$VERSION\"\)\;, jfjoch_drv.c
sed -i s,VERSION=.*,VERSION=$VERSION, postinstall.sh
sed -i s,VERSION=.*,VERSION=$VERSION, preuninstall.sh
sed -i s,PACKAGE_VERSION=.*,PACKAGE_VERSION=$VERSION, dkms.conf
sed -i s,VERSION=.*,VERSION=$VERSION, install_dkms.sh
