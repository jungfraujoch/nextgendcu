openapi: 3.0.3
info:
  title: Jungfraujoch writer
  description: Jungfraujoch Writer Web API
  version: 1.0.0-rc.31
  contact:
    name: Filip Leonarski (Paul Scherrer Institute)
    email: filip.leonarski@psi.ch
  license:
    name: GPL-3.0
    url: https://www.gnu.org/licenses/gpl-3.0.html
components:
  schemas:
    writer_statistics:
      type: object
      properties:
        nimages:
          type: integer
          format: int64
          description: Number of images written
        performance_MBs:
          type: number
          format: float
          description: Performance in MB/s
        performance_Hz:
          type: number
          format: float
          description: Performance in images/s
        run_number:
          type: integer
          format: int64
        run_name:
          type: string
        file_prefix:
          type: string
          description: File prefix for the last written dataset
        socket_number:
          type: integer
          format: int64
          description: Number of socket on `jfjoch_broker` side for the current/last data collection
        state:
          type: string
          enum:
            - idle
            - started
            - receiving
            - error
paths:
  /status:
    get:
      summary: Get writer status
      responses:
        "200":
          description: Statistics of the last measurement
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/writer_statistics'
  /cancel:
    post:
      summary: Cancel running data collection
      description: |
        It only instructs writer to cancel, but doesn't wait for cancellation actually happening.
        It still requires to call `/wait_till_done`
      responses:
        "200":
          description: Cancel message acknowledged
  /version:
    get:
      responses:
        "200":
          description: Release number of Jungfraujoch
          content:
            text/plain:
              schema:
                type: string
                example: 1.0.0
