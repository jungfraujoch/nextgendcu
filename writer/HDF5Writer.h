// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_HDF5WRITER_H
#define JUNGFRAUJOCH_HDF5WRITER_H

#include <numeric>

#include "HDF5DataFile.h"
#include "../frame_serialize/JFJochMessages.h"
#include "../common/ZMQWrappers.h"
#include "HDF5NXmx.h"

class HDF5Writer {
    StartMessage start_message;
    std::unique_ptr<NXmx> master_file;
    std::vector<std::unique_ptr<HDF5DataFile> > files;
    std::vector<HDF5DataFileStatistics> stats;

    std::unique_ptr<ZMQSocket> finalized_file_socket;

    void AddStats(const std::optional<HDF5DataFileStatistics>& s);
public:
    explicit HDF5Writer(const StartMessage &request);
    void Write(const DataMessage& msg);
    void Write(const CompressedImage& msg);
    void Write(const EndMessage& msg);
    std::vector<HDF5DataFileStatistics> Finalize();
    void SetupFinalizedFileSocket(const std::string &addr);
    std::optional<std::string> GetZMQAddr();
};

#endif //JUNGFRAUJOCH_HDF5WRITER_H
