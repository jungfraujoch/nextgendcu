// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "../common/GitInfo.h"
#include "JFJochWriterHttp.h"
#include "gen/model/Writer_statistics.h"

void JFJochWriterHttp::status_get(Pistache::Http::ResponseWriter &response) {
    auto stat = writer.GetStatistics();
    org::openapitools::server::model::Writer_statistics resp_struct;
    resp_struct.setNimages(stat.processed_images);
    resp_struct.setPerformanceMBs(stat.performance_MBs);
    resp_struct.setPerformanceHz(stat.performance_Hz);
    resp_struct.setFilePrefix(stat.file_prefix);
    resp_struct.setRunName(stat.run_name);
    resp_struct.setRunNumber(stat.run_number);
    resp_struct.setSocketNumber(stat.socket_number);
    switch (stat.state) {
        case StreamWriterState::Idle:
            resp_struct.setState("idle");
            break;
        case StreamWriterState::Started:
            resp_struct.setState("started");
            break;
        case StreamWriterState::Receiving:
            resp_struct.setState("receiving");
            break;
        case StreamWriterState::Error:
            resp_struct.setState("error");
            break;
    }

    nlohmann::json j = resp_struct;
    response.send(Pistache::Http::Code::Ok, j.dump(), MIME(Application, Json));
}

void JFJochWriterHttp::cancel_post(Pistache::Http::ResponseWriter &response) {
    writer.Cancel();
    response.send(Pistache::Http::Code::Ok);
}

JFJochWriterHttp::JFJochWriterHttp(StreamWriter &in_writer, std::shared_ptr<Pistache::Rest::Router> &rtr)
: DefaultApi(rtr), writer(in_writer){
    init();
}

void JFJochWriterHttp::version_get(Pistache::Http::ResponseWriter &response) {
    response.send(Pistache::Http::Code::Ok, jfjoch_version(), MIME(Text, Plain));
}
