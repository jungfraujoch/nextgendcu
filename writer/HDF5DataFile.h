// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef HDF5DATAFILE_H
#define HDF5DATAFILE_H

#include <queue>
#include <string>
#include <chrono>

#include "HDF5Objects.h"
#include "../common/SpotToSave.h"
#include "../frame_serialize/JFJochMessages.h"

#include "HDF5DataFilePlugin.h"

struct HDF5DataFileStatistics {
    std::string filename;
    uint64_t file_number; // counting from 1!
    size_t max_image_number;
    uint64_t total_images;
};

class HDF5DataFile {
    std::string filename;
    std::string tmp_filename;

    std::unique_ptr<HDF5File> data_file = nullptr;
    std::unique_ptr<HDF5DataSet> data_set = nullptr;
    std::unique_ptr<HDF5DataSet> data_set_image_number = nullptr;
    std::vector<std::unique_ptr<HDF5DataFilePlugin>> plugins;

    size_t xpixel;
    size_t ypixel;
    size_t max_image_number;
    size_t nimages;

    std::string image_units;

    std::vector<uint64_t> timestamp;
    std::vector<uint32_t> exptime;
    std::vector<uint64_t> number;

    int32_t image_low;

    bool closed = false;

    bool overwrite = false;
    int64_t file_number;
    void CreateFile(const DataMessage& msg);
public:
    HDF5DataFile(const StartMessage &msg, uint64_t file_number);
    ~HDF5DataFile();
    std::optional<HDF5DataFileStatistics> Close();
    void Write(const DataMessage& msg, uint64_t image_number);
    size_t GetNumImages() const;
};

#endif //HDF5DATAFILE_H
