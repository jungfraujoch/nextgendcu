// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_STREAMWRITER_H
#define JUNGFRAUJOCH_STREAMWRITER_H

#include <future>

#include "ZMQImagePuller.h"
#include "HDF5DataFile.h"

enum class StreamWriterState {Idle, Started, Receiving, Error};

struct StreamWriterStatistics {
    uint64_t processed_images;
    float performance_MBs;
    float performance_Hz;
    std::string file_prefix;
    std::string run_name;
    uint64_t run_number;
    uint64_t socket_number;
    StreamWriterState state;
    ZMQImagePullerStatistics puller_stats;
};

struct StreamWriterOutput {
    StreamWriterStatistics stats;
    std::vector<HDF5DataFileStatistics> data_file_stats;
};

class StreamWriter {
    std::string file_done_address;

    StreamWriterState state = StreamWriterState::Idle;

    ZMQImagePullerOutput image_puller_output;

    std::atomic<uint64_t> processed_images;
    std::atomic<uint64_t> processed_image_size;
    std::chrono::time_point<std::chrono::system_clock> start_time;
    std::chrono::time_point<std::chrono::system_clock> end_time;
    std::string file_prefix;
    std::string run_name;
    uint64_t run_number;
    uint64_t socket_number;

    bool debug_skip_write_notification = false;

    ZMQImagePuller image_puller;
    Logger &logger;
    void CollectImages(std::vector<HDF5DataFileStatistics> &v);
    bool WaitForImage();
    void NotifyReceiverOnFinalizedWrite(const std::string &detector_update_zmq_addr,
                                        bool ok);
public:
    StreamWriter(Logger &logger,
                 const std::string& zmq_addr,
                 const std::string& repub_address = "",
                 const std::string& file_done_address = "",
                 const std::optional<int32_t> &rcv_watermark = {},
                 const std::optional<int32_t> &repub_watermark = {});
    StreamWriterOutput Run();
    void Cancel();
    StreamWriterStatistics GetStatistics() const;
    void DebugSkipWriteNotification(bool input);
};


#endif //JUNGFRAUJOCH_STREAMWRITER_H
