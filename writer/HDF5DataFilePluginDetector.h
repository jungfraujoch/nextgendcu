// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_HDF5DATAFILEPLUGINJUNGFRAU_H
#define JUNGFRAUJOCH_HDF5DATAFILEPLUGINJUNGFRAU_H

#include "HDF5DataFilePlugin.h"
#include "../common/AutoIncrVector.h"

class HDF5DataFilePluginDetector : public HDF5DataFilePlugin {
    AutoIncrVector<uint64_t> jf_info;
    AutoIncrVector<uint8_t> storage_cell;
    AutoIncrVector<uint64_t> receiver_aq_dev_delay;
    AutoIncrVector<uint64_t> receiver_free_buffers;
    AutoIncrVector<float> efficiency;
    AutoIncrVector<uint64_t> packets_received;
    AutoIncrVector<uint64_t> packets_expected;
    AutoIncrVector<int64_t> max_value;
public:
    void OpenFile(HDF5File &data_file, const DataMessage& msg) override;
    void Write(const DataMessage& msg, uint64_t image_number) override;
    void WriteFinal(HDF5File &data_file) override;
};

#endif //JUNGFRAUJOCH_HDF5DATAFILEPLUGINJUNGFRAU_H
