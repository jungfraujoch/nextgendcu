// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "HDF5DataFilePluginDetector.h"

void HDF5DataFilePluginDetector::OpenFile(HDF5File &in_data_file, const DataMessage &msg) {}

void HDF5DataFilePluginDetector::Write(const DataMessage &msg, uint64_t image_number) {
    if (msg.jf_info.has_value())
        jf_info[image_number] = msg.jf_info.value();
    if (msg.storage_cell.has_value())
        storage_cell[image_number] = msg.storage_cell.value();
    if (msg.receiver_aq_dev_delay.has_value())
        receiver_aq_dev_delay[image_number] = msg.receiver_aq_dev_delay.value();
    if (msg.receiver_free_send_buf.has_value())
        receiver_free_buffers[image_number] = msg.receiver_free_send_buf.value();

    if (msg.packets_received.has_value())
        packets_received[image_number] = msg.packets_received.value();
    if (msg.packets_expected.has_value())
        packets_expected[image_number] = msg.packets_expected.value();

    if (msg.image_collection_efficiency.has_value())
        efficiency[image_number] = msg.image_collection_efficiency.value();
    else
        efficiency[image_number] = 1.0;

    if (msg.max_viable_pixel_value.has_value())
        max_value[image_number] = msg.max_viable_pixel_value.value();
    else {
        // TODO: For non-jungfraujoch systems, calculate the value
        max_value[image_number] = 0;
    }

}

void HDF5DataFilePluginDetector::WriteFinal(HDF5File &data_file) {
    if (!jf_info.empty())
        data_file.SaveVector("/entry/detector/det_info", jf_info.vec());
    if (!storage_cell.empty())
        data_file.SaveVector("/entry/detector/storage_cell_image", storage_cell.vec());
    if (!receiver_aq_dev_delay.empty())
        data_file.SaveVector("/entry/detector/rcv_delay", receiver_aq_dev_delay.vec());
    if (!receiver_free_buffers.empty())
        data_file.SaveVector("/entry/detector/rcv_free_send_buffers", receiver_free_buffers.vec());
    if (!packets_received.empty())
        data_file.SaveVector("/entry/detector/packets_received", packets_received.vec());
    if (!packets_expected.empty())
        data_file.SaveVector("/entry/detector/packets_expected", packets_expected.vec());

    HDF5Group(data_file, "/entry/image").NXClass("NXCollection");

    data_file.SaveVector("/entry/image/max_value", max_value.vec());
    data_file.SaveVector("/entry/detector/data_collection_efficiency_image", efficiency.vec());
}
