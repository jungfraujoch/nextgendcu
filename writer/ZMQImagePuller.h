// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_ZMQIMAGEPULLER_H
#define JUNGFRAUJOCH_ZMQIMAGEPULLER_H

#include <future>

#include "../common/ZMQWrappers.h"
#include "../common/Logger.h"
#include "../common/SpotToSave.h"
#include "../frame_serialize/CBORStream2Deserializer.h"
#include "../common/ThreadSafeFIFO.h"

struct ZMQImagePullerOutput {
    std::shared_ptr<ZMQMessage> msg;
    std::shared_ptr<CBORStream2DeserializerOutput> cbor;
};

struct ZMQImagePullerStatistics {
    size_t cbor_fifo_max_util;
    size_t outside_fifo_max_util;
    size_t repub_fifo_max_util;
};

class ZMQImagePuller {
    const uint32_t default_receive_watermark = 100;
    const uint32_t default_repub_watermark = 220;

    // ZeroMQ receive timeout allows to check for abort value from time to time
    constexpr static auto ReceiveTimeout = std::chrono::milliseconds(100);
    constexpr static auto RepubTimeout = std::chrono::milliseconds(100);

    ZMQSocket socket;
    std::string addr;

    volatile int abort = 0;
    volatile int disconnect = 0;

    std::unique_ptr<ZMQSocket> repub_socket;

    ThreadSafeFIFO<ZMQImagePullerOutput> cbor_fifo{200};
    ThreadSafeFIFO<ZMQImagePullerOutput> repub_fifo{200};
    ThreadSafeFIFO<ZMQImagePullerOutput> outside_fifo{200};

    std::thread puller_thread;
    std::thread cbor_thread;
    std::thread repub_thread;

    void PullerThread();
    void CBORThread();
    void RepubThread();
    Logger logger{"ZMQImagePuller"};
public:
    explicit ZMQImagePuller(const std::string &repub_address = "",
        const std::optional<int32_t> &rcv_watermark = {},
        const std::optional<int32_t> &repub_watermark = {});
    ~ZMQImagePuller();
    void Connect(const std::string &in_address);
    void Disconnect();
    void Abort();

    [[nodiscard]] ZMQImagePullerOutput WaitForImage();
    [[nodiscard]] ZMQImagePullerStatistics GetStatistics() const;
};

#endif //JUNGFRAUJOCH_ZMQIMAGEPULLER_H
