// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "HDF5DataFilePluginXFEL.h"

void HDF5DataFilePluginXFEL::OpenFile(HDF5File &data_file, const DataMessage &msg) {}

void HDF5DataFilePluginXFEL::Write(const DataMessage &msg, uint64_t image_number) {
    if (!msg.xfel_pulse_id.has_value()
        || !msg.xfel_event_code.has_value())
        return;

    pulseid[image_number] = msg.xfel_pulse_id.value();
    event_code[image_number] = msg.xfel_event_code.value();
}

void HDF5DataFilePluginXFEL::WriteFinal(HDF5File &data_file) {
    if (!pulseid.empty()) {
        HDF5Group group(data_file, "/entry/xfel");
        group.NXClass("NXcollection");
        group.SaveVector("pulseID", pulseid.vec());
        group.SaveVector("eventCode", event_code.vec());
    }
}
