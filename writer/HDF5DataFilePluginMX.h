// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_HDF5DATAFILEPLUGINMX_H
#define JUNGFRAUJOCH_HDF5DATAFILEPLUGINMX_H

#include "HDF5DataFilePlugin.h"
#include "../common/AutoIncrVector.h"

class HDF5DataFilePluginMX : public HDF5DataFilePlugin {
    size_t max_spots;

    // spots
    std::vector<float> spot_x;
    std::vector<float> spot_y;
    std::vector<float> spot_int;
    std::vector<float> spot_indexed;

    AutoIncrVector<uint32_t> npeaks;
    AutoIncrVector<uint32_t> strong_pixel_count;
    AutoIncrVector<uint32_t> spot_count_rings;

    // indexing
    AutoIncrVector<uint8_t> indexed;
    std::vector<float> indexed_lattice;

    // bkg_estimate
    AutoIncrVector<float> bkg_estimate;
public:
    explicit HDF5DataFilePluginMX(size_t max_spots);
    void OpenFile(HDF5File &data_file, const DataMessage& msg) override;
    void Write(const DataMessage& msg, uint64_t image_number) override;
    void WriteFinal(HDF5File &data_file) override;
};

#endif //JUNGFRAUJOCH_HDF5DATAFILEPLUGINMX_H
