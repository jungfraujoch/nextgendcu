// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_HDF5DATAFILEPLUGINROI_H
#define JFJOCH_HDF5DATAFILEPLUGINROI_H

#include <map>
#include "../common/AutoIncrVector.h"
#include "HDF5DataFilePlugin.h"

struct ROIData {
    AutoIncrVector<int64_t> max;
    AutoIncrVector<int64_t> sum;
    AutoIncrVector<int64_t> sum_sq;
    AutoIncrVector<int64_t> npixel;
};

class HDF5DataFilePluginROI : public HDF5DataFilePlugin {
    std::map<std::string, ROIData> roi_data;
public:
    void OpenFile(HDF5File &data_file, const DataMessage &msg) override;
    void Write(const DataMessage &msg, uint64_t image_number) override;
    void WriteFinal(HDF5File &data_file) override;
};

#endif //JFJOCH_HDF5DATAFILEPLUGINROI_H
