// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_HDF5DATAFILEPLUGINAZINT_H
#define JUNGFRAUJOCH_HDF5DATAFILEPLUGINAZINT_H

#include "HDF5DataFilePlugin.h"

class HDF5DataFilePluginAzInt : public HDF5DataFilePlugin {
    std::vector<float> az_int_bin_to_q;
    std::vector<float> az_int;
public:
    explicit HDF5DataFilePluginAzInt(const std::vector<float> &rad_int_bin_to_q);
    void OpenFile(HDF5File &data_file, const DataMessage& msg) override;
    void Write(const DataMessage& msg, uint64_t image_number) override;
    void WriteFinal(HDF5File &data_file) override;
};

#endif //JUNGFRAUJOCH_HDF5DATAFILEPLUGINAZINT_H
