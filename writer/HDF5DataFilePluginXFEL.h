// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_HDF5DATAFILEPLUGINXFEL_H
#define JUNGFRAUJOCH_HDF5DATAFILEPLUGINXFEL_H

#include "HDF5DataFilePlugin.h"
#include "../common/AutoIncrVector.h"

class HDF5DataFilePluginXFEL : public HDF5DataFilePlugin {
    AutoIncrVector<uint64_t> pulseid;
    AutoIncrVector<uint32_t> event_code;
public:
    void OpenFile(HDF5File &data_file, const DataMessage& msg) override;
    void Write(const DataMessage& msg, uint64_t image_number) override;
    void WriteFinal(HDF5File &data_file) override;
};


#endif //JUNGFRAUJOCH_HDF5DATAFILEPLUGINXFEL_H
