// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_HDF5DATAFILEPLUGIN_H
#define JUNGFRAUJOCH_HDF5DATAFILEPLUGIN_H

#include "../frame_serialize/JFJochMessages.h"
#include "HDF5Objects.h"

class HDF5DataFilePlugin {
protected:
    size_t max_image_number = 0;
public:
    virtual void OpenFile(HDF5File &data_file, const DataMessage& msg) = 0;
    virtual void Write(const DataMessage& msg, uint64_t image_number) = 0;
    virtual void WriteFinal(HDF5File &data_file) = 0;
    virtual ~HDF5DataFilePlugin() = default;
};

#endif //JUNGFRAUJOCH_HDF5DATAFILEPLUGIN_H
