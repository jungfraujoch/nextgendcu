// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_JFJOCHWRITERHTTP_H
#define JUNGFRAUJOCH_JFJOCHWRITERHTTP_H

#include <pistache/endpoint.h>
#include <pistache/router.h>
#include <pistache/client.h>

#include "../common/Logger.h"
#include "StreamWriter.h"
#include "gen/api/DefaultApi.h"

class JFJochWriterHttp : public org::openapitools::server::api::DefaultApi {
    StreamWriter& writer;

    void status_get(Pistache::Http::ResponseWriter &response) override;

    void version_get(Pistache::Http::ResponseWriter &response) override;

    void cancel_post(Pistache::Http::ResponseWriter &response) override;
public:
    JFJochWriterHttp(StreamWriter& writer, std::shared_ptr<Pistache::Rest::Router> &rtr);
};


#endif //JUNGFRAUJOCH_JFJOCHWRITERHTTP_H
