// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "HDF5DataFilePluginAzInt.h"

#define RESERVE_IMAGES 1000

HDF5DataFilePluginAzInt::HDF5DataFilePluginAzInt(const std::vector<float> &in_rad_int_bin_to_q) :
az_int_bin_to_q(in_rad_int_bin_to_q) {}

void HDF5DataFilePluginAzInt::OpenFile(HDF5File &data_file, const DataMessage &msg) {
    if (az_int_bin_to_q.empty())
        return;

    HDF5Group(data_file, "/entry/azint").NXClass("NXcollection");
    data_file.SaveVector("/entry/azint/bin_to_q", az_int_bin_to_q);

    az_int.reserve(RESERVE_IMAGES * az_int_bin_to_q.size());
}

void HDF5DataFilePluginAzInt::Write(const DataMessage &msg, uint64_t image_number) {
    if (az_int_bin_to_q.empty())
        return;

    if (image_number >= max_image_number) {
        max_image_number = image_number;
        az_int.resize((max_image_number + 1) * az_int_bin_to_q.size());
    }

    if (!msg.az_int_profile.empty() && (msg.az_int_profile.size() == az_int_bin_to_q.size())) {
        for (int i = 0; i < az_int_bin_to_q.size(); i++)
            az_int[image_number * az_int_bin_to_q.size() + i] = msg.az_int_profile[i];
    }
}

void HDF5DataFilePluginAzInt::WriteFinal(HDF5File &data_file) {
    if (!az_int.empty())
        data_file.SaveVector("/entry/azint/image", az_int, {(hsize_t) (max_image_number + 1), az_int_bin_to_q.size()});
}
