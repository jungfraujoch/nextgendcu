// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_HDF5NXMX_H
#define JUNGFRAUJOCH_HDF5NXMX_H

#include "../frame_serialize/JFJochMessages.h"

#include "HDF5Objects.h"

namespace HDF5Metadata {
    std::string DataFileName(const StartMessage &msg, int64_t file_number);
}

class NXmx {
    std::unique_ptr<HDF5File> hdf5_file;
    const StartMessage start_message;
    const std::string filename;
    std::string tmp_filename;
    bool overwrite = false;
    bool calibration_group_created = false;

    void LinkToData(const StartMessage &start, const EndMessage &end);
    void LinkToData_VDS(const StartMessage &start, const EndMessage &end);
    std::unique_ptr<HDF5DataSet> VDS(const StartMessage &start,
                                     const std::string& name,
                                     const std::vector<hsize_t> &dim,
                                     const HDF5DataType &data_type);

    std::unique_ptr<HDF5DataSet> VDS(const StartMessage &start,
                                     const std::string& name_src,
                                     const std::string& name_dest,
                                     const std::vector<hsize_t> &dim,
                                     const HDF5DataType &data_type);

    void Detector(const StartMessage &start);
    void Detector(const EndMessage &end);
    void DetectorModule(const std::string &name, const std::vector<int32_t> &origin,
                        const std::vector<int32_t> &size, const std::vector<double> &fast_axis,
                        const std::vector<double> &slow_axis, const std::string &nx_axis, double pixel_size_mm);

    void Facility(const StartMessage &start);
    void Beam(const StartMessage &start);
    void Metrology(const StartMessage &start);
    void Sample(const StartMessage &start, const EndMessage &end);
    void Attenuator(const StartMessage &start);

    void SaveCBORImage(const std::string& hdf5_path, const CompressedImage &image);
    void AzimuthalIntegration(const StartMessage &start, const EndMessage &end);
    void ADUHistogram(const EndMessage &end);
    void UserData(const StartMessage &start);
public:
    NXmx(const StartMessage& start);
    ~NXmx();
    NXmx(const NXmx &other) = delete;
    NXmx& operator=(const NXmx &other) = delete;
    void Finalize(const EndMessage &end);
    void WriteCalibration(const CompressedImage &image);
};

#endif //JUNGFRAUJOCH_HDF5NXMX_H
