// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_DETECTORWRAPPER_H
#define JUNGFRAUJOCH_DETECTORWRAPPER_H

#include <sls/Detector.h>
#include "../common/DiffractionExperiment.h"
#include "../common/Logger.h"

#define BASE_DETECTOR_MAC 0xAABBCCDDEE10 // little-endian!
#define THRESHOLD_TEMPERATURE_DEGC 55

enum class DetectorState {IDLE, ERROR, BUSY, WAITING, NOT_CONNECTED};
enum class DetectorPowerState {ON, OFF, PARTIAL};

struct DetectorStatus {
    std::vector<int64_t> temperature_fpga_degC;
    std::vector<int64_t> high_voltage_V;
    DetectorState detector_state;
    DetectorPowerState power_state;
    std::string detector_server_version;
    int64_t remaining_triggers;
};

class DetectorWrapper {
    Logger logger{"DetectorWrapper"};
    DetectorType det_type;
    sls::Detector det;
    void InternalStop();
    [[nodiscard]] int64_t GetNumberOfTriggersLeft() const;
    [[nodiscard]] DetectorPowerState GetPowerState() const;
    int64_t GetFirmwareVersion() const;
    std::string GetDetectorServerVersion() const;
    std::vector<int64_t> GetFPGATemperatures() const;
    std::vector<int64_t> GetHighVoltage() const;
public:
    [[nodiscard]] DetectorState GetState() const;
    void Initialize(const DiffractionExperiment& experiment, const std::vector<AcquisitionDeviceNetConfig>& net_config);
    void Configure(const DiffractionExperiment& experiment);
    void Start(const DiffractionExperiment& experiment);
    void Stop();
    void Trigger();
    void Deactivate();

    DetectorStatus GetStatus() const;
};


#endif //JUNGFRAUJOCH_DETECTORWRAPPER_H
