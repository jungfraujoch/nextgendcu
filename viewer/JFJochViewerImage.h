// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCHVIEWERIMAGE_H
#define JFJOCHVIEWERIMAGE_H

#include <QGraphicsView>
#include <QVector>
#include "../reader/JFJochHDF5Reader.h"
#include "../common/ColorScale.h"

class JFJochViewerImage : public QGraphicsView {
Q_OBJECT

    QColor feature_color = Qt::magenta;
    QColor spot_color = Qt::green;

    JFJochReader &reader;
public:
    JFJochViewerImage(JFJochReader &reader,
                      QWidget *parent = nullptr);
signals:
    void foregroundChanged(float val);
protected:
    void wheelEvent(QWheelEvent *event) override;   // Zooming
    void mousePressEvent(QMouseEvent *event) override;  // Panning start
    void mouseMoveEvent(QMouseEvent *event) override;   // Panning interaction
    void resizeEvent(QResizeEvent *event) override;
    void updateOverlay();
    void LoadImageInternal();
    void Redraw();
    void DrawResolutionRings();
    void DrawSpots();
    void DrawBeamCenter();
    void DrawTopPixels();
    void DrawSaturation();
    void DrawCross(float x, float y, float size, float width, float z = 1);
    double scale_factor = 1.0;
    ColorScale color_scale;
    std::shared_ptr<JFJochReaderImage> image;
    std::vector<rgb> image_rgb;
    std::vector<QGraphicsItem *> overlay;

    float foreground = 10;
    float background = 0;
    int32_t show_highest_pixels = 0;

    QVector<float> res_ring = {2.0, 3.0};
    bool res_ring_auto = false;
    bool show_spots = true;
    bool show_saturation = false;
private:
    QPoint lastMousePos;  // To track panning movement
protected slots:
    void onScroll(int value);
public slots:
    void loadImage();
    void noImage();
    void changeForeground(float val);
    void changeBackground(float val);

    void setResolutionRing(QVector<float> v);
    void setResolutionRingAuto();
    void showSpots(bool input);

    void setColorMap(int color_map);
    void setFeatureColor(QColor input);
    void setSpotColor(QColor input);

    void showHighestPixels(int32_t v);
    void showSaturation(bool input);
};



#endif //JFJOCHVIEWERIMAGE_H
