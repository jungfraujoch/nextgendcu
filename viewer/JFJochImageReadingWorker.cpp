// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "JFJochImageReadingWorker.h"
#include <QVector>
#include <iostream>

JFJochImageReadingWorker::JFJochImageReadingWorker(JFJochHDF5Reader &reader,
                                                   QObject *parent) : QObject(parent), reader(reader) {}

void JFJochImageReadingWorker::LoadFile(const QString &filename) {
    try {
        reader.ReadFile(filename.toStdString());
        current_image = 0;
        total_images = reader.GetNumberOfImages();
        LoadImage(current_image);
        emit datasetLoaded();
    } catch (std::exception &e) {
        std::cerr << "E2 " << e.what();
    }
}

void JFJochImageReadingWorker::CloseFile() {
    reader.Close();
    current_image = 0;
    total_images = 0;
    emit noImageLoaded();
}

void JFJochImageReadingWorker::LoadImage(int64_t image_number) {
    try {
        if (image_number < 0 || image_number >= total_images)
            return;

        std::vector<int32_t> image;
        reader.LoadImage(image_number);
        current_image = image_number;
        emit imageNumberChanged(total_images, current_image);
        emit imageLoaded();
    } catch (std::exception &e) {
        std::cerr << "E1 " << e.what();
    }
}
