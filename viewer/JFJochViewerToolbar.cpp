// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "JFJochViewerToolbar.h"
#include "../common/ColorScale.h"

#include <QPushButton>
#include <QSlider>
#include <QLabel>
#include <QLineEdit>
#include <QIntValidator>
#include <cmath>

JFJochViewerToolbar::JFJochViewerToolbar(QWidget *parent) : QToolBar(parent) {
    auto title = new QLabel("<b>Image number</b>   ", this);
    addWidget(title);

    leftmost_button = new QPushButton("|⇐", this);
    leftmost_button->setFixedWidth(32);
    addWidget(leftmost_button);

    left_button = new QPushButton("⇐", this);
    left_button->setFixedWidth(32);
    addWidget(left_button);

    current_image_edit_validator = new QIntValidator(0, 1000, this);

    current_image_edit = new QLineEdit("0", this);
    current_image_edit->setFixedWidth(50);
    current_image_edit->setAlignment(Qt::AlignmentFlag::AlignRight);
    current_image_edit->setValidator(current_image_edit_validator);

    addWidget(current_image_edit);

    total_number_label = new QLabel("/0", this);
    addWidget(total_number_label);

    right_button = new QPushButton("⇒", this);
    right_button->setFixedWidth(32);
    addWidget(right_button);

    rightmost_button = new QPushButton("⇒|", this);
    rightmost_button->setFixedWidth(32);
    addWidget(rightmost_button);

    image_number_slider = new QSlider(Qt::Horizontal, this);
    addWidget(image_number_slider);

    auto *stretch = new QWidget(this);
    stretch->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    addWidget(stretch);

    addSeparator();
    auto title_fg = new QLabel("<b>Foreground </b>   ", this);
    addWidget(title_fg);

    foreground_edit = new QLineEdit("10", this);
    foreground_edit->setAlignment(Qt::AlignmentFlag::AlignRight);
    foreground_edit->setFixedWidth(80);
    foreground_edit_validator = new QIntValidator(0, 32000, this);
    foreground_edit->setValidator(foreground_edit_validator);
    addWidget(foreground_edit);


    foreground_slider = new QSlider(Qt::Horizontal, this);
    foreground_slider->setRange(1, 255);
    foreground_slider->setValue(10);
    addWidget(foreground_slider);

    // Initialize QComboBox with the options
    color_map_select = new QComboBox(this);
    color_map_select->addItem("Viridis", static_cast<int>(ColorScaleEnum::Viridis));
    color_map_select->addItem("Heat", static_cast<int>(ColorScaleEnum::Heat));
    color_map_select->addItem("Indigo", static_cast<int>(ColorScaleEnum::Indigo));
    color_map_select->addItem("B/W", static_cast<int>(ColorScaleEnum::BW));
    color_map_select->setCurrentIndex(static_cast<int>(ColorScaleEnum::Indigo));
    addWidget(color_map_select);

    auto *stretch_2 = new QWidget(this);
    stretch_2->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Preferred);
    addWidget(stretch_2);

    setImageNumber(0, 0);

    connect(left_button, &QPushButton::clicked, this, &JFJochViewerToolbar::leftButtonPressed);
    connect(right_button, &QPushButton::clicked, this, &JFJochViewerToolbar::rightButtonPressed);
    connect(leftmost_button, &QPushButton::clicked, this, &JFJochViewerToolbar::leftmostButtonPressed);
    connect(rightmost_button, &QPushButton::clicked, this, &JFJochViewerToolbar::rightmostButtonPressed);

    connect(current_image_edit, &QLineEdit::editingFinished, this, &JFJochViewerToolbar::editFinalized);
    connect(image_number_slider, &QSlider::sliderMoved, this, &JFJochViewerToolbar::sliderMoved);
    connect(foreground_slider, &QSlider::sliderMoved, this, &JFJochViewerToolbar::foregroundSliderMoved);
    connect(foreground_edit, &QLineEdit::editingFinished, this, &JFJochViewerToolbar::foregroundEditFinalized);
    connect(color_map_select, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &JFJochViewerToolbar::colorComboBoxSet);
}

void JFJochViewerToolbar::setImageNumber(int64_t total_images, int64_t current_image) {
    image_number = total_images;
    curr_image = current_image;

    if (total_images > 0) {
        current_image_edit->setDisabled(false);
        current_image_edit->setText(QString::number(curr_image));
        current_image_edit_validator->setRange(0, image_number - 1);

        left_button->setDisabled(curr_image == 0);
        right_button->setDisabled(curr_image == image_number - 1);
        leftmost_button->setDisabled(curr_image == 0);
        rightmost_button->setDisabled(curr_image == image_number - 1);

        image_number_slider->setRange(0, image_number - 1);
        image_number_slider->setValue(curr_image);
        image_number_slider->setEnabled(true);
    } else {
        left_button->setDisabled(true);
        right_button->setDisabled(true);
        leftmost_button->setDisabled(true);
        rightmost_button->setDisabled(true);

        current_image_edit->setText(QString::number(0));
        image_number_slider->setValue(0);
        image_number_slider->setDisabled(true);
    }
    total_number_label->setText("/" + QString::number(image_number));
}

void JFJochViewerToolbar::leftButtonPressed() {
    if (curr_image > 0) {
        emit loadImage(curr_image - 1);
    }
}

void JFJochViewerToolbar::rightButtonPressed() {
    if (curr_image < image_number - 1) {
        emit loadImage(curr_image + 1);
    }
}

void JFJochViewerToolbar::rightmostButtonPressed() {
    emit loadImage(image_number - 1);
}

void JFJochViewerToolbar::leftmostButtonPressed() {
    emit loadImage(0);
}

void JFJochViewerToolbar::sliderMoved(int val) {
    emit loadImage(val);
}

void JFJochViewerToolbar::editFinalized() {
    bool ok;
    int editedValue = current_image_edit->text().toInt(&ok);

    if (ok && editedValue >= 0 && editedValue <= image_number - 1) {
        emit loadImage(editedValue);
    }
}

void JFJochViewerToolbar::foregroundSliderMoved(int val) {
    foreground = val;
    foreground_edit->setText(QString::number(std::lround(foreground)));
    emit setForeground(foreground);
}

void JFJochViewerToolbar::colorComboBoxSet(int val) {
    emit colorMapChanged(val);
}

void JFJochViewerToolbar::updateForeground(float val) {
    int val_int = std::lround(val);
    foreground_edit->setText(QString::number(val_int));
    foreground_slider->setValue(std::clamp(val_int, 1, foreground_slider->maximum()));
}

void JFJochViewerToolbar::foregroundEditFinalized() {
    bool ok;
    int editedValue = foreground_edit->text().toInt(&ok);

    if (ok && editedValue >= 1 && editedValue <= INT16_MAX) {
        foreground_slider->setValue(std::clamp(editedValue, 1, foreground_slider->maximum()));
        emit setForeground(editedValue);
    }
}