// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_JFJOCHVIEWERTOOLBAR_H
#define JFJOCH_JFJOCHVIEWERTOOLBAR_H

#include <QToolBar>
#include <QLabel>
#include <QLineEdit>
#include <QIntValidator>
#include <QPushButton>
#include <QSlider>
#include <QComboBox>
#define MAX_FOREGROUND 32000

class JFJochViewerToolbar : public QToolBar {
Q_OBJECT

    size_t image_number;
    int64_t curr_image;

    float foreground;

    QLabel *total_number_label;
    QLineEdit *current_image_edit;
    QIntValidator *current_image_edit_validator;

    QPushButton *leftmost_button;
    QPushButton *left_button;
    QPushButton *right_button;
    QPushButton *rightmost_button;

    QSlider *image_number_slider;
    QSlider *foreground_slider;
    QLineEdit *foreground_edit;
    QIntValidator *foreground_edit_validator;

    QComboBox *color_map_select;
signals:
    void loadImage(int64_t number);
    void setForeground(float val);
    void colorMapChanged(int val);
public:
    explicit JFJochViewerToolbar(QWidget *parent = nullptr);

public slots:
    void setImageNumber(int64_t total_images, int64_t current_image);
    void updateForeground(float val);
private slots:

    void leftButtonPressed();
    void rightButtonPressed();
    void leftmostButtonPressed();
    void rightmostButtonPressed();

    void editFinalized();
    void sliderMoved(int val);
    void foregroundEditFinalized();

    void foregroundSliderMoved(int val);
    void colorComboBoxSet(int val);
};

#endif //JFJOCH_JFJOCHVIEWERTOOLBAR_H
