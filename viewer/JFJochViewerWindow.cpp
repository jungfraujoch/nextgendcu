// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "JFJochViewerWindow.h"

#include <QSplitter>

#include "JFJochImageReadingWorker.h"
#include "JFJochViewerImage.h"
#include "JFJochViewerSidePanel.h"
#include <QThread>

JFJochViewerWindow::JFJochViewerWindow(QWidget *parent) : QMainWindow(parent) {
    menuBar = new JFJochViewerMenu(this);
    setMenuBar(menuBar);

    toolBar = new JFJochViewerToolbar(this);
    addToolBar(toolBar);

    setStyleSheet(stylesheet);
    setWindowTitle("Jungfraujoch image viewer");
    resize(1200,1200);

    auto v_splitter = new QSplitter(this);
    setCentralWidget(v_splitter);
    v_splitter->setOrientation(Qt::Vertical);

    auto h_splitter = new QSplitter(this);
    h_splitter->setOrientation(Qt::Horizontal);
    v_splitter->addWidget(h_splitter);

    auto viewer = new JFJochViewerImage(reader, this);
    viewer->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    h_splitter->addWidget(viewer);

    auto side_panel = new JFJochViewerSidePanel(reader, this);
    side_panel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    h_splitter->addWidget(side_panel);

    dataset_info = new JFJochViewerDatasetInfo(reader, this);
    dataset_info->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Minimum);
    v_splitter->addWidget(dataset_info);

    reading_worker = new JFJochImageReadingWorker(reader);
    reading_thread = new QThread(this);
    reading_worker->moveToThread(reading_thread);
    reading_thread->start();

    connect(menuBar, &JFJochViewerMenu::fileOpenSelected,
        reading_worker, &JFJochImageReadingWorker::LoadFile, Qt::QueuedConnection);

    connect(menuBar, &JFJochViewerMenu::fileCloseSelected,
            reading_worker, &JFJochImageReadingWorker::CloseFile, Qt::QueuedConnection);

    connect(reading_worker, &JFJochImageReadingWorker::imageLoaded,
            viewer,&JFJochViewerImage::loadImage,
        Qt::QueuedConnection);
    connect(reading_worker, &JFJochImageReadingWorker::imageLoaded,
            side_panel, &JFJochViewerSidePanel::loadImage,
            Qt::QueuedConnection);

    connect(reading_worker, &JFJochImageReadingWorker::noImageLoaded,
            viewer, &JFJochViewerImage::noImage, Qt::QueuedConnection);

    connect(reading_worker, &JFJochImageReadingWorker::imageNumberChanged, toolBar,
            &JFJochViewerToolbar::setImageNumber, Qt::QueuedConnection);

    connect(toolBar, &JFJochViewerToolbar::loadImage, reading_worker, &JFJochImageReadingWorker::LoadImage,
            Qt::QueuedConnection);

    connect(toolBar, &JFJochViewerToolbar::setForeground, viewer,
            &JFJochViewerImage::changeForeground);

    connect(toolBar, &JFJochViewerToolbar::colorMapChanged, viewer,
            &JFJochViewerImage::setColorMap);

    connect(viewer,&JFJochViewerImage::foregroundChanged,
            toolBar, &JFJochViewerToolbar::updateForeground);

    connect(reading_worker, &JFJochImageReadingWorker::datasetLoaded,
            dataset_info, &JFJochViewerDatasetInfo::datasetLoaded,
            Qt::QueuedConnection);

    connect(reading_worker, &JFJochImageReadingWorker::imageLoaded,
            dataset_info, &JFJochViewerDatasetInfo::imageLoaded,
            Qt::QueuedConnection);

    connect(dataset_info, &JFJochViewerDatasetInfo::imageSelected,
            reading_worker, &JFJochImageReadingWorker::LoadImage,
            Qt::QueuedConnection);

    connect(side_panel, &JFJochViewerSidePanel::showSpots,
            viewer, &JFJochViewerImage::showSpots);

    connect(side_panel, &JFJochViewerSidePanel::autoResRings,
            viewer, &JFJochViewerImage::setResolutionRingAuto);
    connect(side_panel, &JFJochViewerSidePanel::setFeatureColor,
            viewer, &JFJochViewerImage::setFeatureColor);
    connect(side_panel, &JFJochViewerSidePanel::setSpotColor,
            viewer, &JFJochViewerImage::setSpotColor);
    connect(side_panel, &JFJochViewerSidePanel::showHighestPixels,
            viewer, &JFJochViewerImage::showHighestPixels);
    connect(side_panel, &JFJochViewerSidePanel::showSaturatedPixels,
            viewer, &JFJochViewerImage::showSaturation);
    connect(side_panel, &JFJochViewerSidePanel::setResRings,
            viewer, &JFJochViewerImage::setResolutionRing);

}

JFJochViewerWindow::~JFJochViewerWindow() {
    if (reading_thread && reading_thread->isRunning()) {
        reading_thread->quit();
        reading_thread->wait();
    }

    delete reading_worker;
}