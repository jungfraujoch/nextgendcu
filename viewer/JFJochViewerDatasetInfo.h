// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_JFJOCHVIEWERDATASETINFO_H
#define JFJOCH_JFJOCHVIEWERDATASETINFO_H

#include <QComboBox>
#include "widgets/JFJochChartView.h"

#include "../reader/JFJochReader.h"

class JFJochViewerDatasetInfo : public QWidget {
    Q_OBJECT
    JFJochReader &reader;
    QComboBox *combo_box;
    JFJochChartView *chart_view;
    const std::vector<float> *GetDataset();

signals:
    void imageSelected(int64_t number);
public:
    explicit JFJochViewerDatasetInfo(JFJochReader &reader, QWidget *parent = nullptr);
private slots:
    void imageSelectedInChart(int64_t number);
    void comboBoxSelected(int val);
public slots:
    void datasetLoaded();
    void imageLoaded();
};


#endif //JFJOCH_JFJOCHVIEWERDATASETINFO_H
