// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <QGridLayout>
#include <QPushButton>

#include "JFJochViewerDatasetInfo.h"

JFJochViewerDatasetInfo::JFJochViewerDatasetInfo(JFJochReader &in_reader, QWidget *parent)
        : reader(in_reader), QWidget(parent) {
    setFixedHeight(200);

    auto layout = new QGridLayout(this);
    combo_box = new QComboBox(this);

    layout->addWidget(combo_box, 0, 0);

    auto reset_button = new QPushButton("Reset zoom", this);
    layout->addWidget(reset_button, 0, 1);

    chart_view = new JFJochChartView(this);
    layout->addWidget(chart_view, 1, 0, 1, 2);

    connect(chart_view, &JFJochChartView::imageSelected,
            this, &JFJochViewerDatasetInfo::imageSelectedInChart);

    connect(reset_button, &QPushButton::clicked,
            chart_view, &JFJochChartView::resetZoom);

    setLayout(layout);
}

void JFJochViewerDatasetInfo::datasetLoaded() {
    auto image = reader.CopyImage();
    auto dataset = reader.GetStartMessage();

    if (dataset && image) {
        combo_box->clear();

        if (!dataset->bkg_estimate.empty())
            combo_box->addItem("Background estimate", 0);
        if (!dataset->spot_count.empty())
            combo_box->addItem("Spot count", 1);
        if (!dataset->indexing_result.empty())
            combo_box->addItem("Indexing result", 2);

        for (int i = 0; i < dataset->roi.size(); i++) {
            combo_box->addItem(QString::fromStdString("ROI " + dataset->roi[i] + " sum"),
                                                      1024 + 4 * i);
            combo_box->addItem(QString::fromStdString("ROI " + dataset->roi[i] + " pixels"),
                                                      1024 + 4 * i + 1);
        }
        connect(combo_box, &QComboBox::currentIndexChanged,
                this, &JFJochViewerDatasetInfo::comboBoxSelected);

        comboBoxSelected(0);
    } else
        chart_view->loadValues<float>({}, 0);
}

void JFJochViewerDatasetInfo::imageLoaded() {
    auto image = reader.CopyImage();
    if (image)
        chart_view->setImage(image->number);
}

void JFJochViewerDatasetInfo::imageSelectedInChart(int64_t number) {
    emit imageSelected(number);
}

void JFJochViewerDatasetInfo::comboBoxSelected(int index) {
    int val = combo_box->itemData(index).toInt();
    auto image = reader.CopyImage();
    auto dataset = reader.GetStartMessage();

    std::vector<float> tmp;
    if (image && dataset) {
        if (val == 0)
            chart_view->loadValues(dataset->bkg_estimate, image->number);
        else if (val == 1)
            chart_view->loadValues(dataset->spot_count, image->number);
        else if (val == 2)
            chart_view->loadValues(dataset->indexing_result, image->number);
        else if ((val & 1024)) {
            int roi_number = (val - 1024) / 4;
            if (roi_number < dataset->roi.size()) {
                if (val % 4 == 0)
                    chart_view->loadValues(dataset->roi_sum[roi_number], image->number);
                else if (val % 4 == 1)
                    chart_view->loadValues(dataset->roi_npixel[roi_number], image->number);
            }
        }
    }
}
