// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "TitleLabel.h"

TitleLabel::TitleLabel(QString text, QWidget *parent) : QLabel(parent) {
    setText(QString("<h3>%1</h3>").arg(text));
    setStyleSheet("background-color: #4B0082; color: #ffffff;");
    setAlignment(Qt::AlignmentFlag::AlignCenter);
    setFixedHeight(50);
}
