// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only


#include "JFJochChartView.h"

JFJochChartView::JFJochChartView(QWidget *parent)
: QChartView(new QChart(), parent) {
    chart()->legend()->hide();
    setRenderHint(QPainter::Antialiasing);
    setRubberBand(QChartView::RubberBand::RectangleRubberBand);
}

void JFJochChartView::setImage(int64_t val) {
    if (!currentSeries)
        return;

    curr_image = val;

    if (val < values.size() && val >= 0) {
        currentSeries->clear();
        currentSeries->append(curr_image, values[curr_image]);
    }
}

void JFJochChartView::mousePressEvent(QMouseEvent *event) {
    if (event->button() == Qt::LeftButton) {
        QPointF clickedPoint = event->pos();
        QPointF chartCoord = chart()->mapToValue(clickedPoint);
        int64_t val = std::lround(chartCoord.x());
        if (val >= 0 && val < values.size())
            emit imageSelected(val);
    }
    QChartView::mousePressEvent(event); // Call the base implementation
}

void JFJochChartView::resetZoom() {
    chart()->zoomReset();
}

void JFJochChartView::updateChart() {
    chart()->removeAllSeries();
    if (values.size() >= binning) {
        // At least one full point

        series = new QLineSeries(this);
        currentSeries = new QScatterSeries(this);

        if (binning == 1) {
            for (int i = 0; i < values.size(); i++)
                series->append(i, values[i]);
        } else {
            for (int i = 0; i < values.size() / binning; i++) {
                float tmp = 0;
                for (int b = 0; b < binning; b++)
                    tmp += values[i * binning + b];
                series->append((i + 0.5) * binning, tmp / binning);
            }
        }

        if (curr_image < values.size() && curr_image >= 0)
            currentSeries->append(curr_image, values[curr_image]);

        chart()->addSeries(series);
        chart()->addSeries(currentSeries);
        chart()->createDefaultAxes();
    }
}

void JFJochChartView::setBinning(int64_t val) {
    if (val >= 1) {
        binning = val;
        updateChart();
    }
}