// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_TITLELABEL_H
#define JFJOCH_TITLELABEL_H

#include <QLabel>

class TitleLabel : public QLabel {
public:
    TitleLabel(QString text, QWidget *parent);
};


#endif //JFJOCH_TITLELABEL_H
