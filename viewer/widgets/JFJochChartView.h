// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_JFJOCHCHARTVIEW_H
#define JFJOCH_JFJOCHCHARTVIEW_H

#include <QtCharts/QChartView>
#include <QtCharts/QValueAxis>
#include <QtCharts/QLineSeries>
#include <QtCharts/QScatterSeries>
#include <QMouseEvent>

class JFJochChartView : public QChartView {
    Q_OBJECT

    QLineSeries *series = nullptr;
    QScatterSeries *currentSeries = nullptr;

    std::vector<float> values;
    int64_t binning = 1;
    int64_t curr_image = 0;

    void updateChart();
signals:
    void imageSelected(int64_t number);
private:
    void mousePressEvent(QMouseEvent *event) override;
public:
    explicit JFJochChartView(QWidget *parent = nullptr);
    void setImage(int64_t val);

public slots:
    void resetZoom();
    void setBinning(int64_t val);
public:
    template <class T>
    void loadValues(const std::vector<T> &input, int64_t image) {
        values.resize(input.size());
        for (int i = 0; i < input.size(); i++)
            values[i] = static_cast<float>(input[i]);
        curr_image = image;
        updateChart();
    }
};


#endif //JFJOCH_JFJOCHCHARTVIEW_H
