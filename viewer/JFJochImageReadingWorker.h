// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCHIMAGEREADINGWORKER_H
#define JFJOCHIMAGEREADINGWORKER_H

#include <QObject>
#include <QString>
#include <QVector>

#include "../reader/JFJochHDF5Reader.h"

class JFJochImageReadingWorker : public QObject {
    Q_OBJECT

    JFJochHDF5Reader &reader;

    int64_t current_image = 0;
    int64_t total_images = 0;

signals:
    void datasetLoaded();
    void imageLoaded();
    void noImageLoaded();
    void imageNumberChanged(int64_t total_images, int64_t current_image);
public:
    explicit JFJochImageReadingWorker(JFJochHDF5Reader &reader,
                                      QObject *parent = nullptr);
    ~JFJochImageReadingWorker() override = default;
public slots:
    void LoadFile(const QString &filename);
    void CloseFile();
    void LoadImage(int64_t image_number);
};


#endif //JFJOCHIMAGEREADINGWORKER_H
