// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCHVIEWERMENU_H
#define JFJOCHVIEWERMENU_H

#include <QMenuBar>

class JFJochViewerMenu : public QMenuBar {
    Q_OBJECT

public:
    explicit JFJochViewerMenu(QWidget *parent = nullptr);
    ~JFJochViewerMenu() override = default;

signals:
    void fileOpenSelected(const QString &filename);
    void fileCloseSelected();

private slots:
    void aboutSelected(); // Slot for the About action
    void quitSelected(); // Slot for the Quit action
    void openSelected(); // Slot for the Open action
    void closeSelected(); // Slot for the Close action
};



#endif //JFJOCHVIEWERMENU_H
