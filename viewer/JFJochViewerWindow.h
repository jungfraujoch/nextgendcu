// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCHVIEWERWINDOW_H
#define JFJOCHVIEWERWINDOW_H

#include <QMainWindow>

#include "JFJochViewerMenu.h"
#include "JFJochViewerToolbar.h"
#include "../reader/JFJochHDF5Reader.h"
#include "JFJochImageReadingWorker.h"
#include "JFJochViewerDatasetInfo.h"

class JFJochViewerWindow : public QMainWindow {
    Q_OBJECT
    const QString stylesheet = "background-color: rgb(216, 228, 253);";
public:
    explicit JFJochViewerWindow(QWidget *parent = nullptr);
    ~JFJochViewerWindow() override;
private:
    JFJochHDF5Reader reader;
    JFJochViewerToolbar *toolBar;
    JFJochViewerMenu *menuBar;
    JFJochViewerDatasetInfo *dataset_info;
    JFJochImageReadingWorker *reading_worker;
    QThread *reading_thread;
};



#endif //JFJOCHVIEWERWINDOW_H
