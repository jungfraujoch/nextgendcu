#include <QApplication>

#include "JFJochViewerWindow.h"
#include "../writer/HDF5Objects.h"

int main(int argc, char *argv[]) {

    RegisterHDF5Filter();
    QApplication app(argc, argv);

    JFJochViewerWindow mainWindow;
    mainWindow.show();

    return QApplication::exec();
}
