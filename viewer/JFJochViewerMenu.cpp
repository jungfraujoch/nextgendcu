// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "JFJochViewerMenu.h"

#include <QMessageBox>
#include <QApplication>
#include <QFileDialog>
#include <QString>

#include "../common/GitInfo.h"

JFJochViewerMenu::JFJochViewerMenu(QWidget *parent) : QMenuBar(parent) {
    // Create "File" menu
    QMenu *fileMenu = addMenu("&File");

    const QAction *openAction = fileMenu->addAction("&Open");
    connect(openAction, &QAction::triggered, this, &JFJochViewerMenu::openSelected);

    const QAction *closeAction = fileMenu->addAction("&Close");
    connect(closeAction, &QAction::triggered, this, &JFJochViewerMenu::closeSelected);

    // Add seperator
    fileMenu->addSeparator();

    // Add "Quit" action
    const QAction *quitAction = fileMenu->addAction("&Quit");
    connect(quitAction, &QAction::triggered, this, &JFJochViewerMenu::quitSelected);

    QMenu *helpMenu = addMenu("&Help");
    // Add "About" action
    const QAction *aboutAction = helpMenu->addAction("About");
    connect(aboutAction, &QAction::triggered, this, &JFJochViewerMenu::aboutSelected);

}

void JFJochViewerMenu::aboutSelected() {
    QString version(QString::fromStdString(jfjoch_version()));
    // Create QMessageBox
    QMessageBox aboutBox(this);
    aboutBox.setWindowTitle("About Jungfraujoch Image Viewer");

    // Set detailed text with proper spacing and structure
    aboutBox.setText(
        "<h3>Jungfraujoch Image Viewer</h3>" // Use HTML for improved style
        "<p><b>Version:</b> " + version + "</p>"
        "<p><b>Copyright:</b> 2019-2025 Paul Scherrer Institute</p>"
        "<p><b>Author:</b> Filip Leonarski &lt;filip.leonarski@psi.ch&gt;</p>"
    );

    // Optional: Add a larger informative text, if needed
    aboutBox.setInformativeText(
        "This program comes with ABSOLUTELY NO WARRANTY. "
        "This is free software, and you are welcome to redistribute it "
        "under certain conditions (GPLv3).\n\n"
        "Development supported by Innovation Project (101.535.1 IP-ENG) from Innosuisse."
    );

    // Set an icon
    aboutBox.setIcon(QMessageBox::Information);

    // Execute the dialog
    aboutBox.exec();
}

void JFJochViewerMenu::openSelected() {
    QString fileName = QFileDialog::getOpenFileName(
        this,
        "Open File",                // Dialog title
        "",                        // Default folder
        "HDF5 Master Files (*_master.h5);; HDF5 Files (*.h5);;All Files (*)" // Filter for .h5 files
    );

    if (!fileName.isEmpty())
        emit fileOpenSelected(fileName);
}

void JFJochViewerMenu::closeSelected() {
    emit fileCloseSelected();
}

void JFJochViewerMenu::quitSelected() {
    QApplication::quit();
}
