// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_JFJOCHVIEWERSIDEPANEL_H
#define JFJOCH_JFJOCHVIEWERSIDEPANEL_H

#include <QLabel>
#include <QWidget>
#include <QLineEdit>
#include <QCheckBox>

#include "../reader/JFJochReader.h"

class JFJochViewerSidePanel : public QWidget {
    Q_OBJECT

    JFJochReader &reader;
    QLineEdit *res_rings_edit = nullptr;
    QLabel *image_stats;

    QCheckBox *autoResRingsCheckBox;
    QCheckBox *resRingsCheckBox;

    void SetImageStats(const std::shared_ptr<JFJochReaderImage> &image);

signals:
    void showSpots(bool input);
    void autoResRings();
    void setResRings(QVector<float> v);
    void setFeatureColor(QColor input);
    void setSpotColor(QColor input);
    void showHighestPixels(int32_t v);
    void showSaturatedPixels(bool input);
public:
    JFJochViewerSidePanel(JFJochReader &reader,
                          QWidget *parent);
public slots:
    void loadImage();

private slots:
    void editingFinished();
    void enableResRings(bool input);
    void enableAutoResRings(bool input);
    void spotsToggled(bool input);
    void saturatedPixelsToggled(bool input);
};


#endif //JFJOCH_JFJOCHVIEWERSIDEPANEL_H
