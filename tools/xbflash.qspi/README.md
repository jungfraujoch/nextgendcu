# xbflash.qspi

This program allows to flash firmware of Xilinx/AMD FPGA cards via PCIe edge and QSPI core.
This tool is taken from the Xilinx/AMD [XRT repository](https://githb.com/Xilinx/XRT) and extracted for convenience, since the original repository has
an extreme number of dependencies. 

Note: Xilinx has two tools `xbflash.qspi` and `xbflash2`. 
The latter is only repackaging of the original code with extra Boost libraries for nice options parsing.
Thus, we decided not to only take the original program, to not increase number of dependencies of the code.

## Usage
`xbflash.qspi` has to be run as root.

```
xbflash.qspi --primary <name of image> --card <PCIe id of the card>
```
