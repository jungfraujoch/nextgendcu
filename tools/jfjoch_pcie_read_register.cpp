// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "../common/Logger.h"
#include "../common/JFJochException.h"
#include "../fpga/host_library/JungfraujochDevice.h"
#include "../common/NetworkAddressConvert.h"


int main(int argc, char **argv) {
    Logger logger("jfjoch_pcie_read_register");

    if (argc != 3) {
        std::cout << "Usage: ./jfjoch_pcie_status <device name> <reg number hex>" << std::endl;
        exit(EXIT_FAILURE);
    }

    logger.Info("Device {}", argv[1]);

    char *endptr;
    uint32_t reg_number = std::strtol(argv[2], &endptr, 16);
    if (*endptr != '\0') {
        logger.Error("Invalid hexadecimal number: {} ", argv[2]);
        exit(EXIT_FAILURE);
    }

    JungfraujochDevice dev(argv[1], false);
    logger.Info("Reg 0x{:x} = 0x{:x}", reg_number,dev.ReadRegister(reg_number));
}
