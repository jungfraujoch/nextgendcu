// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "UDPSimulator.h"
#include <sys/socket.h>
#include <sys/types.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <cstring>
#include <fstream>

#include "../common/JFJochException.h"
#include "../common/Definitions.h"
#include "../fpga/hls_simulation/sls_packet.h"

UDPSimulator::UDPSimulator(const std::vector<uint16_t> &in_image) : image(in_image) {
    if (image.size() != RAW_MODULE_SIZE)
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "Input image for simulator is wrong");

    fd = socket(AF_INET, SOCK_DGRAM, 0);
    if (fd < 0)
        throw JFJochException(JFJochExceptionCategory::UDPError,
                              "Cannot create UDP socket");
}

UDPSimulator::~UDPSimulator() {
    close(fd);
}

void UDPSimulator::SendImage(const std::string &ipv4_dest_addr,
                             uint16_t udp_port,
                             uint64_t frame_number,
                             uint16_t module_number) {
    std::unique_lock ul(m);

    struct sockaddr_in addr {
        .sin_family = AF_INET,
        .sin_port = htons(udp_port),
        .sin_addr = {
                .s_addr = inet_addr(ipv4_dest_addr.c_str())
        }
    };

    if (inet_pton(AF_INET, ipv4_dest_addr.c_str(), &addr.sin_addr.s_addr) <= 0)
        throw JFJochException(JFJochExceptionCategory::UDPError, "Cannot parse address " + ipv4_dest_addr);

    uint16_t half_module_number = 2 * module_number;
    jf_udp_payload packet{
            .framenum = frame_number,
            .row = half_module_number,
            .detectortype = SLS_DETECTOR_TYPE_JUNGFRAU
    };

    for (int i = 0; i < 128; i++) {
        packet.packetnum = i;
        memcpy(packet.data, image.data() + 4096 * i, 4096 * sizeof(uint16_t));
        sendto(fd, &packet, sizeof(jf_udp_payload), 0, (struct sockaddr *) &addr, sizeof(addr));
    }
}

