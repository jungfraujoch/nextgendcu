// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_UDPSIMULATOR_H
#define JUNGFRAUJOCH_UDPSIMULATOR_H

#include <string>
#include <vector>
#include <mutex>

class UDPSimulator {
    std::mutex m;
    int fd;
    const std::vector<uint16_t> &image;
public:
    UDPSimulator(const std::vector<uint16_t> &image);
    ~UDPSimulator();
    void SendImage(const std::string &ipv4_dest_addr, uint16_t udp_port, uint64_t frame_number, uint16_t module_number);
};


#endif //JUNGFRAUJOCH_UDPSIMULATOR_H
