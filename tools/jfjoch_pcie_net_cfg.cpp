// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "../common/Logger.h"
#include "../fpga/host_library/JungfraujochDevice.h"
#include "../common/NetworkAddressConvert.h"

void print_if(const std::string &if_name, const NetworkStatus &net_status, Logger &logger) {
    logger.Info("{:10s} MAC {:18s} IPv4 {:15s} Link {} Mode {:4d} Packets eth. {:10d} ICMP {:10d}", if_name,
                MacAddressToStr(net_status.mac_addr),
                IPv4AddressToStr(net_status.ipv4_addr),
                net_status.stat_rx_status? "*" : "-",
                net_status.jfjoch_net_mode,
                net_status.packets_eth,
                net_status.packets_icmp);
}

int main(int argc, char **argv) {
    Logger logger("jfjoch_pcie_net_cfg");

    if ((argc < 2) || (argc > 5)) {
        logger.Error("Usage:");
        logger.Error("./jfjoch_pcie_net_cfg <device name>");
        logger.Error("     Read configuration for all network interface of a device");
        logger.Error("./jfjoch_pcie_net_cfg <device name> <if number>|fgen ");
        logger.Error("     Read configuration for a particular network interface / internal frame generator");
        logger.Error("./jfjoch_pcie_net_cfg <device name> <if number>|fgen ipv4 <IPv4 address>");
        logger.Error("     Set IPv4 address for a particular network interface / internal frame generator");
        logger.Error("./jfjoch_pcie_net_cfg <device name> <if number>|fgen direct 0|1");
        logger.Error("     Set direct mode for a particular network interface / internal frame generator");
        logger.Error("./jfjoch_pcie_net_cfg <device name> <if number>|fgen clear");
        logger.Error("     Clear Ethernet counter for a particular network interface / internal frame generator");

        exit(EXIT_FAILURE);
    }

    uint32_t interface = 0;
    if (argc > 2) {
        if (strncmp(argv[2], "fgen", 5) == 0)
            interface = NET_IF_FRAME_GENERATOR;
        else
            interface = atol(argv[2]);
    }
    bool rw = (argc > 3); // with 1 or 2 arguments, access can be R/O
    try {
        JungfraujochDevice device(argv[1], rw);
        auto dev_status = device.GetDeviceStatus();
        if (argc == 5) {
            if (strncmp(argv[3], "ipv4",5) == 0)
                device.SetIPv4Address(IPv4AddressFromStr(std::string(argv[4])), interface);
            else if (strncmp(argv[3], "direct",6) == 0) {
                if (strncmp(argv[4], "1", 2) == 0) {
                    uint32_t mode = device.GetNetworkMode(interface);
                    mode |= 2;
                    device.SetNetworkMode(mode, interface);
                } else if (strncmp(argv[4], "0", 2) == 0) {
                    uint32_t mode = device.GetNetworkMode(interface);
                    mode &= ~2;
                    device.SetNetworkMode(mode, interface);
                } else
                    throw std::runtime_error("Only 0|1 allowed as option for direct settings");
            } else
                throw std::runtime_error("Option not supported");
        } else if (argc == 4) {
            if (strncmp(argv[3], "clear",6) == 0) {
                uint32_t mode = device.GetNetworkMode(interface);
                mode |= 4;
                device.SetNetworkMode(mode, interface);
                mode &= ~4;
                device.SetNetworkMode(mode, interface);
            } else
                throw std::runtime_error("Option not supported");
        }
        if (argc == 2) {
            auto status = device.GetDeviceStatus();
            for (int i = 0; i < status.eth_link_count; i++)
                print_if("net" + std::to_string(i), device.GetNetworkStatus(i),logger);
            print_if("fgen", device.GetNetworkStatus(NET_IF_FRAME_GENERATOR), logger);
        } else {
            if (interface == NET_IF_FRAME_GENERATOR)
                print_if("fgen", device.GetNetworkStatus(interface), logger);
            else
                print_if("net" + std::to_string(interface), device.GetNetworkStatus(interface), logger);
        }
    } catch (const std::runtime_error &e) {
        logger.ErrorException(e);
    }
}
