// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "UDPSimulator.h"

#include <chrono>
#include <fstream>

#include "../common/Logger.h"
#include "../common/Definitions.h"

int main(int argc, char **argv) {

    Logger logger("jfjoch_udp_simulator");

    if ((argc < 3) || (argc > 6)) {
        logger.Error("Usage ./jfjoch_udp_simulator <IPv4 address> <UDP dest port> {<# of frames> <content file>}");
        exit(EXIT_FAILURE);
    }

    const std::string ipv4_addr(argv[1]);


    const uint16_t udp_port = std::strtol(argv[2], nullptr, 10);

    logger.Info("Address to send: {}:{}", ipv4_addr, udp_port);

    uint64_t nframes = 1;
    if (argc == 4)
        nframes = std::strtol(argv[3], nullptr, 10);
    logger.Info("Frames to send: {}", nframes);

    const std::vector<uint16_t> image(RAW_MODULE_SIZE, 0);

    if (argc == 5) {
        std::fstream file(argv[4], std::fstream::in | std::fstream::binary);
        if (file.is_open())
            file.read((char *) image.data(), RAW_MODULE_SIZE * sizeof(uint16_t));
        else {
            logger.Error("Cannot open file {}", argv[4]);
            exit(EXIT_FAILURE);
        }
        logger.Info("Loaded file {}", argv[4]);
    }

    logger.Info("Starting...");

    auto start_time = std::chrono::system_clock::now();

    UDPSimulator simulator(image);
    for (int i = 0 ; i < nframes; i++)
        simulator.SendImage(ipv4_addr, udp_port, i + 1, 0);

    auto end_time = std::chrono::system_clock::now();
    logger.Info("   ... done");

    auto elapsed = std::chrono::duration_cast<std::chrono::microseconds>(end_time - start_time);
    logger.Info("Performance {:.0f} Hz", static_cast<double>(nframes*1000*1000)/static_cast<double>(elapsed.count()));
}
