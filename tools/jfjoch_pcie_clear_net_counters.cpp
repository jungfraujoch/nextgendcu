// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <iostream>

#include "../common/JFJochException.h"
#include "../fpga/host_library/JungfraujochDevice.h"
#include "../common/Logger.h"

int main(int argc, char **argv) {
    Logger logger("jfjoch_pcie_clear_net_counters");

    if (argc != 2) {
        logger.Error("Usage: ./jfjoch_pcie_clear_net_counters <device name>");
        exit(EXIT_FAILURE);
    }

    logger.Info("Device {}", argv[1]);

    try {
        JungfraujochDevice device(argv[1], true);
        device.ClearNetworkCounters();
        logger.Info("Done");
    } catch (const std::runtime_error &e) {
        logger.ErrorException(e);
    }
}