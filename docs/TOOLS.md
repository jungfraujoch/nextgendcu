# Tools

## jfjoch_pcie_status
Prints detailed status information about the card. Execute by adding device path, e.g.:
```
./jfjoch_pcie_status /dev/jfjoch0
```
The program is safe to execute during a running data collection.

## jfjoch_pcie_clear_net_counters
Network counters in the card give information about Ethernet, UDP and ICMP packets encountered by the network stack prior to Jungfraujoch logic.
These counters are running from the moment card is powered on. They can be reset by running the program with device name, e.g.:
```
./jfjoch_pcie_clear_net_counters /dev/jfjoch0
```

## jfjoch_pcie_net_cfg
Network configuration can be retrieved and modified with `jfjoch_pcie_net_cfg` tool. Usage:

```
jfjoch_pcie_net_cfg <device name>
     Read configuration for all network interface of a device
jfjoch_pcie_net_cfg <device name> <if number>|fgen
     Read configuration for a particular network interface / internal frame generator"
jfjoch_pcie_net_cfg <device name> <if number>|fgen ipv4 <IPv4 address>
     Set IPv4 address for a particular network interface / internal frame generator
jfjoch_pcie_net_cfg <device name> <if number>|fgen direct 0|1
     Set direct mode for a particular network interface / internal frame generator
jfjoch_pcie_net_cfg <device name> <if number>|fgen clear
     Clear Ethernet counter for a particular network interface / internal frame generator
```

## jfjoch_hdf5_tools

Tool to test single threaded HDF5 writer performance

## jfjoch_offline_process

Tool to run offline processing on existing HDF5 dataset

## jfjoch_udp_simulator

UDP simulator to test Jungfraujoch FPGA

## jfjoch_pcie_read_register

Tool to read verbatim Jungfraujoch FPGA registers