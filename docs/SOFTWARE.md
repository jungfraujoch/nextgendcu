# Software requirements
## Operating system
Recommended operating system is Red Hat Enterprise Linux (RHEL) / Rocky Linux versions 8.
For this operating system we provide RPMs with pre-built binaries to simplify deployment.

We do also operate one of the systems with Rocky 9 without issues.
Running Jungfraujoch on Red Hat Enterprise Linux 7 is currently not tested and not recommended,
but likely possible with providing some packages from external repositories.
There are some limited tests with recent Ubuntu and Fedora distributions, though these are not systematic.
Other linux platforms should work, but no tests were done so far.

## Software dependencies
Required:
* C++20 compiler and C++20 standard library; recommended GCC 11+ or clang 14+ (Intel OneAPI, AMD AOCC)
* CMake version 3.21 or newer + GNU make tool
* zlib compression library

Optional:
* CUDA compiler version 11 or newer - required for MX fast feedback indexer
* NUMA library - to pin threads to nodes/CPUs
* Node.js - to make frontend
* Qt version 6.8 (for jfjoch_viewer)

Automatically downloaded by CMake and statically linked:
* SLS Detector Package - see [github.com/slsdetectorgroup/slsDetectorPackage](https://github.com/slsdetectorgroup/slsDetectorPackage)
* Zstandard (Facebook) - see [github.com/facebook/zstd](https://github.com/facebook/zstd)
* Pistache webserver  - see [github.com/pistacheio/pistache](https://github.com/pistacheio/pistache)
* Fast feedback indexer (Hans-Christian Stadler, PSI) - see [github.com/paulscherrerinstitute/fast-feedback-indexer](https://github.com/paulscherrerinstitute/fast-feedback-indexer)
* Catch2 testing library - see [github.com/catchorg/Catch2](https://github.com/catchorg/Catch2)
* HDF5 library - see [github.com/HDFGroup/hdf5](https://github.com/HDFGroup/hdf5)
* TIFF library - see [gitlab.com/libtiff/libtiff](https://gitlab.com/libtiff/libtiff)

Please follow the link provided above to check for LICENSE file. Building code with dependencies above requires access from the build system to github.com.

Directly included in the repository:
* JSON parser/writer from N. Lohmann - see [github.com/nlohmann/json](https://github.com/nlohmann/json)
* Xilinx arbitrary precision arithmetic headers - see [github.com/Xilinx/HLS_arbitrary_Precision_Types](https://github.com/Xilinx/HLS_arbitrary_Precision_Types)
* Bitshuffle filter from K. Masui - see [github.com/kiyo-masui/bitshuffle](https://github.com/kiyo-masui/bitshuffle)
* Fast replacement for Bitshuffle pre-compression filter (Kal Cutter, DECTRIS)  - see [github.com/kalcutter/bitshuffle](https://github.com/kalcutter/bitshuffle)
* Tinycbor (Intel) - see [github.com/intel/tinycbor](https://github.com/intel/tinycbor)
* LZ4 compression by Y.Collet - see [github.com/lz4/lz4](https://github.com/lz4/lz4)
* Spdlog logging library - see [github.com/gabime/spdlog](https://github.com/gabime/spdlog)
* ZeroMQ library (through slsDetectorPackage) - see [github.com/zeromq/libzmq](https://github.com/zeromq/libzmq)

For license check LICENSE file in respective directory
