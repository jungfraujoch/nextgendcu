PSI Jungfraujoch
================

.. toctree::
   :maxdepth: 2
   :caption: General

   ACKNOWLEDGEMENT
   LICENSE
   DETECTORS
   HARDWARE
   SOFTWARE
   VERSIONING
   DEPLOYMENT
   CHANGELOG

.. toctree::
   :maxdepth: 2
   :caption: Software

   JFJOCH_BROKER
   JFJOCH_WRITER
   TOOLS

.. toctree::
   :maxdepth: 2
   :caption: FPGA

   FPGA
   FPGA_LICENSE
   FPGA_DESIGN
   FPGA_NETWORK
   FPGA_PCIE_DRIVER
   FPGA_SETTINGS

.. toctree::
   :maxdepth: 2
   :caption: Reference

   OPENAPI
   OPENAPI_SPECS
   CBOR
   ZEROMQ_STREAM
   PIXEL_MASK
   WEB_FRONTEND
   TESTS

.. toctree::
   :maxdepth: 1
   :caption: OpenAPI Python client

   python_client/README
   python_client/docs/DefaultApi