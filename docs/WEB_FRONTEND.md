# Web frontend
Jungfraujoch is equipped with React-based web frontend for user-friendly experience. Frontend has the following options:
* Presenting current state of the detector
* Plotting results of online quality calculations
* Showing live view images from the detector
* JUNGFRAU calibration numbers
* Configuring the detector, as well as pedestal/initialization operations

Frontend is written in TypeScript. For details see `frontend/` directory.