# FPGA data flow

The following steps are performed on FPGA (in the order of operation):

1. UDP header decoding
2. SLS detector header decoding
3. State machine that controls data acquisition (start/stop/cancel)
4. High-bandwidth memory cache to buffer network packets and reorder them to form full modules
5. ADU histogram for JUNGFRAU
6. Mask pixels from missing packets with special value
7. Reorder lines for EIGER to form a proper module
8. Mask pixels based on provided pixel mask
9. JUNGFRAU conversion with gain and pedestal corrections
10. Threshold to zero pixels below certain count value
11. Integration according to predefined map (e.g., 1D azimuthal integration)
12. Spot finding
13. ROI calculation
14. Image lossy compression using N*sqrt(pixel) values
15. Send images, analysis results and metadata to host memory via PCI Express

Each step has dedicated core, written in the high-level synthesis.