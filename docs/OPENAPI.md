# OpenAPI
## OpenAPI specs

See document with detailed [OpenAPI specs](OPENAPI_SPECS.rst).

## Python client
Jungfraujoch is controlled with HTTP/REST interface defined with an OpenAPI specification.
For convenience, we provide Python client as [jfjoch-client](https://pypi.org/project/jfjoch-client/) PyPi package.
To install the client you can use `pip` tool:
```
pip install jfjoch-client
```
See [API reference from the OpenAPI generator](python_client/README.md).