# RoiBox

Box ROI

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Name for the ROI; used in the plots | 
**min_x_pxl** | **int** | Lower bound (inclusive) in X coordinate for the box | 
**max_x_pxl** | **int** | Upper bound (inclusive) in X coordinate for the box | 
**min_y_pxl** | **int** | Lower bound (inclusive) in Y coordinate for the box | 
**max_y_pxl** | **int** | Upper bound (inclusive) in Y coordinate for the box | 

## Example

```python
from jfjoch_client.models.roi_box import RoiBox

# TODO update the JSON string below
json = "{}"
# create an instance of RoiBox from a JSON string
roi_box_instance = RoiBox.from_json(json)
# print the JSON string representation of the object
print(RoiBox.to_json())

# convert the object into a dict
roi_box_dict = roi_box_instance.to_dict()
# create an instance of RoiBox from a dict
roi_box_from_dict = RoiBox.from_dict(roi_box_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


