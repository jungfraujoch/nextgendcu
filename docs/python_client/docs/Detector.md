# Detector


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**description** | **str** |  | 
**serial_number** | **str** |  | 
**type** | [**DetectorType**](DetectorType.md) |  | [optional] 
**high_voltage_v** | **int** |  | [optional] [default to 0]
**udp_interface_count** | **int** |  | [optional] [default to 1]
**module_sync** | **bool** | Use module 0 as master for timing. Only applies to JUNGFRAU detector (this cannot be turned off for EIGER). | [optional] [default to True]
**sensor_thickness_um** | **float** |  | [optional] [default to 320]
**readout_time_us** | **int** | Minimum difference between frame time and count time in microseconds Defaults are 3 us for EIGER and 20 us for JUNGFRAU  | [optional] 
**calibration_file** | **List[str]** | Can be empty for all detectors - default calibration used. For JUNGFRAU: list of gain files, one entry per module. For EIGER: one directory (with detector settings) or list of trim bit files, one entry per half-module.  | [optional] 
**hostname** | **List[str]** | Hostname for detector module. One entry per module One entry per module. Either empty or number of module entries.  | [optional] 
**sensor_material** | **str** |  | [optional] [default to 'Si']
**tx_delay** | **List[int]** |  | [optional] 
**base_data_ipv4_address** | **str** |  | [optional] 
**standard_geometry** | [**StandardDetectorGeometry**](StandardDetectorGeometry.md) |  | [optional] 
**custom_geometry** | [**List[DetectorModule]**](DetectorModule.md) |  | [optional] 
**mirror_y** | **bool** | Mirror detector in Y direction to account for MX convention of (0,0) point in top left corner | [optional] [default to True]

## Example

```python
from jfjoch_client.models.detector import Detector

# TODO update the JSON string below
json = "{}"
# create an instance of Detector from a JSON string
detector_instance = Detector.from_json(json)
# print the JSON string representation of the object
print(Detector.to_json())

# convert the object into a dict
detector_dict = detector_instance.to_dict()
# create an instance of Detector from a dict
detector_from_dict = Detector.from_dict(detector_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


