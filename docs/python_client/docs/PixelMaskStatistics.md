# PixelMaskStatistics


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**user_mask** | **int** | Number of pixels masked using the user mask | [optional] 
**too_high_pedestal_rms** | **int** | Number of pixels with G0 pedestal RMS higher than provided threshold | [optional] 
**wrong_gain** | **int** | Number of pixels that show wrong gain level during the pedestal procedure | [optional] 

## Example

```python
from jfjoch_client.models.pixel_mask_statistics import PixelMaskStatistics

# TODO update the JSON string below
json = "{}"
# create an instance of PixelMaskStatistics from a JSON string
pixel_mask_statistics_instance = PixelMaskStatistics.from_json(json)
# print the JSON string representation of the object
print(PixelMaskStatistics.to_json())

# convert the object into a dict
pixel_mask_statistics_dict = pixel_mask_statistics_instance.to_dict()
# create an instance of PixelMaskStatistics from a dict
pixel_mask_statistics_from_dict = PixelMaskStatistics.from_dict(pixel_mask_statistics_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


