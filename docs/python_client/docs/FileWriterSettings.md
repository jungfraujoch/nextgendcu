# FileWriterSettings


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**overwrite** | **bool** | Inform jfjoch_write to overwrite existing files. Otherwise files would be saved with .h5.{timestamp}.tmp suffix.  | [optional] [default to False]
**file_writer_version** | **int** | version 1 - legacy format with soft links to data files in the master file; necessary for DECTRIS Albula 4.0 and DECTRIS Neggia   version 2 - newer format with virtual dataset linking data files in the master file, also includes better metadata handling  | [optional] [default to 2]

## Example

```python
from jfjoch_client.models.file_writer_settings import FileWriterSettings

# TODO update the JSON string below
json = "{}"
# create an instance of FileWriterSettings from a JSON string
file_writer_settings_instance = FileWriterSettings.from_json(json)
# print the JSON string representation of the object
print(FileWriterSettings.to_json())

# convert the object into a dict
file_writer_settings_dict = file_writer_settings_instance.to_dict()
# create an instance of FileWriterSettings from a dict
file_writer_settings_from_dict = FileWriterSettings.from_dict(file_writer_settings_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


