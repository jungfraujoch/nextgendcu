# DatasetSettingsUnitCell

Unit cell parameters. Necessary to run indexing. Units of angstrom and degree

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**a** | **float** |  | 
**b** | **float** |  | 
**c** | **float** |  | 
**alpha** | **float** |  | 
**beta** | **float** |  | 
**gamma** | **float** |  | 

## Example

```python
from jfjoch_client.models.dataset_settings_unit_cell import DatasetSettingsUnitCell

# TODO update the JSON string below
json = "{}"
# create an instance of DatasetSettingsUnitCell from a JSON string
dataset_settings_unit_cell_instance = DatasetSettingsUnitCell.from_json(json)
# print the JSON string representation of the object
print(DatasetSettingsUnitCell.to_json())

# convert the object into a dict
dataset_settings_unit_cell_dict = dataset_settings_unit_cell_instance.to_dict()
# create an instance of DatasetSettingsUnitCell from a dict
dataset_settings_unit_cell_from_dict = DatasetSettingsUnitCell.from_dict(dataset_settings_unit_cell_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


