# DetectorList


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**detectors** | [**List[DetectorListElement]**](DetectorListElement.md) |  | 
**current_id** | **int** |  | 

## Example

```python
from jfjoch_client.models.detector_list import DetectorList

# TODO update the JSON string below
json = "{}"
# create an instance of DetectorList from a JSON string
detector_list_instance = DetectorList.from_json(json)
# print the JSON string representation of the object
print(DetectorList.to_json())

# convert the object into a dict
detector_list_dict = detector_list_instance.to_dict()
# create an instance of DetectorList from a dict
detector_list_from_dict = DetectorList.from_dict(detector_list_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


