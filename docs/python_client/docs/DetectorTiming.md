# DetectorTiming


## Enum

* `AUTO` (value: `'auto'`)

* `TRIGGER` (value: `'trigger'`)

* `BURST` (value: `'burst'`)

* `GATED` (value: `'gated'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


