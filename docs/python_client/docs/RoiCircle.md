# RoiCircle

Circular ROI

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Name for the ROI; used in the plots | 
**center_x_pxl** | **float** | X coordinate of center of the circle [pixels] | 
**center_y_pxl** | **float** | Y coordinate of center of the circle [pixels] | 
**radius_pxl** | **float** | Radius of the circle [pixels] | 

## Example

```python
from jfjoch_client.models.roi_circle import RoiCircle

# TODO update the JSON string below
json = "{}"
# create an instance of RoiCircle from a JSON string
roi_circle_instance = RoiCircle.from_json(json)
# print the JSON string representation of the object
print(RoiCircle.to_json())

# convert the object into a dict
roi_circle_dict = roi_circle_instance.to_dict()
# create an instance of RoiCircle from a dict
roi_circle_from_dict = RoiCircle.from_dict(roi_circle_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


