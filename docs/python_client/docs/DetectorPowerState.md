# DetectorPowerState

Power on of ASICs

## Enum

* `POWERON` (value: `'PowerOn'`)

* `POWEROFF` (value: `'PowerOff'`)

* `PARTIAL` (value: `'Partial'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


