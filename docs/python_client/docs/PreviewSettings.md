# PreviewSettings

Settings for JPEG rendering of preview images

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**saturation** | **int** | Saturation value to set contrast in the preview image | 
**show_spots** | **bool** | Show spot finding results on the image | [optional] [default to True]
**show_roi** | **bool** | Show ROI areas on the image | [optional] [default to False]
**jpeg_quality** | **int** | Quality of JPEG image (100 - highest; 0 - lowest) | [optional] [default to 100]
**show_indexed** | **bool** | Preview indexed images only | [optional] [default to False]
**show_user_mask** | **bool** | Show user mask | [optional] [default to False]
**resolution_ring** | **float** |  | [optional] [default to 0.1]

## Example

```python
from jfjoch_client.models.preview_settings import PreviewSettings

# TODO update the JSON string below
json = "{}"
# create an instance of PreviewSettings from a JSON string
preview_settings_instance = PreviewSettings.from_json(json)
# print the JSON string representation of the object
print(PreviewSettings.to_json())

# convert the object into a dict
preview_settings_dict = preview_settings_instance.to_dict()
# create an instance of PreviewSettings from a dict
preview_settings_from_dict = PreviewSettings.from_dict(preview_settings_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


