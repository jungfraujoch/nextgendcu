# RoiCircleList

List of circular ROIs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rois** | [**List[RoiCircle]**](RoiCircle.md) |  | 

## Example

```python
from jfjoch_client.models.roi_circle_list import RoiCircleList

# TODO update the JSON string below
json = "{}"
# create an instance of RoiCircleList from a JSON string
roi_circle_list_instance = RoiCircleList.from_json(json)
# print the JSON string representation of the object
print(RoiCircleList.to_json())

# convert the object into a dict
roi_circle_list_dict = roi_circle_list_instance.to_dict()
# create an instance of RoiCircleList from a dict
roi_circle_list_from_dict = RoiCircleList.from_dict(roi_circle_list_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


