# ZeromqPreviewSettings


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **bool** | ZeroMQ preview socket is enabled. | [default to True]
**period_ms** | **int** | Period for generating preview image sent to the ZeroMQ interface in milliseconds. Default is 1 second.  If set to zero, all images will be sent ZeroMQ (should be used only in case of relatively slow data collection). This has no effect on HTTP based preview, which updates always at rate of 1 second.  | [default to 1000]
**socket_address** | **str** | PUB ZeroMQ socket for preview images. This socket operates at a reduced frame rate. Images are serialized using CBOR. Address follows ZeroMQ convention for sockets - in practice ipc://&lt;socket file&gt; and tcp://&lt;IP address&gt;:&lt;port&gt; sockets are OK. 0.0.0.0 instead of IP address is accepted and means listening on all network interfaces.  | [optional] 

## Example

```python
from jfjoch_client.models.zeromq_preview_settings import ZeromqPreviewSettings

# TODO update the JSON string below
json = "{}"
# create an instance of ZeromqPreviewSettings from a JSON string
zeromq_preview_settings_instance = ZeromqPreviewSettings.from_json(json)
# print the JSON string representation of the object
print(ZeromqPreviewSettings.to_json())

# convert the object into a dict
zeromq_preview_settings_dict = zeromq_preview_settings_instance.to_dict()
# create an instance of ZeromqPreviewSettings from a dict
zeromq_preview_settings_from_dict = ZeromqPreviewSettings.from_dict(zeromq_preview_settings_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


