# AzimIntSettings


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**polarization_factor** | **float** | If polarization factor is provided, than polarization correction is enabled. | [optional] 
**solid_angle_corr** | **bool** | Apply solid angle correction for radial integration | [default to True]
**high_q_recip_a** | **float** |  | 
**low_q_recip_a** | **float** |  | 
**q_spacing** | **float** |  | 

## Example

```python
from jfjoch_client.models.azim_int_settings import AzimIntSettings

# TODO update the JSON string below
json = "{}"
# create an instance of AzimIntSettings from a JSON string
azim_int_settings_instance = AzimIntSettings.from_json(json)
# print the JSON string representation of the object
print(AzimIntSettings.to_json())

# convert the object into a dict
azim_int_settings_dict = azim_int_settings_instance.to_dict()
# create an instance of AzimIntSettings from a dict
azim_int_settings_from_dict = AzimIntSettings.from_dict(azim_int_settings_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


