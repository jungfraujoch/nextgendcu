# DatasetSettings


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**images_per_trigger** | **int** | For standard synchrotron data collection - this is number of images collected per one TTL trigger For XFEL (pulsed source) - this number is ignored and set to 1 For storage cell mode - this number is ignored and set to number of storage cells  | [optional] [default to 1]
**ntrigger** | **int** | Number of TTL trigger that the detector is expected to receive during data collection  | [optional] [default to 1]
**image_time_us** | **int** | Image time.  If not provided (or zero value) the frame time is assumed as default. Image time must be multiple of frame time; max value is 256 * frame_time.             In XFEL mode: summation happens for frames collected with multiple triggers. Ignored for storage cells and if raw data are saved.  | [optional] 
**beam_x_pxl** | **float** | /entry/detector/beam_center_x in NXmx Beam center in X direction [pixels]  | 
**beam_y_pxl** | **float** | /entry/detector/beam_center_y in NXmx Beam center in X direction [pixels]  | 
**detector_distance_mm** | **float** | /entry/detector/distance in NXmx Detector distance [mm] | 
**incident_energy_ke_v** | **float** | Used to calculate /entry/beam/incident_wavelength in NXmx Incident particle (photon, electron) energy in keV  | 
**file_prefix** | **str** | Prefix for filenames. If left empty, no file will be saved. | [optional] [default to '']
**images_per_file** | **int** | Number of files in a single HDF5 data file (0 &#x3D; write all images to a single data file). | [optional] [default to 1000]
**space_group_number** | **int** | Number of space group for the crystal. Currently used solely as metadata, not relevant for image processing done in Jungfraujoch. | [optional] [default to 0]
**sample_name** | **str** | /entry/sample/name in NXmx Sample name  | [optional] [default to '']
**compression** | **str** | Compression type for the images transferred over ZeroMQ and saved to HDF5 file.  | [optional] [default to 'bslz4']
**total_flux** | **float** | /entry/beam/total_flux in NXmx Flux incident on beam plane in photons per second. In other words this is the flux integrated over area. [photons/s]  | [optional] 
**transmission** | **float** | /entry/instrument/attenuator/attenuator_transmission Transmission of attenuator (filter) [no units]  | [optional] 
**goniometer** | [**RotationAxis**](RotationAxis.md) |  | [optional] 
**header_appendix** | **object** | Header appendix, added as user_data/user to start ZeroMQ message (can be any valid JSON) In general, it is not saved in HDF5 file.  However, if values are placed in \&quot;hdf5\&quot; object, &#x60;jfjoch_writer&#x60; will write them in /entry/data of the HDF5 file.  This applies solely to string and number (double floating-point). No arrays/sub-objects is allowed. For example {\&quot;hdf5\&quot;: {\&quot;val1\&quot;:1, \&quot;val2\&quot;:\&quot;xyz\&quot;}}, will write /entry/user/val1 and /entry/user/val2.  | [optional] 
**image_appendix** | **object** | Image appendix, added as user_data to image ZeroMQ message (can be any valid JSON) Not saved in HDF5 file  | [optional] 
**data_reduction_factor_serialmx** | **float** | Rate at which non-indexed images are accepted to be forwarded to writer.  Value of 1.0 (default) means that all images are written. Values below zero mean that non-indexed images will be accepted with a given probability.  | [optional] [default to 1.0]
**pixel_value_low_threshold** | **int** | Set all counts lower than the value to zero.  When the value is set, negative numbers other than error pixel value are always set to zero. Setting to zero is equivalent to turning the option off.  | [optional] 
**run_number** | **int** | Number of run within an experimental session.  Transferred over CBOR stream as \&quot;series ID\&quot;, though not saved in HDF5 file. It is highly recommended to keep this number unique for each data collection during experimental series. If not provided, the number will be automatically incremented.  | [optional] 
**run_name** | **str** | Unique ID of run. Transferred over CBOR stream as \&quot;unique series ID\&quot;, though not saved in HDF5 file. It is highly recommended to keep this name unique for each data collection during experimental series. If not provided, the name will be automatically generated as number + colon + file_prefix.  | [optional] 
**experiment_group** | **str** | Name of group owning the data (e.g. p-group or proposal number).  Transferred over CBOR stream, though not saved in HDF5 file.  | [optional] 
**poisson_compression** | **int** | Enable lossy compression of pixel values that preserves Poisson statistics.  Requires to provide a numerical factor SQ. Pixel value P will be transformed to round(sqrt(P) * SQ), with rounding to the closest integer. Compression is turned off if the value is missing or it is set to zero.  | [optional] 
**write_nxmx_hdf5_master** | **bool** | Write NXmx formatted HDF5 master file. Recommended to use for macromolecular crystallography experiments and to turn off for other experiments.  | [optional] [default to True]
**save_calibration** | **bool** | Forward image calibration (at the moment pedestal and pedestal RMS for JUNGFRAU) using the ZeroMQ stream to writer. If parameter is not provided calibration will be saved only if more than 4 images are recorded.  | [optional] 
**unit_cell** | [**DatasetSettingsUnitCell**](DatasetSettingsUnitCell.md) |  | [optional] 

## Example

```python
from jfjoch_client.models.dataset_settings import DatasetSettings

# TODO update the JSON string below
json = "{}"
# create an instance of DatasetSettings from a JSON string
dataset_settings_instance = DatasetSettings.from_json(json)
# print the JSON string representation of the object
print(DatasetSettings.to_json())

# convert the object into a dict
dataset_settings_dict = dataset_settings_instance.to_dict()
# create an instance of DatasetSettings from a dict
dataset_settings_from_dict = DatasetSettings.from_dict(dataset_settings_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


