# JfjochSettings

Default settings for Jungfraujoch software.  This structure is used to provide default settings using configuration JSON file and is not used in HTTP. 

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pcie** | [**List[PcieDevicesInner]**](PcieDevicesInner.md) |  | [optional] 
**zeromq** | [**ZeromqSettings**](ZeromqSettings.md) |  | [optional] 
**instrument** | [**InstrumentMetadata**](InstrumentMetadata.md) |  | [optional] 
**file_writer** | [**FileWriterSettings**](FileWriterSettings.md) |  | [optional] 
**detector** | [**List[Detector]**](Detector.md) |  | 
**detector_settings** | [**DetectorSettings**](DetectorSettings.md) |  | [optional] 
**azim_int** | [**AzimIntSettings**](AzimIntSettings.md) |  | [optional] 
**image_format** | [**ImageFormatSettings**](ImageFormatSettings.md) |  | [optional] 
**image_buffer_mi_b** | **int** | Size of internal buffer in MiB for images before they are sent to a stream | [optional] [default to 2048]
**receiver_threads** | **int** | Number of threads used by the receiver | [optional] [default to 64]
**numa_policy** | **str** | NUMA policy to bind CPUs | [optional] 
**frontend_directory** | **str** | Location of built JavaScript web frontend | 
**image_pusher** | [**ImagePusherType**](ImagePusherType.md) |  | [default to ImagePusherType.NONE]
**zeromq_preview** | [**ZeromqPreviewSettings**](ZeromqPreviewSettings.md) |  | [optional] 
**zeromq_metadata** | [**ZeromqMetadataSettings**](ZeromqMetadataSettings.md) |  | [optional] 

## Example

```python
from jfjoch_client.models.jfjoch_settings import JfjochSettings

# TODO update the JSON string below
json = "{}"
# create an instance of JfjochSettings from a JSON string
jfjoch_settings_instance = JfjochSettings.from_json(json)
# print the JSON string representation of the object
print(JfjochSettings.to_json())

# convert the object into a dict
jfjoch_settings_dict = jfjoch_settings_instance.to_dict()
# create an instance of JfjochSettings from a dict
jfjoch_settings_from_dict = JfjochSettings.from_dict(jfjoch_settings_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


