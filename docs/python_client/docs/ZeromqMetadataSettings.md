# ZeromqMetadataSettings


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enabled** | **bool** | ZeroMQ metadata socket is enabled. | [default to True]
**period_ms** | **int** | Period for generating metadata package sent to the ZeroMQ interface in milliseconds.  | [default to 1000]
**socket_address** | **str** | PUB ZeroMQ socket for image metadata information. Image metadata are serialized using CBOR. Address follows ZeroMQ convention for sockets - in practice ipc://&lt;socket file&gt; and tcp://&lt;IP address&gt;:&lt;port&gt; sockets are OK. 0.0.0.0 instead of IP address is accepted and means listening on all network interfaces.  | [optional] 

## Example

```python
from jfjoch_client.models.zeromq_metadata_settings import ZeromqMetadataSettings

# TODO update the JSON string below
json = "{}"
# create an instance of ZeromqMetadataSettings from a JSON string
zeromq_metadata_settings_instance = ZeromqMetadataSettings.from_json(json)
# print the JSON string representation of the object
print(ZeromqMetadataSettings.to_json())

# convert the object into a dict
zeromq_metadata_settings_dict = zeromq_metadata_settings_instance.to_dict()
# create an instance of ZeromqMetadataSettings from a dict
zeromq_metadata_settings_from_dict = ZeromqMetadataSettings.from_dict(zeromq_metadata_settings_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


