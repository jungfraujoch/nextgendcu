# ImageBufferStatus


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**image_numbers** | **List[int]** | Image numbers currently present in the buffer. | 
**total_slots** | **int** | Number of slots in the image buffer. This number, compared to number of images in data collection and frame rate will determine \&quot;retention\&quot; rate of the image buffer.  | 
**available_slots** | **int** | Slots available for the data collection | 

## Example

```python
from jfjoch_client.models.image_buffer_status import ImageBufferStatus

# TODO update the JSON string below
json = "{}"
# create an instance of ImageBufferStatus from a JSON string
image_buffer_status_instance = ImageBufferStatus.from_json(json)
# print the JSON string representation of the object
print(ImageBufferStatus.to_json())

# convert the object into a dict
image_buffer_status_dict = image_buffer_status_instance.to_dict()
# create an instance of ImageBufferStatus from a dict
image_buffer_status_from_dict = ImageBufferStatus.from_dict(image_buffer_status_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


