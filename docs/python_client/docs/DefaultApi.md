# jfjoch_client.DefaultApi

All URIs are relative to *http://localhost:5232*

Method | HTTP request | Description
------------- | ------------- | -------------
[**cancel_post**](DefaultApi.md#cancel_post) | **POST** /cancel | Cancel running data collection
[**config_azim_int_get**](DefaultApi.md#config_azim_int_get) | **GET** /config/azim_int | Get radial integration configuration
[**config_azim_int_put**](DefaultApi.md#config_azim_int_put) | **PUT** /config/azim_int | Configure radial integration
[**config_detector_get**](DefaultApi.md#config_detector_get) | **GET** /config/detector | Get detector configuration
[**config_detector_put**](DefaultApi.md#config_detector_put) | **PUT** /config/detector | Change detector configuration
[**config_file_writer_get**](DefaultApi.md#config_file_writer_get) | **GET** /config/file_writer | Get file writer settings
[**config_file_writer_put**](DefaultApi.md#config_file_writer_put) | **PUT** /config/file_writer | Change file writer settings
[**config_image_format_conversion_post**](DefaultApi.md#config_image_format_conversion_post) | **POST** /config/image_format/conversion | Configure format for data collection with full conversion
[**config_image_format_get**](DefaultApi.md#config_image_format_get) | **GET** /config/image_format | Get image output format
[**config_image_format_put**](DefaultApi.md#config_image_format_put) | **PUT** /config/image_format | Change image output format
[**config_image_format_raw_post**](DefaultApi.md#config_image_format_raw_post) | **POST** /config/image_format/raw | Configure format for raw data collection
[**config_instrument_get**](DefaultApi.md#config_instrument_get) | **GET** /config/instrument | Get instrument metadata
[**config_instrument_put**](DefaultApi.md#config_instrument_put) | **PUT** /config/instrument | Change instrument metadata
[**config_internal_generator_image_put**](DefaultApi.md#config_internal_generator_image_put) | **PUT** /config/internal_generator_image | Load binary image for internal FPGA generator
[**config_internal_generator_image_tiff_put**](DefaultApi.md#config_internal_generator_image_tiff_put) | **PUT** /config/internal_generator_image.tiff | Load TIFF image for internal FPGA generator
[**config_mask_get**](DefaultApi.md#config_mask_get) | **GET** /config/mask | Get mask of the detector (binary)
[**config_mask_tiff_get**](DefaultApi.md#config_mask_tiff_get) | **GET** /config/mask.tiff | Get mask of the detector (TIFF)
[**config_roi_get**](DefaultApi.md#config_roi_get) | **GET** /config/roi | Get ROI definitions
[**config_roi_put**](DefaultApi.md#config_roi_put) | **PUT** /config/roi | Upload ROI definitions
[**config_select_detector_get**](DefaultApi.md#config_select_detector_get) | **GET** /config/select_detector | List available detectors
[**config_select_detector_put**](DefaultApi.md#config_select_detector_put) | **PUT** /config/select_detector | Select detector
[**config_spot_finding_get**](DefaultApi.md#config_spot_finding_get) | **GET** /config/spot_finding | Get data processing configuration
[**config_spot_finding_put**](DefaultApi.md#config_spot_finding_put) | **PUT** /config/spot_finding | Configure spot finding
[**config_user_mask_get**](DefaultApi.md#config_user_mask_get) | **GET** /config/user_mask | Get user mask of the detector (binary)
[**config_user_mask_put**](DefaultApi.md#config_user_mask_put) | **PUT** /config/user_mask | Upload user mask of the detector (binary)
[**config_user_mask_tiff_get**](DefaultApi.md#config_user_mask_tiff_get) | **GET** /config/user_mask.tiff | Get user mask of the detector (TIFF)
[**config_user_mask_tiff_put**](DefaultApi.md#config_user_mask_tiff_put) | **PUT** /config/user_mask.tiff | Upload user mask of the detector
[**config_zeromq_metadata_get**](DefaultApi.md#config_zeromq_metadata_get) | **GET** /config/zeromq_metadata | Get ZeroMQ metadata socket settings
[**config_zeromq_metadata_put**](DefaultApi.md#config_zeromq_metadata_put) | **PUT** /config/zeromq_metadata | Set ZeroMQ metadata settings
[**config_zeromq_preview_get**](DefaultApi.md#config_zeromq_preview_get) | **GET** /config/zeromq_preview | Get ZeroMQ preview settings
[**config_zeromq_preview_put**](DefaultApi.md#config_zeromq_preview_put) | **PUT** /config/zeromq_preview | Set ZeroMQ preview settings
[**deactivate_post**](DefaultApi.md#deactivate_post) | **POST** /deactivate | Prepare detector to turn off
[**detector_status_get**](DefaultApi.md#detector_status_get) | **GET** /detector/status | Get detector status
[**fpga_status_get**](DefaultApi.md#fpga_status_get) | **GET** /fpga_status | Get status of FPGA devices
[**image_buffer_clear_post**](DefaultApi.md#image_buffer_clear_post) | **POST** /image_buffer/clear | Clear image buffer
[**image_buffer_image_cbor_get**](DefaultApi.md#image_buffer_image_cbor_get) | **GET** /image_buffer/image.cbor | Get image message in CBOR format
[**image_buffer_start_cbor_get**](DefaultApi.md#image_buffer_start_cbor_get) | **GET** /image_buffer/start.cbor | Get Start message in CBOR format
[**image_buffer_status_get**](DefaultApi.md#image_buffer_status_get) | **GET** /image_buffer/status | Get status of the image buffers
[**initialize_post**](DefaultApi.md#initialize_post) | **POST** /initialize | Initialize detector and data acquisition
[**pedestal_post**](DefaultApi.md#pedestal_post) | **POST** /pedestal | Collect dark current for the detector
[**plot_azim_int_get**](DefaultApi.md#plot_azim_int_get) | **GET** /plot/azim_int | Generate radial integration profile
[**plot_bkg_estimate_get**](DefaultApi.md#plot_bkg_estimate_get) | **GET** /plot/bkg_estimate | Generate background estimate plot
[**plot_error_pixel_get**](DefaultApi.md#plot_error_pixel_get) | **GET** /plot/error_pixel | Generate error pixels plot
[**plot_image_collection_efficiency_get**](DefaultApi.md#plot_image_collection_efficiency_get) | **GET** /plot/image_collection_efficiency | Generate image collection efficiency plot
[**plot_indexing_rate_get**](DefaultApi.md#plot_indexing_rate_get) | **GET** /plot/indexing_rate | Generate indexing rate plot
[**plot_indexing_unit_cell_angle_get**](DefaultApi.md#plot_indexing_unit_cell_angle_get) | **GET** /plot/indexing_unit_cell_angle | Generate indexing unit cell angle plot
[**plot_indexing_unit_cell_get**](DefaultApi.md#plot_indexing_unit_cell_get) | **GET** /plot/indexing_unit_cell | Generate indexing unit cell length plots
[**plot_max_value_get**](DefaultApi.md#plot_max_value_get) | **GET** /plot/max_value | Generate maximum pixel value plot
[**plot_packets_received_get**](DefaultApi.md#plot_packets_received_get) | **GET** /plot/packets_received | Generate plot with number of received packets per image
[**plot_receiver_delay_get**](DefaultApi.md#plot_receiver_delay_get) | **GET** /plot/receiver_delay | Generate receiver delay plot
[**plot_receiver_free_send_buffers_get**](DefaultApi.md#plot_receiver_free_send_buffers_get) | **GET** /plot/receiver_free_send_buffers | Generate receiver free send buffer plot
[**plot_roi_max_count_get**](DefaultApi.md#plot_roi_max_count_get) | **GET** /plot/roi_max_count | Generate plot of ROI max count
[**plot_roi_sum_get**](DefaultApi.md#plot_roi_sum_get) | **GET** /plot/roi_sum | Generate ROI sum plot
[**plot_roi_valid_pixels_get**](DefaultApi.md#plot_roi_valid_pixels_get) | **GET** /plot/roi_valid_pixels | Generate plot of ROI valid pixels
[**plot_spot_count_get**](DefaultApi.md#plot_spot_count_get) | **GET** /plot/spot_count | Generate spot count plot
[**plot_strong_pixel_get**](DefaultApi.md#plot_strong_pixel_get) | **GET** /plot/strong_pixel | Generate strong pixels plot
[**preview_calibration_tiff_get**](DefaultApi.md#preview_calibration_tiff_get) | **GET** /preview/calibration.tiff | Get last preview image in TIFF format for calibration with PyFAI/Dioptas
[**preview_image_jpeg_get**](DefaultApi.md#preview_image_jpeg_get) | **GET** /preview/image.jpeg | Get last preview image in JPEG format using default settings
[**preview_image_jpeg_post**](DefaultApi.md#preview_image_jpeg_post) | **POST** /preview/image.jpeg | Get last preview image in JPEG format using custom settings
[**preview_image_tiff_get**](DefaultApi.md#preview_image_tiff_get) | **GET** /preview/image.tiff | Get last preview image in TIFF format
[**preview_pedestal_tiff_get**](DefaultApi.md#preview_pedestal_tiff_get) | **GET** /preview/pedestal.tiff | Get pedestal in TIFF format
[**start_post**](DefaultApi.md#start_post) | **POST** /start | Start detector
[**statistics_calibration_get**](DefaultApi.md#statistics_calibration_get) | **GET** /statistics/calibration | Get calibration statistics
[**statistics_data_collection_get**](DefaultApi.md#statistics_data_collection_get) | **GET** /statistics/data_collection | Get data collection statistics
[**statistics_get**](DefaultApi.md#statistics_get) | **GET** /statistics | Get general statistics
[**status_get**](DefaultApi.md#status_get) | **GET** /status | Get Jungfraujoch status
[**trigger_post**](DefaultApi.md#trigger_post) | **POST** /trigger | Send soft trigger to the detector
[**version_get**](DefaultApi.md#version_get) | **GET** /version | Get Jungfraujoch version of jfjoch_broker
[**wait_till_done_post**](DefaultApi.md#wait_till_done_post) | **POST** /wait_till_done | Wait for acquisition done
[**xfel_event_code_get**](DefaultApi.md#xfel_event_code_get) | **GET** /xfel/event_code | Return XFEL event codes for the current data acquisition
[**xfel_pulse_id_get**](DefaultApi.md#xfel_pulse_id_get) | **GET** /xfel/pulse_id | Return XFEL pulse IDs for the current data acquisition


# **cancel_post**
> cancel_post()

Cancel running data collection

Command will inform FPGA network card to stop pedestal or data collection at the current stage. Any frame that is currently being processed by CPU will be finished and sent to writer. Given the command is making sure to gracefully stop data acquisition and detector, it might take some time to switch back after command finished to `Idle` state.  If data collection is not running, the command has no effect. 

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Cancel running data collection
        api_instance.cancel_post()
    except Exception as e:
        print("Exception when calling DefaultApi->cancel_post: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Cancel request sent to FPGAs (or ignored, as data collection is not running) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_azim_int_get**
> AzimIntSettings config_azim_int_get()

Get radial integration configuration

Can be done anytime

### Example


```python
import jfjoch_client
from jfjoch_client.models.azim_int_settings import AzimIntSettings
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get radial integration configuration
        api_response = api_instance.config_azim_int_get()
        print("The response of DefaultApi->config_azim_int_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->config_azim_int_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**AzimIntSettings**](AzimIntSettings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_azim_int_put**
> config_azim_int_put(azim_int_settings=azim_int_settings)

Configure radial integration

Can be done when detector is Inactive or Idle

### Example


```python
import jfjoch_client
from jfjoch_client.models.azim_int_settings import AzimIntSettings
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    azim_int_settings = jfjoch_client.AzimIntSettings() # AzimIntSettings |  (optional)

    try:
        # Configure radial integration
        api_instance.config_azim_int_put(azim_int_settings=azim_int_settings)
    except Exception as e:
        print("Exception when calling DefaultApi->config_azim_int_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **azim_int_settings** | [**AzimIntSettings**](AzimIntSettings.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain, application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**400** | Input parsing or validation error |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_detector_get**
> DetectorSettings config_detector_get()

Get detector configuration

Can be done anytime

### Example


```python
import jfjoch_client
from jfjoch_client.models.detector_settings import DetectorSettings
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get detector configuration
        api_response = api_instance.config_detector_get()
        print("The response of DefaultApi->config_detector_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->config_detector_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**DetectorSettings**](DetectorSettings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_detector_put**
> config_detector_put(detector_settings=detector_settings)

Change detector configuration

Detector settings are ones that have effect on calibration, i.e., pedestal has to be collected again after changing these settings. This can only be done when detector is `Idle`, `Error` or `Inactive` states. If detector is in `Idle` state , pedestal procedure will be executed automatically - there must be no X-rays on the detector during the operation. If detector is in `Inactive` or `Error` states, new settings will be saved, but no calibration will be executed. 

### Example


```python
import jfjoch_client
from jfjoch_client.models.detector_settings import DetectorSettings
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    detector_settings = jfjoch_client.DetectorSettings() # DetectorSettings |  (optional)

    try:
        # Change detector configuration
        api_instance.config_detector_put(detector_settings=detector_settings)
    except Exception as e:
        print("Exception when calling DefaultApi->config_detector_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **detector_settings** | [**DetectorSettings**](DetectorSettings.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain, application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**400** | Input parsing or validation error |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_file_writer_get**
> FileWriterSettings config_file_writer_get()

Get file writer settings

Can be done anytime

### Example


```python
import jfjoch_client
from jfjoch_client.models.file_writer_settings import FileWriterSettings
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get file writer settings
        api_response = api_instance.config_file_writer_get()
        print("The response of DefaultApi->config_file_writer_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->config_file_writer_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**FileWriterSettings**](FileWriterSettings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_file_writer_put**
> config_file_writer_put(file_writer_settings=file_writer_settings)

Change file writer settings

This can only be done when detector is `Idle`, `Error` or `Inactive` states. 

### Example


```python
import jfjoch_client
from jfjoch_client.models.file_writer_settings import FileWriterSettings
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    file_writer_settings = jfjoch_client.FileWriterSettings() # FileWriterSettings |  (optional)

    try:
        # Change file writer settings
        api_instance.config_file_writer_put(file_writer_settings=file_writer_settings)
    except Exception as e:
        print("Exception when calling DefaultApi->config_file_writer_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **file_writer_settings** | [**FileWriterSettings**](FileWriterSettings.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain, application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**400** | Input parsing or validation error |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_image_format_conversion_post**
> config_image_format_conversion_post()

Configure format for data collection with full conversion

This can only be done when detector is `Idle`, `Error` or `Inactive` states. 

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Configure format for data collection with full conversion
        api_instance.config_image_format_conversion_post()
    except Exception as e:
        print("Exception when calling DefaultApi->config_image_format_conversion_post: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_image_format_get**
> ImageFormatSettings config_image_format_get()

Get image output format

Can be done anytime

### Example


```python
import jfjoch_client
from jfjoch_client.models.image_format_settings import ImageFormatSettings
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get image output format
        api_response = api_instance.config_image_format_get()
        print("The response of DefaultApi->config_image_format_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->config_image_format_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**ImageFormatSettings**](ImageFormatSettings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_image_format_put**
> config_image_format_put(image_format_settings=image_format_settings)

Change image output format

This can only be done when detector is `Idle`, `Error` or `Inactive` states. 

### Example


```python
import jfjoch_client
from jfjoch_client.models.image_format_settings import ImageFormatSettings
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    image_format_settings = jfjoch_client.ImageFormatSettings() # ImageFormatSettings |  (optional)

    try:
        # Change image output format
        api_instance.config_image_format_put(image_format_settings=image_format_settings)
    except Exception as e:
        print("Exception when calling DefaultApi->config_image_format_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **image_format_settings** | [**ImageFormatSettings**](ImageFormatSettings.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain, application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**400** | Input parsing or validation error |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_image_format_raw_post**
> config_image_format_raw_post()

Configure format for raw data collection

This can only be done when detector is `Idle`, `Error` or `Inactive` states. 

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Configure format for raw data collection
        api_instance.config_image_format_raw_post()
    except Exception as e:
        print("Exception when calling DefaultApi->config_image_format_raw_post: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_instrument_get**
> InstrumentMetadata config_instrument_get()

Get instrument metadata

Can be done anytime

### Example


```python
import jfjoch_client
from jfjoch_client.models.instrument_metadata import InstrumentMetadata
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get instrument metadata
        api_response = api_instance.config_instrument_get()
        print("The response of DefaultApi->config_instrument_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->config_instrument_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**InstrumentMetadata**](InstrumentMetadata.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_instrument_put**
> config_instrument_put(instrument_metadata=instrument_metadata)

Change instrument metadata

This can only be done when detector is `Idle`, `Error` or `Inactive` states. 

### Example


```python
import jfjoch_client
from jfjoch_client.models.instrument_metadata import InstrumentMetadata
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    instrument_metadata = jfjoch_client.InstrumentMetadata() # InstrumentMetadata |  (optional)

    try:
        # Change instrument metadata
        api_instance.config_instrument_put(instrument_metadata=instrument_metadata)
    except Exception as e:
        print("Exception when calling DefaultApi->config_instrument_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **instrument_metadata** | [**InstrumentMetadata**](InstrumentMetadata.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain, application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**400** | Input parsing or validation error |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_internal_generator_image_put**
> config_internal_generator_image_put(id=id, body=body)

Load binary image for internal FPGA generator

Load image for internal FPGA generator. This can only happen in Idle state of the detector. Requires binary blob with 16-bit integer numbers of size of detector in raw/converted coordinates  (depending on detector settings). 

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    id = 56 # int | Image id to upload (optional)
    body = None # bytearray |  (optional)

    try:
        # Load binary image for internal FPGA generator
        api_instance.config_internal_generator_image_put(id=id, body=body)
    except Exception as e:
        print("Exception when calling DefaultApi->config_internal_generator_image_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Image id to upload | [optional] 
 **body** | **bytearray**|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/octet-stream
 - **Accept**: text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**400** | Input parsing or validation error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_internal_generator_image_tiff_put**
> config_internal_generator_image_tiff_put(id=id, body=body)

Load TIFF image for internal FPGA generator

Load image for internal FPGA generator. This can only happen in Idle state of the detector. Requires TIFF with 16-bit integer numbers of size of detector in raw/converted coordinates  (depending on detector settings). 

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    id = 56 # int | Image ID to upload (optional)
    body = None # bytearray |  (optional)

    try:
        # Load TIFF image for internal FPGA generator
        api_instance.config_internal_generator_image_tiff_put(id=id, body=body)
    except Exception as e:
        print("Exception when calling DefaultApi->config_internal_generator_image_tiff_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **id** | **int**| Image ID to upload | [optional] 
 **body** | **bytearray**|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: image/tiff
 - **Accept**: text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**400** | Input parsing or validation error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_mask_get**
> bytearray config_mask_get()

Get mask of the detector (binary)

Get full pixel mask of the detector See NXmx standard for meaning of pixel values 

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get mask of the detector (binary)
        api_response = api_instance.config_mask_get()
        print("The response of DefaultApi->config_mask_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->config_mask_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

**bytearray**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Binary array (4 byte; unsigned) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_mask_tiff_get**
> bytearray config_mask_tiff_get()

Get mask of the detector (TIFF)

Get full pixel mask of the detector See NXmx standard for meaning of pixel values 

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get mask of the detector (TIFF)
        api_response = api_instance.config_mask_tiff_get()
        print("The response of DefaultApi->config_mask_tiff_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->config_mask_tiff_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

**bytearray**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/tiff

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Pixel mask in TIFF format (4 byte; unsigned) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_roi_get**
> RoiDefinitions config_roi_get()

Get ROI definitions

### Example


```python
import jfjoch_client
from jfjoch_client.models.roi_definitions import RoiDefinitions
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get ROI definitions
        api_response = api_instance.config_roi_get()
        print("The response of DefaultApi->config_roi_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->config_roi_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**RoiDefinitions**](RoiDefinitions.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_roi_put**
> config_roi_put(roi_definitions=roi_definitions)

Upload ROI definitions

### Example


```python
import jfjoch_client
from jfjoch_client.models.roi_definitions import RoiDefinitions
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    roi_definitions = jfjoch_client.RoiDefinitions() # RoiDefinitions |  (optional)

    try:
        # Upload ROI definitions
        api_instance.config_roi_put(roi_definitions=roi_definitions)
    except Exception as e:
        print("Exception when calling DefaultApi->config_roi_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **roi_definitions** | [**RoiDefinitions**](RoiDefinitions.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain, application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**400** | Input parsing or validation error |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_select_detector_get**
> DetectorList config_select_detector_get()

List available detectors

Configured detectors that can be selected by used

### Example


```python
import jfjoch_client
from jfjoch_client.models.detector_list import DetectorList
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # List available detectors
        api_response = api_instance.config_select_detector_get()
        print("The response of DefaultApi->config_select_detector_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->config_select_detector_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**DetectorList**](DetectorList.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_select_detector_put**
> config_select_detector_put(detector_selection=detector_selection)

Select detector

Jungfraujoch allows to control multiple detectors and/or region-of-interests. The command allows to choose one detector from the list (ID has to be consistent with one provided by GET response). Changing detector will set detector to `Inactive` state and will require reinitialization. 

### Example


```python
import jfjoch_client
from jfjoch_client.models.detector_selection import DetectorSelection
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    detector_selection = jfjoch_client.DetectorSelection() # DetectorSelection |  (optional)

    try:
        # Select detector
        api_instance.config_select_detector_put(detector_selection=detector_selection)
    except Exception as e:
        print("Exception when calling DefaultApi->config_select_detector_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **detector_selection** | [**DetectorSelection**](DetectorSelection.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain, application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**400** | Input parsing or validation error |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_spot_finding_get**
> SpotFindingSettings config_spot_finding_get()

Get data processing configuration

Can be done anytime

### Example


```python
import jfjoch_client
from jfjoch_client.models.spot_finding_settings import SpotFindingSettings
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get data processing configuration
        api_response = api_instance.config_spot_finding_get()
        print("The response of DefaultApi->config_spot_finding_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->config_spot_finding_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**SpotFindingSettings**](SpotFindingSettings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_spot_finding_put**
> config_spot_finding_put(spot_finding_settings=spot_finding_settings)

Configure spot finding

Can be done anytime, also while data collection is running

### Example


```python
import jfjoch_client
from jfjoch_client.models.spot_finding_settings import SpotFindingSettings
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    spot_finding_settings = jfjoch_client.SpotFindingSettings() # SpotFindingSettings |  (optional)

    try:
        # Configure spot finding
        api_instance.config_spot_finding_put(spot_finding_settings=spot_finding_settings)
    except Exception as e:
        print("Exception when calling DefaultApi->config_spot_finding_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **spot_finding_settings** | [**SpotFindingSettings**](SpotFindingSettings.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**400** | Input parsing or validation error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_user_mask_get**
> bytearray config_user_mask_get()

Get user mask of the detector (binary)

Get user pixel mask of the detector in the actual detector coordinates: 0 - good pixel, 1 - masked

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get user mask of the detector (binary)
        api_response = api_instance.config_user_mask_get()
        print("The response of DefaultApi->config_user_mask_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->config_user_mask_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

**bytearray**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/octet-stream

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | User mask in binary format (4 byte; unsigned) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_user_mask_put**
> config_user_mask_put(body=body)

Upload user mask of the detector (binary)

Should be in `Idle` state. Upload user mask of the detector - this is for example to account for beam stop shadow or misbehaving regions. If detector is conversion mode the mask can be both in raw (1024x512; stacked modules) or converted coordinates. In the latter case - module gaps are ignored and don't need to be assigned value. Mask is expected as binary array (4-byte; unsigned). 0 - good pixel, other value - masked User mask is stored in NXmx pixel mask (bit 8), as well as used in spot finding and azimuthal integration. 

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    body = None # bytearray |  (optional)

    try:
        # Upload user mask of the detector (binary)
        api_instance.config_user_mask_put(body=body)
    except Exception as e:
        print("Exception when calling DefaultApi->config_user_mask_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **bytearray**|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/octet-stream
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | All good |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_user_mask_tiff_get**
> bytearray config_user_mask_tiff_get()

Get user mask of the detector (TIFF)

Get user pixel mask of the detector in the actual detector coordinates: 0 - good pixel, 1 - masked

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get user mask of the detector (TIFF)
        api_response = api_instance.config_user_mask_tiff_get()
        print("The response of DefaultApi->config_user_mask_tiff_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->config_user_mask_tiff_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

**bytearray**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/tiff

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | User mask in TIFF format (4 byte; unsigned) |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_user_mask_tiff_put**
> config_user_mask_tiff_put(body=body)

Upload user mask of the detector

Should be in `Idle` state. Upload user mask of the detector - this is for example to account for beam stop shadow or misbehaving regions. If detector is conversion mode the mask can be both in raw (1024x512; stacked modules) or converted coordinates. In the latter case - module gaps are ignored and don't need to be assigned value. Mask is expected as TIFF (4-byte; unsigned). 0 - good pixel, other value - masked User mask is stored in NXmx pixel mask (bit 8), as well as used in spot finding and azimuthal integration.  User mask is not automatically applied - i.e. pixels with user mask will have a valid pixel value in the images. 

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    body = None # bytearray |  (optional)

    try:
        # Upload user mask of the detector
        api_instance.config_user_mask_tiff_put(body=body)
    except Exception as e:
        print("Exception when calling DefaultApi->config_user_mask_tiff_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **body** | **bytearray**|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: image/tiff
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | All good |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_zeromq_metadata_get**
> ZeromqMetadataSettings config_zeromq_metadata_get()

Get ZeroMQ metadata socket settings

### Example


```python
import jfjoch_client
from jfjoch_client.models.zeromq_metadata_settings import ZeromqMetadataSettings
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get ZeroMQ metadata socket settings
        api_response = api_instance.config_zeromq_metadata_get()
        print("The response of DefaultApi->config_zeromq_metadata_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->config_zeromq_metadata_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**ZeromqMetadataSettings**](ZeromqMetadataSettings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_zeromq_metadata_put**
> config_zeromq_metadata_put(zeromq_metadata_settings=zeromq_metadata_settings)

Set ZeroMQ metadata settings

Jungfraujoch can generate metadata message stream on ZeroMQ PUB socket. This stream covers all images. Here settings of the socket can be adjusted. While the data structure contains also socket_address, this cannot be changed via HTTP and is ignore in PUT request. 

### Example


```python
import jfjoch_client
from jfjoch_client.models.zeromq_metadata_settings import ZeromqMetadataSettings
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    zeromq_metadata_settings = jfjoch_client.ZeromqMetadataSettings() # ZeromqMetadataSettings |  (optional)

    try:
        # Set ZeroMQ metadata settings
        api_instance.config_zeromq_metadata_put(zeromq_metadata_settings=zeromq_metadata_settings)
    except Exception as e:
        print("Exception when calling DefaultApi->config_zeromq_metadata_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **zeromq_metadata_settings** | [**ZeromqMetadataSettings**](ZeromqMetadataSettings.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain, application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**400** | Input parsing or validation error |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_zeromq_preview_get**
> ZeromqPreviewSettings config_zeromq_preview_get()

Get ZeroMQ preview settings

### Example


```python
import jfjoch_client
from jfjoch_client.models.zeromq_preview_settings import ZeromqPreviewSettings
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get ZeroMQ preview settings
        api_response = api_instance.config_zeromq_preview_get()
        print("The response of DefaultApi->config_zeromq_preview_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->config_zeromq_preview_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**ZeromqPreviewSettings**](ZeromqPreviewSettings.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **config_zeromq_preview_put**
> config_zeromq_preview_put(zeromq_preview_settings=zeromq_preview_settings)

Set ZeroMQ preview settings

Jungfraujoch can generate preview message stream on ZeroMQ SUB socket. Here settings of the socket can be adjusted. While the data structure contains also socket_address, this cannot be changed via HTTP and is ignore in PUT request. Options set with this PUT request have no effect on HTTP based preview. 

### Example


```python
import jfjoch_client
from jfjoch_client.models.zeromq_preview_settings import ZeromqPreviewSettings
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    zeromq_preview_settings = jfjoch_client.ZeromqPreviewSettings() # ZeromqPreviewSettings |  (optional)

    try:
        # Set ZeroMQ preview settings
        api_instance.config_zeromq_preview_put(zeromq_preview_settings=zeromq_preview_settings)
    except Exception as e:
        print("Exception when calling DefaultApi->config_zeromq_preview_put: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **zeromq_preview_settings** | [**ZeromqPreviewSettings**](ZeromqPreviewSettings.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain, application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**400** | Input parsing or validation error |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **deactivate_post**
> deactivate_post()

Prepare detector to turn off

Should be in `Idle` or `Error` state. Command deactivates data acquisition and turns off detector high voltage and ASIC. Should be used always before turning off power from the detector. 

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Prepare detector to turn off
        api_instance.deactivate_post()
    except Exception as e:
        print("Exception when calling DefaultApi->deactivate_post: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Detector ready to turn off |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **detector_status_get**
> DetectorStatus detector_status_get()

Get detector status

Status of the JUNGFRAU detector

### Example


```python
import jfjoch_client
from jfjoch_client.models.detector_status import DetectorStatus
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get detector status
        api_response = api_instance.detector_status_get()
        print("The response of DefaultApi->detector_status_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->detector_status_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**DetectorStatus**](DetectorStatus.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**404** | Running in \&quot;simulator\&quot; mode - no detector present |  -  |
**500** | Error encountered when trying to read status |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **fpga_status_get**
> List[FpgaStatusInner] fpga_status_get()

Get status of FPGA devices

### Example


```python
import jfjoch_client
from jfjoch_client.models.fpga_status_inner import FpgaStatusInner
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get status of FPGA devices
        api_response = api_instance.fpga_status_get()
        print("The response of DefaultApi->fpga_status_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->fpga_status_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**List[FpgaStatusInner]**](FpgaStatusInner.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **image_buffer_clear_post**
> image_buffer_clear_post()

Clear image buffer

Turns off image buffer for the last data collection. Can be only run when Jungfraujoch is not collecting data.

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Clear image buffer
        api_instance.image_buffer_clear_post()
    except Exception as e:
        print("Exception when calling DefaultApi->image_buffer_clear_post: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Done |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **image_buffer_image_cbor_get**
> bytearray image_buffer_image_cbor_get(image_number=image_number)

Get image message in CBOR format

Contains full image data and metadata. The image must come from the latest data collection.

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    image_number = 56 # int | Image number. If omitted, the image with the highest number in the image buffer will be provided. (optional)

    try:
        # Get image message in CBOR format
        api_response = api_instance.image_buffer_image_cbor_get(image_number=image_number)
        print("The response of DefaultApi->image_buffer_image_cbor_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->image_buffer_image_cbor_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **image_number** | **int**| Image number. If omitted, the image with the highest number in the image buffer will be provided. | [optional] 

### Return type

**bytearray**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/cbor, application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Return image message |  -  |
**404** | Image not present in the buffer - either not yet measured or already replaced by a next image. |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **image_buffer_start_cbor_get**
> bytearray image_buffer_start_cbor_get()

Get Start message in CBOR format

Contains metadata for a dataset (e.g., experimental geometry)

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get Start message in CBOR format
        api_response = api_instance.image_buffer_start_cbor_get()
        print("The response of DefaultApi->image_buffer_start_cbor_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->image_buffer_start_cbor_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

**bytearray**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/cbor, application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Return start message |  -  |
**404** | No measurement so far |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **image_buffer_status_get**
> ImageBufferStatus image_buffer_status_get()

Get status of the image buffers

Can be run at any stage of Jungfraujoch operation, including during data collection. The status of the image buffer is volatile during data collection - if data collection goes for more images than available buffer slots, then image might be replaced in the buffer between calling /images and /image.cbor. 

### Example


```python
import jfjoch_client
from jfjoch_client.models.image_buffer_status import ImageBufferStatus
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get status of the image buffers
        api_response = api_instance.image_buffer_status_get()
        print("The response of DefaultApi->image_buffer_status_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->image_buffer_status_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**ImageBufferStatus**](ImageBufferStatus.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Done |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **initialize_post**
> initialize_post()

Initialize detector and data acquisition

Should be used in two cases:   - Detector is in `Inactive` state   - Detector is in `Error` state X-ray shutter must be closed.  This operation will reconfigure network interface of the detector.  During operation of the detector it is recommended to use the `POST /pedestal` operation instead. If storage cells are used, the execution time might be few minutes.  This is async function - one needs to use `POST /wait_till_done` to ensure operation is done. 

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Initialize detector and data acquisition
        api_instance.initialize_post()
    except Exception as e:
        print("Exception when calling DefaultApi->initialize_post: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Initialization started |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **pedestal_post**
> pedestal_post()

Collect dark current for the detector

Updates calibration of the JUNGFRAU detector. Must be in `Idle` state.  X-ray shutter must be closed. Recommended to run once per hour for long integration times (> 100 us).  This is async function - one needs to use `POST /wait_till_done` to ensure operation is done. 

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Collect dark current for the detector
        api_instance.pedestal_post()
    except Exception as e:
        print("Exception when calling DefaultApi->pedestal_post: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plot_azim_int_get**
> Plots plot_azim_int_get(compression=compression)

Generate radial integration profile

Generate average radial integration profile

### Example


```python
import jfjoch_client
from jfjoch_client.models.plots import Plots
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    compression = True # bool | Enable DEFLATE compression of output data. (optional) (default to True)

    try:
        # Generate radial integration profile
        api_response = api_instance.plot_azim_int_get(compression=compression)
        print("The response of DefaultApi->plot_azim_int_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->plot_azim_int_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **compression** | **bool**| Enable DEFLATE compression of output data. | [optional] [default to True]

### Return type

[**Plots**](Plots.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plot_bkg_estimate_get**
> Plots plot_bkg_estimate_get(binning=binning, compression=compression)

Generate background estimate plot

Mean intensity for d = 3 - 5 A per image; binning is configurable 

### Example


```python
import jfjoch_client
from jfjoch_client.models.plots import Plots
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    binning = 56 # int | Binning of frames for the plot (0 = default binning) (optional)
    compression = True # bool | Enable DEFLATE compression of output data. (optional) (default to True)

    try:
        # Generate background estimate plot
        api_response = api_instance.plot_bkg_estimate_get(binning=binning, compression=compression)
        print("The response of DefaultApi->plot_bkg_estimate_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->plot_bkg_estimate_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **binning** | **int**| Binning of frames for the plot (0 &#x3D; default binning) | [optional] 
 **compression** | **bool**| Enable DEFLATE compression of output data. | [optional] [default to True]

### Return type

[**Plots**](Plots.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK. Response will be by default compressed with deflate algorithm, if using curl, use --compressed option. |  -  |
**400** | Input parsing or validation error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plot_error_pixel_get**
> Plots plot_error_pixel_get(binning=binning, compression=compression)

Generate error pixels plot

Count of error (mean) and saturated (mean/max) pixels per image; binning is configurable

### Example


```python
import jfjoch_client
from jfjoch_client.models.plots import Plots
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    binning = 56 # int | Binning of frames for the plot (0 = default binning) (optional)
    compression = True # bool | Enable DEFLATE compression of output data. (optional) (default to True)

    try:
        # Generate error pixels plot
        api_response = api_instance.plot_error_pixel_get(binning=binning, compression=compression)
        print("The response of DefaultApi->plot_error_pixel_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->plot_error_pixel_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **binning** | **int**| Binning of frames for the plot (0 &#x3D; default binning) | [optional] 
 **compression** | **bool**| Enable DEFLATE compression of output data. | [optional] [default to True]

### Return type

[**Plots**](Plots.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Response will be by default compressed with deflate algorithm, if using curl, use --compressed option. |  -  |
**400** | Input parsing or validation error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plot_image_collection_efficiency_get**
> Plots plot_image_collection_efficiency_get(binning=binning, compression=compression)

Generate image collection efficiency plot

Ratio of collected and expected packets per image; binning is configurable

### Example


```python
import jfjoch_client
from jfjoch_client.models.plots import Plots
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    binning = 56 # int | Binning of frames for the plot (0 = default binning) (optional)
    compression = True # bool | Enable DEFLATE compression of output data. (optional) (default to True)

    try:
        # Generate image collection efficiency plot
        api_response = api_instance.plot_image_collection_efficiency_get(binning=binning, compression=compression)
        print("The response of DefaultApi->plot_image_collection_efficiency_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->plot_image_collection_efficiency_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **binning** | **int**| Binning of frames for the plot (0 &#x3D; default binning) | [optional] 
 **compression** | **bool**| Enable DEFLATE compression of output data. | [optional] [default to True]

### Return type

[**Plots**](Plots.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**400** | Input parsing or validation error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plot_indexing_rate_get**
> Plots plot_indexing_rate_get(binning=binning, compression=compression)

Generate indexing rate plot

Image indexing rate; binning is configurable

### Example


```python
import jfjoch_client
from jfjoch_client.models.plots import Plots
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    binning = 56 # int | Binning of frames for the plot (0 = default binning) (optional)
    compression = True # bool | Enable DEFLATE compression of output data. (optional) (default to True)

    try:
        # Generate indexing rate plot
        api_response = api_instance.plot_indexing_rate_get(binning=binning, compression=compression)
        print("The response of DefaultApi->plot_indexing_rate_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->plot_indexing_rate_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **binning** | **int**| Binning of frames for the plot (0 &#x3D; default binning) | [optional] 
 **compression** | **bool**| Enable DEFLATE compression of output data. | [optional] [default to True]

### Return type

[**Plots**](Plots.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Response will be by default compressed with deflate algorithm, if using curl, use --compressed option. |  -  |
**400** | Input parsing or validation error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plot_indexing_unit_cell_angle_get**
> Plots plot_indexing_unit_cell_angle_get(binning=binning, compression=compression)

Generate indexing unit cell angle plot

Crystal unit cell based on indexing results; binning is configurable

### Example


```python
import jfjoch_client
from jfjoch_client.models.plots import Plots
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    binning = 56 # int | Binning of frames for the plot (0 = default binning) (optional)
    compression = True # bool | Enable DEFLATE compression of output data. (optional) (default to True)

    try:
        # Generate indexing unit cell angle plot
        api_response = api_instance.plot_indexing_unit_cell_angle_get(binning=binning, compression=compression)
        print("The response of DefaultApi->plot_indexing_unit_cell_angle_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->plot_indexing_unit_cell_angle_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **binning** | **int**| Binning of frames for the plot (0 &#x3D; default binning) | [optional] 
 **compression** | **bool**| Enable DEFLATE compression of output data. | [optional] [default to True]

### Return type

[**Plots**](Plots.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Response will be by default compressed with deflate algorithm, if using curl, use --compressed option. |  -  |
**400** | Input parsing or validation error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plot_indexing_unit_cell_get**
> Plots plot_indexing_unit_cell_get(binning=binning, compression=compression)

Generate indexing unit cell length plots

Crystal unit cell based on indexing results; binning is configurable

### Example


```python
import jfjoch_client
from jfjoch_client.models.plots import Plots
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    binning = 56 # int | Binning of frames for the plot (0 = default binning) (optional)
    compression = True # bool | Enable DEFLATE compression of output data. (optional) (default to True)

    try:
        # Generate indexing unit cell length plots
        api_response = api_instance.plot_indexing_unit_cell_get(binning=binning, compression=compression)
        print("The response of DefaultApi->plot_indexing_unit_cell_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->plot_indexing_unit_cell_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **binning** | **int**| Binning of frames for the plot (0 &#x3D; default binning) | [optional] 
 **compression** | **bool**| Enable DEFLATE compression of output data. | [optional] [default to True]

### Return type

[**Plots**](Plots.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Response will be by default compressed with deflate algorithm, if using curl, use --compressed option. |  -  |
**400** | Input parsing or validation error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plot_max_value_get**
> Plots plot_max_value_get(binning=binning, compression=compression)

Generate maximum pixel value plot

Provides maximum viable pixel value (excluding overloads and error pixels); binning is configurable and maximum of a bin is returned

### Example


```python
import jfjoch_client
from jfjoch_client.models.plots import Plots
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    binning = 56 # int | Binning of frames for the plot (0 = default binning) (optional)
    compression = True # bool | Enable DEFLATE compression of output data. (optional) (default to True)

    try:
        # Generate maximum pixel value plot
        api_response = api_instance.plot_max_value_get(binning=binning, compression=compression)
        print("The response of DefaultApi->plot_max_value_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->plot_max_value_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **binning** | **int**| Binning of frames for the plot (0 &#x3D; default binning) | [optional] 
 **compression** | **bool**| Enable DEFLATE compression of output data. | [optional] [default to True]

### Return type

[**Plots**](Plots.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Response will be by default compressed with deflate algorithm, if using curl, use --compressed option. |  -  |
**400** | Input parsing or validation error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plot_packets_received_get**
> Plots plot_packets_received_get(binning=binning, compression=compression)

Generate plot with number of received packets per image

Number of collected packets per image; binning is configurable

### Example


```python
import jfjoch_client
from jfjoch_client.models.plots import Plots
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    binning = 56 # int | Binning of frames for the plot (0 = default binning) (optional)
    compression = True # bool | Enable DEFLATE compression of output data. (optional) (default to True)

    try:
        # Generate plot with number of received packets per image
        api_response = api_instance.plot_packets_received_get(binning=binning, compression=compression)
        print("The response of DefaultApi->plot_packets_received_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->plot_packets_received_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **binning** | **int**| Binning of frames for the plot (0 &#x3D; default binning) | [optional] 
 **compression** | **bool**| Enable DEFLATE compression of output data. | [optional] [default to True]

### Return type

[**Plots**](Plots.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**400** | Input parsing or validation error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plot_receiver_delay_get**
> Plots plot_receiver_delay_get(binning=binning, compression=compression)

Generate receiver delay plot

Amount of frames the receiver is behind the FPGA for each image - used for internal debugging; binning is configurable

### Example


```python
import jfjoch_client
from jfjoch_client.models.plots import Plots
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    binning = 56 # int | Binning of frames for the plot (0 = default binning) (optional)
    compression = True # bool | Enable DEFLATE compression of output data. (optional) (default to True)

    try:
        # Generate receiver delay plot
        api_response = api_instance.plot_receiver_delay_get(binning=binning, compression=compression)
        print("The response of DefaultApi->plot_receiver_delay_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->plot_receiver_delay_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **binning** | **int**| Binning of frames for the plot (0 &#x3D; default binning) | [optional] 
 **compression** | **bool**| Enable DEFLATE compression of output data. | [optional] [default to True]

### Return type

[**Plots**](Plots.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**400** | Input parsing or validation error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plot_receiver_free_send_buffers_get**
> Plots plot_receiver_free_send_buffers_get(binning=binning, compression=compression)

Generate receiver free send buffer plot

Amount of send buffers available during frame processing - used for internal debugging; binning is configurable

### Example


```python
import jfjoch_client
from jfjoch_client.models.plots import Plots
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    binning = 56 # int | Binning of frames for the plot (0 = default binning) (optional)
    compression = True # bool | Enable DEFLATE compression of output data. (optional) (default to True)

    try:
        # Generate receiver free send buffer plot
        api_response = api_instance.plot_receiver_free_send_buffers_get(binning=binning, compression=compression)
        print("The response of DefaultApi->plot_receiver_free_send_buffers_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->plot_receiver_free_send_buffers_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **binning** | **int**| Binning of frames for the plot (0 &#x3D; default binning) | [optional] 
 **compression** | **bool**| Enable DEFLATE compression of output data. | [optional] [default to True]

### Return type

[**Plots**](Plots.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**400** | Input parsing or validation error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plot_roi_max_count_get**
> Plots plot_roi_max_count_get(binning=binning, compression=compression)

Generate plot of ROI max count

Max count of ROI per image; binning is configurable

### Example


```python
import jfjoch_client
from jfjoch_client.models.plots import Plots
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    binning = 56 # int | Binning of frames for the plot (0 = default binning) (optional)
    compression = True # bool | Enable DEFLATE compression of output data. (optional) (default to True)

    try:
        # Generate plot of ROI max count
        api_response = api_instance.plot_roi_max_count_get(binning=binning, compression=compression)
        print("The response of DefaultApi->plot_roi_max_count_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->plot_roi_max_count_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **binning** | **int**| Binning of frames for the plot (0 &#x3D; default binning) | [optional] 
 **compression** | **bool**| Enable DEFLATE compression of output data. | [optional] [default to True]

### Return type

[**Plots**](Plots.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Response will be by default compressed with deflate algorithm, if using curl, use --compressed option. |  -  |
**400** | Input parsing or validation error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plot_roi_sum_get**
> Plots plot_roi_sum_get(binning=binning, compression=compression)

Generate ROI sum plot

Sum of ROI rectangle per image; binning is configurable

### Example


```python
import jfjoch_client
from jfjoch_client.models.plots import Plots
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    binning = 56 # int | Binning of frames for the plot (0 = default binning) (optional)
    compression = True # bool | Enable DEFLATE compression of output data. (optional) (default to True)

    try:
        # Generate ROI sum plot
        api_response = api_instance.plot_roi_sum_get(binning=binning, compression=compression)
        print("The response of DefaultApi->plot_roi_sum_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->plot_roi_sum_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **binning** | **int**| Binning of frames for the plot (0 &#x3D; default binning) | [optional] 
 **compression** | **bool**| Enable DEFLATE compression of output data. | [optional] [default to True]

### Return type

[**Plots**](Plots.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Response will be by default compressed with deflate algorithm, if using curl, use --compressed option. |  -  |
**400** | Input parsing or validation error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plot_roi_valid_pixels_get**
> Plots plot_roi_valid_pixels_get(binning=binning, compression=compression)

Generate plot of ROI valid pixels

Number of pixels within a ROI area; pixels with special values (overload, bad pixel) are excluded; multipixels are counted just once; binning is configurable

### Example


```python
import jfjoch_client
from jfjoch_client.models.plots import Plots
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    binning = 56 # int | Binning of frames for the plot (0 = default binning) (optional)
    compression = True # bool | Enable DEFLATE compression of output data. (optional) (default to True)

    try:
        # Generate plot of ROI valid pixels
        api_response = api_instance.plot_roi_valid_pixels_get(binning=binning, compression=compression)
        print("The response of DefaultApi->plot_roi_valid_pixels_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->plot_roi_valid_pixels_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **binning** | **int**| Binning of frames for the plot (0 &#x3D; default binning) | [optional] 
 **compression** | **bool**| Enable DEFLATE compression of output data. | [optional] [default to True]

### Return type

[**Plots**](Plots.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Response will be by default compressed with deflate algorithm, if using curl, use --compressed option. |  -  |
**400** | Input parsing or validation error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plot_spot_count_get**
> Plots plot_spot_count_get(binning=binning, compression=compression)

Generate spot count plot

Number of spots per image; binning is configurable

### Example


```python
import jfjoch_client
from jfjoch_client.models.plots import Plots
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    binning = 56 # int | Binning of frames for the plot (0 = default binning) (optional)
    compression = True # bool | Enable DEFLATE compression of output data. (optional) (default to True)

    try:
        # Generate spot count plot
        api_response = api_instance.plot_spot_count_get(binning=binning, compression=compression)
        print("The response of DefaultApi->plot_spot_count_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->plot_spot_count_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **binning** | **int**| Binning of frames for the plot (0 &#x3D; default binning) | [optional] 
 **compression** | **bool**| Enable DEFLATE compression of output data. | [optional] [default to True]

### Return type

[**Plots**](Plots.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Response will be by default compressed with deflate algorithm, if using curl, use --compressed option. |  -  |
**400** | Input parsing or validation error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **plot_strong_pixel_get**
> Plots plot_strong_pixel_get(binning=binning, compression=compression)

Generate strong pixels plot

Count of strong pixels per image (from spot finding); binning is configurable

### Example


```python
import jfjoch_client
from jfjoch_client.models.plots import Plots
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    binning = 56 # int | Binning of frames for the plot (0 = default binning) (optional)
    compression = True # bool | Enable DEFLATE compression of output data. (optional) (default to True)

    try:
        # Generate strong pixels plot
        api_response = api_instance.plot_strong_pixel_get(binning=binning, compression=compression)
        print("The response of DefaultApi->plot_strong_pixel_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->plot_strong_pixel_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **binning** | **int**| Binning of frames for the plot (0 &#x3D; default binning) | [optional] 
 **compression** | **bool**| Enable DEFLATE compression of output data. | [optional] [default to True]

### Return type

[**Plots**](Plots.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json, text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Response will be by default compressed with deflate algorithm, if using curl, use --compressed option. |  -  |
**400** | Input parsing or validation error |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **preview_calibration_tiff_get**
> bytearray preview_calibration_tiff_get()

Get last preview image in TIFF format for calibration with PyFAI/Dioptas

Image is reduced to unsigned 16-bit images, all bad pixels are set to 65535 and image is mirrored in vertical direction

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get last preview image in TIFF format for calibration with PyFAI/Dioptas
        api_response = api_instance.preview_calibration_tiff_get()
        print("The response of DefaultApi->preview_calibration_tiff_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->preview_calibration_tiff_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

**bytearray**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/tiff

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Preview image |  -  |
**404** | No preview image recorded so far |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **preview_image_jpeg_get**
> bytearray preview_image_jpeg_get()

Get last preview image in JPEG format using default settings

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get last preview image in JPEG format using default settings
        api_response = api_instance.preview_image_jpeg_get()
        print("The response of DefaultApi->preview_image_jpeg_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->preview_image_jpeg_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

**bytearray**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/jpeg

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Preview image |  -  |
**404** | No preview image recorded so far |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **preview_image_jpeg_post**
> bytearray preview_image_jpeg_post(preview_settings=preview_settings)

Get last preview image in JPEG format using custom settings

### Example


```python
import jfjoch_client
from jfjoch_client.models.preview_settings import PreviewSettings
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    preview_settings = jfjoch_client.PreviewSettings() # PreviewSettings |  (optional)

    try:
        # Get last preview image in JPEG format using custom settings
        api_response = api_instance.preview_image_jpeg_post(preview_settings=preview_settings)
        print("The response of DefaultApi->preview_image_jpeg_post:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->preview_image_jpeg_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **preview_settings** | [**PreviewSettings**](PreviewSettings.md)|  | [optional] 

### Return type

**bytearray**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: image/jpeg, text/plain, application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Preview image |  -  |
**404** | No preview image recorded so far |  -  |
**400** | Input parsing or validation error |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **preview_image_tiff_get**
> bytearray preview_image_tiff_get()

Get last preview image in TIFF format

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get last preview image in TIFF format
        api_response = api_instance.preview_image_tiff_get()
        print("The response of DefaultApi->preview_image_tiff_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->preview_image_tiff_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

**bytearray**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/tiff

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Preview image |  -  |
**404** | No preview image recorded so far |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **preview_pedestal_tiff_get**
> bytearray preview_pedestal_tiff_get(gain_level, sc=sc)

Get pedestal in TIFF format

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    gain_level = 56 # int | Gain level (0, 1, 2)
    sc = 56 # int | Storage cell number (optional)

    try:
        # Get pedestal in TIFF format
        api_response = api_instance.preview_pedestal_tiff_get(gain_level, sc=sc)
        print("The response of DefaultApi->preview_pedestal_tiff_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->preview_pedestal_tiff_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **gain_level** | **int**| Gain level (0, 1, 2) | 
 **sc** | **int**| Storage cell number | [optional] 

### Return type

**bytearray**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: image/tiff

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Calibration image |  -  |
**404** | No calibration recorded so far |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **start_post**
> start_post(dataset_settings=dataset_settings)

Start detector

Start data acquisition. Detector must be in `Idle` state. Doesn't run calibration procedure. When the function returns, detector is ready to accept soft/TTL triggers. 

### Example


```python
import jfjoch_client
from jfjoch_client.models.dataset_settings import DatasetSettings
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    dataset_settings = jfjoch_client.DatasetSettings() # DatasetSettings |  (optional)

    try:
        # Start detector
        api_instance.start_post(dataset_settings=dataset_settings)
    except Exception as e:
        print("Exception when calling DefaultApi->start_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **dataset_settings** | [**DatasetSettings**](DatasetSettings.md)|  | [optional] 

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: text/plain, application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**400** | Input parsing or validation error |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **statistics_calibration_get**
> List[CalibrationStatisticsInner] statistics_calibration_get()

Get calibration statistics

Statistics are provided for each module/storage cell separately

### Example


```python
import jfjoch_client
from jfjoch_client.models.calibration_statistics_inner import CalibrationStatisticsInner
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get calibration statistics
        api_response = api_instance.statistics_calibration_get()
        print("The response of DefaultApi->statistics_calibration_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->statistics_calibration_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**List[CalibrationStatisticsInner]**](CalibrationStatisticsInner.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **statistics_data_collection_get**
> MeasurementStatistics statistics_data_collection_get()

Get data collection statistics

Results of the last data collection

### Example


```python
import jfjoch_client
from jfjoch_client.models.measurement_statistics import MeasurementStatistics
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get data collection statistics
        api_response = api_instance.statistics_data_collection_get()
        print("The response of DefaultApi->statistics_data_collection_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->statistics_data_collection_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**MeasurementStatistics**](MeasurementStatistics.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |
**404** | No data collection performed so far |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **statistics_get**
> JfjochStatistics statistics_get(compression=compression)

Get general statistics

### Example


```python
import jfjoch_client
from jfjoch_client.models.jfjoch_statistics import JfjochStatistics
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    compression = True # bool | Enable DEFLATE compression of output data. (optional) (default to True)

    try:
        # Get general statistics
        api_response = api_instance.statistics_get(compression=compression)
        print("The response of DefaultApi->statistics_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->statistics_get: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **compression** | **bool**| Enable DEFLATE compression of output data. | [optional] [default to True]

### Return type

[**JfjochStatistics**](JfjochStatistics.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **status_get**
> BrokerStatus status_get()

Get Jungfraujoch status

Status of the data acquisition

### Example


```python
import jfjoch_client
from jfjoch_client.models.broker_status import BrokerStatus
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get Jungfraujoch status
        api_response = api_instance.status_get()
        print("The response of DefaultApi->status_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->status_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

[**BrokerStatus**](BrokerStatus.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Everything OK |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **trigger_post**
> trigger_post()

Send soft trigger to the detector

Generate soft trigger

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Send soft trigger to the detector
        api_instance.trigger_post()
    except Exception as e:
        print("Exception when calling DefaultApi->trigger_post: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Trigger sent |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **version_get**
> str version_get()

Get Jungfraujoch version of jfjoch_broker

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Get Jungfraujoch version of jfjoch_broker
        api_response = api_instance.version_get()
        print("The response of DefaultApi->version_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->version_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

**str**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: text/plain

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Release number of Jungfraujoch |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **wait_till_done_post**
> wait_till_done_post(timeout=timeout)

Wait for acquisition done

Block execution of external script till initialization, data collection or pedestal is finished. Running this command does not affect (cancel) running data collection, it is only to ensure synchronous execution of other software.  To not block web server for a indefinite period of time, the procedure is provided with a timeout. Extending timeout is possible, but requires to ensure safety that client will not close the connection and retry the connection. 

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)
    timeout = 60 # int | Timeout in seconds (0 == immediate response) (optional) (default to 60)

    try:
        # Wait for acquisition done
        api_instance.wait_till_done_post(timeout=timeout)
    except Exception as e:
        print("Exception when calling DefaultApi->wait_till_done_post: %s\n" % e)
```



### Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **timeout** | **int**| Timeout in seconds (0 &#x3D;&#x3D; immediate response) | [optional] [default to 60]

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Detector in &#x60;Idle&#x60; state, another data collection can start immediately |  -  |
**400** | Timeout parameter out of bounds |  -  |
**500** | Error within Jungfraujoch code - see output message. |  -  |
**502** | Detector is inactive mode |  -  |
**504** | Timeout reached, need to restart operation |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **xfel_event_code_get**
> List[int] xfel_event_code_get()

Return XFEL event codes for the current data acquisition

Return array of XFEL event codes

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Return XFEL event codes for the current data acquisition
        api_response = api_instance.xfel_event_code_get()
        print("The response of DefaultApi->xfel_event_code_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->xfel_event_code_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

**List[int]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Event codes collected |  -  |
**404** | Not in XFEL mode or no acquisition recorded |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **xfel_pulse_id_get**
> List[int] xfel_pulse_id_get()

Return XFEL pulse IDs for the current data acquisition

Return array of XFEL pulse IDs - (-1) if image not recorded

### Example


```python
import jfjoch_client
from jfjoch_client.rest import ApiException
from pprint import pprint

# Defining the host is optional and defaults to http://localhost:5232
# See configuration.py for a list of all supported configuration parameters.
configuration = jfjoch_client.Configuration(
    host = "http://localhost:5232"
)


# Enter a context with an instance of the API client
with jfjoch_client.ApiClient(configuration) as api_client:
    # Create an instance of the API class
    api_instance = jfjoch_client.DefaultApi(api_client)

    try:
        # Return XFEL pulse IDs for the current data acquisition
        api_response = api_instance.xfel_pulse_id_get()
        print("The response of DefaultApi->xfel_pulse_id_get:\n")
        pprint(api_response)
    except Exception as e:
        print("Exception when calling DefaultApi->xfel_pulse_id_get: %s\n" % e)
```



### Parameters

This endpoint does not need any parameter.

### Return type

**List[int]**

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

### HTTP response details

| Status code | Description | Response headers |
|-------------|-------------|------------------|
**200** | Pulse ID collected |  -  |
**404** | Not in XFEL mode or no acquisition recorded |  -  |

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

