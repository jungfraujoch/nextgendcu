# SpotFindingSettings


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**enable** | **bool** | Enable spot finding. This is temporary setting, i.e. can be changed anytime during data collection.  Even if disabled spot finding information will still be send and written, though always with zero spots.  | [default to True]
**indexing** | **bool** | Enable indexing. This is temporary setting, i.e. can be changed anytime during data collection.  | [default to True]
**filter_powder_rings** | **bool** | Filter spots which form powder rings (e.g., ice rings) | [optional] [default to False]
**min_spot_count_powder_ring** | **int** | Minimum number of spots to consider a thin resolution shell (0.01 A^-1) a powder ring and filter out. | [optional] 
**signal_to_noise_threshold** | **float** |  | 
**photon_count_threshold** | **int** |  | 
**min_pix_per_spot** | **int** |  | 
**max_pix_per_spot** | **int** |  | 
**high_resolution_limit** | **float** |  | 
**low_resolution_limit** | **float** |  | 
**indexing_tolerance** | **float** | Acceptance tolerance for spots after the indexing run - the larger the number, the more spots will be accepted | 

## Example

```python
from jfjoch_client.models.spot_finding_settings import SpotFindingSettings

# TODO update the JSON string below
json = "{}"
# create an instance of SpotFindingSettings from a JSON string
spot_finding_settings_instance = SpotFindingSettings.from_json(json)
# print the JSON string representation of the object
print(SpotFindingSettings.to_json())

# convert the object into a dict
spot_finding_settings_dict = spot_finding_settings_instance.to_dict()
# create an instance of SpotFindingSettings from a dict
spot_finding_settings_from_dict = SpotFindingSettings.from_dict(spot_finding_settings_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


