# MeasurementStatistics


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file_prefix** | **str** |  | [optional] 
**run_number** | **int** | Number of data collection run. This can be either automatically incremented or provided externally for each data collection.  | [optional] 
**experiment_group** | **str** | Name of group owning the data (e.g. p-group or proposal number).  | [optional] 
**images_expected** | **int** |  | [optional] 
**images_collected** | **int** | Images collected by the receiver. This number will be lower than images expected if there were issues with data collection performance.  | [optional] 
**images_sent** | **int** | Images sent to the writer.  The value does not include images discarded by lossy compression filter and images not forwarded due to full ZeroMQ queue.  | [optional] 
**images_discarded_lossy_compression** | **int** | Images discarded by the lossy compression filter | [optional] 
**max_image_number_sent** | **int** |  | [optional] 
**collection_efficiency** | **float** |  | [optional] 
**compression_ratio** | **float** |  | [optional] 
**cancelled** | **bool** |  | [optional] 
**max_receiver_delay** | **int** |  | [optional] 
**indexing_rate** | **float** |  | [optional] 
**detector_width** | **int** |  | [optional] 
**detector_height** | **int** |  | [optional] 
**detector_pixel_depth** | **int** |  | [optional] 
**bkg_estimate** | **float** |  | [optional] 
**unit_cell** | **str** |  | [optional] 
**error_pixels** | **float** | Moving average of 1000 images counting number of error pixels on the detector | [optional] 
**saturated_pixels** | **float** | Moving average of 1000 images counting number of saturated pixels on the detector | [optional] 
**roi_beam_pixels** | **float** | If there is an ROI defined with name \&quot;beam\&quot;, this number will hold moving average of 1000 images for number of valid pixels within this ROI  | [optional] 
**roi_beam_sum** | **float** | If there is an ROI defined with name \&quot;beam\&quot;, this number will hold moving average of 1000 images for sum of valid pixels within this ROI  | [optional] 

## Example

```python
from jfjoch_client.models.measurement_statistics import MeasurementStatistics

# TODO update the JSON string below
json = "{}"
# create an instance of MeasurementStatistics from a JSON string
measurement_statistics_instance = MeasurementStatistics.from_json(json)
# print the JSON string representation of the object
print(MeasurementStatistics.to_json())

# convert the object into a dict
measurement_statistics_dict = measurement_statistics_instance.to_dict()
# create an instance of MeasurementStatistics from a dict
measurement_statistics_from_dict = MeasurementStatistics.from_dict(measurement_statistics_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


