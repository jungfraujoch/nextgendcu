# RotationAxis

Definition of a crystal rotation axis

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**name** | **str** | Name of rotation axis (e.g., omega, phi) | [optional] [default to 'omega']
**step** | **float** | Angle step in degrees | 
**start** | **float** | Start angle in degrees | [optional] [default to 0]
**vector** | **List[float]** | Rotation axis | 

## Example

```python
from jfjoch_client.models.rotation_axis import RotationAxis

# TODO update the JSON string below
json = "{}"
# create an instance of RotationAxis from a JSON string
rotation_axis_instance = RotationAxis.from_json(json)
# print the JSON string representation of the object
print(RotationAxis.to_json())

# convert the object into a dict
rotation_axis_dict = rotation_axis_instance.to_dict()
# create an instance of RotationAxis from a dict
rotation_axis_from_dict = RotationAxis.from_dict(rotation_axis_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


