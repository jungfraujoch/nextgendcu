# ZeromqSettings

ZeroMQ configuration for Jungfraujoch software. This structure is used to provide default settings using configuration JSON file and is not used in HTTP. 

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**send_watermark** | **int** | Watermark for ZeroMQ send queue (number of outstanding messages queued on Jungfraujoch server per queue) | [optional] [default to 100]
**send_buffer_size** | **int** | Send buffer size for ZeroMQ socket | [optional] 
**image_socket** | **List[str]** | PUSH ZeroMQ socket for images. In case multiple sockets are provided, images are streamed over multiple sockets.   Images are serialized using CBOR. Address follows ZeroMQ convention for sockets - in practice ipc://&lt;socket file&gt; and tpc://&lt;IP address&gt;:&lt;port&gt; sockets are OK. 0.0.0.0 instead of IP address is accepted and means listening on all network interfaces.  | [optional] 
**writer_notification_socket** | **str** | PULL ZeroMQ socket for notifications from writer that it finished operation.  This allows Jungfraujoch to operate in a synchronous manner, with end of acquisition being also end of writing. Address follows ZeroMQ convention for sockets - in practice ipc://&lt;socket file&gt; and tpc://&lt;IP address&gt;:&lt;port&gt; sockets are OK. 0.0.0.0 instead of IP address should be avoided, as this socket address is forwarded to the writer process via START ZerOMQ message and in case of multiple ineterfaces the address might be ambigous. Using * (star) instead of port number is allowed and it means a random free port number.  | [optional] 

## Example

```python
from jfjoch_client.models.zeromq_settings import ZeromqSettings

# TODO update the JSON string below
json = "{}"
# create an instance of ZeromqSettings from a JSON string
zeromq_settings_instance = ZeromqSettings.from_json(json)
# print the JSON string representation of the object
print(ZeromqSettings.to_json())

# convert the object into a dict
zeromq_settings_dict = zeromq_settings_instance.to_dict()
# create an instance of ZeromqSettings from a dict
zeromq_settings_from_dict = ZeromqSettings.from_dict(zeromq_settings_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


