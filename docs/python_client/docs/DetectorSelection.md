# DetectorSelection


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 

## Example

```python
from jfjoch_client.models.detector_selection import DetectorSelection

# TODO update the JSON string below
json = "{}"
# create an instance of DetectorSelection from a JSON string
detector_selection_instance = DetectorSelection.from_json(json)
# print the JSON string representation of the object
print(DetectorSelection.to_json())

# convert the object into a dict
detector_selection_dict = detector_selection_instance.to_dict()
# create an instance of DetectorSelection from a dict
detector_selection_from_dict = DetectorSelection.from_dict(detector_selection_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


