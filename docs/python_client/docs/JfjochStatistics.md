# JfjochStatistics

Pool statistics for Jungfraujoch to reduce transfers between frontend and jfjoch_broker

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**detector** | [**DetectorStatus**](DetectorStatus.md) |  | [optional] 
**detector_list** | [**DetectorList**](DetectorList.md) |  | [optional] 
**detector_settings** | [**DetectorSettings**](DetectorSettings.md) |  | [optional] 
**image_format_settings** | [**ImageFormatSettings**](ImageFormatSettings.md) |  | [optional] 
**instrument_metadata** | [**InstrumentMetadata**](InstrumentMetadata.md) |  | [optional] 
**file_writer_settings** | [**FileWriterSettings**](FileWriterSettings.md) |  | [optional] 
**data_processing_settings** | [**SpotFindingSettings**](SpotFindingSettings.md) |  | [optional] 
**measurement** | [**MeasurementStatistics**](MeasurementStatistics.md) |  | [optional] 
**broker** | [**BrokerStatus**](BrokerStatus.md) |  | [optional] 
**fpga** | [**List[FpgaStatusInner]**](FpgaStatusInner.md) |  | [optional] 
**calibration** | [**List[CalibrationStatisticsInner]**](CalibrationStatisticsInner.md) |  | [optional] 
**zeromq_preview** | [**ZeromqPreviewSettings**](ZeromqPreviewSettings.md) |  | [optional] 
**zeromq_metadata** | [**ZeromqMetadataSettings**](ZeromqMetadataSettings.md) |  | [optional] 
**pixel_mask** | [**PixelMaskStatistics**](PixelMaskStatistics.md) |  | [optional] 
**roi** | [**RoiDefinitions**](RoiDefinitions.md) |  | [optional] 

## Example

```python
from jfjoch_client.models.jfjoch_statistics import JfjochStatistics

# TODO update the JSON string below
json = "{}"
# create an instance of JfjochStatistics from a JSON string
jfjoch_statistics_instance = JfjochStatistics.from_json(json)
# print the JSON string representation of the object
print(JfjochStatistics.to_json())

# convert the object into a dict
jfjoch_statistics_dict = jfjoch_statistics_instance.to_dict()
# create an instance of JfjochStatistics from a dict
jfjoch_statistics_from_dict = JfjochStatistics.from_dict(jfjoch_statistics_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


