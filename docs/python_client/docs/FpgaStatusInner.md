# FpgaStatusInner


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pci_dev_id** | **str** |  | 
**serial_number** | **str** |  | 
**fw_version** | **str** |  | 
**base_mac_addr** | **str** |  | 
**eth_link_count** | **int** |  | 
**eth_link_status** | **int** |  | 
**power_usage_w** | **float** |  | 
**fpga_temp_c** | **float** |  | 
**hbm_temp_c** | **float** |  | 
**packets_udp** | **int** |  | 
**packets_sls** | **int** |  | 
**idle** | **bool** |  | 
**pcie_link_speed** | **int** | PCIe link speed measured by generation (expected value is 4 &#x3D;&#x3D; PCIe Gen4) | 
**pcie_link_width** | **int** | PCIe link width (expected value is 8 &#x3D;&#x3D; x8) | 

## Example

```python
from jfjoch_client.models.fpga_status_inner import FpgaStatusInner

# TODO update the JSON string below
json = "{}"
# create an instance of FpgaStatusInner from a JSON string
fpga_status_inner_instance = FpgaStatusInner.from_json(json)
# print the JSON string representation of the object
print(FpgaStatusInner.to_json())

# convert the object into a dict
fpga_status_inner_dict = fpga_status_inner_instance.to_dict()
# create an instance of FpgaStatusInner from a dict
fpga_status_inner_from_dict = FpgaStatusInner.from_dict(fpga_status_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


