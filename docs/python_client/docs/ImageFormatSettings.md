# ImageFormatSettings


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**summation** | **bool** | Enable summation of images to a given image_time If disabled images are saved according to original detector speed, but image count is adjusted  | 
**geometry_transform** | **bool** | Place module read-out into their location on composed detector and extend multipixels  | 
**jungfrau_conversion** | **bool** | Convert pixel value in ADU to photon counts/energy Only affects JUNGFRAU detector  | 
**jungfrau_conversion_factor_ke_v** | **float** | Used to convert energy deposited into pixel to counts If not provided incident_energy_keV is used  | [optional] 
**bit_depth_image** | **int** | Bit depth of resulting image (it doesn&#39;t affect the detector read-out value) If not provided value is adjusted automatically  | [optional] 
**signed_output** | **bool** | Controls if pixels have signed output If not provided value is adjusted automatically  | [optional] 
**mask_module_edges** | **bool** | Mask 1 pixel on the module boundary  | [default to True]
**mask_chip_edges** | **bool** | Mask multipixels on chip boundary  | [default to True]
**jungfrau_mask_pixels_without_g0** | **bool** | JUNGFRAU: mask pixels that don&#39;t operate in G0, but do operate in G1 and G1. This should be turned off for cases, where detector is operated at room temperature with long exposure time.  | [optional] [default to True]
**apply_mask** | **bool** | Masked pixels are set to special value in the images produced by Jungfraujoch  | [default to False]
**jungfrau_pedestal_g0_rms_limit** | **int** | Pixels with pedestal G0 RMS above the threshold are marked as masked pixels | [optional] [default to 100]

## Example

```python
from jfjoch_client.models.image_format_settings import ImageFormatSettings

# TODO update the JSON string below
json = "{}"
# create an instance of ImageFormatSettings from a JSON string
image_format_settings_instance = ImageFormatSettings.from_json(json)
# print the JSON string representation of the object
print(ImageFormatSettings.to_json())

# convert the object into a dict
image_format_settings_dict = image_format_settings_instance.to_dict()
# create an instance of ImageFormatSettings from a dict
image_format_settings_from_dict = ImageFormatSettings.from_dict(image_format_settings_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


