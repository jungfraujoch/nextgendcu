# CalibrationStatisticsInner


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**module_number** | **int** |  | 
**storage_cell_number** | **int** |  | 
**pedestal_g0_mean** | **float** |  | 
**pedestal_g1_mean** | **float** |  | 
**pedestal_g2_mean** | **float** |  | 
**gain_g0_mean** | **float** |  | 
**gain_g1_mean** | **float** |  | 
**gain_g2_mean** | **float** |  | 
**masked_pixels** | **int** |  | 

## Example

```python
from jfjoch_client.models.calibration_statistics_inner import CalibrationStatisticsInner

# TODO update the JSON string below
json = "{}"
# create an instance of CalibrationStatisticsInner from a JSON string
calibration_statistics_inner_instance = CalibrationStatisticsInner.from_json(json)
# print the JSON string representation of the object
print(CalibrationStatisticsInner.to_json())

# convert the object into a dict
calibration_statistics_inner_dict = calibration_statistics_inner_instance.to_dict()
# create an instance of CalibrationStatisticsInner from a dict
calibration_statistics_inner_from_dict = CalibrationStatisticsInner.from_dict(calibration_statistics_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


