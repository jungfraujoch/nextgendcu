# BrokerStatus


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**state** | **str** |  | 
**progress** | **float** | Progress of data collection (only available if receiving is running) | [optional] 
**message** | **str** | Message to display besides state of the jfjoch_broker; mostly used for errors and warnings This matters especially for async functions (start/initialize), where API won&#39;t return reason for the error during async operation.  | [optional] 
**message_severity** | **str** | Level of the message to display | [optional] [default to 'error']

## Example

```python
from jfjoch_client.models.broker_status import BrokerStatus

# TODO update the JSON string below
json = "{}"
# create an instance of BrokerStatus from a JSON string
broker_status_instance = BrokerStatus.from_json(json)
# print the JSON string representation of the object
print(BrokerStatus.to_json())

# convert the object into a dict
broker_status_dict = broker_status_instance.to_dict()
# create an instance of BrokerStatus from a dict
broker_status_from_dict = BrokerStatus.from_dict(broker_status_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


