# RoiBoxList

List of box ROIs

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**rois** | [**List[RoiBox]**](RoiBox.md) |  | [optional] 

## Example

```python
from jfjoch_client.models.roi_box_list import RoiBoxList

# TODO update the JSON string below
json = "{}"
# create an instance of RoiBoxList from a JSON string
roi_box_list_instance = RoiBoxList.from_json(json)
# print the JSON string representation of the object
print(RoiBoxList.to_json())

# convert the object into a dict
roi_box_list_dict = roi_box_list_instance.to_dict()
# create an instance of RoiBoxList from a dict
roi_box_list_from_dict = RoiBoxList.from_dict(roi_box_list_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


