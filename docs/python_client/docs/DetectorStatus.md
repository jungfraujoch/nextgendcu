# DetectorStatus


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**state** | [**DetectorState**](DetectorState.md) |  | 
**powerchip** | [**DetectorPowerState**](DetectorPowerState.md) |  | 
**server_version** | **str** | Detector server (on read-out boards) version | 
**number_of_triggers_left** | **int** | Remaining triggers to the detector (max of all modules) | 
**fpga_temp_deg_c** | **List[int]** | Temperature of detector FPGAs | 
**high_voltage_v** | **List[int]** | High voltage for detector modules | 

## Example

```python
from jfjoch_client.models.detector_status import DetectorStatus

# TODO update the JSON string below
json = "{}"
# create an instance of DetectorStatus from a JSON string
detector_status_instance = DetectorStatus.from_json(json)
# print the JSON string representation of the object
print(DetectorStatus.to_json())

# convert the object into a dict
detector_status_dict = detector_status_instance.to_dict()
# create an instance of DetectorStatus from a dict
detector_status_from_dict = DetectorStatus.from_dict(detector_status_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


