# StandardDetectorGeometry

Regular rectangular geometry, first module is in the bottom left corner of the detector

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nmodules** | **int** | Number of modules in the detector | 
**gap_x** | **int** | Gap size in X direction [pixels] | [optional] [default to 8]
**gap_y** | **int** | Gap size in Y direction [pixels] | [optional] [default to 36]
**modules_in_row** | **int** | Number of modules in one row | [optional] [default to 1]

## Example

```python
from jfjoch_client.models.standard_detector_geometry import StandardDetectorGeometry

# TODO update the JSON string below
json = "{}"
# create an instance of StandardDetectorGeometry from a JSON string
standard_detector_geometry_instance = StandardDetectorGeometry.from_json(json)
# print the JSON string representation of the object
print(StandardDetectorGeometry.to_json())

# convert the object into a dict
standard_detector_geometry_dict = standard_detector_geometry_instance.to_dict()
# create an instance of StandardDetectorGeometry from a dict
standard_detector_geometry_from_dict = StandardDetectorGeometry.from_dict(standard_detector_geometry_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


