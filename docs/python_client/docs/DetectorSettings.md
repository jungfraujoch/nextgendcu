# DetectorSettings


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**frame_time_us** | **int** | Interval between consecutive frames. This is internal frame time for the JUNGFRAU detector, image time has to be integer multiply of this number. For EIGER detector this is default frame time, not used otherwise  | 
**count_time_us** | **int** | Integration time of the detector.  If not provided count time will be set to maximum value for a given frame time.  | [optional] 
**internal_frame_generator** | **bool** | Use internal frame generator in FPGA instead of getting data from a real detector | [optional] [default to False]
**internal_frame_generator_images** | **int** | Number of images stored in the internal frame generator. | [optional] [default to 1]
**detector_trigger_delay_ns** | **int** | Delay between TTL trigger and acquisition start [ns] | [optional] [default to 0]
**timing** | [**DetectorTiming**](DetectorTiming.md) |  | [optional] [default to DetectorTiming.TRIGGER]
**eiger_threshold_ke_v** | **float** | Threshold for the EIGER detector. If value is provided, it will be used for all subsequent acquisitions, irrespective of beam energy. If value is not provided, threshold will be determined on start of acquisition as half of incident energy.  This might lead to increased start time.  | [optional] 
**eiger_bit_depth** | **int** | Bit depth of EIGER read-out.  If value is not provided bit depth is adjusted automatically based on the image time.  | [optional] 
**jungfrau_pedestal_g0_frames** | **int** |  | [optional] [default to 2000]
**jungfrau_pedestal_g1_frames** | **int** |  | [optional] [default to 300]
**jungfrau_pedestal_g2_frames** | **int** |  | [optional] [default to 300]
**jungfrau_pedestal_min_image_count** | **int** | Minimum number of collected images for pedestal to consider it viable | [optional] [default to 128]
**jungfrau_storage_cell_count** | **int** |  | [optional] [default to 1]
**jungfrau_storage_cell_delay_ns** | **int** | Delay between two storage cells [ns] | [optional] [default to 5000]
**jungfrau_fixed_gain_g1** | **bool** | Fix gain to G1 (can be useful for storage cells) | [optional] [default to False]
**jungfrau_use_gain_hg0** | **bool** | Use high G0 (for low energy applications) | [optional] [default to False]

## Example

```python
from jfjoch_client.models.detector_settings import DetectorSettings

# TODO update the JSON string below
json = "{}"
# create an instance of DetectorSettings from a JSON string
detector_settings_instance = DetectorSettings.from_json(json)
# print the JSON string representation of the object
print(DetectorSettings.to_json())

# convert the object into a dict
detector_settings_dict = detector_settings_instance.to_dict()
# create an instance of DetectorSettings from a dict
detector_settings_from_dict = DetectorSettings.from_dict(detector_settings_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


