# DetectorListDetectorsInner


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**description** | **str** |  | 
**serial_number** | **str** |  | 
**base_ipv4_addr** | **str** |  | 
**udp_interface_count** | **int** | Number of UDP interfaces per detector module | 
**nmodules** | **int** |  | 
**width** | **int** |  | 
**height** | **int** |  | 
**readout_time_us** | **int** |  | 
**min_frame_time_us** | **int** |  | 
**min_count_time_us** | **int** |  | 

## Example

```python
from jfjoch_client.models.detector_list_detectors_inner import DetectorListDetectorsInner

# TODO update the JSON string below
json = "{}"
# create an instance of DetectorListDetectorsInner from a JSON string
detector_list_detectors_inner_instance = DetectorListDetectorsInner.from_json(json)
# print the JSON string representation of the object
print(DetectorListDetectorsInner.to_json())

# convert the object into a dict
detector_list_detectors_inner_dict = detector_list_detectors_inner_instance.to_dict()
# create an instance of DetectorListDetectorsInner from a dict
detector_list_detectors_inner_from_dict = DetectorListDetectorsInner.from_dict(detector_list_detectors_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


