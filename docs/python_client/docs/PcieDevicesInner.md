# PcieDevicesInner


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**blk** | **str** | Block device name | [optional] 
**ipv4** | **str** | IPv4 address of the block device | [optional] 

## Example

```python
from jfjoch_client.models.pcie_devices_inner import PcieDevicesInner

# TODO update the JSON string below
json = "{}"
# create an instance of PcieDevicesInner from a JSON string
pcie_devices_inner_instance = PcieDevicesInner.from_json(json)
# print the JSON string representation of the object
print(PcieDevicesInner.to_json())

# convert the object into a dict
pcie_devices_inner_dict = pcie_devices_inner_instance.to_dict()
# create an instance of PcieDevicesInner from a dict
pcie_devices_inner_from_dict = PcieDevicesInner.from_dict(pcie_devices_inner_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


