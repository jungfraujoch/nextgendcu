# RoiDefinitions

ROI defintions

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**box** | [**RoiBoxList**](RoiBoxList.md) |  | 
**circle** | [**RoiCircleList**](RoiCircleList.md) |  | 

## Example

```python
from jfjoch_client.models.roi_definitions import RoiDefinitions

# TODO update the JSON string below
json = "{}"
# create an instance of RoiDefinitions from a JSON string
roi_definitions_instance = RoiDefinitions.from_json(json)
# print the JSON string representation of the object
print(RoiDefinitions.to_json())

# convert the object into a dict
roi_definitions_dict = roi_definitions_instance.to_dict()
# create an instance of RoiDefinitions from a dict
roi_definitions_from_dict = RoiDefinitions.from_dict(roi_definitions_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


