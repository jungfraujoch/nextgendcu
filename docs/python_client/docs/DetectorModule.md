# DetectorModule


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**x0** | **float** |  | 
**y0** | **float** |  | 
**fast_axis** | [**DetectorModuleDirection**](DetectorModuleDirection.md) |  | 
**slow_axis** | [**DetectorModuleDirection**](DetectorModuleDirection.md) |  | 

## Example

```python
from jfjoch_client.models.detector_module import DetectorModule

# TODO update the JSON string below
json = "{}"
# create an instance of DetectorModule from a JSON string
detector_module_instance = DetectorModule.from_json(json)
# print the JSON string representation of the object
print(DetectorModule.to_json())

# convert the object into a dict
detector_module_dict = detector_module_instance.to_dict()
# create an instance of DetectorModule from a dict
detector_module_from_dict = DetectorModule.from_dict(detector_module_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


