# DetectorState

Current state of the detector

## Enum

* `IDLE` (value: `'Idle'`)

* `WAITING` (value: `'Waiting'`)

* `BUSY` (value: `'Busy'`)

* `ERROR` (value: `'Error'`)

* `NOT_CONNECTED` (value: `'Not connected'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


