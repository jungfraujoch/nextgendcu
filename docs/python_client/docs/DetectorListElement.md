# DetectorListElement


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | 
**description** | **str** |  | 
**serial_number** | **str** |  | 
**base_ipv4_addr** | **str** |  | 
**udp_interface_count** | **int** | Number of UDP interfaces per detector module | 
**nmodules** | **int** |  | 
**width** | **int** |  | 
**height** | **int** |  | 
**readout_time_us** | **int** |  | 
**min_frame_time_us** | **int** |  | 
**min_count_time_us** | **int** |  | 

## Example

```python
from jfjoch_client.models.detector_list_element import DetectorListElement

# TODO update the JSON string below
json = "{}"
# create an instance of DetectorListElement from a JSON string
detector_list_element_instance = DetectorListElement.from_json(json)
# print the JSON string representation of the object
print(DetectorListElement.to_json())

# convert the object into a dict
detector_list_element_dict = detector_list_element_instance.to_dict()
# create an instance of DetectorListElement from a dict
detector_list_element_from_dict = DetectorListElement.from_dict(detector_list_element_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


