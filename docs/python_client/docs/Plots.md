# Plots


## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**title** | **str** |  | [optional] 
**plot** | [**List[Plot]**](Plot.md) |  | 

## Example

```python
from jfjoch_client.models.plots import Plots

# TODO update the JSON string below
json = "{}"
# create an instance of Plots from a JSON string
plots_instance = Plots.from_json(json)
# print the JSON string representation of the object
print(Plots.to_json())

# convert the object into a dict
plots_dict = plots_instance.to_dict()
# create an instance of Plots from a dict
plots_from_dict = Plots.from_dict(plots_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


