# DetectorModuleDirection


## Enum

* `XP` (value: `'Xp'`)

* `XN` (value: `'Xn'`)

* `YP` (value: `'Yp'`)

* `YN` (value: `'Yn'`)

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


