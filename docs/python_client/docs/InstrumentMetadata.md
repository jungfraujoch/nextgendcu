# InstrumentMetadata

Metadata for a measurement instrument

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source_name** | **str** |  | 
**source_type** | **str** | Type of radiation source. NXmx gives a fixed dictionary, though Jungfraujoch is not enforcing compliance.  https://manual.nexusformat.org/classes/base_classes/NXsource.html#nxsource NXsource allows the following:  Spallation Neutron Source Pulsed Reactor Neutron Source Reactor Neutron Source Synchrotron X-ray Source Pulsed Muon Source Rotating Anode X-ray Fixed Tube X-ray UV Laser Free-Electron Laser Optical Laser Ion Source UV Plasma Source Metal Jet X-ray  | [optional] [default to '']
**instrument_name** | **str** |  | 
**pulsed_source** | **bool** | Settings specific to XFEL (e.g., every image has to come from TTL trigger, save pulse ID and event code)  | [optional] [default to False]
**electron_source** | **bool** | Settings specific to electron source (e.g., wavelength definition)  | [optional] [default to False]

## Example

```python
from jfjoch_client.models.instrument_metadata import InstrumentMetadata

# TODO update the JSON string below
json = "{}"
# create an instance of InstrumentMetadata from a JSON string
instrument_metadata_instance = InstrumentMetadata.from_json(json)
# print the JSON string representation of the object
print(InstrumentMetadata.to_json())

# convert the object into a dict
instrument_metadata_dict = instrument_metadata_instance.to_dict()
# create an instance of InstrumentMetadata from a dict
instrument_metadata_from_dict = InstrumentMetadata.from_dict(instrument_metadata_dict)
```
[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


