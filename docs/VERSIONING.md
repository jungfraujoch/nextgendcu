# Semantic versioning

Jungfraujoch is following semantic versioning. For this purpose we define public API as following:

* OpenAPI configuration interface
* CBOR serialization ZeroMQ stream
* HDF5 file format

This means that changes in the format of thereof must be accompanied by version change - major version in case of breaking changes, minor version in case of feature expansion.

NOTE: FPGA design, PCIe driver, and internal libraries are not part of the public API and are considered internals of Jungfraujoch.
Breaking changes in these components can happen without incrementing major version of the whole package.
It will be marked in changelog.