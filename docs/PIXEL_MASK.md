# Pixel mask

## Mask format

Jungfraujoch follows generally [NXmx format](https://manual.nexusformat.org/classes/applications/NXmx.html) format for pixel mask.
Pixel mask is described as 32-bit unsigned integer array of size the same as the image.
Conditions to mask pixel are described by setting a particular bit to one. This way it is possible to encode reason why pixel is included in the pixel mask, also for one pixel there can be multiple reasons encoded at the same time.

Bit values are set as follows:

Bit 0 - gap (pixel with no sensor)

Bit 1 - pixel doesn't set proper gain during pedestal (JUNGFRAU only)

Bit 2 - pixel pedestal G0 RMS is over threshold (JUNGFRAU only)

Bit 8 - user defined mask

Bit 30 - module edge

Bit 31 - chip edge interpolated pixel (multipixel)

## Custom user mask

Jungfraujoch allows to upload custom user mask. This happens in two steps. First create mask in TIFF format:

```python
import numpy as np
import tifffile as tiff

# Create a 2068x2164 numpy array filled with zeros, with 32-bit unsigned integers
array = np.zeros((2068, 2164), dtype=np.uint32)

# Mark the pixel (300, 400) with the value 1
array[300, 400] = 1

# Save the array as a TIFF file
tiff.imwrite('mask.tiff', array)
```

Pixels with non-zero value in the TIFF file will be marked as belonging to the user mask (bit 8).

Then upload the mask to Jungfraujoch server:
```shell
curl -v http://<jfjoch_broker http address>/config/user_mask.tiff -XPUT --data-binary @mask.tiff
```
