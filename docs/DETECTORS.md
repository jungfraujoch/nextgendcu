# Supported detectors

Jungfraujoch supports PSI JUNGFRAU and PSI EIGER detectors. Jungfruajoch controls the detector via statically compiled `slsDetectorPackage` into its source code.
It is important that detector firmware has to match `slsDetectorPackage` version used in Jungfraujoch (8.0.2 at the moment).
See [PSI Detector group website](https://www.psi.ch/en/lxn/software-releases) for details.