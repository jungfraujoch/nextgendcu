# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'Jungfraujoch'
copyright = '2024, Paul Scherrer Institute'
author = 'Filip Leonarski'
release = '1.0.0-rc.31'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['myst_parser']

templates_path = ['_templates']
exclude_patterns = []
myst_enable_extensions = ['linkify', 'smartquotes']


myst_heading_anchor = 3

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_material'
html_static_path = ['../broker/redoc-static.html']
#html_theme_options = {
#    'sidebar_width': '300px',
#    'page_width': 'auto'
#}

html_theme_options = {
    'repo_url': 'https://gitlab.psi.ch/jungfraujoch/nextgendcu',
    'repo_name': 'Jungfraujoch',
    'nav_title': 'PSI Jungfraujoch',
    'html_minify': True,
    'css_minify': True,
    'globaltoc_depth': 2,
    'repo_type': 'gitlab',
    'color_primary': 'indigo',
    'color_accent': 'lime',
    'logo_icon': '&#xe30d'
}

html_sidebars = {
    "**": ["logo-text.html", "globaltoc.html", "searchbox.html"]
}


html_show_sourcelink = False

