// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_CBORSTREAM2DESERIALIZER_H
#define JUNGFRAUJOCH_CBORSTREAM2DESERIALIZER_H

#include <cstdint>
#include <vector>
#include <string>
#include <memory>

#include "../common/SpotToSave.h"
#include "tinycbor/cbor.h"
#include "JFJochMessages.h"
#include <mutex>

struct CBORStream2DeserializerOutput {
    CBORImageType msg_type = CBORImageType::NONE;
    std::optional<DataMessage> data_message;
    std::optional<StartMessage> start_message;
    std::optional<EndMessage> end_message;
    std::optional<CompressedImage> calibration;
    std::optional<MetadataMessage> metadata;
};

std::shared_ptr<CBORStream2DeserializerOutput> CBORStream2Deserialize(const uint8_t *msg, size_t msg_size);

#endif //JUNGFRAUJOCH_CBORSTREAM2DESERIALIZER_H
