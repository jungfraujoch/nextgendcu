// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_CBORERR_H
#define JUNGFRAUJOCH_CBORERR_H

#include "tinycbor/cbor.h"
#include "../common/JFJochException.h"

inline void cborErr(CborError err) {
    if (err != CborNoError)
        throw JFJochException(JFJochExceptionCategory::CBORError, cbor_error_string(err));
}

#endif //JUNGFRAUJOCH_CBORERR_H
