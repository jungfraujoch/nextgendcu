// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_CBORSTREAM2SERIALIZER_H
#define JUNGFRAUJOCH_CBORSTREAM2SERIALIZER_H

#include <vector>
#include <cstdint>
#include <cstddef>

#include "JFJochMessages.h"

struct CborEncoder;

class CBORStream2Serializer {
    uint8_t *buffer = nullptr;
    size_t max_buffer_size;
    size_t curr_size;
    static void SerializeImageInternal(CborEncoder &ptr, const DataMessage& message, bool metadata_only);
public:
    explicit CBORStream2Serializer(uint8_t *buffer, size_t buffer_size);
    [[nodiscard]] size_t GetBufferSize() const;
    void SerializeSequenceStart(const StartMessage& message);
    void SerializeSequenceEnd(const EndMessage& message);
    void SerializeImage(const DataMessage& message);
    void SerializeMetadata(const MetadataMessage& messages);
    void SerializeCalibration(const CompressedImage& image);

    [[nodiscard]] size_t GetImageAppendOffset() const;
    void AppendImage(size_t image_size);
};


#endif //JUNGFRAUJOCH_CBORSTREAM2SERIALIZER_H
