// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_CBORUTIL_H
#define JUNGFRAUJOCH_CBORUTIL_H

#include "tinycbor/cbor.h"

constexpr const CborTag TagMultiDimArray      = 40;
constexpr const CborTag TagDECTRISCompression = 56500;

// rfc8746
constexpr const CborTag TagFloatLE            = 0b01010101;
constexpr const CborTag TagUnsignedInt8Bit    = 0b01000000;
constexpr const CborTag TagSignedInt8Bit      = 0b01001000;

constexpr const CborTag TagUnsignedInt16BitLE = 0b01000101;
constexpr const CborTag TagSignedInt16BitLE   = 0b01001101;

constexpr const CborTag TagUnsignedInt32BitLE = 0b01000110;
constexpr const CborTag TagSignedInt32BitLE   = 0b01001110;

constexpr const CborTag TagUnsignedInt64BitLE = 0b01000111;

#endif //JUNGFRAUJOCH_CBORUTIL_H
