// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_JFJOCHMESSAGES_H
#define JUNGFRAUJOCH_JFJOCHMESSAGES_H

#include <string>
#include <cstdint>
#include <map>
#include <vector>
#include <optional>
#include <cstring>
#include <stdexcept>
#include <nlohmann/json.hpp>

#include "../compression/CompressionAlgorithmEnum.h"
#include "../common/SpotToSave.h"
#include "../common/UnitCell.h"

constexpr const uint64_t user_data_release = 5;
constexpr const uint64_t user_data_magic_number = 0x52320000UL | user_data_release;

enum class CBORImageType {START, END, IMAGE, CALIBRATION, METADATA, NONE};

struct CompressedImage {
    const uint8_t *data;
    size_t size; // Including compression
    size_t xpixel;
    size_t ypixel;
    size_t pixel_depth_bytes;
    bool pixel_is_signed;
    bool pixel_is_float = false;
    CompressionAlgorithm algorithm;
    std::string channel;

    std::vector<uint8_t> storage;
};

struct ROIMessage {
    int64_t sum;
    uint64_t sum_square;
    int64_t max_count;
    uint64_t pixels;
};

struct DataMessage {
    int64_t number = INT64_MIN;
    CompressedImage image;

    std::optional<uint64_t> packets_expected;
    std::optional<uint64_t> packets_received;
    std::optional<float> image_collection_efficiency;

    std::vector<SpotToSave> spots;
    std::optional<uint64_t> spot_count_in_rings = 0;

    std::vector<float> az_int_profile;
    std::optional<float> bkg_estimate;

    bool indexing_result;
    std::vector<float> indexing_lattice;
    std::optional<UnitCell> indexing_unit_cell;

    std::vector<uint64_t> adu_histogram;

    uint64_t timestamp;
    uint32_t timestamp_base;

    uint32_t exptime;
    uint32_t exptime_base;

    std::string run_name;
    uint64_t run_number;

    uint64_t saturated_pixel_count;
    uint64_t error_pixel_count;
    uint64_t strong_pixel_count;
    std::optional<int64_t> min_viable_pixel_value;
    std::optional<int64_t> max_viable_pixel_value;

    nlohmann::json user_data;

    std::optional<uint64_t> jf_info;
    std::optional<uint64_t> receiver_aq_dev_delay;
    std::optional<uint64_t> receiver_free_send_buf;
    std::optional<uint64_t> storage_cell;

    std::optional<uint64_t> xfel_pulse_id;
    std::optional<uint64_t> xfel_event_code;

    std::map<std::string, ROIMessage> roi;

    std::optional<int64_t> original_number;

};

struct GoniometerAxis {
    std::string name;
    float increment;
    float start;
};

struct StartMessage {
    float detector_distance;
    float beam_center_x;
    float beam_center_y;

    uint64_t number_of_images;

    uint64_t image_size_x;
    uint64_t image_size_y;
    uint64_t bit_depth_image; // user data
    std::optional<uint64_t> bit_depth_readout;
    bool pixel_signed; // user data

    bool countrate_correction_enabled;

    float incident_energy;
    float incident_wavelength;

    float frame_time;
    float count_time;

    int64_t saturation_value;
    std::optional<int64_t> error_value;

    float pixel_size_x;
    float pixel_size_y;
    float sensor_thickness;
    std::string sensor_material;

    std::optional<UnitCell> unit_cell; // user data
    uint64_t space_group_number; // user data
    uint64_t max_spot_count; // user data

    std::optional<uint64_t> storage_cell_number;
    uint64_t storage_cell_delay_ns;

    bool flatfield_enabled;
    bool pixel_mask_enabled;

    std::string arm_date;

    std::string sample_name; // user data
    std::string file_prefix; // user data
    int64_t images_per_file = 1; // user data

    std::vector<std::string> channels;

    std::string detector_description;
    std::string detector_serial_number;
    std::string run_name;
    uint64_t run_number;

    std::vector<std::string> gain_file_names;

    std::vector<std::string> roi_names;

    std::optional<GoniometerAxis> goniometer;
    float rotation_axis[3];
    float detector_translation[3];

    std::string source_type;
    std::string source_name;
    std::string instrument_name;

    uint64_t az_int_bin_number;
    uint64_t summation;

    std::vector<float> az_int_bin_to_q;

    std::vector<CompressedImage> pixel_mask;

    std::optional<float> total_flux;
    std::optional<float> attenuator_transmission;

    std::optional<bool> write_master_file;

    void AddPixelMask(CompressedImage image) {
        pixel_mask.emplace_back(std::move(image));
    }

    nlohmann::json user_data;

    std::optional<float> data_reduction_factor_serialmx;
    std::string experiment_group;

    std::string jfjoch_release;

    std::optional<uint64_t> socket_number; // This is number of socket in ZeroMQ
    std::string writer_notification_zmq_addr; // Socket to inform detector on writer done

    std::optional<bool> jungfrau_conversion_enabled;
    std::optional<float> jungfrau_conversion_factor;
    std::optional<bool> geometry_transformation_enabled;
    std::optional<std::string> summation_mode;
    std::optional<bool> overwrite;
    std::optional<int64_t> hdf5_format_version;
    std::optional<bool> xfel_pulse_id;
};

struct EndMessage {
    uint64_t max_image_number; // Counting from 1, i.e. 0 = no images collected
    std::optional<uint64_t> images_collected_count;
    std::optional<uint64_t> images_sent_to_write_count;
    std::optional<uint64_t> max_receiver_delay;
    std::optional<float> efficiency;
    std::optional<float> indexing_rate;
    std::optional<float> bkg_estimate;

    std::optional<std::string> end_date;

    std::string run_name;
    uint64_t run_number;

    std::map<std::string, std::vector<float>> az_int_result;
    std::map<std::string, std::vector<uint64_t>> adu_histogram;
    uint64_t adu_histogram_bin_width;
};

struct MetadataMessage {
    std::string run_name;
    uint64_t run_number;
    std::vector<DataMessage> images;
};

#endif //JUNGFRAUJOCH_JFJOCHMESSAGES_H
