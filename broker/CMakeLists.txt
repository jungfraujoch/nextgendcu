AUX_SOURCE_DIRECTORY(gen/model MODEL_SOURCES)

ADD_LIBRARY(JFJochAPI STATIC ${MODEL_SOURCES} gen/api/DefaultApi.cpp gen/api/DefaultApi.h)

TARGET_LINK_LIBRARIES(JFJochAPI pistache_static)
TARGET_INCLUDE_DIRECTORIES(JFJochAPI PUBLIC gen/model gen/api)

ADD_LIBRARY(JFJochBroker STATIC
        JFJochStateMachine.cpp JFJochStateMachine.h
        JFJochServices.cpp JFJochServices.h
        JFJochBrokerHttp.cpp JFJochBrokerHttp.h JFJochBrokerParser.cpp JFJochBrokerParser.h
        OpenAPIConvert.h
        OpenAPIConvert.cpp)

TARGET_LINK_LIBRARIES(JFJochBroker JFJochReceiver JFJochDetector JFJochCommon JFJochAPI JFJochPreview)

ADD_EXECUTABLE(jfjoch_broker jfjoch_broker.cpp)
TARGET_LINK_LIBRARIES(jfjoch_broker JFJochBroker)

INSTALL(TARGETS jfjoch_broker RUNTIME COMPONENT jfjoch)

INSTALL(FILES redoc-static.html DESTINATION jfjoch/frontend COMPONENT jfjoch )
