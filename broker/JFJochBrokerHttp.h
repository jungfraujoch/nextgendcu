// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only
// Using OpenAPI licensed with Apache License 2.0

#ifndef JUNGFRAUJOCH_JFJOCHBROKERHTTP_H
#define JUNGFRAUJOCH_JFJOCHBROKERHTTP_H

#include <pistache/endpoint.h>
#include <pistache/router.h>
#include <pistache/client.h>
#include <pistache/http.h>

#include "../common/Logger.h"
#include "JFJochStateMachine.h"
#include "gen/api/DefaultApi.h"


class JFJochBrokerHttp : public org::openapitools::server::api::DefaultApi {
    Logger logger{"JFJochBroker"};
    JFJochServices services {logger};

    void config_image_format_get(Pistache::Http::ResponseWriter &response) override;

    void config_image_format_put(const org::openapitools::server::model::Image_format_settings &imageFormatSettings,
                                 Pistache::Http::ResponseWriter &response) override;

    void fpga_status_get(Pistache::Http::ResponseWriter &response) override;

    JFJochStateMachine state_machine {services, logger};
    std::string frontend_directory;

    void config_detector_get(Pistache::Http::ResponseWriter &response) override;

    void config_detector_put(const org::openapitools::server::model::Detector_settings &detectorSettings,
                             Pistache::Http::ResponseWriter &response) override;

    void config_azim_int_get(Pistache::Http::ResponseWriter &response) override;

    void config_azim_int_put(const org::openapitools::server::model::Azim_int_settings &radIntSettings,
                            Pistache::Http::ResponseWriter &response) override;

    void config_select_detector_get(Pistache::Http::ResponseWriter &response) override;

    void config_select_detector_put(const org::openapitools::server::model::Detector_selection &detectorSelection,
                                    Pistache::Http::ResponseWriter &response) override;

    void detector_status_get(Pistache::Http::ResponseWriter &response) override;

    void config_spot_finding_get(Pistache::Http::ResponseWriter &response) override;

    void config_spot_finding_put(const org::openapitools::server::model::Spot_finding_settings &spotFindingSettings,
                                 Pistache::Http::ResponseWriter &response) override;

    void GenericPlot(PlotType plot_type,
                     const std::optional<int32_t> &binning,
                     const std::optional<bool>& compression,
                     Pistache::Http::ResponseWriter &response);

    void plot_bkg_estimate_get(const std::optional<int32_t> &binning, const std::optional<bool>& compression,
                               Pistache::Http::ResponseWriter &response) override;
    void plot_error_pixel_get(const std::optional<int32_t> &binning, const std::optional<bool>& compression,
                              Pistache::Http::ResponseWriter &response) override;
    void plot_image_collection_efficiency_get(const std::optional<int32_t> &binning, const std::optional<bool>& compression,
                                              Pistache::Http::ResponseWriter &response) override;
    void plot_indexing_rate_get(const std::optional<int32_t> &binning, const std::optional<bool>& compression,
                                Pistache::Http::ResponseWriter &response) override;
    void plot_receiver_delay_get(const std::optional<int32_t> &binning, const std::optional<bool>& compression,
                                 Pistache::Http::ResponseWriter &response) override;
    void plot_receiver_free_send_buffers_get(const std::optional<int32_t> &binning, const std::optional<bool>& compression,
                                             Pistache::Http::ResponseWriter &response) override;
    void plot_roi_max_count_get(const std::optional<int32_t> &binning, const std::optional<bool>& compression,
                                Pistache::Http::ResponseWriter &response) override;
    void plot_roi_sum_get(const std::optional<int32_t> &binning, const std::optional<bool>& compression,
                          Pistache::Http::ResponseWriter &response) override;
    void plot_roi_valid_pixels_get(const std::optional<int32_t> &binning, const std::optional<bool>& compression,
                                   Pistache::Http::ResponseWriter &response) override;
    void plot_spot_count_get(const std::optional<int32_t> &binning, const std::optional<bool>& compression,
                             Pistache::Http::ResponseWriter &response) override;
    void plot_strong_pixel_get(const std::optional<int32_t> &binning, const std::optional<bool>& compression,
                               Pistache::Http::ResponseWriter &response) override;
    void plot_max_value_get(const std::optional<int32_t> &binning, const std::optional<bool> &compression,
                            Pistache::Http::ResponseWriter &response) override;
     void plot_azim_int_get(const std::optional<bool>& compression, Pistache::Http::ResponseWriter &response) override;

    void statistics_calibration_get(Pistache::Http::ResponseWriter &response) override;

    void statistics_data_collection_get(Pistache::Http::ResponseWriter &response) override;

    void cancel_post(Pistache::Http::ResponseWriter &response) override;

    void deactivate_post(Pistache::Http::ResponseWriter &response) override;

    void initialize_post(Pistache::Http::ResponseWriter &response) override;

    void start_post(const org::openapitools::server::model::Dataset_settings &datasetSettings,
                    Pistache::Http::ResponseWriter &response) override;

    void status_get(Pistache::Http::ResponseWriter &response) override;

    void wait_till_done_post(const std::optional<int32_t> &timeoutMs, Pistache::Http::ResponseWriter &response) override;
    void trigger_post(Pistache::Http::ResponseWriter &response) override;
    void pedestal_post(Pistache::Http::ResponseWriter &response) override;


    void preview_calibration_tiff_get(Pistache::Http::ResponseWriter &response) override;
    void preview_pedestal_tiff_get(const std::optional<int32_t> &gainLevel,
                                   const std::optional<int32_t> &sc,
                                   Pistache::Http::ResponseWriter &response) override;

    void preview_image_jpeg_get(Pistache::Http::ResponseWriter &response) override;
    void preview_image_jpeg_post(const org::openapitools::server::model::Preview_settings &previewSettings,
                                 Pistache::Http::ResponseWriter &response) override;
    void preview_image_tiff_get(Pistache::Http::ResponseWriter &response) override;

    void config_roi_get(Pistache::Http::ResponseWriter &response) override;
    void config_roi_put(const org::openapitools::server::model::Roi_definitions &roiDefinitions,
        Pistache::Http::ResponseWriter &response) override;

    void config_internal_generator_image_put(const Pistache::Rest::Request &request,
                                             Pistache::Http::ResponseWriter &response) override;

    void xfel_event_code_get(Pistache::Http::ResponseWriter &response) override;
    void xfel_pulse_id_get(Pistache::Http::ResponseWriter &response) override;

    void config_mask_tiff_get(Pistache::Http::ResponseWriter &response) override;

    void config_user_mask_tiff_get(Pistache::Http::ResponseWriter &response) override;

    void config_user_mask_tiff_put(const Pistache::Rest::Request &request,
                                    Pistache::Http::ResponseWriter &response) override;

    void config_internal_generator_image_tiff_put(const Pistache::Rest::Request &request,
                                                  Pistache::Http::ResponseWriter &response) override;

    void config_instrument_get(Pistache::Http::ResponseWriter &response) override;

    void config_instrument_put(const org::openapitools::server::model::Instrument_metadata &instrumentMetadata,
                               Pistache::Http::ResponseWriter &response) override;

    void GetStaticFile(const Pistache::Rest::Request &request, Pistache::Http::ResponseWriter response);
    std::pair<Pistache::Http::Code, std::string> handleOperationException(const std::exception &ex) const noexcept override;

    template <class T>
    void ProcessOutput(const T& output, Pistache::Http::ResponseWriter &response, bool compression = false) {
        std::stringstream s;
        if(!output.validate(s)) {
            logger.Error(s.str());
            response.send(Pistache::Http::Code::Internal_Server_Error, s.str(), MIME(Text, Plain));
        }

        nlohmann::json j = output;
        if (compression)
            response.setCompression(Pistache::Http::Header::Encoding::Deflate);
        response.send(Pistache::Http::Code::Ok, j.dump(), MIME(Application, Json));
    }

    void version_get(Pistache::Http::ResponseWriter &response) override;

    void config_image_format_conversion_post(Pistache::Http::ResponseWriter &response) override;

    void config_image_format_raw_post(Pistache::Http::ResponseWriter &response) override;

    void statistics_get(const std::optional<bool> &compression, Pistache::Http::ResponseWriter &response) override;

    void config_zeromq_preview_get(Pistache::Http::ResponseWriter &response) override;

    void
    config_zeromq_preview_put(const org::openapitools::server::model::Zeromq_preview_settings &zeromqPreviewSettings,
                              Pistache::Http::ResponseWriter &response) override;

    void config_zeromq_metadata_get(Pistache::Http::ResponseWriter &response) override;

    void config_zeromq_metadata_put(
        const org::openapitools::server::model::Zeromq_metadata_settings &zeromqMetadataSettings,
        Pistache::Http::ResponseWriter &response) override;

    void config_mask_get(Pistache::Http::ResponseWriter &response) override;

    void config_user_mask_get(Pistache::Http::ResponseWriter &response) override;

    void config_user_mask_put(const Pistache::Rest::Request &request,
        Pistache::Http::ResponseWriter &response) override;

    void plot_indexing_unit_cell_get(const std::optional<int32_t> &binning, const std::optional<bool> &compression,
        Pistache::Http::ResponseWriter &response) override;

    void plot_indexing_unit_cell_angle_get(const std::optional<int32_t> &binning, const std::optional<bool> &compression,
        Pistache::Http::ResponseWriter &response) override;

    void plot_packets_received_get(const std::optional<int32_t> &binning, const std::optional<bool> &compression,
        Pistache::Http::ResponseWriter &response) override;

    void image_buffer_clear_post(Pistache::Http::ResponseWriter &response) override;

    void image_buffer_image_cbor_get(const std::optional<int64_t> &imageNumber,
                                     Pistache::Http::ResponseWriter &response) override;

    void image_buffer_start_cbor_get(Pistache::Http::ResponseWriter &response) override;

    void image_buffer_status_get(Pistache::Http::ResponseWriter &response) override;

public:
    JFJochBrokerHttp(const DiffractionExperiment& experiment, std::shared_ptr<Pistache::Rest::Router> &rtr);
    void AddDetectorSetup(const DetectorSetup &setup);
    JFJochServices& Services();

    JFJochBrokerHttp& FrontendDirectory(const std::string &directory);

    ~JFJochBrokerHttp() override = default;

private:
    void config_file_writer_get(Pistache::Http::ResponseWriter &response) override;

    void config_file_writer_put(const org::openapitools::server::model::File_writer_settings &fileWriterSettings,
                                Pistache::Http::ResponseWriter &response) override;
};


#endif //JUNGFRAUJOCH_JFJOCHBROKERHTTP_H
