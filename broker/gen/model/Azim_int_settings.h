/**
* Jungfraujoch
* API to control Jungfraujoch developed by the Paul Scherrer Institute (Switzerland). Jungfraujoch is a data acquisition and analysis system for pixel array detectors, primarly PSI JUNGFRAU. Jungfraujoch uses FPGA boards to acquire data at high data rates. 
*
* The version of the OpenAPI document: 1.0.0-rc.31
* Contact: filip.leonarski@psi.ch
*
* NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
* https://openapi-generator.tech
* Do not edit the class manually.
*/
/*
 * Azim_int_settings.h
 *
 * 
 */

#ifndef Azim_int_settings_H_
#define Azim_int_settings_H_


#include <nlohmann/json.hpp>

namespace org::openapitools::server::model
{

/// <summary>
/// 
/// </summary>
class  Azim_int_settings
{
public:
    Azim_int_settings();
    virtual ~Azim_int_settings() = default;


    /// <summary>
    /// Validate the current data in the model. Throws a ValidationException on failure.
    /// </summary>
    void validate() const;

    /// <summary>
    /// Validate the current data in the model. Returns false on error and writes an error
    /// message into the given stringstream.
    /// </summary>
    bool validate(std::stringstream& msg) const;

    /// <summary>
    /// Helper overload for validate. Used when one model stores another model and calls it's validate.
    /// Not meant to be called outside that case.
    /// </summary>
    bool validate(std::stringstream& msg, const std::string& pathPrefix) const;

    bool operator==(const Azim_int_settings& rhs) const;
    bool operator!=(const Azim_int_settings& rhs) const;

    /////////////////////////////////////////////
    /// Azim_int_settings members

    /// <summary>
    /// If polarization factor is provided, than polarization correction is enabled.
    /// </summary>
    float getPolarizationFactor() const;
    void setPolarizationFactor(float const value);
    bool polarizationFactorIsSet() const;
    void unsetPolarization_factor();
    /// <summary>
    /// Apply solid angle correction for radial integration
    /// </summary>
    bool isSolidAngleCorr() const;
    void setSolidAngleCorr(bool const value);
    /// <summary>
    /// 
    /// </summary>
    float getHighQRecipA() const;
    void setHighQRecipA(float const value);
    /// <summary>
    /// 
    /// </summary>
    float getLowQRecipA() const;
    void setLowQRecipA(float const value);
    /// <summary>
    /// 
    /// </summary>
    float getQSpacing() const;
    void setQSpacing(float const value);

    friend  void to_json(nlohmann::json& j, const Azim_int_settings& o);
    friend  void from_json(const nlohmann::json& j, Azim_int_settings& o);
protected:
    float m_Polarization_factor;
    bool m_Polarization_factorIsSet;
    bool m_Solid_angle_corr;

    float m_High_q_recipA;

    float m_Low_q_recipA;

    float m_Q_spacing;

    
};

} // namespace org::openapitools::server::model

#endif /* Azim_int_settings_H_ */
