/**
* Jungfraujoch
* API to control Jungfraujoch developed by the Paul Scherrer Institute (Switzerland). Jungfraujoch is a data acquisition and analysis system for pixel array detectors, primarly PSI JUNGFRAU. Jungfraujoch uses FPGA boards to acquire data at high data rates. 
*
* The version of the OpenAPI document: 1.0.0-rc.31
* Contact: filip.leonarski@psi.ch
*
* NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
* https://openapi-generator.tech
* Do not edit the class manually.
*/


#include "Roi_circle_list.h"
#include "Helpers.h"

#include <sstream>

namespace org::openapitools::server::model
{

Roi_circle_list::Roi_circle_list()
{
    
}

void Roi_circle_list::validate() const
{
    std::stringstream msg;
    if (!validate(msg))
    {
        throw org::openapitools::server::helpers::ValidationException(msg.str());
    }
}

bool Roi_circle_list::validate(std::stringstream& msg) const
{
    return validate(msg, "");
}

bool Roi_circle_list::validate(std::stringstream& msg, const std::string& pathPrefix) const
{
    bool success = true;
    const std::string _pathPrefix = pathPrefix.empty() ? "Roi_circle_list" : pathPrefix;

         
    
    /* Rois */ {
        const std::vector<org::openapitools::server::model::Roi_circle>& value = m_Rois;
        const std::string currentValuePath = _pathPrefix + ".rois";
                
        
        if (value.size() > 32)
        {
            success = false;
            msg << currentValuePath << ": must have at most 32 elements;";
        }
        { // Recursive validation of array elements
            const std::string oldValuePath = currentValuePath;
            int i = 0;
            for (const org::openapitools::server::model::Roi_circle& value : value)
            { 
                const std::string currentValuePath = oldValuePath + "[" + std::to_string(i) + "]";
                        
        success = value.validate(msg, currentValuePath + ".rois") && success;
 
                i++;
            }
        }

    }
    
    return success;
}

bool Roi_circle_list::operator==(const Roi_circle_list& rhs) const
{
    return
    
    
    (getRois() == rhs.getRois())
    
    
    ;
}

bool Roi_circle_list::operator!=(const Roi_circle_list& rhs) const
{
    return !(*this == rhs);
}

void to_json(nlohmann::json& j, const Roi_circle_list& o)
{
    j = nlohmann::json::object();
    j["rois"] = o.m_Rois;
    
}

void from_json(const nlohmann::json& j, Roi_circle_list& o)
{
    j.at("rois").get_to(o.m_Rois);
    
}

std::vector<org::openapitools::server::model::Roi_circle> Roi_circle_list::getRois() const
{
    return m_Rois;
}
void Roi_circle_list::setRois(std::vector<org::openapitools::server::model::Roi_circle> const& value)
{
    m_Rois = value;
}


} // namespace org::openapitools::server::model

