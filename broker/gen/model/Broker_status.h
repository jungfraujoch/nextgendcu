/**
* Jungfraujoch
* API to control Jungfraujoch developed by the Paul Scherrer Institute (Switzerland). Jungfraujoch is a data acquisition and analysis system for pixel array detectors, primarly PSI JUNGFRAU. Jungfraujoch uses FPGA boards to acquire data at high data rates. 
*
* The version of the OpenAPI document: 1.0.0-rc.31
* Contact: filip.leonarski@psi.ch
*
* NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
* https://openapi-generator.tech
* Do not edit the class manually.
*/
/*
 * Broker_status.h
 *
 * 
 */

#ifndef Broker_status_H_
#define Broker_status_H_


#include <string>
#include <nlohmann/json.hpp>

namespace org::openapitools::server::model
{

/// <summary>
/// 
/// </summary>
class  Broker_status
{
public:
    Broker_status();
    virtual ~Broker_status() = default;


    /// <summary>
    /// Validate the current data in the model. Throws a ValidationException on failure.
    /// </summary>
    void validate() const;

    /// <summary>
    /// Validate the current data in the model. Returns false on error and writes an error
    /// message into the given stringstream.
    /// </summary>
    bool validate(std::stringstream& msg) const;

    /// <summary>
    /// Helper overload for validate. Used when one model stores another model and calls it's validate.
    /// Not meant to be called outside that case.
    /// </summary>
    bool validate(std::stringstream& msg, const std::string& pathPrefix) const;

    bool operator==(const Broker_status& rhs) const;
    bool operator!=(const Broker_status& rhs) const;

    /////////////////////////////////////////////
    /// Broker_status members

    /// <summary>
    /// 
    /// </summary>
    std::string getState() const;
    void setState(std::string const& value);
    /// <summary>
    /// Progress of data collection (only available if receiving is running)
    /// </summary>
    float getProgress() const;
    void setProgress(float const value);
    bool progressIsSet() const;
    void unsetProgress();
    /// <summary>
    /// Message to display besides state of the jfjoch_broker; mostly used for errors and warnings This matters especially for async functions (start/initialize), where API won&#39;t return reason for the error during async operation. 
    /// </summary>
    std::string getMessage() const;
    void setMessage(std::string const& value);
    bool messageIsSet() const;
    void unsetMessage();
    /// <summary>
    /// Level of the message to display
    /// </summary>
    std::string getMessageSeverity() const;
    void setMessageSeverity(std::string const& value);
    bool messageSeverityIsSet() const;
    void unsetMessage_severity();

    friend  void to_json(nlohmann::json& j, const Broker_status& o);
    friend  void from_json(const nlohmann::json& j, Broker_status& o);
protected:
    std::string m_State;

    float m_Progress;
    bool m_ProgressIsSet;
    std::string m_Message;
    bool m_MessageIsSet;
    std::string m_Message_severity;
    bool m_Message_severityIsSet;
    
};

} // namespace org::openapitools::server::model

#endif /* Broker_status_H_ */
