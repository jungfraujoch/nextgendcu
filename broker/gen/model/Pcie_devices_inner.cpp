/**
* Jungfraujoch
* API to control Jungfraujoch developed by the Paul Scherrer Institute (Switzerland). Jungfraujoch is a data acquisition and analysis system for pixel array detectors, primarly PSI JUNGFRAU. Jungfraujoch uses FPGA boards to acquire data at high data rates. 
*
* The version of the OpenAPI document: 1.0.0-rc.31
* Contact: filip.leonarski@psi.ch
*
* NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
* https://openapi-generator.tech
* Do not edit the class manually.
*/


#include "Pcie_devices_inner.h"
#include "Helpers.h"

#include <sstream>

namespace org::openapitools::server::model
{

Pcie_devices_inner::Pcie_devices_inner()
{
    m_Blk = "";
    m_BlkIsSet = false;
    m_Ipv4 = "";
    m_Ipv4IsSet = false;
    
}

void Pcie_devices_inner::validate() const
{
    std::stringstream msg;
    if (!validate(msg))
    {
        throw org::openapitools::server::helpers::ValidationException(msg.str());
    }
}

bool Pcie_devices_inner::validate(std::stringstream& msg) const
{
    return validate(msg, "");
}

bool Pcie_devices_inner::validate(std::stringstream& msg, const std::string& pathPrefix) const
{
    bool success = true;
    const std::string _pathPrefix = pathPrefix.empty() ? "Pcie_devices_inner" : pathPrefix;

            
    return success;
}

bool Pcie_devices_inner::operator==(const Pcie_devices_inner& rhs) const
{
    return
    
    
    
    ((!blkIsSet() && !rhs.blkIsSet()) || (blkIsSet() && rhs.blkIsSet() && getBlk() == rhs.getBlk())) &&
    
    
    ((!ipv4IsSet() && !rhs.ipv4IsSet()) || (ipv4IsSet() && rhs.ipv4IsSet() && getIpv4() == rhs.getIpv4()))
    
    ;
}

bool Pcie_devices_inner::operator!=(const Pcie_devices_inner& rhs) const
{
    return !(*this == rhs);
}

void to_json(nlohmann::json& j, const Pcie_devices_inner& o)
{
    j = nlohmann::json::object();
    if(o.blkIsSet())
        j["blk"] = o.m_Blk;
    if(o.ipv4IsSet())
        j["ipv4"] = o.m_Ipv4;
    
}

void from_json(const nlohmann::json& j, Pcie_devices_inner& o)
{
    if(j.find("blk") != j.end())
    {
        j.at("blk").get_to(o.m_Blk);
        o.m_BlkIsSet = true;
    } 
    if(j.find("ipv4") != j.end())
    {
        j.at("ipv4").get_to(o.m_Ipv4);
        o.m_Ipv4IsSet = true;
    } 
    
}

std::string Pcie_devices_inner::getBlk() const
{
    return m_Blk;
}
void Pcie_devices_inner::setBlk(std::string const& value)
{
    m_Blk = value;
    m_BlkIsSet = true;
}
bool Pcie_devices_inner::blkIsSet() const
{
    return m_BlkIsSet;
}
void Pcie_devices_inner::unsetBlk()
{
    m_BlkIsSet = false;
}
std::string Pcie_devices_inner::getIpv4() const
{
    return m_Ipv4;
}
void Pcie_devices_inner::setIpv4(std::string const& value)
{
    m_Ipv4 = value;
    m_Ipv4IsSet = true;
}
bool Pcie_devices_inner::ipv4IsSet() const
{
    return m_Ipv4IsSet;
}
void Pcie_devices_inner::unsetIpv4()
{
    m_Ipv4IsSet = false;
}


} // namespace org::openapitools::server::model

