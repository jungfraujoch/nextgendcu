/**
* Jungfraujoch
* API to control Jungfraujoch developed by the Paul Scherrer Institute (Switzerland). Jungfraujoch is a data acquisition and analysis system for pixel array detectors, primarly PSI JUNGFRAU. Jungfraujoch uses FPGA boards to acquire data at high data rates. 
*
* The version of the OpenAPI document: 1.0.0-rc.31
* Contact: filip.leonarski@psi.ch
*
* NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
* https://openapi-generator.tech
* Do not edit the class manually.
*/


#include "Azim_int_settings.h"
#include "Helpers.h"

#include <sstream>

namespace org::openapitools::server::model
{

Azim_int_settings::Azim_int_settings()
{
    m_Polarization_factor = 0.0f;
    m_Polarization_factorIsSet = false;
    m_Solid_angle_corr = true;
    m_High_q_recipA = 0.0f;
    m_Low_q_recipA = 0.0f;
    m_Q_spacing = 0.0f;
    
}

void Azim_int_settings::validate() const
{
    std::stringstream msg;
    if (!validate(msg))
    {
        throw org::openapitools::server::helpers::ValidationException(msg.str());
    }
}

bool Azim_int_settings::validate(std::stringstream& msg) const
{
    return validate(msg, "");
}

bool Azim_int_settings::validate(std::stringstream& msg, const std::string& pathPrefix) const
{
    bool success = true;
    const std::string _pathPrefix = pathPrefix.empty() ? "Azim_int_settings" : pathPrefix;

         
    if (polarizationFactorIsSet())
    {
        const float& value = m_Polarization_factor;
        const std::string currentValuePath = _pathPrefix + ".polarizationFactor";
                
        
        if (value < static_cast<float>(-1.0))
        {
            success = false;
            msg << currentValuePath << ": must be greater than or equal to -1.0;";
        }
        if (value > static_cast<float>(1.0))
        {
            success = false;
            msg << currentValuePath << ": must be less than or equal to 1.0;";
        }

    }
                    
    return success;
}

bool Azim_int_settings::operator==(const Azim_int_settings& rhs) const
{
    return
    
    
    
    ((!polarizationFactorIsSet() && !rhs.polarizationFactorIsSet()) || (polarizationFactorIsSet() && rhs.polarizationFactorIsSet() && getPolarizationFactor() == rhs.getPolarizationFactor())) &&
    
    (isSolidAngleCorr() == rhs.isSolidAngleCorr())
     &&
    
    (getHighQRecipA() == rhs.getHighQRecipA())
     &&
    
    (getLowQRecipA() == rhs.getLowQRecipA())
     &&
    
    (getQSpacing() == rhs.getQSpacing())
    
    
    ;
}

bool Azim_int_settings::operator!=(const Azim_int_settings& rhs) const
{
    return !(*this == rhs);
}

void to_json(nlohmann::json& j, const Azim_int_settings& o)
{
    j = nlohmann::json::object();
    if(o.polarizationFactorIsSet())
        j["polarization_factor"] = o.m_Polarization_factor;
    j["solid_angle_corr"] = o.m_Solid_angle_corr;
    j["high_q_recipA"] = o.m_High_q_recipA;
    j["low_q_recipA"] = o.m_Low_q_recipA;
    j["q_spacing"] = o.m_Q_spacing;
    
}

void from_json(const nlohmann::json& j, Azim_int_settings& o)
{
    if(j.find("polarization_factor") != j.end())
    {
        j.at("polarization_factor").get_to(o.m_Polarization_factor);
        o.m_Polarization_factorIsSet = true;
    } 
    j.at("solid_angle_corr").get_to(o.m_Solid_angle_corr);
    j.at("high_q_recipA").get_to(o.m_High_q_recipA);
    j.at("low_q_recipA").get_to(o.m_Low_q_recipA);
    j.at("q_spacing").get_to(o.m_Q_spacing);
    
}

float Azim_int_settings::getPolarizationFactor() const
{
    return m_Polarization_factor;
}
void Azim_int_settings::setPolarizationFactor(float const value)
{
    m_Polarization_factor = value;
    m_Polarization_factorIsSet = true;
}
bool Azim_int_settings::polarizationFactorIsSet() const
{
    return m_Polarization_factorIsSet;
}
void Azim_int_settings::unsetPolarization_factor()
{
    m_Polarization_factorIsSet = false;
}
bool Azim_int_settings::isSolidAngleCorr() const
{
    return m_Solid_angle_corr;
}
void Azim_int_settings::setSolidAngleCorr(bool const value)
{
    m_Solid_angle_corr = value;
}
float Azim_int_settings::getHighQRecipA() const
{
    return m_High_q_recipA;
}
void Azim_int_settings::setHighQRecipA(float const value)
{
    m_High_q_recipA = value;
}
float Azim_int_settings::getLowQRecipA() const
{
    return m_Low_q_recipA;
}
void Azim_int_settings::setLowQRecipA(float const value)
{
    m_Low_q_recipA = value;
}
float Azim_int_settings::getQSpacing() const
{
    return m_Q_spacing;
}
void Azim_int_settings::setQSpacing(float const value)
{
    m_Q_spacing = value;
}


} // namespace org::openapitools::server::model

