/**
* Jungfraujoch
* API to control Jungfraujoch developed by the Paul Scherrer Institute (Switzerland). Jungfraujoch is a data acquisition and analysis system for pixel array detectors, primarly PSI JUNGFRAU. Jungfraujoch uses FPGA boards to acquire data at high data rates. 
*
* The version of the OpenAPI document: 1.0.0-rc.31
* Contact: filip.leonarski@psi.ch
*
* NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
* https://openapi-generator.tech
* Do not edit the class manually.
*/


#include "Jfjoch_statistics.h"
#include "Helpers.h"

#include <sstream>

namespace org::openapitools::server::model
{

Jfjoch_statistics::Jfjoch_statistics()
{
    m_DetectorIsSet = false;
    m_Detector_listIsSet = false;
    m_Detector_settingsIsSet = false;
    m_Image_format_settingsIsSet = false;
    m_Instrument_metadataIsSet = false;
    m_File_writer_settingsIsSet = false;
    m_Data_processing_settingsIsSet = false;
    m_MeasurementIsSet = false;
    m_BrokerIsSet = false;
    m_FpgaIsSet = false;
    m_CalibrationIsSet = false;
    m_Zeromq_previewIsSet = false;
    m_Zeromq_metadataIsSet = false;
    m_Pixel_maskIsSet = false;
    m_RoiIsSet = false;
    
}

void Jfjoch_statistics::validate() const
{
    std::stringstream msg;
    if (!validate(msg))
    {
        throw org::openapitools::server::helpers::ValidationException(msg.str());
    }
}

bool Jfjoch_statistics::validate(std::stringstream& msg) const
{
    return validate(msg, "");
}

bool Jfjoch_statistics::validate(std::stringstream& msg, const std::string& pathPrefix) const
{
    bool success = true;
    const std::string _pathPrefix = pathPrefix.empty() ? "Jfjoch_statistics" : pathPrefix;

                                             
    if (fpgaIsSet())
    {
        const std::vector<org::openapitools::server::model::Fpga_status_inner>& value = m_Fpga;
        const std::string currentValuePath = _pathPrefix + ".fpga";
                
        
        { // Recursive validation of array elements
            const std::string oldValuePath = currentValuePath;
            int i = 0;
            for (const org::openapitools::server::model::Fpga_status_inner& value : value)
            { 
                const std::string currentValuePath = oldValuePath + "[" + std::to_string(i) + "]";
                        
        success = value.validate(msg, currentValuePath + ".fpga") && success;
 
                i++;
            }
        }

    }
         
    if (calibrationIsSet())
    {
        const std::vector<org::openapitools::server::model::Calibration_statistics_inner>& value = m_Calibration;
        const std::string currentValuePath = _pathPrefix + ".calibration";
                
        
        { // Recursive validation of array elements
            const std::string oldValuePath = currentValuePath;
            int i = 0;
            for (const org::openapitools::server::model::Calibration_statistics_inner& value : value)
            { 
                const std::string currentValuePath = oldValuePath + "[" + std::to_string(i) + "]";
                        
        success = value.validate(msg, currentValuePath + ".calibration") && success;
 
                i++;
            }
        }

    }
                    
    return success;
}

bool Jfjoch_statistics::operator==(const Jfjoch_statistics& rhs) const
{
    return
    
    
    
    ((!detectorIsSet() && !rhs.detectorIsSet()) || (detectorIsSet() && rhs.detectorIsSet() && getDetector() == rhs.getDetector())) &&
    
    
    ((!detectorListIsSet() && !rhs.detectorListIsSet()) || (detectorListIsSet() && rhs.detectorListIsSet() && getDetectorList() == rhs.getDetectorList())) &&
    
    
    ((!detectorSettingsIsSet() && !rhs.detectorSettingsIsSet()) || (detectorSettingsIsSet() && rhs.detectorSettingsIsSet() && getDetectorSettings() == rhs.getDetectorSettings())) &&
    
    
    ((!imageFormatSettingsIsSet() && !rhs.imageFormatSettingsIsSet()) || (imageFormatSettingsIsSet() && rhs.imageFormatSettingsIsSet() && getImageFormatSettings() == rhs.getImageFormatSettings())) &&
    
    
    ((!instrumentMetadataIsSet() && !rhs.instrumentMetadataIsSet()) || (instrumentMetadataIsSet() && rhs.instrumentMetadataIsSet() && getInstrumentMetadata() == rhs.getInstrumentMetadata())) &&
    
    
    ((!fileWriterSettingsIsSet() && !rhs.fileWriterSettingsIsSet()) || (fileWriterSettingsIsSet() && rhs.fileWriterSettingsIsSet() && getFileWriterSettings() == rhs.getFileWriterSettings())) &&
    
    
    ((!dataProcessingSettingsIsSet() && !rhs.dataProcessingSettingsIsSet()) || (dataProcessingSettingsIsSet() && rhs.dataProcessingSettingsIsSet() && getDataProcessingSettings() == rhs.getDataProcessingSettings())) &&
    
    
    ((!measurementIsSet() && !rhs.measurementIsSet()) || (measurementIsSet() && rhs.measurementIsSet() && getMeasurement() == rhs.getMeasurement())) &&
    
    
    ((!brokerIsSet() && !rhs.brokerIsSet()) || (brokerIsSet() && rhs.brokerIsSet() && getBroker() == rhs.getBroker())) &&
    
    
    ((!fpgaIsSet() && !rhs.fpgaIsSet()) || (fpgaIsSet() && rhs.fpgaIsSet() && getFpga() == rhs.getFpga())) &&
    
    
    ((!calibrationIsSet() && !rhs.calibrationIsSet()) || (calibrationIsSet() && rhs.calibrationIsSet() && getCalibration() == rhs.getCalibration())) &&
    
    
    ((!zeromqPreviewIsSet() && !rhs.zeromqPreviewIsSet()) || (zeromqPreviewIsSet() && rhs.zeromqPreviewIsSet() && getZeromqPreview() == rhs.getZeromqPreview())) &&
    
    
    ((!zeromqMetadataIsSet() && !rhs.zeromqMetadataIsSet()) || (zeromqMetadataIsSet() && rhs.zeromqMetadataIsSet() && getZeromqMetadata() == rhs.getZeromqMetadata())) &&
    
    
    ((!pixelMaskIsSet() && !rhs.pixelMaskIsSet()) || (pixelMaskIsSet() && rhs.pixelMaskIsSet() && getPixelMask() == rhs.getPixelMask())) &&
    
    
    ((!roiIsSet() && !rhs.roiIsSet()) || (roiIsSet() && rhs.roiIsSet() && getRoi() == rhs.getRoi()))
    
    ;
}

bool Jfjoch_statistics::operator!=(const Jfjoch_statistics& rhs) const
{
    return !(*this == rhs);
}

void to_json(nlohmann::json& j, const Jfjoch_statistics& o)
{
    j = nlohmann::json::object();
    if(o.detectorIsSet())
        j["detector"] = o.m_Detector;
    if(o.detectorListIsSet())
        j["detector_list"] = o.m_Detector_list;
    if(o.detectorSettingsIsSet())
        j["detector_settings"] = o.m_Detector_settings;
    if(o.imageFormatSettingsIsSet())
        j["image_format_settings"] = o.m_Image_format_settings;
    if(o.instrumentMetadataIsSet())
        j["instrument_metadata"] = o.m_Instrument_metadata;
    if(o.fileWriterSettingsIsSet())
        j["file_writer_settings"] = o.m_File_writer_settings;
    if(o.dataProcessingSettingsIsSet())
        j["data_processing_settings"] = o.m_Data_processing_settings;
    if(o.measurementIsSet())
        j["measurement"] = o.m_Measurement;
    if(o.brokerIsSet())
        j["broker"] = o.m_Broker;
    if(o.fpgaIsSet() || !o.m_Fpga.empty())
        j["fpga"] = o.m_Fpga;
    if(o.calibrationIsSet() || !o.m_Calibration.empty())
        j["calibration"] = o.m_Calibration;
    if(o.zeromqPreviewIsSet())
        j["zeromq_preview"] = o.m_Zeromq_preview;
    if(o.zeromqMetadataIsSet())
        j["zeromq_metadata"] = o.m_Zeromq_metadata;
    if(o.pixelMaskIsSet())
        j["pixel_mask"] = o.m_Pixel_mask;
    if(o.roiIsSet())
        j["roi"] = o.m_Roi;
    
}

void from_json(const nlohmann::json& j, Jfjoch_statistics& o)
{
    if(j.find("detector") != j.end())
    {
        j.at("detector").get_to(o.m_Detector);
        o.m_DetectorIsSet = true;
    } 
    if(j.find("detector_list") != j.end())
    {
        j.at("detector_list").get_to(o.m_Detector_list);
        o.m_Detector_listIsSet = true;
    } 
    if(j.find("detector_settings") != j.end())
    {
        j.at("detector_settings").get_to(o.m_Detector_settings);
        o.m_Detector_settingsIsSet = true;
    } 
    if(j.find("image_format_settings") != j.end())
    {
        j.at("image_format_settings").get_to(o.m_Image_format_settings);
        o.m_Image_format_settingsIsSet = true;
    } 
    if(j.find("instrument_metadata") != j.end())
    {
        j.at("instrument_metadata").get_to(o.m_Instrument_metadata);
        o.m_Instrument_metadataIsSet = true;
    } 
    if(j.find("file_writer_settings") != j.end())
    {
        j.at("file_writer_settings").get_to(o.m_File_writer_settings);
        o.m_File_writer_settingsIsSet = true;
    } 
    if(j.find("data_processing_settings") != j.end())
    {
        j.at("data_processing_settings").get_to(o.m_Data_processing_settings);
        o.m_Data_processing_settingsIsSet = true;
    } 
    if(j.find("measurement") != j.end())
    {
        j.at("measurement").get_to(o.m_Measurement);
        o.m_MeasurementIsSet = true;
    } 
    if(j.find("broker") != j.end())
    {
        j.at("broker").get_to(o.m_Broker);
        o.m_BrokerIsSet = true;
    } 
    if(j.find("fpga") != j.end())
    {
        j.at("fpga").get_to(o.m_Fpga);
        o.m_FpgaIsSet = true;
    } 
    if(j.find("calibration") != j.end())
    {
        j.at("calibration").get_to(o.m_Calibration);
        o.m_CalibrationIsSet = true;
    } 
    if(j.find("zeromq_preview") != j.end())
    {
        j.at("zeromq_preview").get_to(o.m_Zeromq_preview);
        o.m_Zeromq_previewIsSet = true;
    } 
    if(j.find("zeromq_metadata") != j.end())
    {
        j.at("zeromq_metadata").get_to(o.m_Zeromq_metadata);
        o.m_Zeromq_metadataIsSet = true;
    } 
    if(j.find("pixel_mask") != j.end())
    {
        j.at("pixel_mask").get_to(o.m_Pixel_mask);
        o.m_Pixel_maskIsSet = true;
    } 
    if(j.find("roi") != j.end())
    {
        j.at("roi").get_to(o.m_Roi);
        o.m_RoiIsSet = true;
    } 
    
}

org::openapitools::server::model::Detector_status Jfjoch_statistics::getDetector() const
{
    return m_Detector;
}
void Jfjoch_statistics::setDetector(org::openapitools::server::model::Detector_status const& value)
{
    m_Detector = value;
    m_DetectorIsSet = true;
}
bool Jfjoch_statistics::detectorIsSet() const
{
    return m_DetectorIsSet;
}
void Jfjoch_statistics::unsetDetector()
{
    m_DetectorIsSet = false;
}
org::openapitools::server::model::Detector_list Jfjoch_statistics::getDetectorList() const
{
    return m_Detector_list;
}
void Jfjoch_statistics::setDetectorList(org::openapitools::server::model::Detector_list const& value)
{
    m_Detector_list = value;
    m_Detector_listIsSet = true;
}
bool Jfjoch_statistics::detectorListIsSet() const
{
    return m_Detector_listIsSet;
}
void Jfjoch_statistics::unsetDetector_list()
{
    m_Detector_listIsSet = false;
}
org::openapitools::server::model::Detector_settings Jfjoch_statistics::getDetectorSettings() const
{
    return m_Detector_settings;
}
void Jfjoch_statistics::setDetectorSettings(org::openapitools::server::model::Detector_settings const& value)
{
    m_Detector_settings = value;
    m_Detector_settingsIsSet = true;
}
bool Jfjoch_statistics::detectorSettingsIsSet() const
{
    return m_Detector_settingsIsSet;
}
void Jfjoch_statistics::unsetDetector_settings()
{
    m_Detector_settingsIsSet = false;
}
org::openapitools::server::model::Image_format_settings Jfjoch_statistics::getImageFormatSettings() const
{
    return m_Image_format_settings;
}
void Jfjoch_statistics::setImageFormatSettings(org::openapitools::server::model::Image_format_settings const& value)
{
    m_Image_format_settings = value;
    m_Image_format_settingsIsSet = true;
}
bool Jfjoch_statistics::imageFormatSettingsIsSet() const
{
    return m_Image_format_settingsIsSet;
}
void Jfjoch_statistics::unsetImage_format_settings()
{
    m_Image_format_settingsIsSet = false;
}
org::openapitools::server::model::Instrument_metadata Jfjoch_statistics::getInstrumentMetadata() const
{
    return m_Instrument_metadata;
}
void Jfjoch_statistics::setInstrumentMetadata(org::openapitools::server::model::Instrument_metadata const& value)
{
    m_Instrument_metadata = value;
    m_Instrument_metadataIsSet = true;
}
bool Jfjoch_statistics::instrumentMetadataIsSet() const
{
    return m_Instrument_metadataIsSet;
}
void Jfjoch_statistics::unsetInstrument_metadata()
{
    m_Instrument_metadataIsSet = false;
}
org::openapitools::server::model::File_writer_settings Jfjoch_statistics::getFileWriterSettings() const
{
    return m_File_writer_settings;
}
void Jfjoch_statistics::setFileWriterSettings(org::openapitools::server::model::File_writer_settings const& value)
{
    m_File_writer_settings = value;
    m_File_writer_settingsIsSet = true;
}
bool Jfjoch_statistics::fileWriterSettingsIsSet() const
{
    return m_File_writer_settingsIsSet;
}
void Jfjoch_statistics::unsetFile_writer_settings()
{
    m_File_writer_settingsIsSet = false;
}
org::openapitools::server::model::Spot_finding_settings Jfjoch_statistics::getDataProcessingSettings() const
{
    return m_Data_processing_settings;
}
void Jfjoch_statistics::setDataProcessingSettings(org::openapitools::server::model::Spot_finding_settings const& value)
{
    m_Data_processing_settings = value;
    m_Data_processing_settingsIsSet = true;
}
bool Jfjoch_statistics::dataProcessingSettingsIsSet() const
{
    return m_Data_processing_settingsIsSet;
}
void Jfjoch_statistics::unsetData_processing_settings()
{
    m_Data_processing_settingsIsSet = false;
}
org::openapitools::server::model::Measurement_statistics Jfjoch_statistics::getMeasurement() const
{
    return m_Measurement;
}
void Jfjoch_statistics::setMeasurement(org::openapitools::server::model::Measurement_statistics const& value)
{
    m_Measurement = value;
    m_MeasurementIsSet = true;
}
bool Jfjoch_statistics::measurementIsSet() const
{
    return m_MeasurementIsSet;
}
void Jfjoch_statistics::unsetMeasurement()
{
    m_MeasurementIsSet = false;
}
org::openapitools::server::model::Broker_status Jfjoch_statistics::getBroker() const
{
    return m_Broker;
}
void Jfjoch_statistics::setBroker(org::openapitools::server::model::Broker_status const& value)
{
    m_Broker = value;
    m_BrokerIsSet = true;
}
bool Jfjoch_statistics::brokerIsSet() const
{
    return m_BrokerIsSet;
}
void Jfjoch_statistics::unsetBroker()
{
    m_BrokerIsSet = false;
}
std::vector<org::openapitools::server::model::Fpga_status_inner> Jfjoch_statistics::getFpga() const
{
    return m_Fpga;
}
void Jfjoch_statistics::setFpga(std::vector<org::openapitools::server::model::Fpga_status_inner> const& value)
{
    m_Fpga = value;
    m_FpgaIsSet = true;
}
bool Jfjoch_statistics::fpgaIsSet() const
{
    return m_FpgaIsSet;
}
void Jfjoch_statistics::unsetFpga()
{
    m_FpgaIsSet = false;
}
std::vector<org::openapitools::server::model::Calibration_statistics_inner> Jfjoch_statistics::getCalibration() const
{
    return m_Calibration;
}
void Jfjoch_statistics::setCalibration(std::vector<org::openapitools::server::model::Calibration_statistics_inner> const& value)
{
    m_Calibration = value;
    m_CalibrationIsSet = true;
}
bool Jfjoch_statistics::calibrationIsSet() const
{
    return m_CalibrationIsSet;
}
void Jfjoch_statistics::unsetCalibration()
{
    m_CalibrationIsSet = false;
}
org::openapitools::server::model::Zeromq_preview_settings Jfjoch_statistics::getZeromqPreview() const
{
    return m_Zeromq_preview;
}
void Jfjoch_statistics::setZeromqPreview(org::openapitools::server::model::Zeromq_preview_settings const& value)
{
    m_Zeromq_preview = value;
    m_Zeromq_previewIsSet = true;
}
bool Jfjoch_statistics::zeromqPreviewIsSet() const
{
    return m_Zeromq_previewIsSet;
}
void Jfjoch_statistics::unsetZeromq_preview()
{
    m_Zeromq_previewIsSet = false;
}
org::openapitools::server::model::Zeromq_metadata_settings Jfjoch_statistics::getZeromqMetadata() const
{
    return m_Zeromq_metadata;
}
void Jfjoch_statistics::setZeromqMetadata(org::openapitools::server::model::Zeromq_metadata_settings const& value)
{
    m_Zeromq_metadata = value;
    m_Zeromq_metadataIsSet = true;
}
bool Jfjoch_statistics::zeromqMetadataIsSet() const
{
    return m_Zeromq_metadataIsSet;
}
void Jfjoch_statistics::unsetZeromq_metadata()
{
    m_Zeromq_metadataIsSet = false;
}
org::openapitools::server::model::Pixel_mask_statistics Jfjoch_statistics::getPixelMask() const
{
    return m_Pixel_mask;
}
void Jfjoch_statistics::setPixelMask(org::openapitools::server::model::Pixel_mask_statistics const& value)
{
    m_Pixel_mask = value;
    m_Pixel_maskIsSet = true;
}
bool Jfjoch_statistics::pixelMaskIsSet() const
{
    return m_Pixel_maskIsSet;
}
void Jfjoch_statistics::unsetPixel_mask()
{
    m_Pixel_maskIsSet = false;
}
org::openapitools::server::model::Roi_definitions Jfjoch_statistics::getRoi() const
{
    return m_Roi;
}
void Jfjoch_statistics::setRoi(org::openapitools::server::model::Roi_definitions const& value)
{
    m_Roi = value;
    m_RoiIsSet = true;
}
bool Jfjoch_statistics::roiIsSet() const
{
    return m_RoiIsSet;
}
void Jfjoch_statistics::unsetRoi()
{
    m_RoiIsSet = false;
}


} // namespace org::openapitools::server::model

