/**
* Jungfraujoch
* API to control Jungfraujoch developed by the Paul Scherrer Institute (Switzerland). Jungfraujoch is a data acquisition and analysis system for pixel array detectors, primarly PSI JUNGFRAU. Jungfraujoch uses FPGA boards to acquire data at high data rates. 
*
* The version of the OpenAPI document: 1.0.0-rc.31
* Contact: filip.leonarski@psi.ch
*
* NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
* https://openapi-generator.tech
* Do not edit the class manually.
*/


#include "Jfjoch_settings.h"
#include "Helpers.h"

#include <sstream>

namespace org::openapitools::server::model
{

Jfjoch_settings::Jfjoch_settings()
{
    m_PcieIsSet = false;
    m_ZeromqIsSet = false;
    m_InstrumentIsSet = false;
    m_File_writerIsSet = false;
    m_Detector_settingsIsSet = false;
    m_Azim_intIsSet = false;
    m_Image_formatIsSet = false;
    m_Image_buffer_MiB = 2048;
    m_Image_buffer_MiBIsSet = false;
    m_Receiver_threads = 64;
    m_Receiver_threadsIsSet = false;
    m_Numa_policy = "";
    m_Numa_policyIsSet = false;
    m_Frontend_directory = "";
    m_Zeromq_previewIsSet = false;
    m_Zeromq_metadataIsSet = false;
    
}

void Jfjoch_settings::validate() const
{
    std::stringstream msg;
    if (!validate(msg))
    {
        throw org::openapitools::server::helpers::ValidationException(msg.str());
    }
}

bool Jfjoch_settings::validate(std::stringstream& msg) const
{
    return validate(msg, "");
}

bool Jfjoch_settings::validate(std::stringstream& msg, const std::string& pathPrefix) const
{
    bool success = true;
    const std::string _pathPrefix = pathPrefix.empty() ? "Jfjoch_settings" : pathPrefix;

         
    if (pcieIsSet())
    {
        const std::vector<org::openapitools::server::model::Pcie_devices_inner>& value = m_Pcie;
        const std::string currentValuePath = _pathPrefix + ".pcie";
                
        
        { // Recursive validation of array elements
            const std::string oldValuePath = currentValuePath;
            int i = 0;
            for (const org::openapitools::server::model::Pcie_devices_inner& value : value)
            { 
                const std::string currentValuePath = oldValuePath + "[" + std::to_string(i) + "]";
                        
        success = value.validate(msg, currentValuePath + ".pcie") && success;
 
                i++;
            }
        }

    }
                     
    
    /* Detector */ {
        const std::vector<org::openapitools::server::model::Detector>& value = m_Detector;
        const std::string currentValuePath = _pathPrefix + ".detector";
                
        
        { // Recursive validation of array elements
            const std::string oldValuePath = currentValuePath;
            int i = 0;
            for (const org::openapitools::server::model::Detector& value : value)
            { 
                const std::string currentValuePath = oldValuePath + "[" + std::to_string(i) + "]";
                        
        success = value.validate(msg, currentValuePath + ".detector") && success;
 
                i++;
            }
        }

    }
                     
    if (imageBufferMiBIsSet())
    {
        const int32_t& value = m_Image_buffer_MiB;
        const std::string currentValuePath = _pathPrefix + ".imageBufferMiB";
                
        
        if (value < 128)
        {
            success = false;
            msg << currentValuePath << ": must be greater than or equal to 128;";
        }

    }
         
    if (receiverThreadsIsSet())
    {
        const int32_t& value = m_Receiver_threads;
        const std::string currentValuePath = _pathPrefix + ".receiverThreads";
                
        
        if (value < 1)
        {
            success = false;
            msg << currentValuePath << ": must be greater than or equal to 1;";
        }
        if (value > 512)
        {
            success = false;
            msg << currentValuePath << ": must be less than or equal to 512;";
        }

    }
                        
    return success;
}

bool Jfjoch_settings::operator==(const Jfjoch_settings& rhs) const
{
    return
    
    
    
    ((!pcieIsSet() && !rhs.pcieIsSet()) || (pcieIsSet() && rhs.pcieIsSet() && getPcie() == rhs.getPcie())) &&
    
    
    ((!zeromqIsSet() && !rhs.zeromqIsSet()) || (zeromqIsSet() && rhs.zeromqIsSet() && getZeromq() == rhs.getZeromq())) &&
    
    
    ((!instrumentIsSet() && !rhs.instrumentIsSet()) || (instrumentIsSet() && rhs.instrumentIsSet() && getInstrument() == rhs.getInstrument())) &&
    
    
    ((!fileWriterIsSet() && !rhs.fileWriterIsSet()) || (fileWriterIsSet() && rhs.fileWriterIsSet() && getFileWriter() == rhs.getFileWriter())) &&
    
    (getDetector() == rhs.getDetector())
     &&
    
    
    ((!detectorSettingsIsSet() && !rhs.detectorSettingsIsSet()) || (detectorSettingsIsSet() && rhs.detectorSettingsIsSet() && getDetectorSettings() == rhs.getDetectorSettings())) &&
    
    
    ((!azimIntIsSet() && !rhs.azimIntIsSet()) || (azimIntIsSet() && rhs.azimIntIsSet() && getAzimInt() == rhs.getAzimInt())) &&
    
    
    ((!imageFormatIsSet() && !rhs.imageFormatIsSet()) || (imageFormatIsSet() && rhs.imageFormatIsSet() && getImageFormat() == rhs.getImageFormat())) &&
    
    
    ((!imageBufferMiBIsSet() && !rhs.imageBufferMiBIsSet()) || (imageBufferMiBIsSet() && rhs.imageBufferMiBIsSet() && getImageBufferMiB() == rhs.getImageBufferMiB())) &&
    
    
    ((!receiverThreadsIsSet() && !rhs.receiverThreadsIsSet()) || (receiverThreadsIsSet() && rhs.receiverThreadsIsSet() && getReceiverThreads() == rhs.getReceiverThreads())) &&
    
    
    ((!numaPolicyIsSet() && !rhs.numaPolicyIsSet()) || (numaPolicyIsSet() && rhs.numaPolicyIsSet() && getNumaPolicy() == rhs.getNumaPolicy())) &&
    
    (getFrontendDirectory() == rhs.getFrontendDirectory())
     &&
    
    (getImagePusher() == rhs.getImagePusher())
     &&
    
    
    ((!zeromqPreviewIsSet() && !rhs.zeromqPreviewIsSet()) || (zeromqPreviewIsSet() && rhs.zeromqPreviewIsSet() && getZeromqPreview() == rhs.getZeromqPreview())) &&
    
    
    ((!zeromqMetadataIsSet() && !rhs.zeromqMetadataIsSet()) || (zeromqMetadataIsSet() && rhs.zeromqMetadataIsSet() && getZeromqMetadata() == rhs.getZeromqMetadata()))
    
    ;
}

bool Jfjoch_settings::operator!=(const Jfjoch_settings& rhs) const
{
    return !(*this == rhs);
}

void to_json(nlohmann::json& j, const Jfjoch_settings& o)
{
    j = nlohmann::json::object();
    if(o.pcieIsSet() || !o.m_Pcie.empty())
        j["pcie"] = o.m_Pcie;
    if(o.zeromqIsSet())
        j["zeromq"] = o.m_Zeromq;
    if(o.instrumentIsSet())
        j["instrument"] = o.m_Instrument;
    if(o.fileWriterIsSet())
        j["file_writer"] = o.m_File_writer;
    j["detector"] = o.m_Detector;
    if(o.detectorSettingsIsSet())
        j["detector_settings"] = o.m_Detector_settings;
    if(o.azimIntIsSet())
        j["azim_int"] = o.m_Azim_int;
    if(o.imageFormatIsSet())
        j["image_format"] = o.m_Image_format;
    if(o.imageBufferMiBIsSet())
        j["image_buffer_MiB"] = o.m_Image_buffer_MiB;
    if(o.receiverThreadsIsSet())
        j["receiver_threads"] = o.m_Receiver_threads;
    if(o.numaPolicyIsSet())
        j["numa_policy"] = o.m_Numa_policy;
    j["frontend_directory"] = o.m_Frontend_directory;
    j["image_pusher"] = o.m_Image_pusher;
    if(o.zeromqPreviewIsSet())
        j["zeromq_preview"] = o.m_Zeromq_preview;
    if(o.zeromqMetadataIsSet())
        j["zeromq_metadata"] = o.m_Zeromq_metadata;
    
}

void from_json(const nlohmann::json& j, Jfjoch_settings& o)
{
    if(j.find("pcie") != j.end())
    {
        j.at("pcie").get_to(o.m_Pcie);
        o.m_PcieIsSet = true;
    } 
    if(j.find("zeromq") != j.end())
    {
        j.at("zeromq").get_to(o.m_Zeromq);
        o.m_ZeromqIsSet = true;
    } 
    if(j.find("instrument") != j.end())
    {
        j.at("instrument").get_to(o.m_Instrument);
        o.m_InstrumentIsSet = true;
    } 
    if(j.find("file_writer") != j.end())
    {
        j.at("file_writer").get_to(o.m_File_writer);
        o.m_File_writerIsSet = true;
    } 
    j.at("detector").get_to(o.m_Detector);
    if(j.find("detector_settings") != j.end())
    {
        j.at("detector_settings").get_to(o.m_Detector_settings);
        o.m_Detector_settingsIsSet = true;
    } 
    if(j.find("azim_int") != j.end())
    {
        j.at("azim_int").get_to(o.m_Azim_int);
        o.m_Azim_intIsSet = true;
    } 
    if(j.find("image_format") != j.end())
    {
        j.at("image_format").get_to(o.m_Image_format);
        o.m_Image_formatIsSet = true;
    } 
    if(j.find("image_buffer_MiB") != j.end())
    {
        j.at("image_buffer_MiB").get_to(o.m_Image_buffer_MiB);
        o.m_Image_buffer_MiBIsSet = true;
    } 
    if(j.find("receiver_threads") != j.end())
    {
        j.at("receiver_threads").get_to(o.m_Receiver_threads);
        o.m_Receiver_threadsIsSet = true;
    } 
    if(j.find("numa_policy") != j.end())
    {
        j.at("numa_policy").get_to(o.m_Numa_policy);
        o.m_Numa_policyIsSet = true;
    } 
    j.at("frontend_directory").get_to(o.m_Frontend_directory);
    j.at("image_pusher").get_to(o.m_Image_pusher);
    if(j.find("zeromq_preview") != j.end())
    {
        j.at("zeromq_preview").get_to(o.m_Zeromq_preview);
        o.m_Zeromq_previewIsSet = true;
    } 
    if(j.find("zeromq_metadata") != j.end())
    {
        j.at("zeromq_metadata").get_to(o.m_Zeromq_metadata);
        o.m_Zeromq_metadataIsSet = true;
    } 
    
}

std::vector<org::openapitools::server::model::Pcie_devices_inner> Jfjoch_settings::getPcie() const
{
    return m_Pcie;
}
void Jfjoch_settings::setPcie(std::vector<org::openapitools::server::model::Pcie_devices_inner> const& value)
{
    m_Pcie = value;
    m_PcieIsSet = true;
}
bool Jfjoch_settings::pcieIsSet() const
{
    return m_PcieIsSet;
}
void Jfjoch_settings::unsetPcie()
{
    m_PcieIsSet = false;
}
org::openapitools::server::model::Zeromq_settings Jfjoch_settings::getZeromq() const
{
    return m_Zeromq;
}
void Jfjoch_settings::setZeromq(org::openapitools::server::model::Zeromq_settings const& value)
{
    m_Zeromq = value;
    m_ZeromqIsSet = true;
}
bool Jfjoch_settings::zeromqIsSet() const
{
    return m_ZeromqIsSet;
}
void Jfjoch_settings::unsetZeromq()
{
    m_ZeromqIsSet = false;
}
org::openapitools::server::model::Instrument_metadata Jfjoch_settings::getInstrument() const
{
    return m_Instrument;
}
void Jfjoch_settings::setInstrument(org::openapitools::server::model::Instrument_metadata const& value)
{
    m_Instrument = value;
    m_InstrumentIsSet = true;
}
bool Jfjoch_settings::instrumentIsSet() const
{
    return m_InstrumentIsSet;
}
void Jfjoch_settings::unsetInstrument()
{
    m_InstrumentIsSet = false;
}
org::openapitools::server::model::File_writer_settings Jfjoch_settings::getFileWriter() const
{
    return m_File_writer;
}
void Jfjoch_settings::setFileWriter(org::openapitools::server::model::File_writer_settings const& value)
{
    m_File_writer = value;
    m_File_writerIsSet = true;
}
bool Jfjoch_settings::fileWriterIsSet() const
{
    return m_File_writerIsSet;
}
void Jfjoch_settings::unsetFile_writer()
{
    m_File_writerIsSet = false;
}
std::vector<org::openapitools::server::model::Detector> Jfjoch_settings::getDetector() const
{
    return m_Detector;
}
void Jfjoch_settings::setDetector(std::vector<org::openapitools::server::model::Detector> const& value)
{
    m_Detector = value;
}
org::openapitools::server::model::Detector_settings Jfjoch_settings::getDetectorSettings() const
{
    return m_Detector_settings;
}
void Jfjoch_settings::setDetectorSettings(org::openapitools::server::model::Detector_settings const& value)
{
    m_Detector_settings = value;
    m_Detector_settingsIsSet = true;
}
bool Jfjoch_settings::detectorSettingsIsSet() const
{
    return m_Detector_settingsIsSet;
}
void Jfjoch_settings::unsetDetector_settings()
{
    m_Detector_settingsIsSet = false;
}
org::openapitools::server::model::Azim_int_settings Jfjoch_settings::getAzimInt() const
{
    return m_Azim_int;
}
void Jfjoch_settings::setAzimInt(org::openapitools::server::model::Azim_int_settings const& value)
{
    m_Azim_int = value;
    m_Azim_intIsSet = true;
}
bool Jfjoch_settings::azimIntIsSet() const
{
    return m_Azim_intIsSet;
}
void Jfjoch_settings::unsetAzim_int()
{
    m_Azim_intIsSet = false;
}
org::openapitools::server::model::Image_format_settings Jfjoch_settings::getImageFormat() const
{
    return m_Image_format;
}
void Jfjoch_settings::setImageFormat(org::openapitools::server::model::Image_format_settings const& value)
{
    m_Image_format = value;
    m_Image_formatIsSet = true;
}
bool Jfjoch_settings::imageFormatIsSet() const
{
    return m_Image_formatIsSet;
}
void Jfjoch_settings::unsetImage_format()
{
    m_Image_formatIsSet = false;
}
int32_t Jfjoch_settings::getImageBufferMiB() const
{
    return m_Image_buffer_MiB;
}
void Jfjoch_settings::setImageBufferMiB(int32_t const value)
{
    m_Image_buffer_MiB = value;
    m_Image_buffer_MiBIsSet = true;
}
bool Jfjoch_settings::imageBufferMiBIsSet() const
{
    return m_Image_buffer_MiBIsSet;
}
void Jfjoch_settings::unsetImage_buffer_MiB()
{
    m_Image_buffer_MiBIsSet = false;
}
int32_t Jfjoch_settings::getReceiverThreads() const
{
    return m_Receiver_threads;
}
void Jfjoch_settings::setReceiverThreads(int32_t const value)
{
    m_Receiver_threads = value;
    m_Receiver_threadsIsSet = true;
}
bool Jfjoch_settings::receiverThreadsIsSet() const
{
    return m_Receiver_threadsIsSet;
}
void Jfjoch_settings::unsetReceiver_threads()
{
    m_Receiver_threadsIsSet = false;
}
std::string Jfjoch_settings::getNumaPolicy() const
{
    return m_Numa_policy;
}
void Jfjoch_settings::setNumaPolicy(std::string const& value)
{
    m_Numa_policy = value;
    m_Numa_policyIsSet = true;
}
bool Jfjoch_settings::numaPolicyIsSet() const
{
    return m_Numa_policyIsSet;
}
void Jfjoch_settings::unsetNuma_policy()
{
    m_Numa_policyIsSet = false;
}
std::string Jfjoch_settings::getFrontendDirectory() const
{
    return m_Frontend_directory;
}
void Jfjoch_settings::setFrontendDirectory(std::string const& value)
{
    m_Frontend_directory = value;
}
org::openapitools::server::model::Image_pusher_type Jfjoch_settings::getImagePusher() const
{
    return m_Image_pusher;
}
void Jfjoch_settings::setImagePusher(org::openapitools::server::model::Image_pusher_type const& value)
{
    m_Image_pusher = value;
}
org::openapitools::server::model::Zeromq_preview_settings Jfjoch_settings::getZeromqPreview() const
{
    return m_Zeromq_preview;
}
void Jfjoch_settings::setZeromqPreview(org::openapitools::server::model::Zeromq_preview_settings const& value)
{
    m_Zeromq_preview = value;
    m_Zeromq_previewIsSet = true;
}
bool Jfjoch_settings::zeromqPreviewIsSet() const
{
    return m_Zeromq_previewIsSet;
}
void Jfjoch_settings::unsetZeromq_preview()
{
    m_Zeromq_previewIsSet = false;
}
org::openapitools::server::model::Zeromq_metadata_settings Jfjoch_settings::getZeromqMetadata() const
{
    return m_Zeromq_metadata;
}
void Jfjoch_settings::setZeromqMetadata(org::openapitools::server::model::Zeromq_metadata_settings const& value)
{
    m_Zeromq_metadata = value;
    m_Zeromq_metadataIsSet = true;
}
bool Jfjoch_settings::zeromqMetadataIsSet() const
{
    return m_Zeromq_metadataIsSet;
}
void Jfjoch_settings::unsetZeromq_metadata()
{
    m_Zeromq_metadataIsSet = false;
}


} // namespace org::openapitools::server::model

