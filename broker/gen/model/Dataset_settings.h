/**
* Jungfraujoch
* API to control Jungfraujoch developed by the Paul Scherrer Institute (Switzerland). Jungfraujoch is a data acquisition and analysis system for pixel array detectors, primarly PSI JUNGFRAU. Jungfraujoch uses FPGA boards to acquire data at high data rates. 
*
* The version of the OpenAPI document: 1.0.0-rc.31
* Contact: filip.leonarski@psi.ch
*
* NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
* https://openapi-generator.tech
* Do not edit the class manually.
*/
/*
 * Dataset_settings.h
 *
 * 
 */

#ifndef Dataset_settings_H_
#define Dataset_settings_H_


#include <nlohmann/json.hpp>
#include "Rotation_axis.h"
#include <string>
#include "Dataset_settings_unit_cell.h"
#include <nlohmann/json.hpp>

namespace org::openapitools::server::model
{

/// <summary>
/// 
/// </summary>
class  Dataset_settings
{
public:
    Dataset_settings();
    virtual ~Dataset_settings() = default;


    /// <summary>
    /// Validate the current data in the model. Throws a ValidationException on failure.
    /// </summary>
    void validate() const;

    /// <summary>
    /// Validate the current data in the model. Returns false on error and writes an error
    /// message into the given stringstream.
    /// </summary>
    bool validate(std::stringstream& msg) const;

    /// <summary>
    /// Helper overload for validate. Used when one model stores another model and calls it's validate.
    /// Not meant to be called outside that case.
    /// </summary>
    bool validate(std::stringstream& msg, const std::string& pathPrefix) const;

    bool operator==(const Dataset_settings& rhs) const;
    bool operator!=(const Dataset_settings& rhs) const;

    /////////////////////////////////////////////
    /// Dataset_settings members

    /// <summary>
    /// For standard synchrotron data collection - this is number of images collected per one TTL trigger For XFEL (pulsed source) - this number is ignored and set to 1 For storage cell mode - this number is ignored and set to number of storage cells 
    /// </summary>
    int64_t getImagesPerTrigger() const;
    void setImagesPerTrigger(int64_t const value);
    bool imagesPerTriggerIsSet() const;
    void unsetImages_per_trigger();
    /// <summary>
    /// Number of TTL trigger that the detector is expected to receive during data collection 
    /// </summary>
    int64_t getNtrigger() const;
    void setNtrigger(int64_t const value);
    bool ntriggerIsSet() const;
    void unsetNtrigger();
    /// <summary>
    /// Image time.  If not provided (or zero value) the frame time is assumed as default. Image time must be multiple of frame time; max value is 256 * frame_time.             In XFEL mode: summation happens for frames collected with multiple triggers. Ignored for storage cells and if raw data are saved. 
    /// </summary>
    int64_t getImageTimeUs() const;
    void setImageTimeUs(int64_t const value);
    bool imageTimeUsIsSet() const;
    void unsetImage_time_us();
    /// <summary>
    /// /entry/detector/beam_center_x in NXmx Beam center in X direction [pixels] 
    /// </summary>
    float getBeamXPxl() const;
    void setBeamXPxl(float const value);
    /// <summary>
    /// /entry/detector/beam_center_y in NXmx Beam center in X direction [pixels] 
    /// </summary>
    float getBeamYPxl() const;
    void setBeamYPxl(float const value);
    /// <summary>
    /// /entry/detector/distance in NXmx Detector distance [mm]
    /// </summary>
    float getDetectorDistanceMm() const;
    void setDetectorDistanceMm(float const value);
    /// <summary>
    /// Used to calculate /entry/beam/incident_wavelength in NXmx Incident particle (photon, electron) energy in keV 
    /// </summary>
    float getIncidentEnergyKeV() const;
    void setIncidentEnergyKeV(float const value);
    /// <summary>
    /// Prefix for filenames. If left empty, no file will be saved.
    /// </summary>
    std::string getFilePrefix() const;
    void setFilePrefix(std::string const& value);
    bool filePrefixIsSet() const;
    void unsetFile_prefix();
    /// <summary>
    /// Number of files in a single HDF5 data file (0 &#x3D; write all images to a single data file).
    /// </summary>
    int64_t getImagesPerFile() const;
    void setImagesPerFile(int64_t const value);
    bool imagesPerFileIsSet() const;
    void unsetImages_per_file();
    /// <summary>
    /// Number of space group for the crystal. Currently used solely as metadata, not relevant for image processing done in Jungfraujoch.
    /// </summary>
    int64_t getSpaceGroupNumber() const;
    void setSpaceGroupNumber(int64_t const value);
    bool spaceGroupNumberIsSet() const;
    void unsetSpace_group_number();
    /// <summary>
    /// /entry/sample/name in NXmx Sample name 
    /// </summary>
    std::string getSampleName() const;
    void setSampleName(std::string const& value);
    bool sampleNameIsSet() const;
    void unsetSample_name();
    /// <summary>
    /// Compression type for the images transferred over ZeroMQ and saved to HDF5 file. 
    /// </summary>
    std::string getCompression() const;
    void setCompression(std::string const& value);
    bool compressionIsSet() const;
    void unsetCompression();
    /// <summary>
    /// /entry/beam/total_flux in NXmx Flux incident on beam plane in photons per second. In other words this is the flux integrated over area. [photons/s] 
    /// </summary>
    float getTotalFlux() const;
    void setTotalFlux(float const value);
    bool totalFluxIsSet() const;
    void unsetTotal_flux();
    /// <summary>
    /// /entry/instrument/attenuator/attenuator_transmission Transmission of attenuator (filter) [no units] 
    /// </summary>
    float getTransmission() const;
    void setTransmission(float const value);
    bool transmissionIsSet() const;
    void unsetTransmission();
    /// <summary>
    /// 
    /// </summary>
    org::openapitools::server::model::Rotation_axis getGoniometer() const;
    void setGoniometer(org::openapitools::server::model::Rotation_axis const& value);
    bool goniometerIsSet() const;
    void unsetGoniometer();
    /// <summary>
    /// Header appendix, added as user_data/user to start ZeroMQ message (can be any valid JSON) In general, it is not saved in HDF5 file.  However, if values are placed in \&quot;hdf5\&quot; object, &#x60;jfjoch_writer&#x60; will write them in /entry/data of the HDF5 file.  This applies solely to string and number (double floating-point). No arrays/sub-objects is allowed. For example {\&quot;hdf5\&quot;: {\&quot;val1\&quot;:1, \&quot;val2\&quot;:\&quot;xyz\&quot;}}, will write /entry/user/val1 and /entry/user/val2. 
    /// </summary>
    nlohmann::json getHeaderAppendix() const;
    void setHeaderAppendix(nlohmann::json const& value);
    bool headerAppendixIsSet() const;
    void unsetHeader_appendix();
    /// <summary>
    /// Image appendix, added as user_data to image ZeroMQ message (can be any valid JSON) Not saved in HDF5 file 
    /// </summary>
    nlohmann::json getImageAppendix() const;
    void setImageAppendix(nlohmann::json const& value);
    bool imageAppendixIsSet() const;
    void unsetImage_appendix();
    /// <summary>
    /// Rate at which non-indexed images are accepted to be forwarded to writer.  Value of 1.0 (default) means that all images are written. Values below zero mean that non-indexed images will be accepted with a given probability. 
    /// </summary>
    float getDataReductionFactorSerialmx() const;
    void setDataReductionFactorSerialmx(float const value);
    bool dataReductionFactorSerialmxIsSet() const;
    void unsetData_reduction_factor_serialmx();
    /// <summary>
    /// Set all counts lower than the value to zero.  When the value is set, negative numbers other than error pixel value are always set to zero. Setting to zero is equivalent to turning the option off. 
    /// </summary>
    int64_t getPixelValueLowThreshold() const;
    void setPixelValueLowThreshold(int64_t const value);
    bool pixelValueLowThresholdIsSet() const;
    void unsetPixel_value_low_threshold();
    /// <summary>
    /// Number of run within an experimental session.  Transferred over CBOR stream as \&quot;series ID\&quot;, though not saved in HDF5 file. It is highly recommended to keep this number unique for each data collection during experimental series. If not provided, the number will be automatically incremented. 
    /// </summary>
    int64_t getRunNumber() const;
    void setRunNumber(int64_t const value);
    bool runNumberIsSet() const;
    void unsetRun_number();
    /// <summary>
    /// Unique ID of run. Transferred over CBOR stream as \&quot;unique series ID\&quot;, though not saved in HDF5 file. It is highly recommended to keep this name unique for each data collection during experimental series. If not provided, the name will be automatically generated as number + colon + file_prefix. 
    /// </summary>
    std::string getRunName() const;
    void setRunName(std::string const& value);
    bool runNameIsSet() const;
    void unsetRun_name();
    /// <summary>
    /// Name of group owning the data (e.g. p-group or proposal number).  Transferred over CBOR stream, though not saved in HDF5 file. 
    /// </summary>
    std::string getExperimentGroup() const;
    void setExperimentGroup(std::string const& value);
    bool experimentGroupIsSet() const;
    void unsetExperiment_group();
    /// <summary>
    /// Enable lossy compression of pixel values that preserves Poisson statistics.  Requires to provide a numerical factor SQ. Pixel value P will be transformed to round(sqrt(P) * SQ), with rounding to the closest integer. Compression is turned off if the value is missing or it is set to zero. 
    /// </summary>
    int64_t getPoissonCompression() const;
    void setPoissonCompression(int64_t const value);
    bool poissonCompressionIsSet() const;
    void unsetPoisson_compression();
    /// <summary>
    /// Write NXmx formatted HDF5 master file. Recommended to use for macromolecular crystallography experiments and to turn off for other experiments. 
    /// </summary>
    bool isWriteNxmxHdf5Master() const;
    void setWriteNxmxHdf5Master(bool const value);
    bool writeNxmxHdf5MasterIsSet() const;
    void unsetWrite_nxmx_hdf5_master();
    /// <summary>
    /// Forward image calibration (at the moment pedestal and pedestal RMS for JUNGFRAU) using the ZeroMQ stream to writer. If parameter is not provided calibration will be saved only if more than 4 images are recorded. 
    /// </summary>
    bool isSaveCalibration() const;
    void setSaveCalibration(bool const value);
    bool saveCalibrationIsSet() const;
    void unsetSave_calibration();
    /// <summary>
    /// 
    /// </summary>
    org::openapitools::server::model::Dataset_settings_unit_cell getUnitCell() const;
    void setUnitCell(org::openapitools::server::model::Dataset_settings_unit_cell const& value);
    bool unitCellIsSet() const;
    void unsetUnit_cell();

    friend  void to_json(nlohmann::json& j, const Dataset_settings& o);
    friend  void from_json(const nlohmann::json& j, Dataset_settings& o);
protected:
    int64_t m_Images_per_trigger;
    bool m_Images_per_triggerIsSet;
    int64_t m_Ntrigger;
    bool m_NtriggerIsSet;
    int64_t m_Image_time_us;
    bool m_Image_time_usIsSet;
    float m_Beam_x_pxl;

    float m_Beam_y_pxl;

    float m_Detector_distance_mm;

    float m_Incident_energy_keV;

    std::string m_File_prefix;
    bool m_File_prefixIsSet;
    int64_t m_Images_per_file;
    bool m_Images_per_fileIsSet;
    int64_t m_Space_group_number;
    bool m_Space_group_numberIsSet;
    std::string m_Sample_name;
    bool m_Sample_nameIsSet;
    std::string m_Compression;
    bool m_CompressionIsSet;
    float m_Total_flux;
    bool m_Total_fluxIsSet;
    float m_Transmission;
    bool m_TransmissionIsSet;
    org::openapitools::server::model::Rotation_axis m_Goniometer;
    bool m_GoniometerIsSet;
    nlohmann::json m_Header_appendix;
    bool m_Header_appendixIsSet;
    nlohmann::json m_Image_appendix;
    bool m_Image_appendixIsSet;
    float m_Data_reduction_factor_serialmx;
    bool m_Data_reduction_factor_serialmxIsSet;
    int64_t m_Pixel_value_low_threshold;
    bool m_Pixel_value_low_thresholdIsSet;
    int64_t m_Run_number;
    bool m_Run_numberIsSet;
    std::string m_Run_name;
    bool m_Run_nameIsSet;
    std::string m_Experiment_group;
    bool m_Experiment_groupIsSet;
    int64_t m_Poisson_compression;
    bool m_Poisson_compressionIsSet;
    bool m_Write_nxmx_hdf5_master;
    bool m_Write_nxmx_hdf5_masterIsSet;
    bool m_Save_calibration;
    bool m_Save_calibrationIsSet;
    org::openapitools::server::model::Dataset_settings_unit_cell m_Unit_cell;
    bool m_Unit_cellIsSet;
    
};

} // namespace org::openapitools::server::model

#endif /* Dataset_settings_H_ */
