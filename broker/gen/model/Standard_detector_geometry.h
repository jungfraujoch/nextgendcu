/**
* Jungfraujoch
* API to control Jungfraujoch developed by the Paul Scherrer Institute (Switzerland). Jungfraujoch is a data acquisition and analysis system for pixel array detectors, primarly PSI JUNGFRAU. Jungfraujoch uses FPGA boards to acquire data at high data rates. 
*
* The version of the OpenAPI document: 1.0.0-rc.31
* Contact: filip.leonarski@psi.ch
*
* NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
* https://openapi-generator.tech
* Do not edit the class manually.
*/
/*
 * Standard_detector_geometry.h
 *
 * Regular rectangular geometry, first module is in the bottom left corner of the detector
 */

#ifndef Standard_detector_geometry_H_
#define Standard_detector_geometry_H_


#include <nlohmann/json.hpp>

namespace org::openapitools::server::model
{

/// <summary>
/// Regular rectangular geometry, first module is in the bottom left corner of the detector
/// </summary>
class  Standard_detector_geometry
{
public:
    Standard_detector_geometry();
    virtual ~Standard_detector_geometry() = default;


    /// <summary>
    /// Validate the current data in the model. Throws a ValidationException on failure.
    /// </summary>
    void validate() const;

    /// <summary>
    /// Validate the current data in the model. Returns false on error and writes an error
    /// message into the given stringstream.
    /// </summary>
    bool validate(std::stringstream& msg) const;

    /// <summary>
    /// Helper overload for validate. Used when one model stores another model and calls it's validate.
    /// Not meant to be called outside that case.
    /// </summary>
    bool validate(std::stringstream& msg, const std::string& pathPrefix) const;

    bool operator==(const Standard_detector_geometry& rhs) const;
    bool operator!=(const Standard_detector_geometry& rhs) const;

    /////////////////////////////////////////////
    /// Standard_detector_geometry members

    /// <summary>
    /// Number of modules in the detector
    /// </summary>
    int32_t getNmodules() const;
    void setNmodules(int32_t const value);
    /// <summary>
    /// Gap size in X direction [pixels]
    /// </summary>
    int32_t getGapX() const;
    void setGapX(int32_t const value);
    bool gapXIsSet() const;
    void unsetGap_x();
    /// <summary>
    /// Gap size in Y direction [pixels]
    /// </summary>
    int32_t getGapY() const;
    void setGapY(int32_t const value);
    bool gapYIsSet() const;
    void unsetGap_y();
    /// <summary>
    /// Number of modules in one row
    /// </summary>
    int32_t getModulesInRow() const;
    void setModulesInRow(int32_t const value);
    bool modulesInRowIsSet() const;
    void unsetModules_in_row();

    friend  void to_json(nlohmann::json& j, const Standard_detector_geometry& o);
    friend  void from_json(const nlohmann::json& j, Standard_detector_geometry& o);
protected:
    int32_t m_Nmodules;

    int32_t m_Gap_x;
    bool m_Gap_xIsSet;
    int32_t m_Gap_y;
    bool m_Gap_yIsSet;
    int32_t m_Modules_in_row;
    bool m_Modules_in_rowIsSet;
    
};

} // namespace org::openapitools::server::model

#endif /* Standard_detector_geometry_H_ */
