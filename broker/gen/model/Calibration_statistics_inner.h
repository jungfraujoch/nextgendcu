/**
* Jungfraujoch
* API to control Jungfraujoch developed by the Paul Scherrer Institute (Switzerland). Jungfraujoch is a data acquisition and analysis system for pixel array detectors, primarly PSI JUNGFRAU. Jungfraujoch uses FPGA boards to acquire data at high data rates. 
*
* The version of the OpenAPI document: 1.0.0-rc.31
* Contact: filip.leonarski@psi.ch
*
* NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
* https://openapi-generator.tech
* Do not edit the class manually.
*/
/*
 * Calibration_statistics_inner.h
 *
 * 
 */

#ifndef Calibration_statistics_inner_H_
#define Calibration_statistics_inner_H_


#include <nlohmann/json.hpp>

namespace org::openapitools::server::model
{

/// <summary>
/// 
/// </summary>
class  Calibration_statistics_inner
{
public:
    Calibration_statistics_inner();
    virtual ~Calibration_statistics_inner() = default;


    /// <summary>
    /// Validate the current data in the model. Throws a ValidationException on failure.
    /// </summary>
    void validate() const;

    /// <summary>
    /// Validate the current data in the model. Returns false on error and writes an error
    /// message into the given stringstream.
    /// </summary>
    bool validate(std::stringstream& msg) const;

    /// <summary>
    /// Helper overload for validate. Used when one model stores another model and calls it's validate.
    /// Not meant to be called outside that case.
    /// </summary>
    bool validate(std::stringstream& msg, const std::string& pathPrefix) const;

    bool operator==(const Calibration_statistics_inner& rhs) const;
    bool operator!=(const Calibration_statistics_inner& rhs) const;

    /////////////////////////////////////////////
    /// Calibration_statistics_inner members

    /// <summary>
    /// 
    /// </summary>
    int64_t getModuleNumber() const;
    void setModuleNumber(int64_t const value);
    /// <summary>
    /// 
    /// </summary>
    int64_t getStorageCellNumber() const;
    void setStorageCellNumber(int64_t const value);
    /// <summary>
    /// 
    /// </summary>
    float getPedestalG0Mean() const;
    void setPedestalG0Mean(float const value);
    /// <summary>
    /// 
    /// </summary>
    float getPedestalG1Mean() const;
    void setPedestalG1Mean(float const value);
    /// <summary>
    /// 
    /// </summary>
    float getPedestalG2Mean() const;
    void setPedestalG2Mean(float const value);
    /// <summary>
    /// 
    /// </summary>
    float getGainG0Mean() const;
    void setGainG0Mean(float const value);
    /// <summary>
    /// 
    /// </summary>
    float getGainG1Mean() const;
    void setGainG1Mean(float const value);
    /// <summary>
    /// 
    /// </summary>
    float getGainG2Mean() const;
    void setGainG2Mean(float const value);
    /// <summary>
    /// 
    /// </summary>
    int64_t getMaskedPixels() const;
    void setMaskedPixels(int64_t const value);

    friend  void to_json(nlohmann::json& j, const Calibration_statistics_inner& o);
    friend  void from_json(const nlohmann::json& j, Calibration_statistics_inner& o);
protected:
    int64_t m_Module_number;

    int64_t m_Storage_cell_number;

    float m_Pedestal_g0_mean;

    float m_Pedestal_g1_mean;

    float m_Pedestal_g2_mean;

    float m_Gain_g0_mean;

    float m_Gain_g1_mean;

    float m_Gain_g2_mean;

    int64_t m_Masked_pixels;

    
};

} // namespace org::openapitools::server::model

#endif /* Calibration_statistics_inner_H_ */
