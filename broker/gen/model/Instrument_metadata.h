/**
* Jungfraujoch
* API to control Jungfraujoch developed by the Paul Scherrer Institute (Switzerland). Jungfraujoch is a data acquisition and analysis system for pixel array detectors, primarly PSI JUNGFRAU. Jungfraujoch uses FPGA boards to acquire data at high data rates. 
*
* The version of the OpenAPI document: 1.0.0-rc.31
* Contact: filip.leonarski@psi.ch
*
* NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
* https://openapi-generator.tech
* Do not edit the class manually.
*/
/*
 * Instrument_metadata.h
 *
 * Metadata for a measurement instrument
 */

#ifndef Instrument_metadata_H_
#define Instrument_metadata_H_


#include <string>
#include <nlohmann/json.hpp>

namespace org::openapitools::server::model
{

/// <summary>
/// Metadata for a measurement instrument
/// </summary>
class  Instrument_metadata
{
public:
    Instrument_metadata();
    virtual ~Instrument_metadata() = default;


    /// <summary>
    /// Validate the current data in the model. Throws a ValidationException on failure.
    /// </summary>
    void validate() const;

    /// <summary>
    /// Validate the current data in the model. Returns false on error and writes an error
    /// message into the given stringstream.
    /// </summary>
    bool validate(std::stringstream& msg) const;

    /// <summary>
    /// Helper overload for validate. Used when one model stores another model and calls it's validate.
    /// Not meant to be called outside that case.
    /// </summary>
    bool validate(std::stringstream& msg, const std::string& pathPrefix) const;

    bool operator==(const Instrument_metadata& rhs) const;
    bool operator!=(const Instrument_metadata& rhs) const;

    /////////////////////////////////////////////
    /// Instrument_metadata members

    /// <summary>
    /// 
    /// </summary>
    std::string getSourceName() const;
    void setSourceName(std::string const& value);
    /// <summary>
    /// Type of radiation source. NXmx gives a fixed dictionary, though Jungfraujoch is not enforcing compliance.  https://manual.nexusformat.org/classes/base_classes/NXsource.html#nxsource NXsource allows the following:  Spallation Neutron Source Pulsed Reactor Neutron Source Reactor Neutron Source Synchrotron X-ray Source Pulsed Muon Source Rotating Anode X-ray Fixed Tube X-ray UV Laser Free-Electron Laser Optical Laser Ion Source UV Plasma Source Metal Jet X-ray 
    /// </summary>
    std::string getSourceType() const;
    void setSourceType(std::string const& value);
    bool sourceTypeIsSet() const;
    void unsetSource_type();
    /// <summary>
    /// 
    /// </summary>
    std::string getInstrumentName() const;
    void setInstrumentName(std::string const& value);
    /// <summary>
    /// Settings specific to XFEL (e.g., every image has to come from TTL trigger, save pulse ID and event code) 
    /// </summary>
    bool isPulsedSource() const;
    void setPulsedSource(bool const value);
    bool pulsedSourceIsSet() const;
    void unsetPulsed_source();
    /// <summary>
    /// Settings specific to electron source (e.g., wavelength definition) 
    /// </summary>
    bool isElectronSource() const;
    void setElectronSource(bool const value);
    bool electronSourceIsSet() const;
    void unsetElectron_source();

    friend  void to_json(nlohmann::json& j, const Instrument_metadata& o);
    friend  void from_json(const nlohmann::json& j, Instrument_metadata& o);
protected:
    std::string m_Source_name;

    std::string m_Source_type;
    bool m_Source_typeIsSet;
    std::string m_Instrument_name;

    bool m_Pulsed_source;
    bool m_Pulsed_sourceIsSet;
    bool m_Electron_source;
    bool m_Electron_sourceIsSet;
    
};

} // namespace org::openapitools::server::model

#endif /* Instrument_metadata_H_ */
