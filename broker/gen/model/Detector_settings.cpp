/**
* Jungfraujoch
* API to control Jungfraujoch developed by the Paul Scherrer Institute (Switzerland). Jungfraujoch is a data acquisition and analysis system for pixel array detectors, primarly PSI JUNGFRAU. Jungfraujoch uses FPGA boards to acquire data at high data rates. 
*
* The version of the OpenAPI document: 1.0.0-rc.31
* Contact: filip.leonarski@psi.ch
*
* NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
* https://openapi-generator.tech
* Do not edit the class manually.
*/


#include "Detector_settings.h"
#include "Helpers.h"

#include <sstream>

namespace org::openapitools::server::model
{

Detector_settings::Detector_settings()
{
    m_Frame_time_us = 0L;
    m_Count_time_us = 0L;
    m_Count_time_usIsSet = false;
    m_Internal_frame_generator = false;
    m_Internal_frame_generatorIsSet = false;
    m_Internal_frame_generator_images = 1L;
    m_Internal_frame_generator_imagesIsSet = false;
    m_Detector_trigger_delay_ns = 0L;
    m_Detector_trigger_delay_nsIsSet = false;
    m_TimingIsSet = false;
    m_Eiger_threshold_keV = 0.0f;
    m_Eiger_threshold_keVIsSet = false;
    m_Eiger_bit_depth = 0L;
    m_Eiger_bit_depthIsSet = false;
    m_Jungfrau_pedestal_g0_frames = 2000L;
    m_Jungfrau_pedestal_g0_framesIsSet = false;
    m_Jungfrau_pedestal_g1_frames = 300L;
    m_Jungfrau_pedestal_g1_framesIsSet = false;
    m_Jungfrau_pedestal_g2_frames = 300L;
    m_Jungfrau_pedestal_g2_framesIsSet = false;
    m_Jungfrau_pedestal_min_image_count = 128L;
    m_Jungfrau_pedestal_min_image_countIsSet = false;
    m_Jungfrau_storage_cell_count = 1L;
    m_Jungfrau_storage_cell_countIsSet = false;
    m_Jungfrau_storage_cell_delay_ns = 5000L;
    m_Jungfrau_storage_cell_delay_nsIsSet = false;
    m_Jungfrau_fixed_gain_g1 = false;
    m_Jungfrau_fixed_gain_g1IsSet = false;
    m_Jungfrau_use_gain_hg0 = false;
    m_Jungfrau_use_gain_hg0IsSet = false;
    
}

void Detector_settings::validate() const
{
    std::stringstream msg;
    if (!validate(msg))
    {
        throw org::openapitools::server::helpers::ValidationException(msg.str());
    }
}

bool Detector_settings::validate(std::stringstream& msg) const
{
    return validate(msg, "");
}

bool Detector_settings::validate(std::stringstream& msg, const std::string& pathPrefix) const
{
    bool success = true;
    const std::string _pathPrefix = pathPrefix.empty() ? "Detector_settings" : pathPrefix;

         
    
    /* Frame_time_us */ {
        const int64_t& value = m_Frame_time_us;
        const std::string currentValuePath = _pathPrefix + ".frameTimeUs";
                
        
        if (value < 1ll)
        {
            success = false;
            msg << currentValuePath << ": must be greater than or equal to 1;";
        }

    }
                 
    if (internalFrameGeneratorImagesIsSet())
    {
        const int64_t& value = m_Internal_frame_generator_images;
        const std::string currentValuePath = _pathPrefix + ".internalFrameGeneratorImages";
                
        
        if (value < 1ll)
        {
            success = false;
            msg << currentValuePath << ": must be greater than or equal to 1;";
        }
        if (value > 64ll)
        {
            success = false;
            msg << currentValuePath << ": must be less than or equal to 64;";
        }

    }
         
    if (detectorTriggerDelayNsIsSet())
    {
        const int64_t& value = m_Detector_trigger_delay_ns;
        const std::string currentValuePath = _pathPrefix + ".detectorTriggerDelayNs";
                
        
        if (value < 0ll)
        {
            success = false;
            msg << currentValuePath << ": must be greater than or equal to 0;";
        }

    }
             
    if (eigerThresholdKeVIsSet())
    {
        const float& value = m_Eiger_threshold_keV;
        const std::string currentValuePath = _pathPrefix + ".eigerThresholdKeV";
                
        
        if (value < static_cast<float>(1.0))
        {
            success = false;
            msg << currentValuePath << ": must be greater than or equal to 1.0;";
        }
        if (value > static_cast<float>(100.0))
        {
            success = false;
            msg << currentValuePath << ": must be less than or equal to 100.0;";
        }

    }
             
    if (jungfrauPedestalG0FramesIsSet())
    {
        const int64_t& value = m_Jungfrau_pedestal_g0_frames;
        const std::string currentValuePath = _pathPrefix + ".jungfrauPedestalG0Frames";
                
        
        if (value < 0ll)
        {
            success = false;
            msg << currentValuePath << ": must be greater than or equal to 0;";
        }

    }
         
    if (jungfrauPedestalG1FramesIsSet())
    {
        const int64_t& value = m_Jungfrau_pedestal_g1_frames;
        const std::string currentValuePath = _pathPrefix + ".jungfrauPedestalG1Frames";
                
        
        if (value < 0ll)
        {
            success = false;
            msg << currentValuePath << ": must be greater than or equal to 0;";
        }

    }
         
    if (jungfrauPedestalG2FramesIsSet())
    {
        const int64_t& value = m_Jungfrau_pedestal_g2_frames;
        const std::string currentValuePath = _pathPrefix + ".jungfrauPedestalG2Frames";
                
        
        if (value < 0ll)
        {
            success = false;
            msg << currentValuePath << ": must be greater than or equal to 0;";
        }

    }
         
    if (jungfrauPedestalMinImageCountIsSet())
    {
        const int64_t& value = m_Jungfrau_pedestal_min_image_count;
        const std::string currentValuePath = _pathPrefix + ".jungfrauPedestalMinImageCount";
                
        
        if (value < 32ll)
        {
            success = false;
            msg << currentValuePath << ": must be greater than or equal to 32;";
        }

    }
         
    if (jungfrauStorageCellCountIsSet())
    {
        const int64_t& value = m_Jungfrau_storage_cell_count;
        const std::string currentValuePath = _pathPrefix + ".jungfrauStorageCellCount";
                
        
        if (value < 1ll)
        {
            success = false;
            msg << currentValuePath << ": must be greater than or equal to 1;";
        }
        if (value > 16ll)
        {
            success = false;
            msg << currentValuePath << ": must be less than or equal to 16;";
        }

    }
         
    if (jungfrauStorageCellDelayNsIsSet())
    {
        const int64_t& value = m_Jungfrau_storage_cell_delay_ns;
        const std::string currentValuePath = _pathPrefix + ".jungfrauStorageCellDelayNs";
                
        
        if (value < 2100ll)
        {
            success = false;
            msg << currentValuePath << ": must be greater than or equal to 2100;";
        }

    }
            
    return success;
}

bool Detector_settings::operator==(const Detector_settings& rhs) const
{
    return
    
    
    (getFrameTimeUs() == rhs.getFrameTimeUs())
     &&
    
    
    ((!countTimeUsIsSet() && !rhs.countTimeUsIsSet()) || (countTimeUsIsSet() && rhs.countTimeUsIsSet() && getCountTimeUs() == rhs.getCountTimeUs())) &&
    
    
    ((!internalFrameGeneratorIsSet() && !rhs.internalFrameGeneratorIsSet()) || (internalFrameGeneratorIsSet() && rhs.internalFrameGeneratorIsSet() && isInternalFrameGenerator() == rhs.isInternalFrameGenerator())) &&
    
    
    ((!internalFrameGeneratorImagesIsSet() && !rhs.internalFrameGeneratorImagesIsSet()) || (internalFrameGeneratorImagesIsSet() && rhs.internalFrameGeneratorImagesIsSet() && getInternalFrameGeneratorImages() == rhs.getInternalFrameGeneratorImages())) &&
    
    
    ((!detectorTriggerDelayNsIsSet() && !rhs.detectorTriggerDelayNsIsSet()) || (detectorTriggerDelayNsIsSet() && rhs.detectorTriggerDelayNsIsSet() && getDetectorTriggerDelayNs() == rhs.getDetectorTriggerDelayNs())) &&
    
    
    ((!timingIsSet() && !rhs.timingIsSet()) || (timingIsSet() && rhs.timingIsSet() && getTiming() == rhs.getTiming())) &&
    
    
    ((!eigerThresholdKeVIsSet() && !rhs.eigerThresholdKeVIsSet()) || (eigerThresholdKeVIsSet() && rhs.eigerThresholdKeVIsSet() && getEigerThresholdKeV() == rhs.getEigerThresholdKeV())) &&
    
    
    ((!eigerBitDepthIsSet() && !rhs.eigerBitDepthIsSet()) || (eigerBitDepthIsSet() && rhs.eigerBitDepthIsSet() && getEigerBitDepth() == rhs.getEigerBitDepth())) &&
    
    
    ((!jungfrauPedestalG0FramesIsSet() && !rhs.jungfrauPedestalG0FramesIsSet()) || (jungfrauPedestalG0FramesIsSet() && rhs.jungfrauPedestalG0FramesIsSet() && getJungfrauPedestalG0Frames() == rhs.getJungfrauPedestalG0Frames())) &&
    
    
    ((!jungfrauPedestalG1FramesIsSet() && !rhs.jungfrauPedestalG1FramesIsSet()) || (jungfrauPedestalG1FramesIsSet() && rhs.jungfrauPedestalG1FramesIsSet() && getJungfrauPedestalG1Frames() == rhs.getJungfrauPedestalG1Frames())) &&
    
    
    ((!jungfrauPedestalG2FramesIsSet() && !rhs.jungfrauPedestalG2FramesIsSet()) || (jungfrauPedestalG2FramesIsSet() && rhs.jungfrauPedestalG2FramesIsSet() && getJungfrauPedestalG2Frames() == rhs.getJungfrauPedestalG2Frames())) &&
    
    
    ((!jungfrauPedestalMinImageCountIsSet() && !rhs.jungfrauPedestalMinImageCountIsSet()) || (jungfrauPedestalMinImageCountIsSet() && rhs.jungfrauPedestalMinImageCountIsSet() && getJungfrauPedestalMinImageCount() == rhs.getJungfrauPedestalMinImageCount())) &&
    
    
    ((!jungfrauStorageCellCountIsSet() && !rhs.jungfrauStorageCellCountIsSet()) || (jungfrauStorageCellCountIsSet() && rhs.jungfrauStorageCellCountIsSet() && getJungfrauStorageCellCount() == rhs.getJungfrauStorageCellCount())) &&
    
    
    ((!jungfrauStorageCellDelayNsIsSet() && !rhs.jungfrauStorageCellDelayNsIsSet()) || (jungfrauStorageCellDelayNsIsSet() && rhs.jungfrauStorageCellDelayNsIsSet() && getJungfrauStorageCellDelayNs() == rhs.getJungfrauStorageCellDelayNs())) &&
    
    
    ((!jungfrauFixedGainG1IsSet() && !rhs.jungfrauFixedGainG1IsSet()) || (jungfrauFixedGainG1IsSet() && rhs.jungfrauFixedGainG1IsSet() && isJungfrauFixedGainG1() == rhs.isJungfrauFixedGainG1())) &&
    
    
    ((!jungfrauUseGainHg0IsSet() && !rhs.jungfrauUseGainHg0IsSet()) || (jungfrauUseGainHg0IsSet() && rhs.jungfrauUseGainHg0IsSet() && isJungfrauUseGainHg0() == rhs.isJungfrauUseGainHg0()))
    
    ;
}

bool Detector_settings::operator!=(const Detector_settings& rhs) const
{
    return !(*this == rhs);
}

void to_json(nlohmann::json& j, const Detector_settings& o)
{
    j = nlohmann::json::object();
    j["frame_time_us"] = o.m_Frame_time_us;
    if(o.countTimeUsIsSet())
        j["count_time_us"] = o.m_Count_time_us;
    if(o.internalFrameGeneratorIsSet())
        j["internal_frame_generator"] = o.m_Internal_frame_generator;
    if(o.internalFrameGeneratorImagesIsSet())
        j["internal_frame_generator_images"] = o.m_Internal_frame_generator_images;
    if(o.detectorTriggerDelayNsIsSet())
        j["detector_trigger_delay_ns"] = o.m_Detector_trigger_delay_ns;
    if(o.timingIsSet())
        j["timing"] = o.m_Timing;
    if(o.eigerThresholdKeVIsSet())
        j["eiger_threshold_keV"] = o.m_Eiger_threshold_keV;
    if(o.eigerBitDepthIsSet())
        j["eiger_bit_depth"] = o.m_Eiger_bit_depth;
    if(o.jungfrauPedestalG0FramesIsSet())
        j["jungfrau_pedestal_g0_frames"] = o.m_Jungfrau_pedestal_g0_frames;
    if(o.jungfrauPedestalG1FramesIsSet())
        j["jungfrau_pedestal_g1_frames"] = o.m_Jungfrau_pedestal_g1_frames;
    if(o.jungfrauPedestalG2FramesIsSet())
        j["jungfrau_pedestal_g2_frames"] = o.m_Jungfrau_pedestal_g2_frames;
    if(o.jungfrauPedestalMinImageCountIsSet())
        j["jungfrau_pedestal_min_image_count"] = o.m_Jungfrau_pedestal_min_image_count;
    if(o.jungfrauStorageCellCountIsSet())
        j["jungfrau_storage_cell_count"] = o.m_Jungfrau_storage_cell_count;
    if(o.jungfrauStorageCellDelayNsIsSet())
        j["jungfrau_storage_cell_delay_ns"] = o.m_Jungfrau_storage_cell_delay_ns;
    if(o.jungfrauFixedGainG1IsSet())
        j["jungfrau_fixed_gain_g1"] = o.m_Jungfrau_fixed_gain_g1;
    if(o.jungfrauUseGainHg0IsSet())
        j["jungfrau_use_gain_hg0"] = o.m_Jungfrau_use_gain_hg0;
    
}

void from_json(const nlohmann::json& j, Detector_settings& o)
{
    j.at("frame_time_us").get_to(o.m_Frame_time_us);
    if(j.find("count_time_us") != j.end())
    {
        j.at("count_time_us").get_to(o.m_Count_time_us);
        o.m_Count_time_usIsSet = true;
    } 
    if(j.find("internal_frame_generator") != j.end())
    {
        j.at("internal_frame_generator").get_to(o.m_Internal_frame_generator);
        o.m_Internal_frame_generatorIsSet = true;
    } 
    if(j.find("internal_frame_generator_images") != j.end())
    {
        j.at("internal_frame_generator_images").get_to(o.m_Internal_frame_generator_images);
        o.m_Internal_frame_generator_imagesIsSet = true;
    } 
    if(j.find("detector_trigger_delay_ns") != j.end())
    {
        j.at("detector_trigger_delay_ns").get_to(o.m_Detector_trigger_delay_ns);
        o.m_Detector_trigger_delay_nsIsSet = true;
    } 
    if(j.find("timing") != j.end())
    {
        j.at("timing").get_to(o.m_Timing);
        o.m_TimingIsSet = true;
    } 
    if(j.find("eiger_threshold_keV") != j.end())
    {
        j.at("eiger_threshold_keV").get_to(o.m_Eiger_threshold_keV);
        o.m_Eiger_threshold_keVIsSet = true;
    } 
    if(j.find("eiger_bit_depth") != j.end())
    {
        j.at("eiger_bit_depth").get_to(o.m_Eiger_bit_depth);
        o.m_Eiger_bit_depthIsSet = true;
    } 
    if(j.find("jungfrau_pedestal_g0_frames") != j.end())
    {
        j.at("jungfrau_pedestal_g0_frames").get_to(o.m_Jungfrau_pedestal_g0_frames);
        o.m_Jungfrau_pedestal_g0_framesIsSet = true;
    } 
    if(j.find("jungfrau_pedestal_g1_frames") != j.end())
    {
        j.at("jungfrau_pedestal_g1_frames").get_to(o.m_Jungfrau_pedestal_g1_frames);
        o.m_Jungfrau_pedestal_g1_framesIsSet = true;
    } 
    if(j.find("jungfrau_pedestal_g2_frames") != j.end())
    {
        j.at("jungfrau_pedestal_g2_frames").get_to(o.m_Jungfrau_pedestal_g2_frames);
        o.m_Jungfrau_pedestal_g2_framesIsSet = true;
    } 
    if(j.find("jungfrau_pedestal_min_image_count") != j.end())
    {
        j.at("jungfrau_pedestal_min_image_count").get_to(o.m_Jungfrau_pedestal_min_image_count);
        o.m_Jungfrau_pedestal_min_image_countIsSet = true;
    } 
    if(j.find("jungfrau_storage_cell_count") != j.end())
    {
        j.at("jungfrau_storage_cell_count").get_to(o.m_Jungfrau_storage_cell_count);
        o.m_Jungfrau_storage_cell_countIsSet = true;
    } 
    if(j.find("jungfrau_storage_cell_delay_ns") != j.end())
    {
        j.at("jungfrau_storage_cell_delay_ns").get_to(o.m_Jungfrau_storage_cell_delay_ns);
        o.m_Jungfrau_storage_cell_delay_nsIsSet = true;
    } 
    if(j.find("jungfrau_fixed_gain_g1") != j.end())
    {
        j.at("jungfrau_fixed_gain_g1").get_to(o.m_Jungfrau_fixed_gain_g1);
        o.m_Jungfrau_fixed_gain_g1IsSet = true;
    } 
    if(j.find("jungfrau_use_gain_hg0") != j.end())
    {
        j.at("jungfrau_use_gain_hg0").get_to(o.m_Jungfrau_use_gain_hg0);
        o.m_Jungfrau_use_gain_hg0IsSet = true;
    } 
    
}

int64_t Detector_settings::getFrameTimeUs() const
{
    return m_Frame_time_us;
}
void Detector_settings::setFrameTimeUs(int64_t const value)
{
    m_Frame_time_us = value;
}
int64_t Detector_settings::getCountTimeUs() const
{
    return m_Count_time_us;
}
void Detector_settings::setCountTimeUs(int64_t const value)
{
    m_Count_time_us = value;
    m_Count_time_usIsSet = true;
}
bool Detector_settings::countTimeUsIsSet() const
{
    return m_Count_time_usIsSet;
}
void Detector_settings::unsetCount_time_us()
{
    m_Count_time_usIsSet = false;
}
bool Detector_settings::isInternalFrameGenerator() const
{
    return m_Internal_frame_generator;
}
void Detector_settings::setInternalFrameGenerator(bool const value)
{
    m_Internal_frame_generator = value;
    m_Internal_frame_generatorIsSet = true;
}
bool Detector_settings::internalFrameGeneratorIsSet() const
{
    return m_Internal_frame_generatorIsSet;
}
void Detector_settings::unsetInternal_frame_generator()
{
    m_Internal_frame_generatorIsSet = false;
}
int64_t Detector_settings::getInternalFrameGeneratorImages() const
{
    return m_Internal_frame_generator_images;
}
void Detector_settings::setInternalFrameGeneratorImages(int64_t const value)
{
    m_Internal_frame_generator_images = value;
    m_Internal_frame_generator_imagesIsSet = true;
}
bool Detector_settings::internalFrameGeneratorImagesIsSet() const
{
    return m_Internal_frame_generator_imagesIsSet;
}
void Detector_settings::unsetInternal_frame_generator_images()
{
    m_Internal_frame_generator_imagesIsSet = false;
}
int64_t Detector_settings::getDetectorTriggerDelayNs() const
{
    return m_Detector_trigger_delay_ns;
}
void Detector_settings::setDetectorTriggerDelayNs(int64_t const value)
{
    m_Detector_trigger_delay_ns = value;
    m_Detector_trigger_delay_nsIsSet = true;
}
bool Detector_settings::detectorTriggerDelayNsIsSet() const
{
    return m_Detector_trigger_delay_nsIsSet;
}
void Detector_settings::unsetDetector_trigger_delay_ns()
{
    m_Detector_trigger_delay_nsIsSet = false;
}
org::openapitools::server::model::Detector_timing Detector_settings::getTiming() const
{
    return m_Timing;
}
void Detector_settings::setTiming(org::openapitools::server::model::Detector_timing const& value)
{
    m_Timing = value;
    m_TimingIsSet = true;
}
bool Detector_settings::timingIsSet() const
{
    return m_TimingIsSet;
}
void Detector_settings::unsetTiming()
{
    m_TimingIsSet = false;
}
float Detector_settings::getEigerThresholdKeV() const
{
    return m_Eiger_threshold_keV;
}
void Detector_settings::setEigerThresholdKeV(float const value)
{
    m_Eiger_threshold_keV = value;
    m_Eiger_threshold_keVIsSet = true;
}
bool Detector_settings::eigerThresholdKeVIsSet() const
{
    return m_Eiger_threshold_keVIsSet;
}
void Detector_settings::unsetEiger_threshold_keV()
{
    m_Eiger_threshold_keVIsSet = false;
}
int64_t Detector_settings::getEigerBitDepth() const
{
    return m_Eiger_bit_depth;
}
void Detector_settings::setEigerBitDepth(int64_t const value)
{
    m_Eiger_bit_depth = value;
    m_Eiger_bit_depthIsSet = true;
}
bool Detector_settings::eigerBitDepthIsSet() const
{
    return m_Eiger_bit_depthIsSet;
}
void Detector_settings::unsetEiger_bit_depth()
{
    m_Eiger_bit_depthIsSet = false;
}
int64_t Detector_settings::getJungfrauPedestalG0Frames() const
{
    return m_Jungfrau_pedestal_g0_frames;
}
void Detector_settings::setJungfrauPedestalG0Frames(int64_t const value)
{
    m_Jungfrau_pedestal_g0_frames = value;
    m_Jungfrau_pedestal_g0_framesIsSet = true;
}
bool Detector_settings::jungfrauPedestalG0FramesIsSet() const
{
    return m_Jungfrau_pedestal_g0_framesIsSet;
}
void Detector_settings::unsetJungfrau_pedestal_g0_frames()
{
    m_Jungfrau_pedestal_g0_framesIsSet = false;
}
int64_t Detector_settings::getJungfrauPedestalG1Frames() const
{
    return m_Jungfrau_pedestal_g1_frames;
}
void Detector_settings::setJungfrauPedestalG1Frames(int64_t const value)
{
    m_Jungfrau_pedestal_g1_frames = value;
    m_Jungfrau_pedestal_g1_framesIsSet = true;
}
bool Detector_settings::jungfrauPedestalG1FramesIsSet() const
{
    return m_Jungfrau_pedestal_g1_framesIsSet;
}
void Detector_settings::unsetJungfrau_pedestal_g1_frames()
{
    m_Jungfrau_pedestal_g1_framesIsSet = false;
}
int64_t Detector_settings::getJungfrauPedestalG2Frames() const
{
    return m_Jungfrau_pedestal_g2_frames;
}
void Detector_settings::setJungfrauPedestalG2Frames(int64_t const value)
{
    m_Jungfrau_pedestal_g2_frames = value;
    m_Jungfrau_pedestal_g2_framesIsSet = true;
}
bool Detector_settings::jungfrauPedestalG2FramesIsSet() const
{
    return m_Jungfrau_pedestal_g2_framesIsSet;
}
void Detector_settings::unsetJungfrau_pedestal_g2_frames()
{
    m_Jungfrau_pedestal_g2_framesIsSet = false;
}
int64_t Detector_settings::getJungfrauPedestalMinImageCount() const
{
    return m_Jungfrau_pedestal_min_image_count;
}
void Detector_settings::setJungfrauPedestalMinImageCount(int64_t const value)
{
    m_Jungfrau_pedestal_min_image_count = value;
    m_Jungfrau_pedestal_min_image_countIsSet = true;
}
bool Detector_settings::jungfrauPedestalMinImageCountIsSet() const
{
    return m_Jungfrau_pedestal_min_image_countIsSet;
}
void Detector_settings::unsetJungfrau_pedestal_min_image_count()
{
    m_Jungfrau_pedestal_min_image_countIsSet = false;
}
int64_t Detector_settings::getJungfrauStorageCellCount() const
{
    return m_Jungfrau_storage_cell_count;
}
void Detector_settings::setJungfrauStorageCellCount(int64_t const value)
{
    m_Jungfrau_storage_cell_count = value;
    m_Jungfrau_storage_cell_countIsSet = true;
}
bool Detector_settings::jungfrauStorageCellCountIsSet() const
{
    return m_Jungfrau_storage_cell_countIsSet;
}
void Detector_settings::unsetJungfrau_storage_cell_count()
{
    m_Jungfrau_storage_cell_countIsSet = false;
}
int64_t Detector_settings::getJungfrauStorageCellDelayNs() const
{
    return m_Jungfrau_storage_cell_delay_ns;
}
void Detector_settings::setJungfrauStorageCellDelayNs(int64_t const value)
{
    m_Jungfrau_storage_cell_delay_ns = value;
    m_Jungfrau_storage_cell_delay_nsIsSet = true;
}
bool Detector_settings::jungfrauStorageCellDelayNsIsSet() const
{
    return m_Jungfrau_storage_cell_delay_nsIsSet;
}
void Detector_settings::unsetJungfrau_storage_cell_delay_ns()
{
    m_Jungfrau_storage_cell_delay_nsIsSet = false;
}
bool Detector_settings::isJungfrauFixedGainG1() const
{
    return m_Jungfrau_fixed_gain_g1;
}
void Detector_settings::setJungfrauFixedGainG1(bool const value)
{
    m_Jungfrau_fixed_gain_g1 = value;
    m_Jungfrau_fixed_gain_g1IsSet = true;
}
bool Detector_settings::jungfrauFixedGainG1IsSet() const
{
    return m_Jungfrau_fixed_gain_g1IsSet;
}
void Detector_settings::unsetJungfrau_fixed_gain_g1()
{
    m_Jungfrau_fixed_gain_g1IsSet = false;
}
bool Detector_settings::isJungfrauUseGainHg0() const
{
    return m_Jungfrau_use_gain_hg0;
}
void Detector_settings::setJungfrauUseGainHg0(bool const value)
{
    m_Jungfrau_use_gain_hg0 = value;
    m_Jungfrau_use_gain_hg0IsSet = true;
}
bool Detector_settings::jungfrauUseGainHg0IsSet() const
{
    return m_Jungfrau_use_gain_hg0IsSet;
}
void Detector_settings::unsetJungfrau_use_gain_hg0()
{
    m_Jungfrau_use_gain_hg0IsSet = false;
}


} // namespace org::openapitools::server::model

