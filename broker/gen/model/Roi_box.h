/**
* Jungfraujoch
* API to control Jungfraujoch developed by the Paul Scherrer Institute (Switzerland). Jungfraujoch is a data acquisition and analysis system for pixel array detectors, primarly PSI JUNGFRAU. Jungfraujoch uses FPGA boards to acquire data at high data rates. 
*
* The version of the OpenAPI document: 1.0.0-rc.31
* Contact: filip.leonarski@psi.ch
*
* NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
* https://openapi-generator.tech
* Do not edit the class manually.
*/
/*
 * Roi_box.h
 *
 * Box ROI
 */

#ifndef Roi_box_H_
#define Roi_box_H_


#include <string>
#include <nlohmann/json.hpp>

namespace org::openapitools::server::model
{

/// <summary>
/// Box ROI
/// </summary>
class  Roi_box
{
public:
    Roi_box();
    virtual ~Roi_box() = default;


    /// <summary>
    /// Validate the current data in the model. Throws a ValidationException on failure.
    /// </summary>
    void validate() const;

    /// <summary>
    /// Validate the current data in the model. Returns false on error and writes an error
    /// message into the given stringstream.
    /// </summary>
    bool validate(std::stringstream& msg) const;

    /// <summary>
    /// Helper overload for validate. Used when one model stores another model and calls it's validate.
    /// Not meant to be called outside that case.
    /// </summary>
    bool validate(std::stringstream& msg, const std::string& pathPrefix) const;

    bool operator==(const Roi_box& rhs) const;
    bool operator!=(const Roi_box& rhs) const;

    /////////////////////////////////////////////
    /// Roi_box members

    /// <summary>
    /// Name for the ROI; used in the plots
    /// </summary>
    std::string getName() const;
    void setName(std::string const& value);
    /// <summary>
    /// Lower bound (inclusive) in X coordinate for the box
    /// </summary>
    int64_t getMinXPxl() const;
    void setMinXPxl(int64_t const value);
    /// <summary>
    /// Upper bound (inclusive) in X coordinate for the box
    /// </summary>
    int64_t getMaxXPxl() const;
    void setMaxXPxl(int64_t const value);
    /// <summary>
    /// Lower bound (inclusive) in Y coordinate for the box
    /// </summary>
    int64_t getMinYPxl() const;
    void setMinYPxl(int64_t const value);
    /// <summary>
    /// Upper bound (inclusive) in Y coordinate for the box
    /// </summary>
    int64_t getMaxYPxl() const;
    void setMaxYPxl(int64_t const value);

    friend  void to_json(nlohmann::json& j, const Roi_box& o);
    friend  void from_json(const nlohmann::json& j, Roi_box& o);
protected:
    std::string m_Name;

    int64_t m_Min_x_pxl;

    int64_t m_Max_x_pxl;

    int64_t m_Min_y_pxl;

    int64_t m_Max_y_pxl;

    
};

} // namespace org::openapitools::server::model

#endif /* Roi_box_H_ */
