// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_JFJOCHBROKERPARSER_H
#define JUNGFRAUJOCH_JFJOCHBROKERPARSER_H

#include "../common/DiffractionExperiment.h"
#include "../acquisition_device/AcquisitionDeviceGroup.h"
#include "../image_pusher/ImagePusher.h"
#include "../receiver/JFJochReceiverService.h"
#include "gen/model/Jfjoch_settings.h"

DetectorGeometry ParseStandardDetectorGeometry(const org::openapitools::server::model::Detector &j);
DetectorGeometry ParseCustomDetectorGeometry(const org::openapitools::server::model::Detector &j);
DetectorGeometry ParseDetectorGeometry(const org::openapitools::server::model::Detector &j);
DetectorSetup ParseDetectorSetup(const org::openapitools::server::model::Detector &j);

void ParseFacilityConfiguration(const org::openapitools::server::model::Jfjoch_settings &j, DiffractionExperiment &experiment);

std::unique_ptr<ImagePusher> ParseImagePusher(const org::openapitools::server::model::Jfjoch_settings &j);

void ParseAcquisitionDeviceGroup(const org::openapitools::server::model::Jfjoch_settings &input, AcquisitionDeviceGroup &aq_devices);
void ParseReceiverSettings(const org::openapitools::server::model::Jfjoch_settings &input, JFJochReceiverService &service);

#endif //JUNGFRAUJOCH_JFJOCHBROKERPARSER_H
