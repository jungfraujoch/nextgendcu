// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_OPENAPICONVERT_H
#define JFJOCH_OPENAPICONVERT_H

#include "gen/model/Spot_finding_settings.h"
#include "gen/model/Measurement_statistics.h"
#include "gen/model/Detector_settings.h"
#include "gen/model/Broker_status.h"

#include "gen/model/Calibration_statistics_inner.h"
#include "gen/model/Instrument_metadata.h"
#include "gen/model/Detector_list.h"
#include "gen/model/Detector_status.h"
#include "gen/model/Plots.h"
#include "gen/model/Azim_int_settings.h"
#include "gen/model/Roi_definitions.h"
#include "gen/model/Image_format_settings.h"
#include "gen/model/Preview_settings.h"
#include "gen/model/Dataset_settings.h"
#include "gen/model/Fpga_status_inner.h"
#include "gen/model/Pixel_mask_statistics.h"
#include "gen/model/Zeromq_preview_settings.h"
#include "gen/model/Zeromq_metadata_settings.h"
#include "gen/model/File_writer_settings.h"
#include "gen/model/Image_buffer_status.h"

#include "../common/DatasetSettings.h"
#include "../common/ImageFormatSettings.h"
#include "../image_analysis/SpotFindingSettings.h"
#include "JFJochStateMachine.h"
#include "../common/DetectorSettings.h"
#include "../jungfrau/JFCalibration.h"
#include "../common/InstrumentMetadata.h"

SpotFindingSettings Convert(const org::openapitools::server::model::Spot_finding_settings &input);
org::openapitools::server::model::Spot_finding_settings Convert(const SpotFindingSettings &input);

org::openapitools::server::model::Measurement_statistics Convert(const MeasurementStatistics &input);
DetectorSettings Convert(const org::openapitools::server::model::Detector_settings &input);

org::openapitools::server::model::Detector_settings Convert(const DetectorSettings &input);
org::openapitools::server::model::Broker_status Convert(const BrokerStatus& input);
org::openapitools::server::model::Calibration_statistics_inner Convert(const JFCalibrationModuleStatistics& input);
std::vector<org::openapitools::server::model::Calibration_statistics_inner> Convert(const std::vector<JFCalibrationModuleStatistics>& input);
org::openapitools::server::model::Instrument_metadata Convert(const InstrumentMetadata& input);

InstrumentMetadata Convert(const org::openapitools::server::model::Instrument_metadata &input);

org::openapitools::server::model::File_writer_settings Convert(const FileWriterSettings& input);
FileWriterSettings Convert(const org::openapitools::server::model::File_writer_settings &input);

org::openapitools::server::model::Detector_status Convert(const DetectorStatus &input);
org::openapitools::server::model::Detector_list Convert(const DetectorList &input);
org::openapitools::server::model::Plots Convert(const MultiLinePlot& input);
AzimuthalIntegrationSettings Convert(const org::openapitools::server::model::Azim_int_settings& input);
org::openapitools::server::model::Azim_int_settings Convert(const AzimuthalIntegrationSettings& settings);
ROIDefinition Convert(const org::openapitools::server::model::Roi_definitions& input);
org::openapitools::server::model::Roi_definitions Convert(const ROIDefinition &input);
PreviewJPEGSettings Convert(const org::openapitools::server::model::Preview_settings& input);
ImageFormatSettings Convert(const org::openapitools::server::model::Image_format_settings& input);
org::openapitools::server::model::Image_format_settings Convert(const ImageFormatSettings& input);
DatasetSettings Convert(const org::openapitools::server::model::Dataset_settings& input);
std::vector<org::openapitools::server::model::Fpga_status_inner> Convert(const std::vector<DeviceStatus> &input);
org::openapitools::server::model::Pixel_mask_statistics Convert(const PixelMaskStatistics& input);
org::openapitools::server::model::Image_buffer_status Convert(const ImageBufferStatus& input);

org::openapitools::server::model::Zeromq_preview_settings Convert(const ZMQPreviewSettings& settings);
ZMQPreviewSettings Convert(const org::openapitools::server::model::Zeromq_preview_settings& input);
org::openapitools::server::model::Zeromq_metadata_settings Convert(const ZMQMetadataSettings& settings);
ZMQMetadataSettings Convert(const org::openapitools::server::model::Zeromq_metadata_settings& input);

#endif //JFJOCH_OPENAPICONVERT_H
