// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "OpenAPIConvert.h"

// From https://en.cppreference.com/w/cpp/string/byte/tolower
std::string str_tolower(std::string s) {
    std::transform(s.begin(), s.end(), s.begin(), [](unsigned char c){ return std::tolower(c); });
    return s;
}

SpotFindingSettings Convert(const org::openapitools::server::model::Spot_finding_settings &input) {
    SpotFindingSettings ret{};
    ret.signal_to_noise_threshold = input.getSignalToNoiseThreshold();
    ret.photon_count_threshold = input.getPhotonCountThreshold();
    ret.min_pix_per_spot = input.getMinPixPerSpot();
    ret.max_pix_per_spot = input.getMaxPixPerSpot();
    ret.high_resolution_limit = input.getHighResolutionLimit();
    ret.low_resolution_limit = input.getLowResolutionLimit();
    ret.enable = input.isEnable();
    ret.indexing = input.isIndexing();
    ret.indexing_tolerance = input.getIndexingTolerance();
    if (input.filterPowderRingsIsSet())
        ret.filter_spots_powder_ring = input.isFilterPowderRings();
    if (input.minSpotCountPowderRingIsSet())
        ret.min_spot_count_powder_ring = input.getMinSpotCountPowderRing();
    return ret;
}

org::openapitools::server::model::Spot_finding_settings Convert(const SpotFindingSettings &input) {
    org::openapitools::server::model::Spot_finding_settings ret;
    ret.setSignalToNoiseThreshold(input.signal_to_noise_threshold);
    ret.setPhotonCountThreshold(input.photon_count_threshold);
    ret.setMinPixPerSpot(input.min_pix_per_spot);
    ret.setMaxPixPerSpot(input.max_pix_per_spot);
    ret.setHighResolutionLimit(input.high_resolution_limit);
    ret.setLowResolutionLimit(input.low_resolution_limit);
    ret.setEnable(input.enable);
    ret.setIndexing(input.indexing);
    ret.setIndexingTolerance(input.indexing_tolerance);
    ret.setFilterPowderRings(input.filter_spots_powder_ring);
    ret.setMinSpotCountPowderRing(input.min_spot_count_powder_ring);
    return ret;
}

org::openapitools::server::model::Measurement_statistics Convert(const MeasurementStatistics &input) {
    org::openapitools::server::model::Measurement_statistics ret{};

    if (!input.file_prefix.empty())
        ret.setFilePrefix(input.file_prefix);

    ret.setExperimentGroup(input.experiment_group);
    ret.setImagesExpected(input.images_expected);
    ret.setImagesCollected(input.images_collected);
    ret.setImagesSent(input.images_sent);
    ret.setImagesDiscardedLossyCompression(input.images_skipped);
    ret.setMaxImageNumberSent(input.max_image_number_sent);
    if (input.collection_efficiency)
        ret.setCollectionEfficiency(input.collection_efficiency.value());
    if (input.compression_ratio)
        ret.setCompressionRatio(input.compression_ratio.value());

    ret.setCancelled(input.cancelled);
    ret.setMaxReceiverDelay(input.max_receive_delay);

    ret.setDetectorWidth(input.detector_width);
    ret.setDetectorHeight(input.detector_height);
    ret.setDetectorPixelDepth(input.detector_pixel_depth);

    if (input.roi_beam_npixel)
        ret.setRoiBeamPixels(input.roi_beam_npixel.value());
    if (input.roi_beam_sum)
        ret.setRoiBeamSum(input.roi_beam_sum.value());

    if (input.error_pixels)
        ret.setErrorPixels(input.error_pixels.value());
    if (input.saturated_pixels)
        ret.setSaturatedPixels(input.saturated_pixels.value());

    if (input.indexing_rate)
        ret.setIndexingRate(input.indexing_rate.value());

    if (input.bkg_estimate)
        ret.setBkgEstimate(input.bkg_estimate.value());
    ret.setUnitCell(input.unit_cell);
    ret.setRunNumber(input.run_number);
    
    
    return ret;
}

DetectorTiming Convert(const org::openapitools::server::model::Detector_timing& input) {
    switch (input.getValue()) {
        case org::openapitools::server::model::Detector_timing::eDetector_timing::AUTO:
            return DetectorTiming::Auto;
        case org::openapitools::server::model::Detector_timing::eDetector_timing::TRIGGER:
            return DetectorTiming::Trigger;
        case org::openapitools::server::model::Detector_timing::eDetector_timing::BURST:
            return DetectorTiming::Burst;
        case org::openapitools::server::model::Detector_timing::eDetector_timing::GATED:
            return DetectorTiming::Gated;
        default:
        case org::openapitools::server::model::Detector_timing::eDetector_timing::INVALID_VALUE_OPENAPI_GENERATED:
            throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "invalid input");
    }
}

org::openapitools::server::model::Detector_timing Convert(DetectorTiming input) {
    org::openapitools::server::model::Detector_timing val;
    switch (input) {

        case DetectorTiming::Auto:
            val.setValue(org::openapitools::server::model::Detector_timing::eDetector_timing::AUTO);
            break;
        case DetectorTiming::Trigger:
            val.setValue(org::openapitools::server::model::Detector_timing::eDetector_timing::TRIGGER);
            break;
        case DetectorTiming::Burst:
            val.setValue(org::openapitools::server::model::Detector_timing::eDetector_timing::BURST);
            break;
        case DetectorTiming::Gated:
            val.setValue(org::openapitools::server::model::Detector_timing::eDetector_timing::GATED);
            break;
        default:
            throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "invalid input");
    }
    return val;
}

DetectorSettings Convert(const org::openapitools::server::model::Detector_settings &input) {
    DetectorSettings ret{};

    if (input.countTimeUsIsSet())
        ret.FrameTime(std::chrono::microseconds(input.getFrameTimeUs()),
                      std::chrono::microseconds(input.getCountTimeUs()));
    else
        ret.FrameTime(std::chrono::microseconds(input.getFrameTimeUs()));

    ret.InternalGeneratorEnable(input.isInternalFrameGenerator());
    ret.InternalGeneratorImages(input.getInternalFrameGeneratorImages());
    ret.DetectorDelay(std::chrono::nanoseconds(input.getDetectorTriggerDelayNs()));

    ret.StorageCells(input.getJungfrauStorageCellCount());
    ret.StorageCellDelay(std::chrono::nanoseconds(input.getJungfrauStorageCellDelayNs()));

    ret.FixGainG1(input.isJungfrauFixedGainG1());
    ret.UseGainHG0(input.isJungfrauUseGainHg0());

    ret.PedestalG0Frames(input.getJungfrauPedestalG0Frames());
    ret.PedestalG1Frames(input.getJungfrauPedestalG1Frames());
    ret.PedestalG2Frames(input.getJungfrauPedestalG2Frames());
    ret.PedestalMinImageCount(input.getJungfrauPedestalMinImageCount());

    if (input.eigerBitDepthIsSet())
            ret.EigerBitDepth(input.getEigerBitDepth());

    if (input.eigerThresholdKeVIsSet())
        ret.EigerThreshold_keV(input.getEigerThresholdKeV());

    if (input.timingIsSet())
        ret.Timing(Convert(input.getTiming()));
    else
        ret.Timing(DetectorTiming::Trigger);
    return ret;
}

org::openapitools::server::model::Detector_settings Convert(const DetectorSettings &input) {
    org::openapitools::server::model::Detector_settings ret{};

    ret.setFrameTimeUs(input.GetFrameTime().count());
    if (input.GetCountTime().has_value())
        ret.setCountTimeUs(input.GetCountTime()->count());
    ret.setDetectorTriggerDelayNs(input.GetDetectorDelay().count());
    ret.setInternalFrameGeneratorImages(input.GetInternalGeneratorImages());
    ret.setInternalFrameGenerator(input.IsInternalGeneratorEnable());

    ret.setJungfrauStorageCellCount(input.GetStorageCells());
    ret.setJungfrauFixedGainG1(input.IsFixGainG1());
    ret.setJungfrauUseGainHg0(input.IsUseGainHG0());

    ret.setJungfrauPedestalG0Frames(input.GetPedestalG0Frames());
    ret.setJungfrauPedestalG1Frames(input.GetPedestalG1Frames());
    ret.setJungfrauPedestalG2Frames(input.GetPedestalG2Frames());
    ret.setJungfrauPedestalMinImageCount(input.GetPedestalMinImageCount());

    ret.setJungfrauStorageCellDelayNs(input.GetStorageCellDelay().count());
    if (input.GetEigerThreshold_keV().has_value())
        ret.setEigerThresholdKeV(input.GetEigerThreshold_keV().value());
    if (input.GetEigerBitDepth().has_value())
        ret.setEigerBitDepth(input.GetEigerBitDepth().value());

    ret.setTiming(Convert(input.GetTiming()));
    return ret;
}

org::openapitools::server::model::Broker_status Convert(const BrokerStatus& input) {
    org::openapitools::server::model::Broker_status ret;

    switch (input.state) {
        case JFJochState::Inactive:
            ret.setState("Inactive");
            break;
        case JFJochState::Idle:
            ret.setState("Idle");
            break;
        case JFJochState::Measuring:
            ret.setState("Measuring");
            break;
        case JFJochState::Error:
            ret.setState("Error");
            break;
        case JFJochState::Busy:
            ret.setState("Busy");
            break;
        case JFJochState::Pedestal:
            ret.setState("Pedestal");
            break;
    }

    if (input.message.has_value())
        ret.setMessage(input.message.value());

    switch (input.message_severity) {
        case BrokerStatus::MessageSeverity::Info:
            ret.setMessageSeverity("info");
            break;
        case BrokerStatus::MessageSeverity::Success:
            ret.setMessageSeverity("success");
            break;
        case BrokerStatus::MessageSeverity::Warning:
            ret.setMessageSeverity("warning");
            break;
        default:
            ret.setMessageSeverity("error");
            break;
    }
    if (input.progress.has_value())
        ret.setProgress(input.progress.value());

    return ret;
}

org::openapitools::server::model::Calibration_statistics_inner Convert(const JFCalibrationModuleStatistics& input) {
    org::openapitools::server::model::Calibration_statistics_inner output;

    output.setModuleNumber(input.module_number);
    output.setMaskedPixels(input.bad_pixels);
    output.setStorageCellNumber(input.storage_cell_number);
    output.setGainG0Mean(input.gain_g0_mean);
    output.setGainG1Mean(input.gain_g1_mean);
    output.setGainG2Mean(input.gain_g2_mean);
    output.setPedestalG0Mean(input.pedestal_g0_mean);
    output.setPedestalG1Mean(input.pedestal_g1_mean);
    output.setPedestalG2Mean(input.pedestal_g2_mean);

    return output;
}

std::vector<org::openapitools::server::model::Calibration_statistics_inner> Convert(const std::vector<JFCalibrationModuleStatistics>& input) {
    std::vector<org::openapitools::server::model::Calibration_statistics_inner> ret;
    for (const auto &i: input)
        ret.push_back(Convert(i));
    return ret;
}


org::openapitools::server::model::Instrument_metadata Convert(const InstrumentMetadata& input) {
    org::openapitools::server::model::Instrument_metadata output;
    output.setInstrumentName(input.GetInstrumentName());
    output.setSourceName(input.GetSourceName());
    output.setSourceType(input.GetSourceType());
    output.setPulsedSource(input.IsPulsedSource());
    output.setElectronSource(input.IsElectronSource());
    return output;
}

InstrumentMetadata Convert(const org::openapitools::server::model::Instrument_metadata &input) {
    InstrumentMetadata output;
    output.InstrumentName(input.getInstrumentName())
            .SourceName(input.getSourceName())
            .SourceType(input.getSourceType())
            .PulsedSource(input.isPulsedSource())
            .ElectronSource(input.isElectronSource());
    return output;
}


org::openapitools::server::model::Detector_state Convert(DetectorState input) {
    org::openapitools::server::model::Detector_state ret;
    switch (input) {
        case DetectorState::IDLE:
            ret.setValue(org::openapitools::server::model::Detector_state::eDetector_state::IDLE);
            break;
        case DetectorState::ERROR:
            ret.setValue(org::openapitools::server::model::Detector_state::eDetector_state::ERROR);
            break;
        case DetectorState::BUSY:
            ret.setValue(org::openapitools::server::model::Detector_state::eDetector_state::BUSY);
            break;
        case DetectorState::WAITING:
            ret.setValue(org::openapitools::server::model::Detector_state::eDetector_state::WAITING);
            break;
        default:
        case DetectorState::NOT_CONNECTED:
            ret.setValue(org::openapitools::server::model::Detector_state::eDetector_state::NOT_CONNECTED);
            break;
    }
    return ret;
}

org::openapitools::server::model::Detector_power_state Convert(DetectorPowerState input) {
    org::openapitools::server::model::Detector_power_state ret;
    switch (input) {
        case DetectorPowerState::ON:
            ret.setValue(org::openapitools::server::model::Detector_power_state::eDetector_power_state::POWERON);
            break;
        case DetectorPowerState::PARTIAL:
            ret.setValue(org::openapitools::server::model::Detector_power_state::eDetector_power_state::PARTIAL);
            break;
        default:
        case DetectorPowerState::OFF:
            ret.setValue(org::openapitools::server::model::Detector_power_state::eDetector_power_state::POWEROFF);
            break;
    }
    return ret;
}

org::openapitools::server::model::Detector_status Convert(const DetectorStatus &input) {
    org::openapitools::server::model::Detector_status output;
    output.setServerVersion(input.detector_server_version);
    output.setNumberOfTriggersLeft(input.remaining_triggers);
    output.setFpgaTempDegC(input.temperature_fpga_degC);
    output.setHighVoltageV(input.high_voltage_V);
    output.setPowerchip(Convert(input.power_state));
    output.setState(Convert(input.detector_state));
    return output;
}

org::openapitools::server::model::Detector_list Convert(const DetectorList &input) {
    org::openapitools::server::model::Detector_list ret;
    std::vector<org::openapitools::server::model::Detector_list_element> dets;
    for (int i = 0; i < input.detector.size(); i++) {
        org::openapitools::server::model::Detector_list_element d;
        d.setId(i);
        d.setDescription(input.detector[i].description);
        d.setNmodules(input.detector[i].nmodules);
        d.setHeight(input.detector[i].height);
        d.setWidth(input.detector[i].width);
        d.setSerialNumber(input.detector[i].serial_number);
        d.setBaseIpv4Addr(input.detector[i].base_ipv4_addr);
        d.setUdpInterfaceCount(input.detector[i].udp_interface_count);
        d.setMinFrameTimeUs(input.detector[i].min_frame_time.count());
        d.setMinCountTimeUs(input.detector[i].min_count_time.count());
        d.setReadoutTimeUs(input.detector[i].readout_time.count());
        dets.emplace_back(std::move(d));
    }
    ret.setDetectors(dets);
    ret.setCurrentId(input.current_id);
    return ret;
}

org::openapitools::server::model::Plots Convert(const MultiLinePlot& input) {
    std::vector<org::openapitools::server::model::Plot> tmp(input.size());
    for (int i = 0; i < input.size(); i++) {
        tmp[i].setTitle(input[i].title);
        tmp[i].setX(input[i].x);
        tmp[i].setY(input[i].y);
    }

    org::openapitools::server::model::Plots output;
    output.setPlot(tmp);
    return output;
}

AzimuthalIntegrationSettings Convert(const org::openapitools::server::model::Azim_int_settings& input) {
    AzimuthalIntegrationSettings ret{};
    ret.SolidAngleCorrection(input.isSolidAngleCorr());
    if (input.polarizationFactorIsSet())
        ret.PolarizationFactor(input.getPolarizationFactor());
    ret.QSpacing_recipA(input.getQSpacing());
    ret.LowQ_recipA(input.getLowQRecipA());
    ret.HighQ_recipA(input.getHighQRecipA());
    return ret;
}

org::openapitools::server::model::Azim_int_settings Convert(const AzimuthalIntegrationSettings& settings) {
    org::openapitools::server::model::Azim_int_settings ret{};
    ret.setSolidAngleCorr(settings.IsSolidAngleCorrection());
    if (settings.GetPolarizationFactor().has_value())
        ret.setPolarizationFactor(settings.GetPolarizationFactor().value());
    ret.setHighQRecipA(settings.GetHighQ_recipA());
    ret.setLowQRecipA(settings.GetLowQ_recipA());
    ret.setQSpacing(settings.GetQSpacing_recipA());
    return ret;
}

ROIDefinition Convert(const org::openapitools::server::model::Roi_definitions& input) {
    ROIDefinition output{};
    for (const auto &i: input.getBox().getRois())
        output.boxes.emplace_back(ROIBox(i.getName(), i.getMinXPxl(), i.getMaxXPxl(), i.getMinYPxl(), i.getMaxYPxl()));
    for (const auto &i: input.getCircle().getRois())
        output.circles.emplace_back(ROICircle(i.getName(), i.getCenterXPxl(), i.getCenterYPxl(), i.getRadiusPxl()));
    return output;
}

org::openapitools::server::model::Roi_circle_list Convert(const std::vector<ROICircle> &input) {
    org::openapitools::server::model::Roi_circle_list ret{};
    std::vector<org::openapitools::server::model::Roi_circle> tmp;
    for (const auto &i: input) {
        org::openapitools::server::model::Roi_circle elem;
        elem.setName(i.GetName());
        elem.setCenterXPxl(i.GetX());
        elem.setCenterYPxl(i.GetY());
        elem.setRadiusPxl(i.GetRadius_pxl());
        tmp.emplace_back(elem);
    }
    ret.setRois(tmp);
    return ret;
}

org::openapitools::server::model::Roi_box_list Convert(const std::vector<ROIBox> &input) {
    org::openapitools::server::model::Roi_box_list ret{};
    std::vector<org::openapitools::server::model::Roi_box> tmp;
    for (const auto &i: input) {
        org::openapitools::server::model::Roi_box elem;
        elem.setName(i.GetName());
        elem.setMinXPxl(i.GetXMin());
        elem.setMaxXPxl(i.GetXMax());
        elem.setMinYPxl(i.GetYMin());
        elem.setMaxYPxl(i.GetYMax());
        tmp.emplace_back(elem);
    }
    ret.setRois(tmp);
    return ret;
}

org::openapitools::server::model::Roi_definitions Convert(const ROIDefinition &input) {
    org::openapitools::server::model::Roi_definitions ret{};
    ret.setCircle(Convert(input.circles));
    ret.setBox(Convert(input.boxes));
    return ret;
}

PreviewJPEGSettings Convert(const org::openapitools::server::model::Preview_settings& input) {
    PreviewJPEGSettings ret{};
    ret.show_spots = input.isShowSpots();
    ret.jpeg_quality = input.getJpegQuality();
    ret.saturation_value = input.getSaturation();
    ret.show_roi = input.isShowRoi();
    ret.show_indexed = input.isShowIndexed();
    ret.show_user_mask = input.isShowUserMask();
    if (input.resolutionRingIsSet())
        ret.resolution_ring = input.getResolutionRing();
    return ret;
}

ImageFormatSettings Convert(const org::openapitools::server::model::Image_format_settings& input) {
    ImageFormatSettings ret{};
    ret.GeometryTransformed(input.isGeometryTransform());
    ret.AutoSummation(input.isSummation());
    ret.JungfrauConversion(input.isJungfrauConversion());
    ret.MaskChipEdges(input.isMaskChipEdges());
    ret.MaskModuleEdges(input.isMaskModuleEdges());
    ret.ApplyPixelMask(input.isApplyMask());
    ret.PedestalG0RMSLimit(input.getJungfrauPedestalG0RmsLimit());
    ret.MaskPixelsWithoutG0(input.isJungfrauMaskPixelsWithoutG0());
    if (input.signedOutputIsSet())
        ret.PixelSigned(input.isSignedOutput());
    if (input.jungfrauConversionFactorKeVIsSet())
        ret.JungfrauConvFactor_keV(input.getJungfrauConversionFactorKeV());
    if (input.bitDepthImageIsSet())
        ret.BitDepthImage(input.getBitDepthImage());
    return ret;
}

org::openapitools::server::model::Image_format_settings Convert(const ImageFormatSettings& input) {
    org::openapitools::server::model::Image_format_settings ret{};
    ret.setGeometryTransform(input.IsGeometryTransformed());
    ret.setSummation(input.IsAutoSummation());
    ret.setJungfrauConversion(input.IsJungfrauConversion());
    ret.setMaskChipEdges(input.IsMaskChipEdges());
    ret.setMaskModuleEdges(input.IsMaskModuleEdges());
    ret.setApplyMask(input.IsApplyPixelMask());
    ret.setJungfrauMaskPixelsWithoutG0(input.IsMaskPixelsWithoutG0());
    ret.setJungfrauPedestalG0RmsLimit(input.GetPedestalG0RMSLimit());
    if (input.IsPixelSigned().has_value())
        ret.setSignedOutput(input.IsPixelSigned().value());
    if (input.GetJungfrauConvFactor_keV().has_value())
        ret.setJungfrauConversionFactorKeV(input.GetJungfrauConvFactor_keV().value());
    if (input.GetBitDepthImage().has_value())
        ret.setBitDepthImage(input.GetBitDepthImage().value());
    return ret;
}

DatasetSettings Convert(const org::openapitools::server::model::Dataset_settings& input) {
    DatasetSettings ret;

    ret.ImagesPerTrigger(input.getImagesPerTrigger());
    ret.NumTriggers(input.getNtrigger());

    if (input.runNumberIsSet())
        ret.RunNumber(input.getRunNumber());
    if (input.runNameIsSet())
        ret.RunName(input.getRunName());

    ret.ExperimentGroup(input.getExperimentGroup());

    if (input.imageTimeUsIsSet())
        ret.ImageTime(std::chrono::microseconds(input.getImageTimeUs()));
    ret.BeamX_pxl(input.getBeamXPxl());
    ret.BeamY_pxl(input.getBeamYPxl());
    ret.DetectorDistance_mm(input.getDetectorDistanceMm());
    ret.PhotonEnergy_keV(input.getIncidentEnergyKeV());

    ret.FilePrefix(input.getFilePrefix());

    if (!input.compressionIsSet())
        ret.Compression(CompressionAlgorithm::BSHUF_LZ4);
    else {
        std::string compr = str_tolower(input.getCompression());
        if (compr == "bslz4")
            ret.Compression(CompressionAlgorithm::BSHUF_LZ4);
        else if (compr == "bszstd")
            ret.Compression(CompressionAlgorithm::BSHUF_ZSTD);
        else if (compr == "bszstd_rle")
            ret.Compression(CompressionAlgorithm::BSHUF_ZSTD_RLE);
        else if (compr == "none")
            ret.Compression(CompressionAlgorithm::NO_COMPRESSION);
        else
            throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "Unknown compression");
    }
    if (input.poissonCompressionIsSet())
        ret.LossyCompressionPoisson(input.getPoissonCompression());

    if (input.unitCellIsSet())
        ret.SetUnitCell(UnitCell{
                .a = input.getUnitCell().getA(),
                .b = input.getUnitCell().getB(),
                .c = input.getUnitCell().getC(),
                .alpha = input.getUnitCell().getAlpha(),
                .beta = input.getUnitCell().getBeta(),
                .gamma = input.getUnitCell().getGamma()
        });

    if (input.totalFluxIsSet())
        ret.TotalFlux(input.getTotalFlux());
    if (input.transmissionIsSet())
        ret.AttenuatorTransmission(input.getTransmission());

    if (input.goniometerIsSet()) {
        ret.Goniometer(GoniometerAxis{
                .name = input.getGoniometer().getName(),
                .increment = input.getGoniometer().getStep(),
                .start = input.getGoniometer().getStart()
        });
        if (input.getGoniometer().getVector().size() == 3) {
            auto v = input.getGoniometer().getVector();
            ret.RotationAxis(Coord(v[0], v[1], v[2]));
        } else
            throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "Rotation axis must be provided");
    }
    if (input.spaceGroupNumberIsSet())
        ret.SpaceGroupNumber(input.getSpaceGroupNumber());
    ret.SampleName(input.getSampleName());
    ret.HeaderAppendix(input.getHeaderAppendix());
    ret.ImageAppendix(input.getImageAppendix());
    ret.ImagesPerFile(input.getImagesPerFile());

    if (input.dataReductionFactorSerialmxIsSet())
        ret.LossyCompressionSerialMX(input.getDataReductionFactorSerialmx());

    if (input.pixelValueLowThresholdIsSet())
        ret.PixelValueLowThreshold(input.getPixelValueLowThreshold());

    if (input.saveCalibrationIsSet())
        ret.SaveCalibration(input.isSaveCalibration());

    ret.WriteNXmxHDF5Master(input.isWriteNxmxHdf5Master());

    return ret;
}

std::vector<org::openapitools::server::model::Fpga_status_inner> Convert(const std::vector<DeviceStatus> &input) {
    std::vector<org::openapitools::server::model::Fpga_status_inner>  ret;
    for (const auto &d: input) {
        org::openapitools::server::model::Fpga_status_inner tmp;
        tmp.setPciDevId(d.device_number);
        tmp.setSerialNumber(d.serial_number);
        tmp.setFwVersion(d.fpga_firmware_version);
        tmp.setBaseMacAddr(MacAddressToStr(d.fpga_default_mac_addr));
        tmp.setPacketsSls(d.packets_sls);
        tmp.setPacketsUdp(d.packets_udp);
        tmp.setEthLinkCount(d.eth_link_count);
        tmp.setEthLinkStatus(d.eth_link_status);
        tmp.setFpgaTempC(static_cast<float>(d.fpga_temp_C));
        tmp.setHbmTempC(static_cast<float>(d.hbm_0_temp_C));
        tmp.setPowerUsageW(static_cast<float>(d.fpga_pcie_12V_I_mA * d.fpga_pcie_12V_V_mV + d.fpga_pcie_3p3V_I_mA
        * d.fpga_pcie_3p3V_V_mV) / (1000.0f * 1000.0f));
        tmp.setIdle(d.idle);
        tmp.setPcieLinkSpeed(d.pcie_link_speed);
        tmp.setPcieLinkWidth(d.pcie_link_width);
        ret.emplace_back(std::move(tmp));
    }
    return ret;
}

ZMQPreviewSettings Convert(const org::openapitools::server::model::Zeromq_preview_settings &input) {
    ZMQPreviewSettings ret;
    if (input.isEnabled())
        ret.period = std::chrono::milliseconds(input.getPeriodMs());
    else
        ret.period = {};
    ret.address = "";
    return ret;
}

ZMQMetadataSettings Convert(const org::openapitools::server::model::Zeromq_metadata_settings &input) {
    ZMQMetadataSettings ret;
    if (input.isEnabled())
        ret.period = std::chrono::milliseconds(input.getPeriodMs());
    else
        ret.period = {};
    ret.address = "";
    return ret;
}

org::openapitools::server::model::Zeromq_preview_settings Convert(const ZMQPreviewSettings &settings) {
    org::openapitools::server::model::Zeromq_preview_settings ret;
    ret.setEnabled(settings.period.has_value());
    if (settings.period.has_value())
        ret.setPeriodMs(settings.period.value().count() / 1000);
    ret.setSocketAddress(settings.address);
    return ret;
}

org::openapitools::server::model::Zeromq_metadata_settings Convert(const ZMQMetadataSettings &settings) {
    org::openapitools::server::model::Zeromq_metadata_settings ret;
    ret.setEnabled(settings.period.has_value());
    if (settings.period.has_value())
        ret.setPeriodMs(settings.period.value().count() / 1000);
    ret.setSocketAddress(settings.address);
    return ret;
}

org::openapitools::server::model::Pixel_mask_statistics Convert(const PixelMaskStatistics& input) {
    org::openapitools::server::model::Pixel_mask_statistics ret;
    ret.setUserMask(input.user_mask);
    ret.setWrongGain(input.wrong_gain);
    ret.setTooHighPedestalRms(input.too_high_pedestal_rms);
    return ret;
}

org::openapitools::server::model::Image_buffer_status Convert(const ImageBufferStatus& input) {
    org::openapitools::server::model::Image_buffer_status ret;
    ret.setAvailableSlots(input.available_slots);
    ret.setTotalSlots(input.total_slots);
    ret.setImageNumbers(input.images_in_the_buffer);
    return ret;
}

org::openapitools::server::model::File_writer_settings Convert(const FileWriterSettings& input) {
    org::openapitools::server::model::File_writer_settings ret;
    ret.setFileWriterVersion(input.GetHDF5MasterFormatVersion());
    ret.setOverwrite(input.IsOverwriteExistingFiles());
    return ret;
}

FileWriterSettings Convert(const org::openapitools::server::model::File_writer_settings &input) {
    FileWriterSettings ret;
    ret.OverwriteExistingFiles(input.isOverwrite());
    ret.HDF5MasterFormatVersion(input.getFileWriterVersion());
    return ret;
}