// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_JFJOCHSERVICES_H
#define JUNGFRAUJOCH_JFJOCHSERVICES_H

#include "../common/DiffractionExperiment.h"
#include "../jungfrau/JFCalibration.h"
#include "../common/Logger.h"
#include "../receiver/JFJochReceiverService.h"
#include "../detector_control/DetectorWrapper.h"

struct JFJochServicesOutput {
    JFJochReceiverOutput receiver_output;
};

class JFJochServices {
    JFJochReceiverService *receiver = nullptr;
    std::unique_ptr<DetectorWrapper> detector;

    Logger &logger;
public:
    explicit JFJochServices(Logger &in_logger);
    void On(const DiffractionExperiment& experiment);
    void Off();
    void ConfigureDetector(const DiffractionExperiment& experiment);
    void Start(const DiffractionExperiment& experiment,
               const PixelMask &pixel_mask,
               const JFCalibration &calibration);
    JFJochServicesOutput Stop();
    void Cancel();
    void Trigger();

    void LoadInternalGeneratorImage(const DiffractionExperiment &experiment,
                                    const std::vector<uint16_t> &image,
                                    uint64_t image_number);
    std::optional<JFJochReceiverStatus> GetReceiverStatus() const;
    std::optional<float> GetReceiverProgress() const;
    MultiLinePlot GetPlots(const PlotRequest &request);

    void SetSpotFindingSettings(const SpotFindingSettings &settings);
    JFJochServices& Receiver(JFJochReceiverService *input);

    std::optional<DetectorStatus> GetDetectorStatus() const;

    std::string GetPreviewJPEG(const PreviewJPEGSettings &settings) const;
    std::string GetPreviewTIFF(bool calibration) const;

    void GetXFELPulseID(std::vector<uint64_t> &v) const;
    void GetXFELEventCode(std::vector<uint64_t> &v) const;

    std::vector<DeviceStatus> GetDeviceStatus() const;

    void SetPreviewSocketSettings(const ZMQPreviewSettings &input);
    ZMQPreviewSettings GetPreviewSocketSettings();

    void SetMetadataSocketSettings(const ZMQMetadataSettings &input);
    ZMQMetadataSettings GetMetadataSocketSettings();

    void GetStartMessageFromBuffer(std::vector<uint8_t> &v);
    void GetImageFromBuffer(std::vector<uint8_t> &v, int64_t image_number = -1);
    ImageBufferStatus GetImageBufferStatus() const;
    void ClearImageBuffer() const;
};


#endif //JUNGFRAUJOCH_JFJOCHSERVICES_H
