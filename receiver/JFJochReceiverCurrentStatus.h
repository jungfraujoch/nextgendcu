// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_JFJOCHRECEIVERCURRENTSTATUS_H
#define JFJOCH_JFJOCHRECEIVERCURRENTSTATUS_H

#include <mutex>
#include <optional>
#include <cstdint>

struct JFJochReceiverStatus {
    std::optional<float> indexing_rate;
    std::optional<float> bkg_estimate;
    std::optional<float> compressed_ratio;
    std::optional<float> efficiency;
    std::optional<float> error_pixels;
    std::optional<float> saturated_pixels;
    std::optional<float> roi_beam_sum;
    std::optional<float> roi_beam_npixel;
    uint64_t compressed_size;
    uint64_t max_receive_delay;
    uint64_t max_image_number_sent;
    uint64_t images_collected;
    uint64_t images_sent;
    uint64_t images_skipped;
    bool cancelled;
};

class JFJochReceiverCurrentStatus {
    mutable std::mutex m;
    std::optional<JFJochReceiverStatus> status;
    volatile float progress = NAN;
public:
    std::optional<JFJochReceiverStatus> GetStatus() const;
    void Clear();
    void SetStatus(const JFJochReceiverStatus &status);
    void SetEfficiency(const std::optional<float> &e);
    void SetProgress(std::optional<float> input);
    std::optional<float> GetProgress() const;
};


#endif //JFJOCH_JFJOCHRECEIVERCURRENTSTATUS_H
