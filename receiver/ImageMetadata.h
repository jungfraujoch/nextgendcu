// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_IMAGEMETADATA_H
#define JUNGFRAUJOCH_IMAGEMETADATA_H

#include <cstdint>
#include <mutex>

#include "../fpga/pcie_driver/jfjoch_fpga.h"
#include "../frame_serialize/JFJochMessages.h"
#include "../common/DiffractionExperiment.h"

class ImageMetadata {
    mutable std::mutex m;

    bool pulsed_source;

    bool first_module_loaded = false;
    bool metadata_consistent = true;

    uint64_t xfel_pulse_id = UINT64_MAX;
    uint64_t xfel_event_code = 0;

    uint64_t jf_info = 0;
    uint64_t timestamp = 0;
    uint64_t exptime = 0;
    uint64_t storage_cell  = 0;
    uint64_t saturated_pixels = 0;
    uint64_t error_pixels = 0;

    int64_t max_value = INT64_MIN;
    int64_t min_value = INT64_MAX;

    uint64_t strong_pixels = 0;
    uint64_t packets_collected = 0;

    std::map<std::string, ROIMessage> rois;
    std::map<std::string, uint16_t> roi_map_name;
public:
    explicit ImageMetadata(const DiffractionExperiment &experiment);
    void Process(const DeviceOutput *output);
    void Export(DataMessage &message, uint64_t packets_expected_per_image) const;
    bool IsBunchIDConsistent() const;
};


#endif //JUNGFRAUJOCH_IMAGEMETADATA_H
