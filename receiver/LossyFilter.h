// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_LOSSYFILTER_H
#define JUNGFRAUJOCH_LOSSYFILTER_H

#include <random>
#include <mutex>
#include <cstdint>

#include "../frame_serialize/JFJochMessages.h"
#include "../common/DiffractionExperiment.h"
#include <atomic>

class LossyFilter {
    std::mutex random_m;

    std::random_device rdev;
    std::mt19937 mt{rdev()};
    std::uniform_real_distribution<float> distr{0.0, 1.0};

    std::atomic<int64_t> image_number{0};

    float p;
    bool RollDice();
public:
    explicit LossyFilter(float p);
    explicit LossyFilter(const DiffractionExperiment& x);
    bool ApplyFilter(DataMessage& message);
};


#endif //JUNGFRAUJOCH_LOSSYFILTER_H
