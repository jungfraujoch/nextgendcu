// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_JFJOCHRECEIVER_H
#define JUNGFRAUJOCH_JFJOCHRECEIVER_H

#include <condition_variable>
#include <atomic>
#include <future>
#include <latch>

#include "../acquisition_device/AcquisitionDeviceGroup.h"

#include "../common/DiffractionExperiment.h"
#include "../common/JFJochException.h"
#include "FrameTransformation.h"
#include "../image_pusher/ImagePusher.h"
#include "../common/Logger.h"
#include "../common/ThreadSafeFIFO.h"
#include "../common/NUMAHWPolicy.h"
#include "../common/ImageBuffer.h"
#include "../common/PixelMask.h"

#include "../image_analysis/StrongPixelSet.h"
#include "../image_analysis/AzimuthalIntegrationMapping.h"
#include "../image_analysis/AzimuthalIntegrationProfile.h"

#include "../jungfrau/JFCalibration.h"
#include "../common/ADUHistogram.h"
#include "../common/Plot.h"
#include "JFJochReceiverPlots.h"
#include "../preview/PreviewImage.h"
#include "LossyFilter.h"
#include "JFJochReceiverCurrentStatus.h"
#include "../jungfrau/JFPedestalCalc.h"
#include "../preview/ZMQPreviewSocket.h"
#include "../preview/ZMQMetadataSocket.h"
#include "ImageMetadata.h"
#include "../common/ModuleSummation.h"
#include "../image_analysis/MXAnalyzer.h"
#include "../common/MovingAverage.h"

struct JFJochReceiverOutput {
    std::vector<JFModulePedestal> pedestal_result;
    std::vector<uint64_t> received_packets;
    std::vector<uint64_t> expected_packets;
    std::string master_file_name;
    uint64_t start_time_ms;
    uint64_t end_time_ms;
    float efficiency;
    bool writer_queue_full_warning;
    JFJochReceiverStatus status;
};

class JFJochReceiver {
    const DiffractionExperiment &experiment;
    const JFCalibration *calibration;
    const PixelMask &pixel_mask;

    std::vector<std::unique_ptr<JFPedestalCalc>> pedestal;
    ImageBuffer &image_buffer;

    Logger &logger;

    std::vector<std::future<void>> frame_transformation_futures;
    std::vector<std::future<void>> data_acquisition_futures;

    std::vector<ADUHistogram> adu_histogram_module;

    AzimuthalIntegrationMapping az_int_mapping;

    std::future<void> measurement;

    ThreadSafeFIFO<int64_t> images_to_go;

    ImagePusher &image_pusher;
    bool push_images_to_writer;
    bool writer_queue_full;

    volatile bool cancelled{false};

    AcquisitionDeviceGroup &acquisition_device;
    uint16_t ndatastreams{0};

    uint64_t max_delay = 0;
    std::mutex max_delay_mutex;

    std::atomic<size_t> compressed_size{0};
    std::atomic<size_t> images_sent{0};
    std::atomic<size_t> images_collected{0};
    std::atomic<size_t> images_skipped{0};

    const int64_t frame_transformation_nthreads;
    const int64_t pedestal_nthreads;
    const int64_t summation_nthreads;
    int64_t expected_packets_per_image;
    bool only_2nd_sc_pedestal = false; // special mode used for pedestal G1/G2 with storage cells

    std::latch frame_transformation_ready;
    std::latch data_acquisition_ready;

    std::chrono::time_point<std::chrono::system_clock> start_time;
    std::chrono::time_point<std::chrono::system_clock> end_time;

    SpotFindingSettings spot_finding_settings;
    std::mutex spot_finding_settings_mutex;

    int64_t max_image_number_sent = 0;
    std::mutex max_image_number_sent_mutex;

    NUMAHWPolicy numa_policy;

    PreviewImage &preview_image;
    PreviewImage &preview_image_indexed;
    ZMQPreviewSocket *zmq_preview_socket;
    ZMQMetadataSocket *zmq_metadata_socket;

    JFJochReceiverCurrentStatus &current_status;
    JFJochReceiverPlots &plots;

    LossyFilter serialmx_filter;

    std::vector<uint32_t> mask_raw_coord;

    MovingAverage saturated_pixels;
    MovingAverage error_pixels;
    MovingAverage roi_beam_npixel;
    MovingAverage roi_beam_sum;

    void AcquireThread(uint16_t data_stream);
    void LoadCalibrationToFPGA(uint16_t data_stream);

    void FrameTransformationThread(uint32_t threadid);
    void MeasurePedestalThread(uint16_t data_stream,
                               uint16_t module_number,
                               uint16_t storage_cell,
                               uint32_t threadid,
                               bool ignore);

    int64_t SummationThread(uint16_t data_stream,
                         int64_t image_number,
                         uint16_t module_number,
                         uint32_t threadid,
                         ModuleSummation &summation);
    void Cancel(const JFJochException &e);
    void FinalizeMeasurement();
    SpotFindingSettings GetSpotFindingSettings();
    void UpdateMaxImage(int64_t image_number);
    void UpdateMaxDelay(uint64_t delay);
    void SendStartMessage();
    void SendCalibration();
    void SendPedestal(const std::string &prefix, const std::vector<uint8_t> &v, int gain, int sc);
    void RetrievePedestal(std::vector<JFModulePedestal> &output) const;
    JFJochReceiverStatus GetStatus() const;
public:
    JFJochReceiver(const DiffractionExperiment& experiment,
                   const PixelMask &pixel_mask,
                   const JFCalibration *calibration,
                   AcquisitionDeviceGroup &acquisition_devices,
                   ImagePusher &image_pusher,
                   Logger &logger, int64_t forward_and_sum_nthreads,
                   const NUMAHWPolicy &numa_policy,
                   const SpotFindingSettings &spot_finding_settings,
                   PreviewImage &preview_image,
                   PreviewImage &preview_image_indexed,
                   JFJochReceiverCurrentStatus &current_status,
                   JFJochReceiverPlots &plots,
                   ImageBuffer &image_buffer,
                   ZMQPreviewSocket *zmq_preview_socket,
                   ZMQMetadataSocket *zmq_metadata_socket);
    ~JFJochReceiver();
    JFJochReceiver(const JFJochReceiver &other) = delete;
    JFJochReceiver& operator=(const JFJochReceiver &other) = delete;
    void StopReceiver();

    float GetEfficiency() const;
    void Cancel(bool silent);
    float GetProgress() const;

    void SetSpotFindingSettings(const SpotFindingSettings &spot_finding_settings);

    JFJochReceiverOutput GetFinalStatistics() const;
};

#endif //JUNGFRAUJOCH_JFJOCHRECEIVER_H

