// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <cstring>

#include "bitshuffle/bitshuffle.h"

#include "FrameTransformation.h"
#include "../common/RawToConvertedGeometry.h"
#include "../common/JFJochException.h"

template <class T> void FillVector(std::vector<char> &v, int64_t fill_value) {
    auto ptr = (T *) v.data();
    for (int i = 0; i < v.size() / sizeof(T); i++)
        ptr[i] = static_cast<T>(fill_value);
}

FrameTransformation::FrameTransformation(const DiffractionExperiment &in_experiment) :
        experiment(in_experiment),
        pixel_depth(experiment.GetByteDepthImage()), pixel_signed(experiment.IsPixelSigned()),
        compressor(in_experiment.GetCompressionAlgorithm()),
        err_value(experiment.GetByteDepthImage() * RAW_MODULE_SIZE) {

    precompression_buffer.resize(experiment.GetPixelsNum() * pixel_depth);

    if (pixel_depth == 2) {
        if (pixel_signed) {
            FillVector<int16_t>(precompression_buffer, experiment.GetImageFillValue());
            FillVector<int16_t>(err_value, experiment.GetImageFillValue());
        } else {
            FillVector<uint16_t>(precompression_buffer, experiment.GetImageFillValue());
            FillVector<uint16_t>(err_value, experiment.GetImageFillValue());
        }
    } else if (pixel_depth == 4) {
        if (pixel_signed) {
            FillVector<int32_t>(precompression_buffer, experiment.GetImageFillValue());
            FillVector<int32_t>(err_value, experiment.GetImageFillValue());
        } else {
            FillVector<uint32_t>(precompression_buffer, experiment.GetImageFillValue());
            FillVector<uint32_t>(err_value, experiment.GetImageFillValue());
        }
    } else if (pixel_depth == 1) {
        if (pixel_signed) {
            FillVector<int8_t>(precompression_buffer, experiment.GetImageFillValue());
            FillVector<int8_t>(err_value, experiment.GetImageFillValue());
        } else {
            FillVector<uint8_t>(precompression_buffer, experiment.GetImageFillValue());
            FillVector<uint8_t>(err_value, experiment.GetImageFillValue());
        }
    } else
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "Pixel depth unsupported");
}

size_t FrameTransformation::CompressImage(void *output) {
    return compressor.Compress(output, precompression_buffer.data(), experiment.GetPixelsNum(),
                               experiment.GetByteDepthImage());
}

CompressedImage FrameTransformation::GetCompressedImage() {
    CompressedImage image{};
    if (experiment.GetCompressionAlgorithm() == CompressionAlgorithm::NO_COMPRESSION) {
        image.data = (uint8_t *) precompression_buffer.data();
        image.size = experiment.GetPixelsNum() * experiment.GetByteDepthImage();
    } else {
        compressed_buffer.resize(MaxCompressedSize(experiment.GetCompressionAlgorithm(),
                                                   experiment.GetPixelsNum(),
                                                   experiment.GetByteDepthImage()));
        image.data = (uint8_t *) compressed_buffer.data();
        image.size = CompressImage(compressed_buffer.data());
    }
    image.xpixel = experiment.GetXPixelsNum();
    image.ypixel = experiment.GetYPixelsNum();
    image.algorithm = experiment.GetCompressionAlgorithm();
    image.pixel_depth_bytes = experiment.GetByteDepthImage();
    image.pixel_is_signed = pixel_signed;
    image.pixel_is_float = false;
    image.channel = "default";
    return image;
}

void FrameTransformation::ProcessModule(const void *input, uint16_t module_number, int data_stream) {
    size_t module_number_abs = experiment.GetFirstModuleOfDataStream(data_stream) + module_number;

    if (!experiment.IsGeometryTransformed()) {
        size_t mod_size = RAW_MODULE_SIZE * pixel_depth;
        memcpy(precompression_buffer.data() + module_number_abs * mod_size, input, mod_size);
    } else {
        auto pixel0 = experiment.GetPixel0OfModule(module_number_abs);
        auto fast_step = experiment.GetModuleFastDirectionStep(module_number_abs);
        auto slow_step = experiment.GetModuleSlowDirectionStep(module_number_abs);
        
        if (pixel_depth == 2) {
            if (pixel_signed)
                TransferModuleAdjustMultipixels((int16_t *) precompression_buffer.data(), (int16_t *) input,
                                                slow_step, static_cast<int16_t>(INT16_MIN), static_cast<int16_t>(INT16_MAX),
                                                fast_step, pixel0);
            else
                TransferModuleAdjustMultipixels((uint16_t *) precompression_buffer.data(), (uint16_t *) input,
                                                slow_step, static_cast<uint16_t>(0), static_cast<uint16_t>(UINT16_MAX - 1),
                                                fast_step, pixel0);
        } else if (pixel_depth == 4) {
            if (pixel_signed)
                TransferModuleAdjustMultipixels((int32_t *) precompression_buffer.data(), (int32_t *) input,
                                                slow_step, static_cast<int32_t>(INT32_MIN), static_cast<int32_t>(INT32_MAX),
                                                fast_step, pixel0);
            else
                TransferModuleAdjustMultipixels((uint32_t *) precompression_buffer.data(), (uint32_t *) input,
                                                slow_step, static_cast<uint32_t>(0), static_cast<uint32_t>(UINT32_MAX - 1),
                                                fast_step, pixel0);
        } else if (pixel_depth == 1) {
            if (pixel_signed)
                TransferModuleAdjustMultipixels((int8_t *) precompression_buffer.data(), (int8_t *) input,
                                                slow_step, static_cast<int8_t>(INT8_MIN), static_cast<int8_t>(INT8_MAX),
                                                fast_step, pixel0);
            else
                TransferModuleAdjustMultipixels((uint8_t *) precompression_buffer.data(), (uint8_t *) input,
                                                slow_step, static_cast<uint8_t>(0), static_cast<uint8_t>(UINT8_MAX - 1),
                                                fast_step, pixel0);
        } else
            throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "Pixel depth unsupported");

    }
}

void FrameTransformation::ProcessModule(const DeviceOutput *output, int data_stream) {
    ProcessModule(output->pixels, output->module_statistics.module_number, data_stream);
}

const void *FrameTransformation::GetImage() const {
    return precompression_buffer.data();
}

void FrameTransformation::FillNotCollectedModule(uint16_t module_number, int data_stream) {
    ProcessModule(err_value.data(), module_number, data_stream);
}
