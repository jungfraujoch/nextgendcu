// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <cmath>
#include "JFJochReceiverCurrentStatus.h"

std::optional<JFJochReceiverStatus> JFJochReceiverCurrentStatus::GetStatus() const {
    std::unique_lock ul(m);
    return status;
}

void JFJochReceiverCurrentStatus::Clear() {
    std::unique_lock ul(m);
    status.reset();
}

void JFJochReceiverCurrentStatus::SetStatus(const JFJochReceiverStatus &in_status) {
    std::unique_lock ul(m);
    status = in_status;
}

void JFJochReceiverCurrentStatus::SetEfficiency(const std::optional<float> &e) {
    std::unique_lock ul(m);
    status->efficiency = e;
}

std::optional<float> JFJochReceiverCurrentStatus::GetProgress() const {
    float tmp = progress;
    if (std::isnan(tmp))
        return {};
    else
        return tmp;
}

void JFJochReceiverCurrentStatus::SetProgress(std::optional<float> input) {
    if (input.has_value()) {
        if (input.value() < 0.0)
            progress = 0.0;
        else if (input.value() > 1.0)
            progress = 1.0;
        else
            progress = input.value();
    } else
        progress = NAN;
}
