// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_JFJOCHRECEIVERPLOTS_H
#define JUNGFRAUJOCH_JFJOCHRECEIVERPLOTS_H

#include "../common/StatusVector.h"
#include "../common/Histogram.h"
#include "../common/ADUHistogram.h"
#include "../common/DiffractionExperiment.h"
#include "../image_analysis/AzimuthalIntegrationProfile.h"
#include "../frame_serialize/JFJochMessages.h"

class JFJochReceiverPlots {
    int64_t default_binning = 1;

    StatusVector<uint64_t> xfel_pulse_id;
    StatusVector<uint64_t> xfel_event_code;

    StatusVector<float> bkg_estimate;
    StatusMultiVector<uint64_t> spot_count;

    StatusVector<uint64_t> indexing_solution;
    StatusMultiVector<float> indexing_unit_cell_len;
    StatusMultiVector<float> indexing_unit_cell_angle;
    StatusMultiVector<uint64_t> error_pixels;
    StatusVector<uint64_t> strong_pixels;
    StatusVector<uint64_t> receiver_delay;
    StatusVector<uint64_t> receiver_free_send_buf;
    StatusVector<float> image_collection_efficiency;
    StatusVector<uint64_t> packets_received;
    StatusVector<int64_t> max_value;

    StatusMultiVector<int64_t> roi_sum;
    StatusMultiVector<int64_t> roi_max_count;
    StatusMultiVector<uint64_t> roi_pixels;

    mutable std::mutex az_int_profile_mutex;
    std::unique_ptr<AzimuthalIntegrationProfile> az_int_profile;
public:
    void Setup(const DiffractionExperiment& experiment, const AzimuthalIntegrationMapping& mapping);

    void Add(const DataMessage& msg, const AzimuthalIntegrationProfile &profile);
    void AddEmptyImage(const DataMessage& msg);
    MultiLinePlot GetPlots(const PlotRequest& request);

    void GetXFELPulseID(std::vector<uint64_t>& v) const;
    void GetXFELEventCode(std::vector<uint64_t>& v) const;

    std::optional<float> GetIndexingRate() const;
    std::optional<float> GetBkgEstimate() const;

    std::vector<float> GetAzIntProfile() const;
    MultiLinePlot GetAzIntProfilePlot() const;
};


#endif //JUNGFRAUJOCH_JFJOCHRECEIVERPLOTS_H
