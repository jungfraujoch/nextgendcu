// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_JFJOCHRECEIVERSERVICE_H
#define JUNGFRAUJOCH_JFJOCHRECEIVERSERVICE_H

#include <mutex>
#include "JFJochReceiver.h"

#include "../common/NUMAHWPolicy.h"
#include "../preview/ZMQMetadataSocket.h"
#include "../preview/ZMQPreviewSocket.h"

class JFJochReceiverService {
    NUMAHWPolicy numa_policy;
    std::unique_ptr<JFJochReceiver> receiver;
    AcquisitionDeviceGroup &aq_devices;
    Logger &logger;
    ImageBuffer image_buffer;
    ImagePusher &image_pusher;
    int64_t nthreads = 8;

    enum class ReceiverState {Idle, Running};
    ReceiverState state = ReceiverState::Idle;
    mutable std::mutex state_mutex;
    std::condition_variable measurement_done;
    std::future<void> measurement;
    void FinalizeMeasurement();
    void FinalizeMeasurementChangeState();
    SpotFindingSettings spot_finding_settings;

    PreviewImage preview_image;
    PreviewImage preview_image_indexed;
    std::unique_ptr<ZMQPreviewSocket> zmq_preview_socket;
    std::unique_ptr<ZMQMetadataSocket> zmq_metadata_socket;

    JFJochReceiverCurrentStatus receiver_status;
    JFJochReceiverPlots plots;
public:
    JFJochReceiverService(AcquisitionDeviceGroup &aq_devices,
                          Logger &logger,
                          ImagePusher &pusher,
                          size_t send_buffer_size_MiB = 512);
    JFJochReceiverService& NumThreads(int64_t input);
    JFJochReceiverService& NUMAPolicy(const NUMAHWPolicy& policy);
    JFJochReceiverService& NUMAPolicy(const std::string& policy);
    JFJochReceiverService& PreviewSocket(const std::string &addr);
    JFJochReceiverService& MetadataSocket(const std::string &addr);

    JFJochReceiverService& PreviewSocketSettings(const ZMQPreviewSettings &input);
    JFJochReceiverService& MetadataSocketSettings(const ZMQMetadataSettings &input);

    ZMQPreviewSettings GetPreviewSocketSettings() const;
    ZMQMetadataSettings GetMetadataSocketSettings() const;

    std::string GetPreviewSocketAddress() const;
    std::string GetMetadataSocketAddress() const;

    void LoadInternalGeneratorImage(const DiffractionExperiment& experiment,
                                    const std::vector<uint16_t> &raw_expected_image,
                                    uint64_t image_number);
    void Start(const DiffractionExperiment &experiment,
               const PixelMask &pixel_mask,
               const JFCalibration *calibration);
    void Cancel(bool silent);
    JFJochReceiverOutput Stop();
    void SetSpotFindingSettings(const SpotFindingSettings& settings);
    MultiLinePlot GetDataProcessingPlot(const PlotRequest& request);
    std::optional<JFJochReceiverStatus> GetStatus();
    std::vector<AcquisitionDeviceNetConfig> GetNetworkConfig();
    std::vector<DeviceStatus> GetDeviceStatus() const;
    std::optional<float> GetProgress() const;

    std::string GetTIFF(bool calibration) const;
    std::string GetJPEG(const PreviewJPEGSettings &settings) const;

    void GetXFELEventCode(std::vector<uint64_t> &v) const;
    void GetXFELPulseID(std::vector<uint64_t> &v) const;

    void GetStartMessageFromBuffer(std::vector<uint8_t> &v);
    void GetImageFromBuffer(std::vector<uint8_t> &v, int64_t image_number = -1);
    ImageBufferStatus GetImageBufferStatus() const;
    void ClearImageBuffer();
};


#endif //JUNGFRAUJOCH_JFJOCHRECEIVERSERVICE_H
