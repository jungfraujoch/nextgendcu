// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_FRAMETRANSFORMATION_H
#define JUNGFRAUJOCH_FRAMETRANSFORMATION_H

#include "../common/DiffractionExperiment.h"
#include "JFJochCompressor.h"
#include "../frame_serialize/JFJochMessages.h"
#include "../fpga/pcie_driver/jfjoch_fpga.h"

class FrameTransformation {
    const DiffractionExperiment& experiment;
    JFJochBitShuffleCompressor compressor;

    std::vector<char> precompression_buffer;
    std::vector<char> compressed_buffer;
    std::vector<char> err_value;

    const bool pixel_signed;
    const size_t pixel_depth;
public:
    explicit FrameTransformation(const DiffractionExperiment &experiment);
    void ProcessModule(const DeviceOutput *output, int data_stream);
    void ProcessModule(const void *input, uint16_t module_number, int data_stream);
    void FillNotCollectedModule(uint16_t module_number, int data_stream);
    CompressedImage GetCompressedImage();
    size_t CompressImage(void *output);
    const void *GetImage() const;
};

#endif //JUNGFRAUJOCH_FRAMETRANSFORMATION_H
