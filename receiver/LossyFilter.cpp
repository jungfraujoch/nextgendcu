// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "LossyFilter.h"

LossyFilter::LossyFilter(float in_p)
        : p(in_p) {}


bool LossyFilter::RollDice() {
    std::unique_lock ul(random_m);

    if (distr(mt) < p)
        return true;
    else
        return false;
}

bool LossyFilter::ApplyFilter(DataMessage &message) {
    if (p == 1.0)
        return true;
    else {
        if (message.indexing_result || ((p > 0.0) && RollDice())) {
            message.number = image_number++;
            return true;
        } else
            return false;
    }
}

LossyFilter::LossyFilter(const DiffractionExperiment &x)
: p (x.GetLossyCompressionSerialMX()){}
