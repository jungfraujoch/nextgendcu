// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "JFJochReceiver.h"

#ifndef JUNGFRAUJOCH_JFJOCHRECEIVERTEST_H
#define JUNGFRAUJOCH_JFJOCHRECEIVERTEST_H

bool JFJochReceiverTest(JFJochReceiverOutput &output, Logger &logger,
                        AcquisitionDeviceGroup &aq_devices,
                        const DiffractionExperiment &x,
                        const PixelMask &pixel_mask,
                        uint16_t nthreads,
                        const std::string &numa_policy = "",
                        size_t send_buf_size_MiB = 1024);

bool JFJochReceiverTest(JFJochReceiverOutput &output, Logger &logger,
                        AcquisitionDeviceGroup &aq_devices,
                        const DiffractionExperiment &x,
                        const PixelMask &pixel_mask,
                        const std::vector<uint16_t> &data,
                        uint16_t nthreads,
                        const std::string &numa_policy = "",
                        size_t send_buf_size_MiB = 1024);

#endif //JUNGFRAUJOCH_JFJOCHRECEIVERTEST_H
