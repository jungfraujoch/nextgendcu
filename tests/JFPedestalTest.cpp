// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>
#include <random>

#include "../jungfrau/JFModulePedestal.h"

TEST_CASE("JFPedestal_Mean","[JFModulePedestal]") {
    JFModulePedestal pedestal;
    auto pedestal_arr = pedestal.GetPedestal();

    for (int i = 0; i < RAW_MODULE_SIZE; i++)
        pedestal_arr[i] = 2455;

    REQUIRE(pedestal.Mean() == Catch::Approx(2455));
}

TEST_CASE("JFPedestal_GetPedestal","[JFModulePedestal]") {
    JFModulePedestal pedestal;
    auto pedestal_arr = pedestal.GetPedestal();
    for (int i = 0; i < RAW_MODULE_SIZE; i++)
        pedestal_arr[i] = i;

    auto fp = pedestal.GetPedestal();

    REQUIRE(fp[0] == 0.0);
    REQUIRE(fp[1] == 1);
    REQUIRE(fp[2] == 2);
    REQUIRE(fp[65535] == 65535);
}