// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>
#include "../fpga/hls_simulation/hls_cores.h"
#include "../common/Logger.h"

TEST_CASE("FPGA_FrameWriterTestIgnore") {
    Logger logger(Catch::getResultCapture().getCurrentTestName());
    STREAM_512  data_in;
    hls::stream<ap_uint<512>> adu_histo_in;
    hls::stream<ap_uint<512>> integration_in;
    hls::stream<ap_uint<512>> spot_finder_in;
    hls::stream<ap_uint<256>> roi_calc_in;
    hls::stream<ap_uint<128>> pixel_calc_in;
    hls::stream<axis_completion > s_axis_completion;
    hls::stream<ap_axiu<512,1,1,1> > host_memory_out;
    hls::stream<axis_datamover_ctrl> datamover_out_cmd;
    hls::stream<ap_uint<32> > s_axis_work_request;
    hls::stream<ap_uint<32> > m_axis_completion;
    uint64_t dma_address_table[1024];
    uint64_t packets_processed = 0;
    ap_uint<1> idle;
    ap_uint<1> in_cancel = 0;

    data_in.write(packet_512_t{.data = 0, .user = 0});

    uint64_t nframes = 128;
    for (int i = 0; i < nframes; i++) {
        for (int j = 0; j < RAW_MODULE_SIZE * 2 / 64; j++)
            data_in.write(packet_512_t{.data = 0, .user = 0, .last = (j == RAW_MODULE_SIZE * 2 / 64 - 1)});
        for (int j = 0; j < RAW_MODULE_SIZE / (64 * 8) + 1; j++)
            spot_finder_in.write(0);
        for (int j = 0; j < FPGA_INTEGRATION_BIN_COUNT / 8; j++)
            integration_in.write(0);
        for (int j = 0; j < ADU_HISTO_BIN_COUNT / 16; j++)
            adu_histo_in.write(0);
        for (int j = 0; j < FPGA_ROI_COUNT; j++)
            roi_calc_in.write(0);
        pixel_calc_in.write(0);
        s_axis_completion.write(axis_completion{.frame_number = i, .last = 0});
    }

    ap_uint<3> state;

    s_axis_completion.write(axis_completion{.last = 1});
    data_in.write(packet_512_t{.data = 0, .user = 1});
    REQUIRE(data_in.size() == nframes * RAW_MODULE_SIZE * 2 / 64 + 2 );
    std::thread thr([&] { std::this_thread::sleep_for(std::chrono::seconds(1)); in_cancel = 1; });
    host_writer(data_in,
                adu_histo_in,
                integration_in,
                spot_finder_in,
                roi_calc_in,
                pixel_calc_in,
                s_axis_completion,
                host_memory_out,
                datamover_out_cmd,
                s_axis_work_request,
                m_axis_completion,
                dma_address_table,
                packets_processed,
                idle,
                in_cancel,
                state);
    CHECK(data_in.empty());
    CHECK(host_memory_out.empty());
    CHECK(datamover_out_cmd.empty());
    CHECK(m_axis_completion.size() == 2);
    CHECK(packets_processed == 0);
    CHECK(idle == 1);
    thr.join();
}