// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>
#include "../common/DiffractionSpot.h"

TEST_CASE("DiffractionSpot_ConvertToImageCoordinates", "[LinearAlgebra][Coord]") {
    DiffractionExperiment experiment(DetectorGeometry(4, 2, 8, 36, true));
    DiffractionSpot spot_1(800, 300, 12);
    spot_1 += DiffractionSpot(800, 301, 4);
    spot_1 += DiffractionSpot(801, 300, 12);
    spot_1 += DiffractionSpot(801, 301, 4);

    REQUIRE(spot_1.RawCoord().x == 800.5);
    REQUIRE(spot_1.RawCoord().y == 300.25);
    REQUIRE(spot_1.Count() == 12 * 2 + 4 * 2);

    spot_1.ConvertToImageCoordinates(experiment, 1);
    REQUIRE(spot_1.RawCoord().x == Catch::Approx(CONVERTED_MODULE_COLS + 8 + 800.5 + 6));
    REQUIRE(spot_1.RawCoord().y == Catch::Approx(CONVERTED_MODULE_LINES * 2 + 36 - 1 - 300.25 - 2));
    REQUIRE(spot_1.Count() == 12 * 2 + 4 * 2);
}
