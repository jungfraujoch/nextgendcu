// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "catch2/catch_all.hpp"
#include "../common/PixelMask.h"

TEST_CASE("PixelMask_MaskModuleEdges","[PixelMask]") {
    DiffractionExperiment experiment(DetectorGeometry(4, 1));
    experiment.MaskModuleEdges(true).MaskChipEdges(false).Mode(DetectorMode::Raw);

    PixelMask mask(experiment);

    auto mask_out = mask.GetMask(experiment);

    REQUIRE(mask_out.size() == experiment.GetModulesNum() * RAW_MODULE_SIZE);

    CHECK(mask_out[0] == (1u<<30));
    CHECK(mask_out[1022] == (1u<<30));
    CHECK(mask_out[1024*5] == (1u<<30));
    CHECK(mask_out[1024*5+1023] == (1u<<30));
    CHECK(mask_out[1024*512-1] == (1u<<30));
    CHECK(mask_out[1024*512] == (1u<<30));
    CHECK(mask_out[1024*512*3+123] == (1u<<30));
    CHECK(mask_out[1024*512*3+1024] == (1u<<30));
    CHECK(mask_out[1024*800+256] == 0);
}

TEST_CASE("PixelMask_MaskChipEdges","[PixelMask]") {
    DiffractionExperiment experiment(DetectorGeometry(4, 1));
    experiment.MaskChipEdges(true).MaskModuleEdges(false).Mode(DetectorMode::Raw);

    PixelMask mask(experiment);
    auto mask_out = mask.GetMask(experiment);
    REQUIRE(mask_out.size() == experiment.GetModulesNum() * RAW_MODULE_SIZE);

    CHECK(mask_out[255] == (1u<<31));
    CHECK(mask_out[1024*800+256] == (1u<<31));

    CHECK(mask_out[1024*233+511] == (1u<<31));
    CHECK(mask_out[1024*512+512] == (1u<<31));
    CHECK(mask_out[1024*1000+767] == (1u<<31));
    CHECK(mask_out[1024*(512*3+12)+768] == (1u<<31));
    CHECK(mask_out[1024*(512+255)+345] == (1u<<31));
    CHECK(mask_out[1024*(1024+256)+876] == (1u<<31));

    CHECK(mask_out[1022] == 0);
    CHECK(mask_out[1024*5+1023] == 0);
    CHECK(mask_out[1024*18] == 0);
}

TEST_CASE("PixelMask_MaskWrongGain","[PixelMask]") {
    ImageFormatSettings image_format;
    image_format.MaskModuleEdges(false).MaskChipEdges(false).MaskPixelsWithoutG0(true).GeometryTransformed(false);

    DiffractionExperiment experiment(DetectorGeometry(4, 1));
    experiment.ImportImageFormatSettings(image_format);

    PixelMask mask(experiment);

    JFCalibration calibration(experiment);
    for (int m = 0; m < experiment.GetModulesNum(); m++) {
        calibration.Pedestal(m, 0) = JFModulePedestal(1500);
        calibration.Pedestal(m, 1) = JFModulePedestal(12000);
        calibration.Pedestal(m, 2) = JFModulePedestal(13000);
    }

    calibration.Pedestal(2,0).GetPedestal()[345] = UINT16_MAX;
    calibration.Pedestal(2,1).GetPedestal()[346] = UINT16_MAX;
    calibration.Pedestal(2,2).GetPedestal()[347] = UINT16_MAX;

    mask.LoadDetectorBadPixelMask(experiment, &calibration);

    auto mask_out = mask.GetMask(experiment);
    REQUIRE(mask_out.size() == experiment.GetModulesNum() * RAW_MODULE_SIZE);

    CHECK(mask_out[2*RAW_MODULE_SIZE + 345] != 0);
    CHECK(mask_out[2*RAW_MODULE_SIZE + 346] != 0);
    CHECK(mask_out[2*RAW_MODULE_SIZE + 347] != 0);

    CHECK(mask.GetStatistics().too_high_pedestal_rms == 0);
    CHECK(mask.GetStatistics().wrong_gain == 3);
    CHECK(mask.GetStatistics().user_mask == 0);

    image_format.MaskPixelsWithoutG0(false);
    experiment.ImportImageFormatSettings(image_format);

    REQUIRE(!experiment.IsMaskPixelsWithoutG0());
    mask.LoadDetectorBadPixelMask(experiment, &calibration);
    mask_out = mask.GetMask(experiment);

    CHECK(mask_out[2*RAW_MODULE_SIZE + 345] == 0);
    CHECK(mask_out[2*RAW_MODULE_SIZE + 346] != 0);
    CHECK(mask_out[2*RAW_MODULE_SIZE + 347] != 0);

    CHECK(mask.GetStatistics().too_high_pedestal_rms == 0);
    CHECK(mask.GetStatistics().wrong_gain == 2);
    CHECK(mask.GetStatistics().user_mask == 0);
}

TEST_CASE("PixelMask_MaskG0RMS","[PixelMask]") {
    ImageFormatSettings image_format;
    image_format.MaskModuleEdges(false).MaskChipEdges(false).MaskPixelsWithoutG0(true).GeometryTransformed(false);
    image_format.PedestalG0RMSLimit(100);

    DiffractionExperiment experiment(DetectorGeometry(4, 1));
    experiment.ImportImageFormatSettings(image_format);

    PixelMask mask(experiment);

    JFCalibration calibration(experiment);
    for (int m = 0; m < experiment.GetModulesNum(); m++) {
        calibration.Pedestal(m, 0) = JFModulePedestal(1500);
        calibration.Pedestal(m, 1) = JFModulePedestal(12000);
        calibration.Pedestal(m, 2) = JFModulePedestal(13000);
    }

    calibration.Pedestal(2,0).GetPedestal()[345] = UINT16_MAX;
    calibration.Pedestal(2,1).GetPedestal()[346] = UINT16_MAX;
    calibration.Pedestal(2,2).GetPedestal()[347] = UINT16_MAX;
    calibration.Pedestal(2, 0).GetPedestalRMS()[245] = 150;
    calibration.Pedestal(2, 0).GetPedestalRMS()[345] = UINT16_MAX;
    calibration.Pedestal(2, 0).GetPedestalRMS()[346] = UINT16_MAX;

    mask.LoadDetectorBadPixelMask(experiment, &calibration);

    auto mask_out = mask.GetMask(experiment);
    REQUIRE(mask_out.size() == experiment.GetModulesNum() * RAW_MODULE_SIZE);

    CHECK(mask_out[2*RAW_MODULE_SIZE + 245] == (1 << PixelMask::TooHighPedestalRMSPixelBit));

    CHECK(mask_out[2*RAW_MODULE_SIZE + 345] == (1 << PixelMask::ErrorPixelBit));
    CHECK(mask_out[2*RAW_MODULE_SIZE + 346] == ((1 << PixelMask::ErrorPixelBit) | (1 << PixelMask::TooHighPedestalRMSPixelBit)));
    CHECK(mask_out[2*RAW_MODULE_SIZE + 347] == (1 << PixelMask::ErrorPixelBit));

    CHECK(mask.GetStatistics().too_high_pedestal_rms == 2);
    CHECK(mask.GetStatistics().wrong_gain == 3);
    CHECK(mask.GetStatistics().user_mask == 0);
}

TEST_CASE("PixelMask_CalculateNexusMask","[PixelMask]") {
    DiffractionExperiment experiment(DetectorGeometry(4, 1, 8, 36, false));
    experiment.MaskModuleEdges(false).MaskChipEdges(false);

    PixelMask mask(experiment);

    std::vector<uint32_t> v(4 * RAW_MODULE_SIZE, 0);
    v[2] = 1;
    v[4] = 1;
    v[5] = 1;
    v[3 * RAW_MODULE_SIZE + 780] = 1;

    REQUIRE_NOTHROW(mask.LoadDetectorBadPixelMask(v));

    auto mask_v = mask.GetMask(experiment);

    REQUIRE(mask_v.size() == experiment.GetPixelsNum() );
    REQUIRE(mask_v[0] == 0);
    REQUIRE(mask_v[1] == 0);
    REQUIRE(mask_v[2] == (1 << PixelMask::ErrorPixelBit));
    REQUIRE(mask_v[3] == 0);
    REQUIRE(mask_v[4] == (1 << PixelMask::ErrorPixelBit));
    REQUIRE(mask_v[5] == (1 << PixelMask::ErrorPixelBit));
    REQUIRE(mask_v[6] == 0);
    REQUIRE(mask_v[1030 * 700 + 300] == 0);
    REQUIRE(mask_v[(514+36)*1030*3 + 780 + 6] == (1 << PixelMask::ErrorPixelBit));
}

TEST_CASE("PixelMask_CalculateNexusMask_GetMaskRaw","[PixelMask]") {
    DiffractionExperiment experiment(DetectorGeometry(4, 1, 8, 36, false));
    experiment.MaskModuleEdges(false).MaskChipEdges(false);

    PixelMask mask(experiment);

    std::vector<uint32_t> v(4 * RAW_MODULE_SIZE, 0);
    v[2] = 1;
    v[4] = 1;
    v[5] = 1;
    v[3 * RAW_MODULE_SIZE + 780] = 1;

    REQUIRE_NOTHROW(mask.LoadDetectorBadPixelMask(v));

    auto mask_v = mask.GetMaskRaw();

    REQUIRE(mask_v.size() == 4 * RAW_MODULE_SIZE );
    REQUIRE(mask_v[0] == 0);
    REQUIRE(mask_v[1] == 0);
    REQUIRE(mask_v[2] == (1 << PixelMask::ErrorPixelBit));
    REQUIRE(mask_v[3] == 0);
    REQUIRE(mask_v[4] == (1 << PixelMask::ErrorPixelBit));
    REQUIRE(mask_v[5] == (1 << PixelMask::ErrorPixelBit));
    REQUIRE(mask_v[6] == 0);

    REQUIRE(mask_v[3 * RAW_MODULE_SIZE + 780] == (1 << PixelMask::ErrorPixelBit));
}

TEST_CASE("PixelMask_CalculateNexusMask_GetMask_NoGeomTransfer","[PixelMask]") {
    DiffractionExperiment experiment(DetectorGeometry(4, 1, 8, 36, false));
    experiment.MaskModuleEdges(false).MaskChipEdges(false);
    experiment.GeometryTransformation(false);
    PixelMask mask(experiment);

    std::vector<uint32_t> v(4 * RAW_MODULE_SIZE, 0);
    v[2] = 1;
    v[4] = 1;
    v[5] = 1;
    v[3 * RAW_MODULE_SIZE + 780] = 1;

    REQUIRE_NOTHROW(mask.LoadDetectorBadPixelMask(v));

    auto mask_v = mask.GetMask(experiment);

    REQUIRE(mask_v.size() == 4 * RAW_MODULE_SIZE );
    REQUIRE(mask_v[0] == 0);
    REQUIRE(mask_v[1] == 0);
    REQUIRE(mask_v[2] == (1 << PixelMask::ErrorPixelBit));
    REQUIRE(mask_v[3] == 0);
    REQUIRE(mask_v[4] == (1 << PixelMask::ErrorPixelBit));
    REQUIRE(mask_v[5] == (1 << PixelMask::ErrorPixelBit));
    REQUIRE(mask_v[6] == 0);

    REQUIRE(mask_v[3 * RAW_MODULE_SIZE + 780] == (1 << PixelMask::ErrorPixelBit));
}

TEST_CASE("PixelMask_CalculateNexusMask_UserMaskRaw","[PixelMask]") {
    DiffractionExperiment experiment(DetectorGeometry(4, 1, 8, 36, false));
    experiment.MaskModuleEdges(false).MaskChipEdges(false);

    PixelMask mask(experiment);

    std::vector<uint32_t> v(4 * RAW_MODULE_SIZE, 0);
    v[2] = 1;
    v[4] = 1;
    v[5] = 1;

    REQUIRE_NOTHROW(mask.LoadUserMask(experiment, v));

    auto mask_v = mask.GetMask(experiment);

    REQUIRE(mask_v.size() == experiment.GetPixelsNum() );
    REQUIRE(mask_v[0] == 0);
    REQUIRE(mask_v[1] == 0);
    REQUIRE(mask_v[2] == (1 << PixelMask::UserMaskedPixelBit));
    REQUIRE(mask_v[3] == 0);
    REQUIRE(mask_v[4] == (1 << PixelMask::UserMaskedPixelBit));
    REQUIRE(mask_v[5] == (1 << PixelMask::UserMaskedPixelBit));
    REQUIRE(mask_v[6] == 0);
    REQUIRE(mask_v[1030 * 700 + 300] == 0);
    REQUIRE(mask_v[(1030+8)*514] == (1 << PixelMask::ModuleGapPixelBit));

    auto user_mask_v = mask.GetUserMask(experiment);

    REQUIRE(user_mask_v.size() == experiment.GetPixelsNum() );
    REQUIRE(user_mask_v[0] == 0);
    REQUIRE(user_mask_v[1] == 0);
    REQUIRE(user_mask_v[2] == 1);
    REQUIRE(user_mask_v[3] == 0);
    REQUIRE(user_mask_v[4] == 1);
    REQUIRE(user_mask_v[5] == 1);
    REQUIRE(user_mask_v[6] == 0);
    REQUIRE(user_mask_v[1030 * 700 + 300] == 0);

    REQUIRE(user_mask_v[(1030+8)*514] == 0);
}

TEST_CASE("PixelMask_CalculateNexusMask_UserMaskConv","[PixelMask]") {
    DiffractionExperiment experiment(DetectorGeometry(4, 1, 8, 36, false));
    experiment.MaskModuleEdges(false).MaskChipEdges(false);

    PixelMask mask(experiment);

    std::vector<uint32_t> v(experiment.GetPixelsNum(), 0);
    v[1030 * 700 + 300] = 1;

    REQUIRE_NOTHROW(mask.LoadUserMask(experiment, v));

    auto mask_v = mask.GetMask(experiment);

    REQUIRE(mask_v.size() == experiment.GetPixelsNum() );
    REQUIRE(mask_v[1030 * 700 + 300] == (1 << PixelMask::UserMaskedPixelBit));
    REQUIRE(mask_v[(1030+8)*514] == (1 << PixelMask::ModuleGapPixelBit));

    auto user_mask_v = mask.GetUserMask(experiment);

    REQUIRE(user_mask_v.size() == experiment.GetPixelsNum() );
    REQUIRE(user_mask_v[1030 * 700 + 300] == 1);

    REQUIRE(user_mask_v[(1030+8)*514] == 0);
}

TEST_CASE("PixelMask_MaskDetectorGaps","[PixelMask]") {
    DiffractionExperiment experiment(DetectorGeometry(8, 2, 8, 36, true));
    experiment.MaskChipEdges(false).MaskModuleEdges(false);
    experiment.IncidentEnergy_keV(WVL_1A_IN_KEV);

    PixelMask mask(experiment);
    auto mask_export = mask.GetMask(experiment);
    REQUIRE(mask_export.size() == experiment.GetPixelsNum());

    REQUIRE(mask_export[0] == 0);
    REQUIRE(mask_export[1029] == 0);
    REQUIRE(mask_export[1030] == 1);
    REQUIRE(mask_export[1031] == 1);
    REQUIRE(mask_export[1037] == 1);
    REQUIRE(mask_export[1038] == 0);

    REQUIRE(mask_export[(1030*2+8)*514] == 1);
    REQUIRE(mask_export[(1030*2+8)*514 + 566] == 1);
    REQUIRE(mask_export[(1030*2+8)*(514*3+36*2+12) + 566] == 1);
}
