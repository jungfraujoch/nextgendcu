// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>

#include "../receiver/JFJochReceiverTest.h"
#include "../acquisition_device/HLSSimulatedDevice.h"
#include "../jungfrau/JFPedestalCalc.h"
#include "../image_pusher/TestImagePusher.h"
#include "../receiver/JFJochReceiverService.h"

using namespace std::literals::chrono_literals;

TEST_CASE("JFJochReceiverTest_Raw", "[JFJochReceiver]") {
    DiffractionExperiment x(DetectorGeometry(4));
    const uint16_t nthreads = 4;

    x.Mode(DetectorMode::Raw);
    x.PedestalG0Frames(0).NumTriggers(1).UseInternalPacketGenerator(true)
            .ImagesPerTrigger(100).ImagesPerFile(10).IncidentEnergy_keV(12.4).Compression(CompressionAlgorithm::NO_COMPRESSION);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i,64);
        aq_devices.Add(std::move(test));
    }

    Logger logger("JFJochReceiverTest_Raw");

    PixelMask mask(x);
    
    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.status.images_sent == x.GetImageNum());
    REQUIRE(output.status.compressed_ratio == 1.0);
    REQUIRE(output.status.compressed_size == x.GetImageNum() * x.GetByteDepthImage() * x.GetPixelsNum());
    REQUIRE(output.status.max_image_number_sent == x.GetImageNum());
    REQUIRE(!output.status.cancelled);
}

TEST_CASE("JFJochReceiverTest_Conversion", "[JFJochReceiver]") {
    Logger logger("JFJochReceiverTest_Conversion");

    DiffractionExperiment x(DetectorGeometry(2));
    const uint16_t nthreads = 4;

    x.Mode(DetectorMode::Conversion);
    x.PedestalG0Frames(0).NumTriggers(1).UseInternalPacketGenerator(true)
            .ImagesPerTrigger(32).ImagesPerFile(10).IncidentEnergy_keV(12.4).Compression(CompressionAlgorithm::BSHUF_ZSTD);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i,64);
        aq_devices.Add(std::move(test));
    }

    REQUIRE(x.IsGeometryTransformed());

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.status.images_sent == x.GetImageNum());

    REQUIRE(!output.status.cancelled);
}

TEST_CASE("JFJochReceiverTest_Conversion_NoGeomTransform", "[JFJochReceiver]") {
    Logger logger("JFJochReceiverTest_Conversion_NoGeomTransform");

    DiffractionExperiment x(DetectorGeometry(2));
    const uint16_t nthreads = 4;

    x.Mode(DetectorMode::Conversion);
    x.PedestalG0Frames(0).NumTriggers(1).UseInternalPacketGenerator(true)
            .ImagesPerTrigger(32).ImagesPerFile(10).IncidentEnergy_keV(12.4).Compression(CompressionAlgorithm::BSHUF_ZSTD)
            .GeometryTransformation(false);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i,64);
        aq_devices.Add(std::move(test));
    }

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.status.images_sent == x.GetImageNum());

    REQUIRE(!output.status.cancelled);
}

TEST_CASE("JFJochReceiverTest_Conversion_Poisson", "[JFJochReceiver]") {
    Logger logger("JFJochReceiverTest_Conversion_Poisson");

    DiffractionExperiment x(DetectorGeometry(2));
    const uint16_t nthreads = 4;

    x.Mode(DetectorMode::Conversion);
    x.PedestalG0Frames(0).NumTriggers(1).UseInternalPacketGenerator(true)
            .ImagesPerTrigger(32).ImagesPerFile(10).IncidentEnergy_keV(12.4).Compression(
                    CompressionAlgorithm::BSHUF_ZSTD);
    x.LossyCompressionPoisson(1);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i, 64);
        aq_devices.Add(std::move(test));
    }

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.status.images_sent == x.GetImageNum());

    REQUIRE(!output.status.cancelled);
}

TEST_CASE("JFJochReceiverTest_Conversion_Threshold", "[JFJochReceiver]") {
    Logger logger("JFJochReceiverTest_Conversion_Threshold");

    DiffractionExperiment x(DetectorGeometry(2));
    const uint16_t nthreads = 4;

    x.Mode(DetectorMode::Conversion);
    x.PedestalG0Frames(0).NumTriggers(1).UseInternalPacketGenerator(true)
            .ImagesPerTrigger(32).ImagesPerFile(10).IncidentEnergy_keV(12.4).Compression(
                    CompressionAlgorithm::BSHUF_ZSTD);
    x.PixelValueLowThreshold(50);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i, 64);
        aq_devices.Add(std::move(test));
    }

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.status.images_sent == x.GetImageNum());

    REQUIRE(!output.status.cancelled);
}

TEST_CASE("JFJochReceiverTest_Conversion_ApplyMask", "[JFJochReceiver]") {
    Logger logger("JFJochReceiverTest_Conversion_ApplyMask");

    DiffractionExperiment x(DetectorGeometry(2));
    const uint16_t nthreads = 4;

    x.Mode(DetectorMode::Conversion);
    x.PedestalG0Frames(0).NumTriggers(1).UseInternalPacketGenerator(true)
            .ImagesPerTrigger(32).ImagesPerFile(10).IncidentEnergy_keV(12.4).Compression(CompressionAlgorithm::BSHUF_ZSTD);
    x.ApplyPixelMask(true).MaskChipEdges(true).MaskModuleEdges(true);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i, 64);
        aq_devices.Add(std::move(test));
    }

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.status.images_sent == x.GetImageNum());

    REQUIRE(!output.status.cancelled);
}

TEST_CASE("JFJochReceiverTest_Conversion_Threshold_Summation", "[JFJochReceiver]") {
    Logger logger("JFJochReceiverTest_Conversion_Threshold");

    DiffractionExperiment x(DetectorGeometry(2));
    const uint16_t nthreads = 4;

    x.Mode(DetectorMode::Conversion);
    x.PedestalG0Frames(0).NumTriggers(1).UseInternalPacketGenerator(true)
            .ImagesPerTrigger(1).Summation(4).ImagesPerFile(10).IncidentEnergy_keV(12.4).Compression(
                    CompressionAlgorithm::BSHUF_ZSTD);
    x.PixelValueLowThreshold(10);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i, 64);
        aq_devices.Add(std::move(test));
    }

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.status.images_sent == x.GetImageNum());

    REQUIRE(!output.status.cancelled);
}

TEST_CASE("JFJochReceiverTest_Conversion_FixedGainG1", "[JFJochReceiver]") {
    Logger logger("JFJochReceiverTest_Conversion");

    DiffractionExperiment x(DetectorGeometry(2));
    const uint16_t nthreads = 4;

    x.Mode(DetectorMode::Conversion);
    x.PedestalG0Frames(0).NumTriggers(1).UseInternalPacketGenerator(true)
            .ImagesPerTrigger(32).ImagesPerFile(10).IncidentEnergy_keV(12.4).Compression(CompressionAlgorithm::BSHUF_ZSTD)
            .FixedGainG1(true);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i,64);
        aq_devices.Add(std::move(test));
    }

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.status.images_sent == x.GetImageNum());

    REQUIRE(!output.status.cancelled);
}


TEST_CASE("JFJochReceiverTest_Conversion_FixedGainG1_onlyG1", "[JFJochReceiver]") {
    Logger logger("JFJochReceiverTest_Conversion");

    DiffractionExperiment x(DetectorGeometry(2));
    const uint16_t nthreads = 4;

    x.Mode(DetectorMode::Conversion);
    x.PedestalG0Frames(0).NumTriggers(1).UseInternalPacketGenerator(true)
            .ImagesPerTrigger(32).ImagesPerFile(10).IncidentEnergy_keV(12.4).Compression(CompressionAlgorithm::BSHUF_ZSTD)
            .FixedGainG1(true);

    std::vector<uint16_t> input(RAW_MODULE_SIZE*2, 0x4000 + 3000);
    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i,64);
        aq_devices.Add(std::move(test));
    }

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, input, nthreads));
    REQUIRE(ret);
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.status.images_sent == x.GetImageNum());

    REQUIRE(!output.status.cancelled);
}

TEST_CASE("JFJochReceiverTest_Conversion_U16", "[JFJochReceiver]") {
    Logger logger("JFJochReceiverTest_Conversion");

    DiffractionExperiment x(DetectorGeometry(2));
    const uint16_t nthreads = 4;

    x.Mode(DetectorMode::Conversion);
    x.PedestalG0Frames(0).NumTriggers(1).UseInternalPacketGenerator(true)
            .ImagesPerTrigger(32).ImagesPerFile(10).IncidentEnergy_keV(12.4).Compression(CompressionAlgorithm::BSHUF_ZSTD)
            .BitDepthImage(16).PixelSigned(false);

    REQUIRE(!x.IsPixelSigned());
    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i,64);
        aq_devices.Add(std::move(test));
    }

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.status.images_sent == x.GetImageNum());

    REQUIRE(!output.status.cancelled);
}

TEST_CASE("JFJochReceiverTest_Conversion_U8", "[JFJochReceiver]") {
    Logger logger("JFJochReceiverTest_Conversion_U8");

    DiffractionExperiment x(DetectorGeometry(2));
    const uint16_t nthreads = 4;

    x.Mode(DetectorMode::Conversion);
    x.PedestalG0Frames(0).NumTriggers(1).UseInternalPacketGenerator(true)
            .ImagesPerTrigger(32).ImagesPerFile(10).IncidentEnergy_keV(12.4).Compression(CompressionAlgorithm::BSHUF_ZSTD)
            .BitDepthImage(8).PixelSigned(false);

    REQUIRE(!x.IsPixelSigned());
    REQUIRE(x.GetByteDepthImage() == 1);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i,64);
        aq_devices.Add(std::move(test));
    }

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.status.images_sent == x.GetImageNum());

    REQUIRE(!output.status.cancelled);
}

TEST_CASE("JFJochReceiverTest_Conversion_I32", "[JFJochReceiver]") {
    Logger logger("JFJochReceiverTest_Conversion");

    DiffractionExperiment x(DetectorGeometry(2));
    const uint16_t nthreads = 4;

    x.Mode(DetectorMode::Conversion);
    x.PedestalG0Frames(0).NumTriggers(1).UseInternalPacketGenerator(true)
            .ImagesPerTrigger(32).ImagesPerFile(10).IncidentEnergy_keV(12.4).Compression(CompressionAlgorithm::BSHUF_ZSTD)
            .PixelSigned(true).BitDepthImage(32);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i,64);
        aq_devices.Add(std::move(test));
    }

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.status.images_sent == x.GetImageNum());

    REQUIRE(!output.status.cancelled);
}

TEST_CASE("JFJochReceiverTest_Conversion_Summation2", "[JFJochReceiver]") {
    Logger logger("JFJochReceiverTest_Conversion");

    DiffractionExperiment x(DetectorGeometry(2));
    const uint16_t nthreads = 4;

    x.Mode(DetectorMode::Conversion);
    x.PedestalG0Frames(0).NumTriggers(1).UseInternalPacketGenerator(true)
            .ImagesPerTrigger(32).ImagesPerFile(10).IncidentEnergy_keV(12.4).Compression(CompressionAlgorithm::BSHUF_ZSTD).Summation(2);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i,64);
        aq_devices.Add(std::move(test));
    }

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.status.images_sent == x.GetImageNum());

    REQUIRE(!output.status.cancelled);
}

TEST_CASE("JFJochReceiverTest_Conversion_Summation7_CPU", "[JFJochReceiver]") {
    Logger logger("JFJochReceiverTest_Conversion");

    DiffractionExperiment x(DetectorGeometry(2));
    const uint16_t nthreads = 4;

    x.Mode(DetectorMode::Conversion);
    x.PedestalG0Frames(0).NumTriggers(1).UseInternalPacketGenerator(true)
            .ImagesPerTrigger(8).ImagesPerFile(10).IncidentEnergy_keV(12.4).Compression(CompressionAlgorithm::BSHUF_ZSTD)
            .Summation(7).CPUSummation(true);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i,64);
        aq_devices.Add(std::move(test));
    }

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    CHECK(ret);
    CHECK(output.efficiency == 1.0);
    CHECK(output.status.images_sent == x.GetImageNum());

    CHECK(!output.status.cancelled);
}

TEST_CASE("JFJochReceiverTest_Conversion_StorageCell", "[JFJochReceiver]") {
    Logger logger("JFJochReceiverTest_Conversion_StorageCell");

    DiffractionExperiment x(DetectorGeometry(2));
    const uint16_t nthreads = 4;

    x.Mode(DetectorMode::Conversion);
    x.PedestalG0Frames(0).NumTriggers(1).UseInternalPacketGenerator(true)
            .ImagesPerTrigger(32).ImagesPerFile(10).IncidentEnergy_keV(12.4).Compression(CompressionAlgorithm::BSHUF_ZSTD).StorageCells(16);

    REQUIRE(x.GetImageNum() == 16);
    REQUIRE(x.GetStorageCellNumber() == 16);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i,64);
        aq_devices.Add(std::move(test));
    }

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.status.images_sent == x.GetImageNum());

    REQUIRE(!output.status.cancelled);
}

TEST_CASE("JFJochReceiverTest_PedestalG1", "[JFJochReceiver]") {

    const uint16_t nthreads = 4;
    size_t nframes = 256;

    std::vector<uint16_t> pedestal_in(RAW_MODULE_SIZE*nframes);

    for (auto &i: pedestal_in) i = 16384 | 14000;

    // Predictable random number generator
    std::mt19937 g1(1900);
    std::normal_distribution<double> distribution(14000, 100);

    for (auto &i: pedestal_in) {
        uint16_t number = distribution(g1);
        if (number < 20) number = 20;
        if (number > 16300) number = 16300;
        i = 16384 | number;
    }

    DiffractionExperiment x(DetectorGeometry(1));
    x.Mode(DetectorMode::PedestalG1).PedestalG0Frames(0)
            .PedestalG1Frames(nframes).NumTriggers(1).UseInternalPacketGenerator(false)
            .ImagesPerTrigger(0).IncidentEnergy_keV(12.4);
    
    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i,64);
        test->CreateJFPackets(x, 1, nframes, 0, pedestal_in.data());
        test->CreateFinalPacket(x);
        aq_devices.Add(std::move(test));
    }

    JFPedestalCalc pc(x);
    for (int i = 0; i < nframes; i++)
        pc.AnalyzeImage(pedestal_in.data() + i * RAW_MODULE_SIZE);

    Logger logger("JFJochReceiverTest_PedestalG1");

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.status.images_sent == x.GetImageNum());
    REQUIRE(output.efficiency == 1.0);

    JFModulePedestal ref_pedestal;
    pc.Export(ref_pedestal, 0);
    JFModulePedestal out_pedestal = output.pedestal_result.at(0);
    REQUIRE(abs(ref_pedestal.Mean() - out_pedestal.Mean()) < 0.65);
    REQUIRE(out_pedestal.CountMaskedPixels() == 0);
    REQUIRE(out_pedestal.GetFrameCount() == nframes);
    REQUIRE(!output.status.cancelled);
}

TEST_CASE("JFJochReceiverTest_PedestalG2_storage_cell", "[JFJochReceiver]") {

    const uint16_t nthreads = 4;
    size_t nframes = 150;

    std::vector<uint16_t> pedestal_in(RAW_MODULE_SIZE*nframes), pedestal_in2(RAW_MODULE_SIZE*nframes);;

    // Predictable random number generator
    std::mt19937 g2(1900);
    std::normal_distribution<double> distribution1(13000, 100);
    std::normal_distribution<double> distribution2(12000, 100);

    for (auto &i: pedestal_in) {
        uint16_t number = distribution1(g2);
        if (number < 20) number = 20;
        if (number > 16300) number = 16300;
        i = 32768 | 16384 | number;
    }

    for (auto &i: pedestal_in2) {
        uint16_t number = distribution2(g2);
        if (number < 20) number = 20;
        if (number > 16300) number = 16300;
        i = 32768 | 16384 | number;
    }
    DiffractionExperiment x(DetectorGeometry(1));
    x.Mode(DetectorMode::PedestalG2).PedestalG0Frames(0)
            .PedestalG2Frames(nframes).NumTriggers(1).UseInternalPacketGenerator(false)
            .ImagesPerTrigger(0).IncidentEnergy_keV(12.4).StorageCells(16);

    REQUIRE(x.GetStorageCellNumber() == 2);
    REQUIRE(x.GetFrameNum() == nframes * 2);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        auto test = std::make_unique<HLSSimulatedDevice>(i, nframes * 2);
        for (int j = 0; j < nframes; j++) {
            test->CreateJFPackets(x, 2 * j + 1, 1, 0, pedestal_in2.data() + j * RAW_MODULE_SIZE);
            test->CreateJFPackets(x, 2 * j + 2, 1, 0, pedestal_in.data() + j * RAW_MODULE_SIZE);
        }
        test->CreateFinalPacket(x);
        aq_devices.Add(std::move(test));
    }

    JFPedestalCalc pc(x);
    for (int i = 0; i < nframes; i++)
        pc.AnalyzeImage(pedestal_in.data() + i * RAW_MODULE_SIZE);

    Logger logger("JFJochReceiverTest_PedestalG2_storage_cell");

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.status.images_sent == x.GetImageNum());
    REQUIRE(output.efficiency == 1.0);

    JFModulePedestal ref_pedestal;
    pc.Export(ref_pedestal, 0);
    JFModulePedestal out_pedestal = output.pedestal_result.at(0);
    REQUIRE(abs(ref_pedestal.Mean() - out_pedestal.Mean()) < 0.65);
    REQUIRE(out_pedestal.CountMaskedPixels() == 0);
}

TEST_CASE("JFJochReceiverTest_PedestalG0", "[JFJochReceiver]") {
    DiffractionExperiment x(DetectorGeometry(1));
    const uint16_t nthreads = 4;
    size_t nframes = 500;

    std::vector<uint16_t> pedestal_in(RAW_MODULE_SIZE*nframes);

    for (auto &i: pedestal_in) i = 16384 | 14000;

    // Predictable random number generator
    std::mt19937 g1(1900);
    std::normal_distribution<double> distribution(3000, 50);

    for (auto &i: pedestal_in) {
        uint16_t number = distribution(g1);
        if (number < 20) number = 20;
        if (number > 16300) number = 16300;
        i = number;
    }

    x.Mode(DetectorMode::PedestalG0);
    x.PedestalG0Frames(nframes)
            .NumTriggers(1).UseInternalPacketGenerator(false)
            .ImagesPerTrigger(0).IncidentEnergy_keV(12.4);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        auto test = std::make_unique<HLSSimulatedDevice>(i,64);
        test->CreateJFPackets(x, 1, nframes, 0, pedestal_in.data());
        test->CreateFinalPacket(x);
        aq_devices.Add(std::move(test));
    }

    JFPedestalCalc pc(x);
    for (int i = 0; i < nframes; i++)
        pc.AnalyzeImage(pedestal_in.data() + i * RAW_MODULE_SIZE);

    PixelMask mask(x);

    Logger logger("JFJochReceiverTest_PedestalG0");
    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.status.images_sent == x.GetImageNum());
    REQUIRE(output.efficiency == 1.0);

    JFModulePedestal ref_pedestal;
    pc.Export(ref_pedestal, 0);
    JFModulePedestal out_pedestal = output.pedestal_result.at(0);
    REQUIRE(abs(ref_pedestal.Mean() - out_pedestal.Mean()) < 0.65);
    REQUIRE(out_pedestal.CountMaskedPixels() == 0);
    REQUIRE(out_pedestal.GetFrameCount() == nframes);
}

TEST_CASE("JFJochReceiverTest_PedestalG0_StorageCell", "[JFJochReceiver]") {
    DiffractionExperiment x(DetectorGeometry(1));
    const uint16_t nthreads = 4;
    size_t nframes = 140;

    std::vector<uint16_t> pedestal_in_0(RAW_MODULE_SIZE);
    std::vector<uint16_t> pedestal_in_1(RAW_MODULE_SIZE);
    std::vector<uint16_t> pedestal_in_2(RAW_MODULE_SIZE);
    std::vector<uint16_t> pedestal_in_3(RAW_MODULE_SIZE);

    for (auto &i: pedestal_in_0) i = 5670;
    for (auto &i: pedestal_in_1) i = 4560;
    for (auto &i: pedestal_in_2) i = 3450;
    for (auto &i: pedestal_in_3) i = 2000;

    x.Mode(DetectorMode::PedestalG0).StorageCells(4);
    x.PedestalG0Frames(nframes)
            .NumTriggers(1).UseInternalPacketGenerator(false)
            .ImagesPerTrigger(0).IncidentEnergy_keV(12.4);

    AcquisitionDeviceGroup aq_devices;
    auto test = std::make_unique<HLSSimulatedDevice>(0, 64);
    for (int i = 0; i < nframes; i++) {
        test->CreateJFPackets(x, i * 4 + 1, 1, 0, pedestal_in_0.data());
        test->CreateJFPackets(x, i * 4 + 2, 1, 0, pedestal_in_1.data());
        test->CreateJFPackets(x, i * 4 + 3, 1, 0, pedestal_in_2.data());
        test->CreateJFPackets(x, i * 4 + 4, 1, 0, pedestal_in_3.data());
    }
    test->CreateFinalPacket(x);
    aq_devices.Add(std::move(test));

    PixelMask mask(x);

    Logger logger("JFJochReceiverTest_PedestalG0_StorageCell");
    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.status.images_sent == x.GetImageNum());
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.pedestal_result.size() == 4);
    JFModulePedestal out_pedestal_0 = output.pedestal_result[0];
    JFModulePedestal out_pedestal_1 = output.pedestal_result[1];
    JFModulePedestal out_pedestal_2 = output.pedestal_result[2];
    JFModulePedestal out_pedestal_3 = output.pedestal_result[3];

    REQUIRE(out_pedestal_0.Mean() == Catch::Approx(5670));
    REQUIRE(out_pedestal_0.GetFrameCount() == nframes);
    REQUIRE(out_pedestal_1.Mean() == Catch::Approx(4560));
    REQUIRE(out_pedestal_1.GetFrameCount() == nframes);
    REQUIRE(out_pedestal_2.Mean() == Catch::Approx(3450));
    REQUIRE(out_pedestal_2.GetFrameCount() == nframes);
    REQUIRE(out_pedestal_3.Mean() == Catch::Approx(2000));
    REQUIRE(out_pedestal_3.GetFrameCount() == nframes);
}

TEST_CASE("JFJochReceiverTest_PedestalFixedG1_StorageCell", "[JFJochReceiver]") {
    DiffractionExperiment x(DetectorGeometry(1));
    const uint16_t nthreads = 4;
    size_t nframes = 140;

    std::vector<uint16_t> pedestal_in_0(RAW_MODULE_SIZE);
    std::vector<uint16_t> pedestal_in_1(RAW_MODULE_SIZE);
    std::vector<uint16_t> pedestal_in_2(RAW_MODULE_SIZE);
    std::vector<uint16_t> pedestal_in_3(RAW_MODULE_SIZE);

    for (auto &i: pedestal_in_0) i = 16384 | 5670;
    for (auto &i: pedestal_in_1) i = 16384 | 4560;
    for (auto &i: pedestal_in_2) i = 16384 | 3450;
    for (auto &i: pedestal_in_3) i = 16384 | 2000;

    x.Mode(DetectorMode::PedestalG1).StorageCells(4).FixedGainG1(true);
    x.PedestalG1Frames(nframes)
            .NumTriggers(1).UseInternalPacketGenerator(false)
            .ImagesPerTrigger(0).IncidentEnergy_keV(12.4);

    REQUIRE(x.GetStorageCellNumber() == 4);

    AcquisitionDeviceGroup aq_devices;
    auto test = std::make_unique<HLSSimulatedDevice>(0, 64);
    for (int i = 0; i < nframes; i++) {
        test->CreateJFPackets(x, i * 4 + 1, 1, 0, pedestal_in_0.data());
        test->CreateJFPackets(x, i * 4 + 2, 1, 0, pedestal_in_1.data());
        test->CreateJFPackets(x, i * 4 + 3, 1, 0, pedestal_in_2.data());
        test->CreateJFPackets(x, i * 4 + 4, 1, 0, pedestal_in_3.data());
    }
    test->CreateFinalPacket(x);
    aq_devices.Add(std::move(test));

    PixelMask mask(x);

    Logger logger("JFJochReceiverTest_PedestalFixedG1_StorageCell");
    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.status.images_sent == x.GetImageNum());
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.pedestal_result.size() == 4);
    JFModulePedestal out_pedestal_0 = output.pedestal_result[0];
    JFModulePedestal out_pedestal_1 = output.pedestal_result[1];
    JFModulePedestal out_pedestal_2 = output.pedestal_result[2];
    JFModulePedestal out_pedestal_3 = output.pedestal_result[3];

    REQUIRE(out_pedestal_0.Mean() == Catch::Approx(5670));
    REQUIRE(out_pedestal_0.GetFrameCount() == nframes);
    REQUIRE(out_pedestal_1.Mean() == Catch::Approx(4560));
    REQUIRE(out_pedestal_1.GetFrameCount() == nframes);
    REQUIRE(out_pedestal_2.Mean() == Catch::Approx(3450));
    REQUIRE(out_pedestal_2.GetFrameCount() == nframes);
    REQUIRE(out_pedestal_3.Mean() == Catch::Approx(2000));
    REQUIRE(out_pedestal_3.GetFrameCount() == nframes);
}

TEST_CASE("JFJochReceiverTest_PedestalG1_NoFrames", "[JFJochReceiver]") {
    DiffractionExperiment x(DetectorGeometry(1));
    const uint16_t nthreads = 4;
    size_t nframes = 256;

    std::vector<uint16_t> pedestal_in(RAW_MODULE_SIZE*nframes);

    for (auto &i: pedestal_in) i = 16384 | 14000;

    x.Mode(DetectorMode::PedestalG1);
    x.PedestalG0Frames(0).PedestalG1Frames(256).NumTriggers(1)
            .UseInternalPacketGenerator(false).ImagesPerTrigger(0).IncidentEnergy_keV(12.4);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        auto test = std::make_unique<HLSSimulatedDevice>(i, nframes + 8);
        test->CreateFinalPacket(x);
        aq_devices.Add(std::move(test));
    }

    PixelMask mask(x);

    Logger logger("JFJochReceiverTest_PedestalG1_NoFrames");
    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.expected_packets[0] != 0);
    REQUIRE(output.received_packets[0] == 0);
    REQUIRE(output.status.images_sent == 0);
    REQUIRE(output.efficiency < 1.0);

    JFModulePedestal out_pedestal = output.pedestal_result.at(0);
    REQUIRE(out_pedestal.CountMaskedPixels() == RAW_MODULE_SIZE);
}

TEST_CASE("JFJochReceiverTest_PacketLost_Raw", "[JFJochReceiver]") {
    DiffractionExperiment x(DetectorGeometry(2));
    const uint16_t nthreads = 4;

    std::vector<uint16_t> frame_in(RAW_MODULE_SIZE);
    for (auto &i: frame_in) i = 776;

    x.Mode(DetectorMode::Raw);
    x.PedestalG0Frames(0).NumTriggers(1)
            .UseInternalPacketGenerator(false).ImagesPerTrigger(4).IncidentEnergy_keV(12.4)
            .Compression(CompressionAlgorithm::NO_COMPRESSION);

    PixelMask pixel_mask(x);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        auto test = std::make_unique<HLSSimulatedDevice>(i, 64);
        test->CreateJFPackets(x, 1, 1, 0, frame_in.data());
        test->CreateJFPackets(x, 1, 1, 1, frame_in.data());
        test->CreateJFPackets(x, 2, 1, 0, frame_in.data());
        test->CreateJFPackets(x, 2, 1, 1, frame_in.data());
        test->CreateJFPackets(x, 3, 1, 0, frame_in.data());
        test->CreateJFPackets(x, 3, 1, 1, frame_in.data());

        for (int j = 0; j < 127; j++)
            test->CreateJFPacket(x, 4, j, 0, frame_in.data() + (i * 128 + j) * 4096);
        test->CreateJFPackets(x, 4, 1, 1, frame_in.data());
        test->CreateFinalPacket(x);

        aq_devices.Add(std::move(test));
    }
    Logger logger("JFJochReceiverTest_PacketLost_Raw");

    JFCalibration calib(x);

    TestImagePusher pusher(x.GetImageNum() - 1);

    JFJochReceiverService service(aq_devices, logger, pusher);
    service.NumThreads(nthreads);

    service.Start(x, pixel_mask, &calib);
    auto receiver_out =  service.Stop();

    const auto image = pusher.GetImage();

    REQUIRE(aq_devices[0].GetBytesReceived() == (8*128-1) * 8192UL);
    REQUIRE(image.size() == 2 * RAW_MODULE_SIZE * sizeof(uint16_t));
    REQUIRE(pusher.GetCounter() == x.GetImageNum());

    auto raw = (const uint16_t *) (image.data());
    CHECK(raw[0] == 776);
    CHECK(raw[256*1024+315] == 776);
    CHECK(raw[RAW_MODULE_SIZE-4095] == UINT16_MAX);
    CHECK(raw[RAW_MODULE_SIZE-1]    == UINT16_MAX);

    CHECK(raw[RAW_MODULE_SIZE+0] == 776);
    CHECK(raw[RAW_MODULE_SIZE+256*1024+315] == 776);
    CHECK(raw[2*RAW_MODULE_SIZE-1] == 776);

    REQUIRE(receiver_out.efficiency < 1.0);
    REQUIRE(receiver_out.status.images_sent == x.GetImageNum());
}

TEST_CASE("JFJochReceiverTest_Cancel", "[JFJochReceiver]") {
    DiffractionExperiment x(DetectorGeometry(2));
    const uint16_t nthreads = 4;

    std::vector<uint16_t> frame_in(RAW_MODULE_SIZE);
    for (auto &i: frame_in) i = 776;

    x.Mode(DetectorMode::Raw);
    x.PedestalG0Frames(0).NumTriggers(1)
            .UseInternalPacketGenerator(false).ImagesPerTrigger(4).IncidentEnergy_keV(12.4);

    PixelMask pixel_mask(x);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        auto test = std::make_unique<HLSSimulatedDevice>(i, 64);
        test->CreateJFPackets(x, 1, 1, 0, frame_in.data());
        test->CreateJFPackets(x, 1, 1, 1, frame_in.data());
        test->CreateJFPackets(x, 2, 1, 0, frame_in.data());
        test->CreateJFPackets(x, 2, 1, 1, frame_in.data());
        test->CreateJFPackets(x, 3, 1, 0, frame_in.data());
        test->CreateJFPackets(x, 3, 1, 1, frame_in.data());
        aq_devices.Add(std::move(test));
    }
    Logger logger("JFJochReceiverTest_Cancel");

    JFCalibration calib(x);

    TestImagePusher pusher(x.GetImageNum() - 1);
    JFJochReceiverService service(aq_devices, logger, pusher);
    service.NumThreads(nthreads);

    service.Start(x, pixel_mask, &calib);

    std::this_thread::sleep_for(std::chrono::seconds(60));
    service.Cancel(false);

    auto receiver_out =  service.Stop();
    REQUIRE(receiver_out.status.cancelled);
}

TEST_CASE("JFJochReceiverTest_EIGER", "[JFJochReceiver]") {
    Logger logger("JFJochReceiverTest_EIGER");

    DiffractionExperiment x(DetectorSetup(DetectorGeometry(2), DetectorType::EIGER));
    const uint16_t nthreads = 4;

    x.Mode(DetectorMode::Raw);
    x.PedestalG0Frames(0).NumTriggers(1).UseInternalPacketGenerator(true)
            .ImagesPerTrigger(32).ImagesPerFile(10).IncidentEnergy_keV(12.4).Compression(CompressionAlgorithm::BSHUF_ZSTD);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i,64);
        aq_devices.Add(std::move(test));
    }

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.status.images_sent == x.GetImageNum());

    REQUIRE(!output.status.cancelled);
}

TEST_CASE("JFJochReceiverTest_EIGER_8bit", "[JFJochReceiver]") {
    Logger logger("JFJochReceiverTest_EIGER_8bit");

    DiffractionExperiment x(DetectorSetup(DetectorGeometry(2), DetectorType::EIGER));
    const uint16_t nthreads = 4;

    x.PedestalG0Frames(0).NumTriggers(1).UseInternalPacketGenerator(true)
            .ImagesPerTrigger(32).ImagesPerFile(10).IncidentEnergy_keV(12.4).Compression(CompressionAlgorithm::BSHUF_ZSTD);
    x.EigerBitDepth(8);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i,64);
        aq_devices.Add(std::move(test));
    }

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.status.images_sent == x.GetImageNum());

    REQUIRE(!output.status.cancelled);
}

TEST_CASE("JFJochReceiverTest_EIGER_32bit", "[JFJochReceiver]") {
    Logger logger("JFJochReceiverTest_EIGER_32bit");

    DiffractionExperiment x(DetectorSetup(DetectorGeometry(2), DetectorType::EIGER));
    const uint16_t nthreads = 4;

    x.PedestalG0Frames(0).NumTriggers(1).UseInternalPacketGenerator(true)
            .ImagesPerTrigger(32).ImagesPerFile(10).IncidentEnergy_keV(12.4).Compression(CompressionAlgorithm::BSHUF_ZSTD);
    x.EigerBitDepth(32);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i,64);
        aq_devices.Add(std::move(test));
    }

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.status.images_sent == x.GetImageNum());

    REQUIRE(!output.status.cancelled);
}

TEST_CASE("JFJochReceiverTest_EIGER_conversion", "[JFJochReceiver]") {
    Logger logger("JFJochReceiverTest_EIGER_conversion");

    DiffractionExperiment x(DetectorSetup(DetectorGeometry(2), DetectorType::EIGER));
    const uint16_t nthreads = 4;

    x.Mode(DetectorMode::Conversion);
    x.PedestalG0Frames(0).NumTriggers(1).UseInternalPacketGenerator(true)
            .ImagesPerTrigger(32).ImagesPerFile(10).IncidentEnergy_keV(12.4).Compression(CompressionAlgorithm::BSHUF_ZSTD);

    AcquisitionDeviceGroup aq_devices;
    for (int i = 0; i < x.GetDataStreamsNum(); i++) {
        std::unique_ptr<HLSSimulatedDevice> test = std::make_unique<HLSSimulatedDevice>(i,64);
        aq_devices.Add(std::move(test));
    }

    PixelMask mask(x);

    JFJochReceiverOutput output;
    bool ret;
    REQUIRE_NOTHROW(ret = JFJochReceiverTest(output, logger, aq_devices, x, mask, nthreads));
    REQUIRE(ret);
    REQUIRE(output.efficiency == 1.0);
    REQUIRE(output.status.images_sent == x.GetImageNum());

    REQUIRE(!output.status.cancelled);
}
