// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>

#include "../preview/ZMQPreviewSocket.h"

TEST_CASE("ZMQPreviewSocket","[ZeroMQ]") {
    ZMQPreviewSocket preview_socket("ipc://*");

    ZMQSocket sub_socket(ZMQSocketType::Sub);
    sub_socket.Connect(preview_socket.GetAddress());
    sub_socket.SubscribeAll();
    sub_socket.ReceiveTimeout(std::chrono::seconds(2));

    // ensure sub socket is properly connected
    std::this_thread::sleep_for(std::chrono::seconds(2));

    std::vector<uint8_t> vec(256);
    for (int i = 0; i < vec.size(); i++)
        vec[i] = (i * 7 + 13) % 256;

    preview_socket.SendImage(vec.data(), vec.size());
    preview_socket.SendImage(vec.data(), vec.size());
    ZMQMessage msg1;
    REQUIRE(sub_socket.Receive(msg1, true));
    REQUIRE(msg1.size() == vec.size());
    REQUIRE(memcmp(msg1.data(), vec.data(), vec.size()) == 0);
    REQUIRE(!sub_socket.Receive(msg1, true));
}
