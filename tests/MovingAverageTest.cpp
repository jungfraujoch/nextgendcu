// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>
#include "../common/MovingAverage.h"

TEST_CASE("MovingAverage") {
    MovingAverage a(4);
    REQUIRE(!a.Read());

    a.Add(5);
    a.Add(3);
    a.Add(4);
    a.Add(4);
    REQUIRE(a.Read() == 4);
    a.Add(3);
    REQUIRE(a.Read() == 3.5);
}