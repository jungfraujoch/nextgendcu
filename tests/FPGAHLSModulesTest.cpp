// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>
#include <random>

#include "../fpga/hls_simulation/hls_cores.h"


TEST_CASE("HLS_DataCollectionFSM","[OpenCAPI]") {
    DataCollectionConfig act_reg;

    STREAM_512 raw0;
    STREAM_512 raw1;

    hls::stream<axis_addr> addr0;
    hls::stream<axis_addr> addr1;

    ap_uint<1> run_data_collection = 0;
    ap_uint<1> cancel_data_collection = 0;
    ap_uint<1> idle_data_collection;
    uint32_t save_data_collection_counter;
    act_reg.mode = MODE_CONV;

    // state = WAIT_FOR_START
    volatile rcv_state_t state;
    data_collection_fsm(raw0, raw1,
                        addr0, addr1,
                        run_data_collection,
                        cancel_data_collection,
                        idle_data_collection,
                        act_reg.mode,
                        act_reg.energy_kev,
                        act_reg.nframes,
                        act_reg.nmodules,
                        act_reg.nstorage_cells,
                        act_reg.nsummation,
                        act_reg.sqrtmult,
                        act_reg.pxlthreshold,
                        state);
    REQUIRE(idle_data_collection == 1);
    REQUIRE(addr1.empty());
    REQUIRE(raw1.empty());
    REQUIRE( state == RCV_WAIT_FOR_START);
    run_data_collection = 1;

    // state = WAIT_FOR_START
    data_collection_fsm(raw0, raw1,
                        addr0, addr1,
                        run_data_collection,
                        cancel_data_collection,
                        idle_data_collection,
                        act_reg.mode,
                        act_reg.energy_kev,
                        act_reg.nframes,
                        act_reg.nmodules,
                        act_reg.nstorage_cells,
                        act_reg.nsummation,
                        act_reg.sqrtmult,
                        act_reg.pxlthreshold,
                        state);
    REQUIRE(idle_data_collection == 0);
    REQUIRE(addr1.empty());
    REQUIRE(raw1.empty());
    REQUIRE( state == RCV_WAIT_FOR_START_LOW);
    // state = WAIT_FOR_START_LOW
    data_collection_fsm(raw0, raw1,
                        addr0, addr1,
                        run_data_collection,
                        cancel_data_collection,
                        idle_data_collection,
                        act_reg.mode,
                        act_reg.energy_kev,
                        act_reg.nframes,
                        act_reg.nmodules,
                        act_reg.nstorage_cells,
                        act_reg.nsummation,
                        act_reg.sqrtmult,
                        act_reg.pxlthreshold,
                        state);
    REQUIRE(idle_data_collection == 0);
    REQUIRE(addr1.empty());
    REQUIRE(raw1.empty());
    REQUIRE( state == RCV_WAIT_FOR_START_LOW);
    // state = WAIT_FOR_START_LOW

    run_data_collection = 0;

    data_collection_fsm(raw0, raw1,
                        addr0, addr1,
                        run_data_collection,
                        cancel_data_collection,
                        idle_data_collection,
                        act_reg.mode,
                        act_reg.energy_kev,
                        act_reg.nframes,
                        act_reg.nmodules,
                        act_reg.nstorage_cells,
                        act_reg.nsummation,
                        act_reg.sqrtmult,
                        act_reg.pxlthreshold,
                        state);
    REQUIRE(idle_data_collection == 0);
    REQUIRE(addr1.empty());
    REQUIRE(raw1.empty());
    REQUIRE( state == RCV_START);
    // state = START
    data_collection_fsm(raw0, raw1,
                        addr0, addr1,
                        run_data_collection,
                        cancel_data_collection,
                        idle_data_collection,
                        act_reg.mode,
                        act_reg.energy_kev,
                        act_reg.nframes,
                        act_reg.nmodules,
                        act_reg.nstorage_cells,
                        act_reg.nsummation,
                        act_reg.sqrtmult,
                        act_reg.pxlthreshold,
                        state);

    REQUIRE(idle_data_collection == 0);
    REQUIRE(raw1.size() == 1);

    REQUIRE( state == RCV_INIT);
    // state = INIT

    data_collection_fsm(raw0, raw1,
                        addr0, addr1,
                        run_data_collection,
                        cancel_data_collection,
                        idle_data_collection,
                        act_reg.mode,
                        act_reg.energy_kev,
                        act_reg.nframes,
                        act_reg.nmodules,
                        act_reg.nstorage_cells,
                        act_reg.nsummation,
                        act_reg.sqrtmult,
                        act_reg.pxlthreshold,
                        state);

    REQUIRE(idle_data_collection == 0);
    REQUIRE(raw1.size() == 1);
    REQUIRE( state == RCV_INIT);
    // state = INIT

    cancel_data_collection = 1;

    data_collection_fsm(raw0, raw1,
                        addr0, addr1,
                        run_data_collection,
                        cancel_data_collection,
                        idle_data_collection,
                        act_reg.mode,
                        act_reg.energy_kev,
                        act_reg.nframes,
                        act_reg.nmodules,
                        act_reg.nstorage_cells,
                        act_reg.nsummation,
                        act_reg.sqrtmult,
                        act_reg.pxlthreshold,
                        state);

    REQUIRE(idle_data_collection == 0);
    REQUIRE(raw1.size() == 1);

    // state = LAST
    REQUIRE( state == RCV_LAST);
    data_collection_fsm(raw0, raw1,
                        addr0, addr1,
                        run_data_collection,
                        cancel_data_collection,
                        idle_data_collection,
                        act_reg.mode,
                        act_reg.energy_kev,
                        act_reg.nframes,
                        act_reg.nmodules,
                        act_reg.nstorage_cells,
                        act_reg.nsummation,
                        act_reg.sqrtmult,
                        act_reg.pxlthreshold,
                        state);

    REQUIRE(idle_data_collection == 0);
    REQUIRE(addr1.size() == 1);
    REQUIRE(raw1.size() == 2);
    REQUIRE( state == RCV_WAIT_FOR_START);
    // state = WAIT_FOR_START

    data_collection_fsm(raw0, raw1,
                        addr0, addr1,
                        run_data_collection,
                        cancel_data_collection,
                        idle_data_collection,
                        act_reg.mode,
                        act_reg.energy_kev,
                        act_reg.nframes,
                        act_reg.nmodules,
                        act_reg.nstorage_cells,
                        act_reg.nsummation,
                        act_reg.sqrtmult,
                        act_reg.pxlthreshold,
                        state);

    REQUIRE( state == RCV_WAIT_FOR_START);
    REQUIRE(idle_data_collection == 1);
    REQUIRE(addr1.size() == 1);
    REQUIRE(raw1.size() == 2);

    auto packet = raw1.read();
    REQUIRE(packet.last == 1);
    REQUIRE(packet.dest == 0);

    packet = raw1.read();
    REQUIRE(packet.last);
    REQUIRE(packet.dest == 0);

    auto addr = addr1.read();
    REQUIRE(addr.last);
}
