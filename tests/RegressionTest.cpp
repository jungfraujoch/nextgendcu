// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>

#include "../image_analysis/Regression.h"

TEST_CASE("Regression") {
    std::vector<float> x(10);
    std::vector<float> y(10);
    for (int i = 0; i < x.size(); i++) {
        x[i] = i;
        y[i] = 7 * i + 5;
    }
    auto reg = regression(x, y);
    REQUIRE(reg.intercept == Catch::Approx(5.0));
    REQUIRE(reg.slope == Catch::Approx(7.0));
}
