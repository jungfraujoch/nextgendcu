// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>
#include "../acquisition_device/AcquisitionCounters.h"

TEST_CASE("AcquisitionCountersTest","[AcquisitionDeviceCounters]") {
    DiffractionExperiment x(DetectorGeometry(2));
    x.NumTriggers(1).ImagesPerTrigger(50);

    AcquisitionCounters counters;
    counters.Reset(x, 0);
    REQUIRE(counters.GetSlowestFrameNumber() == 0);
    REQUIRE(counters.GetFastestFrameNumber() == 0);
    REQUIRE(counters.GetCurrFrameNumber(0) == 0);
    REQUIRE(counters.GetCurrFrameNumber(1) == 0);
    REQUIRE_THROWS(counters.GetCurrFrameNumber(32));
    REQUIRE(counters.CalculateDelay(2) == 0);

    REQUIRE(!counters.IsAcquisitionFinished());

    Completion c{};
    c.frame_number = 32;
    c.module_number = 1;
    c.handle = 17;
    counters.UpdateCounters(&c);

    REQUIRE(counters.GetSlowestFrameNumber() == 0);
    REQUIRE(counters.GetFastestFrameNumber() == 32);
    REQUIRE(counters.GetCurrFrameNumber(0) == 0);
    REQUIRE(counters.GetCurrFrameNumber(1) == 32);

    REQUIRE(counters.CalculateDelay(31, 1) == 1);
    REQUIRE(counters.CalculateDelay(33, 1) == 0);
    REQUIRE(counters.GetBufferHandle(32, 1) == 17);
    REQUIRE(counters.GetBufferHandle(32, 0) == AcquisitionCounters::HandleNotFound);

    c.frame_number = 15;
    c.module_number = 0;
    counters.UpdateCounters(&c);

    REQUIRE(counters.GetSlowestFrameNumber() == 15);
    REQUIRE(counters.GetFastestFrameNumber() == 32);
    REQUIRE(counters.GetCurrFrameNumber(0) == 15);
    REQUIRE(counters.GetCurrFrameNumber(1) == 32);

    REQUIRE(counters.CalculateDelay(14) == 1);
    REQUIRE(counters.CalculateDelay(16) == 0);

    REQUIRE(counters.CalculateDelay(14, 0) == 1);
    REQUIRE(counters.CalculateDelay(16, 0) == 0);

    REQUIRE(counters.CalculateDelay(31, 1) == 1);
    REQUIRE(counters.CalculateDelay(33, 1) == 0);

    counters.SetAcquisitionFinished();

    REQUIRE(counters.GetFastestFrameNumber() == 32);
    REQUIRE(counters.GetSlowestFrameNumber() == 15);
    REQUIRE(counters.GetCurrFrameNumber(0) == 15);
    REQUIRE(counters.GetCurrFrameNumber(1) == 32);
    REQUIRE(counters.CalculateDelay(0) == 15);
    REQUIRE(counters.CalculateDelay(50) == 0);
}

TEST_CASE("AcquisitionCountersTest_OutOfBounds","[AcquisitionDeviceCounters]") {
    DiffractionExperiment x(DetectorGeometry(2));
    x.NumTriggers(1).ImagesPerTrigger(50);

    AcquisitionCounters counters;
    counters.Reset(x, 0);

    Completion c{};
    c.frame_number = 50;
    REQUIRE_THROWS(counters.UpdateCounters(&c));

    c.frame_number = 80;
    REQUIRE_THROWS(counters.UpdateCounters(&c));

    c.frame_number = 20;
    c.module_number = 2;
    REQUIRE_THROWS(counters.UpdateCounters(&c));
}

TEST_CASE("AcquisitionCountersTest_PacketCount","[AcquisitionDeviceCounters]") {
    DiffractionExperiment x(DetectorGeometry(2));
    x.NumTriggers(1).ImagesPerTrigger(50).Summation(2);

    AcquisitionCounters counters;
    counters.Reset(x, 0);
    REQUIRE(counters.GetTotalPackets() == 0);

    Completion c{};
    c.frame_number = 32;
    c.module_number = 1;
    c.handle = 17;
    c.packet_count = 86;
    counters.UpdateCounters(&c);

    REQUIRE(counters.GetTotalPackets() == 86);
    REQUIRE(counters.GetTotalPackets(0) == 0);
    REQUIRE(counters.GetTotalPackets(1) == 86);

    c.frame_number = 15;
    c.module_number = 0;
    c.packet_count = 128;
    counters.UpdateCounters(&c);

    REQUIRE(counters.GetTotalPackets() == 86 + 128);
    REQUIRE(counters.GetTotalPackets(0) == 128);
    REQUIRE(counters.GetTotalPackets(1) == 86);

    REQUIRE(counters.GetTotalExpectedPackets() == 50 * 512 * 2 * 2);
    REQUIRE(counters.GetTotalExpectedPacketsPerModule() == 50 * 512 * 2);
}