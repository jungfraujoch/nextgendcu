// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>

#include <fstream>

#include "../preview/JFJochTIFF.h"
#include "../preview/PreviewImage.h"
#include "../writer/HDF5Objects.h"
#include "../common/RawToConvertedGeometry.h"

TEST_CASE("TIFFTest","[TIFF]") {
    std::vector<uint16_t> values(512*1024);
    REQUIRE_NOTHROW(WriteTIFFToString(values.data(), 1024, 512, 2));
}

#include <iostream>

TEST_CASE("TIFFTest_Write_Read","[TIFF]") {
    std::vector<uint32_t> values(512*1024), values_out;
    for (int i = 0; i < values.size(); i++) {
        values[i] = (i * 17 + 2);
    }

    std::string s;
    REQUIRE_NOTHROW(s = WriteTIFFToString(values.data(), 1024, 512, 4));
    uint32_t lines, cols;

    REQUIRE_NOTHROW(values_out = ReadTIFFFromString32(s, cols, lines));
    REQUIRE(lines == 512);
    REQUIRE(cols == 1024);
    REQUIRE(values.size() == values_out.size());
    REQUIRE(memcmp(values.data(), values_out.data(), cols * lines * sizeof(uint32_t)) == 0);
}

TEST_CASE("TIFFTest_File","[TIFF]") {
    std::vector<uint16_t> values(512*1024);

    for (auto &i: values)
        i = 345;

    REQUIRE_NOTHROW(WriteTIFFToFile("test_image.tiff", values.data(), 1024, 512, 2));
}

TEST_CASE("TIFFTest_File_signed","[TIFF]") {
    std::vector<int16_t> values(512 * 1024);

    for (int i = 0; i < values.size(); i++)
        values[i] = static_cast<int16_t>(((i % 2 == 0) ? 1 : -1) * i);

    REQUIRE_NOTHROW(WriteTIFFToFile("test_image_signed.tiff", values.data(), 1024, 512, 2, true));
}

TEST_CASE("PreviewImage_GenerateTIFF","[TIFF]") {
    RegisterHDF5Filter();

    DiffractionExperiment experiment(DetectorGeometry(8,2,8,36));
    experiment.ImagesPerTrigger(5).NumTriggers(1).UseInternalPacketGenerator(true)
            .FilePrefix("lyso_test_min_pix_2").JungfrauConvPhotonCnt(false)
            .DetectorDistance_mm(75).BeamY_pxl(1136).BeamX_pxl(1090).IncidentEnergy_keV(12.4)
            .SetUnitCell(UnitCell{.a = 36.9, .b = 78.95, .c = 78.95, .alpha =90, .beta = 90, .gamma = 90});

    // Load example image
    HDF5ReadOnlyFile data("../../tests/test_data/compression_benchmark.h5");
    HDF5DataSet dataset(data, "/entry/data/data");
    HDF5DataSpace file_space(dataset);

    std::vector<int16_t> image_conv (file_space.GetDimensions()[1] * file_space.GetDimensions()[2]);

    std::vector<hsize_t> start = {4,0,0};
    std::vector<hsize_t> file_size = {1, file_space.GetDimensions()[1], file_space.GetDimensions()[2]};
    dataset.ReadVector(image_conv, start, file_size);

    // to fill gaps with INT16_MIN
    std::vector<int16_t> image_raw_geom(experiment.GetModulesNum() * RAW_MODULE_SIZE);
    std::vector<int16_t> image_conv_2 (file_space.GetDimensions()[1] * file_space.GetDimensions()[2], INT16_MIN);

    ConvertedToRawGeometry(experiment, image_raw_geom.data(), image_conv.data());
    RawToConvertedGeometry(experiment, image_conv_2.data(), image_raw_geom.data());

    PixelMask mask(experiment);

    std::vector<SpotToSave> spots;
    PreviewImage image;
    image.Configure(experiment, mask);
    image.UpdateImage(image_conv_2.data(), spots);

    std::string s;
    REQUIRE_NOTHROW(s = image.GenerateTIFF());
    std::ofstream f("lyso_diff.tiff", std::ios::binary);
    f.write(s.data(), s.size());
}


TEST_CASE("PreviewImage_GenerateTIFFDioptas","[TIFF]") {
    RegisterHDF5Filter();

    DiffractionExperiment experiment(DetectorGeometry(8,2,8,36));
    experiment.ImagesPerTrigger(5).NumTriggers(1).UseInternalPacketGenerator(true)
            .FilePrefix("lyso_test_min_pix_2").JungfrauConvPhotonCnt(false)
            .DetectorDistance_mm(75).BeamY_pxl(1136).BeamX_pxl(1090).IncidentEnergy_keV(12.4)
            .SetUnitCell(UnitCell{.a = 36.9, .b = 78.95, .c = 78.95, .alpha =90, .beta = 90, .gamma = 90});

    // Load example image
    HDF5ReadOnlyFile data("../../tests/test_data/compression_benchmark.h5");
    HDF5DataSet dataset(data, "/entry/data/data");
    HDF5DataSpace file_space(dataset);

    std::vector<int16_t> image_conv (file_space.GetDimensions()[1] * file_space.GetDimensions()[2]);

    std::vector<hsize_t> start = {4,0,0};
    std::vector<hsize_t> file_size = {1, file_space.GetDimensions()[1], file_space.GetDimensions()[2]};
    dataset.ReadVector(image_conv, start, file_size);

    // to fill gaps with INT16_MIN
    std::vector<int16_t> image_raw_geom(experiment.GetModulesNum() * RAW_MODULE_SIZE);
    std::vector<int16_t> image_conv_2 (file_space.GetDimensions()[1] * file_space.GetDimensions()[2], INT16_MIN);

    ConvertedToRawGeometry(experiment, image_raw_geom.data(), image_conv.data());
    RawToConvertedGeometry(experiment, image_conv_2.data(), image_raw_geom.data());

    PixelMask mask(experiment);

    std::vector<SpotToSave> spots;
    PreviewImage image;
    image.Configure(experiment, mask);
    image.UpdateImage(image_conv_2.data(), spots);

    std::string s;
    REQUIRE_NOTHROW(s = image.GenerateTIFFDioptas());
    std::ofstream f("lyso_diff_dioptas.tiff", std::ios::binary);
    f.write(s.data(), s.size());
}
