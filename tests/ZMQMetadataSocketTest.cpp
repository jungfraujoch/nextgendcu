// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>

#include "../preview/ZMQMetadataSocket.h"

#include "../frame_serialize/tinycbor/cbor.h"
#include "../frame_serialize/CBORStream2Deserializer.h"

TEST_CASE("ZMQMetadataSocket","[ZeroMQ]") {
    ZMQMetadataSocket metadata_socket("tcp://0.0.0.0:*");
    metadata_socket.Period(std::chrono::seconds(60));
    ZMQSocket sub_socket(ZMQSocketType::Sub);
    sub_socket.Connect(metadata_socket.GetAddress());
    sub_socket.SubscribeAll();
    sub_socket.ReceiveTimeout(std::chrono::seconds(2));

    // ensure sub socket is properly connected
    std::this_thread::sleep_for(std::chrono::seconds(2));

    StartMessage start_message;
    DataMessage data_message_0{.number = 0};
    DataMessage data_message_1{.number = 1};
    metadata_socket.StartDataCollection(start_message);
    metadata_socket.AddDataMessage(data_message_0);
    metadata_socket.AddDataMessage(data_message_1);

    // Check if there is only start message in the queue
    ZMQMessage msg1;
    REQUIRE(sub_socket.Receive(msg1, true));
    auto msg_out = CBORStream2Deserialize(msg1.data(), msg1.size());
    REQUIRE(msg_out);
    REQUIRE(msg_out->msg_type == CBORImageType::START);

    REQUIRE(!sub_socket.Receive(msg1, true));

    EndMessage msg;
    // Metadata message should be only here after calling finalize
    metadata_socket.EndDataCollection(msg);

    REQUIRE(sub_socket.Receive(msg1, true));
    msg_out = CBORStream2Deserialize(msg1.data(), msg1.size());
    REQUIRE(msg_out);
    REQUIRE(msg_out->msg_type == CBORImageType::METADATA);
    REQUIRE(msg_out->metadata);
    REQUIRE(msg_out->metadata->images.size() == 2);
    REQUIRE(msg_out->metadata->images.at(0).number == 0);
    REQUIRE(msg_out->metadata->images.at(1).number == 1);

    REQUIRE(sub_socket.Receive(msg1, true));
    msg_out = CBORStream2Deserialize(msg1.data(), msg1.size());
    REQUIRE(msg_out);
    REQUIRE(msg_out->msg_type == CBORImageType::END);
    REQUIRE(!sub_socket.Receive(msg1, true));
}