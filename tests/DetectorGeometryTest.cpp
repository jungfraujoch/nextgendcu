// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>
#include "../common/DetectorGeometry.h"

TEST_CASE("DetectorGeometry_NonModular", "[DetectorGeometry]") {
    DetectorGeometry geometry(std::make_pair<int64_t, int64_t>(1024,1068));

    REQUIRE(geometry.GetWidth(true) == 1024);
    REQUIRE(geometry.GetWidth(false) == 1024);
    REQUIRE(geometry.GetHeight(true) == 1068);
    REQUIRE(geometry.GetHeight(false) == 1068);
    REQUIRE(geometry.GetModulesNum() == 1);
    REQUIRE(geometry.GetPixel0(0) == 0);
    REQUIRE_THROWS(geometry.GetFastDirection(0));
    REQUIRE_THROWS(geometry.GetSlowDirection(0));
    REQUIRE_THROWS(geometry.GetFastDirectionStep(0));
    REQUIRE_THROWS(geometry.GetSlowDirectionStep(0));
}

TEST_CASE("DetectorGeometry_Regular", "[DetectorGeometry]") {
    DetectorGeometry geometry(18, 3, 8, 36, false);

    REQUIRE(geometry.GetModulesNum() == 18);

    REQUIRE(geometry.GetWidth() == 3 * 1030 + 2 * 8);
    REQUIRE(geometry.GetHeight() == 6 * 514 + 5 * 36);
    REQUIRE(geometry.GetPixel0(0) == 0);
    REQUIRE(geometry.GetPixel0(2) == 2 * 1030 + 2 * 8);

    REQUIRE(geometry.GetPixel0(15) == geometry.GetWidth() * (514 * 5 + 36 * 5));
    REQUIRE(geometry.GetFastDirectionStep(15) == 1);
    REQUIRE(geometry.GetFastDirection(15).x == 1);
    REQUIRE(geometry.GetFastDirection(15).y == 0);
    REQUIRE(geometry.GetFastDirection(15).z == 0);
    REQUIRE(geometry.GetSlowDirectionStep(15) == geometry.GetWidth());

    REQUIRE(geometry.GetSlowDirection(15).x == 0);
    REQUIRE(geometry.GetSlowDirection(15).y == 1);
    REQUIRE(geometry.GetSlowDirection(15).z == 0);
}

TEST_CASE("DetectorGeometry_Regular_NoTransformation", "[DetectorGeometry]") {
    DetectorGeometry geometry(18, 3, 8, 36, false);
    REQUIRE(geometry.GetWidth(false) == RAW_MODULE_COLS);
    REQUIRE(geometry.GetHeight(false) == 18 * RAW_MODULE_LINES);
    REQUIRE(geometry.GetPixel0(16, false) == 16 * RAW_MODULE_SIZE);
}

TEST_CASE("DetectorGeometry_Regular_1Module", "[DetectorGeometry]") {
    DetectorGeometry geometry(1, 3, 8, 36, false);

    REQUIRE(geometry.GetModulesNum() == 1);

    REQUIRE(geometry.GetWidth() == CONVERTED_MODULE_COLS);
    REQUIRE(geometry.GetHeight() == CONVERTED_MODULE_LINES);
    REQUIRE(geometry.GetPixel0(0) == 0);

    REQUIRE(geometry.GetFastDirectionStep(0) == 1);
    REQUIRE(geometry.GetSlowDirectionStep(0) == CONVERTED_MODULE_COLS);
}

TEST_CASE("DetectorGeometry_Regular_2Module", "[DetectorGeometry]") {
    DetectorGeometry geometry(2, 2, 8, 36, false);

    REQUIRE(geometry.GetModulesNum() == 2);

    REQUIRE(geometry.GetWidth() == CONVERTED_MODULE_COLS * 2 + 8);
    REQUIRE(geometry.GetHeight() == CONVERTED_MODULE_LINES);
    REQUIRE(geometry.GetPixel0(0) == 0);
    REQUIRE(geometry.GetPixel0(1) == CONVERTED_MODULE_COLS + 8);

    REQUIRE(geometry.GetFastDirectionStep(0) == 1);
    REQUIRE(geometry.GetSlowDirectionStep(0) == CONVERTED_MODULE_COLS * 2 + 8);
}

TEST_CASE("DetectorGeometry_RegularMirror", "[DetectorGeometry]") {
    DetectorGeometry geometry(18, 3, 8, 36, true);

    REQUIRE(geometry.GetModulesNum() == 18);

    REQUIRE(geometry.GetWidth() == 3 * 1030 + 2 * 8);
    REQUIRE(geometry.GetHeight() == 6 * 514 + 5 * 36);
    REQUIRE(geometry.GetPixel0(0) == geometry.GetWidth() * (geometry.GetHeight() - 1));
    REQUIRE(geometry.GetPixel0(2) == geometry.GetWidth() * (geometry.GetHeight() - 1) + 2 * 1030 + 2 * 8);

    CHECK(geometry.GetPixel0(15) == geometry.GetWidth() * 513);
    CHECK(geometry.GetPixel0(17) == geometry.GetWidth() * (513) + 2 * 1030 + 2 * 8);
    REQUIRE(geometry.GetFastDirectionStep(15) == 1);
    REQUIRE(geometry.GetSlowDirectionStep(15) == -geometry.GetWidth());
}

TEST_CASE("DetectorModuleGeometry_SameAxis", "[DetectorGeometry]") {
    std::unique_ptr<DetectorModuleGeometry> geometry;

    REQUIRE_THROWS(geometry = std::make_unique<DetectorModuleGeometry>(0,0,
                                                                       DetectorModuleGeometry::Direction::Xpos,
                                                                       DetectorModuleGeometry::Direction::Xpos));

    REQUIRE_THROWS(geometry = std::make_unique<DetectorModuleGeometry>(0,0,
                                                                       DetectorModuleGeometry::Direction::Ypos,
                                                                       DetectorModuleGeometry::Direction::Ypos));
    REQUIRE_THROWS(geometry = std::make_unique<DetectorModuleGeometry>(2000,2000,
                                                                       DetectorModuleGeometry::Direction::Yneg,
                                                                       DetectorModuleGeometry::Direction::Ypos));

    REQUIRE_THROWS(geometry = std::make_unique<DetectorModuleGeometry>(2000,2000,
                                                                       DetectorModuleGeometry::Direction::Xpos,
                                                                       DetectorModuleGeometry::Direction::Xneg));

    REQUIRE_NOTHROW(geometry = std::make_unique<DetectorModuleGeometry>(2000,2000,
                                                                       DetectorModuleGeometry::Direction::Xneg,
                                                                       DetectorModuleGeometry::Direction::Ypos));

    REQUIRE_NOTHROW(geometry = std::make_unique<DetectorModuleGeometry>(0,0,
                                                                        DetectorModuleGeometry::Direction::Ypos,
                                                                        DetectorModuleGeometry::Direction::Xpos));
}


TEST_CASE("DetectorModuleGeometry_NoSpace", "[DetectorGeometry]") {
    std::unique_ptr<DetectorModuleGeometry> geometry;

    REQUIRE_THROWS(geometry = std::make_unique<DetectorModuleGeometry>(1022,0,
                                                                       DetectorModuleGeometry::Direction::Xneg,
                                                                       DetectorModuleGeometry::Direction::Ypos));

    REQUIRE_THROWS(geometry = std::make_unique<DetectorModuleGeometry>(1022,512,
                                                                       DetectorModuleGeometry::Direction::Xpos,
                                                                       DetectorModuleGeometry::Direction::Yneg));

    REQUIRE_THROWS(geometry = std::make_unique<DetectorModuleGeometry>(1029,513,
                                                                        DetectorModuleGeometry::Direction::Yneg,
                                                                        DetectorModuleGeometry::Direction::Xneg));

    REQUIRE_NOTHROW(geometry = std::make_unique<DetectorModuleGeometry>(1029,513,
                                                                       DetectorModuleGeometry::Direction::Xneg,
                                                                       DetectorModuleGeometry::Direction::Yneg));

    REQUIRE_NOTHROW(geometry = std::make_unique<DetectorModuleGeometry>(513,1029,
                                                                        DetectorModuleGeometry::Direction::Yneg,
                                                                        DetectorModuleGeometry::Direction::Xneg));
}

TEST_CASE("DetectorModuleGeometry_GetMaxX_GetMaxY", "[DetectorGeometry]") {
    std::unique_ptr<DetectorModuleGeometry> geometry;

    REQUIRE_NOTHROW(geometry = std::make_unique<DetectorModuleGeometry>(0,0,
                                                                       DetectorModuleGeometry::Direction::Xpos,
                                                                       DetectorModuleGeometry::Direction::Ypos));

    REQUIRE(geometry->GetMaxX() == 1029);
    REQUIRE(geometry->GetMaxY() == 513);

    REQUIRE_NOTHROW(geometry = std::make_unique<DetectorModuleGeometry>(2029,1513,
                                                                        DetectorModuleGeometry::Direction::Xneg,
                                                                        DetectorModuleGeometry::Direction::Yneg));

    REQUIRE(geometry->GetMaxX() == 2029);
    REQUIRE(geometry->GetMaxY() == 1513);

    REQUIRE_NOTHROW(geometry = std::make_unique<DetectorModuleGeometry>(2029,1513,
                                                                        DetectorModuleGeometry::Direction::Yneg,
                                                                        DetectorModuleGeometry::Direction::Xneg));

    REQUIRE(geometry->GetMaxX() == 2029);
    REQUIRE(geometry->GetMaxY() == 1513);

    REQUIRE_NOTHROW(geometry = std::make_unique<DetectorModuleGeometry>(2029,1513,
                                                                        DetectorModuleGeometry::Direction::Yneg,
                                                                        DetectorModuleGeometry::Direction::Xpos));

    REQUIRE(geometry->GetMaxX() == 2029 + 513);
    REQUIRE(geometry->GetMaxY() == 1513);

    REQUIRE_NOTHROW(geometry = std::make_unique<DetectorModuleGeometry>(2029,1513,
                                                                        DetectorModuleGeometry::Direction::Ypos,
                                                                        DetectorModuleGeometry::Direction::Xneg));

    REQUIRE(geometry->GetMaxX() == 2029);
    REQUIRE(geometry->GetMaxY() == 1513 + 1029);
}

TEST_CASE("DetectorGeometry_Custom", "[DetectorGeometry]") {
    std::vector<DetectorModuleGeometry> module_geom;

    REQUIRE_NOTHROW(module_geom.emplace_back(2999, 2999, DetectorModuleGeometry::Direction::Xneg,
                             DetectorModuleGeometry::Direction::Yneg));

    REQUIRE_NOTHROW(module_geom.emplace_back(0, 0, DetectorModuleGeometry::Direction::Ypos,
                             DetectorModuleGeometry::Direction::Xpos));

    REQUIRE_NOTHROW(module_geom.emplace_back(5000, 0, DetectorModuleGeometry::Direction::Ypos,
                             DetectorModuleGeometry::Direction::Xpos));

    DetectorGeometry geometry(module_geom);

    REQUIRE(geometry.GetModulesNum() == 3);


    CHECK(geometry.GetHeight() == 2999+1);
    CHECK(geometry.GetWidth() == 5513+1);
    CHECK(geometry.GetPixel0(0) == 2999 * geometry.GetWidth() + 2999);
    CHECK(geometry.GetPixel0(1) == 0);
    CHECK(geometry.GetPixel0(2) == 5000);

    CHECK(geometry.GetFastDirectionStep(0) == -1);

    REQUIRE(geometry.GetFastDirection(0).x == -1);
    REQUIRE(geometry.GetFastDirection(0).y == 0);
    REQUIRE(geometry.GetFastDirection(0).z == 0);

    CHECK(geometry.GetFastDirectionStep(1) == geometry.GetWidth());
    REQUIRE(geometry.GetFastDirection(1).x == 0);
    REQUIRE(geometry.GetFastDirection(1).y == 1);
    REQUIRE(geometry.GetFastDirection(1).z == 0);

    CHECK(geometry.GetFastDirectionStep(2) == geometry.GetWidth());

    CHECK(geometry.GetSlowDirectionStep(0) == -geometry.GetWidth());

    REQUIRE(geometry.GetSlowDirection(0).x == 0);
    REQUIRE(geometry.GetSlowDirection(0).y == -1);
    REQUIRE(geometry.GetSlowDirection(0).z == 0);

    CHECK(geometry.GetSlowDirectionStep(1) == 1);

    REQUIRE(geometry.GetSlowDirection(1).x == 1);
    REQUIRE(geometry.GetSlowDirection(1).y == 0);
    REQUIRE(geometry.GetSlowDirection(1).z == 0);

    CHECK(geometry.GetSlowDirectionStep(2) == 1);
}

TEST_CASE("DetectorGeometry_Custom_Mirror", "[DetectorGeometry]") {
    std::vector<DetectorModuleGeometry> module_geom;

    REQUIRE_NOTHROW(module_geom.emplace_back(2999, 2999, DetectorModuleGeometry::Direction::Xneg,
                                             DetectorModuleGeometry::Direction::Yneg));

    REQUIRE_NOTHROW(module_geom.emplace_back(0, 0, DetectorModuleGeometry::Direction::Ypos,
                                             DetectorModuleGeometry::Direction::Xpos));

    REQUIRE_NOTHROW(module_geom.emplace_back(5000, 0, DetectorModuleGeometry::Direction::Ypos,
                                             DetectorModuleGeometry::Direction::Xpos));

    DetectorGeometry geometry(module_geom, true);

    REQUIRE(geometry.GetModulesNum() == 3);

    CHECK(geometry.GetHeight() == 2999+1);
    CHECK(geometry.GetWidth() == 5513+1);
    CHECK(geometry.GetPixel0(0) == 2999);
    CHECK(geometry.GetPixel0(1) == 2999 * geometry.GetWidth());
    CHECK(geometry.GetPixel0(2) == 2999 * geometry.GetWidth() + 5000 );

    CHECK(geometry.GetFastDirectionStep(0) == -1);

    REQUIRE(geometry.GetFastDirection(0).x == -1);
    REQUIRE(geometry.GetFastDirection(0).y == 0);
    REQUIRE(geometry.GetFastDirection(0).z == 0);

    CHECK(geometry.GetFastDirectionStep(1) == -geometry.GetWidth());
    REQUIRE(geometry.GetFastDirection(1).x == 0);
    REQUIRE(geometry.GetFastDirection(1).y == -1);
    REQUIRE(geometry.GetFastDirection(1).z == 0);

    CHECK(geometry.GetFastDirectionStep(2) == -geometry.GetWidth());

    CHECK(geometry.GetSlowDirectionStep(0) == geometry.GetWidth());

    REQUIRE(geometry.GetSlowDirection(0).x == 0);
    REQUIRE(geometry.GetSlowDirection(0).y == 1);
    REQUIRE(geometry.GetSlowDirection(0).z == 0);

    CHECK(geometry.GetSlowDirectionStep(1) == 1);

    REQUIRE(geometry.GetSlowDirection(1).x == 1);
    REQUIRE(geometry.GetSlowDirection(1).y == 0);
    REQUIRE(geometry.GetSlowDirection(1).z == 0);

    CHECK(geometry.GetSlowDirectionStep(2) == 1);
}
