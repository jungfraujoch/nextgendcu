// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>
#include "../common/StatusVector.h"

TEST_CASE("StatusVector_GetMeanPerBin","[StatusVector]") {
    StatusVector<uint64_t> status_vector;
    status_vector.AddElement(5, 11);

    status_vector.AddElement(2000, 45);
    status_vector.AddElement(2543, 44);
    status_vector.AddElement(2600, 41);

    MultiLinePlotStruct plot;

    plot = status_vector.GetMeanPerBin(1000);

    REQUIRE(plot.x.size() == 2);
    REQUIRE(plot.y.size() == 2);
    CHECK(plot.x[0] == Catch::Approx(500));
    CHECK(plot.x[1] == Catch::Approx(2500));
    CHECK(plot.y[0] == Catch::Approx(11));
    CHECK(plot.y[1] == Catch::Approx((45 + 44 + 41) / 3.0));

    plot = status_vector.GetMeanPerBin(500);

    REQUIRE(plot.x.size() == 3);
    REQUIRE(plot.y.size() == 3);
    CHECK(plot.x[0] == Catch::Approx(250));
    CHECK(plot.x[1] == Catch::Approx(2250));
    CHECK(plot.x[2] == Catch::Approx(2750));

    CHECK(plot.y[0] == Catch::Approx(11));
    CHECK(plot.y[1] == Catch::Approx(45));
    CHECK(plot.y[2] == Catch::Approx((44 + 41) / 2.0));

    plot = status_vector.GetMeanPerBin(1);

    REQUIRE(plot.x.size() == 4);
    REQUIRE(plot.y.size() == 4);
    CHECK(plot.x[0] == Catch::Approx(5));
    CHECK(plot.x[1] == Catch::Approx(2000));
    CHECK(plot.x[2] == Catch::Approx(2543));
    CHECK(plot.x[3] == Catch::Approx(2600));

    CHECK(plot.y[0] == Catch::Approx(11));
    CHECK(plot.y[1] == Catch::Approx(45));
    CHECK(plot.y[2] == Catch::Approx(44));
    CHECK(plot.y[3] == Catch::Approx(41));
}

TEST_CASE("StatusVector_Mean","[StatusVector]") {
    StatusVector<float> status_vector;
    status_vector.AddElement(5, 0.045f);

    status_vector.AddElement(2000, .010f);
    status_vector.AddElement(2543, 0.076f);
    status_vector.AddElement(2600, 0.011f);

    REQUIRE(status_vector.Mean() == Catch::Approx((0.045+0.010+0.076+0.011)/4.0));
}

TEST_CASE("StatusVector_Mean_LargeNum","[StatusVector]") {
    StatusVector<float> status_vector;
    for (int i = 0; i < 100000; i++)
        status_vector.AddElement(i, 0.045f);

    REQUIRE(status_vector.Mean() == Catch::Approx(0.045));
}

TEST_CASE("StatusVector_GetMaxPerBin","[StatusVector]") {
    StatusVector<uint64_t> status_vector;
    status_vector.AddElement(5, 11);

    status_vector.AddElement(2000, 45);
    status_vector.AddElement(2543, 44);
    status_vector.AddElement(2600, 41);

    auto status_out = status_vector.GetMaxPerBin(1000);

    REQUIRE(status_out.x.size() == 2);
    REQUIRE(status_out.y.size() == 2);
    REQUIRE(status_out.x[0] == 500);
    REQUIRE(status_out.x[1] == 2500);
    REQUIRE(status_out.y[0] == 11);
    REQUIRE(status_out.y[1] == 45);

    status_out = status_vector.GetMaxPerBin(500);

    REQUIRE(status_out.x.size() == 3);
    REQUIRE(status_out.y.size() == 3);
    REQUIRE(status_out.y[0] == 11);
    REQUIRE(status_out.y[1] == 45);
    REQUIRE(status_out.y[2] == 44);

    status_out = status_vector.GetMaxPerBin(1);
    REQUIRE(status_out.x.size() == 4);
    REQUIRE(status_out.y.size() == 4);
    REQUIRE(status_out.x[0] == 5);
    REQUIRE(status_out.y[0] == 11);
    REQUIRE(status_out.x[1] == 2000);
    REQUIRE(status_out.y[1] == 45);
    REQUIRE(status_out.x[2] == 2543);
    REQUIRE(status_out.y[2] == 44);
    REQUIRE(status_out.x[3] == 2600);
    REQUIRE(status_out.y[3] == 41);
}

TEST_CASE("StatusVector_ExportArray","[StatusVector]") {
    StatusVector<uint64_t> status_vector;
    status_vector.AddElement(5, 11);

    status_vector.AddElement(7, 45);
    status_vector.AddElement(8, 44);
    status_vector.AddElement(10, 41);

    auto status_out = status_vector.ExportArray(1);
    REQUIRE(status_out.size() == 11);
    CHECK(status_out[0] == 1);
    CHECK(status_out[5] == 11);
    CHECK(status_out[7] == 45);
    CHECK(status_out[8] == 44);
    CHECK(status_out[9] == 1);
    CHECK(status_out[10] == 41);
}

TEST_CASE("StatusVector_Plot_OneBin","[StatusVector]") {
    StatusVector<uint64_t> status_vector;
    status_vector.AddElement(5, 11);
    status_vector.AddElement(7, 12);

    auto plot_out = status_vector.GetMeanPlot(1000);
    REQUIRE(plot_out.size() == 1);
    REQUIRE(plot_out[0].x.size() == 2);
    REQUIRE(plot_out[0].y.size() == 2);
    REQUIRE(plot_out[0].x[0] == Catch::Approx(5));
    REQUIRE(plot_out[0].y[0] == Catch::Approx(11));
    REQUIRE(plot_out[0].x[1] == Catch::Approx(7));
    REQUIRE(plot_out[0].y[1] == Catch::Approx(12));
}

TEST_CASE("StatusVector_Plot_NoBinning","[StatusVector]") {
    StatusVector<uint64_t> status_vector;
    status_vector.AddElement(5, 11);

    auto plot_out = status_vector.GetMaxPlot(1);
    REQUIRE(plot_out.size() == 1);
    REQUIRE(plot_out[0].x.size() == 1);
    REQUIRE(plot_out[0].y.size() == 1);
    REQUIRE(plot_out[0].x[0] == Catch::Approx(5));
    REQUIRE(plot_out[0].y[0] == Catch::Approx(11));
}

TEST_CASE("StatusMultiVector","[StatusMultiVector]") {
    StatusMultiVector<uint64_t> status_vector;
    status_vector.AddElement("plot1", 0, 4);
    status_vector.AddElement("plot1", 1, 3);
    status_vector.AddElement("plot2", 0, 5);
    status_vector.AddElement("plot2", 1, 4);

    auto ret = status_vector.GetMeanPlot(1);
    REQUIRE(ret.size() == 2);
    REQUIRE(ret[0].title == "plot1");
    REQUIRE(ret[0].x.size() == 2);
    REQUIRE(ret[0].y.size() == 2);
    REQUIRE(ret[0].x[0] == 0.0f);
    REQUIRE(ret[0].y[1] == 3.0f);

    REQUIRE(ret[1].title == "plot2");
    REQUIRE(ret[1].x.size() == 2);
    REQUIRE(ret[1].y.size() == 2);
    REQUIRE(ret[1].x[1] == 1.0f);
    REQUIRE(ret[1].y[1] == 4.0f);
}

TEST_CASE("StatusVector_Clear","[StatusVector]") {
    StatusVector<uint64_t> status_vector;
    status_vector.AddElement(5, 800);
    status_vector.AddElement(8, 1000);
    REQUIRE(status_vector.Mean() == 900);

    status_vector.Clear();
    REQUIRE(!std::isfinite(status_vector.Mean()));

    status_vector.AddElement(50, 200);
    status_vector.AddElement(80, 100);
    REQUIRE(status_vector.Mean() == 150);

    auto plot = status_vector.GetMeanPlot(1);

    REQUIRE(plot.size() == 1);
    REQUIRE(plot[0].x.size() == 2);
    REQUIRE(plot[0].x[0] == 50);
    REQUIRE(plot[0].x[1] == 80);
}