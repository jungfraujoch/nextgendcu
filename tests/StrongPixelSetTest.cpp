// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>
#include "../image_analysis/StrongPixelSet.h"

TEST_CASE("DiffractionSpot_AddOperator","[StrongPixelSet]") {
    DiffractionSpot spot1(4,1,10), spot2(3,4,5);

    spot1 += spot2;

    // Spot2 is not changed
    REQUIRE(spot2.Count() == 5);

    REQUIRE(spot1.Count() == 15);
    REQUIRE(spot1.RawCoord().x == Catch::Approx((4*10+3*5)/15.0));
    REQUIRE(spot1.RawCoord().y == Catch::Approx((1*10+4*5)/15.0));
    REQUIRE(spot1.MaxCount() == 10);
    REQUIRE(spot1.PixelCount() == 2);
}

TEST_CASE("StrongPixelSet_BuildSpots","[StrongPixelSet]") {
    DiffractionExperiment experiment(DetectorGeometry(1,1,0,0,false));
    experiment.Mode(DetectorMode::Raw);

    SpotFindingSettings settings;
    settings.low_resolution_limit = 200.0;
    settings.high_resolution_limit = 0.5;
    settings.min_pix_per_spot = 3;
    settings.max_pix_per_spot = 200;

    std::vector<DiffractionSpot> spots;
    StrongPixelSet strong_pixel_set;
    strong_pixel_set.AddStrongPixel(7,105);
    strong_pixel_set.AddStrongPixel(7,106);
    strong_pixel_set.AddStrongPixel(7,104);
    strong_pixel_set.AddStrongPixel(6,106);
    strong_pixel_set.AddStrongPixel(6,105);
    strong_pixel_set.AddStrongPixel(6,104);
    strong_pixel_set.AddStrongPixel(8,106);
    strong_pixel_set.AddStrongPixel(8,105);
    strong_pixel_set.AddStrongPixel(8,104);
    strong_pixel_set.FindSpots(experiment, settings, spots, 0);
    REQUIRE(spots.size() == 1);

    REQUIRE(spots[0].Count() == 9.0f);
    REQUIRE(spots[0].PixelCount() == 9);
    REQUIRE(spots[0].RawCoord().x == Catch::Approx(7.0));
    REQUIRE(spots[0].RawCoord().y == Catch::Approx(105.0));
}

/*
TEST_CASE("StrongPixelSet_ReadFPGAOutput_1","[StrongPixelSet]") {
    DiffractionExperiment experiment(DetectorGeometry(8, 2, 8, 36, true));
    experiment.BeamX_pxl(1000).BeamY_pxl(1000).DetectorDistance_mm(100).PhotonEnergy_keV(WVL_1A_IN_KEV);

    StrongPixelSet strong_pixel_set(experiment);

    DeviceOutput output{};
    for (auto &i: output.spot_finding_result.strong_pixel)
        i = 0;
    output.pixels[0] = 7;
    output.pixels[1] = 7;
    output.pixels[1024] = 7;
    output.pixels[1025] = 7;
    output.pixels[7] = 345;

    // Pixels 0, 1, 7, 1024 and 1025 will be enabled
    output.spot_finding_result.strong_pixel[0] = (1U<<0) | (1U<<1) | (1U<<7);
    output.spot_finding_result.strong_pixel[RAW_MODULE_COLS/8] = (1<<0) | (1<<1);
    output.spot_finding_result.strong_pixel_count = 5;

    strong_pixel_set.ReadFPGAOutput(output);
    REQUIRE(strong_pixel_set.GetStrongPixelCount() == 5);
    SpotFindingSettings settings{
            .min_pix_per_spot = 2,
            .max_pix_per_spot = 55,
            .high_resolution_limit = 1.0,
            .low_resolution_limit = 50.0
    };
    std::vector<DiffractionSpot> spots;
    strong_pixel_set.FindSpots(experiment, settings, spots, 2);
    CHECK(spots.size() == 1);
    REQUIRE(spots[0].RawCoord().x == Approx(0.5));
    REQUIRE(spots[0].RawCoord().y == Approx(CONVERTED_MODULE_LINES * 3 + 36 * 2 - 1 - 0.5));
    REQUIRE(spots[0].PixelCount() == 4);
    REQUIRE(spots[0].MaxCount() == 7);
}

TEST_CASE("StrongPixelSet_ReadFPGAOutput_2","[StrongPixelSet]") {
    DiffractionExperiment experiment(DetectorGeometry(8, 2, 8, 36, true));
    experiment.BeamX_pxl(1000).BeamY_pxl(1000).DetectorDistance_mm(100).PhotonEnergy_keV(WVL_1A_IN_KEV);

    StrongPixelSet strong_pixel_set(experiment);

    DeviceOutput output{};
    for (auto &i: output.spot_finding_result.strong_pixel)
        i = 0;
    output.pixels[0] = 7;
    output.pixels[1] = 7;
    output.pixels[1024] = 7;
    output.pixels[1025] = 7;
    output.pixels[7] = 345;

    // Pixels 0, 1, 7, 1024 and 1025 will be enabled
    output.spot_finding_result.strong_pixel[0] = (1U<<0) | (1U<<1) | (1U<<7);
    output.spot_finding_result.strong_pixel[RAW_MODULE_COLS/8] = (1<<0) | (1<<1);
    output.spot_finding_result.strong_pixel_count = 5;

    strong_pixel_set.ReadFPGAOutput(output);
    REQUIRE(strong_pixel_set.GetStrongPixelCount() == 5);
    SpotFindingSettings settings{
            .min_pix_per_spot = 1,
            .max_pix_per_spot = 3,
            .high_resolution_limit = 1.0,
            .low_resolution_limit = 50.0
    };
    std::vector<DiffractionSpot> spots;
    strong_pixel_set.FindSpots(experiment, settings, spots, 2);
    CHECK(spots.size() == 1);
    REQUIRE(spots[0].RawCoord().x == Approx(7));
    REQUIRE(spots[0].RawCoord().y == Approx(CONVERTED_MODULE_LINES * 3 + 36 * 2 - 1));
    REQUIRE(spots[0].PixelCount() == 1);
    REQUIRE(spots[0].MaxCount() == 345);
} */
