// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>
#include "../broker/JFJochStateMachine.h"

using namespace std::literals::chrono_literals;

TEST_CASE("JFJochStateMachine_States") {
    Logger logger("JFJochStateMachine_States");
    JFJochServices services(logger);
    JFJochStateMachine state_machine(services, logger);
    state_machine.AddDetectorSetup(DetectorGeometry(4));

    DatasetSettings setup;

    REQUIRE(state_machine.GetStatus().state == JFJochState::Inactive);

    REQUIRE_THROWS(state_machine.Start(setup));
    REQUIRE_THROWS(state_machine.Pedestal());

    REQUIRE_NOTHROW(state_machine.Initialize());
    REQUIRE_NOTHROW(state_machine.WaitTillMeasurementDone());
    REQUIRE(state_machine.GetStatus().state == JFJochState::Idle);

    REQUIRE_NOTHROW(state_machine.Deactivate());
    REQUIRE(state_machine.GetStatus().state == JFJochState::Inactive);
}

TEST_CASE("JFJochStateMachine_State_Pedestal") {
    Logger logger("JFJochStateMachine_State_Pedestal");
    JFJochServices services(logger);
    JFJochStateMachine state_machine(services, logger);
    state_machine.AddDetectorSetup(DetectorGeometry(4));

    DatasetSettings setup;

    state_machine.DebugOnly_SetState(JFJochState::Pedestal);

    REQUIRE(state_machine.GetStatus().state == JFJochState::Pedestal);

    REQUIRE_THROWS(state_machine.Start(setup));
    REQUIRE_THROWS(state_machine.Pedestal());
    REQUIRE_THROWS(state_machine.Initialize());
    REQUIRE(state_machine.WaitTillMeasurementDone(std::chrono::milliseconds(1)).state == JFJochState::Pedestal);
}

TEST_CASE("JFJochStateMachine_State_Measure") {
    Logger logger("JFJochStateMachine_State_Measure");
    JFJochServices services(logger);
    JFJochStateMachine state_machine(services, logger);
    state_machine.AddDetectorSetup(DetectorGeometry(4));

    DatasetSettings setup;

    state_machine.DebugOnly_SetState(JFJochState::Measuring);

    REQUIRE(state_machine.GetStatus().state == JFJochState::Measuring);

    REQUIRE_THROWS(state_machine.Start(setup));
    REQUIRE_THROWS(state_machine.Pedestal());
    REQUIRE_THROWS(state_machine.Initialize());
    REQUIRE(state_machine.WaitTillMeasurementDone(std::chrono::milliseconds(1)).state == JFJochState::Measuring);

    DetectorSettings settings{};
    REQUIRE_THROWS(state_machine.LoadDetectorSettings(settings));
}

TEST_CASE("JFJochStateMachine_State_Error") {
    Logger logger("JFJochStateMachine_State_Error");
    JFJochServices services(logger);
    JFJochStateMachine state_machine(services, logger);
    state_machine.AddDetectorSetup(DetectorGeometry(4));

    DatasetSettings setup;

    state_machine.DebugOnly_SetState(JFJochState::Error,
        "msg1234",
        BrokerStatus::MessageSeverity::Error);

    REQUIRE(state_machine.GetStatus().state == JFJochState::Error);
    REQUIRE(state_machine.GetStatus().message.has_value());
    REQUIRE(state_machine.GetStatus().message == "msg1234");
    REQUIRE(state_machine.GetStatus().message_severity == BrokerStatus::MessageSeverity::Error);

    state_machine.DebugOnly_SetState(JFJochState::Inactive, "msg3456", BrokerStatus::MessageSeverity::Info);
    REQUIRE(state_machine.GetStatus().state == JFJochState::Inactive);
    REQUIRE(state_machine.GetStatus().message.has_value());
    REQUIRE(state_machine.GetStatus().message == "msg3456");
    REQUIRE(state_machine.GetStatus().message_severity == BrokerStatus::MessageSeverity::Info);

    state_machine.DebugOnly_SetState(JFJochState::Error);
    REQUIRE(state_machine.GetStatus().state == JFJochState::Error);
    REQUIRE(!state_machine.GetStatus().message.has_value());

    REQUIRE_THROWS(state_machine.Start(setup));
    REQUIRE_THROWS(state_machine.Pedestal());

    REQUIRE(state_machine.WaitTillMeasurementDone(std::chrono::milliseconds(1)).state == JFJochState::Error);

    DetectorSettings settings;
    REQUIRE_NOTHROW(state_machine.LoadDetectorSettings(settings));

    REQUIRE_NOTHROW(state_machine.Initialize());
    REQUIRE_NOTHROW(state_machine.WaitTillMeasurementDone());
    REQUIRE(state_machine.GetStatus().state == JFJochState::Idle);
}

TEST_CASE("JFJochStateMachine_NoDetectorSetup") {
    Logger logger("JFJochStateMachine_NoDetectorSetup");
    JFJochServices services(logger);
    JFJochStateMachine state_machine(services, logger);
    REQUIRE_THROWS(state_machine.Initialize());
    REQUIRE_NOTHROW(state_machine.WaitTillMeasurementDone());
}

TEST_CASE("JFJochStateMachine_AddDetectorSetup") {
    Logger logger("JFJochStateMachine_AddDetectorSetup");
    JFJochServices services(logger);
    JFJochStateMachine state_machine(services, logger);
    DetectorSetup setup = DetectorGeometry(4);
    state_machine.AddDetectorSetup(setup);
    REQUIRE_NOTHROW(state_machine.Initialize());
    REQUIRE_NOTHROW(state_machine.WaitTillMeasurementDone());
}

TEST_CASE("JFJochStateMachine_AddDetectorSetup_Gain") {
    Logger logger("JFJochStateMachine_AddDetectorSetup_Gain");
    JFJochServices services(logger);
    JFJochStateMachine state_machine(services, logger);
    DetectorSetup setup = DetectorGeometry(4);
    setup.LoadGain({"../../tests/test_data/gainMaps_M049.bin",
                    "../../tests/test_data/gainMaps_M049.bin",
                    "../../tests/test_data/gainMaps_M049.bin",
                    "../../tests/test_data/gainMaps_M049.bin"});
    state_machine.AddDetectorSetup(setup);
    REQUIRE_NOTHROW(state_machine.Initialize());
    REQUIRE_NOTHROW(state_machine.WaitTillMeasurementDone());
}

TEST_CASE("JFJochStateMachine_AddDetectorSetup_Multiple") {
    Logger logger("JFJochBrokerService_StorageCells");
    JFJochServices services(logger);
    JFJochStateMachine state_machine(services, logger);
    REQUIRE_NOTHROW(state_machine.AddDetectorSetup(DetectorSetup(DetectorGeometry(4), DetectorType::JUNGFRAU, "Det1",
                                                                 {"mx1", "mx2", "mx3", "mx4"})));
    REQUIRE_NOTHROW(state_machine.AddDetectorSetup(DetectorSetup(DetectorGeometry(2), DetectorType::JUNGFRAU, "Det2",
                                                                 {"mx1", "mx2"})));
    REQUIRE_NOTHROW(state_machine.AddDetectorSetup(DetectorSetup(DetectorGeometry(1), DetectorType::JUNGFRAU, "Det3",
                                                                 {"mx1"})));

    auto dl = state_machine.GetDetectorsList();
    REQUIRE(dl.detector.size() == 3);
    REQUIRE(dl.detector[0].description == "Det1");
    REQUIRE(dl.detector[0].nmodules == 4);
    REQUIRE(dl.detector[1].description == "Det2");
    REQUIRE(dl.detector[1].nmodules == 2);
    REQUIRE(dl.detector[2].description == "Det3");
    REQUIRE(dl.detector[2].nmodules == 1);

    REQUIRE_NOTHROW(state_machine.Initialize());
    REQUIRE_NOTHROW(state_machine.WaitTillMeasurementDone());
    REQUIRE(state_machine.NotThreadSafe_Experiment().GetModulesNum() == 4);
    REQUIRE(state_machine.GetStatus().state == JFJochState::Idle);

    REQUIRE_THROWS(state_machine.SelectDetector(7));
    REQUIRE(state_machine.GetStatus().state == JFJochState::Idle);

    REQUIRE_NOTHROW(state_machine.SelectDetector(2));
    REQUIRE(state_machine.NotThreadSafe_Experiment().GetModulesNum() == 1);
    REQUIRE(state_machine.NotThreadSafe_Experiment().GetDetectorDescription() == "Det3");
    REQUIRE(state_machine.GetStatus().state == JFJochState::Inactive);
}
