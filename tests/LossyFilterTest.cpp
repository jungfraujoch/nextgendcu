// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>
#include "../receiver/LossyFilter.h"

TEST_CASE("LossyFilterDisabled","[LossyFilter]") {
    LossyFilter filter(1.0);

    DataMessage message{
        .number = 123,
        .indexing_result = 0
    };

    REQUIRE(filter.ApplyFilter(message));
    REQUIRE(message.number == 123);
    REQUIRE(filter.ApplyFilter(message));
    REQUIRE(message.number == 123);
    REQUIRE(filter.ApplyFilter(message));
    REQUIRE(message.number == 123);
}

TEST_CASE("LossyFilterEnable","[LossyFilter]") {
    LossyFilter filter(0.0);

    DataMessage message_1{
            .number = 123,
            .indexing_result = 1
    };

    DataMessage message_2{
            .number = 124,
            .indexing_result = 0
    };

    REQUIRE(filter.ApplyFilter(message_1));
    REQUIRE(message_1.number == 0);
    REQUIRE(!filter.ApplyFilter(message_2));
    REQUIRE(filter.ApplyFilter(message_1));
    REQUIRE(message_1.number == 1);
    REQUIRE(filter.ApplyFilter(message_1));
    REQUIRE(message_1.number == 2);
}

TEST_CASE("LossyFilterEnable_IncrNumber","[LossyFilter]") {
    LossyFilter filter(0.999);

    DataMessage message_1{
            .number = 123,
            .indexing_result = 1
    };

    REQUIRE(filter.ApplyFilter(message_1));
    REQUIRE(message_1.number == 0);
    REQUIRE(filter.ApplyFilter(message_1));
    REQUIRE(message_1.number == 1);
    REQUIRE(filter.ApplyFilter(message_1));
    REQUIRE(message_1.number == 2);
}

TEST_CASE("LossyFilterEnable_Prob","[LossyFilter]") {
    LossyFilter filter(0.5);

    int64_t accepted = 0;
    int64_t count = 0;
    for (int i = 0; i < 1000000; i++) {
        DataMessage message{
                .number = 124,
                .indexing_result = 0
        };
        accepted += filter.ApplyFilter(message);
        count++;
    }
    double frac = static_cast<double>(accepted) / static_cast<double>(count);
    REQUIRE(frac < 0.51);
    REQUIRE(frac > 0.49);
}
