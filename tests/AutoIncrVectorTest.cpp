// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>
#include "../common/AutoIncrVector.h"

TEST_CASE("AutoIncrVector","[AutoIncrVector]") {
    AutoIncrVector<int64_t> aiv;

    REQUIRE(aiv.size() == 0);

    REQUIRE_NOTHROW(aiv.reserve(500));
    REQUIRE_THROWS(aiv.reserve(-5));

    REQUIRE_THROWS(aiv[-5] = 1);
    REQUIRE_NOTHROW(aiv[5] = 1);
    REQUIRE(aiv.size() == 6);
    REQUIRE(aiv[4] == 0);
    REQUIRE(aiv[5] == 1);

    const auto &ref = aiv;

    REQUIRE(ref[5] == 1);
    REQUIRE_THROWS(ref[6]);
}