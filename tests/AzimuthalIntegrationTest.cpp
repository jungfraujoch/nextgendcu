// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>

#include "../image_analysis/AzimuthalIntegrationProfile.h"

TEST_CASE("AzimuthalIntegrationMapping_Constructor","[AzimuthalIntegration]") {
    DiffractionExperiment x;

    std::unique_ptr<AzimuthalIntegrationMapping> radial;

    x.QSpacingForAzimInt_recipA(0.1).LowQForAzimInt_recipA(0.1).HighQForAzimInt_recipA(5);
    REQUIRE_NOTHROW(radial = std::make_unique<AzimuthalIntegrationMapping>(x));
}

TEST_CASE("AzimuthalIntegrationMapping_GetBinNumber","[AzimuthalIntegration]") {
    DiffractionExperiment x(DetectorGeometry(8, 2, 8, 36));
    x.DetectorDistance_mm(50).BeamX_pxl(1000).BeamY_pxl(1000);
    x.QSpacingForAzimInt_recipA(0.1).LowQForAzimInt_recipA(0.1).HighQForAzimInt_recipA(4);
    AzimuthalIntegrationMapping mapping(x);
    REQUIRE(mapping.GetBinNumber() == 39);
}

TEST_CASE("AzimuthalIntegrationMapping_GetBinNumber_DetectorLimit","[AzimuthalIntegration]") {
    DiffractionExperiment x(DetectorGeometry(8, 2, 8, 36));
    x.DetectorDistance_mm(50).BeamX_pxl(1000).BeamY_pxl(1000);

    x.QSpacingForAzimInt_recipA(0.1).LowQForAzimInt_recipA(0.1).HighQForAzimInt_recipA(9.9);
    AzimuthalIntegrationMapping mapping(x);
    REQUIRE(mapping.GetBinNumber() < 80);
}

TEST_CASE("AzimuthalIntegrationMapping_GetBinToQ","[AzimuthalIntegration]") {
    DiffractionExperiment x(DetectorGeometry(8, 2, 8, 36));
    x.DetectorDistance_mm(50).BeamX_pxl(1000).BeamY_pxl(1000);
    x.QSpacingForAzimInt_recipA(0.1).LowQForAzimInt_recipA(0.1).HighQForAzimInt_recipA(4);

    AzimuthalIntegrationMapping mapping(x);
    auto bin_to_q = mapping.GetBinToQ();

    REQUIRE(bin_to_q[0] == Catch::Approx(0.15));
    REQUIRE(bin_to_q[1] == Catch::Approx(0.25));
    REQUIRE(bin_to_q[15] == Catch::Approx(1.65));
    REQUIRE(bin_to_q[38] == Catch::Approx(3.95));
}

TEST_CASE("AzimuthalIntegrationMapping_QToBin","[AzimuthalIntegration]") {
    DiffractionExperiment x(DetectorGeometry(8, 2, 8, 36));
    x.DetectorDistance_mm(50).BeamX_pxl(1000).BeamY_pxl(1000);
    x.QSpacingForAzimInt_recipA(0.1).LowQForAzimInt_recipA(0.1).HighQForAzimInt_recipA(4);

    AzimuthalIntegrationMapping mapping(x);

    REQUIRE(mapping.QToBin(0.0) == 0);
    REQUIRE(std::floor(mapping.QToBin(0.200001)) == 1);
    REQUIRE(mapping.QToBin(0.6) == Catch::Approx(5));
    REQUIRE(mapping.QToBin(50.0) == Catch::Approx(38));
}

TEST_CASE("AzimuthalIntegrationProfile","[AzimuthalIntegration]") {
    DiffractionExperiment x(DetectorGeometry(8, 2, 8, 36));
    x.DetectorDistance_mm(50).BeamX_pxl(1000).BeamY_pxl(1000);
    x.QSpacingForAzimInt_recipA(0.1).LowQForAzimInt_recipA(0.1).HighQForAzimInt_recipA(4);

    AzimuthalIntegrationMapping mapping(x);

    AzimuthalIntegrationProfile profile(mapping);

    std::vector<float> sum(mapping.GetBinNumber());
    std::vector<uint32_t> count(mapping.GetBinNumber());

    for (int i = 0; i < mapping.GetBinNumber(); i++) {
        sum[i] = i * i * 4;
        count[i] = i;
    }
    REQUIRE_NOTHROW(profile.Add(sum, count));
    REQUIRE_NOTHROW(profile.Add(sum, count));

    std::vector<float> sum_wr(mapping.GetBinNumber() - 1);
    REQUIRE_THROWS(profile.Add(sum_wr, count));

    auto plot = profile.GetPlot();
    REQUIRE(plot.size() == 1);

    REQUIRE(plot[0].x.size() == mapping.GetBinNumber());
    REQUIRE(plot[0].y.size() == mapping.GetBinNumber());
    for (int i = 0; i < mapping.GetBinNumber(); i++) {
        REQUIRE(plot[0].x[i] == Catch::Approx(mapping.GetBinToQ()[i]));
        REQUIRE(plot[0].y[i] == Catch::Approx(i * 4));
    }
}

TEST_CASE("AzimuthalIntegrationProfile_operatorAdd","[AzimuthalIntegration]") {
    DiffractionExperiment x(DetectorGeometry(8, 2, 8, 36));
    x.DetectorDistance_mm(50).BeamX_pxl(1000).BeamY_pxl(1000);
    x.QSpacingForAzimInt_recipA(0.1).LowQForAzimInt_recipA(0.1).HighQForAzimInt_recipA(4);

    AzimuthalIntegrationMapping mapping(x);

    AzimuthalIntegrationProfile profile0(mapping), profile1(mapping);

    std::vector<float> sum(mapping.GetBinNumber());
    std::vector<uint32_t> count(mapping.GetBinNumber());

    for (int i = 0; i < mapping.GetBinNumber(); i++) {
        sum[i] = i * i * 4;
        count[i] = i;
    }
    REQUIRE_NOTHROW(profile0.Add(sum, count));
    REQUIRE_NOTHROW(profile1 += profile0);

    auto plot = profile1.GetPlot();

    REQUIRE(plot.size() == 1);
    REQUIRE(plot[0].x.size() == mapping.GetBinNumber());
    REQUIRE(plot[0].y.size() == mapping.GetBinNumber());
    for (int i = 0; i < mapping.GetBinNumber(); i++) {
        REQUIRE(plot[0].x[i] == Catch::Approx(mapping.GetBinToQ()[i]));
        REQUIRE(plot[0].y[i] == Catch::Approx(i * 4));
    }
}

TEST_CASE("AzimuthalIntegrationProfile_GetMeanValueOfBins","[AzimuthalIntegration]") {
    DiffractionExperiment x(DetectorGeometry(8, 2, 8, 36));
    x.DetectorDistance_mm(50).BeamX_pxl(1000).BeamY_pxl(1000);
    x.QSpacingForAzimInt_recipA(0.1).LowQForAzimInt_recipA(0.1).HighQForAzimInt_recipA(4);

    AzimuthalIntegrationMapping mapping(x);

    AzimuthalIntegrationProfile profile(mapping);

    std::vector<float> sum(mapping.GetBinNumber());
    std::vector<uint32_t> count(mapping.GetBinNumber());

    for (int i = 0; i < mapping.GetBinNumber(); i++) {
        sum[i] = i * i * 4;
        count[i] = i;
    }
    REQUIRE_NOTHROW(profile.Add(sum, count));

    REQUIRE(profile.GetMeanValueOfBins(0,2) == Catch::Approx((sum[0] + sum[1] + sum[2])  / double(count[0] + count[1] + count[2])));
    REQUIRE(profile.GetMeanValueOfBins(5,7) == Catch::Approx((sum[5] + sum[6] + sum[7])  / double (count[5] + count[6] + count[7])));
}
