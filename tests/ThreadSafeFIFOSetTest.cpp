// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>
#include <../common/ThreadSafeFIFO.h>

using namespace std::chrono_literals;

TEST_CASE("ThreadSafeFIFO","[ThreadSafeFIFO]") {
    ThreadSafeFIFO<uint32_t> fifo;
    uint32_t tmp;
    fifo.Put(0);
    fifo.Put(1);

    REQUIRE(fifo.Get(tmp) == 1);
    CHECK (tmp == 0);

    fifo.Put(0);

    REQUIRE(fifo.Get(tmp) == 1);
    CHECK (tmp == 1);

    REQUIRE(fifo.Get(tmp) == 1);
    CHECK (tmp == 0);

    REQUIRE(fifo.Get(tmp) == 0);
}


TEST_CASE("ThreadSafeFIFO_LimitedSize","[ThreadSafeFIFO]") {
    ThreadSafeFIFO<uint32_t> fifo(5);
    uint32_t tmp;
    REQUIRE(fifo.Put(0));
    REQUIRE(fifo.Put(1));
    REQUIRE(fifo.Put(2));
    REQUIRE(fifo.Put(3));
    REQUIRE(fifo.Put(4));
    REQUIRE(!fifo.Put(5));
    REQUIRE(fifo.Size() == 5);

    REQUIRE(fifo.Get(tmp) == 1);
    CHECK (tmp == 0);

    REQUIRE(fifo.Size() == 4);

    fifo.PutBlocking(5);
    REQUIRE(fifo.Size() == 5);
    fifo.GetBlocking();
    fifo.GetBlocking();
    fifo.GetBlocking();
    fifo.GetBlocking();
    fifo.GetBlocking();
}

TEST_CASE("ThreadSafeFIFO_GetTimeout","[ThreadSafeFIFO]") {
    ThreadSafeFIFO<uint32_t> fifo;
    uint32_t tmp;
    fifo.Put(0);
    fifo.Put(1);

    REQUIRE(fifo.GetTimeout(tmp, 1ms) == 1);
    CHECK (tmp == 0);

    fifo.Put(0);

    REQUIRE(fifo.GetTimeout(tmp, 1ms)== 1);
    CHECK (tmp == 1);

    REQUIRE(fifo.GetTimeout(tmp, 1ms) == 1);
    CHECK (tmp == 0);

    REQUIRE(fifo.GetTimeout(tmp, 1ms) == 0);
    REQUIRE(fifo.GetTimeout(tmp, 1ms) == 0);
}

TEST_CASE("ThreadSafeSet","[ThreadSafeFIFO]") {
    ThreadSafeSet<uint32_t> set;
    uint32_t tmp;
    set.Put(0);
    set.Put(1);

    REQUIRE(set.Get(tmp) == 1);
    CHECK (tmp == 0);

    set.Put(0);

    REQUIRE(set.Get(tmp) == 1);
    CHECK (tmp == 0);

    REQUIRE(set.Get(tmp) == 1);
    CHECK (tmp == 1);

    REQUIRE(set.Get(tmp) == 0);
}

TEST_CASE("ThreadSafeFIFO_Utilization","[ThreadSafeFIFO]") {
    ThreadSafeFIFO<uint32_t> fifo;
    uint32_t tmp;
    REQUIRE(fifo.GetMaxUtilization() == 0);
    fifo.Put(0);
    fifo.Put(1);
    REQUIRE(fifo.GetMaxUtilization() == 2);
    fifo.GetBlocking();
    REQUIRE(fifo.GetMaxUtilization() == 2);
    fifo.ClearMaxUtilization();
    REQUIRE(fifo.GetMaxUtilization() == 1);
    fifo.Clear();
    REQUIRE(fifo.GetMaxUtilization() == 0);
}