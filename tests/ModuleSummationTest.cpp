// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>
#include "../common/ModuleSummation.h"

TEST_CASE("ModuleSummation_signed") {
    constexpr size_t nsummation = 27;

    ModuleSummation summation(true);

    DeviceOutput output0{};
    output0.pixels[0] = 1;
    output0.pixels[1] = INT16_MIN;
    output0.pixels[2765] = INT16_MAX;
    output0.pixels[500000] = 123;
    output0.pixels[500001] = INT16_MAX;
    output0.adu_histogram[7] = 345;

    output0.module_statistics.err_pixels = 1;
    output0.module_statistics.saturated_pixels = 4;
    output0.module_statistics.packet_count = 255;

    output0.roi_counts[0].sum = 8;
    output0.roi_counts[0].sum2 = 888;
    output0.roi_counts[0].good_pixels = 340;
    output0.roi_counts[0].max_value = 234;

    for (int i = 0; i < nsummation; i++)
        summation.AddFPGAOutput(output0);

    DeviceOutput output1{};

    output1.pixels[0] = 0;
    output1.pixels[1] = 0;
    output1.pixels[2765] = 0;
    output1.pixels[500000] = 0;
    output1.pixels[500001] = INT16_MIN;
    output1.adu_histogram[7] = 812;
    output1.module_statistics.err_pixels = 2;
    output1.module_statistics.saturated_pixels = 5;
    output1.module_statistics.packet_count = 1;

    output1.roi_counts[0].sum = 1;
    output1.roi_counts[0].sum2 = 95;
    output1.roi_counts[0].good_pixels = 1;
    output1.roi_counts[0].max_value = 500;

    summation.AddFPGAOutput(output1);

    auto out32 = reinterpret_cast<const uint32_t *>(summation.GetOutput().pixels);
    CHECK(out32[0] == nsummation * output0.pixels[0]);
    CHECK(out32[1] == INT32_MIN);
    CHECK(out32[2765] == INT32_MAX);
    CHECK(out32[500000] == nsummation * output0.pixels[500000]);
    CHECK(out32[500001] == INT32_MIN);

    CHECK(summation.GetOutput().module_statistics.err_pixels == nsummation * 1 + 2);
    CHECK(summation.GetOutput().module_statistics.saturated_pixels == nsummation * 4 + 5);
    CHECK(summation.GetOutput().module_statistics.packet_count == nsummation * 255 + 1);

    CHECK(summation.GetOutput().roi_counts[0].sum == nsummation * 8 + 1);
    CHECK(summation.GetOutput().roi_counts[0].sum2 == nsummation * 888 + 95);
    CHECK(summation.GetOutput().roi_counts[0].good_pixels == nsummation * 340 + 1);
    CHECK(summation.GetOutput().roi_counts[0].max_value == 500);

    CHECK(summation.GetOutput().adu_histogram[7] == nsummation * 345 + 812);
}

TEST_CASE("ModuleSummation_unsigned") {
    constexpr size_t nsummation = 27;

    ModuleSummation summation(false);

    DeviceOutput output0{};
    const auto pxls = reinterpret_cast<uint16_t *>(output0.pixels);
    pxls[0] = 1;
    pxls[1] = UINT16_MAX;
    pxls[2765] = INT16_MAX;
    pxls[500000] = 123;
    for (int i = 0; i < nsummation; i++)
        summation.AddFPGAOutput(output0);

    auto out32 = reinterpret_cast<const uint32_t *>(summation.GetOutput().pixels);
    CHECK(out32[0] == nsummation * output0.pixels[0]);
    CHECK(out32[1] == UINT32_MAX);
    CHECK(out32[2765] == INT16_MAX * nsummation);
    CHECK(out32[500000] == nsummation * output0.pixels[500000]);

}
