// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>

#include <fstream>
#include "../preview//JFJochJPEG.h"
#include "../preview//PreviewImage.h"
#include "../writer/HDF5Objects.h"
#include "../common/RawToConvertedGeometry.h"

TEST_CASE("JPEGTest","[JPEG]") {
    size_t width = 1024;
    size_t height = 1024;
    std::vector<unsigned char> val(3 * width * height);
    for (int i = 0; i < width * height; i++) {
        val[i * 3] = 234;
        val[i * 3 + 1] = 0;
        val[i * 3 + 2] = 0;
    }

    std::string s;
    REQUIRE_NOTHROW(s = WriteJPEGToMem(val, width, height));
    std::ofstream f("x.jpeg", std::ios::binary);
    f.write(s.data(), s.size());
}

TEST_CASE("PreviewImage_NotConfigured","[JPEG]") {
    std::vector<int16_t> image_conv_2(67878);
    std::vector<SpotToSave> spots;

    PreviewImage image;
    REQUIRE_THROWS(image.UpdateImage(image_conv_2.data(), spots));
    REQUIRE(image.GenerateJPEG(PreviewJPEGSettings()).empty());
}


TEST_CASE("PreviewImage_GenerateJPEG","[JPEG]") {
    RegisterHDF5Filter();

    DiffractionExperiment experiment(DetectorGeometry(8,2,8,36));
    experiment.ImagesPerTrigger(5).NumTriggers(1).UseInternalPacketGenerator(true)
            .FilePrefix("lyso_test_min_pix_2").JungfrauConvPhotonCnt(false)
            .DetectorDistance_mm(75).BeamY_pxl(1136).BeamX_pxl(1090).IncidentEnergy_keV(12.4)
            .SetUnitCell(UnitCell{.a = 36.9, .b = 78.95, .c = 78.95, .alpha =90, .beta = 90, .gamma = 90});

    experiment.ROI().SetROI(ROIDefinition{
        .boxes = {ROIBox("roi0", 800, 1200, 800, 1200)},
        .circles = {ROICircle("roi2", 500, 500, 50)}
    });

    // Load example image
    HDF5ReadOnlyFile data("../../tests/test_data/compression_benchmark.h5");
    HDF5DataSet dataset(data, "/entry/data/data");
    HDF5DataSpace file_space(dataset);

    std::vector<int16_t> image_conv (file_space.GetDimensions()[1] * file_space.GetDimensions()[2]);

    std::vector<hsize_t> start = {4,0,0};
    std::vector<hsize_t> file_size = {1, file_space.GetDimensions()[1], file_space.GetDimensions()[2]};
    dataset.ReadVector(image_conv, start, file_size);

    // to fill gaps with INT16_MIN
    std::vector<int16_t> image_raw_geom(experiment.GetModulesNum() * RAW_MODULE_SIZE);
    std::vector<int16_t> image_conv_2 (file_space.GetDimensions()[1] * file_space.GetDimensions()[2], INT16_MIN);

    ConvertedToRawGeometry(experiment, image_raw_geom.data(), image_conv.data());
    RawToConvertedGeometry(experiment, image_conv_2.data(), image_raw_geom.data());

    std::vector<SpotToSave> spots = {
            {.x = 500,  .y = 400, .indexed = true},
            {.x = 800,  .y = 1000, .indexed = false},
            {.x = 1200, .y = 500, .indexed = true}
    };

    PixelMask mask(experiment);

    PreviewImage image;
    image.Configure(experiment, mask);

    PreviewJPEGSettings preview_settings{
            .saturation_value = 5,
            .jpeg_quality = 70,
            .show_spots = true
    };

    REQUIRE(image.GenerateJPEG(preview_settings).empty());

    image.UpdateImage(image_conv_2.data(), spots);

    std::string s;
    REQUIRE_NOTHROW(s = image.GenerateJPEG(preview_settings));
    std::ofstream f("lyso_diff.jpeg", std::ios::binary);
    f.write(s.data(), s.size());
}

TEST_CASE("PreviewImage_GenerateJPEG_ROI","[JPEG]") {
    RegisterHDF5Filter();

    DiffractionExperiment experiment(DetectorGeometry(8,2,8,36));
    experiment.ImagesPerTrigger(5).NumTriggers(1).UseInternalPacketGenerator(true)
            .FilePrefix("lyso_test_min_pix_2").JungfrauConvPhotonCnt(false)
            .DetectorDistance_mm(75).BeamY_pxl(1136).BeamX_pxl(1090).IncidentEnergy_keV(12.4)
            .SetUnitCell(UnitCell{.a = 36.9, .b = 78.95, .c = 78.95, .alpha =90, .beta = 90, .gamma = 90});

    experiment.ROI().SetROI(ROIDefinition{
        .boxes = {
            ROIBox("roi1", 0, 20, 0, 20),
            ROIBox("roi0", 800, 1200, 800, 1200),
            ROIBox("roi3", 2000, 2067, 2000, 2163)
        },
        .circles = {ROICircle("roi2", 500, 500, 50)}
    });

    // Load example image
    HDF5ReadOnlyFile data("../../tests/test_data/compression_benchmark.h5");
    HDF5DataSet dataset(data, "/entry/data/data");
    HDF5DataSpace file_space(dataset);

    std::vector<int16_t> image_conv (file_space.GetDimensions()[1] * file_space.GetDimensions()[2]);

    std::vector<hsize_t> start = {4,0,0};
    std::vector<hsize_t> file_size = {1, file_space.GetDimensions()[1], file_space.GetDimensions()[2]};
    dataset.ReadVector(image_conv, start, file_size);

    // to fill gaps with INT16_MIN
    std::vector<int16_t> image_raw_geom(experiment.GetModulesNum() * RAW_MODULE_SIZE);
    std::vector<int16_t> image_conv_2 (file_space.GetDimensions()[1] * file_space.GetDimensions()[2], INT16_MIN);

    ConvertedToRawGeometry(experiment, image_raw_geom.data(), image_conv.data());
    RawToConvertedGeometry(experiment, image_conv_2.data(), image_raw_geom.data());

    std::vector<SpotToSave> spots = {
            {.x = 500,  .y = 400, .indexed = true},
            {.x = 800,  .y = 1000, .indexed = false},
            {.x = 1200, .y = 500, .indexed = true}
    };
    PreviewImage image;

    PixelMask mask(experiment);

    image.Configure(experiment, mask);
    image.UpdateImage(image_conv_2.data(), spots);

    PreviewJPEGSettings preview_settings{
            .saturation_value = 5,
            .jpeg_quality = 70,
            .show_spots = true,
            .show_roi = true
    };

    std::string s;
    REQUIRE_NOTHROW(s = image.GenerateJPEG(preview_settings));
    std::ofstream f("lyso_diff_roi.jpeg", std::ios::binary);
    f.write(s.data(), s.size());
}

TEST_CASE("PreviewImage_GenerateJPEG_resolution","[JPEG]") {
    RegisterHDF5Filter();

    DiffractionExperiment experiment(DetectorGeometry(8,2,8,36));
    experiment.ImagesPerTrigger(5).NumTriggers(1).UseInternalPacketGenerator(true)
            .FilePrefix("lyso_test_min_pix_2").JungfrauConvPhotonCnt(false)
            .DetectorDistance_mm(75).BeamY_pxl(1136).BeamX_pxl(1090).IncidentEnergy_keV(12.4)
            .SetUnitCell(UnitCell{.a = 36.9, .b = 78.95, .c = 78.95, .alpha =90, .beta = 90, .gamma = 90});

    // Load example image
    HDF5ReadOnlyFile data("../../tests/test_data/compression_benchmark.h5");
    HDF5DataSet dataset(data, "/entry/data/data");
    HDF5DataSpace file_space(dataset);

    std::vector<int16_t> image_conv (file_space.GetDimensions()[1] * file_space.GetDimensions()[2]);

    std::vector<hsize_t> start = {4,0,0};
    std::vector<hsize_t> file_size = {1, file_space.GetDimensions()[1], file_space.GetDimensions()[2]};
    dataset.ReadVector(image_conv, start, file_size);

    // to fill gaps with INT16_MIN
    std::vector<int16_t> image_raw_geom(experiment.GetModulesNum() * RAW_MODULE_SIZE);
    std::vector<int16_t> image_conv_2 (file_space.GetDimensions()[1] * file_space.GetDimensions()[2], INT16_MIN);

    ConvertedToRawGeometry(experiment, image_raw_geom.data(), image_conv.data());
    RawToConvertedGeometry(experiment, image_conv_2.data(), image_raw_geom.data());

    PixelMask mask(experiment);

    std::vector<SpotToSave> spots = {};
    PreviewImage image;
    image.Configure(experiment, mask);
    image.UpdateImage(image_conv_2.data(), spots);

    PreviewJPEGSettings preview_settings{
            .saturation_value = 5,
            .jpeg_quality = 70,
            .show_spots = false,
            .show_roi = false,
            .resolution_ring = 2.0
    };

    std::string s;
    REQUIRE_NOTHROW(s = image.GenerateJPEG(preview_settings));
    std::ofstream f("lyso_diff_d_ring.jpeg", std::ios::binary);
    f.write(s.data(), s.size());
}
