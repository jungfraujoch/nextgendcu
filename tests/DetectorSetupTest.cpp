// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>
#include "../common/DetectorSetup.h"
#include "../common/NetworkAddressConvert.h"

TEST_CASE("DetectorSetup_MismatchInSize") {
    REQUIRE_THROWS(DetectorSetup(DetectorGeometry(8), DetectorType::JUNGFRAU, "JF", {"mx1","mx2","mx3","mx4"}));
    REQUIRE_THROWS(DetectorSetup(DetectorGeometry(2), DetectorType::JUNGFRAU, "JF", {"mx1","mx2","mx3","mx4"}));
    REQUIRE_NOTHROW(DetectorSetup(DetectorGeometry(4), DetectorType::JUNGFRAU, "JF", {"mx1","mx2","mx3","mx4"}));
}

TEST_CASE("DetectorSetup_ProtoBuf") {
   DetectorSetup setup(DetectorGeometry(4), DetectorType::JUNGFRAU, "JF", {"mx1","mx2","mx3","mx4"});

   REQUIRE(setup.GetDescription() == "JF");
   REQUIRE(setup.GetDetectorModuleHostname().size() == 4);
   REQUIRE(setup.GetDetectorModuleHostname()[3] == "mx4");
   REQUIRE(setup.GetPixelSize_mm() == Catch::Approx(0.075));
   REQUIRE(setup.GetModulesNum() == 4);
   REQUIRE(setup.GetGeometry().GetModulesNum() == 4);
}

TEST_CASE("DetectorSetup_ProtoBuf_FullSpeed") {
    DetectorSetup setup(DetectorGeometry(4), DetectorType::JUNGFRAU, "JF", {"mx1","mx2","mx3","mx4"});

    REQUIRE(setup.GetUDPInterfaceCount() == 2);
    REQUIRE_NOTHROW(setup.UDPInterfaceCount(1));
    REQUIRE_THROWS(setup.UDPInterfaceCount(0));
    REQUIRE_THROWS(setup.UDPInterfaceCount(5));
    REQUIRE_THROWS(setup.UDPInterfaceCount(-56));

    REQUIRE(setup.GetUDPInterfaceCount() == 1);
}

TEST_CASE("DetectorSetup_IPv4Base_2Interfaces") {
    DetectorSetup detector(DetectorGeometry(2));
    detector.UDPInterfaceCount(2);
    REQUIRE_NOTHROW(detector.BaseIPv4Addr("64.1.124.1"));

    REQUIRE(detector.GetSrcIPv4Addr(0) == IPv4AddressFromStr("64.1.124.1"));
    REQUIRE(detector.GetSrcIPv4Addr(1) == IPv4AddressFromStr("64.1.124.2"));
    REQUIRE(detector.GetSrcIPv4Addr(3) == IPv4AddressFromStr("64.1.124.4"));

    REQUIRE_THROWS(detector.GetSrcIPv4Addr(4));
}

TEST_CASE("DetectorSetup_IPv4Base_1Interface") {
    DetectorSetup detector(DetectorGeometry(4));
    detector.UDPInterfaceCount(1);
    REQUIRE_NOTHROW(detector.BaseIPv4Addr("64.1.124.1"));

    REQUIRE(detector.GetSrcIPv4Addr(0) == IPv4AddressFromStr("64.1.124.1"));
    REQUIRE(detector.GetSrcIPv4Addr(1) == IPv4AddressFromStr("64.1.124.2"));
    REQUIRE(detector.GetSrcIPv4Addr(3) == IPv4AddressFromStr("64.1.124.4"));

    REQUIRE_THROWS(detector.GetSrcIPv4Addr(4));
}

TEST_CASE("DetectorSetup_LoadGainFile") {
    DetectorSetup setup(DetectorGeometry(4), DetectorType::JUNGFRAU, "JF", {"mx1","mx2","mx3","mx4"});
    REQUIRE_THROWS(setup.LoadGain({}));

    REQUIRE(setup.GetGainCalibration().empty());

    REQUIRE_THROWS(setup.LoadGain({
                                          "../../tests/test_data/gainMaps_M049.bin",
                                          "../../tests/test_data/gainMaps_M049.bin",
                                          "../../tests/test_data/gainMaps_M049.bin"
                                  }));

    REQUIRE_THROWS(setup.LoadGain({
                                          "../../tests/test_data/gainMaps_M049.bin",
                                          "../../tests/test_data/gainMaps_M049.bin",
                                          "../../tests/test_data/gainMaps_M049.bin",
                                          "../../tests/test_data/gainMaps_M049.bin",
                                          "../../tests/test_data/gainMaps_M049.bin"
                                  }));

    REQUIRE_NOTHROW(setup.LoadGain({
                                           "../../tests/test_data/gainMaps_M049.bin",
                                           "../../tests/test_data/gainMaps_M049.bin",
                                           "../../tests/test_data/gainMaps_M049.bin",
                                           "../../tests/test_data/gainMaps_M049.bin"
                                   }));
    REQUIRE(setup.GetGainCalibration().size() == 4);
}


TEST_CASE("DetectorSetup_MaxFrameTime") {
    DetectorSetup s1(DetectorGeometry(8), DetectorType::JUNGFRAU);
    s1.UDPInterfaceCount(1);
    REQUIRE(s1.GetMinFrameTime() == std::chrono::microseconds(MIN_FRAME_TIME_JUNGFRAU_HALF_SPEED_IN_US));
    s1.UDPInterfaceCount(2);
    REQUIRE(s1.GetMinFrameTime() == std::chrono::microseconds(MIN_FRAME_TIME_JUNGFRAU_FULL_SPEED_IN_US));

    DetectorSetup s2(DetectorGeometry(8), DetectorType::EIGER);
    REQUIRE(s2.GetMinFrameTime() == std::chrono::microseconds(MIN_FRAME_TIME_EIGER_IN_US));
}