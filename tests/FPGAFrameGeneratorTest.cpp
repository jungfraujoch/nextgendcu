// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>
#include "../fpga/hls_simulation/hls_cores.h"

TEST_CASE("FPGA_FrameGenerator_Cancel") {
    STREAM_512 data_out;
    std::vector<ap_uint<256> > d_hbm_p0(RAW_MODULE_SIZE / (256 / 8) * 22 * 32);
    std::vector<ap_uint<256> > d_hbm_p1(RAW_MODULE_SIZE / (256 / 8) * 22 * 32);
    ap_uint<1> cancel = 1;
    FrameGeneratorConfig config{
        .frames = 256,
        .modules = 8,
        .detector_type = SLS_DETECTOR_TYPE_JUNGFRAU,
        .images_in_memory = 0
    };
    REQUIRE(frame_generator(data_out, d_hbm_p0.data(), d_hbm_p1.data(), 16*1024*1024, 0, 0, cancel, config) == 0);
    REQUIRE(data_out.size() == 130);
}


TEST_CASE("FPGA_FrameGenerator_WrongDetectorType") {
    STREAM_512 data_out;
    std::vector<ap_uint<256> > d_hbm_p0(RAW_MODULE_SIZE / (256 / 8) * 22 * 32);
    std::vector<ap_uint<256> > d_hbm_p1(RAW_MODULE_SIZE / (256 / 8) * 22 * 32);
    ap_uint<1> cancel = 1;
    FrameGeneratorConfig config{
        .frames = 256,
        .modules = 8,
        .detector_type = 0,
        .images_in_memory = 0
    };
    REQUIRE(frame_generator(data_out, d_hbm_p0.data(), d_hbm_p1.data(), 16*1024*1024, 0, 0, cancel, config) != 0);
    REQUIRE(data_out.size() == 0);
}

TEST_CASE("FPGA_FrameGenerator_WrongModuleCount") {
    STREAM_512 data_out;
    std::vector<ap_uint<256> > d_hbm_p0(RAW_MODULE_SIZE / (256 / 8) * 22 * 32);
    std::vector<ap_uint<256> > d_hbm_p1(RAW_MODULE_SIZE / (256 / 8) * 22 * 32);
    ap_uint<1> cancel = 1;
    FrameGeneratorConfig config{
        .frames = 256,
        .modules = MAX_MODULES_FPGA + 1,
        .detector_type = SLS_DETECTOR_TYPE_JUNGFRAU,
        .images_in_memory = 0
    };
    REQUIRE(frame_generator(data_out, d_hbm_p0.data(), d_hbm_p1.data(), 16*1024*1024, 0, 0, cancel, config) != 0);
    REQUIRE(data_out.size() == 0);

    config.modules = 0;
    REQUIRE(frame_generator(data_out, d_hbm_p0.data(), d_hbm_p1.data(), 16*1024*1024, 0, 0, cancel, config) != 0);
    REQUIRE(data_out.size() == 0);
}

TEST_CASE("FPGA_FrameGenerator_TooManyCells") {
    STREAM_512 data_out;
    std::vector<ap_uint<256> > d_hbm_p0(RAW_MODULE_SIZE / (256 / 8) * 22 * 32);
    std::vector<ap_uint<256> > d_hbm_p1(RAW_MODULE_SIZE / (256 / 8) * 22 * 32);
    ap_uint<1> cancel = 1;
    FrameGeneratorConfig config{
        .frames = 256,
        .modules = 4,
        .detector_type = SLS_DETECTOR_TYPE_JUNGFRAU,
        .images_in_memory = 8
    };
    REQUIRE(frame_generator(data_out, d_hbm_p0.data(), d_hbm_p1.data(), 16*1024*1024, 0, 0, cancel, config) == 3);
    REQUIRE(data_out.size() == 0);
}

TEST_CASE("FPGA_FrameGenerator_Run_JF") {
    STREAM_512 data_out;
    std::vector<ap_uint<256> > d_hbm_p0(RAW_MODULE_SIZE / (256 / 8) * 22 * 32);
    std::vector<ap_uint<256> > d_hbm_p1(RAW_MODULE_SIZE / (256 / 8) * 22 * 32);
    ap_uint<1> cancel = 0;
    FrameGeneratorConfig config{
        .frames = 2,
        .modules = 2,
        .detector_type = SLS_DETECTOR_TYPE_JUNGFRAU,
        .images_in_memory = 0
    };
    REQUIRE(frame_generator(data_out, d_hbm_p0.data(), d_hbm_p1.data(), 16*1024*1024, 0, 0, cancel, config) == 0);
    REQUIRE(data_out.size() == (config.frames * config.modules * 128 + 1) * 130);
}

TEST_CASE("FPGA_FrameGenerator_Run_EIGER") {
    STREAM_512 data_out;
    std::vector<ap_uint<256> > d_hbm_p0(RAW_MODULE_SIZE / (256 / 8) * 22 * 32);
    std::vector<ap_uint<256> > d_hbm_p1(RAW_MODULE_SIZE / (256 / 8) * 22 * 32);
    ap_uint<1> cancel = 0;
    FrameGeneratorConfig config{
        .frames = 2,
        .modules = 2,
        .detector_type = SLS_DETECTOR_TYPE_EIGER,
        .images_in_memory = 0,
        .eiger_bit_depth = 16
    };
    REQUIRE(frame_generator(data_out, d_hbm_p0.data(), d_hbm_p1.data(), 16*1024*1024, 0, 0, cancel, config) == 0);
    REQUIRE(data_out.size() == (config.frames * config.modules * 256) * 66 + 130); // Trailing packet is JF type!
}


TEST_CASE("FPGA_FrameGenerator_Run_EIGER_32bit") {
    STREAM_512 data_out;
    std::vector<ap_uint<256> > d_hbm_p0(RAW_MODULE_SIZE / (256 / 8) * 22 * 32);
    std::vector<ap_uint<256> > d_hbm_p1(RAW_MODULE_SIZE / (256 / 8) * 22 * 32);
    ap_uint<1> cancel = 0;
    FrameGeneratorConfig config{
        .frames = 2,
        .modules = 2,
        .detector_type = SLS_DETECTOR_TYPE_EIGER,
        .images_in_memory = 0,
        .eiger_bit_depth = 32
    };
    REQUIRE(frame_generator(data_out, d_hbm_p0.data(), d_hbm_p1.data(), 16*1024*1024, 0, 0, cancel, config) == 0);
    REQUIRE(data_out.size() == (config.frames * config.modules * 512) * 66 + 130); // Trailing packet is JF type!
}

TEST_CASE("FPGA_FrameGenerator_Run_EIGER_8bit") {
    STREAM_512 data_out;
    std::vector<ap_uint<256> > d_hbm_p0(RAW_MODULE_SIZE / (256 / 8) * 22 * 32);
    std::vector<ap_uint<256> > d_hbm_p1(RAW_MODULE_SIZE / (256 / 8) * 22 * 32);
    ap_uint<1> cancel = 0;
    FrameGeneratorConfig config{
        .frames = 2,
        .modules = 2,
        .detector_type = SLS_DETECTOR_TYPE_EIGER,
        .images_in_memory = 0,
        .eiger_bit_depth = 8
    };
    REQUIRE(frame_generator(data_out, d_hbm_p0.data(), d_hbm_p1.data(), 16*1024*1024, 0, 0, cancel, config) == 0);
    REQUIRE(data_out.size() == (config.frames * config.modules * 128) * 66 + 130); // Trailing packet is JF type!
}

TEST_CASE("FPGA_FrameGenerator_Run_EIGER_wrong_depth") {
    STREAM_512 data_out;
    std::vector<ap_uint<256> > d_hbm_p0(RAW_MODULE_SIZE / (256 / 8) * 22 * 32);
    std::vector<ap_uint<256> > d_hbm_p1(RAW_MODULE_SIZE / (256 / 8) * 22 * 32);
    ap_uint<1> cancel = 0;
    FrameGeneratorConfig config{
        .frames = 2,
        .modules = 2,
        .detector_type = SLS_DETECTOR_TYPE_EIGER,
        .images_in_memory = 0,
        .eiger_bit_depth = 0
    };
    REQUIRE(frame_generator(data_out, d_hbm_p0.data(), d_hbm_p1.data(), 16*1024*1024, 0, 0, cancel, config) != 0);
    REQUIRE(data_out.size() == 0); // Trailing packet is JF type!
}