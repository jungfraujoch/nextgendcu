// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>
#include "../common/DetectorSettings.h"

TEST_CASE("DetectorSettings_CountTime") {
    DetectorSettings d;

    REQUIRE_NOTHROW(d.FrameTime(std::chrono::microseconds(1000)));
    REQUIRE(d.GetFrameTime() == std::chrono::milliseconds(1));
    REQUIRE(!d.GetCountTime());

    REQUIRE_NOTHROW(d.FrameTime(std::chrono::microseconds(1000), std::chrono::microseconds(500)));
    REQUIRE(d.GetFrameTime() == std::chrono::milliseconds(1));
    REQUIRE(d.GetCountTime());
    REQUIRE(d.GetCountTime().value().count() == 500);
}