// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>

#include "../common/Histogram.h"

TEST_CASE("SetAverage") {
    SetAverage<float> h(100);
    h.Add(80, 30.0);
    h.Add(50, 20.0);
    h.Add(50, 30.0);
    h.Add(0, -2.0);
    h.Add(100, 50.0);

    auto p = h.GetPlot();
    REQUIRE(p.size() == 1);
    REQUIRE(p[0].x.size() == 100);
    REQUIRE(p[0].y.size() == 100);

    CHECK(p[0].x[0] == 0.0);
    CHECK(p[0].x[80] == 80.0);
    CHECK(p[0].x[30] == 30.0);
    CHECK(p[0].x[99] == 99.0);

    CHECK(p[0].y[0] == -2.0);
    CHECK(p[0].y[50] == 25.0);
    CHECK(p[0].y[80] == 30.0);
    CHECK(p[0].y[99] == 0.0);

}

TEST_CASE("FloatHistogram") {
    FloatHistogram h(100, 100.0, 200.0);
    h.Add(150.5);
    h.Add(100.2);
    h.Add(100.9999);
    h.Add(100.0);
    h.Add(100.00001);
    h.Add(101.0);
    h.Add(200.0);

    auto p = h.GetPlot();
    REQUIRE(p.size() == 1);
    REQUIRE(p[0].x.size() == 100);
    REQUIRE(p[0].y.size() == 100);

    CHECK(p[0].x[0] == 100.5);
    CHECK(p[0].x[34] == 134.5);
    CHECK(p[0].x[99] == 199.5);

    CHECK(p[0].y[0] == 4.0);
    CHECK(p[0].y[1] == 1.0);
    CHECK(p[0].y[50] == 1.0);
    CHECK(p[0].y[99] == 0.0);
}
