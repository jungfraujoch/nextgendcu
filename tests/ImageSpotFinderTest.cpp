// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>
#include "../image_analysis/ImageSpotFinder.h"

TEST_CASE("ImageSpotFinder_SignalToNoise") {
    size_t width = 100;
    size_t height = 100;
    std::vector<float> resolution(width * height, 2.0);

    resolution[width * 50 + 50] = 1.0;

    ImageSpotFinder s(resolution, width, height);

    std::vector<int32_t> input (width * height);
    for (int i = 0; i < width * height; i++)
        input[i] = (i % 2) * 5 + 5;

    input[width * 50 + 50] = 20;
    input[width * 25 + 26] = 16;
    input[width * 75 + 25] = 12;

    // Mean is 7.5, std. dev is 2.5
    SpotFindingSettings settings{
            .signal_to_noise_threshold = 3.0,
            .photon_count_threshold = 0,
            .min_pix_per_spot = 1,
            .max_pix_per_spot = 20,
            .high_resolution_limit = 0.5,
            .low_resolution_limit = 3.0,
    };

    auto spots = s.Run(input.data(), settings);

    REQUIRE(spots.size() == 2);
    REQUIRE(spots[0].RawCoord().y == 25);
    REQUIRE(spots[1].RawCoord().y == 50);
}

TEST_CASE("ImageSpotFinder_SignalToNoise_Resolution") {
    size_t width = 100;
    size_t height = 100;
    std::vector<float> resolution(width * height, 2.0);

    resolution[width * 50 + 50] = 1.0;

    ImageSpotFinder s(resolution, width, height);

    std::vector<int32_t> input (width * height);
    for (int i = 0; i < width * height; i++)
        input[i] = (i % 2) * 5 + 5;

    input[width * 50 + 50] = 20;
    input[width * 25 + 26] = 16;
    input[width * 75 + 25] = 12;

    // Mean is 7.5, std. dev is 2.5
    SpotFindingSettings settings{
            .signal_to_noise_threshold = 3.0,
            .photon_count_threshold = 0,
            .min_pix_per_spot = 1,
            .max_pix_per_spot = 20,
            .high_resolution_limit = 1.5,
            .low_resolution_limit = 3.0,
    };

    auto spots = s.Run(input.data(), settings);

    REQUIRE(spots.size() == 1);
    REQUIRE(spots[0].RawCoord().x == 26);
    REQUIRE(spots[0].RawCoord().y == 25);
}


TEST_CASE("ImageSpotFinder_CountThreshold_Resolution") {
    size_t width = 100;
    size_t height = 100;
    std::vector<float> resolution(width * height, 2.0);

    resolution[width * 50 + 50] = 1.0;

    ImageSpotFinder s(resolution, width, height);

    std::vector<int32_t> input (width * height);
    for (int i = 0; i < width * height; i++)
        input[i] = (i % 2) * 5 + 5;

    input[width * 50 + 50] = 20;
    input[width * 25 + 26] = 16;
    input[width * 75 + 25] = 12;

    // Mean is 7.5, std. dev is 2.5
    SpotFindingSettings settings{
            .signal_to_noise_threshold = -1,
            .photon_count_threshold = 11,
            .min_pix_per_spot = 1,
            .max_pix_per_spot = 20,
            .high_resolution_limit = 1.5,
            .low_resolution_limit = 3.0,
    };

    auto spots = s.Run(input.data(), settings);

    REQUIRE(spots.size() == 2);
    REQUIRE(spots[0].RawCoord().y == 25);
    REQUIRE(spots[1].RawCoord().y == 75);
}
