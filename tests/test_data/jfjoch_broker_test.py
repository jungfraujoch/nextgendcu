import requests
import h5py
import hdf5plugin

requests.post("http://localhost:5232/initialize")

with h5py.File("compression_benchmark.h5", "r") as f:
     img = f["/entry/data/data"]
     res = requests.put(url="http://localhost:5232/config/internal_generator_image?number=0", data = img[0, :, :].tobytes())

start_input = {
    "beam_x_pxl":1090, 
    "beam_y_pxl":1136,
    "detector_distance_mm":75, 
    "incident_energy_keV":12.4,
    "sample_name":"lyso", 
    "unit_cell":{"a":36.9, "b":78.95, "c":78.95, "alpha":90, "beta":90, "gamma":90}, 
    "images_per_trigger":5,
    "file_prefix":"lyso_test"
}
requests.post("http://localhost:5232/start", json=start_input)
