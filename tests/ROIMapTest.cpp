// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>
#include "../common/ROIMap.h"
#include "../common/DiffractionExperiment.h"

TEST_CASE("ROICircle", "[ROIMap]") {
    DiffractionExperiment x(DetectorGeometry(1));

    std::unique_ptr<ROICircle> circle;

    REQUIRE_THROWS(circle = std::make_unique<ROICircle>("roi1", 200,200,0));
    REQUIRE_THROWS(circle = std::make_unique<ROICircle>("roi1", 200,200,-5.0));
    REQUIRE_NOTHROW(circle = std::make_unique<ROICircle>("roi1", 200,220,2));

    REQUIRE(circle->GetName() == "roi1");
    REQUIRE(circle->GetX() == 200.0f);
    REQUIRE(circle->GetY() == 220.0f);
    REQUIRE(circle->GetRadius_pxl() == 2.0f);

    std::vector<uint16_t> mask(x.GetPixelsNum(), UINT16_MAX);
    REQUIRE_NOTHROW(circle->MarkROI(mask, 15, x.GetXPixelsNum(), x.GetYPixelsNum()));
    REQUIRE (mask[220 * x.GetXPixelsNum() + 200] == 15);
    REQUIRE (mask[218 * x.GetXPixelsNum() + 200] == 15);
    REQUIRE (mask[220 * x.GetXPixelsNum() + 202] == 15);
    REQUIRE (mask[221 * x.GetXPixelsNum() + 201] == 15);

    REQUIRE (mask[218 * x.GetXPixelsNum() + 202] == UINT16_MAX);
}

TEST_CASE("ROIBox", "[ROIMap]") {
    DiffractionExperiment x(DetectorGeometry(1));

    std::unique_ptr<ROIBox> rectangle;

    REQUIRE_THROWS(rectangle = std::make_unique<ROIBox>("roi1", 200,199,199, 221));
    REQUIRE_THROWS(rectangle = std::make_unique<ROIBox>("roi1", 200,220,199, 198));
    REQUIRE_THROWS(rectangle = std::make_unique<ROIBox>("roi1", -200,220,220, 221));
    REQUIRE_THROWS(rectangle = std::make_unique<ROIBox>("roi1", 200,-220,220, 221));
    REQUIRE_NOTHROW(rectangle = std::make_unique<ROIBox>("roi1", 198,202,218, 222));

    REQUIRE(rectangle->GetName() == "roi1");
    REQUIRE(rectangle->GetXMin() == 198);
    REQUIRE(rectangle->GetXMax() == 202);
    REQUIRE(rectangle->GetYMin() == 218);
    REQUIRE(rectangle->GetYMax() == 222);

    std::vector<uint16_t> mask(x.GetPixelsNum(), UINT16_MAX);
    REQUIRE_NOTHROW(rectangle->MarkROI(mask, 189, x.GetXPixelsNum(), x.GetYPixelsNum()));
    REQUIRE (mask[220 * x.GetXPixelsNum() + 202] == 189);
    REQUIRE (mask[218 * x.GetXPixelsNum() + 198] == 189);
    REQUIRE (mask[218 * x.GetXPixelsNum() + 202] == 189);
    REQUIRE (mask[222 * x.GetXPixelsNum() + 201] == 189);

    REQUIRE (mask[218 * x.GetXPixelsNum() + 202] == 189);

    REQUIRE (mask[218 * x.GetXPixelsNum() + 197] == UINT16_MAX);
    REQUIRE (mask[218 * x.GetXPixelsNum() + 203] == UINT16_MAX);
    REQUIRE (mask[217 * x.GetXPixelsNum() + 200] == UINT16_MAX);
    REQUIRE (mask[223 * x.GetXPixelsNum() + 200] == UINT16_MAX);
}

TEST_CASE("ROIMap") {
    DiffractionExperiment x(DetectorGeometry(1));

    ROIMap mask(x.GetDetectorSetup());

    mask.SetROI({
        .boxes = {
            ROIBox("roi12", 100, 120, 240, 260),
            ROIBox("roi11", 200, 220, 240, 260)
        },
        .circles = {ROICircle("roi7", 300, 400, 5)}
    });


    mask.SetROI({
        .boxes = {ROIBox("roi12", 100, 120, 240, 260)},
        .circles = {ROICircle("roi7", 300, 400, 5)}
    });

    // coordinate out of bounds
    REQUIRE_THROWS(mask.SetROI({.boxes = {ROIBox("roi15", 100, x.GetXPixelsNum(), 240, 260)}}));
    REQUIRE_THROWS(mask.SetROI({.boxes = {ROIBox("roi15", 100, 120, 240, x.GetYPixelsNum())}}));

    // Check arrays are proper size
    REQUIRE(mask.GetROINameMap().size() == 2);
    REQUIRE(mask.GetROIMap().size() == x.GetPixelsNum());

    auto it1 = mask.GetROINameMap().find("roi12");
    REQUIRE(it1 != mask.GetROINameMap().end());
    REQUIRE(it1->second == 0);
    REQUIRE(mask.GetROIMap()[260 * x.GetXPixelsNum() + 100] == 0);

    auto it2 = mask.GetROINameMap().find("roi7");
    REQUIRE(it2 != mask.GetROINameMap().end());
    REQUIRE(it2->second == 1);
    REQUIRE(mask.GetROIMap()[405 * x.GetXPixelsNum() + 300] == 1);
}

TEST_CASE("ROIMap_Empty") {
    DiffractionExperiment x(DetectorGeometry(1));
    size_t err = 0;
    for (const auto &i: x.ROI().GetROIMap()) {
        if (i != UINT16_MAX)
            err++;
    }
    REQUIRE(err == 0);
}
