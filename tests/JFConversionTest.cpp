// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <catch2/catch_all.hpp>

#include "../jungfrau/JFConversionFloatingPoint.h"
#include "../tests/CheckImageOutput.h"

void SetupPedestal( JFModulePedestal &pedestal_g0, JFModulePedestal &pedestal_g1, JFModulePedestal &pedestal_g2) {
    auto ptr_g0 = pedestal_g0.GetPedestal();
    auto ptr_g1 = pedestal_g1.GetPedestal();
    auto ptr_g2 = pedestal_g2.GetPedestal();

    for (int i = 0; i < RAW_MODULE_SIZE; i++) {
        ptr_g0[i] = 3000;
        ptr_g1[i] = 15000;
        ptr_g2[i] = 14000;
    }

}

TEST_CASE("JFConversionFloatingPoint_G0","[JFConversion]") {
    JFConversionFloatingPoint conv;

    JFModulePedestal pedestal_g0;
    JFModulePedestal pedestal_g1;
    JFModulePedestal pedestal_g2;

    SetupPedestal(pedestal_g0, pedestal_g1, pedestal_g2);

    JFModuleGainCalibration gain;

    conv.Setup(gain, pedestal_g0, pedestal_g1, pedestal_g2, 12.4);

    std::vector<uint16_t> input(RAW_MODULE_SIZE);
    std::vector<double> output_fp(RAW_MODULE_SIZE);
    std::vector<int16_t> output_16bit(RAW_MODULE_SIZE);

    for (int i = 0; i < RAW_MODULE_SIZE; i++)
        input[i] = i % 16384;

    for (int i = 0; i < 128; i++)
        conv.ConvertModule(output_16bit.data(), input.data());
    conv.ConvertFP(output_fp.data(), input.data());

    auto err = Compare(output_16bit.data(), output_fp, RAW_MODULE_SIZE);
    auto max_err = MaxErrorOnConversion(output_16bit.data(), output_fp, RAW_MODULE_SIZE);

    std::cout << "Error on conversion " << err << " max error " << max_err << std::endl;
    REQUIRE(err < 0.5);
    REQUIRE(max_err <= 1.0);
}

TEST_CASE("JFConversionFloatingPoint_G1","[JFConversion]") {
    JFConversionFloatingPoint conv;

    JFModulePedestal pedestal_g0;
    JFModulePedestal pedestal_g1;
    JFModulePedestal pedestal_g2;

    SetupPedestal(pedestal_g0, pedestal_g1, pedestal_g2);

    JFModuleGainCalibration gain;

    conv.Setup(gain, pedestal_g0, pedestal_g1, pedestal_g2, 12.4);

    std::vector<uint16_t> input(RAW_MODULE_SIZE);
    std::vector<double> output_fp(RAW_MODULE_SIZE);
    std::vector<int16_t> output_16bit(RAW_MODULE_SIZE);

    for (int i = 0; i < RAW_MODULE_SIZE; i++)
        input[i] = (i % 16384) | 0x4000;

    conv.ConvertModule(output_16bit.data(), input.data());
    conv.ConvertFP(output_fp.data(), input.data());

    auto err = Compare(output_16bit.data(), output_fp, RAW_MODULE_SIZE);
    auto max_err = MaxErrorOnConversion(output_16bit.data(), output_fp, RAW_MODULE_SIZE);

    std::cout << "Error on conversion " << err << " max error " << max_err << std::endl;
    REQUIRE(err < 0.5);
    REQUIRE(max_err <= 1.0);
}

TEST_CASE("JFConversionFloatingPoint_G2","[JFConversion]") {
    JFConversionFloatingPoint conv;

    JFModulePedestal pedestal_g0;
    JFModulePedestal pedestal_g1;
    JFModulePedestal pedestal_g2;

    SetupPedestal(pedestal_g0, pedestal_g1, pedestal_g2);

    JFModuleGainCalibration gain;

    conv.Setup(gain, pedestal_g0, pedestal_g1, pedestal_g2, 12.4);

    std::vector<uint16_t> input(RAW_MODULE_SIZE);
    std::vector<double> output_fp(RAW_MODULE_SIZE);
    std::vector<int16_t> output_16bit(RAW_MODULE_SIZE);

    for (int i = 0; i < RAW_MODULE_SIZE; i++)
        input[i] = (i % 16384) | 0xC000;

    conv.ConvertModule(output_16bit.data(), input.data());
    conv.ConvertFP(output_fp.data(), input.data());

    auto err = Compare(output_16bit.data(), output_fp, RAW_MODULE_SIZE);
    auto max_err = MaxErrorOnConversion(output_16bit.data(), output_fp, RAW_MODULE_SIZE);

    std::cout << "Error on conversion " << err << " max error " << max_err << std::endl;
    REQUIRE(err < 0.5);
    REQUIRE(max_err <= 1.0);
}
