// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <thread>
#include <catch2/catch_all.hpp>
#include "../preview/PreviewCounter.h"

TEST_CASE("PreviewCount", "") {
    PreviewCounter counter(std::chrono::seconds(1));

    REQUIRE(counter.GeneratePreview()); // at first run it should generate preview
    REQUIRE(!counter.GeneratePreview()); // immediately after not
    std::this_thread::sleep_for(std::chrono::seconds(1));
    REQUIRE(counter.GeneratePreview());
    REQUIRE(!counter.GeneratePreview()); // immediately after not
    REQUIRE(!counter.GeneratePreview()); // immediately after not
    REQUIRE(!counter.GeneratePreview()); // immediately after not
    std::this_thread::sleep_for(std::chrono::seconds(1));
    REQUIRE(counter.GeneratePreview());
    REQUIRE(!counter.GeneratePreview()); // immediately after not
}

TEST_CASE("PreviewCount_Zero", "") {
    PreviewCounter counter(std::chrono::seconds(0));

    REQUIRE(counter.GeneratePreview()); // always generate preview
    REQUIRE(counter.GeneratePreview());
    REQUIRE(counter.GeneratePreview());
    std::this_thread::sleep_for(std::chrono::seconds(1));
    REQUIRE(counter.GeneratePreview());
}

TEST_CASE("PreviewCount_Off", "") {
    PreviewCounter counter({});

    REQUIRE(!counter.GeneratePreview()); // never generate preview
    REQUIRE(!counter.GeneratePreview());
    REQUIRE(!counter.GeneratePreview());
    std::this_thread::sleep_for(std::chrono::seconds(1));
    REQUIRE(!counter.GeneratePreview());
}