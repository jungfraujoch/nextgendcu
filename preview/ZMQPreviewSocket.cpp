// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "ZMQPreviewSocket.h"
#include "../common/Definitions.h"

ZMQPreviewSocket::ZMQPreviewSocket(const std::string &addr)
    : socket(ZMQSocketType::Pub),
      counter(std::chrono::seconds(1)) {
    socket.Conflate(true);
    socket.SendTimeout(std::chrono::milliseconds(100));
    socket.Bind(addr);
}

ZMQPreviewSocket &ZMQPreviewSocket::Period(const std::optional<std::chrono::microseconds> &period) {
    counter.Period(period);
    return *this;
}


void ZMQPreviewSocket::SendImage(const uint8_t *image_data, size_t image_size) {
    if (counter.GeneratePreview())
        socket.Send(image_data, image_size, false);
}

void ZMQPreviewSocket::StartDataCollection(const StartMessage &message) {
    std::vector<uint8_t> serialization_buffer(MESSAGE_SIZE_FOR_START_END);
    CBORStream2Serializer serializer(serialization_buffer.data(), serialization_buffer.size());
    serializer.SerializeSequenceStart(message);
    socket.Send(serialization_buffer.data(), serializer.GetBufferSize(), true);
}

void ZMQPreviewSocket::EndDataCollection(const EndMessage &message) {
    std::vector<uint8_t> serialization_buffer(MESSAGE_SIZE_FOR_START_END);
    CBORStream2Serializer serializer(serialization_buffer.data(), serialization_buffer.size());
    serializer.SerializeSequenceEnd(message);
    socket.Send(serialization_buffer.data(), serializer.GetBufferSize(), true);
}

std::string ZMQPreviewSocket::GetAddress() {
    return socket.GetEndpointName();
}

ZMQPreviewSettings ZMQPreviewSocket::GetSettings() {
    return {
        .period = counter.GetPeriod(),
        .address = socket.GetEndpointName()
    };
}

ZMQPreviewSocket &ZMQPreviewSocket::ImportSettings(const ZMQPreviewSettings &settings) {
    Period(settings.period);
    return *this;
}
