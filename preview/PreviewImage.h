// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_PREVIEWIMAGE_H
#define JUNGFRAUJOCH_PREVIEWIMAGE_H

#include <vector>
#include <mutex>

#include "../common/SpotToSave.h"
#include "../common/DiffractionExperiment.h"
#include "../common/PixelMask.h"
#include "PreviewCounter.h"

struct PreviewJPEGSettings {
    int64_t saturation_value = 10;
    int64_t jpeg_quality = 70;
    bool show_spots = true;
    bool show_roi = false;
    bool show_indexed = false;
    bool show_user_mask = false;
    std::optional<float> resolution_ring;
};

struct rgb {
    unsigned char r;
    unsigned char g;
    unsigned char b;
};

class PreviewImage {
    mutable std::mutex m;
    DiffractionExperiment experiment;

    bool initialized = false;
    std::vector<uint8_t> uncompressed_image;
    std::vector<uint32_t> user_mask;
    std::vector<SpotToSave> spots;
    size_t xpixel = 0;
    size_t ypixel = 0;
    size_t pixel_depth_bytes = 2;
    bool pixel_is_signed = false;
    float beam_x = 0;
    float beam_y = 0;

    void AddResolutionRing(std::vector<uint8_t> &rgb_image, float d) const;
    void AddBeamCenter(std::vector<uint8_t> &rgb_image) const;
    void AddSpots(std::vector<uint8_t> &rgb_image) const;
    void AddROI(std::vector<uint8_t> &rgb_image) const;
    void AddUserMask(std::vector<uint8_t> &rgb_image) const;
    PreviewCounter counter;

    void color_pixel(std::vector<unsigned char>& ret, int64_t xpixel, int64_t ypixel, const rgb &color) const;
    void spot(std::vector<unsigned char>& ret, int64_t xpixel, int64_t ypixel, bool indexed) const;
    void roi(std::vector<unsigned char>& ret, int64_t xpixel, int64_t ypixel, int64_t roi_number) const;
    void beam_center_mark(std::vector<unsigned char>& ret, int64_t xpixel, int64_t ypixel) const;

public:
    explicit PreviewImage(std::chrono::microseconds period = std::chrono::seconds(1));
    void Configure(const DiffractionExperiment& experiment, const PixelMask& mask);
    void Configure();
    void UpdateImage(const void *uncompressed_image, const std::vector<SpotToSave> &spots);
    [[nodiscard]] std::string GenerateJPEG(const PreviewJPEGSettings& settings) const;
    [[nodiscard]] std::string GenerateTIFF() const;
    [[nodiscard]] std::string GenerateTIFFDioptas() const;
};

#endif //JUNGFRAUJOCH_PREVIEWIMAGE_H
