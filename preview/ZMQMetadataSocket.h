// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef ZMQMETADATASOCKET_H
#define ZMQMETADATASOCKET_H

#include <mutex>
#include <vector>

#include "PreviewCounter.h"
#include "../frame_serialize/JFJochMessages.h"
#include "../common/ZMQWrappers.h"

struct ZMQMetadataSettings {
    std::optional<std::chrono::microseconds> period;
    std::string address;
};

class ZMQMetadataSocket {
    mutable std::mutex m;

    constexpr static uint32_t DefaultSendWatermark = 100;

    MetadataMessage metadata_message;
    ZMQSocket socket;
    std::vector<uint8_t> buffer;
    PreviewCounter counter;

    bool first_image;

    void SendAll();
public:
    explicit ZMQMetadataSocket(const std::string &addr);

    ZMQMetadataSocket& Period(const std::optional<std::chrono::microseconds> &period);
    ZMQMetadataSocket& ImportSettings(const ZMQMetadataSettings &settings);

    void StartDataCollection(const StartMessage &message);
    void AddDataMessage(const DataMessage &msg);
    void EndDataCollection(const EndMessage &message);

    ZMQMetadataSettings GetSettings();
    std::string GetAddress();
};

#endif //ZMQMETADATASOCKET_H
