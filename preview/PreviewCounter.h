// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_PREVIEWCOUNTER_H
#define JUNGFRAUJOCH_PREVIEWCOUNTER_H

#include <cstdint>
#include <mutex>
#include <chrono>
#include <optional>

class PreviewCounter {
    mutable std::mutex m;
    std::optional<std::chrono::microseconds> period;
    std::chrono::time_point<std::chrono::system_clock> last_preview;
public:
    PreviewCounter(const std::optional<std::chrono::microseconds> &period);
    PreviewCounter& Period(const std::optional<std::chrono::microseconds> &period);
    bool GeneratePreview();
    std::optional<std::chrono::microseconds> GetPeriod() const;
};

#endif //JUNGFRAUJOCH_PREVIEWCOUNTER_H
