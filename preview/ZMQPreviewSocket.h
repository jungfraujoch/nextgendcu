// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_ZMQPREVIEWSOCKET_H
#define JFJOCH_ZMQPREVIEWSOCKET_H

#include "PreviewCounter.h"
#include "../common/ZMQWrappers.h"
#include "../frame_serialize/CBORStream2Serializer.h"
#include "ZMQPreviewSettings.h"

class ZMQPreviewSocket {
    ZMQSocket socket;
    PreviewCounter counter;
public:
    explicit ZMQPreviewSocket(const std::string &addr);
    ZMQPreviewSocket& Period(const std::optional<std::chrono::microseconds> &period);
    void SendImage(const uint8_t *image_data, size_t image_size);
    void StartDataCollection(const StartMessage& message);
    void EndDataCollection(const EndMessage& message);
    std::string GetAddress();

    ZMQPreviewSocket& ImportSettings(const ZMQPreviewSettings& settings);
    ZMQPreviewSettings GetSettings();
};

#endif //JFJOCH_ZMQPREVIEWSOCKET_H
