// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <tiffio.h>
#include <tiffio.hxx>
#include <sstream>

#include "JFJochTIFF.h"
#include "../common/JFJochException.h"


void WriteTIFF(TIFF *tiff, void *buff, size_t cols, size_t lines, size_t elem_size, bool is_signed) {

    if (tiff == nullptr)
        throw JFJochException(JFJochExceptionCategory::TIFFGeneratorError, "TIFFStreamOpen error");

    TIFFSetField(tiff, TIFFTAG_IMAGEWIDTH, cols);  // set the width of the image
    TIFFSetField(tiff, TIFFTAG_IMAGELENGTH, lines);    // set the height of the image
    TIFFSetField(tiff, TIFFTAG_SAMPLESPERPIXEL, 1);   // set number of channels per pixel
    TIFFSetField(tiff, TIFFTAG_BITSPERSAMPLE, elem_size * 8);    // set the size of the channels
    TIFFSetField(tiff, TIFFTAG_COMPRESSION, COMPRESSION_NONE); // setc ompression to LZW
    TIFFSetField(tiff, TIFFTAG_ROWSPERSTRIP, lines);
    if (is_signed)
        TIFFSetField(tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_INT);
    else
        TIFFSetField(tiff, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT);

    if (TIFFWriteEncodedStrip(tiff, 0, buff, cols * lines * elem_size) < 0)
        throw JFJochException(JFJochExceptionCategory::TIFFGeneratorError, "TIFFWriteEncodedStrip error");
}

std::string WriteTIFFToString(void *buff, size_t cols, size_t lines, size_t elem_size, bool is_signed) {
    std::stringstream os;

    TIFF *tiff = TIFFStreamOpen("x", (std::ostream *) &os);
    WriteTIFF(tiff, buff, cols, lines, elem_size, is_signed);
    TIFFClose(tiff);

    return os.str();
}

void WriteTIFFToFile(const std::string &filename, void *buff, size_t cols, size_t lines, size_t elem_size,
                     bool is_signed) {
    TIFF *tiff = TIFFOpen(filename.c_str(), "w");

    WriteTIFF(tiff, buff, cols, lines, elem_size, is_signed);

    TIFFClose(tiff);
}

std::vector<uint32_t> ReadTIFFFromString32(const std::string &s, uint32_t &cols, uint32_t &lines) {
    if (s.empty())
        throw JFJochException(JFJochExceptionCategory::TIFFGeneratorError, "No TIFF file provided");

    uint32_t rows_per_string = 0;

    std::vector<uint32_t> ret;

    uint16_t elem_size;

    std::istringstream input_TIFF_stream(s);
    TIFF* tiff = TIFFStreamOpen("MemTIFF", &input_TIFF_stream);
    if (tiff == nullptr)
        throw JFJochException(JFJochExceptionCategory::TIFFGeneratorError,"Not a proper TIFF file");

    TIFFGetField(tiff, TIFFTAG_IMAGEWIDTH, &cols);  // get the width of the image
    TIFFGetField(tiff, TIFFTAG_IMAGELENGTH, &lines);    // get the height of the image
    TIFFGetField(tiff, TIFFTAG_BITSPERSAMPLE, &elem_size);    // get the size of the channels
    TIFFGetField(tiff, TIFFTAG_ROWSPERSTRIP, &rows_per_string);

    if (elem_size != 32)
        throw JFJochException(JFJochExceptionCategory::TIFFGeneratorError, "Only 32-bit format supported");

    ret.resize(cols * lines);

    if (cols * sizeof(uint32_t) != TIFFScanlineSize(tiff))
        throw JFJochException(JFJochExceptionCategory::TIFFGeneratorError, "TIFFScanlineSize mismatch");

    for (int i = 0; i < lines; i++) {
        if (TIFFReadScanline(tiff, ret.data() + i * cols, i, 0) < 0)
            throw JFJochException(JFJochExceptionCategory::TIFFGeneratorError, "TIFFReadScanline error");
    }

    TIFFClose(tiff);
    return ret;
}

std::vector<uint16_t> ReadTIFFFromString16(const std::string &s, uint32_t &cols, uint32_t &lines) {
    if (s.empty())
        throw JFJochException(JFJochExceptionCategory::TIFFGeneratorError, "No TIFF file provided");

    uint32_t rows_per_string = 0;

    std::vector<uint16_t> ret;

    uint16_t elem_size;

    std::istringstream input_TIFF_stream(s);
    TIFF* tiff = TIFFStreamOpen("MemTIFF", &input_TIFF_stream);
    if (tiff == nullptr)
        throw JFJochException(JFJochExceptionCategory::TIFFGeneratorError,
            "TIFF format error");

    TIFFGetField(tiff, TIFFTAG_IMAGEWIDTH, &cols);  // get the width of the image
    TIFFGetField(tiff, TIFFTAG_IMAGELENGTH, &lines);    // get the height of the image
    TIFFGetField(tiff, TIFFTAG_BITSPERSAMPLE, &elem_size);    // get the size of the channels
    TIFFGetField(tiff, TIFFTAG_ROWSPERSTRIP, &rows_per_string);

    if (elem_size != 16)
        throw JFJochException(JFJochExceptionCategory::TIFFGeneratorError, "Only 16-bit format supported");

    ret.resize(cols * lines);

    if (cols * sizeof(uint16_t) != TIFFScanlineSize(tiff))
        throw JFJochException(JFJochExceptionCategory::TIFFGeneratorError, "TIFFScanlineSize mismatch");

    for (int i = 0; i < lines; i++) {
        if (TIFFReadScanline(tiff, ret.data() + i * cols, i, 0) < 0)
            throw JFJochException(JFJochExceptionCategory::TIFFGeneratorError, "TIFFReadScanline error");
    }

    TIFFClose(tiff);
    return ret;
}

void SupressTIFFErrors() {
    TIFFSetErrorHandler(nullptr);
}
