include(ExternalProject)

# Taken from https://github.com/dacap/aseprite/blob/422d74a271e43c6f8846c09bed6a993b1d097ff1/cmake/FindJpegTurbo.cmake

ExternalProject_Add(libjpeg-turbo-project
        GIT_REPOSITORY https://github.com/libjpeg-turbo/libjpeg-turbo
        GIT_TAG 3.0.4
        PREFIX "${CMAKE_CURRENT_BINARY_DIR}/libjpeg-turbo"
        INSTALL_DIR "${CMAKE_CURRENT_BINARY_DIR}/libjpeg-turbo"
        BUILD_BYPRODUCTS "${CMAKE_CURRENT_BINARY_DIR}/libjpeg-turbo/lib/${CMAKE_STATIC_LIBRARY_PREFIX}jpeg${LIBJPEG_TURBO_STATIC_SUFFIX}${CMAKE_STATIC_LIBRARY_SUFFIX}"
        CMAKE_CACHE_ARGS
        -DENABLE_SHARED:BOOL=OFF
        -DENABLE_STATIC:BOOL=ON
        -DWITH_ARITH_DEC:BOOL=ON
        -DWITH_ARITH_ENC:BOOL=ON
        -DWITH_JPEG8:BOOL=OFF
        -DWITH_JPEG7:BOOL=OFF
        -DWITH_TURBOJPEG:BOOL=OFF
        -DCMAKE_BUILD_TYPE:STRING=${CMAKE_BUILD_TYPE}
        -DCMAKE_INSTALL_PREFIX:PATH=<INSTALL_DIR>
        -DCMAKE_INSTALL_LIBDIR:PATH=<INSTALL_DIR>/lib)

ExternalProject_Get_Property(libjpeg-turbo-project install_dir)
SET(LIBJPEG_TURBO_INCLUDE_DIRS "${install_dir}/include")
SET(LIBJPEG_TURBO_LIBRARY "${install_dir}/lib/${CMAKE_STATIC_LIBRARY_PREFIX}jpeg${LIBJPEG_TURBO_STATIC_SUFFIX}${CMAKE_STATIC_LIBRARY_SUFFIX}")

# Create the directory so changing INTERFACE_INCLUDE_DIRECTORIES doesn't fail
FILE(MAKE_DIRECTORY ${LIBJPEG_TURBO_INCLUDE_DIRS})

ADD_LIBRARY(libjpeg-turbo STATIC IMPORTED)
SET_TARGET_PROPERTIES(libjpeg-turbo PROPERTIES
        IMPORTED_LOCATION ${LIBJPEG_TURBO_LIBRARY}
        INTERFACE_INCLUDE_DIRECTORIES ${LIBJPEG_TURBO_INCLUDE_DIRS})
ADD_DEPENDENCIES(libjpeg-turbo libjpeg-turbo-project)

ADD_LIBRARY(JFJochPreview STATIC
        JFJochTIFF.cpp JFJochTIFF.h
        JFJochJPEG.cpp JFJochJPEG.h
        PreviewCounter.cpp PreviewCounter.h
        PreviewImage.cpp PreviewImage.h
        ZMQPreviewSocket.cpp
        ZMQPreviewSocket.h
        ZMQMetadataSocket.cpp
        ZMQMetadataSocket.h)

TARGET_LINK_LIBRARIES(JFJochPreview PUBLIC JFJochZMQ JFJochCommon CBORStream2FrameSerialize)
TARGET_LINK_LIBRARIES(JFJochPreview PUBLIC tiff tiffxx)
TARGET_LINK_LIBRARIES(JFJochPreview PUBLIC libjpeg-turbo)

