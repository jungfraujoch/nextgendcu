// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "PreviewCounter.h"
#include "../common/JFJochException.h"

PreviewCounter::PreviewCounter(const std::optional<std::chrono::microseconds> &in_period) : period(in_period), last_preview() {}

bool PreviewCounter::GeneratePreview() {
    std::unique_lock ul(m);

    if (!period.has_value())
        return false;

    if (period.value().count() == 0)
        return true;
    if (period.value().count() < 0)
        throw JFJochException(JFJochExceptionCategory::InputParameterBelowMin,
                              "Preview period must be zero or above");
    auto now = std::chrono::system_clock::now();

    if (now > last_preview + period.value()) {
        last_preview = now;
        return true;
    } else
        return false;
}

PreviewCounter &PreviewCounter::Period(const std::optional<std::chrono::microseconds> &in_period) {
    std::unique_lock ul(m);
    period = in_period;
    return *this;
}

std::optional<std::chrono::microseconds> PreviewCounter::GetPeriod() const {
    return period;
}
