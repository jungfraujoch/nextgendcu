// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_ZMQPREVIEWSETTINGS_H
#define JFJOCH_ZMQPREVIEWSETTINGS_H

#include <optional>
#include <chrono>

struct ZMQPreviewSettings {
    std::optional<std::chrono::microseconds> period;
    std::string address;
};


#endif //JFJOCH_ZMQPREVIEWSETTINGS_H
