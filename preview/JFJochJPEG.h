// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_WRITEJPEG_H
#define JUNGFRAUJOCH_WRITEJPEG_H

#include <vector>
#include <string>

std::string WriteJPEGToMem(const std::vector<unsigned char> &input, size_t width, size_t height, int quality = 70);

#endif //JUNGFRAUJOCH_WRITEJPEG_H
