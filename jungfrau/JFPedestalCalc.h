// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_JFPEDESTALCALC_H
#define JUNGFRAUJOCH_JFPEDESTALCALC_H

#include <cmath>
#include <mutex>
#include "../common/DiffractionExperiment.h"
#include "JFModulePedestal.h"

class JFPedestalCalc {
    mutable std::mutex m;

    template <unsigned int GAIN_BIT> void AnalyzeImage(const uint16_t *raw_image);
    std::vector<uint32_t> currPedestal;
    std::vector<float> currPedestal2;
    std::vector<uint32_t> wrongCount;
    const uint32_t min_images;
    uint8_t gain_level;
    uint32_t image_number = 0;
public:
    explicit JFPedestalCalc(const DiffractionExperiment& experiment);
    void AnalyzeImage(const uint16_t *raw_image);
    void Export(JFModulePedestal& calibration, size_t allowed_wrong_gains = 0) const;
    JFPedestalCalc &operator+=(const JFPedestalCalc &other);
};

#endif //JUNGFRAUJOCH_JFPEDESTALCALC_H
