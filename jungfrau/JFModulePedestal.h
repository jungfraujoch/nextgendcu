// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_JFMODULEPEDESTAL_H
#define JUNGFRAUJOCH_JFMODULEPEDESTAL_H

#include <vector>
#include <cstdint>
#include <ctime>
#include <cmath>
#include <memory>

#include "../common/Definitions.h"

class JFModulePedestal {
    std::vector<uint16_t> pedestal;
    std::vector<uint16_t> pedestal_rms;
    time_t collection_time;
    int64_t frames;
public:
    JFModulePedestal();
    explicit JFModulePedestal(uint16_t default_value);
    explicit JFModulePedestal(int default_value);

    [[nodiscard]] double Mean() const;

    template<class T> void LoadPedestal(const std::vector<T> &vector) {
        for (int i = 0; i < RAW_MODULE_SIZE; i++) {
            pedestal[i] = std::lround(vector[i]);
        }
    }

    [[nodiscard]] const uint16_t* GetPedestal() const;
    [[nodiscard]] const uint16_t* GetPedestalRMS() const;
    [[nodiscard]] uint16_t* GetPedestal();
    [[nodiscard]] uint16_t* GetPedestalRMS();
    [[nodiscard]] size_t CountMaskedPixels() const;

    void SetCollectionTime(time_t input);
    [[nodiscard]] time_t GetCollectionTime() const;
    void SetFrameCount(int64_t input);
    [[nodiscard]] int64_t GetFrameCount() const;
};


#endif //JUNGFRAUJOCH_JFMODULEPEDESTAL_H
