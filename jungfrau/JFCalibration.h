// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_JFCALIBRATION_H
#define JUNGFRAUJOCH_JFCALIBRATION_H

#include "JFModulePedestal.h"
#include "JFModuleGainCalibration.h"

#include "../common/DiffractionExperiment.h"
#include "../common/RawToConvertedGeometry.h"
#include "../common/JFJochException.h"

struct JFCalibrationModuleStatistics {
    int64_t module_number;
    int64_t storage_cell_number;
    uint64_t bad_pixels;

    float pedestal_g0_mean;
    float pedestal_g1_mean;
    float pedestal_g2_mean;

    float gain_g0_mean;
    float gain_g1_mean;
    float gain_g2_mean;
};

class JFCalibration {
    const size_t nmodules;
    const size_t nstorage_cells;

    std::vector<JFModulePedestal> pedestal;
    std::vector<JFModuleGainCalibration> gain_calibration;
public:
    explicit JFCalibration(size_t nmodules, size_t nstorage_cells = 1);
    explicit JFCalibration(const DiffractionExperiment& experiment);

    [[nodiscard]] size_t GetModulesNum() const;
    [[nodiscard]] size_t GetStorageCellNum() const;

    [[nodiscard]] JFModuleGainCalibration& GainCalibration(size_t module_num);
    [[nodiscard]] const JFModuleGainCalibration& GainCalibration(size_t module_num) const;

    [[nodiscard]] JFModulePedestal& Pedestal(size_t module_num, size_t gain_level, size_t storage_cell = 0);
    [[nodiscard]] const JFModulePedestal& Pedestal(size_t module_num, size_t gain_level, size_t storage_cell = 0) const;

    [[nodiscard]] int64_t CountBadPixels(size_t module_number, size_t storage_cell = 0) const;
    [[nodiscard]] JFCalibrationModuleStatistics GetModuleStatistics(size_t module_number, size_t storage_cell) const;
    [[nodiscard]] std::vector<JFCalibrationModuleStatistics> GetModuleStatistics(size_t storage_cell) const;
    [[nodiscard]] std::vector<JFCalibrationModuleStatistics> GetModuleStatistics() const;

    [[nodiscard]] std::vector<uint16_t> GetPedestal(size_t gain_level, size_t storage_cell = 0) const;
    [[nodiscard]] std::vector<uint16_t> GetPedestalRMS(size_t gain_level, size_t storage_cell = 0) const;
};


#endif //JUNGFRAUJOCH_JFCALIBRATION_H
