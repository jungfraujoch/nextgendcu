// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "JFModulePedestal.h"

JFModulePedestal::JFModulePedestal()
: JFModulePedestal(0) {}

JFModulePedestal::JFModulePedestal(uint16_t default_value)
: pedestal(RAW_MODULE_SIZE), pedestal_rms(RAW_MODULE_SIZE, 0) {
    for (auto & i : pedestal)
        i = default_value;

    collection_time = 0;
    frames = -1;
}

JFModulePedestal::JFModulePedestal(int default_value)
: JFModulePedestal(static_cast<uint16_t>(default_value)){}

double JFModulePedestal::Mean() const {
    uint64_t ret = 0;
    for (auto & i : pedestal)
        ret += i;
    return static_cast<double>(ret) / static_cast<double>(RAW_MODULE_SIZE);
}

size_t JFModulePedestal::CountMaskedPixels() const {
    size_t ret = 0;
    for (auto i : pedestal) {
        if (i == UINT16_MAX) ret++;
    }
    return ret;
}

void JFModulePedestal::SetCollectionTime(time_t input) {
    collection_time = input;
}

time_t JFModulePedestal::GetCollectionTime() const {
    return collection_time;
}

void JFModulePedestal::SetFrameCount(int64_t input) {
    frames = input;
}

int64_t JFModulePedestal::GetFrameCount() const {
    return frames;
}

const uint16_t *JFModulePedestal::GetPedestal() const {
    return pedestal.data();
}

const uint16_t *JFModulePedestal::GetPedestalRMS() const {
    return pedestal_rms.data();
}

uint16_t *JFModulePedestal::GetPedestal() {
    return pedestal.data();
}

uint16_t *JFModulePedestal::GetPedestalRMS() {
    return pedestal_rms.data();
}
