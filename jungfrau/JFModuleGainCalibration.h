// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_JFMODULEGAINCALIBRATION_H
#define JUNGFRAUJOCH_JFMODULEGAINCALIBRATION_H

#include <vector>
#include <cstdint>
#include <string>
#include <array>

#include "../common/Definitions.h"

class JFModuleGainCalibration {
    constexpr const static size_t offset_g0 = 0;
    constexpr const static size_t offset_g1 = RAW_MODULE_SIZE;
    constexpr const static size_t offset_g2 = 2 * RAW_MODULE_SIZE;
    constexpr const static size_t offset_hg0 = 3 * RAW_MODULE_SIZE;
    std::vector<double> gain;
    double GetMean(size_t offset) const;
    double GetStdDev(size_t offset) const;
public:
    constexpr const static float fixed_g1_gain_coeff = 1.0 / 1.65;
    JFModuleGainCalibration();
    explicit JFModuleGainCalibration(const std::string &filename);
    explicit JFModuleGainCalibration(const std::vector<double> &vec);
    [[nodiscard]] const std::vector<double> &GetGainCalibration() const;
    void ExportG0(DeviceOutput *output) const;
    void ExportG1(DeviceOutput *output) const;
    void ExportFixedG1(DeviceOutput *output) const;
    void ExportG2(DeviceOutput *output) const;
    void ExportHG0(DeviceOutput *output) const;

    double GetG0Mean() const;
    double GetG1Mean() const;
    double GetG2Mean() const;
    double GetHG0Mean() const;

    double GetG0StdDev() const;
    double GetG1StdDev() const;
    double GetG2StdDev() const;
    double GetHG0StdDev() const;
};

// Only to use in automated tests run from the default directory
inline JFModuleGainCalibration GainCalibrationFromTestFile() {
    return JFModuleGainCalibration("../../tests/test_data/gainMaps_M049.bin");
}

#endif //JUNGFRAUJOCH_JFMODULEGAINCALIBRATION_H
