// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_JFCONVERSIONFLOATINGPOINT_H
#define JUNGFRAUJOCH_JFCONVERSIONFLOATINGPOINT_H

#include "JFCalibration.h"

class JFConversionFloatingPoint {

    std::vector<uint16_t> pedestal_g0;
    std::vector<uint16_t> pedestal_g1;
    std::vector<uint16_t> pedestal_g2;
    std::vector<double> gain_g0;
    std::vector<double> gain_g1;
    std::vector<double> gain_g2;

    double err_pixel = INT16_MIN;
    double overload_pixel = INT16_MAX;
    double overload_value;
    bool fixed_g1 = false;
    int64_t summation = 1;
    bool pixel_signed = true;
    std::optional<int64_t> threshold;
public:
    JFConversionFloatingPoint();
    JFConversionFloatingPoint(const DiffractionExperiment& experiment);
    void Setup(const JFModuleGainCalibration &gain_calibration,
               const JFModulePedestal &pedestal_g0,
               const JFModulePedestal &pedestal_g1,
               const JFModulePedestal &pedestal_g2,
               double energy, bool using_gain_hg0);
    void ConvertFP(double *dest, const uint16_t *source);
};


#endif //JUNGFRAUJOCH_JFCONVERSIONFLOATINGPOINT_H
