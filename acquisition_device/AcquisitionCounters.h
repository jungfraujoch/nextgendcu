// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_ACQUISITIONCOUNTERS_H
#define JUNGFRAUJOCH_ACQUISITIONCOUNTERS_H

#include <condition_variable>
#include <shared_mutex>
#include <vector>

#include "../common/DiffractionExperiment.h"
#include "../common/Definitions.h"
#include "Completion.h"

// AcquisitionCounters are used for information that needs to be accessed during data collection,
// so uses mutex to ensure consistency

class AcquisitionCounters {
    uint16_t expected_packets_per_module;
    constexpr static const uint64_t max_modules = 32;

    mutable std::shared_mutex m;
    mutable std::condition_variable_any data_updated;

    std::vector<uint64_t> handle_for_frame;
    std::vector<uint64_t> handle_for_pedestal;
    std::vector<uint16_t> packets_collected;

    std::vector<Completion> saved_completions;

    uint64_t total_packets;
    std::vector<uint64_t> packets_per_module;

    uint64_t slowest_frame_number;
    uint64_t fastest_frame_number;
    std::vector<uint64_t> curr_frame_number;
    bool acquisition_finished;
    uint64_t expected_frames;
    uint64_t nmodules = max_modules;
    uint64_t bytes_per_packet;

public:
    static constexpr const uint64_t HandleNotFound = UINT64_MAX;
    AcquisitionCounters();

    void Reset(const DiffractionExperiment &experiment, uint16_t data_stream);
    void UpdateCounters(const Completion *c);
    void SetAcquisitionFinished();

    uint64_t GetBufferHandleAndClear(size_t frame, uint16_t module_number);

    uint64_t GetCurrFrameNumber(uint16_t module_number) const;
    uint64_t GetSlowestFrameNumber() const;
    uint64_t GetFastestFrameNumber() const;

    void WaitForFrame(size_t curr_frame, uint16_t module_number = UINT16_MAX) const;
    int64_t CalculateDelay(size_t curr_frame, uint16_t module_number = UINT16_MAX) const; // mutex acquired indirectly
    uint64_t GetBufferHandle(size_t frame, uint16_t module_number) const;
    uint64_t GetPedestalBufferHandle(size_t storage_cell, uint16_t module_number) const;
    bool IsFullModuleCollected(size_t frame, uint16_t module_number) const;
    bool IsAnyPacketCollected(size_t frame, uint16_t module_number) const;
    bool IsAcquisitionFinished() const;

    uint64_t GetTotalPackets() const;
    uint64_t GetTotalPackets(uint16_t module_number) const;
    uint64_t GetBytesReceived() const;

    uint64_t GetTotalExpectedPackets() const;
    uint64_t GetTotalExpectedPacketsPerModule() const;
    uint64_t GetExpectedPacketsPerImage() const;

    uint64_t GetModuleNumber() const;
};


#endif //JUNGFRAUJOCH_ACQUISITIONCOUNTERS_H
