// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_ACQUISITIONDEVICE_H
#define JUNGFRAUJOCH_ACQUISITIONDEVICE_H

#include <map>
#include <mutex>
#include <future>
#include <unistd.h>

#include "../common/Definitions.h"
#include "../common/DiffractionExperiment.h"
#include "../common/Logger.h"
#include "../common/PixelMask.h"

#include "../common/ThreadSafeFIFO.h"
#include "../jungfrau/JFCalibration.h"

#include "AcquisitionCounters.h"
#include "Completion.h"
#include "../fpga/pcie_driver/jfjoch_fpga.h"
#include "../common/NetworkAddressConvert.h"

struct AcquisitionDeviceStatistics {
    uint64_t good_packets;
    uint64_t packets_expected;
    float efficiency;
    uint64_t bytes_received;
    time_t start_timestamp;
    time_t end_timestamp;
    uint64_t nmodules;
    std::vector<uint64_t> packets_received_per_module;
};

class AcquisitionDevice {
    std::chrono::time_point<std::chrono::system_clock> start_time;
    std::chrono::time_point<std::chrono::system_clock> end_time;

    int64_t expected_frames;

    virtual void Start(const DiffractionExperiment& experiment, uint32_t flags) = 0;
    virtual void Finalize() {}; // do clean-up after action is done
    virtual void StartSendingWorkRequests() {};
    void SendWorkRequest(uint32_t handle);
protected:
    AcquisitionCounters counters;

    ThreadSafeFIFO<Completion> work_completion_queue;
    ThreadSafeFIFO<WorkRequest> work_request_queue;

    std::vector<DeviceOutput *> buffer_device;
    Logger *logger;

    uint16_t data_stream;
    uint32_t max_modules = 1;
    uint64_t mac_addr;
    uint32_t ipv4_addr;
    explicit AcquisitionDevice(uint16_t data_stream);

    void UnmapBuffers();
    void MapBuffersStandard(size_t c2h_buffer_count, int16_t numa_node);
    const DeviceOutput *GetDeviceOutput(size_t handle) const;
    DeviceOutput *GetDeviceOutput(size_t handle);
    virtual void HW_RunInternalGenerator(const FrameGeneratorConfig& config) = 0;
    virtual void HW_SetSpotFinderParameters(const SpotFinderParameters &params) = 0;
public:
    AcquisitionDevice(const AcquisitionDevice &other) = delete;
    AcquisitionDevice &operator=(const AcquisitionDevice &other) = delete;

    static constexpr const uint64_t HandleNotValid = UINT64_MAX;

    virtual ~AcquisitionDevice() { UnmapBuffers(); };

    void StartAction(const DiffractionExperiment &experiment, uint32_t optional_flags = 0);
    void PrepareAction(const DiffractionExperiment &experiment);

    void WaitForActionComplete();
    virtual void Cancel() = 0;

    void EnableLogging(Logger *logger);

    // Post measurement statistics - only guaranteed valid after WaitForActionComplete ends
    uint64_t GetBytesReceived() const;

    virtual DataCollectionStatus GetDataCollectionStatus() const;
    virtual DeviceStatus GetDeviceStatus() const;
    AcquisitionDeviceStatistics GetStatistics() const;
    const DeviceOutput *GetDeviceOutput(size_t frame_number, uint16_t module_number) const;
    void FrameBufferRelease(size_t frame_number, uint16_t module_number);

    // Calibration
    virtual void InitializeCalibration(const DiffractionExperiment &experiment, const JFCalibration &calib);
    virtual void InitializeIntegrationMap(const DiffractionExperiment &experiment, const std::vector<uint16_t> &v,
                                          const std::vector<float> &weights);
    virtual void InitializeIntegrationMap(const uint16_t *map, const float *weights, size_t module_number);
    virtual void InitializeSpotFinderResolutionMap(const float *data, size_t module_number);
    virtual void InitializeROIMap(const uint16_t *map, size_t module_number);
    void InitializeEmptyPixelMask(const DiffractionExperiment &experiment); // Empty Mask
    void InitializePixelMask(const DiffractionExperiment &experiment, const PixelMask &mask);
    virtual void InitializePixelMask(const uint32_t *module_mask, size_t module_number);
    void InitializeROIMap(const DiffractionExperiment &experiment);

    const AcquisitionCounters& Counters() const;

    void SetSpotFinderParameters(const SpotFindingSettings &settings);
    virtual std::string GetIPv4Address() const;
    virtual void SetIPv4Address(uint32_t ipv4_addr_network_order);
    virtual std::string GetMACAddress() const;
    virtual uint16_t GetUDPPort() const;
    virtual int32_t GetNUMANode() const;
    virtual void SetInternalGeneratorFrame(const uint16_t *input, size_t module_number) {}

    AcquisitionDeviceNetConfig GetNetConfig() const;

    virtual uint32_t GetExpectedDescriptorsPerModule() const = 0;
    void RunInternalGenerator(const DiffractionExperiment& experiment);
};


#endif //JUNGFRAUJOCH_ACQUISITIONDEVICE_H
