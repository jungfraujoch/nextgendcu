// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_HLSSIMULATEDDEVICE_H
#define JUNGFRAUJOCH_HLSSIMULATEDDEVICE_H

#include <thread>

#include "../common/DiffractionExperiment.h"
#include "../fpga/hls_simulation/HLSDevice.h"

#include "FPGAAcquisitionDevice.h"

class HLSSimulatedDevice : public FPGAAcquisitionDevice {
    std::unique_ptr<HLSDevice> device;

    void HW_ReadActionRegister(DataCollectionConfig *job) override;
    void HW_WriteActionRegister(const DataCollectionConfig *job) override;

    void FPGA_StartAction(const DiffractionExperiment &experiment) override;
    void FPGA_EndAction() override;
    bool HW_IsIdle() const override;
    bool HW_ReadMailbox(uint32_t *values) override;
    bool HW_SendWorkRequest(uint32_t handle) override;
    void HW_LoadCalibration(const LoadCalibrationConfig &config) override;
    void HW_SetSpotFinderParameters(const SpotFinderParameters &params) override;
    void HW_RunInternalGenerator(const FrameGeneratorConfig &config) override;
public:
    HLSSimulatedDevice(uint16_t data_stream, size_t in_frame_buffer_size_modules, int16_t numa_node = -1);
    ~HLSSimulatedDevice() override = default;
    void CreateJFPacket(const DiffractionExperiment& experiment, uint64_t frame_number, uint32_t eth_packet,
                        uint32_t module_number, const uint16_t *data, int8_t adjust_axis = 0, uint8_t user = 0);
    void CreateJFPackets(const DiffractionExperiment& experiment, uint64_t frame_number_0, uint64_t frames,
                         uint32_t module_number, const uint16_t *data);
    void CreateEIGERPacket(const DiffractionExperiment& experiment, uint64_t frame_number, uint32_t eth_packet,
                            uint32_t module_number, uint32_t col, uint32_t row,
                            const uint16_t *data);
    void CreateXfelBunchIDPacket(double bunchid, uint32_t event_code);
    void CreateFinalPacket(const DiffractionExperiment& experiment);
    DataCollectionStatus GetDataCollectionStatus() const override;
    void Cancel() override;
    DeviceStatus GetDeviceStatus() const override;
};


#endif //JUNGFRAUJOCH_HLSSIMULATEDDEVICE_H
