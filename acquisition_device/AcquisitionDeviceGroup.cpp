// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "AcquisitionDeviceGroup.h"
#include "PCIExpressDevice.h"
#include "HLSSimulatedDevice.h"

AcquisitionDevice &AcquisitionDeviceGroup::operator[](int idx) {
    if ((idx < 0) || (idx >= aq_devices.size()))
        throw JFJochException(JFJochExceptionCategory::ArrayOutOfBounds, "Device out of bounds");
    if (aq_devices[idx] == nullptr)
        throw JFJochException(JFJochExceptionCategory::ArrayOutOfBounds, "Device not initialized");
    return *aq_devices[idx];
}

size_t AcquisitionDeviceGroup::size() {
    return aq_devices.size();
}

void AcquisitionDeviceGroup::Add(std::unique_ptr<AcquisitionDevice> &&device) {
    aq_devices.emplace_back(std::move(device));
}

void AcquisitionDeviceGroup::AddPCIeDevice(const std::string &device_name, std::optional<uint32_t> ipv4_addr) {
    auto tmp = std::make_unique<PCIExpressDevice>(aq_devices.size(), device_name);
    if (ipv4_addr.has_value())
        tmp->SetIPv4Address(ipv4_addr.value());
    aq_devices.emplace_back(std::move(tmp));
}

void AcquisitionDeviceGroup::AddHLSDevice(int64_t buffer_size_modules) {
    aq_devices.emplace_back(std::make_unique<HLSSimulatedDevice>(aq_devices.size(), buffer_size_modules));
}

std::vector<AcquisitionDeviceNetConfig> AcquisitionDeviceGroup::GetNetworkConfig() {
    std::vector<AcquisitionDeviceNetConfig> ret;
    for (const auto &i: aq_devices)
        ret.push_back(i->GetNetConfig());
    return ret;
}

void AcquisitionDeviceGroup::EnableLogging(Logger *logger) {
    for (auto &i: aq_devices)
        i->EnableLogging(logger);
}

std::vector<DeviceStatus> AcquisitionDeviceGroup::GetDeviceStatus() const {
    std::vector<DeviceStatus> ret;
    for (auto &i: aq_devices)
        ret.emplace_back(i->GetDeviceStatus());
    return ret;
}
