// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_FPGAACQUISITIONDEVICE_H
#define JUNGFRAUJOCH_FPGAACQUISITIONDEVICE_H

#include "AcquisitionDevice.h"
#include "../fpga/pcie_driver/jfjoch_fpga.h"

class FPGAAcquisitionDevice : public AcquisitionDevice {
    uint16_t data_collection_id = 0;

    uint32_t expected_descriptors_per_module = 1;

    virtual void FPGA_StartAction(const DiffractionExperiment &experiment) = 0;
    virtual void FPGA_EndAction() = 0;

    virtual void HW_WriteActionRegister(const DataCollectionConfig *job) = 0;
    virtual void HW_ReadActionRegister(DataCollectionConfig *job) = 0;
    virtual bool HW_IsIdle() const = 0;
    void FillActionRegister(const DiffractionExperiment& x, DataCollectionConfig& job);

    void Finalize() final;

    std::future<void> read_work_completion_future;
    void ReadWorkCompletionThread();

    std::future<void> send_work_request_future;
    volatile bool stop_work_requests = false;
    void SendWorkRequestThread();

    virtual void HW_LoadCalibration(const LoadCalibrationConfig &config) = 0;
    void LoadCalibration(uint32_t handle);
    virtual bool HW_ReadMailbox(uint32_t *values) = 0;
    virtual bool HW_SendWorkRequest(uint32_t handle) = 0;
    void StartSendingWorkRequests() override;
    void Start(const DiffractionExperiment &experiment, uint32_t flag) override;
protected:
    std::vector<uint16_t> internal_pkt_gen_frame;
    explicit FPGAAcquisitionDevice(uint16_t data_stream);
public:
    void InitializeCalibration(const DiffractionExperiment &experiment, const JFCalibration &calib) override;
    void InitializeIntegrationMap(const DiffractionExperiment &experiment, const std::vector<uint16_t> &v,
                                  const std::vector<float> &weights) override;
    void InitializeIntegrationMap(const uint16_t *map, const float *weights, size_t module_number) override;
    void InitializeSpotFinderResolutionMap(const float *data, size_t module_number) override;
    void InitializeROIMap(const uint16_t *map, size_t module_number) override;
    void InitializePixelMask(const uint32_t *module_mask, size_t module_number) override;
    void SetInternalGeneratorFrame(const uint16_t *input, size_t module_number) override;
    uint32_t GetExpectedDescriptorsPerModule() const override;
};

#endif //JUNGFRAUJOCH_FPGAACQUISITIONDEVICE_H
