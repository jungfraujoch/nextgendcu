ADD_LIBRARY(JFJochAcquisitionDevice STATIC
        AcquisitionDevice.cpp AcquisitionDevice.h
        AcquisitionCounters.cpp AcquisitionCounters.h
        HLSSimulatedDevice.cpp HLSSimulatedDevice.h
        Completion.cpp Completion.h ../fpga/pcie_driver/jfjoch_fpga.h
        PCIExpressDevice.cpp PCIExpressDevice.h
        FPGAAcquisitionDevice.cpp FPGAAcquisitionDevice.h
        AcquisitionDeviceGroup.cpp
        AcquisitionDeviceGroup.h)

TARGET_LINK_LIBRARIES(JFJochAcquisitionDevice JFJochDevice JFJochCommon JFJochHLSSimulation JFCalibration)