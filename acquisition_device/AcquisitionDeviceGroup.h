// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_ACQUISITIONDEVICEGROUP_H
#define JUNGFRAUJOCH_ACQUISITIONDEVICEGROUP_H

#include <vector>
#include "AcquisitionDevice.h"
#include "../common/JFJochException.h"

class AcquisitionDeviceGroup {
    std::vector<std::unique_ptr<AcquisitionDevice>> aq_devices;
public:
    AcquisitionDevice& operator[](int idx);
    size_t size();
    void Add(std::unique_ptr<AcquisitionDevice> &&device);
    void AddPCIeDevice(const std::string &device_name, std::optional<uint32_t> ipv4_addr = {});
    void AddHLSDevice(int64_t buffer_size_modules);
    std::vector<AcquisitionDeviceNetConfig> GetNetworkConfig();
    std::vector<DeviceStatus> GetDeviceStatus() const;
    void EnableLogging(Logger *logger);
};

#endif //JUNGFRAUJOCH_ACQUISITIONDEVICEGROUP_H
