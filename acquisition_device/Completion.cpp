// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "Completion.h"
#include "../common/Definitions.h"

Completion parse_hw_completion(uint32_t tmp) {
    Completion c{};

    c.handle        = tmp & UINT16_MAX;
    c.data_collection_id = (tmp>>16) & UINT16_MAX;

    if (c.handle == HANDLE_START)
        c.type = Completion::Type::Start;
    else if (c.handle == HANDLE_END)
        c.type = Completion::Type::End;
    else
        c.type = Completion::Type::Image;

    return c;
}