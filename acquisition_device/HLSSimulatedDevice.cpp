// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "HLSSimulatedDevice.h"

HLSSimulatedDevice::HLSSimulatedDevice(uint16_t data_stream, size_t in_frame_buffer_size_modules, int16_t numa_node)
        : FPGAAcquisitionDevice(data_stream) {
    mac_addr = 0xCCAA11223344;
    ipv4_addr = 0x0132010A;

    max_modules = MAX_MODULES_FPGA;

    if (data_stream != 0)
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "HLS simulation can only work with 1 data_stream, due to use of static variables");
    MapBuffersStandard(in_frame_buffer_size_modules, numa_node);


    device = std::make_unique<HLSDevice>(buffer_device);
}

void HLSSimulatedDevice::CreateFinalPacket(const DiffractionExperiment& experiment) {
    device->CreateFinalPacket();
}

void HLSSimulatedDevice::CreateJFPacket(const DiffractionExperiment& experiment, uint64_t frame_number, uint32_t eth_packet,
                                        uint32_t module_number, const uint16_t *data, int8_t adjust_axis, uint8_t user) {
    device->CreateJFPacket(frame_number, eth_packet, module_number, data, adjust_axis, user);
}

void HLSSimulatedDevice::CreateJFPackets(const DiffractionExperiment& experiment, uint64_t frame_number_0, uint64_t frames,
                                         uint32_t module_number, const uint16_t *data) {
    for (uint64_t i = 0; i < frames; i++) {
        for (int j = 0; j < 128; j++)
            CreateJFPacket(experiment, frame_number_0 + i, j, module_number, data + (i * 128 + j) * 4096, 0, 0);
    }
}

void HLSSimulatedDevice::CreateEIGERPacket(const DiffractionExperiment &experiment, uint64_t frame_number,
                                           uint32_t eth_packet, uint32_t module_number, uint32_t col, uint32_t row,
                                           const uint16_t *data) {
    device->CreateEIGERPacket(frame_number, eth_packet, module_number, col, row, data);
}

void HLSSimulatedDevice::HW_ReadActionRegister(DataCollectionConfig *job) {
    device->HW_ReadActionRegister(job);
}

void HLSSimulatedDevice::HW_WriteActionRegister(const DataCollectionConfig *job) {
    device->HW_WriteActionRegister(job);
}

void HLSSimulatedDevice::FPGA_StartAction(const DiffractionExperiment &experiment) {
    device->FPGA_StartAction();
}

void HLSSimulatedDevice::HW_RunInternalGenerator(const FrameGeneratorConfig &config) {
    device->HW_RunInternalGenerator(config);
}

void HLSSimulatedDevice::FPGA_EndAction() {
    device->FPGA_EndAction();
}


bool HLSSimulatedDevice::HW_ReadMailbox(uint32_t *values) {
    return device->HW_ReadMailbox(values);
}

void HLSSimulatedDevice::Cancel() {
    device->Cancel();
}

bool HLSSimulatedDevice::HW_IsIdle() const {
    return device->HW_IsIdle();
}


bool HLSSimulatedDevice::HW_SendWorkRequest(uint32_t handle) {
    return device->HW_SendWorkRequest(handle);
}

DataCollectionStatus HLSSimulatedDevice::GetDataCollectionStatus() const {
    return device->GetDataCollectionStatus();
}

void HLSSimulatedDevice::HW_LoadCalibration(const LoadCalibrationConfig &config) {
    device->HW_LoadCalibration(config);
}

void HLSSimulatedDevice::HW_SetSpotFinderParameters(const SpotFinderParameters &params) {
    device->HW_SetSpotFinderParameters(params);
}

void HLSSimulatedDevice::CreateXfelBunchIDPacket(double pulse_id, uint32_t event_code) {
    device->CreateXfelBunchIDPacket(pulse_id, event_code);
}

DeviceStatus HLSSimulatedDevice::GetDeviceStatus() const {
    DeviceStatus status{};

    strncpy(status.serial_number, "HLS1", sizeof(status.device_number));
    strncpy(status.device_number, "HLS device", sizeof(status.device_number));
    status.fpga_default_mac_addr = mac_addr;
    status.eth_link_count = 1;
    status.eth_link_status = 1;
    status.packets_sls = device->GetSLSPackets();
    status.packets_udp = device->GetUDPPackets();
    status.hbm_0_temp_C = 0;
    status.hbm_1_temp_C = 0;
    status.fpga_temp_C = 0;
    return status;
}