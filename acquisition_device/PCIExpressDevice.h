// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_PCIEXPRESSDEVICE_H
#define JUNGFRAUJOCH_PCIEXPRESSDEVICE_H

#include "FPGAAcquisitionDevice.h"
#include "../fpga/host_library/JungfraujochDevice.h"

class PCIExpressDevice : public FPGAAcquisitionDevice {
    JungfraujochDevice dev;

    void HW_LoadCalibration(const LoadCalibrationConfig &config) override;
    bool HW_ReadMailbox(uint32_t *values) override;
    bool HW_SendWorkRequest(uint32_t handle) override;
    void FPGA_StartAction(const DiffractionExperiment &experiment) override;
    bool HW_IsIdle() const final;
    void HW_WriteActionRegister(const DataCollectionConfig *job) override;
    void HW_ReadActionRegister(DataCollectionConfig *job) override;
    void HW_SetSpotFinderParameters(const SpotFinderParameters &params) override;
    void FPGA_EndAction() override;
    uint32_t GetNumKernelBuffers() const;
    void HW_RunInternalGenerator(const FrameGeneratorConfig &config) override;
public:
    explicit PCIExpressDevice(uint16_t data_stream);
    PCIExpressDevice(uint16_t data_stream, uint16_t pci_slot);
    PCIExpressDevice(uint16_t data_stream, const std::string &device_name);

    void Cancel() override;
    int32_t GetNUMANode() const override;

    void SetIPv4Address(uint32_t ipv4_addr_network_order) override;

    std::string GetMACAddress() const override;
    std::string GetIPv4Address() const override;
    DeviceStatus GetDeviceStatus() const override;
    DataCollectionStatus GetDataCollectionStatus() const override;
};

#endif //JUNGFRAUJOCH_PCIEXPRESSDEVICE_H
