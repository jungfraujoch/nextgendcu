// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_COMPLETION_H
#define JUNGFRAUJOCH_COMPLETION_H

#include <cstdint>

struct WorkRequest {
    uint32_t handle;
};

struct Completion {
    enum class Type {Start, End, Image} type;

    uint64_t frame_number;
    uint32_t handle;
    uint16_t packet_count;
    uint16_t data_collection_id;
    uint16_t module_number;
};

Completion parse_hw_completion(uint32_t hw_input);

#endif //JUNGFRAUJOCH_COMPLETION_H
