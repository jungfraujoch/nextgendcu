// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_ROIBOX_H
#define JUNGFRAUJOCH_ROIBOX_H

#include "ROIElement.h"

class ROIBox : public ROIElement {
    int64_t x_min;
    int64_t x_max;
    int64_t y_min;
    int64_t y_max;
public:
    ROIBox(const std::string &name, int64_t x_min, int64_t x_max, int64_t y_min, int64_t y_max);
    [[nodiscard]] int64_t GetXMin() const;
    [[nodiscard]] int64_t GetXMax() const;
    [[nodiscard]] int64_t GetYMin() const;
    [[nodiscard]] int64_t GetYMax() const;
    void MarkROI(std::vector<uint16_t> &v, uint16_t value_to_mark, int64_t xpixel, int64_t ypixel) const override;
};

#endif //JUNGFRAUJOCH_ROIBOX_H
