// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_DETECTORSETTINGS_H
#define JFJOCH_DETECTORSETTINGS_H

#include <cstdint>
#include <chrono>
#include <optional>

#include "../common/Definitions.h"

enum class DetectorTiming {Auto, Trigger, Burst, Gated};

class DetectorSettings {
    std::chrono::microseconds frame_time_pedestalG1G2 = std::chrono::microseconds(FRAME_TIME_PEDE_G1G2_IN_US);

    bool internal_fpga_packet_generator = false;
    int64_t internal_fpga_packet_generator_images = 1;

    int64_t storage_cells = 1;
    int64_t storage_cell_start = 15;
    std::chrono::nanoseconds storage_cell_delay = std::chrono::nanoseconds{2100};
    std::chrono::nanoseconds detector_delay = std::chrono::nanoseconds{0};
    int64_t pedestal_g0_frames = 0;
    int64_t pedestal_g1_frames = 0;
    int64_t pedestal_g2_frames = 0;
    bool use_gain_hg0 = false;
    bool fix_gain_g1 = false;
    std::chrono::microseconds frame_time = std::chrono::microseconds{500};
    std::optional<std::chrono::microseconds> count_time;
    int64_t pedestal_min_image_count = PEDESTAL_MIN_IMAGE_COUNT;
    
    std::optional<float> eiger_threshold_keV;
    std::optional<int64_t> eiger_bitwidth;

    DetectorTiming timing = DetectorTiming::Trigger;
public:
    DetectorSettings& InternalGeneratorEnable(bool input);
    DetectorSettings& InternalGeneratorImages(int64_t input);
    DetectorSettings& StorageCells(int64_t input);
    DetectorSettings& StorageCellStart(int64_t input);
    DetectorSettings& StorageCellDelay(const std::chrono::nanoseconds &input);
    DetectorSettings& DetectorDelay(const std::chrono::nanoseconds &input);
    DetectorSettings& PedestalG0Frames(int64_t input);
    DetectorSettings& PedestalG1Frames(int64_t input);
    DetectorSettings& PedestalG2Frames(int64_t input);
    DetectorSettings& UseGainHG0(bool input);
    DetectorSettings& FixGainG1(bool input);
    DetectorSettings& FrameTime(const std::chrono::microseconds& input);
    DetectorSettings& FrameTime(const std::chrono::microseconds& input,
                                const std::chrono::microseconds &count_time);
    DetectorSettings& FrameTime(const std::chrono::microseconds& input,
                                const std::optional<std::chrono::microseconds> &count_time);
    DetectorSettings& PedestalMinImageCount(uint32_t input);
    DetectorSettings& EigerThreshold_keV(const std::optional<float> &input);
    DetectorSettings& Timing(DetectorTiming input);
    DetectorSettings& EigerBitDepth(const std::optional<int64_t> &input);

    [[nodiscard]] bool IsInternalGeneratorEnable() const;
    [[nodiscard]] int64_t GetInternalGeneratorImages() const;
    [[nodiscard]] int64_t GetStorageCells() const;
    [[nodiscard]] int64_t GetStorageCellStart() const;
    [[nodiscard]] std::chrono::nanoseconds GetStorageCellDelay() const;
    [[nodiscard]] std::chrono::nanoseconds GetDetectorDelay() const;
    [[nodiscard]] int64_t GetPedestalG0Frames() const;
    [[nodiscard]] int64_t GetPedestalG1Frames() const;
    [[nodiscard]] int64_t GetPedestalG2Frames() const;
    [[nodiscard]] bool IsUseGainHG0() const;
    [[nodiscard]] bool IsFixGainG1() const;
    [[nodiscard]] std::chrono::microseconds GetFrameTime() const;
    [[nodiscard]] std::optional<std::chrono::microseconds> GetCountTime() const;
    [[nodiscard]] std::chrono::microseconds GetFrameTimePedestalG1G2() const;
    [[nodiscard]] uint32_t GetPedestalMinImageCount() const;
    [[nodiscard]] std::optional<float> GetEigerThreshold_keV() const;
    [[nodiscard]] std::optional<int64_t> GetEigerBitDepth() const;
    [[nodiscard]] DetectorTiming GetTiming() const;
};


#endif //JFJOCH_DETECTORSETTINGS_H
