// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <filesystem>

#include "DetectorSetup.h"
#include "JFJochException.h"
#include "NetworkAddressConvert.h"

DetectorSetup::DetectorSetup(const DetectorGeometry &in_geometry)
        : DetectorSetup(in_geometry, DetectorType::JUNGFRAU, "Detector", std::vector<std::string>()) {}

DetectorSetup::DetectorSetup(const DetectorGeometry &in_geometry, DetectorType in_detector_type)
        : DetectorSetup(in_geometry, in_detector_type, "Detector", std::vector<std::string>()) {}

DetectorSetup::DetectorSetup(const DetectorGeometry &in_geometry,
                             DetectorType in_detector_type,
                             const std::string &in_description,
                             const std::vector<std::string> &in_det_modules_hostname) :
        geometry(in_geometry),
        description(in_description),
        det_modules_hostname(in_det_modules_hostname),
        detector_type(in_detector_type) {
    switch (detector_type) {
        case DetectorType::EIGER:
            high_voltage = 150;
            read_out_time = std::chrono::microseconds(3);
            if (!det_modules_hostname.empty() && (2 * geometry.GetModulesNum() != det_modules_hostname.size()))
                throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                                      "Mismatch between number of modules in detector geometry and hostname (For EIGER - one module = 2 hostnames)");
            break;
        case DetectorType::JUNGFRAU:
            high_voltage = 120;
            read_out_time = std::chrono::microseconds(20);
            if (!det_modules_hostname.empty() && (geometry.GetModulesNum() != det_modules_hostname.size()))
                throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                                      "Mismatch between number of modules in detector geometry and hostname");
            break;
        default:
            throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "Detector not supported");
    }
}

const DetectorGeometry &DetectorSetup::GetGeometry() const {
    return geometry;
}

const std::vector<std::string> &DetectorSetup::GetDetectorModuleHostname() const {
    return det_modules_hostname;
}

uint64_t DetectorSetup::GetModulesNum() const {
    return geometry.GetModulesNum();
}

std::string DetectorSetup::GetDescription() const {
    return description;
}

float DetectorSetup::GetPixelSize_mm() const {
    return pixel_size_um / 1000.0f;
}

std::string DetectorSetup::GetSensorMaterial() const {
    return sensor_material;
}

float DetectorSetup::GetSensorThickness_um() const {
    return sensor_thickness_um;
}

void DetectorSetup::LoadGain(const std::vector<std::string> &filenames) {
    if (filenames.size() != GetModulesNum())
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "Mismatch in number of gain calibration files");
    gain_file_names = filenames;
    for (const auto& i: filenames)
        gain_calibration.emplace_back(i);
}

DetectorSetup &DetectorSetup::UDPInterfaceCount(int64_t input) {
    if ((input != 1) && (input != 2))
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                     "Only 1 and 2 are supported as UDP interface count");
    udp_interface_count = input;
    return *this;
}

const std::vector<JFModuleGainCalibration> &DetectorSetup::GetGainCalibration() const {
    return gain_calibration;
}

int64_t DetectorSetup::GetUDPInterfaceCount() const {
    if (detector_type == DetectorType::EIGER)
        return 2;
    return udp_interface_count;
}

DetectorSetup &DetectorSetup::SensorMaterial(const std::string &input) {
    sensor_material = input;
    return *this;
}

DetectorSetup &DetectorSetup::SensorThickness_um(float input) {
    sensor_thickness_um = input;
    return *this;
}

DetectorSetup &DetectorSetup::PixelSize_um(float input) {
    pixel_size_um = input;
    return *this;
}

DetectorSetup &DetectorSetup::TxDelay(const std::vector<int64_t> &v) {
    if (!v.empty() && (v.size() != GetModulesNum()))
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "Mismatch between size of TX delay vector and modules number");
    for (const auto &i: v) {
        if ((i < 0) || (i > 31))
            throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "TX delay must be in range 0-31");
    }
    tx_delay = v;
    return *this;
}

const std::vector<int64_t> &DetectorSetup::GetTxDelay() const {
    return tx_delay;
}

const std::vector<std::string> &DetectorSetup::GetGainFileNames() const {
    return gain_file_names;
}

DetectorType DetectorSetup::GetDetectorType() const {
    return detector_type;
}

DetectorSetup &DetectorSetup::HighVoltage(int32_t input) {
    high_voltage = input;
    return *this;
}

int32_t DetectorSetup::GetHighVoltage() const {
    return high_voltage;
}

void DetectorSetup::SetTrimFiles(const std::vector<std::string> &filenames) {
    if ((filenames.size() == 1) && std::filesystem::is_directory(filenames[0])) {
        trim_file_directory = filenames[0];
        trim_file_names.clear();
    } else if (filenames.size() != 2 * GetModulesNum())
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "Mismatch in number of trim bit calibration files");
    trim_file_names = filenames;
    trim_file_directory = "";
}

const std::vector<std::string> &DetectorSetup::GetTrimFileNames() const {
    return trim_file_names;
}

std::string DetectorSetup::GetTrimFileDirectory() const {
    return trim_file_directory;
}

std::string DetectorSetup::GetSerialNumber() const {
    return serial_number;
}

DetectorSetup &DetectorSetup::SerialNumber(const std::string &input) {
    serial_number = input;
    return *this;
}

DetectorSetup &DetectorSetup::BaseIPv4Addr(const std::string &input) {
    ipv4_base_addr = IPv4AddressFromStr(input);
    return *this;
}

uint32_t DetectorSetup::GetSrcIPv4Addr(uint32_t half_module) const {
    if (half_module >= GetUDPInterfaceCount() * GetModulesNum())
        throw JFJochException(JFJochExceptionCategory::ArrayOutOfBounds, "Non existing module");
    return ipv4_base_addr + (half_module << 24);
}

std::string DetectorSetup::GetBaseIPv4Addr() const {
    return IPv4AddressToStr(ipv4_base_addr);
}

bool DetectorSetup::IsModuleSync() const {
    if (GetModulesNum() == 1)
        return false;
    else
        return module_sync;
}

DetectorSetup &DetectorSetup::ModuleSync(bool input) {
    module_sync = input;
    return *this;
}

DetectorSetup & DetectorSetup::ReadOutTime(std::chrono::microseconds input) {
    if (input.count() <= 0)
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
            "Read out time has to be more than zero");
    read_out_time = input;
    return *this;
}

std::chrono::microseconds DetectorSetup::GetReadOutTime() const {
    return read_out_time;
}

std::chrono::microseconds DetectorSetup::GetMinFrameTime() const {
    switch (GetDetectorType()) {
        case DetectorType::EIGER:
            return std::chrono::microseconds(MIN_FRAME_TIME_EIGER_IN_US);
        case DetectorType::JUNGFRAU:
            if (GetUDPInterfaceCount() == 1)
                return std::chrono::microseconds(MIN_FRAME_TIME_JUNGFRAU_HALF_SPEED_IN_US);
            return std::chrono::microseconds(MIN_FRAME_TIME_JUNGFRAU_FULL_SPEED_IN_US);
        default:
            throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                                  "Detector not supported");
    }
}

std::chrono::microseconds DetectorSetup::GetMinCountTime() const {
    return std::chrono::microseconds(MIN_COUNT_TIME_IN_US);
}
