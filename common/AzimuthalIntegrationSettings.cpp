// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <cmath>

#include "AzimuthalIntegrationSettings.h"
#include "JFJochException.h"

#define check_max(param, val, max) if ((val) > (max)) throw JFJochException(JFJochExceptionCategory::InputParameterAboveMax, param)
#define check_min(param, val, min) if ((val) < (min)) throw JFJochException(JFJochExceptionCategory::InputParameterBelowMin, param)
#define check_finite(param, val) if (!std::isfinite(val)) throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, param)

AzimuthalIntegrationSettings &AzimuthalIntegrationSettings::SolidAngleCorrection(bool input) {
    solid_angle_correction = input;
    return *this;
}

AzimuthalIntegrationSettings &AzimuthalIntegrationSettings::PolarizationFactor(const std::optional<float> &input) {
    if (input.has_value()) {
        check_finite("Polarization factor", input.value());
        check_min("Polarization factor", input.value(), -1.0);
        check_max("Polarization factor", input.value(), 1.0);
    }
    polarization_factor = input;
    return *this;
}

AzimuthalIntegrationSettings &AzimuthalIntegrationSettings::HighQ_recipA(float input) {
    check_finite("Low Q for azimuthal integration", input);
    check_min("Low Q for azimuthal integration", input, 0.001);
    check_max("Low Q for azimuthal integration", input, 10.0);
    high_q_recipA = input;
    return *this;
}

AzimuthalIntegrationSettings &AzimuthalIntegrationSettings::LowQ_recipA(float input) {
    check_finite("Low Q for azimuthal integration", input);
    check_min("Low Q for azimuthal integration", input, 0.001);
    check_max("Low Q for azimuthal integration", input, 10.0);
    low_q_recipA = input;
    return *this;
}

AzimuthalIntegrationSettings &AzimuthalIntegrationSettings::QSpacing_recipA(float input) {
    check_finite("Q spacing for azimuthal integration", input);
    check_min("Q spacing for azimuthal integration", input, 0.01);
    q_spacing = input;
    return *this;
}

bool AzimuthalIntegrationSettings::IsSolidAngleCorrection() const {
    return solid_angle_correction;
}

std::optional<float> AzimuthalIntegrationSettings::GetPolarizationFactor() const {
    return polarization_factor;
}

float AzimuthalIntegrationSettings::GetHighQ_recipA() const {
    return high_q_recipA;
}

float AzimuthalIntegrationSettings::GetLowQ_recipA() const {
    return low_q_recipA;
}

float AzimuthalIntegrationSettings::GetQSpacing_recipA() const {
    return q_spacing;
}
