// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_ZEROCOPYRETURNVALUE_H
#define JUNGFRAUJOCH_ZEROCOPYRETURNVALUE_H

#include "ThreadSafeFIFO.h"

class ImageBuffer;

class ZeroCopyReturnValue {
    void *ptr;
    size_t payload_size;
    int64_t image_number;
    uint32_t handle;
    ImageBuffer &buf_ctrl;
public:
    ZeroCopyReturnValue(void *in_ptr, ImageBuffer &in_ctrl, uint32_t in_handle);
    void *GetImage() const;
    void SetImageNumber(int64_t image_number);
    void SetImageSize(size_t payload_size);
    [[nodiscard]] size_t GetImageSize() const;
    [[nodiscard]] int64_t GetImageNumber() const;
    void release() const;
};

#endif //JUNGFRAUJOCH_ZEROCOPYRETURNVALUE_H
