// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <cmath>

#include "ROICircle.h"
#include "JFJochException.h"

ROICircle::ROICircle(const std::string &name, float in_x, float in_y, float in_r_pxl)
        : ROIElement(name), center_x(in_x), center_y(in_y), r_pxl(in_r_pxl) {
    if (r_pxl <= 0.0)
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "ROIRectangle::MarkROI mismatch in input array size");
}

float ROICircle::GetX() const {
    return center_x;
}

float ROICircle::GetY() const {
    return center_y;
}

float ROICircle::GetRadius_pxl() const {
    return r_pxl;
}

void ROICircle::MarkROI(std::vector<uint16_t> &v, uint16_t value_to_mark, int64_t xpixel, int64_t ypixel) const {
    if (v.size() != xpixel * ypixel)
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "ROICircle::MarkROI mismatch in input array size");
    float r_pxl_sq = r_pxl * r_pxl;

    for (int64_t y = 0; y < ypixel; y++) {
        for (int64_t x = 0; x < xpixel; x++) {
            float x_fl = static_cast<float>(x) - center_x;
            float y_fl = static_cast<float>(y) - center_y;
            float dist_from_center_sq = x_fl * x_fl + y_fl * y_fl;
            if (dist_from_center_sq <= r_pxl_sq)
                v[y * xpixel + x] = value_to_mark;
        }
    }
}