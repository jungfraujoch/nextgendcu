// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "DiffractionSpot.h"
#include "DiffractionGeometry.h"
#include "RawToConvertedGeometry.h"

DiffractionSpot::DiffractionSpot(uint32_t col, uint32_t line, int64_t in_photons) {
    if (in_photons < 0) in_photons = 0;
    x = col * static_cast<float>(in_photons);
    y = line * static_cast<float>(in_photons);
    pixel_count = 1;
    photons = in_photons;
    max_photons = in_photons;
}

DiffractionSpot& DiffractionSpot::operator+=(const DiffractionSpot &other) {
    this->x += other.x;
    this->y += other.y;
    this->photons += other.photons;
    this->max_photons = std::max(this->max_photons, other.max_photons);
    this->pixel_count += other.pixel_count;
    return *this;
}

int64_t DiffractionSpot::Count() const {
    return photons;
}

int64_t DiffractionSpot::MaxCount() const {
    return max_photons;
}

Coord DiffractionSpot::RawCoord() const {
    return {x / (float)photons, y / (float)photons, 0};
}

int64_t DiffractionSpot::PixelCount() const {
    return pixel_count;
}

Coord DiffractionSpot::LabCoord(const DiffractionGeometry &geom) const {
    return geom.LabCoord(x  / (float)photons + 0.5f, y / (float)photons + 0.5f);
}

Coord DiffractionSpot::ReciprocalCoord(const DiffractionGeometry &geom) const {
    return geom.DetectorToRecip(x  / (float)photons + 0.5f, y / (float)photons + 0.5f);
}

double DiffractionSpot::GetResolution(const DiffractionGeometry &geom) const {
    return geom.PxlToRes(x  / (float)photons + 0.5f, y / (float)photons + 0.5f);
}

DiffractionSpot::operator SpotToSave() const {
    return {
            .x = x  / static_cast<float>(photons),
            .y = y  / static_cast<float>(photons),
            .intensity = static_cast<float>(photons),
            .indexed = false
    };
}

void DiffractionSpot::AddPixel(uint32_t col, uint32_t line, int64_t photons) {
    this->x += col * (float) photons;
    this->y += line * (float) photons;
    this->photons += photons;
    this->max_photons = std::max(this->max_photons, photons);
    this->pixel_count += 1;
}

void DiffractionSpot::ConvertToImageCoordinates(const DiffractionExperiment &experiment, uint16_t module_number) {
    auto c_out = RawToConvertedCoordinate(experiment, module_number, RawCoord());
    this->x = c_out.x * (float) photons;
    this->y = c_out.y * (float) photons;
}
