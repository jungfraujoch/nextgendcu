// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_NUMAHWPOLICY_H
#define JUNGFRAUJOCH_NUMAHWPOLICY_H

#include <cstdint>
#include <vector>
#include <string>
#include <atomic>

struct NUMABinding {
    int32_t cpu_node;
    int32_t mem_node;
    int32_t gpu;
};

class NUMAHWPolicy {
    std::string name;
    std::vector<NUMABinding> bindings;
public:
    NUMAHWPolicy() = default;
    explicit NUMAHWPolicy(const std::string& policy);
    NUMABinding GetBinding(uint32_t thread);

    const std::string &GetName() const;

    void Bind(uint32_t thread);
    static void Bind(const NUMABinding &binding);
    static void RunOnNode(int32_t cpu_node);
    static void MemOnNode(int32_t mem_node);
    static void SelectGPU(int32_t gpu);
};

#endif //JUNGFRAUJOCH_NUMAHWPOLICY_H
