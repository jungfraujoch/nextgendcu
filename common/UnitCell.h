// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_UNITCELL_H
#define JUNGFRAUJOCH_UNITCELL_H

struct UnitCell {
    float a;
    float b;
    float c;
    float alpha;
    float beta;
    float gamma;
};

#endif //JUNGFRAUJOCH_UNITCELL_H
