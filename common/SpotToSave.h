// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_SPOTTOSAVE_H
#define JUNGFRAUJOCH_SPOTTOSAVE_H

struct SpotToSave {
    float x;
    float y;
    float intensity;
    bool indexed;
};

#endif //JUNGFRAUJOCH_SPOTTOSAVE_H
