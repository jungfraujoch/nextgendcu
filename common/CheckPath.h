// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_CHECKPATH_H
#define JFJOCH_CHECKPATH_H

#include <string>
#include "JFJochException.h"

inline void CheckPath(const std::string &s) {
    if (s.front() == '/')
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "Path cannot start with slash");
    if (s.substr(0,3) == "../")
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "Path cannot start with ../");
    if (s.find("/../") != std::string::npos)
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "Path cannot contain /../");
}

#endif //JFJOCH_CHECKPATH_H
