// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_PLOT_H
#define JUNGFRAUJOCH_PLOT_H

#include <vector>
#include <cstdint>
#include <string>

enum class PlotType {
    BkgEstimate, AzInt, SpotCount, IndexingRate, IndexingUnitCellLength, IndexingUnitCellAngle,
    ErrorPixels, ImageCollectionEfficiency, ReceiverDelay, ReceiverFreeSendBuf, StrongPixels,
    ROISum, ROIMaxCount, ROIPixels, PacketsReceived, MaxValue
};

struct PlotRequest {
    PlotType type;
    uint64_t binning;
};

struct MultiLinePlotStruct {
    std::string title;
    std::vector<float> x;
    std::vector<float> y;
};

typedef std::vector<MultiLinePlotStruct> MultiLinePlot;

#endif //JUNGFRAUJOCH_PLOT_H
