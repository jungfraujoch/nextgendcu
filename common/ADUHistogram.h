// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_ADUHISTOGRAM_H
#define JUNGFRAUJOCH_ADUHISTOGRAM_H

#include <mutex>
#include "Plot.h"
#include "../acquisition_device/AcquisitionDevice.h"

class ADUHistogram {
    mutable std::mutex m;
    std::vector<uint64_t> histogram;
public:
    explicit ADUHistogram();
    void Add(const std::vector<uint64_t>& input);
    void Add(const DeviceOutput &output);
    const std::vector<uint64_t>& GetHistogram() const;
    MultiLinePlot GetPlot() const;
    void Restart();
};

#endif //JUNGFRAUJOCH_ADUHISTOGRAM_H
