// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "ROIElement.h"

ROIElement::ROIElement(const std::string &in_name)
        : name(in_name) {}

std::string ROIElement::GetName() const {
    return name;
}
