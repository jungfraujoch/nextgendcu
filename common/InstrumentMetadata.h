// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_INSTRUMENTMETADATA_H
#define JFJOCH_INSTRUMENTMETADATA_H

#include <string>
#include <optional>
#include "Coord.h"

class InstrumentMetadata {
    std::string source_name;
    std::string source_type;
    std::string instrument_name;
    bool pulsed_source;
    bool electron_source;
public:
    InstrumentMetadata();

    InstrumentMetadata& SourceName(const std::string &input);
    InstrumentMetadata& SourceType(const std::string &input);
    InstrumentMetadata& InstrumentName(const std::string &input);
    InstrumentMetadata& PulsedSource(bool input);
    InstrumentMetadata& ElectronSource(bool input);

    [[nodiscard]] std::string GetSourceName() const;
    [[nodiscard]] std::string GetSourceType() const;
    [[nodiscard]] std::string GetInstrumentName() const;
    [[nodiscard]] bool IsPulsedSource() const;
    [[nodiscard]] bool IsElectronSource() const;
};

#endif //JFJOCH_INSTRUMENTMETADATA_H
