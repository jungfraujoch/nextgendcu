// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "DetectorSettings.h"
#include "JFJochException.h"
#include "Definitions.h"

#define check_max(param, val, max) if ((val) > (max)) throw JFJochException(JFJochExceptionCategory::InputParameterAboveMax, param)
#define check_min(param, val, min) if ((val) < (min)) throw JFJochException(JFJochExceptionCategory::InputParameterBelowMin, param)

DetectorSettings &DetectorSettings::InternalGeneratorEnable(bool input) {
    internal_fpga_packet_generator = input;
    return *this;
}

DetectorSettings &DetectorSettings::InternalGeneratorImages(int64_t input) {
    check_min("Internal packet generator images", input, 1);
    check_max("Internal packet generator images", input, 64);
    internal_fpga_packet_generator_images = input;
    return *this;
}

DetectorSettings &DetectorSettings::StorageCells(int64_t input) {
    check_min("Storage cell number", input, 1);
    check_max("Storage cell number", input, 16);
    storage_cells = input;
    return *this;
}

DetectorSettings &DetectorSettings::StorageCellStart(int64_t input) {
    check_min("Start storage cell", input, 0);
    check_max("Start storage cell", input, 15);
    storage_cell_start = input;
    return *this;
}

DetectorSettings &DetectorSettings::StorageCellDelay(const std::chrono::nanoseconds &input) {
    check_min("Storage cell delay [ns]", input.count(), MIN_STORAGE_CELL_DELAY_IN_NS);
    storage_cell_delay = input;
    return *this;
}

DetectorSettings &DetectorSettings::DetectorDelay(const std::chrono::nanoseconds &input) {
    check_min("Detector delay (us)", input.count(), 0);
    detector_delay = input;
    return *this;
}

DetectorSettings &DetectorSettings::PedestalG0Frames(int64_t input) {
    check_min("Pedestal G0 frames", input, 0);
    pedestal_g0_frames = input;
    return *this;
}

DetectorSettings &DetectorSettings::PedestalG1Frames(int64_t input) {
    check_min("Pedestal G1 frames", input, 0);
    pedestal_g1_frames = input;
    return *this;
}

DetectorSettings &DetectorSettings::PedestalG2Frames(int64_t input) {
    check_min("Pedestal G2 frames", input, 0);
    pedestal_g2_frames = input;
    return *this;
}

DetectorSettings &DetectorSettings::UseGainHG0(bool input) {
    use_gain_hg0 = input;
    return *this;
}

DetectorSettings &DetectorSettings::FixGainG1(bool input) {
    fix_gain_g1 = input;
    return *this;
}

DetectorSettings &DetectorSettings::FrameTime(const std::chrono::microseconds &input) {
    frame_time = input;
    count_time = {};
    return *this;
}

DetectorSettings &DetectorSettings::FrameTime(const std::chrono::microseconds &in_frame_time,
                                              const std::chrono::microseconds &in_count_time) {
    check_min("Count time (us)", in_count_time.count(), MIN_COUNT_TIME_IN_US);
    frame_time = in_frame_time;
    count_time = in_count_time;
    return *this;
}

bool DetectorSettings::IsInternalGeneratorEnable() const {
    return internal_fpga_packet_generator;
}

int64_t DetectorSettings::GetInternalGeneratorImages() const {
    return internal_fpga_packet_generator_images;
}

int64_t DetectorSettings::GetStorageCells() const {
    return storage_cells;
}

int64_t DetectorSettings::GetStorageCellStart() const {
    return storage_cell_start;
}

std::chrono::nanoseconds DetectorSettings::GetStorageCellDelay() const {
    return storage_cell_delay;
}

std::chrono::nanoseconds DetectorSettings::GetDetectorDelay() const {
    return detector_delay;
}

int64_t DetectorSettings::GetPedestalG0Frames() const {
    return pedestal_g0_frames;
}

int64_t DetectorSettings::GetPedestalG1Frames() const {
    return pedestal_g1_frames;
}

int64_t DetectorSettings::GetPedestalG2Frames() const {
    return pedestal_g2_frames;
}

bool DetectorSettings::IsUseGainHG0() const {
    return use_gain_hg0;
}

bool DetectorSettings::IsFixGainG1() const {
    return fix_gain_g1;
}

std::chrono::microseconds DetectorSettings::GetFrameTime() const {
    return frame_time;
}

std::optional<std::chrono::microseconds> DetectorSettings::GetCountTime() const {
    return count_time;
}

DetectorSettings &DetectorSettings::FrameTime(const std::chrono::microseconds &input,
                                              const std::optional<std::chrono::microseconds> &in_count_time) {
    if (!in_count_time.has_value() || (in_count_time.value().count() == 0))
        return this->FrameTime(input);
    else
        return this->FrameTime(input, in_count_time.value());
}

uint32_t DetectorSettings::GetPedestalMinImageCount() const {
    return pedestal_min_image_count;
}

DetectorSettings &DetectorSettings::PedestalMinImageCount(uint32_t input) {
    check_min("Pedestal window size", input, 32);
    pedestal_min_image_count = input;
    return *this;
}

std::optional<float> DetectorSettings::GetEigerThreshold_keV() const {
    return eiger_threshold_keV;
}

DetectorSettings &DetectorSettings::EigerThreshold_keV(const std::optional<float> &input) {
    check_min("EIGER Threshold (keV)", input, 1.0);
    check_max("EIGER Threshold (keV)", input, 100.0);
    eiger_threshold_keV = input;
    return *this;
}

DetectorSettings &DetectorSettings::Timing(DetectorTiming input) {
    switch(input) {
        case DetectorTiming::Auto:
        case DetectorTiming::Trigger:
        case DetectorTiming::Burst:
        case DetectorTiming::Gated:
            timing = input;
            break;
        default:
            // Handle invalid input, e.g., throw an exception, log an error, set a default value, etc.
            throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                                  "Invalid DetectorTiming value");
    }
    return *this;
}

DetectorTiming DetectorSettings::GetTiming() const {
    return timing;
}

std::chrono::microseconds DetectorSettings::GetFrameTimePedestalG1G2() const {
    return frame_time_pedestalG1G2;
}

std::optional<int64_t> DetectorSettings::GetEigerBitDepth() const {
    return eiger_bitwidth;
}

DetectorSettings &DetectorSettings::EigerBitDepth(const std::optional<int64_t> &input) {
    if (input.has_value() && !((input.value() == 8) || (input.value() == 16) || (input.value() == 32))) {
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "EIGER bitwidth can be only 8, 16 or 32");
    }
    eiger_bitwidth = input;
    return *this;
}
