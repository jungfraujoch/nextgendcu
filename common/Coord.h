// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef INDEX_COORD_H
#define INDEX_COORD_H

#include <ostream>

class Coord {
public:
    float x,y,z;
    Coord();
    Coord(const float in[3]);
    Coord(float x, float y, float z);

    Coord operator+(const Coord &in) const;
    Coord operator-(const Coord &in) const;
    Coord operator*(float in) const;
    Coord operator/(float in) const;
    Coord operator-() const;

    Coord& operator+=(const Coord &in);
    Coord& operator-=(const Coord &in);
    Coord& operator*=(float in);
    Coord& operator/=(float in);

    bool operator==(const Coord &other) const;

    Coord operator%(const Coord &in) const; // Cross product
    float operator*(const Coord &in) const; // Dot product

    const float& operator[](int64_t val) const;
    float& operator[](int64_t val);

    float Length() const;
    Coord Normalize() const;

    friend std::ostream &operator<<( std::ostream &output, const Coord &in );
};

Coord operator*(float in1, const Coord& in2);

float angle_deg(const Coord &in1, const Coord &in2);


#endif //INDEX_COORD_H
