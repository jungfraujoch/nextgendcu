// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_DIFFRACTIONSPOT_H
#define JUNGFRAUJOCH_DIFFRACTIONSPOT_H

#include "Coord.h"
#include "DiffractionExperiment.h"
#include "DiffractionGeometry.h"
#include "SpotToSave.h"

// Definition of Bragg spot
class DiffractionSpot {
    float x = 0;
    float y = 0;
    int64_t pixel_count = 0;
    int64_t photons = 0;     // total photon count
    int64_t max_photons = INT64_MIN; // maximum number of counts per pixel in the spot
public:
    DiffractionSpot() = default;
    DiffractionSpot(uint32_t col, uint32_t line, int64_t photons);
    DiffractionSpot& operator+=(const DiffractionSpot& spot);
    int64_t PixelCount() const;
    int64_t Count() const;
    int64_t MaxCount() const;
    Coord RawCoord() const;
    Coord LabCoord(const DiffractionGeometry &experiment) const;
    void ConvertToImageCoordinates(const DiffractionExperiment& experiment, uint16_t module_number);
    double GetResolution(const DiffractionGeometry &experiment) const;
    Coord ReciprocalCoord(const DiffractionGeometry &experiment) const;
    operator SpotToSave() const;
    void AddPixel(uint32_t col, uint32_t line, int64_t photons);
};

#endif //JUNGFRAUJOCH_DIFFRACTIONSPOT_H
