// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "InstrumentMetadata.h"
#include "JFJochException.h"

InstrumentMetadata::InstrumentMetadata() {
    pulsed_source = false;
    electron_source = false;
}

InstrumentMetadata &InstrumentMetadata::SourceName(const std::string &input) {
    source_name = input;
    return *this;
}

InstrumentMetadata &InstrumentMetadata::SourceType(const std::string &input) {
    source_type = input;
    return *this;
}

InstrumentMetadata &InstrumentMetadata::InstrumentName(const std::string &input) {
    instrument_name = input;
    return *this;
}

InstrumentMetadata &InstrumentMetadata::PulsedSource(bool input) {
    pulsed_source = input;
    return *this;
}

InstrumentMetadata & InstrumentMetadata::ElectronSource(bool input) {
    electron_source = input;
    return *this;
}

bool InstrumentMetadata::IsElectronSource() const {
    return electron_source;
}

std::string InstrumentMetadata::GetSourceName() const {
    return source_name;
}

std::string InstrumentMetadata::GetSourceType() const {
    return source_type;
}

std::string InstrumentMetadata::GetInstrumentName() const {
    return instrument_name;
}

bool InstrumentMetadata::IsPulsedSource() const {
    return pulsed_source;
}
