// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_ROICIRCLE_H
#define JUNGFRAUJOCH_ROICIRCLE_H

#include "ROIElement.h"

class ROICircle : public ROIElement {
    float center_x;
    float center_y;
    float r_pxl;
public:
    ROICircle(const std::string &name, float x, float y, float r_pxl);
    [[nodiscard]] float GetX() const;
    [[nodiscard]] float GetY() const;
    [[nodiscard]] float GetRadius_pxl() const;
    void MarkROI(std::vector<uint16_t> &v, uint16_t value_to_mark, int64_t xpixel, int64_t ypixel) const override;
};

#endif //JUNGFRAUJOCH_ROICIRCLE_H
