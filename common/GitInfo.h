// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_GITINFO_H
#define JUNGFRAUJOCH_GITINFO_H

#include <string>

std::string jfjoch_git_sha1();
std::string jfjoch_git_date();
std::string jfjoch_version();

#endif //JUNGFRAUJOCH_GITINFO_H
