// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_ROIMAP_H
#define JUNGFRAUJOCH_ROIMAP_H

#include <map>
#include <vector>
#include <cstdint>
#include <optional>

#include "DetectorSetup.h"
#include "ROIDefinition.h"

class ROIMap {
    ROIDefinition rois;
    std::vector<uint16_t> map;
    std::map<std::string, uint16_t> roi_name_map;

    int64_t xpixel;
    int64_t ypixel;

    static constexpr const size_t box_array_size = 32;
    static constexpr const size_t circle_array_size = 32;
    void UpdateMask();
public:
    explicit ROIMap(const DetectorSetup& setup);
    ROIMap(int64_t xpixel, int64_t ypixel);
    void SetROIBox(const std::vector<ROIBox> &input);
    void SetROICircle(const std::vector<ROICircle> &input);
    void SetROI(const ROIDefinition& input);

    [[nodiscard]] const std::vector<uint16_t>& GetROIMap() const;
    [[nodiscard]] const std::map<std::string, uint16_t>& GetROINameMap() const;
    [[nodiscard]] const ROIDefinition& GetROIDefinition() const;
};

#endif //JUNGFRAUJOCH_ROIMAP_H
