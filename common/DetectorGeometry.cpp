// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "DetectorGeometry.h"
#include "JFJochException.h"
#include "Definitions.h"

DetectorGeometry::DetectorGeometry(const std::vector<DetectorModuleGeometry> &in_modules, bool vertical_flip)
: modules(in_modules), modular_detector(true) {
    if (modules.empty())
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "Module number must be positive");

    width = 0;
    height = 0;

    for (auto &m: modules) {
        width = std::max<int64_t>(width, m.GetMaxX()+1);
        height = std::max<int64_t>(height, m.GetMaxY()+1);
    }

    if (vertical_flip)
        VerticalFlip();
}

DetectorGeometry::DetectorGeometry(int32_t nmodules, int32_t horizontal_stacking, int32_t gap_x,
                                   int32_t gap_y, bool mirror_y) {
    modular_detector = true;

    if (horizontal_stacking <= 0)
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "Horizontal stacking must be positive");
    if (nmodules <= 0)
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "Module number must be positive");
    if (gap_x < 0)
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "Gap x has to be non-negative");
    if (gap_y < 0)
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "Gap y has to be non-negative");
    if (nmodules < horizontal_stacking)
        horizontal_stacking = nmodules;

    width = horizontal_stacking * CONVERTED_MODULE_COLS + (horizontal_stacking - 1) * gap_x;
    int64_t conv_lines = nmodules / horizontal_stacking + (nmodules % horizontal_stacking > 0 ? 1 : 0);
    height = conv_lines * CONVERTED_MODULE_LINES + (conv_lines - 1) * gap_y;

    for (int m = 0; m < nmodules; m++) {
        int64_t module_x = m % horizontal_stacking;
        int64_t module_y = m / horizontal_stacking;

        int64_t x0 = module_x * (CONVERTED_MODULE_COLS + gap_x);
        int64_t y0 = module_y * (CONVERTED_MODULE_LINES + gap_y);

        DetectorModuleGeometry::Direction fast = DetectorModuleGeometry::Direction::Xpos;
        DetectorModuleGeometry::Direction slow = DetectorModuleGeometry::Direction::Ypos;

        DetectorModuleGeometry mgeom(x0,y0,fast, slow);

        modules.emplace_back(mgeom);
    }

    if (mirror_y)
        VerticalFlip();
}

DetectorGeometry::DetectorGeometry(std::pair<int64_t, int64_t> dim) {
    modular_detector = false;
    if (dim.first <= 0 || dim.second <= 0)
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "Detector dimension must be positive numbers");
    width = dim.first;
    height = dim.second;
}

void DetectorGeometry::VerticalFlip() {
    for (auto &m: modules)
        m.VerticalFlip(height);
}

int64_t DetectorGeometry::GetDirectionStep(DetectorModuleGeometry::Direction direction) const {
    switch (direction) {
        case DetectorModuleGeometry::Direction::Xneg:
            return -1;
        case DetectorModuleGeometry::Direction::Xpos:
            return 1;
        case DetectorModuleGeometry::Direction::Yneg:
            return -width;
        case DetectorModuleGeometry::Direction::Ypos:
            return width;
        default:
            return 0;
    }
}

int64_t DetectorGeometry::GetModulesNum() const {
    if (!modular_detector)
        return 1;
    return modules.size();
}

int64_t DetectorGeometry::GetWidth(bool geom_transformed) const {
    if (geom_transformed || !modular_detector)
        return width;
    else
        return RAW_MODULE_COLS;
}

int64_t DetectorGeometry::GetHeight(bool geom_transformed) const {
    if (geom_transformed || !modular_detector)
        return height;
    else
        return RAW_MODULE_LINES * modules.size();
}

int64_t DetectorGeometry::GetPixel0(int64_t m, bool geom_transformed) const {
    if (m == 0 && !modular_detector)
        return 0;
    if ((m < 0) || (m >= modules.size()))
        throw JFJochException(JFJochExceptionCategory::ArrayOutOfBounds, "Wrong module number");
    if (geom_transformed)
        return modules[m].GetPixel0_X() + width * modules[m].GetPixel0_Y();
    else
        return m * RAW_MODULE_SIZE;
}

int64_t DetectorGeometry::GetFastDirectionStep(int64_t m) const {
    if ((m < 0) || (m >= modules.size()))
        throw JFJochException(JFJochExceptionCategory::ArrayOutOfBounds, "Wrong module number");
    return GetDirectionStep(modules[m].GetFastAxis());
}

int64_t DetectorGeometry::GetSlowDirectionStep(int64_t m) const {
    if ((m < 0) || (m >= modules.size()))
        throw JFJochException(JFJochExceptionCategory::ArrayOutOfBounds, "Wrong module number");
    return GetDirectionStep(modules[m].GetSlowAxis());
}

Coord DetectorGeometry::GetFastDirection(int64_t module_number) const {
    if ((module_number < 0) || (module_number >= modules.size()))
        throw JFJochException(JFJochExceptionCategory::ArrayOutOfBounds, "Wrong module number");

    return GetDirection(modules[module_number].GetFastAxis());
}

Coord DetectorGeometry::GetSlowDirection(int64_t module_number) const {
    if ((module_number < 0) || (module_number >= modules.size()))
        throw JFJochException(JFJochExceptionCategory::ArrayOutOfBounds, "Wrong module number");

    return GetDirection(modules[module_number].GetSlowAxis());
}

Coord DetectorGeometry::GetDirection(DetectorModuleGeometry::Direction direction) {
    switch (direction) {
        case DetectorModuleGeometry::Direction::Xneg:
            return {-1,  0, 0};
        case DetectorModuleGeometry::Direction::Yneg:
            return { 0, -1, 0};
        case DetectorModuleGeometry::Direction::Ypos:
            return { 0,  1, 0};
        default:
        case DetectorModuleGeometry::Direction::Xpos:
            return { 1,  0, 0};
    }
}

bool DetectorGeometry::IsModularDetector() const {
    return modular_detector;
}
