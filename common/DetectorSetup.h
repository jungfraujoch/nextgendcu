// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_DETECTORSETUP_H
#define JUNGFRAUJOCH_DETECTORSETUP_H

#include <chrono>
#include "DetectorGeometry.h"
#include "../jungfrau/JFModuleGainCalibration.h"

enum class DetectorType {EIGER, JUNGFRAU};

class DetectorSetup {
    std::string description;
    std::string serial_number;
    DetectorGeometry geometry;
    std::vector<std::string> det_modules_hostname;
    std::vector<std::string> gain_file_names;
    std::vector<std::string> trim_file_names;
    std::string trim_file_directory;
    std::vector<JFModuleGainCalibration> gain_calibration;
    int64_t udp_interface_count = 2;
    float pixel_size_um = 75.0f;
    std::string sensor_material = "Si";
    float sensor_thickness_um = 320.0f;
    std::vector<int64_t> tx_delay;
    DetectorType detector_type;
    int32_t high_voltage = 120.0;
    uint32_t ipv4_base_addr = 0x010a0a0a;
    bool module_sync = true;
    std::chrono::microseconds read_out_time;
public:
    // This is implicit constructor from DetectorGeometry with JUNGFRAU!
    // TODO: Remove implicit constructor
    DetectorSetup(const DetectorGeometry& geom);
    DetectorSetup(const DetectorGeometry& geom, DetectorType detector_type);
    DetectorSetup(const DetectorGeometry& geom,
                  DetectorType detector_type,
                  const std::string &description,
                  const std::vector<std::string> &det_modules_hostname);

    void LoadGain(const std::vector<std::string> &filenames);
    void SetTrimFiles(const std::vector<std::string> &filenames);

    DetectorSetup& TxDelay(const std::vector<int64_t> &v);
    DetectorSetup& UDPInterfaceCount(int64_t input);
    DetectorSetup& SensorMaterial(const std::string &input);
    DetectorSetup& SensorThickness_um(float input);
    DetectorSetup& PixelSize_um(float input);
    DetectorSetup& HighVoltage(int32_t input);
    DetectorSetup& SerialNumber(const std::string &input);
    DetectorSetup& BaseIPv4Addr(const std::string &input);
    DetectorSetup& ModuleSync(bool input);
    DetectorSetup& ReadOutTime(std::chrono::microseconds input);

    [[nodiscard]] DetectorType GetDetectorType() const;
    [[nodiscard]] const DetectorGeometry& GetGeometry() const;
    [[nodiscard]] const std::vector<std::string>& GetDetectorModuleHostname() const;
    [[nodiscard]] uint64_t GetModulesNum() const;
    [[nodiscard]] std::string GetDescription() const;
    [[nodiscard]] float GetPixelSize_mm() const;
    [[nodiscard]] float GetSensorThickness_um() const;
    [[nodiscard]] std::string GetSensorMaterial() const;
    [[nodiscard]] const std::vector<JFModuleGainCalibration> &GetGainCalibration() const;
    [[nodiscard]] int64_t GetUDPInterfaceCount() const;
    [[nodiscard]] const std::vector<int64_t> &GetTxDelay() const; // can be empty for default
    [[nodiscard]] const std::vector<std::string> &GetGainFileNames() const;
    [[nodiscard]] const std::vector<std::string> &GetTrimFileNames() const;
    [[nodiscard]] std::string GetTrimFileDirectory() const;
    [[nodiscard]] int32_t GetHighVoltage() const;
    [[nodiscard]] std::string GetSerialNumber() const;
    [[nodiscard]] uint32_t GetSrcIPv4Addr(uint32_t half_module) const;
    [[nodiscard]] std::string GetBaseIPv4Addr() const;
    [[nodiscard]] bool IsModuleSync() const;
    [[nodiscard]] std::chrono::microseconds GetReadOutTime() const;
    [[nodiscard]] std::chrono::microseconds GetMinFrameTime() const;
    [[nodiscard]] std::chrono::microseconds GetMinCountTime() const;
};


#endif //JUNGFRAUJOCH_DETECTORSETUP_H
