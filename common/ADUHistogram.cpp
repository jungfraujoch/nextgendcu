// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "ADUHistogram.h"

ADUHistogram::ADUHistogram() : histogram(ADU_HISTO_BIN_COUNT) {

}

void ADUHistogram::Add(const std::vector<uint64_t>& input) {
    std::unique_lock ul(m);
    if (input.size() != ADU_HISTO_BIN_COUNT)
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "Wrong vector input to ADUHistogram::Add");
    for (int i = 0; i < ADU_HISTO_BIN_COUNT; i++)
        histogram[i] += input[i];
}

void ADUHistogram::Add(const DeviceOutput &output) {
    std::unique_lock ul(m);
    for (int i = 0; i < ADU_HISTO_BIN_COUNT; i++)
        histogram[i] += output.adu_histogram[i];
}

const std::vector<uint64_t> &ADUHistogram::GetHistogram() const {
    return histogram;
}

MultiLinePlot ADUHistogram::GetPlot() const {
    std::unique_lock ul(m);
    MultiLinePlot ret(1);
    ret[0].x.resize(ADU_HISTO_BIN_COUNT);
    ret[0].y.resize(ADU_HISTO_BIN_COUNT);
    for (int i = 0; i < ADU_HISTO_BIN_COUNT; i++) {
        ret[0].x[i] = ADU_HISTO_BIN_WIDTH * i + ADU_HISTO_BIN_WIDTH / 2;
        ret[0].y[i] = histogram[i];
    }
    return ret;
}

void ADUHistogram::Restart() {
    std::unique_lock ul(m);
    for (int i = 0; i < ADU_HISTO_BIN_COUNT; i++)
        histogram[i] = 0;
}
