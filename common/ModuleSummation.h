// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef IMAGESUMMATION_H
#define IMAGESUMMATION_H

#include <cstdint>
#include <memory>
#include <mutex>
#include "Definitions.h"
#include "DiffractionExperiment.h"

class ModuleSummation {
    mutable std::mutex module_summation_mutex;
    bool pixel_signed;
    uint32_t output_depth_bytes;
    uint32_t fpga_depth_bytes;
    std::unique_ptr<DeviceOutput> output;
    bool first;
    bool is_empty;
public:
    explicit ModuleSummation(bool pixel_signed, uint32_t fpga_depth_bytes = 2);
    explicit ModuleSummation(const DiffractionExperiment &experiment);

    void AddFPGAOutput(const DeviceOutput& input, const std::optional<uint32_t> &in_fpga_depth_bytes = {});
    void AddEmptyOutput();

    [[nodiscard]] const DeviceOutput& GetOutput() const;
    [[nodiscard]] DeviceOutput& GetOutput();
    [[nodiscard]] bool empty() const;
};

#endif //IMAGESUMMATION_H
