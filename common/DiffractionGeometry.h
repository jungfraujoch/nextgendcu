// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_DIFFRACTIONGEOMETRY_H
#define JUNGFRAUJOCH_DIFFRACTIONGEOMETRY_H

#include "JFJochException.h"
#include "Coord.h"

class DiffractionGeometry {
    float beam_x_pxl = 0.0;
    float beam_y_pxl = 0.0;
    float det_distance_mm = 100.0;
    float pixel_size_mm = 0.075;
    float wavelength_A = 1.0;
public:
    DiffractionGeometry &BeamX_pxl(float input);
    DiffractionGeometry &BeamY_pxl(float input);
    DiffractionGeometry &DetectorDistance_mm(float input);
    DiffractionGeometry &PixelSize_mm(float input);
    DiffractionGeometry &Wavelength_A(float input);

    [[nodiscard]] float GetBeamX_pxl() const;
    [[nodiscard]] float GetBeamY_pxl() const;
    [[nodiscard]] float GetDetectorDistance_mm() const;
    [[nodiscard]] float GetPixelSize_mm() const;
    [[nodiscard]] float GetWavelength_A() const;
    [[nodiscard]] Coord GetScatteringVector() const;

    [[nodiscard]] Coord LabCoord(float x, float y) const;
    [[nodiscard]] Coord DetectorToRecip(float x, float y) const;
    [[nodiscard]] std::pair<float, float> RecipToDector(const Coord &recip) const;
    [[nodiscard]] float CosTwoTheta(float x, float y) const;
    [[nodiscard]] float Phi(float x, float y) const;
    [[nodiscard]] float PxlToRes(float x, float y) const;
    [[nodiscard]] float PxlToRes(float dist_pxl) const;
    [[nodiscard]] float ResToPxl(float d_A) const;
    [[nodiscard]] float DistFromEwaldSphere(const Coord& recip) const;
    [[nodiscard]] float CalcAzIntSolidAngleCorr(float q) const;
    [[nodiscard]] float CalcAzIntSolidAngleCorr(float x, float y) const;
    [[nodiscard]] float CalcAzIntPolarizationCorr(float x, float y, float coeff) const;

};

#endif //JUNGFRAUJOCH_DIFFRACTIONGEOMETRY_H
