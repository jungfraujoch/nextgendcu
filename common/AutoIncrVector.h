// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_AUTOINCRVECTOR_H
#define JFJOCH_AUTOINCRVECTOR_H

#include <vector>
#include "JFJochException.h"

template <class T>
class AutoIncrVector {
    std::vector<T> v;
public:
    T& operator[](int64_t pos ) {
        if (pos < 0)
            throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "Negative vector pos");
        if (pos >= v.size())
            v.resize(pos + 1);
        return v[pos];
    }

    const T& operator[](int64_t pos) const {
        if (pos < 0)
            throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "Negative vector pos");
        if (pos >= v.size())
            throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "Vector pos out of bounds");
        return v[pos];
    }

    void reserve(int64_t size) {
        if (size < 0)
            throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "Negative size for std::vector reserve");
        v.reserve(size);
    }

    const std::vector<T> &vec() const {
        return v;
    }

    auto size() const {
        return v.size();
    }

    bool empty() const {
        return v.empty();
    }
};

#endif //JFJOCH_AUTOINCRVECTOR_H
