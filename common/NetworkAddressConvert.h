// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_NETWORKADDRESSCONVERT_H
#define JUNGFRAUJOCH_NETWORKADDRESSCONVERT_H

#include <cstdint>
#include <string>

std::string IPv4AddressToStr(uint32_t addr);
std::string MacAddressToStr(uint64_t addr);
uint32_t IPv4AddressFromStr(const std::string& addr);
uint64_t MacAddressFromStr(const std::string& addr);

#endif //JUNGFRAUJOCH_NETWORKADDRESSCONVERT_H
