// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_IMAGEFORMATSETTINGS_H
#define JFJOCH_IMAGEFORMATSETTINGS_H

#include <optional>
#include <cstdint>

class ImageFormatSettings {
    bool geometry_transformation;
    bool auto_summation;
    bool jungfrau_conv_to_photon_counts;
    std::optional<bool> pixel_signed;
    std::optional<float> jungfrau_conversion_factor_keV;
    std::optional<int64_t> bit_depth_image;
    bool mask_module_edges;
    bool mask_chip_edges;

    bool apply_pixel_mask;
    bool mask_pixels_without_g0;
    float pedestal_g0_rms_limit;
public:
    ImageFormatSettings();

    ImageFormatSettings& GeometryTransformed(bool input);
    ImageFormatSettings& AutoSummation(bool input);
    ImageFormatSettings& JungfrauConversion(bool input);
    ImageFormatSettings& PixelSigned(const std::optional<bool> &input);
    ImageFormatSettings& JungfrauConvFactor_keV(const std::optional<float> &input);
    ImageFormatSettings& BitDepthImage(const std::optional<int64_t> &input);
    ImageFormatSettings& MaskModuleEdges(bool input);
    ImageFormatSettings& MaskChipEdges(bool input);
    ImageFormatSettings& MaskPixelsWithoutG0(bool input);
    ImageFormatSettings& ApplyPixelMask(bool input);
    ImageFormatSettings& PedestalG0RMSLimit(float value);

    [[nodiscard]] bool IsGeometryTransformed() const;
    [[nodiscard]] bool IsAutoSummation() const;
    [[nodiscard]] bool IsJungfrauConversion() const;
    [[nodiscard]] std::optional<bool> IsPixelSigned() const;
    [[nodiscard]] std::optional<float> GetJungfrauConvFactor_keV() const;
    [[nodiscard]] std::optional<int64_t> GetBitDepthImage() const;
    [[nodiscard]] bool IsMaskModuleEdges() const;
    [[nodiscard]] bool IsMaskChipEdges() const;
    [[nodiscard]] bool IsMaskPixelsWithoutG0() const;
    [[nodiscard]] bool IsApplyPixelMask() const;
    [[nodiscard]] float GetPedestalG0RMSLimit() const;
    void Raw();
    void Conv();
};


#endif //JFJOCH_IMAGEFORMATSETTINGS_H
