// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_AZIMUTHALINTEGRATIONSETTINGS_H
#define JFJOCH_AZIMUTHALINTEGRATIONSETTINGS_H

#include <optional>

class AzimuthalIntegrationSettings {
    bool solid_angle_correction = true;
    float high_q_recipA = 5.0;
    float low_q_recipA = 0.1;
    float q_spacing = 0.05;
    std::optional<float> polarization_factor;
public:
    AzimuthalIntegrationSettings& SolidAngleCorrection(bool input);
    AzimuthalIntegrationSettings& PolarizationFactor(const std::optional<float> &input);
    AzimuthalIntegrationSettings& HighQ_recipA(float input);
    AzimuthalIntegrationSettings& LowQ_recipA(float input);
    AzimuthalIntegrationSettings& QSpacing_recipA(float input);

    [[nodiscard]] bool IsSolidAngleCorrection() const;
    [[nodiscard]] std::optional<float> GetPolarizationFactor() const;
    [[nodiscard]] float GetHighQ_recipA() const;
    [[nodiscard]] float GetLowQ_recipA() const;
    [[nodiscard]] float GetQSpacing_recipA() const;
};


#endif //JFJOCH_AZIMUTHALINTEGRATIONSETTINGS_H
