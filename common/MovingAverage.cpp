// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <cmath>
#include "MovingAverage.h"

MovingAverage::MovingAverage(size_t elems) : elems(elems) {}

void MovingAverage::Add(float val) {
    std::unique_lock ul(m);

    if (q.size() >= elems)
        q.pop_front();
    q.push_back(val);
}

std::optional<float> MovingAverage::Read() const {
    std::unique_lock ul(m);

    if (q.empty())
        return {};

    float sum = 0;
    for (const auto &elem: q)
        sum += elem;

    return sum / q.size();
}