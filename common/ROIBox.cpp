// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "ROIBox.h"
#include "JFJochException.h"

ROIBox::ROIBox(const std::string &in_name, int64_t in_x_min, int64_t in_x_max, int64_t in_y_min, int64_t in_y_max)
        : ROIElement(in_name), x_min(in_x_min), x_max(in_x_max), y_min(in_y_min), y_max(in_y_max) {
    if (x_min < 0)
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "ROI rectangle Xmin negative");
    if (y_min < 0)
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "ROI rectangle Ymin negative");
    if (x_max < x_min)
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "ROI rectangle Xmax must be in range Xmin - (Xdetector - 1)");
    if (y_max < y_min)
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "ROI rectangle Ymax must be in range Ymin - (Ydetector - 1)");
}

int64_t ROIBox::GetXMin() const {
    return x_min;
}

int64_t ROIBox::GetXMax() const {
    return x_max;
}

int64_t ROIBox::GetYMin() const {
    return y_min;
}

int64_t ROIBox::GetYMax() const {
    return y_max;
}

void ROIBox::MarkROI(std::vector<uint16_t> &v, uint16_t value_to_mark, int64_t xpixel, int64_t ypixel) const {
    if (v.size() != xpixel * ypixel)
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "ROIRectangle::MarkROI mismatch in input array size");

    for (int64_t y = y_min; y <= y_max; y++) {
        for (int64_t x = x_min; x <= x_max; x++) {
            v[y * xpixel + x] = value_to_mark;
        }
    }
}
