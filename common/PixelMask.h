// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_PIXELMASK_H
#define JUNGFRAUJOCH_PIXELMASK_H

#include "DetectorSetup.h"
#include "DiffractionExperiment.h"
#include "../jungfrau/JFCalibration.h"

struct PixelMaskStatistics {
    uint32_t user_mask;
    uint32_t too_high_pedestal_rms;
    uint32_t wrong_gain;
};

class PixelMask {
    size_t nmodules;
    std::vector<uint32_t> mask;
    uint32_t LoadMask(const std::vector<uint32_t>& mask, uint8_t bit);
    PixelMaskStatistics statistics{0,0,0};
public:
    // NXmx bits
    constexpr static const uint8_t ModuleGapPixelBit = 0;
    constexpr static const uint8_t ErrorPixelBit = 1;
    constexpr static const uint8_t TooHighPedestalRMSPixelBit = 2;
    constexpr static const uint8_t UserMaskedPixelBit = 8;
    constexpr static const uint8_t ChipGapPixelBit = 31;
    constexpr static const uint8_t ModuleEdgePixelBit = 30;

    explicit PixelMask(const DetectorSetup& detector);
    explicit PixelMask(const DiffractionExperiment& experiment);
    void Update(const ImageFormatSettings& experiment);
    void LoadUserMask(const DiffractionExperiment& experiment, const std::vector<uint32_t>& mask);
    void LoadDetectorBadPixelMask(const DiffractionExperiment& experiment, const JFCalibration *calib);
    void LoadDetectorBadPixelMask(const std::vector<uint32_t>& mask);
    [[nodiscard]] const std::vector<uint32_t> &GetMaskRaw() const;
    [[nodiscard]] std::vector<uint32_t> GetMask(const DiffractionExperiment& experiment) const;
    [[nodiscard]] std::vector<uint8_t> GetCompressedMask(const DiffractionExperiment& experiment) const;
    [[nodiscard]] std::vector<uint32_t> GetUserMask(const DiffractionExperiment& experiment, bool conv = true) const;
    [[nodiscard]] PixelMaskStatistics GetStatistics() const;
};


#endif //JUNGFRAUJOCH_PIXELMASK_H
