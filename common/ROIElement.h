// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_ROIELEMENT_H
#define JUNGFRAUJOCH_ROIELEMENT_H

#include <string>
#include <vector>
#include <cstdint>

class ROIElement {
protected:
    std::string name;
    explicit ROIElement(const std::string &name);
public:
    [[nodiscard]] std::string GetName() const;
    virtual void MarkROI(std::vector<uint16_t> &v, uint16_t value_to_mark, int64_t xpixel, int64_t ypixel) const = 0;
};


#endif //JUNGFRAUJOCH_ROIELEMENT_H
