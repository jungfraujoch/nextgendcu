// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_DATASETSETTINGS_H
#define JUNGFRAUJOCH_DATASETSETTINGS_H

#include <cstdint>
#include <string>
#include <optional>

#include "UnitCell.h"
#include "Coord.h"
#include "../compression/CompressionAlgorithmEnum.h"
#include "../frame_serialize/JFJochMessages.h"

class DatasetSettings {
    int64_t images_per_trigger;
    int64_t ntrigger;

    float beam_x_pxl;
    float beam_y_pxl;
    float detector_distance_mm;
    float photon_energy_keV;

    std::string file_prefix;
    int64_t images_per_file;

    CompressionAlgorithm compression;

    std::string sample_name;
    std::optional<UnitCell> unit_cell;
    int64_t space_group_number;

    std::optional<float> total_flux;
    std::optional<float> attenuator_transmission;
    std::optional<GoniometerAxis> goniometer;
    nlohmann::json image_appendix;
    nlohmann::json header_appendix;

    Coord rotation_axis;

    float data_reduction_factor_serialmx;

    std::optional<std::chrono::microseconds> image_time;
    std::optional<uint64_t> run_number;
    std::optional<std::string> run_name;
    std::string group;
    std::optional<int64_t> compression_poisson_factor;
    std::optional<int64_t> pixel_value_low_threshold;
    std::optional<bool> save_calibration;

    bool overwrite_existing_files;
    bool write_nxmx_hdf5_master;
public:

    DatasetSettings();

    DatasetSettings& ImagesPerTrigger(int64_t input);
    DatasetSettings& NumTriggers(int64_t triggers);
    DatasetSettings& PhotonEnergy_keV(float input);
    DatasetSettings& BeamX_pxl(float input);
    DatasetSettings& BeamY_pxl(float input);
    DatasetSettings& DetectorDistance_mm(float input);
    DatasetSettings& FilePrefix(std::string input);
    DatasetSettings& Compression(CompressionAlgorithm input);
    DatasetSettings& SetUnitCell(const std::optional<UnitCell> &cell);
    DatasetSettings& SpaceGroupNumber(int64_t input);
    DatasetSettings& SampleName(std::string input);
    DatasetSettings& AttenuatorTransmission(const std::optional<float> &input);
    DatasetSettings& TotalFlux(const std::optional<float> &input);
    DatasetSettings& Goniometer(const std::optional<GoniometerAxis>& input);
    DatasetSettings& RotationAxis(const Coord &c);
    DatasetSettings& HeaderAppendix(const nlohmann::json& input);
    DatasetSettings& ImageAppendix(const nlohmann::json& input);
    DatasetSettings& ImagesPerFile(int64_t input);
    DatasetSettings& RunNumber(const std::optional<uint64_t> &run_number);
    DatasetSettings& RunName(const std::optional<std::string> &input);
    DatasetSettings& ExperimentGroup(const std::string &group);
    DatasetSettings& ImageTime(const std::optional<std::chrono::microseconds> &input);

    DatasetSettings& LossyCompressionSerialMX(float input);
    DatasetSettings& LossyCompressionPoisson(std::optional<int64_t> input);
    DatasetSettings& WriteNXmxHDF5Master(bool input);
    DatasetSettings& SaveCalibration(std::optional<bool> input);

    std::optional<float> GetAttenuatorTransmission() const;
    std::optional<float> GetTotalFlux() const;
    std::optional<GoniometerAxis> GetGoniometer() const;
    Coord GetRotationAxis() const;
    const nlohmann::json& GetHeaderAppendix() const;
    const nlohmann::json& GetImageAppendix() const;
    std::optional<UnitCell> GetUnitCell() const;
    int64_t GetSpaceGroupNumber() const;
    std::string GetSampleName() const;
    float GetPhotonEnergy_keV() const;
    float GetBeamX_pxl() const;
    float GetBeamY_pxl() const;
    float GetDetectorDistance_mm() const;

    Coord GetScatteringVector() const;

    std::string GetFilePrefix() const;

    CompressionAlgorithm GetCompressionAlgorithm() const;
    int64_t GetNumTriggers() const;
    int64_t GetImageNumPerTrigger() const;
    int64_t GetImagesPerFile() const;

    std::optional<uint64_t> GetRunNumber() const;
    std::optional<std::string> GetRunName() const;
    std::string GetExperimentGroup() const;
    std::optional<std::chrono::microseconds> GetImageTime() const;
    
    float GetLossyCompressionSerialMX() const;
    std::optional<int64_t> GetLossyCompressionPoisson() const;

    DatasetSettings& PixelValueLowThreshold(const std::optional<int64_t> &input);
    std::optional<int64_t> GetPixelValueLowThreshold() const;
    bool IsWriteNXmxHDF5Master() const;
    std::optional<bool> IsSaveCalibration() const;
};

#endif //JUNGFRAUJOCH_DATASETSETTINGS_H
