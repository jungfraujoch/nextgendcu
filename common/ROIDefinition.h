// Copyright (2019-2024) Paul Scherrer Institute

#ifndef ROIDEFINITION_H
#define ROIDEFINITION_H

#include <vector>
#include "ROIBox.h"
#include "ROICircle.h"

struct ROIDefinition {
    std::vector<ROIBox> boxes;
    std::vector<ROICircle> circles;
};

#endif //ROIDEFINITION_H
