// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_FILEWRITERSETTINGS_H
#define JFJOCH_FILEWRITERSETTINGS_H


class FileWriterSettings {
    int hdf5_master_format_version = 1;
    bool overwrite_files = false;
public:
    FileWriterSettings &OverwriteExistingFiles(bool input);
    FileWriterSettings &HDF5MasterFormatVersion(int input);

    int GetHDF5MasterFormatVersion() const;
    bool IsOverwriteExistingFiles() const;
};


#endif //JFJOCH_FILEWRITERSETTINGS_H
