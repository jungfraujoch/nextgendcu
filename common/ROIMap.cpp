// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "ROIMap.h"
#include "JFJochException.h"

ROIMap::ROIMap(int64_t in_xpixel, int64_t in_ypixel)
        : map(in_xpixel * in_ypixel, UINT16_MAX),
          xpixel(in_xpixel), ypixel(in_ypixel) {
}

ROIMap::ROIMap(const DetectorSetup &setup)
        : ROIMap(setup.GetGeometry().GetWidth(), setup.GetGeometry().GetHeight()) {}

void ROIMap::UpdateMask() {
    roi_name_map.clear();

    for (auto &i: map)
        i = UINT16_MAX;

    for (int i = 0; i < rois.boxes.size(); i++) {
            rois.boxes[i].MarkROI(map, i, xpixel, ypixel);
            roi_name_map[rois.boxes[i].GetName()] = i;
    }

    for (int i = 0; i < rois.circles.size(); i++) {
        size_t id = rois.circles.size() + i;
        rois.circles[i].MarkROI(map, id, xpixel, ypixel);
        roi_name_map[rois.circles[i].GetName()] = id;
    }
}

const std::vector<uint16_t>& ROIMap::GetROIMap() const {
    return map;
}

const std::map<std::string, uint16_t> &ROIMap::GetROINameMap() const {
    return roi_name_map;
}

void ROIMap::SetROI(const ROIDefinition &input) {
    if (rois.boxes.size() > box_array_size)
        throw JFJochException(JFJochExceptionCategory::ArrayOutOfBounds, "Limit of box ROIs exceeded");
    if (rois.circles.size() > circle_array_size)
        throw JFJochException(JFJochExceptionCategory::ArrayOutOfBounds, "Limit of circular ROIs exceeded");
    for (const auto &i: input.boxes) {
        if (i.GetXMax() >= xpixel)
            throw JFJochException(JFJochExceptionCategory::ArrayOutOfBounds, "ROI box X coordinate out of detector bounds");
        if (i.GetYMax() >= ypixel)
            throw JFJochException(JFJochExceptionCategory::ArrayOutOfBounds, "ROI box Y coordinate out of detector bounds");
    }
    rois = input;
    UpdateMask();
}

void ROIMap::SetROIBox(const std::vector<ROIBox> &input) {
    ROIDefinition tmp = rois;
    tmp.boxes = input;
    SetROI(tmp);
}

void ROIMap::SetROICircle(const std::vector<ROICircle> &input) {
    ROIDefinition tmp = rois;
    tmp.circles = input;
    SetROI(tmp);
}

const ROIDefinition &ROIMap::GetROIDefinition() const {
    return rois;
}
