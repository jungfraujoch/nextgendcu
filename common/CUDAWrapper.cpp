// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_USE_CUDA

#include "CUDAWrapper.h"

int32_t get_gpu_count() {
    return 0;
}

void set_gpu(int32_t dev_id) {}

#endif
