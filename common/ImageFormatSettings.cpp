// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "ImageFormatSettings.h"
#include "JFJochException.h"
#include "Definitions.h"

#define check_max(param, val, max) if ((val) > (max)) throw JFJochException(JFJochExceptionCategory::InputParameterAboveMax, param)
#define check_min(param, val, min) if ((val) < (min)) throw JFJochException(JFJochExceptionCategory::InputParameterBelowMin, param)
#define check_finite(param, val) if (!std::isfinite(val)) throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, param)

ImageFormatSettings::ImageFormatSettings() {
    jungfrau_conv_to_photon_counts = true;
    auto_summation = true;
    geometry_transformation = true;
    mask_chip_edges = true;
    mask_module_edges = true;
    mask_pixels_without_g0 = true;
    apply_pixel_mask = false;
    pedestal_g0_rms_limit = 100;
}

ImageFormatSettings &ImageFormatSettings::GeometryTransformed(bool input) {
    geometry_transformation = input;
    return *this;
}

ImageFormatSettings &ImageFormatSettings::AutoSummation(bool input) {
    auto_summation = input;
    return *this;
}

ImageFormatSettings &ImageFormatSettings::PixelSigned(const std::optional<bool> &input) {
    pixel_signed = input;
    return *this;
}

ImageFormatSettings &ImageFormatSettings::JungfrauConvFactor_keV(const std::optional<float> &input) {
    if (!input.has_value() || (input.value() == 0.0f))
        jungfrau_conversion_factor_keV = {};
    else {
        check_min("JUNGFRAU conversion factor (keV)", input.value(), MIN_ENERGY);
        check_max("JUNGFRAU conversion factor (keV)", input.value(), MAX_ENERGY);
        jungfrau_conversion_factor_keV = input;
    }
    return *this;
}

ImageFormatSettings &ImageFormatSettings::BitDepthImage(const std::optional <int64_t> &input) {
    if (!input.has_value() || (input.value() == 0))
        bit_depth_image = {};
    else if ((input.value() == 16) || (input.value() == 32) || (input.value() == 8))
        bit_depth_image = input.value();
    else
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "Bit depth of 8, 16, 32 or 0 (auto) allowed");
    return *this;
}

bool ImageFormatSettings::IsGeometryTransformed() const {
    return geometry_transformation;
}

bool ImageFormatSettings::IsAutoSummation() const {
    return auto_summation;
}

std::optional<bool> ImageFormatSettings::IsPixelSigned() const {
    return pixel_signed;
}

std::optional<float> ImageFormatSettings::GetJungfrauConvFactor_keV() const {
    return jungfrau_conversion_factor_keV;
}

std::optional <int64_t> ImageFormatSettings::GetBitDepthImage() const {
    return bit_depth_image;
}

void ImageFormatSettings::Raw() {
    jungfrau_conv_to_photon_counts = false;
    auto_summation = false;
    geometry_transformation = false;
    pixel_signed = false;
    bit_depth_image = 16;
    jungfrau_conversion_factor_keV = {};
}

void ImageFormatSettings::Conv() {
    jungfrau_conv_to_photon_counts = true;
    auto_summation = true;
    geometry_transformation = true;
    pixel_signed = {};
    bit_depth_image = {};
    jungfrau_conversion_factor_keV = {};
}

bool ImageFormatSettings::IsJungfrauConversion() const {
    return jungfrau_conv_to_photon_counts;
}

ImageFormatSettings &ImageFormatSettings::JungfrauConversion(bool input) {
    jungfrau_conv_to_photon_counts = input;
    return *this;
}

ImageFormatSettings &ImageFormatSettings::MaskModuleEdges(bool input) {
    mask_module_edges = input;
    return *this;
}

ImageFormatSettings &ImageFormatSettings::MaskChipEdges(bool input) {
    mask_chip_edges = input;
    return *this;
}

bool ImageFormatSettings::IsMaskModuleEdges() const {
    return mask_module_edges;
}

bool ImageFormatSettings::IsMaskChipEdges() const {
    return mask_chip_edges;
}

ImageFormatSettings &ImageFormatSettings::ApplyPixelMask(bool input) {
    apply_pixel_mask = input;
    return *this;
}

ImageFormatSettings &ImageFormatSettings::MaskPixelsWithoutG0(bool input) {
    mask_pixels_without_g0 = input;
    return *this;
}

bool ImageFormatSettings::IsApplyPixelMask() const {
    return apply_pixel_mask;
}

bool ImageFormatSettings::IsMaskPixelsWithoutG0() const {
    return mask_pixels_without_g0;
}

ImageFormatSettings &ImageFormatSettings::PedestalG0RMSLimit(float value) {
    check_min("Pedestal G0 RMS limit", value, 0);
    pedestal_g0_rms_limit = value;
    return *this;
}

float ImageFormatSettings::GetPedestalG0RMSLimit() const {
    return pedestal_g0_rms_limit;
}
