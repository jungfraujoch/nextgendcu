// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "ZeroCopyReturnValue.h"

#include "ImageBuffer.h"
#include "ThreadSafeFIFO.h"

ZeroCopyReturnValue::ZeroCopyReturnValue(void *in_ptr, ImageBuffer &in_ctrl, uint32_t in_handle)
: ptr(in_ptr), handle(in_handle), buf_ctrl(in_ctrl), image_number(-1), payload_size(0) {
}

void ZeroCopyReturnValue::SetImageNumber(int64_t in_image_number) {
    image_number = in_image_number;
}

void ZeroCopyReturnValue::SetImageSize(size_t in_payload_size) {
    payload_size = in_payload_size;
}

size_t ZeroCopyReturnValue::GetImageSize() const {
    return payload_size;
}

void * ZeroCopyReturnValue::GetImage() const {
    return ptr;
}

void ZeroCopyReturnValue::release() const {
    buf_ctrl.ReleaseSlot(handle, image_number, payload_size);
}

int64_t ZeroCopyReturnValue::GetImageNumber() const {
    return image_number;
}
