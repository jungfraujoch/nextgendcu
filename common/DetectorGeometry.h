// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_DETECTORGEOMETRY_H
#define JUNGFRAUJOCH_DETECTORGEOMETRY_H

#include <cstdint>
#include <vector>
#include "Coord.h"
#include "DetectorModuleGeometry.h"

class DetectorGeometry {
    int64_t width;
    int64_t height;
    bool modular_detector;
    std::vector<DetectorModuleGeometry> modules;
    [[nodiscard]] int64_t GetDirectionStep(DetectorModuleGeometry::Direction direction) const;
    [[nodiscard]] static Coord GetDirection(DetectorModuleGeometry::Direction direction) ;
public:
    DetectorGeometry(const std::vector<DetectorModuleGeometry> &pixel_0, bool vertical_flip = false);
    DetectorGeometry(int32_t nmodules,
                     int32_t horizontal_stacking = 1,
                     int32_t gap_x = 0,
                     int32_t gap_y = 0,
                     bool mirror_y = true); // regular geometry
    explicit DetectorGeometry(std::pair<int64_t, int64_t> dim);

    [[nodiscard]] int64_t GetModulesNum() const;
    [[nodiscard]] int64_t GetWidth(bool geom_transformed = true) const;
    [[nodiscard]] int64_t GetHeight(bool geom_transformed = true) const;
    [[nodiscard]] int64_t GetPixel0(int64_t module_number, bool geom_transformed = true) const;
    [[nodiscard]] int64_t GetFastDirectionStep(int64_t module_number) const;
    [[nodiscard]] int64_t GetSlowDirectionStep(int64_t module_number) const;
    [[nodiscard]] Coord GetFastDirection(int64_t module_number) const;
    [[nodiscard]] Coord GetSlowDirection(int64_t module_number) const;
    void VerticalFlip();
    [[nodiscard]] bool IsModularDetector() const;
};

#endif //JUNGFRAUJOCH_DETECTORGEOMETRY_H
