// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "DetectorModuleGeometry.h"
#include "JFJochException.h"

inline bool dir_x(DetectorModuleGeometry::Direction direction) {
    if ((direction == DetectorModuleGeometry::Direction::Xpos) ||
        (direction == DetectorModuleGeometry::Direction::Xneg))
        return true;
    else
        return false;
}

DetectorModuleGeometry::DetectorModuleGeometry(int64_t in_x0, int64_t in_y0,
                                               DetectorModuleGeometry::Direction in_fast,
                                               DetectorModuleGeometry::Direction in_slow) :
                                               x0(in_x0), y0(in_y0), fast(in_fast), slow(in_slow) {
    if ((dir_x(fast) && dir_x(slow)) || (!dir_x(fast) && !dir_x(slow)))
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "One of axes (fast/slow) has to be X and another one Y.");
    if (dir_x(fast) && (fast == Direction::Xneg) && (x0 - fast_pixels + 1 < 0))
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "Edge of module in X is outside of the area.");

    if (dir_x(slow) && (slow == Direction::Xneg) && (x0 - slow_pixels + 1 < 0))
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "Edge of module in X is outside of the area.");

    if (dir_x(fast) && (slow == Direction::Yneg) && (y0 - slow_pixels + 1 < 0))
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "Edge of module in Y is outside of the area.");

    if (dir_x(slow) && (fast == Direction::Yneg) && (y0 - fast_pixels + 1 < 0))
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "Edge of module in Y is outside of the area.");
}

int64_t DetectorModuleGeometry::GetMaxX() const {
    if (fast == Direction::Xpos)
        return x0 + fast_pixels - 1;
    else if (slow == Direction::Xpos)
        return x0 + slow_pixels - 1;
    else
        return x0;
}

int64_t DetectorModuleGeometry::GetMaxY() const {
    if (fast == Direction::Ypos)
        return y0 + fast_pixels - 1;
    else if (slow == Direction::Ypos)
        return y0 + slow_pixels - 1;
    else
        return y0;
}

int64_t DetectorModuleGeometry::GetPixel0_X() const {
    return x0;
}

int64_t DetectorModuleGeometry::GetPixel0_Y() const {
    return y0;
}

DetectorModuleGeometry::Direction DetectorModuleGeometry::GetSlowAxis() const {
    return slow;
}

DetectorModuleGeometry::Direction DetectorModuleGeometry::GetFastAxis() const {
    return fast;
}

void DetectorModuleGeometry::VerticalFlip(std::size_t detector_height) {
    if (detector_height - 1 < y0)
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid, "Something wrong with detector_height");

    if (fast == Direction::Yneg)
        fast = Direction::Ypos;
    else if (fast == Direction::Ypos)
        fast = Direction::Yneg;

    if (slow == Direction::Yneg)
        slow = Direction::Ypos;
    else if (slow == Direction::Ypos)
        slow = Direction::Yneg;

    y0 = detector_height - y0 - 1;
}
