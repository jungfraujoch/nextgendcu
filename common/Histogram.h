// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_HISTOGRAM_H
#define JUNGFRAUJOCH_HISTOGRAM_H

#include <cstdint>
#include <cstddef>
#include <vector>
#include <mutex>

#include "Plot.h"
#include "../common/JFJochException.h"

template <class T>
class SetAverage {
    std::vector<T> sum;
    std::vector<uint64_t> count;
    std::mutex m;
public:
    explicit SetAverage(size_t bins) : sum(bins), count(bins) {}
    void Add(size_t bin, T val) {
        std::unique_lock ul(m);

        if (bin < sum.size()) {
            sum[bin] += val;
            count[bin] += 1;
        }
    }

    MultiLinePlot GetPlot() {
        std::unique_lock ul(m);

        MultiLinePlot ret(1);
        ret[0].x.resize(sum.size());
        ret[0].y.resize(sum.size());
        for (int i = 0; i < sum.size(); i++) {
            ret[0].x[i] = static_cast<float>(i);
            if (count[i] > 0)
                ret[0].y[i] = static_cast<float>(sum[i]) / count[i];
            else
                ret[0].y[i] = 0;
        }
        return ret;
    }
};

class FloatHistogram {
    std::vector<uint64_t> count;
    std::mutex m;
    float min;
    float max;
    float spacing;
public:
    explicit FloatHistogram(size_t bins, float in_min, float in_max) : count(bins), min(in_min), max(in_max) {
        if (bins == 0)
            throw JFJochException(JFJochExceptionCategory::InputParameterBelowMin, "FloatHistogram neds to have non-zero bins");
        spacing = (max - min) / bins;
    }
    void Add(float val) {
        std::unique_lock ul(m);

        size_t bin = (val - min) / spacing;

        if (bin < count.size())
            count[bin] += 1;
    }

    MultiLinePlot GetPlot() {
        std::unique_lock ul(m);

        MultiLinePlot ret(1);
        ret[0].x.resize(count.size());
        ret[0].y.resize(count.size());
        for (int i = 0; i < count.size(); i++) {
            ret[0].x[i] = min + (i + 0.5f) * spacing;
            ret[0].y[i] = static_cast<float>(count[i]);
        }
        return ret;
    }
};

#endif //JUNGFRAUJOCH_HISTOGRAM_H
