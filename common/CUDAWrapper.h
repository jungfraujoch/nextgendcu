// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_CUDAWRAPPER_H
#define JUNGFRAUJOCH_CUDAWRAPPER_H

#include <cstdint>

int32_t get_gpu_count();
void set_gpu(int32_t dev_id);

#endif //JUNGFRAUJOCH_CUDAWRAPPER_H
