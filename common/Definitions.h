// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef DEFINITIONS_H
#define DEFINITIONS_H

#include "../fpga/pcie_driver/jfjoch_fpga.h"

#define WVL_1A_IN_KEV           12.39854f

#define CONVERTED_MODULE_LINES  (514L)
#define CONVERTED_MODULE_COLS   (1030L)
#define CONVERTED_MODULE_SIZE   (CONVERTED_MODULE_LINES * CONVERTED_MODULE_COLS)
#define JUNGFRAU_PACKET_SIZE_BYTES  (8192)

#define MIN_FRAME_TIME_JUNGFRAU_HALF_SPEED_IN_US 1000
#define MIN_FRAME_TIME_JUNGFRAU_FULL_SPEED_IN_US 470
#define MIN_FRAME_TIME_EIGER_IN_US 250
#define MAX_COUNT_TIME_JUNGFRAU_IN_US 2000

#define MIN_COUNT_TIME_IN_US 3

#define MIN_STORAGE_CELL_DELAY_IN_NS 2100

#define MIN_ENERGY 0.001
#define MAX_ENERGY 500.0

#define FRAME_TIME_PEDE_G1G2_IN_US (10*1000)

#define DEFAULT_G0_FACTOR   (41.0)
#define DEFAULT_G1_FACTOR   (-1.439)
#define DEFAULT_G2_FACTOR   (-0.1145)
#define DEFAULT_HG0_FACTOR  (100.0)

#define MAX_SPOT_COUNT (250)

#define MASK_PEDESTAL_G0_RMS_LIMIT (1U<<4)

#define PEDESTAL_MIN_IMAGE_COUNT               128
#define PEDESTAL_WRONG                       (UINT16_MAX)
#define PEDESTAL_G0_WRONG_GAIN_ALLOWED_COUNT     2

#define MESSAGE_SIZE_FOR_START_END (256*1024*1024) // pessimistic highest value

#endif //DEFINITIONS_H
