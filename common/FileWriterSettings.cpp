// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "FileWriterSettings.h"
#include "JFJochException.h"

FileWriterSettings &FileWriterSettings::OverwriteExistingFiles(bool input) {
    overwrite_files = input;
    return *this;
}

FileWriterSettings &FileWriterSettings::HDF5MasterFormatVersion(int input) {
    if ((input == 1) || (input == 2))
        hdf5_master_format_version = input;
    else
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "Only 1 and 2 allowed");
    return *this;
}

int FileWriterSettings::GetHDF5MasterFormatVersion() const {
    return hdf5_master_format_version;
}

bool FileWriterSettings::IsOverwriteExistingFiles() const {
    return overwrite_files;
}
