// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <cmath>
#include "Coord.h"
#include "JFJochException.h"

Coord::Coord() {
    x = 0.0; y = 0.0; z = 0.0;
}

Coord::Coord(const float in[3]) {
    x = in[0];
    y = in[1];
    z = in[2];
}

Coord::Coord(float in_x, float in_y, float in_z) {
    x = in_x;
    y = in_y;
    z = in_z;
}

Coord Coord::operator+(const Coord &in) const {
    return Coord(this->x+in.x, this->y+in.y, this->z+in.z);
}

Coord Coord::operator-(const Coord &in) const {
    return Coord(this->x-in.x, this->y-in.y, this->z-in.z);
}

Coord Coord::operator*(float in) const {
    return Coord(this->x*in, this->y*in, this->z*in);
}

Coord Coord::operator/(float in) const {
    return Coord(this->x/in, this->y/in, this->z/in);
};

Coord Coord::operator-() const {
    return Coord(- this->x, -this->y, -this->z);
}

Coord& Coord::operator+=(const Coord &in) {
    this->x += in.x;
    this->y += in.y;
    this->z += in.z;
    return *this;
}

Coord& Coord::operator-=(const Coord &in) {
    this->x -= in.x;
    this->y -= in.y;
    this->z -= in.z;
    return *this;
}

Coord& Coord::operator*=(float in) {
    this->x *= in;
    this->y *= in;
    this->z *= in;
    return *this;
}

Coord& Coord::operator/=(float in) {
    this->x /= in;
    this->y /= in;
    this->z /= in;
    return *this;
}

Coord Coord::operator%(const Coord &in) const {
    return Coord(this->y * in.z - this->z * in.y,
            this->z * in.x - this->x * in.z,
            this->x * in.y - this->y * in.x);
}; // Cross product

float Coord::operator*(const Coord &in) const {
    return this->x * in.x + this->y * in.y + this->z * in.z;
};

bool Coord::operator==(const Coord &other) const {
    if ((this->x == other.x) && (this->y == other.y) && (this->z == other.z))
        return true;
    else
        return false;
}

float Coord::Length() const {
    return sqrt(this->x*this->x + this->y*this->y + this->z*this->z);
}

Coord Coord::Normalize() const {
    float len = Length();
    return Coord(this->x/len, this->y/len, this->z/len);
}

Coord operator*(float in1, const Coord& in2) {
    return in2 * in1;
}

const float& Coord::operator[](int64_t val) const {
    switch (val) {
        case 0:
            return x;
        case 1:
            return y;
        case 2:
            return z;
        default:
            throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                                  "Coord index must be in range 0-2");
    }
}

float& Coord::operator[](int64_t val) {
    switch (val) {
        case 0:
            return x;
        case 1:
            return y;
        case 2:
            return z;
        default:
            throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                                  "Coord index must be in range 0-2");
    }
}

std::ostream &operator<<( std::ostream &output, const Coord &in ) {
    output << in.x << " " << in.y << " " << in.z;
    return output;
}

float angle_deg(const Coord &c1, const Coord &c2) {
    float cos_ang = c1 * c2 / (c1.Length() * c2.Length());
    return acos(cos_ang) * (180.0 / M_PI);
}

