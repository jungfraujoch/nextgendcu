// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_DETECTORMODULEGEOMETRY_H
#define JUNGFRAUJOCH_DETECTORMODULEGEOMETRY_H

#include <cstdint>
#include <cstddef>
#include "Definitions.h"

class DetectorModuleGeometry {
public:
    enum class Direction {Xneg, Xpos, Yneg, Ypos};
private:
    int64_t x0;
    int64_t y0;

    constexpr static const int64_t fast_pixels = CONVERTED_MODULE_COLS;
    constexpr static const int64_t slow_pixels = CONVERTED_MODULE_LINES;

    Direction fast;
    Direction slow;
public:
    DetectorModuleGeometry(int64_t x0, int64_t y0, Direction fast, Direction slow);
    [[nodiscard]] int64_t GetMaxX() const;
    [[nodiscard]] int64_t GetMaxY() const;

    [[nodiscard]] int64_t GetPixel0_X() const;
    [[nodiscard]] int64_t GetPixel0_Y() const;
    [[nodiscard]] Direction GetSlowAxis() const;
    [[nodiscard]] Direction GetFastAxis() const;

    void VerticalFlip(size_t detector_height);
};


#endif //JUNGFRAUJOCH_DETECTORMODULEGEOMETRY_H
