// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_MOVINGAVERAGE_H
#define JFJOCH_MOVINGAVERAGE_H

#include <mutex>
#include <deque>
#include <optional>

class MovingAverage {
    mutable std::mutex m;
    size_t elems;
    std::deque<float> q;
public:
    explicit MovingAverage(size_t elems);
    void Add(float val);
    [[nodiscard]] std::optional<float> Read() const;
};


#endif //JFJOCH_MOVINGAVERAGE_H
