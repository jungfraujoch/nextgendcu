import React, {Component} from 'react';

import {broker_status} from "../openapi";
import {Alert, AlertColor} from "@mui/material";

type MyProps = {
    s?: broker_status
}

type MyState = {}

class ErrorMessage extends Component<MyProps, MyState> {
    severity(input?: broker_status.message_severity) : AlertColor {
        if (input === undefined)
            return "error";

        switch (input) {
            case broker_status.message_severity.INFO:
                return "info";
            case broker_status.message_severity.WARNING:
                return "warning";
            case broker_status.message_severity.ERROR:
                return "error";
            case broker_status.message_severity.SUCCESS:
                return "success";
        }
    }

    alert(input: AlertColor, message: string) : JSX.Element {
        return <Alert severity={input}>{message}</Alert>
    }

    render() {
        if (this.props.s === undefined)
            return this.alert("error", "Not connected to Jungfraujoch instance; check if jfjoch_broker is running");

        if (this.props.s.message === undefined)
            return this.alert("info", "");

        return this.alert(this.severity(this.props.s.message_severity),this.props.s.message);
    }
}

export default ErrorMessage;
