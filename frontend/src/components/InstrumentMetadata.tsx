import React, {Component} from 'react';

import {
    Autocomplete,
    Checkbox,
    FormControlLabel, FormGroup,
    Grid, Stack,
    TextField
} from "@mui/material";
import Paper from "@mui/material/Paper";
import {instrument_metadata} from "../openapi";
import _ from "lodash";
import ButtonWithSnackbar from "./ButtonWithSnackbar";

type MyProps = {
    s?: instrument_metadata
}

type MyState = {
    s: instrument_metadata
    last_downloaded_s: instrument_metadata
};

const default_instrument_metadata: instrument_metadata = {
    source_name: "Swiss Light Source",
    instrument_name: "X06SA",
    source_type: "Synchrotron X-ray Source",
    pulsed_source: false,
    electron_source: false
}

const source_types : string[] = [
    "Synchrotron X-ray Source",
    "Free-Electron Laser",
    "Rotating Anode X-ray",
    "Fixed Tube X-ray",
    "Electron microscope",
    "X-ray Plasma Source",
    "Metal Jet X-ray"
];

class ImageFormatSettings extends Component<MyProps, MyState> {
    state : MyState = {
        s: default_instrument_metadata,
        last_downloaded_s: default_instrument_metadata
    }

    getValues () {
        if (this.props.s !== undefined) {
            let instr_metadata: instrument_metadata = this.props.s;
            if (!_.isEqual(instr_metadata, this.state.last_downloaded_s))
                this.setState({
                    s: instr_metadata,
                    last_downloaded_s: instr_metadata
                });
        }
    }

    componentDidMount() {
        this.getValues();
    }

    componentDidUpdate() {
        this.getValues();
    }

    render() {
        return <Paper style={{textAlign: 'center'}} sx={{ height: 480, width: '100%' }}>

            <Grid container>
                <Grid item xs={1}/>
                <Grid item xs={10}>
                    <br/><strong>Source and instrument metadata</strong><br/><br/>
                </Grid>
                <Grid item xs={1}/>
                <Grid item xs={1}/>
                <Grid item xs={10}>
                    <Stack spacing={2}>
                        <TextField id="source_name" label="Source name" variant="outlined"
                                   fullWidth
                                   onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                                       this.setState(prevState => ({
                                           s: {...prevState.s, source_name: event.target.value}
                                       }))
                                   }}
                                   value={this.state.s.source_name}/>
                        <TextField id="instrument_name" label="Instrument name" variant="outlined"
                                   fullWidth
                                   onChange={(event: React.ChangeEvent<HTMLInputElement>) => {
                                       this.setState(prevState => ({
                                           s: {...prevState.s, instrument_name: event.target.value}
                                       }))
                                   }}
                                   value={this.state.s.instrument_name}/>
                        <Autocomplete
                            fullWidth
                            freeSolo
                            multiple={false}
                            onChange={(event, newValue: string | null | undefined) => {
                                if (typeof newValue === "string")
                                    this.setState(prevState => ({s: {...prevState.s, source_type: newValue}}));
                            }}

                            inputValue={this.state.s.source_type}

                            onInputChange={(_, newInputValue: string) => {
                                this.setState(prevState => ({
                                    s: {...prevState.s, source_type: newInputValue}
                                }));
                            }}
                            selectOnFocus
                            clearOnBlur
                            value={this.state.s.source_type}
                            options={source_types}
                            renderInput={(params) => <TextField {...params} label="Source type" variant="outlined"/>}
                        />
                        <FormGroup>
                            <FormControlLabel control={
                                <Checkbox checked={this.state.s.pulsed_source === true}
                                          onChange={
                                              (event: React.ChangeEvent<HTMLInputElement>) => {
                                                  this.setState(
                                                      prevState => ({
                                                          s: {...prevState.s, pulsed_source: event.target.checked}
                                                      }));
                                              }}/>
                            } label="Pulsed source (XFEL)"/>
                            <FormControlLabel control={
                                <Checkbox checked={this.state.s.electron_source === true}
                                          onChange={
                                              (event: React.ChangeEvent<HTMLInputElement>) => {
                                                  this.setState(
                                                      prevState => ({
                                                          s: {...prevState.s, electron_source: event.target.checked}
                                                      }));
                                              }}/>
                            } label="Electron source"/>
                        </FormGroup></Stack><br/>
                </Grid>
                <Grid item xs={1}/>
                <Grid item xs={1}/>
                <Grid item xs={10}>
                    <ButtonWithSnackbar
                        color={"primary"}
                        path={"/config/instrument"}
                        input={JSON.stringify(this.state.s)}
                        method={"PUT"}
                        text={"Upload"}
                    />
                </Grid>
                <Grid item xs={1}/>
            </Grid>
        </Paper>
    }
}

export default ImageFormatSettings;