import React from 'react';

import Paper from '@mui/material/Paper';
import {
    Grid,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableRow, TextField
} from "@mui/material";
import {DefaultService, pixel_mask_statistics} from "../openapi";
import Button from "@mui/material/Button";

type MyProps = {
    s?: pixel_mask_statistics
};

type MyState = {
    file?: File
};

class PixelMask extends React.Component<MyProps, MyState> {
    state: MyState = {};

    handleUpload = () => {
        if (this.state.file)
            DefaultService.putConfigUserMaskTiff(this.state.file);
    };

    handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        if (event.target.files && event.target.files[0]) {
            this.setState({file: event.target.files[0]});
        }
    };

    render() {
        return <Paper style={{textAlign: 'center'}} sx={{width: '100%'}}>
            <br/><strong>Pixel mask</strong><br/><br/>
            <Grid container spacing={0}>
                <Grid item xs={1}/>
                <Grid item xs={10}>
                    <TableContainer component={Paper} style={{marginLeft: "auto", marginRight: "auto"}}>
                        <Table size="small" aria-label="simple table">
                            <TableBody>
                                <TableRow>
                                    <TableCell component="th" scope="row"> User mask: </TableCell>
                                    <TableCell align="right">{this.props.s?.user_mask ?? "N/A"}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell component="th" scope="row"> Wrong gain: </TableCell>
                                    <TableCell align="right">{this.props.s?.wrong_gain ?? "N/A"}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell component="th" scope="row"> G0 RMS pedestal: </TableCell>
                                    <TableCell align="right">{this.props.s?.too_high_pedestal_rms ?? "N/A"}</TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
                <Grid item xs={1}/>
            </Grid>
            <br/>
            <a href="/config/mask.tiff">Download pixel mask</a><br/>
            <a href="/config/user_mask.tiff">Download user pixel mask</a><br/><br/>
            <TextField
                type="file"
                inputProps={{accept: '.tiff'}}
                onChange={this.handleFileChange}
            /><br/><br/>
            <Button onClick={this.handleUpload}
                    variant="contained"
                    disableElevation
                    disabled={!this.state.file}
                    color={"primary"}>Upload</Button>
            <br/><br/>
        </Paper>
    }
}

export default PixelMask;