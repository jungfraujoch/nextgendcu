import React, {Component} from 'react';

import Plot from "react-plotly.js";

// Not using TypeScript, as plotly is not TypeScript :(

class MultiLinePlotWrapper extends Component {
    render() {
        return <Plot
            style={{width: "100%", height: "100%"}}
            data={this.props.data}
            layout={ {
                xaxis: {title: this.props.xaxis},
                yaxis: {title: this.props.yaxis},
                uirevision: true
            } }
            config = {{responsive: true}}
        />;
    }
}

export default MultiLinePlotWrapper;