
import React from 'react';

import Paper from '@mui/material/Paper';
import {DataGrid, GridColDef} from "@mui/x-data-grid";
import {calibration_statistics, DefaultService} from "../openapi";

type MyProps = {
    s?: calibration_statistics
};

type MyState = {};

class Calibration extends React.Component<MyProps, MyState> {
    columns : GridColDef[] = [
        { field: 'module_number', type: 'number', headerName: 'Module' },
        { field: 'storage_cell_number', type: 'number', headerName: 'SC'},
        { field: 'masked_pixels', headerName: 'Masked pxls', type: 'number'},
        { field: 'pedestal_g0_mean', headerName: 'Pedestal G0', type: 'number'},
        { field: 'pedestal_g1_mean', headerName: 'Pedestal G1', type: 'number'},
        { field: 'pedestal_g2_mean', headerName: 'Pedestal G2', type: 'number'},
        { field: 'gain_g0_mean', headerName: 'Gain G0', type: 'number'},
        { field: 'gain_g1_mean', headerName: 'Gain G1', type: 'number'},
        { field: 'gain_g2_mean', headerName: 'Gain G2', type: 'number'}
    ];

    render() {
        return <Paper style={{textAlign: 'center'}} sx={{ height: 527, width: '100%' }}>
            {((this.props.s !== undefined) && (this.props.s.length > 0)) ?
            <DataGrid
                getRowId={(row) => Number(row.module_number) * 64 + Number(row.storage_cell_number)}
                rows={this.props.s}
                columns={this.columns}
                initialState={{
                    pagination: { paginationModel: { pageSize: 8 } },
                }}
                pageSizeOptions={[4,8,16]}
            /> : <div/>
            }
        </Paper>

    }
}

export default Calibration;
