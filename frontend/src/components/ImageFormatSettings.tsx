import React, {Component} from 'react';

import {Checkbox, FormControlLabel, FormGroup, Grid, List, ListItem, Stack, Switch, Tooltip} from "@mui/material";
import Button from "@mui/material/Button";
import Paper from "@mui/material/Paper";
import FormControl from "@mui/material/FormControl";
import InputLabel from "@mui/material/InputLabel";
import Select, {SelectChangeEvent} from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import {DefaultService, image_format_settings} from "../openapi";
import NumberTextField from "./NumberTextField";
import _ from "lodash";
import ButtonWithSnackbar from "./ButtonWithSnackbar";

type MyProps = {
    s?: image_format_settings
}

type MyState = {
    s: image_format_settings,
    last_downloaded_s: image_format_settings,
    bit_width_selection: string,
    sign_selection: string,
    jungfrau_conversion_factor_err: boolean,
    jungfrau_conversion_factor_old: number,
    download_counter: number,
    pedestal_g0_rms_limit_err: boolean
}

const default_image_format_settings: image_format_settings = {
    summation: true,
    geometry_transform: true,
    jungfrau_conversion: true,
    jungfrau_conversion_factor_keV: undefined,
    bit_depth_image: undefined,
    signed_output: undefined,
    mask_module_edges: true,
    mask_chip_edges: true,
    jungfrau_mask_pixels_without_g0: true,
    apply_mask: false,
    jungfrau_pedestal_g0_rms_limit: 100
};

function extractSignForSelection(input: image_format_settings) : string {
    if (input.signed_output === undefined)
        return 'a';
    else if (input.signed_output)
        return 's';
    else
        return 'u';
}

function extractDepthForSelection(input: image_format_settings) : string {
    if (input.bit_depth_image === undefined)
        return 'a';
    else if (input.bit_depth_image === image_format_settings.bit_depth_image._8)
        return '8';
    else if (input.bit_depth_image === image_format_settings.bit_depth_image._16)
        return '16';
    else if (input.bit_depth_image === image_format_settings.bit_depth_image._32)
        return '32';
    else
        return "";
}

class ImageFormatSettings extends Component<MyProps, MyState> {
    state : MyState = {
        s: default_image_format_settings,
        last_downloaded_s: default_image_format_settings,
        jungfrau_conversion_factor_err: false,
        jungfrau_conversion_factor_old: 12.4,
        bit_width_selection: "a",
        sign_selection: "a",
        download_counter: 0,
        pedestal_g0_rms_limit_err: false
    }

    raw = () => {
        DefaultService.postConfigImageFormatRaw()
            .catch(error => console.log(error) );
        this.getValues();
    }

    conv = () => {
        DefaultService.postConfigImageFormatConversion()
            .catch(error => console.log(error) );
        this.getValues();
    }

    uploadButton = () => { this.putValues(); }

    putValues = () => {
        DefaultService.putConfigImageFormat(this.state.s)
            .catch(error => console.log(error) );
    }

    getValues = () => {
        if (this.props.s !== undefined) {
            let format_set: image_format_settings = this.props.s;
            if (!_.isEqual(format_set, this.state.last_downloaded_s)) {

                this.setState(prevState => ({
                    s: format_set,
                    last_downloaded_s: format_set,
                    jungfrau_conversion_factor_err: false,
                    download_counter: prevState.download_counter + 1,
                    jungfrau_conversion_factor_old: format_set.jungfrau_conversion_factor_keV
                        ?? prevState.jungfrau_conversion_factor_old,
                    bit_width_selection: extractDepthForSelection(format_set),
                    sign_selection: extractSignForSelection(format_set)
                }));
            }
        }
    }

    componentDidMount() {
        this.getValues();
    }

    componentDidUpdate() {
        this.getValues();
    }

    handleBitWidthChange = (event: SelectChangeEvent) => {
        let val: image_format_settings.bit_depth_image|undefined = undefined;

        if (event.target.value === "a")
            val = undefined;
        else if (event.target.value === "8")
            val = image_format_settings.bit_depth_image._8;
        else if (event.target.value === "16")
            val = image_format_settings.bit_depth_image._16;
        else if (event.target.value === "32")
            val = image_format_settings.bit_depth_image._32;

        this.setState(prevState => ({
            bit_width_selection: event.target.value,
            s: {
                ...prevState.s,
                bit_depth_image: val
            }
        }));
    };

    handleSignChange = (event: SelectChangeEvent) => {
        let val: boolean|undefined = undefined;

        if (event.target.value === "a")
            val = undefined;
        else if (event.target.value === "u")
            val = false;
        else if (event.target.value === "s")
            val = true;

        this.setState(prevState => ({
            sign_selection: event.target.value,
            s: {
                ...prevState.s,
                signed_output: val
            }
        }));
    };

    render() {
        return <Paper style={{textAlign: 'center'}} sx={{width: '100%'}}>
            <br/>
            <Stack spacing={3} sx={{
                justifyContent: "center",
                alignItems: "center",
            }}>
                <strong>Output image format settings </strong>
                <FormGroup>
                    <FormControlLabel control={
                        <Checkbox checked={this.state.s.geometry_transform}
                                  onChange={
                                      (event: React.ChangeEvent<HTMLInputElement>) => {
                                          this.setState(
                                              prevState => ({
                                                  s: {
                                                      ...prevState.s,
                                                      geometry_transform: event.target.checked
                                                  }
                                              })
                                          );
                                      }}/>
                    } label="Geometry transformation"/>
                    <FormControlLabel control={
                        <Checkbox checked={this.state.s.summation}
                                  onChange={
                                      (event: React.ChangeEvent<HTMLInputElement>) => {
                                          this.setState(
                                              prevState => ({s: {...prevState.s, summation: event.target.checked}})
                                          );
                                      }}/>
                    } label="Image summation"/>
                    <FormControlLabel control={
                        <Checkbox checked={this.state.s.mask_chip_edges}
                                  onChange={
                                      (event: React.ChangeEvent<HTMLInputElement>) => {
                                          this.setState(
                                              prevState => ({
                                                  s: {
                                                      ...prevState.s,
                                                      mask_chip_edges: event.target.checked
                                                  }
                                              })
                                          );
                                      }}/>
                    } label="Mask chip edges"/>

                    <FormControlLabel control={
                        <Checkbox checked={this.state.s.mask_module_edges}
                                  onChange={
                                      (event: React.ChangeEvent<HTMLInputElement>) => {
                                          this.setState(
                                              prevState => ({
                                                  s: {
                                                      ...prevState.s,
                                                      mask_module_edges: event.target.checked
                                                  }
                                              })
                                          );
                                      }}/>
                    } label="Mask module edges"/>
                    <FormControlLabel control={
                        <Checkbox checked={this.state.s.apply_mask}
                                  onChange={
                                      (event: React.ChangeEvent<HTMLInputElement>) => {
                                          this.setState(
                                              prevState => ({
                                                  s: {
                                                      ...prevState.s,
                                                      apply_mask: event.target.checked
                                                  }
                                              })
                                          );
                                      }}/>
                    } label="Apply pixel masks on images"/>
                </FormGroup>

                <b>Output pixel value</b>
                <Stack spacing={2} direction="row">
                    <FormControl>
                        <InputLabel id="demo-simple-select-label">Bit-width</InputLabel>
                        <Select
                            sx={{width: 150}}
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            label="Detector"
                            value={this.state.bit_width_selection}
                            onChange={this.handleBitWidthChange}
                        >
                            <MenuItem value={"a"}>Auto</MenuItem>
                            <MenuItem value={"16"}>8-bit</MenuItem>
                            <MenuItem value={"16"}>16-bit</MenuItem>
                            <MenuItem value={"32"}>32-bit</MenuItem>
                        </Select>
                    </FormControl>
                    <FormControl>
                        <InputLabel id="demo-simple-select-label">Sign</InputLabel>
                        <Select
                            sx={{width: 150}}
                            labelId="demo-simple-select-label"
                            id="demo-simple-select"
                            label="Sign"
                            value={this.state.sign_selection}
                            onChange={this.handleSignChange}
                        >
                            <MenuItem value={"a"}>Auto</MenuItem>
                            <MenuItem value={"s"}>Signed</MenuItem>
                            <MenuItem value={"u"}>Unsigned</MenuItem>
                        </Select>
                    </FormControl>
                </Stack>
                <b>JUNGFRAU specific</b>
                <FormGroup>
                    <FormControlLabel control={
                        <Checkbox checked={this.state.s.jungfrau_mask_pixels_without_g0}
                                  onChange={
                                      (event: React.ChangeEvent<HTMLInputElement>) => {
                                          this.setState(
                                              prevState => ({
                                                  s: {
                                                      ...prevState.s,
                                                      jungfrau_mask_pixels_without_g0: event.target.checked
                                                  }
                                              }));
                                      }}/>
                    } label="Mask pixels not switching to G0"/>
                    <FormControlLabel control={
                        <Checkbox checked={this.state.s.jungfrau_conversion}
                                  onChange={
                                      (event: React.ChangeEvent<HTMLInputElement>) => {
                                          if (event.target.checked) {
                                              this.setState(
                                                  prevState => ({
                                                      jungfrau_conversion_factor_err: false,
                                                      s: {
                                                          ...prevState.s,
                                                          jungfrau_conversion: true,
                                                          jungfrau_conversion_factor_keV: undefined
                                                      }
                                                  }));
                                          } else {
                                              this.setState(
                                                  prevState => ({
                                                      jungfrau_conversion_factor_err: false,
                                                      s: {
                                                          ...prevState.s,
                                                          jungfrau_conversion: false,
                                                          jungfrau_conversion_factor_keV: undefined
                                                      }
                                                  }));
                                          }
                                      }}/>
                    } label="Conversion to photons"/>

                </FormGroup>
                <List>
                    <Tooltip
                        title="JUNGFRAU conversion factor allows to get different unit than 1 photon count. If not provided incident_energy is used.">
                        <ListItem>
                            <Checkbox
                                checked={this.state.s.jungfrau_conversion_factor_keV !== undefined}
                                disabled={!this.state.s.jungfrau_conversion}
                                onChange={
                                    (event: React.ChangeEvent<HTMLInputElement>) => {
                                        if (!event.target.checked)
                                            this.setState(prevState => ({
                                                download_counter: this.state.download_counter + 1,
                                                jungfrau_conversion_factor_err: false,
                                                s: {
                                                    ...prevState.s,
                                                    jungfrau_conversion_factor_keV: undefined
                                                }
                                            }));
                                        else
                                            this.setState(prevState => ({
                                                download_counter: this.state.download_counter + 1,
                                                jungfrau_conversion_factor_err: false,
                                                s: {
                                                    ...prevState.s,
                                                    jungfrau_conversion_factor_keV: this.state.jungfrau_conversion_factor_old
                                                }
                                            }));
                                    }}
                            />
                            <FormControl fullWidth>
                                <NumberTextField
                                    start_val={this.state.s.jungfrau_conversion_factor_keV}
                                    disabled={this.state.s.jungfrau_conversion_factor_keV === undefined}
                                    label={"JUNGFRAU conversion factor"}
                                    units={"keV"}
                                    float={true}
                                    min={0.001}
                                    fullWidth
                                    counter={this.state.download_counter}
                                    callback={(val: number, err: boolean) => {
                                        let old: number = this.state.jungfrau_conversion_factor_old;
                                        if (!err)
                                            old = val;
                                        this.setState(prevState => ({
                                            jungfrau_conversion_factor_err: err,
                                            jungfrau_conversion_factor_old: old,
                                            s: {...prevState.s, jungfrau_conversion_factor_keV: val}
                                        }));
                                    }}/>
                            </FormControl>

                        </ListItem>
                    </Tooltip>
                </List>
                    <NumberTextField start_val={this.state.s.jungfrau_pedestal_g0_rms_limit}
                                     label={"G0 max RMS"}
                                     min={0}
                                     default={100}
                                     counter={this.state.download_counter}
                                     callback={(val: number, err: boolean) => {
                                         this.setState(prevState => ({
                                             pedestal_g0_rms_limit_err: err,
                                             s: {...prevState.s, jungfrau_pedestal_g0_rms_limit: val}
                                         }));
                                     }}
                    />

                <ButtonWithSnackbar
                    path={"/config/image_format"}
                    color={"primary"}
                    method={"PUT"}
                    disabled={this.state.jungfrau_conversion_factor_err}
                    text={"Upload"}
                    input={JSON.stringify(this.state.s)}
                />

            </Stack>
            <br/>
        </Paper>
    }
}

export default ImageFormatSettings;