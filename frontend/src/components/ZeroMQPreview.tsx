import React from 'react';

import Paper from '@mui/material/Paper';
import {FormControlLabel, Radio, RadioGroup, Stack} from "@mui/material";
import NumberTextField from "./NumberTextField";
import {zeromq_preview_settings} from "../openapi";
import _ from "lodash";
import FormControl from "@mui/material/FormControl";
import ButtonWithSnackbar from "./ButtonWithSnackbar";

type MyProps = {
   s?: zeromq_preview_settings
};

type MyState = {
    s: zeromq_preview_settings,
    last_downloaded_s: zeromq_preview_settings,
    period_err: boolean,
    download_counter: number,
    freq_choice: string,
    period_ms: number
};

const default_zeromq_preview_settings : zeromq_preview_settings = {
    socket_address: "",
    enabled: true,
    period_ms: 1000
}

class ZeroMQPreview extends React.Component<MyProps, MyState> {
    state : MyState = {
        s: default_zeromq_preview_settings,
        last_downloaded_s: default_zeromq_preview_settings,
        period_err: false,
        download_counter: 0,
        freq_choice: "custom",
        period_ms: default_zeromq_preview_settings.period_ms
    }

    update_freq = (freq_choice: string) => {
        switch (freq_choice) {
            case "custom":
                this.setState(prevState => ({
                    s: {...prevState.s, enabled: true, period_ms: this.state.period_ms},
                    freq_choice: "custom"
                }));
                break;
            case "all":
                this.setState(prevState => ({
                    s: {...prevState.s, enabled: true, period_ms: 0},
                    freq_choice: "all"
                }));
                break;
            default:
                this.setState(prevState => ({
                    s: {...prevState.s, enabled: false},
                    freq_choice: "none"
                }));
                break;
        }
    }

    getValues = () => {
        if (this.props.s !== undefined) {
            let format_set: zeromq_preview_settings = this.props.s;
            if (!_.isEqual(format_set, this.state.last_downloaded_s)) {
                let x : number = this.state.period_ms;
                let choice : string = "none";
                if (this.props.s.enabled) {
                    if (this.props.s.period_ms === 0)
                        choice = "all";
                    else {
                        choice = "custom";
                        x = this.props.s.period_ms;
                    }
                }

                this.setState(prevState => ({
                    s: format_set,
                    last_downloaded_s: format_set,
                    download_counter: prevState.download_counter + 1,
                    period_err: false,
                    freq_choice: choice,
                    period_ms: x
                }));
            }
        }
    }

    componentDidMount() {
        this.getValues();
    }

    componentDidUpdate() {
        this.getValues();
    }

    empty() : boolean {
        return (this.state.s.socket_address === undefined) || (this.state.s.socket_address === "");
    }

    render() {
        return <Paper style={{textAlign: 'center'}} sx={{ width: '100%'}}>
            <br/>
            <Stack spacing={5} sx={{
                justifyContent: "center",
                alignItems: "center",
            }}>
                <div><strong>ZeroMQ preview settings </strong></div>
                    <div>
                        {this.empty() ? "No preview available" : `ZeroMQ socket: ${this.state.s.socket_address}`}
                    </div>
                <FormControl>
                    <RadioGroup
                        value={this.state.freq_choice}
                        onChange={(event) => {this.update_freq(event.target.value)}}
                    >
                        <FormControlLabel value="none" control={<Radio />} label="None" />
                        <FormControlLabel value="all" control={<Radio />} label="All images" />
                        <FormControlLabel value="custom" control={<Radio />} label="Frequency" />
                    </RadioGroup>
                </FormControl>
                <NumberTextField
                    default={1000}
                    start_val={this.state.period_ms}
                    label={"Period"}
                    min={0}
                    units={"ms"}
                    counter={this.state.download_counter}
                    callback={(val: number, err: boolean) => {
                        this.setState(prevState => ({
                            s: {...prevState.s, period_ms: val},
                            period_err: err,
                            period_ms: val
                        }));
                    }}
                    disabled={this.state.freq_choice != "custom"}
                    fullWidth/>
                <ButtonWithSnackbar
                    color={"primary"}
                    path={"/config/zeromq_preview"}
                    input={JSON.stringify(this.state.s)}
                    method={"PUT"}
                    text={"Upload"}
                    disabled={((this.state.freq_choice == "custom") && this.state.period_err)}
                />
            </Stack>
            <br/>
        </Paper>
    }
}

export default ZeroMQPreview;
