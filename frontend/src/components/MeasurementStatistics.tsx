import React, {Component} from 'react';

import Paper from '@mui/material/Paper';
import {Grid, Table, TableBody, TableCell, TableContainer, TableRow,} from "@mui/material";
import {DefaultService, measurement_statistics} from "../openapi";

type MyProps = {
    s: measurement_statistics,
};

type MyState = {};

class MeasurementStatistics extends Component<MyProps, MyState> {
 
    render() {
        return <Paper style={{textAlign: 'center'}} sx={{ height: 550, width: '100%' }}>
            <Grid container spacing={0}>
                <Grid item xs={1}/>
                <Grid item xs={10}>
                    <br/><strong>Measurement statistics</strong><br/><br/>

                    <TableContainer component={Paper} style={{marginLeft: "auto", marginRight: "auto"}}>
                        <Table size="small" aria-label="simple table">
                            <TableBody>
                                <TableRow>
                                    <TableCell component="th" scope="row"> File prefix: </TableCell>
                                    <TableCell align="right">{(this.props.s.file_prefix !== undefined) ? this.props.s.file_prefix : "(images not written)"}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell component="th" scope="row"> Run number: </TableCell>
                                    <TableCell align="right">{(this.props.s.run_number !== undefined) ? this.props.s.run_number : "(not set)"}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell component="th" scope="row"> Experiment group: </TableCell>
                                    <TableCell align="right">{this.props.s.experiment_group}</TableCell>
                                </TableRow>

                                <TableRow>
                                    <TableCell component="th" scope="row"> Images expected: </TableCell>
                                    <TableCell align="right">{this.props.s.images_expected}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell component="th" scope="row"> Images collected: </TableCell>
                                    <TableCell align="right">{this.props.s.images_collected}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell component="th" scope="row"> Images sent to writer: </TableCell>
                                    <TableCell align="right">{this.props.s.images_sent}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell component="th" scope="row"> Images discarded by lossy compression: </TableCell>
                                    <TableCell align="right">{this.props.s.images_discarded_lossy_compression}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell component="th" scope="row"> Compression ratio: </TableCell>
                                    <TableCell align="right">{(this.props.s.compression_ratio !== undefined)
                                        ? (this.props.s.compression_ratio.toFixed(1)) + "x" : "-" } </TableCell>

                                </TableRow>
                                <TableRow>
                                    <TableCell component="th" scope="row"> Data acquisition efficiency: </TableCell>
                                    <TableCell align="right">{(this.props.s.collection_efficiency !== undefined)
                                        ? (Math.floor(this.props.s.collection_efficiency * 1000.0) / 1000.0).toFixed(3) : "-"}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell component="th" scope="row"> Indexing rate: </TableCell>
                                    <TableCell align="right">{(this.props.s.indexing_rate !== undefined)
                                        ? this.props.s.indexing_rate.toFixed(2) : "-"}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell component="th" scope="row"> Background estimate: </TableCell>
                                    <TableCell align="right">{(this.props.s.bkg_estimate !== undefined)
                                        ? this.props.s.bkg_estimate.toPrecision(7) : "-"}</TableCell>
                                </TableRow>
                                <TableRow>
                                    <TableCell component="th" scope="row"> Unit cell: </TableCell>
                                    <TableCell align="right">{this.props.s.unit_cell}</TableCell>
                                </TableRow>
                            </TableBody>
                        </Table>
                    </TableContainer>
                    <br/>
                </Grid>
                <Grid item xs={1}/>
            </Grid>
        </Paper>
    }
}

export default MeasurementStatistics;