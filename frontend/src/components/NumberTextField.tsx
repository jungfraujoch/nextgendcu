import React, {Component} from 'react';

import {
    InputAdornment, SxProps,
    TextField, Theme
} from "@mui/material";

type MyProps = {
    start_val?: number,
    default?: number,
    label: string,
    callback: (val: number, err: boolean) => void,
    counter?: number,
    min?: number,
    max?: number,
    sx?: SxProps<Theme>,
    units?: string,
    disabled?: boolean,
    fullWidth?: boolean,
    float?: boolean
}

type MyState = {
    text: string,
    err: boolean,
    val: number
}

class NumberTextField extends Component<MyProps, MyState> {
    state : MyState = {
        text: ".",
        err: true,
        val: 0
    }

    setup() {
        let val : number = 0;
        if (this.props.start_val !== undefined)
            val = this.props.start_val;
        else if (this.props.default !== undefined)
            val = this.props.default;

        if (this.props.float !== true)
            val = Math.round(val);
        else
            val = Math.round(val * 1000.0) / 1000.0

        this.setState({
            err: false,
            val: val,
            text: val.toString()
        });
    }

    componentDidMount() {
        this.setup();
    }

    componentDidUpdate(prevProps: Readonly<MyProps>, prevState: Readonly<MyState>, snapshot?: any) {
        // Something has to change in parent function to trigger setup => otherwise this makes infinite loop
        // In case of error state start_val could stay same, but we want to return to "safe" set
        // so there is external counter that if changed reset is triggered
        if ((prevProps.counter !== this.props.counter) || (prevProps.start_val !== this.props.start_val))
            this.setup();
    }

    updateValue = (event: React.ChangeEvent<HTMLInputElement>) => {
        let text = event.target.value;
        if (!text) text = "0";

        let num_val : number = Number(text);

        let err : boolean = !Number.isFinite(num_val)
            || (!this.props.float && !Number.isInteger(num_val))
            || ((this.props.min !== undefined) && (num_val < this.props.min))
            || ((this.props.max !== undefined) && (num_val > this.props.max));

        // If error I keep old numeric value to return (so there is always a proper value kept)
        let new_val : number = err ? this.state.val : num_val;
        this.setState({err: err, val: new_val, text: text});
        this.props.callback(new_val, err);
    }

    render() {
        return <TextField id="frame_time" label={this.props.label} variant="outlined"
                          sx = {this.props.sx}
                          error={this.state.err}
                          onChange={this.updateValue}
                          value={this.state.text}
                          disabled={this.props.disabled}
                          InputProps={{
                              inputProps: {
                                  style: {textAlign: 'right'}
                              },
                              endAdornment: this.props.units !== undefined ?
                                  (this.props.units === "us" ?
                                      <InputAdornment position="end">&micro;s</InputAdornment> :
                                      <InputAdornment position="end">{this.props.units}</InputAdornment>)
                                  : null
                          }}/>
    }
}

export default NumberTextField;