import React from 'react';

import Paper from '@mui/material/Paper';
import {
    DefaultService,
    instrument_metadata,
    pixel_mask_statistics,
    roi_box,
    roi_circle,
    roi_definitions
} from "../openapi";
import {Grid, Typography} from "@mui/material";
import Button from "@mui/material/Button";
import {DataGrid, GridActionsCellItem, GridColDef, GridRowModel} from "@mui/x-data-grid";
import DeleteIcon from '@mui/icons-material/DeleteOutlined';
import ErrorIcon from '@mui/icons-material/Error';
import ButtonWithSnackbar from "./ButtonWithSnackbar";
import _ from "lodash";

type MyProps = {
    s?: roi_definitions
};

type BoxWrapper = roi_box & {id: number};
type CircleWrapper = roi_circle & {id: number};

type MyState = {
    last_downloaded_s: roi_definitions;
    box: BoxWrapper[];
    circle: CircleWrapper[];
    box_err?: string;
    circle_err?: string;
};

function getRandomInt(max : number) {
    return Math.floor(Math.random() * max);
}

class ROI extends React.Component<MyProps, MyState> {
    counter: number = 0;

    state : MyState = {
        last_downloaded_s: {
            box: {rois: []},
            circle: {rois: []}
        },
        box: [],
        circle: []
    }

    getValues () {
        if (this.props.s !== undefined) {
            let tmp: roi_definitions = this.props.s;
            if (!_.isEqual(tmp, this.state.last_downloaded_s))
                this.setState({
                    circle_err: undefined,
                    circle: (this.props.s.circle.rois === undefined) ? [] :
                        this.props.s.circle.rois.map(
                            (elem) : CircleWrapper => {return {id: this.counter++, ...elem}}
                        ),
                    box_err: undefined,
                    box: (this.props.s.box.rois === undefined) ? [] :
                        this.props.s.box.rois.map(
                            (elem) : BoxWrapper => {return {id: this.counter++, ...elem}}
                        ),
                    last_downloaded_s: tmp
                });
        }
    }

    componentDidMount() {
        this.getValues();
    }

    componentDidUpdate() {
        this.getValues();
    }

    checkDuplicatesBox(input : BoxWrapper[]) {
        let roi_names : string[] = [];
        input.map(element => roi_names.push(element.name));
        this.state.circle.map(element => roi_names.push(element.name));
        const duplicates = roi_names.filter((item, index) => roi_names.indexOf(item) !== index);
        return (duplicates.length !== 0);
    }

    checkDuplicatesCircle(input : CircleWrapper[]) {
        let roi_names : string[] = [];
        input.map(element => roi_names.push(element.name));
        this.state.box?.map(element => roi_names.push(element.name));
        const duplicates = roi_names.filter((item, index) => roi_names.indexOf(item) !== index);
        return (duplicates.length !== 0);
    }

    validateBoxInteger(input: BoxWrapper[]) {
        return input.every((x) : boolean =>
            Number.isInteger(x.min_x_pxl) && Number.isInteger(x.max_x_pxl) &&
            Number.isInteger(x.min_y_pxl) && Number.isInteger(x.max_y_pxl));
    }

    validateBoxBounds(input: BoxWrapper[]) {
        return input.every((x) : boolean =>
            (x.min_x_pxl >= 0) && (x.max_x_pxl >= x.min_x_pxl) &&
            (x.min_y_pxl >= 0) && (x.max_y_pxl >= x.min_y_pxl));
    }

    validateCircleValues(input: CircleWrapper[]) {
        return input.every((x) : boolean => x.radius_pxl >= 0);
    }

    validateBox (input:BoxWrapper[]) : undefined | string {
        if (this.checkDuplicatesBox(input))
            return "Duplicate ROI names.";
        if (!this.validateBoxInteger(input))
            return "Box bounds are not integer values.";
        if (!this.validateBoxBounds(input))
            return "Box bounds are not correct (0 <= min <= max)";
        return undefined;
    }

    validateCircle (input:CircleWrapper[]) : undefined | string {
        if (this.checkDuplicatesCircle(input))
            return "Duplicate ROI names.";
        if (!this.validateCircleValues(input))
            return "Circle ROI radius must be positive.";
        return undefined;
    }

    uploadButton = () => { this.putValues(); }
    downloadButton = () => { this.getValues(); }

    addBoxButton = () => {
        this.setState({
            box: [...this.state.box,
                {
                    id: this.counter++,
                    name: "roi" + getRandomInt(99999),
                    min_x_pxl: 0,
                    max_x_pxl: 0,
                    min_y_pxl: 0,
                    max_y_pxl: 0
                }
            ]
        });
    }

    addCircleButton = () => {
        this.setState({
            circle: [
                ...this.state.circle,
                {
                    id: this.counter++,
                    name: "roi" + getRandomInt(65536),
                    center_x_pxl: 0,
                    center_y_pxl: 0,
                    radius_pxl: 10
                }
            ]
        });
    }

    roiStruct = () : roi_definitions => {
        return {
            box: {
                rois: this.state.box.map((elem): roi_box => elem)
            },
            circle: {
                rois: this.state.circle.map((elem): roi_circle => elem)
            }
        }
    }

    putValues = () => {
        DefaultService.putConfigRoi(this.roiStruct()).catch(error => {} );
    }



    handleDeleteBoxROIClick = (id: GridRowModel) => () => {
        let new_state : BoxWrapper[] = this.state.box.filter((row) => row.id !== id.id);
        let err = this.validateBox(new_state);
        this.setState({box_err: err, box: new_state});
    };

    handleDeleteCircleROIClick = (id: GridRowModel) => () => {
        let new_state : CircleWrapper[] = this.state.circle.filter((row) => row.id !== id.id);
        let err = this.validateCircle(new_state);
        this.setState({circle_err: err, circle: new_state});
    };

    processRowBoxUpdate = (newRow: BoxWrapper, oldRow: BoxWrapper) => {
        let new_state = this.state.box.map((item) => item.id === oldRow.id ? newRow : item);
        let err = this.validateBox(new_state);
        this.setState({ box_err: err, box: new_state });
        return newRow;
    };

    processRowCircleUpdate = (newRow: CircleWrapper, oldRow: CircleWrapper) => {
        let new_state = this.state.circle.map((item) => item === oldRow ? newRow : item);
        let err = this.validateCircle(new_state);
        this.setState({ circle_err: err, circle: new_state });
        return newRow;
    };

    columns_box : GridColDef[] = [
        { field: 'name', type: 'string', headerName: 'Name' , editable: true, width: 200},
        { field: 'min_x_pxl', type: 'number', headerName: 'Min X', editable: true},
        { field: 'max_x_pxl', type: 'number', headerName: 'Max X', editable: true},
        { field: 'min_y_pxl', type: 'number', headerName: 'Min Y', editable: true},
        { field: 'max_y_pxl', type: 'number', headerName: 'Max Y', editable: true},
        { field: 'actions', type: 'actions', width: 50, cellClassName: 'actions',
            getActions: (id : GridRowModel) => {
                return [
                    <GridActionsCellItem
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={this.handleDeleteBoxROIClick(id)}
                        color="inherit"
                    />,
                ];
            }}
    ];

    columns_circle : GridColDef[] = [
        { field: 'name', type: 'string', headerName: 'Name' , editable: true, width: 200},
        { field: 'center_x_pxl', type: 'number', headerName: 'X [pxl]', editable: true},
        { field: 'center_y_pxl', type: 'number', headerName: 'Y [pxl]', editable: true},
        { field: 'radius_pxl', type: 'number', headerName: 'Radius [pxl]', editable: true},
        { field: 'actions', type: 'actions', width: 50, cellClassName: 'actions',
            getActions: (id : GridRowModel) => {
                return [
                    <GridActionsCellItem
                        icon={<DeleteIcon />}
                        label="Delete"
                        onClick={this.handleDeleteCircleROIClick(id)}
                        color="inherit"
                    />,
                ];
            }}
    ];

    render_err(msg?: string) : JSX.Element {
        if (msg === undefined)
            return <div><br/><br/></div>
        return <div><br/><Typography color="error"><ErrorIcon color="error"/>&nbsp;{msg}</Typography><br/></div>;
    }

    render() {
        return <Paper style={{textAlign: 'center'}} sx={{ height: 530, width: '100%' }}>
            <br/>
            <Grid container spacing={0}>
                <Grid item xs={0.25}/>
                <Grid item xs={5.5} >
                    <strong>Box ROI</strong>
                    <br/><br/><br/>
                    <Paper sx={{height: 300, width: '100%'}} elevation={0}>
                        <DataGrid
                            getRowId={row => row.id}
                            rows={this.state.box}
                            columns={this.columns_box}
                            editMode={"row"}
                            processRowUpdate={this.processRowBoxUpdate}
                        />
                        {this.render_err(this.state.box_err)}
                    </Paper>
                </Grid>
                <Grid item xs={0.5}/>
                <Grid item xs={5.5}>
                    <strong>Radial ROI</strong>
                    <br/><br/><br/>

                    <Paper sx={{height: 300, width: '100%'}} elevation={0}>
                        <DataGrid
                            getRowId={row => row.id}
                            rows={this.state.circle}
                            columns={this.columns_circle}
                            editMode={"row"}
                            processRowUpdate={this.processRowCircleUpdate}
                        />{this.render_err(this.state.circle_err)}
                    </Paper>
                </Grid>
                <Grid item xs={0.25}/>
                <Grid item xs={1}/>
                <Grid item xs={10}><br/><br/><br/>
                    <Button color="secondary" onClick={this.addBoxButton} variant="contained"
                            disableElevation>Add Box ROI</Button>&nbsp;&nbsp;
                    <Button color="secondary" onClick={this.addCircleButton} variant="contained"
                            disableElevation>Add Circle ROI</Button>&nbsp;&nbsp;
                    <ButtonWithSnackbar
                        path={"/config/roi"}
                        color={"primary"}
                        method={"PUT"}
                        disabled={(this.state.box_err !== undefined) || (this.state.circle_err !== undefined)}
                        text={"Upload"}
                        input={JSON.stringify(this.roiStruct())}
                    />
                    <br/><br/>

                </Grid>
                <Grid item xs={1}/>
            </Grid>
        </Paper>
    }
}

export default ROI;
