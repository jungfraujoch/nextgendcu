import React from 'react';

import Paper from '@mui/material/Paper';
import {Checkbox, FormControlLabel, Stack, FormGroup, Select} from "@mui/material";
import {file_writer_settings} from "../openapi";
import _ from "lodash";
import ButtonWithSnackbar from "./ButtonWithSnackbar";
import MenuItem from "@mui/material/MenuItem";

type MyProps = {
    s?: file_writer_settings
};

type MyState = {
    s: file_writer_settings,
    last_downloaded_s: file_writer_settings,
    download_counter: number
};

const default_file_writer_settings : file_writer_settings = {
    overwrite: false,
    file_writer_version: 2
}

class FileWriterSettings extends React.Component<MyProps, MyState> {
    state : MyState = {
        s: default_file_writer_settings,
        last_downloaded_s: default_file_writer_settings,
        download_counter: 0,
    }


    getValues = () => {
        if (this.props.s !== undefined) {
            let format_set : file_writer_settings = this.props.s;
            if (!_.isEqual(format_set, this.state.last_downloaded_s)) {
                this.setState(prevState => ({
                    s: format_set,
                    last_downloaded_s: format_set,
                    download_counter: prevState.download_counter + 1,
                }));
            }
        }
    }

    componentDidMount() {
        this.getValues();
    }

    componentDidUpdate() {
        this.getValues();
    }

    render() {
        return <Paper style={{textAlign: 'center'}} sx={{ width: '100%'}}>
            <br/>
            <Stack spacing={5} sx={{
                justifyContent: "center",
                alignItems: "center"
            }}>
                <div><strong>File Writer settings </strong></div>
                <FormGroup>
                    <FormControlLabel
                        control={
                            <Checkbox
                                checked={this.state.s.overwrite}
                                onChange={(event) => {
                                    this.setState(prevState => ({
                                        s: {
                                            ...prevState.s,
                                            overwrite: event.target.checked
                                        }
                                    }));
                                }}
                            />
                        }
                        label="Overwrite existing files"
                    />
                </FormGroup>
                    <Select
                        value={this.state.s.file_writer_version}
                        onChange={(event) => {

                            const selectedVersion = typeof event.target.value === 'number'
                                ? event.target.value
                                : parseInt(event.target.value, 10);
                            this.setState(prevState => ({
                                s: {
                                    ...prevState.s,
                                    file_writer_version: selectedVersion
                                }
                            }));
                        }}
                        sx = {{width: "80%"}}
                    >
                        <MenuItem value={1}>HDF5 master files with soft links</MenuItem>
                        <MenuItem value={2}>HDF5 master files with virtual datasets</MenuItem>
                    </Select>

                <ButtonWithSnackbar
                    color={"primary"}
                    path={"/config/file_writer"}
                    input={JSON.stringify(this.state.s)}
                    method={"PUT"}
                    text={"Upload"}
                />
            </Stack>
            <br/>
        </Paper>
    }
}

export default FileWriterSettings;
