import React, {Component} from 'react';

import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import {broker_status} from "../openapi";
import ButtonWithSnackbar from "./ButtonWithSnackbar";
import state = broker_status.state;

type MyProps = {
    s?: broker_status
}

type MyState = {}

function FormatNumber(x: number) : string {
    if (x === undefined)
        return "";
    else if (x === null)
        return "";
    else
        return x.toFixed(1);
}

class StatusBar extends Component<MyProps, MyState> {

    statusDescription() : JSX.Element {
        if (this.props.s === undefined)
            return <>Not connected</>;
        else
            return <div>
                State: {this.props.s.state.toString()}
                {(this.props.s.progress !== undefined) ? " (" + FormatNumber(this.props.s.progress * 100.0) + " %)" : ""}
            </div>
    }

    render() {
        return <AppBar>
            <Toolbar>
                <Typography variant="h6" style={{flexGrow: 0.5}}>
                    PSI Jungfraujoch
                </Typography>

                <Typography variant="h6" style={{flexGrow: 2.0}}>
                    {this.statusDescription()}
                </Typography>

                <Typography variant="h6" style={{flexGrow: 2.0}}>
                </Typography>
                <ButtonWithSnackbar
                    text={"Pedestal"}
                    path={"/pedestal"}
                    color={"secondary"}
                    disabled={(this.props.s === undefined) || (this.props.s.state !== state.IDLE)}
                />&nbsp;&nbsp;
                <ButtonWithSnackbar
                    text={"Cancel"}
                    path={"/cancel"}
                    color={"secondary"}
                />&nbsp;&nbsp;
                <ButtonWithSnackbar
                    text={"Initialize"}
                    path={"/initialize"}
                    color={"secondary"}
                />
            </Toolbar>
        </AppBar>
    }
}

export default StatusBar;
