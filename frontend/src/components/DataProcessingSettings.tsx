import React, {Component} from 'react';

import Paper from '@mui/material/Paper';
import {Grid, Slider, Switch, Typography} from "@mui/material";
import {DefaultService, spot_finding_settings} from "../openapi";

type MyProps = {
    s?: spot_finding_settings,
    update: () => void
};

type MyState = {
    s: spot_finding_settings
}

class DataProcessingSettings extends Component<MyProps, MyState> {
    state : MyState = {
        s: {
            enable: true,
            indexing: true,
            filter_powder_rings: false,
            min_spot_count_powder_ring: 20,
            photon_count_threshold: 8,
            signal_to_noise_threshold: 3.0,
            min_pix_per_spot: 2,
            max_pix_per_spot: 50,
            high_resolution_limit: 2.5,
            low_resolution_limit: 20.0,
            indexing_tolerance: 0.05
        }
    }

    putValues(x: spot_finding_settings) {
        DefaultService.putConfigSpotFinding(x)
            .catch(error => console.log(error) );
    }

    getValues() {
        if (this.props.s !== undefined)
            this.setState({s: this.props.s});
    }

    componentDidMount() {
        this.getValues();
    }

    componentDidUpdate(prevProps: Readonly<MyProps>) {
        if ((this.props.s !== undefined) && (prevProps.s != this.props.s))
            this.setState({s: this.props.s});
    }

    setPhotonCountThreshold = (event: Event, newValue: number | number[]) => {
        this.setState(prevState => ({s: {...prevState.s, photon_count_threshold: newValue as number}}),
            () => {this.putValues(this.state.s);});
        this.props.update();
    }

    setSignalToNoiseThreshold = (event: Event, newValue: number | number[]) => {
        this.setState(prevState => ({s: {...prevState.s, signal_to_noise_threshold: newValue as number}}),
            () => {this.putValues(this.state.s);});
        this.props.update();
    }

    setMinPixPerSpot = (event: Event, newValue: number | number[]) => {
        this.setState(prevState => ({s: {...prevState.s, min_pix_per_spot: newValue as number}}),
            () => {this.putValues(this.state.s);});
        this.props.update();
    }

    setIndexingTolerance = (event: Event, newValue: number | number[]) => {
        this.setState(prevState => ({s: {...prevState.s, indexing_tolerance: newValue as number}}),
            () => {this.putValues(this.state.s);});
        this.props.update();
    }

    setHighResolutionLimit = (event: Event, newValue: number | number[]) => {
        this.setState(prevState => ({s: {...prevState.s, high_resolution_limit: newValue as number}}),
             () => {this.putValues(this.state.s);});
        this.props.update();
    }

    enableSpotFindingToggle = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState(prevState => ({s: {...prevState.s, enable: event.target.checked}}),
            () => {this.putValues(this.state.s);});
        this.props.update();
    }

    enableIndexingToggle = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState(prevState => ({s: {...prevState.s, indexing: event.target.checked}}),
            () => {this.putValues(this.state.s);});
        this.props.update();
    }

    enableFilterPowderRingToggle = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState(prevState => ({s: {...prevState.s, filter_powder_rings: event.target.checked}}),
            () => {this.putValues(this.state.s);});
        this.props.update();
    }

    setMinSpotCountPowderRing = (event: Event, newValue: number | number[]) => {
        this.setState(prevState => ({s: {...prevState.s, min_spot_count_powder_ring: newValue as number}}),
            () => {this.putValues(this.state.s);});
        this.props.update();
    }

    render() {
        return <Paper style={{textAlign: 'center'}} sx={{ height: 700, width: '100%' }}>
                    <Grid container spacing={0}>

                        <Grid item xs={1}/>
                        <Grid item xs={10}>
                            <br/><strong>Spot finding parameters</strong><br/><br/>
                            <Switch onChange={this.enableSpotFindingToggle} checked={this.state.s.enable}/>
                            Spot finding
                            <br/><br/>

                            <Typography gutterBottom> Count threshold </Typography>
                            <Slider disabled={!this.state.s.enable}
                                    value={Number(this.state.s.photon_count_threshold)}
                                    onChange={this.setPhotonCountThreshold}
                                    min={1} max={50} step={1} valueLabelDisplay="auto"/>

                            <br/><Typography> Signal-to-noise threshold </Typography>
                            <Slider disabled={!this.state.s.enable}
                                    value={Number(this.state.s.signal_to_noise_threshold)}
                                    onChange={this.setSignalToNoiseThreshold}
                                    min={2}
                                    max={10}
                                    step={0.5}
                                    valueLabelDisplay="auto"
                                    valueLabelFormat={(value) => value.toFixed(1)}
                            />

                            <br/><Typography> Minimum pixel / spot </Typography>
                            <Slider disabled={!this.state.s.enable}
                                    value={Number(this.state.s.min_pix_per_spot)}
                                    onChange={this.setMinPixPerSpot}
                                    min={1} max={8} step={1} valueLabelDisplay="auto"/>
                            <Typography> High resolution limit [&#8491;] </Typography>
                            <Slider disabled={!this.state.s.enable}
                                    value={Number(this.state.s.high_resolution_limit)}
                                    onChange={this.setHighResolutionLimit}
                                    min={1} max={5} step={0.2} valueLabelDisplay="auto"
                                    valueLabelFormat={(value) => value.toFixed(1)}
                            />
                            <br/><br/>
                            <Switch onChange={this.enableFilterPowderRingToggle}
                                    checked={this.state.s.filter_powder_rings}
                                    disabled={!this.state.s.enable}/>
                            Filter spots in powder rings (<i>e.g.</i>, ice)
                            <br/><br/>
                            <Typography> Min spots to filter powder ring </Typography>
                            <Slider disabled={!this.state.s.enable || !this.state.s.filter_powder_rings}
                                    value={Number(this.state.s.filter_powder_rings)}
                                    onChange={this.setMinSpotCountPowderRing}
                                    min={5} max={50} step={1} valueLabelDisplay="auto"/>
                            <br/> <br/>
                            <Switch onChange={this.enableIndexingToggle} checked={this.state.s.indexing}
                                    disabled={!this.state.s.enable}/>
                            Indexing <br/><br/>
                            <Typography> Indexing spot acceptance tolerance </Typography>
                            <Slider disabled={!this.state.s.enable || !this.state.s.indexing}
                                    value={Number(this.state.s.indexing_tolerance)}
                                    onChange={this.setIndexingTolerance}
                                    min={0.0} max={0.3} step={0.01} valueLabelDisplay="auto"
                                    valueLabelFormat={(value) => value.toFixed(2)}
                            />
                        </Grid>
                        <Grid item xs={1}/>
                    </Grid>
                </Paper>
    }
}

export default DataProcessingSettings;