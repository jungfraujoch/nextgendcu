import React, {Component} from 'react';

import Paper from '@mui/material/Paper';
import {Stack, Table, TableBody, TableCell, TableContainer, TableRow,} from "@mui/material";
import {detector_status, detector_state, detector_power_state} from "../openapi";

type MyProps = {
    s?: detector_status
}

type MyState = {}

function powerchipToString(s : detector_status) : string {
    switch (s.powerchip) {
        case detector_power_state.POWER_ON:
            return "On";
        case detector_power_state.POWER_OFF:
            return "Off";
        case detector_power_state.PARTIAL:
            return "Partially on";
    }
}

function reduce_array(arr: number[], unit: string) : string {
    if (arr.length == 0)
        return "N/A";
    
    if (arr.every(val => val === arr[0]))
        return arr[0].toString() + " " + unit;
    else {
        let min: number = Math.min(...arr);
        let max: number = Math.max(...arr);
        return `${min} - ${max} ${unit}` ;
    }
}

class DetectorStatus extends Component<MyProps, MyState> {
    det() : detector_status {
        if (this.props.s === undefined)
            return {
                state: detector_state.NOT_CONNECTED,
                powerchip: detector_power_state.POWER_OFF,
                server_version: "N/A",
                number_of_triggers_left: 0,
                fpga_temp_degC: [],
                high_voltage_V: []
            }
        else
            return this.props.s;
    }

    render() {
        return <Paper style={{textAlign: 'center'}} sx={{width: '100%'}}>
            <br/>
            <Stack spacing={3} sx={{
                justifyContent: "center",
                alignItems: "center"
            }}>

                <strong>Detector status</strong>

                <TableContainer component={Paper}
                                style={{marginLeft: "auto", marginRight: "auto"}}
                                sx={{width: '80%'}}
                >
                    <Table size="small" aria-label="simple table">
                        <TableBody>
                            <TableRow>
                                <TableCell component="th" scope="row"> Detector state: </TableCell>
                                <TableCell align="right">{(this.det().state.toString())}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell component="th" scope="row"> Detector ASIC power: </TableCell>
                                <TableCell align="right">{powerchipToString(this.det())}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell component="th" scope="row"> Triggers remaining: </TableCell>
                                <TableCell align="right">{this.det().number_of_triggers_left}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell component="th" scope="row"> FPGA temp.erature: </TableCell>
                                <TableCell align="right">{reduce_array(this.det().fpga_temp_degC, "degC")}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell component="th" scope="row"> High voltage: </TableCell>
                                <TableCell align="right">{reduce_array(this.det().high_voltage_V, "V")}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell component="th" scope="row"> Detector server version: </TableCell>
                                <TableCell align="right">{this.det().server_version} </TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </TableContainer>

            </Stack>
            <br/>
        </Paper>
    }
}

export default DetectorStatus;