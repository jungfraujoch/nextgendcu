import React, {Component} from 'react';

import Paper from '@mui/material/Paper';
import {Box, Grid} from "@mui/material";
import DataProcessingPlot, {PlotType} from "./DataProcessingPlot";
import Toolbar from "@mui/material/Toolbar";
import MenuItem from "@mui/material/MenuItem";
import FormControl from "@mui/material/FormControl";
import Select, {SelectChangeEvent} from "@mui/material/Select";

type MyProps = {
    type: PlotType,
    default_tab: string,
    height: number
}

type MyState = {
    type: PlotType,
    binning: string,
    tab : String
}

class DataProcessingPlots extends Component<MyProps, MyState> {
    state: MyState = {
        type: this.props.type,
        binning: "0",
        tab: this.props.default_tab
    }

    plotTypeChange = (event : SelectChangeEvent<String>) => {
        let val = String(event.target.value).toString();
        this.setState({tab: val});
        switch (val) {
            case "1":
                this.setState({type: PlotType.INDEXING_RATE});
                break;
            case "2":
                this.setState({type: PlotType.SPOT_COUNT });
                break;
            case "3":
                this.setState({type: PlotType.AZIM_INT});
                break;
            case "4":
                this.setState({type: PlotType.INDEXING_UNIT_CELL});
                break;
            case "5":
                this.setState({type: PlotType.INDEXING_UNIT_CELL_ANGLE});
                break;
            case "6":
                this.setState({type: PlotType.ROI_MAX_COUNT});
                break;
            case "7":
                this.setState({type: PlotType.COLLECTION_EFFICIENCY});
                break;
            case "8":
                this.setState({type: PlotType.ERROR_PIXELS});
                break;
            case "9":
                this.setState({type: PlotType.RECEIVER_DELAY});
                break;
            case "10":
                this.setState({type: PlotType.STRONG_PIXELS});
                break;
            case "11":
                this.setState({type: PlotType.ROI_SUM});
                break;
            case "12":
                this.setState({type: PlotType.BKG_ESTIMATE});
                break;
            case "13":
                this.setState({type: PlotType.RECEIVER_FREE_SEND_BUFS});
                break;
            case "14":
                this.setState({type: PlotType.MAX_VALUE});
        }
    };

    handleChange = (event : SelectChangeEvent<String>) => {
        this.setState({ binning: String(event.target.value).toString() });
    };

    render() {
        return <Paper style={{textAlign: 'center'}} sx={{ height: this.props.height, width: "100%" }}>
            <Toolbar>

                <Grid container sx={{ minWidth: 500 }} >
                    <FormControl variant="standard" sx={{ m: 1, minWidth: 500 }}>

                        <Select
                            value={this.state.tab}
                            onChange={this.plotTypeChange}
                            label="Plot category"
                            sx={{fontWeight: "bold"}}
                        >
                            <MenuItem value={1}>Indexing rate</MenuItem>
                            <MenuItem value={4}>Indexing unit cell (length)</MenuItem>
                            <MenuItem value={5}>Indexing unit cell (angle)</MenuItem>
                            <MenuItem value={2}>Spot count</MenuItem>
                            <MenuItem value={3}>Azimuthal integration profile</MenuItem>
                            <MenuItem value={12}>Background estimate</MenuItem>
                            <MenuItem value={11}>ROI area sum</MenuItem>
                            <MenuItem value={6}>ROI area max count</MenuItem>
                            <MenuItem value={8}>Error and saturated pixels</MenuItem>
                            <MenuItem value={10}>Strong pixels (for spot finding)</MenuItem>
                            <MenuItem value={14}>Maximum pixel value (excl. overloads)</MenuItem>
                            <MenuItem value={7}>Image collection efficiency</MenuItem>
                            <MenuItem value={9}>Receiver delay (internal debugging)</MenuItem>
                            <MenuItem value={13}>Receiver free send buffers (internal debugging)</MenuItem>
                        </Select>

                    </FormControl>
                </Grid>
                <Grid container justifyContent="flex-end">
                    <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }}>
                        <Select
                            value={this.state.binning}
                            onChange={this.handleChange}
                            label="Binning"
                        >
                            <MenuItem value={0}>
                                <em>Auto binning</em>
                            </MenuItem>
                            <MenuItem value={1}>1 image</MenuItem>
                            <MenuItem value={10}>10 images</MenuItem>
                            <MenuItem value={100}>100 images</MenuItem>
                            <MenuItem value={500}>500 images</MenuItem>
                            <MenuItem value={1000}>1000 images</MenuItem>
                            <MenuItem value={5000}>5000 images</MenuItem>
                            <MenuItem value={10000}>10000 images</MenuItem>
                        </Select>
                    </FormControl>
                </Grid>
            </Toolbar>
            <Box sx={{width:"95%", height: this.props.height - 150}} >
                <DataProcessingPlot type={this.state.type} binning={Number(this.state.binning)}/>
            </Box>
        </Paper>
    }
}

export default DataProcessingPlots;