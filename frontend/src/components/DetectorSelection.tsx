import React, {Component} from 'react';

import Select, { SelectChangeEvent } from '@mui/material/Select';
import {Grid,Stack, Table, TableBody, TableCell, TableContainer, TableRow} from "@mui/material";
import Paper from "@mui/material/Paper";
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import {detector_list, detector_list_element} from "../openapi";
import ButtonWithSnackbar from "./ButtonWithSnackbar";

type MyProps = {
    s?: detector_list
}

type MyState = {
    choice: string
};

type MapElement = {
    id: number
    name: string
}

type DetectorTableElement = {
    title: string
    val: string
}

const default_detector_element: detector_list_element = {
    id: 0,
    description: "Not connected",
    serial_number: "N/A",
    base_ipv4_addr: "N/A",
    udp_interface_count: 0,
    nmodules: 0,
    width: 0,
    height: 0,
    readout_time_us: 0,
    min_frame_time_us: 0,
    min_count_time_us: 0
}

class DetectorSelection extends Component<MyProps, MyState> {
    state : MyState = {
        choice: "0"
    }

    componentDidMount() {
        if (this.props.s !== undefined)
            this.setState({choice: this.props.s.current_id.toString()});
    }

    handleChange = (event : SelectChangeEvent<String>) => {
        this.setState({ choice: String(event.target.value).toString() });
    };

    current_detector_info = () : detector_list_element => {
        if (this.props.s === undefined)
            return default_detector_element;
        if (this.props.s.detectors.length <= this.props.s.current_id)
            return default_detector_element;
        return this.props.s.detectors[this.props.s.current_id];
    }

    detector_info = () : JSX.Element => {
        let x : detector_list_element = this.current_detector_info();

        let arr: DetectorTableElement[] = [
            {title: "Current Detector", val: x.description},
            {title: "Serial number", val: x.serial_number},
            {title: "Size", val: `${x.width} x ${x.height}`},
            {title: "Modules", val: `${x.nmodules}`},
            {title: "UDP interfaces", val: `${x.udp_interface_count}`},
            {title: "Min. frame time", val: `${x.min_frame_time_us} µs`},
            {title: "Min. count time", val: `${x.min_count_time_us} µs`},
            {title: "Readout time", val: `${x.readout_time_us} µs`}
        ];

        return <TableContainer
            component={Paper}
            style={{marginLeft: "auto", marginRight: "auto"}}
            sx = {{width: '80%'}}>
            <Table size="small" aria-label="simple table">
                <TableBody>
                    {
                        arr.map(d => (
                            <TableRow>
                                <TableCell component="th" scope="row">{d.title}: </TableCell>
                                <TableCell align="right">{d.val}</TableCell>
                            </TableRow>
                        ))
                    }
                </TableBody>
            </Table>
        </TableContainer>
    }

    detector_list() : MapElement[] {
        let v: MapElement[] = [];

        if (this.props.s !== undefined) {
            let id: number = this.props.s.current_id;
            v = this.props.s.detectors.map(d => ({
                id: d.id,
                name:  `${d.description} (${d.width}x${d.height} pxl) ` + ((d.id === id) ? "*** CURRENT ***" : "")
            }));
        }

        return v;
    }

    render() {
        return <Paper style={{textAlign: 'center'}} sx={{width: '100%'}}>
            <br/>
            <Stack spacing={3} sx={{
                justifyContent: "center",
                alignItems: "center"
            }}>
                <strong>Detector selection </strong>

                {this.detector_info()}

                <FormControl sx={{width: '80%'}}>
                    <InputLabel id="detector-select-label">Detector</InputLabel>
                    <Select
                        labelId="detector-select-label"
                        id="detector-select"
                        value={this.state.choice}
                        label="Detector"
                        onChange={this.handleChange}
                        disabled={this.props.s === undefined}
                    >
                        {this.detector_list().map(d => (<MenuItem value={d.id}> {d.name} </MenuItem>))}
                    </Select>
                </FormControl>
                <ButtonWithSnackbar
                    color={"primary"}
                    path={"/config/select_detector"}
                    input={JSON.stringify({id: Number(this.state.choice)})}
                    method={"PUT"}
                    text={"Select detector"}
                    disabled={this.props.s === undefined}
                />
            </Stack>
            <br/>
        </Paper>
    }
}

export default DetectorSelection;