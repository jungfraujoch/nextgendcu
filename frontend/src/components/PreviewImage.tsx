import React, {Component} from 'react';
import Paper from "@mui/material/Paper";
import {Box, Slider, Stack, Switch} from "@mui/material";
import {TransformWrapper, TransformComponent } from "react-zoom-pan-pinch";
import {preview_settings} from "../openapi";

type MyProps = {
    measuring: boolean
}

type MyState = {
    settings: preview_settings,
    s: Blob | null,
    s_url: string | null,
    update: boolean,
    connection_error: boolean
}

function handleErrors(response: Response): Response {
    if (!response.ok) {
        throw Error(response.statusText);
    }
    return response;
}

class PreviewImage extends Component<MyProps, MyState> {
    interval: ReturnType<typeof setInterval> | undefined;

    state : MyState = {
        s: null,
        settings: {
            saturation: 10,
            jpeg_quality: 90,
            show_spots: true,
            show_roi: false,
            show_indexed: false,
            show_user_mask: false,
            resolution_ring: 0.5
        },
        s_url: null,
        update: true,
        connection_error: true
    }

    updateToggle = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({
            update: event.target.checked
        });
        if (event.target.checked)
            this.getValues();
    }

    setSaturation = (event: Event, newValue: number | number[]) => {
        let s : preview_settings = {
            ...this.state.settings,
            saturation: newValue as number
        };
        this.setState({settings: s});
        this.getValues(s);
    }

    showSpotsToggle = (event: React.ChangeEvent<HTMLInputElement>) => {
        let s : preview_settings = {
            ...this.state.settings,
            show_spots: event.target.checked
        };
        this.setState({settings: s});
        this.getValues(s);
    }

    showROIToggle = (event: React.ChangeEvent<HTMLInputElement>) => {
        let s : preview_settings = {
            ...this.state.settings,
            show_roi: event.target.checked
        };
        this.setState({settings: s});
        this.getValues(s);
    }

    setResolutionRing = (event: Event, newValue: number | number[]) => {
        let s : preview_settings = {
            ...this.state.settings,
            resolution_ring: newValue as number
        };
        this.setState({settings: s});
        this.getValues(s);
    }

    showIndexedToggle = (event: React.ChangeEvent<HTMLInputElement>) => {
        let s : preview_settings = {
            ...this.state.settings,
            show_indexed: event.target.checked
        };
        this.setState({settings: s});
        this.getValues(s);
    }

    showUserMaskToggle = (event: React.ChangeEvent<HTMLInputElement>) => {
        let s : preview_settings = {
            ...this.state.settings,
            show_user_mask: event.target.checked
        };
        this.setState({settings: s});
        this.getValues(s);
    }

    getValues(s: preview_settings = this.state.settings) {
        fetch("/preview/image.jpeg", {
            method: "POST",
            body: JSON.stringify(s)
        })
            .then(handleErrors)
            .then(data => data.blob())
            .then(data => {
                const url = URL.createObjectURL(data);
                let tmp = this.state.s_url;
                this.setState({s: data, s_url: url, connection_error: false});
                if (tmp !== null)
                    URL.revokeObjectURL(tmp);
            }).catch(error => {
            this.setState({connection_error: true});
        })
    }

    componentDidMount() {
        this.getValues();
        this.interval = setInterval(() => {
            if (this.state.update && this.props.measuring)
                this.getValues()
        }, 2000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
        let tmp = this.state.s_url;
        if (tmp !== null)
            URL.revokeObjectURL(tmp);
    }

    render() {
        return <Paper sx={{height: 1250, width: 1000, m: 2}}
                      component={Stack}
                      direction="column">
            <br/>
            <Stack spacing={2} direction="row" sx={{mb: 1}} alignItems="center">
                &nbsp;&nbsp;<strong>Preview image</strong>
                <Switch checked={this.state.update}
                        onChange={this.updateToggle} name="Update"/>
                Update&nbsp;&nbsp;&nbsp;
                <Switch checked={this.state.settings.show_spots}
                        onChange={this.showSpotsToggle} name="Show spots"/>
                Show spots&nbsp;&nbsp;&nbsp;
                <Switch checked={this.state.settings.show_roi}
                        onChange={this.showROIToggle} name="Show ROI"/>
                Show ROI&nbsp;&nbsp;&nbsp;
                <Switch checked={this.state.settings.show_indexed}
                        onChange={this.showIndexedToggle} name="Show ROI"/>
                Show only indexed images&nbsp;&nbsp;&nbsp;
                <Switch checked={this.state.settings.show_user_mask}
                        onChange={this.showUserMaskToggle} name="Show user mask"/>
                Show user mask&nbsp;&nbsp;&nbsp;
                <Box sx={{width: 200}}>
                    <Slider value={Number(this.state.settings.saturation)} min={1} max={80}
                            onChange={this.setSaturation} valueLabelDisplay="auto"/> <br/>Saturation value
                </Box>
                &nbsp;&nbsp;&nbsp;
                <Box sx={{width: 200}}>
                    <Slider value={(this.state.settings.resolution_ring === undefined) ? 0.5 : Number(this.state.settings.resolution_ring)}
                            min={0.5} max={5.0} step={0.1}
                            onChange={this.setResolutionRing} valueLabelDisplay="auto"/> <br/>Resolution Ring
                </Box> &nbsp;&nbsp;&nbsp;

            </Stack>
            <br/>
            {(!this.state.connection_error && (this.state.s_url !== null)) ?
                    <Stack
                        direction="row"
                        justifyContent="center"
                        alignItems="center"
                    >
                        <TransformWrapper>
                            <TransformComponent>
                                <img src={this.state.s_url} alt="Live preview"
                                     style={{maxWidth: "100%", maxHeight: 900}}/>
                            </TransformComponent>
                        </TransformWrapper>
                    </Stack> : <div>Preview image not available</div>
            }
            <br/>
        </Paper>
    }
}

export default PreviewImage;
