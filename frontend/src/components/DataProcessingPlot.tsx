import React, {Component} from 'react';

import {DefaultService, plots} from "../openapi";
import MultiLinePlotWrapper from "./MultiLinePlotWrapper";

export enum PlotType {
    COLLECTION_EFFICIENCY,
    SPOT_COUNT,
    AZIM_INT,
    INDEXING_RATE,
    BKG_ESTIMATE,
    ERROR_PIXELS,
    RECEIVER_DELAY,
    STRONG_PIXELS,
    ROI_SUM,
    ROI_MAX_COUNT,
    RECEIVER_FREE_SEND_BUFS,
    INDEXING_UNIT_CELL,
    INDEXING_UNIT_CELL_ANGLE,
    MAX_VALUE
}

type MyProps = {
    type: PlotType;
    binning: number;
};

type MyState = {
    plots : plots,
    connection_error: boolean
}

type PlotlyPlot = {
    x: number[],
    y: number[],
    type: string,
    mode: string,
    name: string
}

type PlotlyData = PlotlyPlot[]

function AxisTypeX(plot: PlotType) : string {
    if (plot == PlotType.AZIM_INT)
        return "Q [&#8491;<sup>-1</sup>]"
    else
        return "Image number";
}

function AxisTypeY(plot: PlotType) : string {
    switch (plot) {
        case PlotType.COLLECTION_EFFICIENCY:
            return "Efficiency";
        case PlotType.SPOT_COUNT:
            return "Spot count";
        case PlotType.ROI_MAX_COUNT:
        case PlotType.ROI_SUM:
        case PlotType.AZIM_INT:
        case PlotType.BKG_ESTIMATE:
            return "Photon count";
        case PlotType.INDEXING_RATE:
            return "Indexing rate";
        case PlotType.ERROR_PIXELS:
        case PlotType.STRONG_PIXELS:
            return "Pixel count";
        case PlotType.RECEIVER_DELAY:
            return "Delay";
        case PlotType.RECEIVER_FREE_SEND_BUFS:
            return "Free buffers";
        case PlotType.INDEXING_UNIT_CELL:
            return "Length [&#8491;]";
        case PlotType.INDEXING_UNIT_CELL_ANGLE:
            return "Angle [deg]";
        case PlotType.MAX_VALUE:
            return "Max pixel value";
    }
}

class DataProcessingPlot extends Component<MyProps, MyState> {
    interval: ReturnType<typeof setInterval> | undefined;

    state: MyState = {
        plots: {
            plot : [
                {
                    x: [0, 100, 200, 300, 400, 500],
                    y: [0.1, 0.3, 0.5, 0.2, 0.0, 0.1],
                    title: "Example"
                }
            ]
        },
        connection_error: true
    }

    getValues() {
        switch(this.props.type) {
            case PlotType.ERROR_PIXELS:
                DefaultService.getPlotErrorPixel(this.props.binning)
                    .then(data => this.setState({plots: data, connection_error: false}))
                    .catch(error => {
                        this.setState({connection_error: true});
                    });
                break;
            case PlotType.STRONG_PIXELS:
                DefaultService.getPlotStrongPixel(this.props.binning)
                    .then(data => this.setState({plots: data, connection_error: false}))
                    .catch(error => {
                        this.setState({connection_error: true});
                    });
                break;
            case PlotType.COLLECTION_EFFICIENCY:
                DefaultService.getPlotImageCollectionEfficiency(this.props.binning)
                    .then(data => this.setState({plots: data, connection_error: false}))
                    .catch(error => {
                        this.setState({connection_error: true});
                    });
                break;
            case PlotType.SPOT_COUNT:
                DefaultService.getPlotSpotCount(this.props.binning)
                    .then(data => this.setState({plots: data, connection_error: false}))
                    .catch(error => {
                        this.setState({connection_error: true});
                    });
                break;
            case PlotType.BKG_ESTIMATE:
                DefaultService.getPlotBkgEstimate(this.props.binning)
                    .then(data => this.setState({plots: data, connection_error: false}))
                    .catch(error => {
                        this.setState({connection_error: true});
                    });
                break;
            case PlotType.AZIM_INT:
                DefaultService.getPlotAzimInt()
                    .then(data => this.setState({plots: data, connection_error: false}))
                    .catch(error => {
                        this.setState({connection_error: true});
                    });
                break;
           case PlotType.INDEXING_RATE:
                DefaultService.getPlotIndexingRate(this.props.binning)
                    .then(data => this.setState({plots: data, connection_error: false}))
                    .catch(error => {
                        this.setState({connection_error: true});
                    });
                break;
            case PlotType.INDEXING_UNIT_CELL:
                DefaultService.getPlotIndexingUnitCell(this.props.binning)
                    .then(data => this.setState({plots: data, connection_error: false}))
                    .catch(error => {
                        this.setState({connection_error: true});
                    });
                break;
            case PlotType.INDEXING_UNIT_CELL_ANGLE:
                DefaultService.getPlotIndexingUnitCellAngle(this.props.binning)
                    .then(data => this.setState({plots: data, connection_error: false}))
                    .catch(error => {
                        this.setState({connection_error: true});
                    });
                break;
            case PlotType.RECEIVER_DELAY:
                DefaultService.getPlotReceiverDelay(this.props.binning)
                    .then(data => this.setState({plots: data, connection_error: false}))
                    .catch(error => {
                        this.setState({connection_error: true});
                    });
                break;
            case PlotType.RECEIVER_FREE_SEND_BUFS:
                DefaultService.getPlotReceiverFreeSendBuffers(this.props.binning)
                    .then(data => this.setState({plots: data, connection_error: false}))
                    .catch(error => {
                        this.setState({connection_error: true});
                    });
                break;
            case PlotType.ROI_SUM:
                DefaultService.getPlotRoiSum(this.props.binning)
                    .then(data => this.setState({plots: data, connection_error: false}))
                    .catch(error => {
                        this.setState({connection_error: true});
                    });
                break;
            case PlotType.ROI_MAX_COUNT:
                DefaultService.getPlotRoiMaxCount(this.props.binning)
                    .then(data => this.setState({plots: data, connection_error: false}))
                    .catch(error => {
                        this.setState({connection_error: true});
                    });
                break;
            case PlotType.MAX_VALUE:
                DefaultService.getPlotMaxValue(this.props.binning)
                    .then(data => this.setState({plots: data, connection_error: false}))
                    .catch(error => {
                        this.setState({connection_error: true});
                    });
                break;
        }
    }

    componentDidMount() {
        this.getValues();
        this.interval = setInterval(() => this.getValues(), 1000);
    }

    componentWillUnmount() {
        clearInterval(this.interval);
    }

    render() {
        if (this.state.connection_error
            || (this.state.plots === undefined)
            || (this.state.plots.plot === null)
            || (this.state.plots.plot.length === 0))
            return <div>No plots available</div>;

        let data: PlotlyData = [];
        this.state.plots.plot.map(d =>
            data.push({
                x: d.x,
                y: d.y,
                type: "scatter",
                mode: "line",
                name: d.title
            }));
        return <MultiLinePlotWrapper xaxis={AxisTypeX(this.props.type)} yaxis={AxisTypeY(this.props.type)} data={data}/>
    }
}

export default DataProcessingPlot;
