/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { rotation_axis } from './rotation_axis';

export type dataset_settings = {
    /**
     * For standard synchrotron data collection - this is number of images collected per one TTL trigger
     * For XFEL (pulsed source) - this number is ignored and set to 1
     * For storage cell mode - this number is ignored and set to number of storage cells
     *
     */
    images_per_trigger?: number;
    /**
     * Number of TTL trigger that the detector is expected to receive during data collection
     *
     */
    ntrigger?: number;
    /**
     * Image time.
     * If not provided (or zero value) the frame time is assumed as default.
     * Image time must be multiple of frame time; max value is 256 * frame_time.
     * In XFEL mode: summation happens for frames collected with multiple triggers.
     * Ignored for storage cells and if raw data are saved.
     *
     */
    image_time_us?: number;
    /**
     * /entry/detector/beam_center_x in NXmx
     * Beam center in X direction [pixels]
     *
     */
    beam_x_pxl: number;
    /**
     * /entry/detector/beam_center_y in NXmx
     * Beam center in X direction [pixels]
     *
     */
    beam_y_pxl: number;
    /**
     * /entry/detector/distance in NXmx Detector distance [mm]
     */
    detector_distance_mm: number;
    /**
     * Used to calculate /entry/beam/incident_wavelength in NXmx
     * Incident particle (photon, electron) energy in keV
     *
     */
    incident_energy_keV: number;
    /**
     * Prefix for filenames. If left empty, no file will be saved.
     */
    file_prefix?: string;
    /**
     * Number of files in a single HDF5 data file (0 = write all images to a single data file).
     */
    images_per_file?: number;
    /**
     * Number of space group for the crystal. Currently used solely as metadata, not relevant for image processing done in Jungfraujoch.
     */
    space_group_number?: number;
    /**
     * /entry/sample/name in NXmx
     * Sample name
     *
     */
    sample_name?: string;
    /**
     * Compression type for the images transferred over ZeroMQ and saved to HDF5 file.
     *
     */
    compression?: dataset_settings.compression;
    /**
     * /entry/beam/total_flux in NXmx
     * Flux incident on beam plane in photons per second. In other words this is the flux integrated over area. [photons/s]
     *
     */
    total_flux?: number;
    /**
     * /entry/instrument/attenuator/attenuator_transmission
     * Transmission of attenuator (filter) [no units]
     *
     */
    transmission?: number;
    goniometer?: rotation_axis;
    /**
     * Header appendix, added as user_data/user to start ZeroMQ message (can be any valid JSON)
     * In general, it is not saved in HDF5 file.
     *
     * However, if values are placed in "hdf5" object, `jfjoch_writer` will write them in /entry/data of the HDF5 file.
     * This applies solely to string and number (double floating-point). No arrays/sub-objects is allowed.
     * For example {"hdf5": {"val1":1, "val2":"xyz"}}, will write /entry/user/val1 and /entry/user/val2.
     *
     */
    header_appendix?: any;
    /**
     * Image appendix, added as user_data to image ZeroMQ message (can be any valid JSON)
     * Not saved in HDF5 file
     *
     */
    image_appendix?: any;
    /**
     * Rate at which non-indexed images are accepted to be forwarded to writer.
     * Value of 1.0 (default) means that all images are written.
     * Values below zero mean that non-indexed images will be accepted with a given probability.
     *
     */
    data_reduction_factor_serialmx?: number;
    /**
     * Set all counts lower than the value to zero.
     * When the value is set, negative numbers other than error pixel value are always set to zero.
     * Setting to zero is equivalent to turning the option off.
     *
     */
    pixel_value_low_threshold?: number;
    /**
     * Number of run within an experimental session.
     * Transferred over CBOR stream as "series ID", though not saved in HDF5 file.
     * It is highly recommended to keep this number unique for each data collection during experimental series.
     * If not provided, the number will be automatically incremented.
     *
     */
    run_number?: number;
    /**
     * Unique ID of run.
     * Transferred over CBOR stream as "unique series ID", though not saved in HDF5 file.
     * It is highly recommended to keep this name unique for each data collection during experimental series.
     * If not provided, the name will be automatically generated as number + colon + file_prefix.
     *
     */
    run_name?: string;
    /**
     * Name of group owning the data (e.g. p-group or proposal number).
     * Transferred over CBOR stream, though not saved in HDF5 file.
     *
     */
    experiment_group?: string;
    /**
     * Enable lossy compression of pixel values that preserves Poisson statistics.
     * Requires to provide a numerical factor SQ.
     * Pixel value P will be transformed to round(sqrt(P) * SQ), with rounding to the closest integer.
     * Compression is turned off if the value is missing or it is set to zero.
     *
     */
    poisson_compression?: number;
    /**
     * Write NXmx formatted HDF5 master file. Recommended to use for macromolecular crystallography experiments
     * and to turn off for other experiments.
     *
     */
    write_nxmx_hdf5_master?: boolean;
    /**
     * Forward image calibration (at the moment pedestal and pedestal RMS for JUNGFRAU) using the ZeroMQ stream to writer.
     * If parameter is not provided calibration will be saved only if more than 4 images are recorded.
     *
     */
    save_calibration?: boolean;
    /**
     * Unit cell parameters. Necessary to run indexing. Units of angstrom and degree
     */
    unit_cell?: {
        'a': number;
        'b': number;
        'c': number;
        alpha: number;
        beta: number;
        gamma: number;
    };
};

export namespace dataset_settings {

    /**
     * Compression type for the images transferred over ZeroMQ and saved to HDF5 file.
     *
     */
    export enum compression {
        BSLZ4 = 'bslz4',
        BSZSTD = 'bszstd',
        BSZSTD_RLE = 'bszstd_rle',
        NONE = 'none',
    }


}

