/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Definition of a crystal rotation axis
 */
export type rotation_axis = {
    /**
     * Name of rotation axis (e.g., omega, phi)
     */
    name?: string;
    /**
     * Angle step in degrees
     */
    step: number;
    /**
     * Start angle in degrees
     */
    start?: number;
    /**
     * Rotation axis
     */
    vector: Array<number>;
};

