/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Box ROI
 */
export type roi_box = {
    /**
     * Name for the ROI; used in the plots
     */
    name: string;
    /**
     * Lower bound (inclusive) in X coordinate for the box
     */
    min_x_pxl: number;
    /**
     * Upper bound (inclusive) in X coordinate for the box
     */
    max_x_pxl: number;
    /**
     * Lower bound (inclusive) in Y coordinate for the box
     */
    min_y_pxl: number;
    /**
     * Upper bound (inclusive) in Y coordinate for the box
     */
    max_y_pxl: number;
};

