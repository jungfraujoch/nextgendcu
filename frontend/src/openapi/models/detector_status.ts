/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { detector_power_state } from './detector_power_state';
import type { detector_state } from './detector_state';

export type detector_status = {
    state: detector_state;
    powerchip: detector_power_state;
    /**
     * Detector server (on read-out boards) version
     */
    server_version: string;
    /**
     * Remaining triggers to the detector (max of all modules)
     */
    number_of_triggers_left: number;
    /**
     * Temperature of detector FPGAs
     */
    fpga_temp_degC: Array<number>;
    /**
     * High voltage for detector modules
     */
    high_voltage_V: Array<number>;
};

