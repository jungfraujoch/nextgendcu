/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type pixel_mask_statistics = {
    /**
     * Number of pixels masked using the user mask
     */
    user_mask?: number;
    /**
     * Number of pixels with G0 pedestal RMS higher than provided threshold
     */
    too_high_pedestal_rms?: number;
    /**
     * Number of pixels that show wrong gain level during the pedestal procedure
     */
    wrong_gain?: number;
};

