/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * x and y coordinates for plotting, it is OK to assume that both arrays have the same size; layout is optimized for Plotly
 */
export type plot = {
    title: string;
    'x': Array<number>;
    'y': Array<number>;
};

