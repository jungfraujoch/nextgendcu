/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type fpga_status = Array<{
    pci_dev_id: string;
    serial_number: string;
    fw_version: string;
    base_mac_addr: string;
    eth_link_count: number;
    eth_link_status: number;
    power_usage_W: number;
    fpga_temp_C: number;
    hbm_temp_C: number;
    packets_udp: number;
    packets_sls: number;
    idle: boolean;
    /**
     * PCIe link speed measured by generation (expected value is 4 == PCIe Gen4)
     */
    pcie_link_speed: number;
    /**
     * PCIe link width (expected value is 8 == x8)
     */
    pcie_link_width: number;
}>;
