/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type detector_list_element = {
    id: number;
    description: string;
    serial_number: string;
    base_ipv4_addr: string;
    /**
     * Number of UDP interfaces per detector module
     */
    udp_interface_count: number;
    nmodules: number;
    width: number;
    height: number;
    readout_time_us: number;
    min_frame_time_us: number;
    min_count_time_us: number;
};

