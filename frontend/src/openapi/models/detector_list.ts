/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { detector_list_element } from './detector_list_element';

export type detector_list = {
    detectors: Array<detector_list_element>;
    current_id: number;
};

