/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { detector_module } from './detector_module';
import type { detector_type } from './detector_type';
import type { standard_detector_geometry } from './standard_detector_geometry';

export type detector = {
    description: string;
    serial_number: string;
    type?: detector_type;
    high_voltage_V?: number;
    udp_interface_count?: number;
    /**
     * Use module 0 as master for timing. Only applies to JUNGFRAU detector (this cannot be turned off for EIGER).
     */
    module_sync?: boolean;
    sensor_thickness_um?: number;
    /**
     * Minimum difference between frame time and count time in microseconds
     * Defaults are 3 us for EIGER and 20 us for JUNGFRAU
     *
     */
    readout_time_us?: number;
    /**
     * Can be empty for all detectors - default calibration used.
     * For JUNGFRAU: list of gain files, one entry per module.
     * For EIGER: one directory (with detector settings) or list of trim bit files, one entry per half-module.
     *
     */
    calibration_file?: Array<string>;
    /**
     * Hostname for detector module. One entry per module
     * One entry per module. Either empty or number of module entries.
     *
     */
    hostname?: Array<string>;
    sensor_material?: string;
    tx_delay?: Array<number>;
    base_data_ipv4_address?: string;
    standard_geometry?: standard_detector_geometry;
    custom_geometry?: Array<detector_module>;
    /**
     * Mirror detector in Y direction to account for MX convention of (0,0) point in top left corner
     */
    mirror_y?: boolean;
};

