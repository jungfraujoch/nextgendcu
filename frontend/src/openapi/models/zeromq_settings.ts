/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * ZeroMQ configuration for Jungfraujoch software.
 * This structure is used to provide default settings using configuration JSON file and is not used in HTTP.
 *
 */
export type zeromq_settings = {
    /**
     * Watermark for ZeroMQ send queue (number of outstanding messages queued on Jungfraujoch server per queue)
     */
    send_watermark?: number;
    /**
     * Send buffer size for ZeroMQ socket
     */
    send_buffer_size?: number;
    /**
     * PUSH ZeroMQ socket for images. In case multiple sockets are provided, images are streamed over multiple sockets.
     * Images are serialized using CBOR.
     * Address follows ZeroMQ convention for sockets - in practice ipc://<socket file> and tpc://<IP address>:<port> sockets are OK.
     * 0.0.0.0 instead of IP address is accepted and means listening on all network interfaces.
     *
     */
    image_socket?: Array<string>;
    /**
     * PULL ZeroMQ socket for notifications from writer that it finished operation.
     * This allows Jungfraujoch to operate in a synchronous manner, with end of acquisition being also end of writing.
     * Address follows ZeroMQ convention for sockets - in practice ipc://<socket file> and tpc://<IP address>:<port> sockets are OK.
     * 0.0.0.0 instead of IP address should be avoided, as this socket address is forwarded to the writer process via START ZerOMQ message and in case of multiple ineterfaces the address might be ambigous.
     * Using * (star) instead of port number is allowed and it means a random free port number.
     *
     */
    writer_notification_socket?: string;
};

