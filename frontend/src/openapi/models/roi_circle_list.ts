/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { roi_circle } from './roi_circle';

/**
 * List of circular ROIs
 */
export type roi_circle_list = {
    rois: Array<roi_circle>;
};

