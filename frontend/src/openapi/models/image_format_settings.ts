/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type image_format_settings = {
    /**
     * Enable summation of images to a given image_time
     * If disabled images are saved according to original detector speed, but image count is adjusted
     *
     */
    summation: boolean;
    /**
     * Place module read-out into their location on composed detector and extend multipixels
     *
     */
    geometry_transform: boolean;
    /**
     * Convert pixel value in ADU to photon counts/energy
     * Only affects JUNGFRAU detector
     *
     */
    jungfrau_conversion: boolean;
    /**
     * Used to convert energy deposited into pixel to counts
     * If not provided incident_energy_keV is used
     *
     */
    jungfrau_conversion_factor_keV?: number;
    /**
     * Bit depth of resulting image (it doesn't affect the detector read-out value)
     * If not provided value is adjusted automatically
     *
     */
    bit_depth_image?: image_format_settings.bit_depth_image;
    /**
     * Controls if pixels have signed output
     * If not provided value is adjusted automatically
     *
     */
    signed_output?: boolean;
    /**
     * Mask 1 pixel on the module boundary
     *
     */
    mask_module_edges: boolean;
    /**
     * Mask multipixels on chip boundary
     *
     */
    mask_chip_edges: boolean;
    /**
     * JUNGFRAU: mask pixels that don't operate in G0, but do operate in G1 and G1.
     * This should be turned off for cases, where detector is operated at room temperature with long exposure time.
     *
     */
    jungfrau_mask_pixels_without_g0?: boolean;
    /**
     * Masked pixels are set to special value in the images produced by Jungfraujoch
     *
     */
    apply_mask: boolean;
    /**
     * Pixels with pedestal G0 RMS above the threshold are marked as masked pixels
     */
    jungfrau_pedestal_g0_rms_limit?: number;
};

export namespace image_format_settings {

    /**
     * Bit depth of resulting image (it doesn't affect the detector read-out value)
     * If not provided value is adjusted automatically
     *
     */
    export enum bit_depth_image {
        '_8' = 8,
        '_16' = 16,
        '_32' = 32,
    }


}

