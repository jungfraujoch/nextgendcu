/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type broker_status = {
    state: broker_status.state;
    /**
     * Progress of data collection (only available if receiving is running)
     */
    progress?: number;
    /**
     * Message to display besides state of the jfjoch_broker; mostly used for errors and warnings
     * This matters especially for async functions (start/initialize), where API won't return reason
     * for the error during async operation.
     *
     */
    message?: string;
    /**
     * Level of the message to display
     */
    message_severity?: broker_status.message_severity;
};

export namespace broker_status {

    export enum state {
        INACTIVE = 'Inactive',
        IDLE = 'Idle',
        BUSY = 'Busy',
        MEASURING = 'Measuring',
        PEDESTAL = 'Pedestal',
        ERROR = 'Error',
    }

    /**
     * Level of the message to display
     */
    export enum message_severity {
        SUCCESS = 'success',
        INFO = 'info',
        WARNING = 'warning',
        ERROR = 'error',
    }


}

