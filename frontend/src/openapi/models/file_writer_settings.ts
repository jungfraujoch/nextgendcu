/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type file_writer_settings = {
    /**
     * Inform jfjoch_write to overwrite existing files. Otherwise files would be saved with .h5.{timestamp}.tmp suffix.
     *
     */
    overwrite?: boolean;
    /**
     * version 1 - legacy format with soft links to data files in the master file; necessary for DECTRIS Albula 4.0 and DECTRIS Neggia
     * version 2 - newer format with virtual dataset linking data files in the master file, also includes better metadata handling
     *
     */
    file_writer_version?: number;
};

