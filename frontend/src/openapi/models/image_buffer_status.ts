/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type image_buffer_status = {
    /**
     * Image numbers currently present in the buffer.
     */
    image_numbers: Array<number>;
    /**
     * Number of slots in the image buffer. This number, compared to number of images in data collection and
     * frame rate will determine "retention" rate of the image buffer.
     *
     */
    total_slots: number;
    /**
     * Slots available for the data collection
     */
    available_slots: number;
};

