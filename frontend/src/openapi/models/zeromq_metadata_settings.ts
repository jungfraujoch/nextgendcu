/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type zeromq_metadata_settings = {
    /**
     * ZeroMQ metadata socket is enabled.
     */
    enabled: boolean;
    /**
     * Period for generating metadata package sent to the ZeroMQ interface in milliseconds.
     *
     */
    period_ms: number;
    /**
     * PUB ZeroMQ socket for image metadata information.
     * Image metadata are serialized using CBOR.
     * Address follows ZeroMQ convention for sockets - in practice ipc://<socket file> and tcp://<IP address>:<port> sockets are OK.
     * 0.0.0.0 instead of IP address is accepted and means listening on all network interfaces.
     *
     */
    socket_address?: string;
};

