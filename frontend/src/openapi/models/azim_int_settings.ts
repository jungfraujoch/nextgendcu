/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type azim_int_settings = {
    /**
     * If polarization factor is provided, than polarization correction is enabled.
     */
    polarization_factor?: number;
    /**
     * Apply solid angle correction for radial integration
     */
    solid_angle_corr: boolean;
    high_q_recipA: number;
    low_q_recipA: number;
    q_spacing: number;
};

