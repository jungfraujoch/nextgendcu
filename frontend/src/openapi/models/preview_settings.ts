/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Settings for JPEG rendering of preview images
 */
export type preview_settings = {
    /**
     * Saturation value to set contrast in the preview image
     */
    saturation: number;
    /**
     * Show spot finding results on the image
     */
    show_spots?: boolean;
    /**
     * Show ROI areas on the image
     */
    show_roi?: boolean;
    /**
     * Quality of JPEG image (100 - highest; 0 - lowest)
     */
    jpeg_quality?: number;
    /**
     * Preview indexed images only
     */
    show_indexed?: boolean;
    /**
     * Show user mask
     */
    show_user_mask?: boolean;
    resolution_ring?: number;
};

