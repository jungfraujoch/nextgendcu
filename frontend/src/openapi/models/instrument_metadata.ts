/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Metadata for a measurement instrument
 */
export type instrument_metadata = {
    source_name: string;
    /**
     * Type of radiation source. NXmx gives a fixed dictionary, though Jungfraujoch is not enforcing compliance.
     * https://manual.nexusformat.org/classes/base_classes/NXsource.html#nxsource
     * NXsource allows the following:
     *
     * Spallation Neutron Source
     * Pulsed Reactor Neutron Source
     * Reactor Neutron Source
     * Synchrotron X-ray Source
     * Pulsed Muon Source
     * Rotating Anode X-ray
     * Fixed Tube X-ray
     * UV Laser
     * Free-Electron Laser
     * Optical Laser
     * Ion Source
     * UV Plasma Source
     * Metal Jet X-ray
     *
     */
    source_type?: string;
    instrument_name: string;
    /**
     * Settings specific to XFEL (e.g., every image has to come from TTL trigger, save pulse ID and event code)
     *
     */
    pulsed_source?: boolean;
    /**
     * Settings specific to electron source (e.g., wavelength definition)
     *
     */
    electron_source?: boolean;
};

