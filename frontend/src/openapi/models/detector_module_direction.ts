/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum detector_module_direction {
    XP = 'Xp',
    XN = 'Xn',
    YP = 'Yp',
    YN = 'Yn',
}
