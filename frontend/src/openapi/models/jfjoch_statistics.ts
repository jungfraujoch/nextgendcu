/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { broker_status } from './broker_status';
import type { calibration_statistics } from './calibration_statistics';
import type { detector_list } from './detector_list';
import type { detector_settings } from './detector_settings';
import type { detector_status } from './detector_status';
import type { file_writer_settings } from './file_writer_settings';
import type { fpga_status } from './fpga_status';
import type { image_format_settings } from './image_format_settings';
import type { instrument_metadata } from './instrument_metadata';
import type { measurement_statistics } from './measurement_statistics';
import type { pixel_mask_statistics } from './pixel_mask_statistics';
import type { roi_definitions } from './roi_definitions';
import type { spot_finding_settings } from './spot_finding_settings';
import type { zeromq_metadata_settings } from './zeromq_metadata_settings';
import type { zeromq_preview_settings } from './zeromq_preview_settings';

/**
 * Pool statistics for Jungfraujoch to reduce transfers between frontend and jfjoch_broker
 */
export type jfjoch_statistics = {
    detector?: detector_status;
    detector_list?: detector_list;
    detector_settings?: detector_settings;
    image_format_settings?: image_format_settings;
    instrument_metadata?: instrument_metadata;
    file_writer_settings?: file_writer_settings;
    data_processing_settings?: spot_finding_settings;
    measurement?: measurement_statistics;
    broker?: broker_status;
    fpga?: fpga_status;
    calibration?: calibration_statistics;
    zeromq_preview?: zeromq_preview_settings;
    zeromq_metadata?: zeromq_metadata_settings;
    pixel_mask?: pixel_mask_statistics;
    roi?: roi_definitions;
};

