/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type pcie_devices = Array<{
    /**
     * Block device name
     */
    blk?: string;
    /**
     * IPv4 address of the block device
     */
    ipv4?: string;
}>;
