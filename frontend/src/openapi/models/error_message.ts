/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type error_message = {
    /**
     * Human readable message
     */
    msg: string;
    /**
     * Enumerate field for automated analysis
     */
    reason: error_message.reason;
};

export namespace error_message {

    /**
     * Enumerate field for automated analysis
     */
    export enum reason {
        WRONG_DAQSTATE = 'WrongDAQState',
        OTHER = 'Other',
    }


}

