/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Power on of ASICs
 */
export enum detector_power_state {
    POWER_ON = 'PowerOn',
    POWER_OFF = 'PowerOff',
    PARTIAL = 'Partial',
}
