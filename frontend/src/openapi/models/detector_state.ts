/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Current state of the detector
 */
export enum detector_state {
    IDLE = 'Idle',
    WAITING = 'Waiting',
    BUSY = 'Busy',
    ERROR = 'Error',
    NOT_CONNECTED = 'Not connected',
}
