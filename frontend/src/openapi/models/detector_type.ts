/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum detector_type {
    EIGER = 'EIGER',
    JUNGFRAU = 'JUNGFRAU',
}
