/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { roi_box } from './roi_box';

/**
 * List of box ROIs
 */
export type roi_box_list = {
    rois?: Array<roi_box>;
};

