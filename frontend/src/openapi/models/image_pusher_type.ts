/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export enum image_pusher_type {
    ZERO_MQ = 'ZeroMQ',
    HDF5 = 'HDF5',
    CBOR = 'CBOR',
    NONE = 'None',
}
