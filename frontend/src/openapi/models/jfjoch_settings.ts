/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { azim_int_settings } from './azim_int_settings';
import type { detector } from './detector';
import type { detector_settings } from './detector_settings';
import type { file_writer_settings } from './file_writer_settings';
import type { image_format_settings } from './image_format_settings';
import type { image_pusher_type } from './image_pusher_type';
import type { instrument_metadata } from './instrument_metadata';
import type { pcie_devices } from './pcie_devices';
import type { zeromq_metadata_settings } from './zeromq_metadata_settings';
import type { zeromq_preview_settings } from './zeromq_preview_settings';
import type { zeromq_settings } from './zeromq_settings';

/**
 * Default settings for Jungfraujoch software.
 * This structure is used to provide default settings using configuration JSON file and is not used in HTTP.
 *
 */
export type jfjoch_settings = {
    pcie?: pcie_devices;
    zeromq?: zeromq_settings;
    instrument?: instrument_metadata;
    file_writer?: file_writer_settings;
    detector: Array<detector>;
    detector_settings?: detector_settings;
    azim_int?: azim_int_settings;
    image_format?: image_format_settings;
    /**
     * Size of internal buffer in MiB for images before they are sent to a stream
     */
    image_buffer_MiB?: number;
    /**
     * Number of threads used by the receiver
     */
    receiver_threads?: number;
    /**
     * NUMA policy to bind CPUs
     */
    numa_policy?: string;
    /**
     * Location of built JavaScript web frontend
     */
    frontend_directory: string;
    image_pusher: image_pusher_type;
    zeromq_preview?: zeromq_preview_settings;
    zeromq_metadata?: zeromq_metadata_settings;
};

