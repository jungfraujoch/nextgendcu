/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { detector_timing } from './detector_timing';

export type detector_settings = {
    /**
     * Interval between consecutive frames.
     * This is internal frame time for the JUNGFRAU detector, image time has to be integer multiply of this number.
     * For EIGER detector this is default frame time, not used otherwise
     *
     */
    frame_time_us: number;
    /**
     * Integration time of the detector.
     * If not provided count time will be set to maximum value for a given frame time.
     *
     */
    count_time_us?: number;
    /**
     * Use internal frame generator in FPGA instead of getting data from a real detector
     */
    internal_frame_generator?: boolean;
    /**
     * Number of images stored in the internal frame generator.
     */
    internal_frame_generator_images?: number;
    /**
     * Delay between TTL trigger and acquisition start [ns]
     */
    detector_trigger_delay_ns?: number;
    timing?: detector_timing;
    /**
     * Threshold for the EIGER detector.
     * If value is provided, it will be used for all subsequent acquisitions, irrespective of beam energy.
     * If value is not provided, threshold will be determined on start of acquisition as half of incident energy.
     * This might lead to increased start time.
     *
     */
    eiger_threshold_keV?: number;
    /**
     * Bit depth of EIGER read-out.
     * If value is not provided bit depth is adjusted automatically based on the image time.
     *
     */
    eiger_bit_depth?: detector_settings.eiger_bit_depth;
    jungfrau_pedestal_g0_frames?: number;
    jungfrau_pedestal_g1_frames?: number;
    jungfrau_pedestal_g2_frames?: number;
    /**
     * Minimum number of collected images for pedestal to consider it viable
     */
    jungfrau_pedestal_min_image_count?: number;
    jungfrau_storage_cell_count?: number;
    /**
     * Delay between two storage cells [ns]
     */
    jungfrau_storage_cell_delay_ns?: number;
    /**
     * Fix gain to G1 (can be useful for storage cells)
     */
    jungfrau_fixed_gain_g1?: boolean;
    /**
     * Use high G0 (for low energy applications)
     */
    jungfrau_use_gain_hg0?: boolean;
};

export namespace detector_settings {

    /**
     * Bit depth of EIGER read-out.
     * If value is not provided bit depth is adjusted automatically based on the image time.
     *
     */
    export enum eiger_bit_depth {
        '_8' = 8,
        '_16' = 16,
        '_32' = 32,
    }


}

