/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type spot_finding_settings = {
    /**
     * Enable spot finding. This is temporary setting, i.e. can be changed anytime during data collection.
     * Even if disabled spot finding information will still be send and written, though always with zero spots.
     *
     */
    enable: boolean;
    /**
     * Enable indexing. This is temporary setting, i.e. can be changed anytime during data collection.
     *
     */
    indexing: boolean;
    /**
     * Filter spots which form powder rings (e.g., ice rings)
     */
    filter_powder_rings?: boolean;
    /**
     * Minimum number of spots to consider a thin resolution shell (0.01 A^-1) a powder ring and filter out.
     */
    min_spot_count_powder_ring?: number;
    signal_to_noise_threshold: number;
    photon_count_threshold: number;
    min_pix_per_spot: number;
    max_pix_per_spot: number;
    high_resolution_limit: number;
    low_resolution_limit: number;
    /**
     * Acceptance tolerance for spots after the indexing run - the larger the number, the more spots will be accepted
     */
    indexing_tolerance: number;
};

