/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type calibration_statistics = Array<{
    module_number: number;
    storage_cell_number: number;
    pedestal_g0_mean: number;
    pedestal_g1_mean: number;
    pedestal_g2_mean: number;
    gain_g0_mean: number;
    gain_g1_mean: number;
    gain_g2_mean: number;
    masked_pixels: number;
}>;
