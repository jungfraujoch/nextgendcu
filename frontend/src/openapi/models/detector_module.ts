/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { detector_module_direction } from './detector_module_direction';

export type detector_module = {
    x0: number;
    y0: number;
    fast_axis: detector_module_direction;
    slow_axis: detector_module_direction;
};

