/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type zeromq_preview_settings = {
    /**
     * ZeroMQ preview socket is enabled.
     */
    enabled: boolean;
    /**
     * Period for generating preview image sent to the ZeroMQ interface in milliseconds. Default is 1 second.
     * If set to zero, all images will be sent ZeroMQ (should be used only in case of relatively slow data collection).
     * This has no effect on HTTP based preview, which updates always at rate of 1 second.
     *
     */
    period_ms: number;
    /**
     * PUB ZeroMQ socket for preview images. This socket operates at a reduced frame rate.
     * Images are serialized using CBOR.
     * Address follows ZeroMQ convention for sockets - in practice ipc://<socket file> and tcp://<IP address>:<port> sockets are OK.
     * 0.0.0.0 instead of IP address is accepted and means listening on all network interfaces.
     *
     */
    socket_address?: string;
};

