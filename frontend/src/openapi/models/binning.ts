/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Binning of frames for the plot (0 = default binning)
 */
export type binning = number;
