/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Circular ROI
 */
export type roi_circle = {
    /**
     * Name for the ROI; used in the plots
     */
    name: string;
    /**
     * X coordinate of center of the circle [pixels]
     */
    center_x_pxl: number;
    /**
     * Y coordinate of center of the circle [pixels]
     */
    center_y_pxl: number;
    /**
     * Radius of the circle [pixels]
     */
    radius_pxl: number;
};

