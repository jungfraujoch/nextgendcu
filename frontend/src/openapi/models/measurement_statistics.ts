/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

export type measurement_statistics = {
    file_prefix?: string;
    /**
     * Number of data collection run. This can be either automatically incremented or provided externally for each data collection.
     *
     */
    run_number?: number;
    /**
     * Name of group owning the data (e.g. p-group or proposal number).
     *
     */
    experiment_group?: string;
    images_expected?: number;
    /**
     * Images collected by the receiver. This number will be lower than images expected if there were issues with data collection performance.
     *
     */
    images_collected?: number;
    /**
     * Images sent to the writer.
     * The value does not include images discarded by lossy compression filter and images not forwarded due to full ZeroMQ queue.
     *
     */
    images_sent?: number;
    /**
     * Images discarded by the lossy compression filter
     */
    images_discarded_lossy_compression?: number;
    max_image_number_sent?: number;
    collection_efficiency?: number;
    compression_ratio?: number;
    cancelled?: boolean;
    max_receiver_delay?: number;
    indexing_rate?: number;
    detector_width?: number;
    detector_height?: number;
    detector_pixel_depth?: measurement_statistics.detector_pixel_depth;
    bkg_estimate?: number;
    unit_cell?: string;
    /**
     * Moving average of 1000 images counting number of error pixels on the detector
     */
    error_pixels?: number;
    /**
     * Moving average of 1000 images counting number of saturated pixels on the detector
     */
    saturated_pixels?: number;
    /**
     * If there is an ROI defined with name "beam", this number will hold moving average of 1000 images
     * for number of valid pixels within this ROI
     *
     */
    roi_beam_pixels?: number;
    /**
     * If there is an ROI defined with name "beam", this number will hold moving average of 1000 images
     * for sum of valid pixels within this ROI
     *
     */
    roi_beam_sum?: number;
};

export namespace measurement_statistics {

    export enum detector_pixel_depth {
        '_2' = 2,
        '_4' = 4,
    }


}

