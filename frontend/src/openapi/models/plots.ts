/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { plot } from './plot';

export type plots = {
    title?: string;
    plot: Array<plot>;
};

