/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

import type { roi_box_list } from './roi_box_list';
import type { roi_circle_list } from './roi_circle_list';

/**
 * ROI defintions
 */
export type roi_definitions = {
    box: roi_box_list;
    circle: roi_circle_list;
};

