/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */

/**
 * Regular rectangular geometry, first module is in the bottom left corner of the detector
 */
export type standard_detector_geometry = {
    /**
     * Number of modules in the detector
     */
    nmodules: number;
    /**
     * Gap size in X direction [pixels]
     */
    gap_x?: number;
    /**
     * Gap size in Y direction [pixels]
     */
    gap_y?: number;
    /**
     * Number of modules in one row
     */
    modules_in_row?: number;
};

