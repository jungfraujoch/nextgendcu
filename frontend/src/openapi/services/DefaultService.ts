/* generated using openapi-typescript-codegen -- do no edit */
/* istanbul ignore file */
/* tslint:disable */
/* eslint-disable */
import type { azim_int_settings } from '../models/azim_int_settings';
import type { broker_status } from '../models/broker_status';
import type { calibration_statistics } from '../models/calibration_statistics';
import type { dataset_settings } from '../models/dataset_settings';
import type { detector_list } from '../models/detector_list';
import type { detector_selection } from '../models/detector_selection';
import type { detector_settings } from '../models/detector_settings';
import type { detector_status } from '../models/detector_status';
import type { file_writer_settings } from '../models/file_writer_settings';
import type { fpga_status } from '../models/fpga_status';
import type { image_buffer_status } from '../models/image_buffer_status';
import type { image_format_settings } from '../models/image_format_settings';
import type { instrument_metadata } from '../models/instrument_metadata';
import type { jfjoch_statistics } from '../models/jfjoch_statistics';
import type { measurement_statistics } from '../models/measurement_statistics';
import type { plots } from '../models/plots';
import type { preview_settings } from '../models/preview_settings';
import type { roi_definitions } from '../models/roi_definitions';
import type { spot_finding_settings } from '../models/spot_finding_settings';
import type { zeromq_metadata_settings } from '../models/zeromq_metadata_settings';
import type { zeromq_preview_settings } from '../models/zeromq_preview_settings';

import type { CancelablePromise } from '../core/CancelablePromise';
import { OpenAPI } from '../core/OpenAPI';
import { request as __request } from '../core/request';

export class DefaultService {

    /**
     * Initialize detector and data acquisition
     * Should be used in two cases:
     * - Detector is in `Inactive` state
     * - Detector is in `Error` state
     * X-ray shutter must be closed.
     * This operation will reconfigure network interface of the detector.
     * During operation of the detector it is recommended to use the `POST /pedestal` operation instead.
     * If storage cells are used, the execution time might be few minutes.
     *
     * This is async function - one needs to use `POST /wait_till_done` to ensure operation is done.
     *
     * @returns any Initialization started
     * @throws ApiError
     */
    public static postInitialize(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/initialize',
            errors: {
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Collect dark current for the detector
     * Updates calibration of the JUNGFRAU detector. Must be in `Idle` state.
     *
     * X-ray shutter must be closed. Recommended to run once per hour for long integration times (> 100 us).
     *
     * This is async function - one needs to use `POST /wait_till_done` to ensure operation is done.
     *
     * @returns any Everything OK
     * @throws ApiError
     */
    public static postPedestal(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/pedestal',
            errors: {
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Start detector
     * Start data acquisition.
     * Detector must be in `Idle` state.
     * Doesn't run calibration procedure.
     * When the function returns, detector is ready to accept soft/TTL triggers.
     *
     * @param requestBody
     * @returns any Everything OK
     * @throws ApiError
     */
    public static postStart(
        requestBody?: dataset_settings,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/start',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Input parsing or validation error`,
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Wait for acquisition done
     * Block execution of external script till initialization, data collection or pedestal is finished.
     * Running this command does not affect (cancel) running data collection, it is only to ensure synchronous execution of other software.
     *
     * To not block web server for a indefinite period of time, the procedure is provided with a timeout.
     * Extending timeout is possible, but requires to ensure safety that client will not close the connection and retry the connection.
     *
     * @param timeout Timeout in seconds (0 == immediate response)
     * @returns any Detector in `Idle` state, another data collection can start immediately
     * @throws ApiError
     */
    public static postWaitTillDone(
        timeout: number = 60,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/wait_till_done',
            query: {
                'timeout': timeout,
            },
            errors: {
                400: `Timeout parameter out of bounds`,
                500: `Error within Jungfraujoch code - see output message.`,
                502: `Detector is inactive mode`,
                504: `Timeout reached, need to restart operation`,
            },
        });
    }

    /**
     * Send soft trigger to the detector
     * Generate soft trigger
     * @returns any Trigger sent
     * @throws ApiError
     */
    public static postTrigger(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/trigger',
        });
    }

    /**
     * Cancel running data collection
     * Command will inform FPGA network card to stop pedestal or data collection at the current stage.
     * Any frame that is currently being processed by CPU will be finished and sent to writer.
     * Given the command is making sure to gracefully stop data acquisition and detector, it might take some time to switch back after command finished to `Idle` state.
     *
     * If data collection is not running, the command has no effect.
     *
     * @returns any Cancel request sent to FPGAs (or ignored, as data collection is not running)
     * @throws ApiError
     */
    public static postCancel(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/cancel',
        });
    }

    /**
     * Prepare detector to turn off
     * Should be in `Idle` or `Error` state.
     * Command deactivates data acquisition and turns off detector high voltage and ASIC.
     * Should be used always before turning off power from the detector.
     *
     * @returns any Detector ready to turn off
     * @throws ApiError
     */
    public static postDeactivate(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/deactivate',
            errors: {
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Change detector configuration
     * Detector settings are ones that have effect on calibration, i.e., pedestal has to be collected again after changing these settings.
     * This can only be done when detector is `Idle`, `Error` or `Inactive` states.
     * If detector is in `Idle` state , pedestal procedure will be executed automatically - there must be no X-rays on the detector during the operation.
     * If detector is in `Inactive` or `Error` states, new settings will be saved, but no calibration will be executed.
     *
     * @param requestBody
     * @returns any Everything OK
     * @throws ApiError
     */
    public static putConfigDetector(
        requestBody?: detector_settings,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/config/detector',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Input parsing or validation error`,
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Get detector configuration
     * Can be done anytime
     * @returns detector_settings Everything OK
     * @throws ApiError
     */
    public static getConfigDetector(): CancelablePromise<detector_settings> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/config/detector',
        });
    }

    /**
     * Change file writer settings
     * This can only be done when detector is `Idle`, `Error` or `Inactive` states.
     *
     * @param requestBody
     * @returns any Everything OK
     * @throws ApiError
     */
    public static putConfigFileWriter(
        requestBody?: file_writer_settings,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/config/file_writer',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Input parsing or validation error`,
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Get file writer settings
     * Can be done anytime
     * @returns file_writer_settings Everything OK
     * @throws ApiError
     */
    public static getConfigFileWriter(): CancelablePromise<file_writer_settings> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/config/file_writer',
        });
    }

    /**
     * Change instrument metadata
     * This can only be done when detector is `Idle`, `Error` or `Inactive` states.
     *
     * @param requestBody
     * @returns any Everything OK
     * @throws ApiError
     */
    public static putConfigInstrument(
        requestBody?: instrument_metadata,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/config/instrument',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Input parsing or validation error`,
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Get instrument metadata
     * Can be done anytime
     * @returns instrument_metadata Everything OK
     * @throws ApiError
     */
    public static getConfigInstrument(): CancelablePromise<instrument_metadata> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/config/instrument',
        });
    }

    /**
     * Change image output format
     * This can only be done when detector is `Idle`, `Error` or `Inactive` states.
     *
     * @param requestBody
     * @returns any Everything OK
     * @throws ApiError
     */
    public static putConfigImageFormat(
        requestBody?: image_format_settings,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/config/image_format',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Input parsing or validation error`,
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Get image output format
     * Can be done anytime
     * @returns image_format_settings Everything OK
     * @throws ApiError
     */
    public static getConfigImageFormat(): CancelablePromise<image_format_settings> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/config/image_format',
        });
    }

    /**
     * Configure format for raw data collection
     * This can only be done when detector is `Idle`, `Error` or `Inactive` states.
     *
     * @returns any Everything OK
     * @throws ApiError
     */
    public static postConfigImageFormatRaw(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/config/image_format/raw',
            errors: {
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Configure format for data collection with full conversion
     * This can only be done when detector is `Idle`, `Error` or `Inactive` states.
     *
     * @returns any Everything OK
     * @throws ApiError
     */
    public static postConfigImageFormatConversion(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/config/image_format/conversion',
            errors: {
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Configure spot finding
     * Can be done anytime, also while data collection is running
     * @param requestBody
     * @returns any Everything OK
     * @throws ApiError
     */
    public static putConfigSpotFinding(
        requestBody?: spot_finding_settings,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/config/spot_finding',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Input parsing or validation error`,
            },
        });
    }

    /**
     * Get data processing configuration
     * Can be done anytime
     * @returns spot_finding_settings Everything OK
     * @throws ApiError
     */
    public static getConfigSpotFinding(): CancelablePromise<spot_finding_settings> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/config/spot_finding',
        });
    }

    /**
     * Configure radial integration
     * Can be done when detector is Inactive or Idle
     * @param requestBody
     * @returns any Everything OK
     * @throws ApiError
     */
    public static putConfigAzimInt(
        requestBody?: azim_int_settings,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/config/azim_int',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Input parsing or validation error`,
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Get radial integration configuration
     * Can be done anytime
     * @returns azim_int_settings Everything OK
     * @throws ApiError
     */
    public static getConfigAzimInt(): CancelablePromise<azim_int_settings> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/config/azim_int',
        });
    }

    /**
     * Load binary image for internal FPGA generator
     * Load image for internal FPGA generator. This can only happen in Idle state of the detector.
     * Requires binary blob with 16-bit integer numbers of size of detector in raw/converted coordinates
     * (depending on detector settings).
     *
     * @param id Image id to upload
     * @param requestBody
     * @returns any Everything OK
     * @throws ApiError
     */
    public static putConfigInternalGeneratorImage(
        id?: number,
        requestBody?: Blob,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/config/internal_generator_image',
            query: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'application/octet-stream',
            errors: {
                400: `Input parsing or validation error`,
            },
        });
    }

    /**
     * Load TIFF image for internal FPGA generator
     * Load image for internal FPGA generator. This can only happen in Idle state of the detector.
     * Requires TIFF with 16-bit integer numbers of size of detector in raw/converted coordinates
     * (depending on detector settings).
     *
     * @param id Image ID to upload
     * @param requestBody
     * @returns any Everything OK
     * @throws ApiError
     */
    public static putConfigInternalGeneratorImageTiff(
        id?: number,
        requestBody?: Blob,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/config/internal_generator_image.tiff',
            query: {
                'id': id,
            },
            body: requestBody,
            mediaType: 'image/tiff',
            errors: {
                400: `Input parsing or validation error`,
            },
        });
    }

    /**
     * Select detector
     * Jungfraujoch allows to control multiple detectors and/or region-of-interests.
     * The command allows to choose one detector from the list (ID has to be consistent with one provided by GET response).
     * Changing detector will set detector to `Inactive` state and will require reinitialization.
     *
     * @param requestBody
     * @returns any Everything OK
     * @throws ApiError
     */
    public static putConfigSelectDetector(
        requestBody?: detector_selection,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/config/select_detector',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Input parsing or validation error`,
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * List available detectors
     * Configured detectors that can be selected by used
     * @returns detector_list Everything OK
     * @throws ApiError
     */
    public static getConfigSelectDetector(): CancelablePromise<detector_list> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/config/select_detector',
        });
    }

    /**
     * Set ZeroMQ preview settings
     * Jungfraujoch can generate preview message stream on ZeroMQ SUB socket.
     * Here settings of the socket can be adjusted.
     * While the data structure contains also socket_address, this cannot be changed via HTTP and is ignore in PUT request.
     * Options set with this PUT request have no effect on HTTP based preview.
     *
     * @param requestBody
     * @returns any Everything OK
     * @throws ApiError
     */
    public static putConfigZeromqPreview(
        requestBody?: zeromq_preview_settings,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/config/zeromq_preview',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Input parsing or validation error`,
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Get ZeroMQ preview settings
     * @returns zeromq_preview_settings Everything OK
     * @throws ApiError
     */
    public static getConfigZeromqPreview(): CancelablePromise<zeromq_preview_settings> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/config/zeromq_preview',
        });
    }

    /**
     * Set ZeroMQ metadata settings
     * Jungfraujoch can generate metadata message stream on ZeroMQ PUB socket. This stream covers all images.
     * Here settings of the socket can be adjusted.
     * While the data structure contains also socket_address, this cannot be changed via HTTP and is ignore in PUT request.
     *
     * @param requestBody
     * @returns any Everything OK
     * @throws ApiError
     */
    public static putConfigZeromqMetadata(
        requestBody?: zeromq_metadata_settings,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/config/zeromq_metadata',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Input parsing or validation error`,
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Get ZeroMQ metadata socket settings
     * @returns zeromq_metadata_settings Everything OK
     * @throws ApiError
     */
    public static getConfigZeromqMetadata(): CancelablePromise<zeromq_metadata_settings> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/config/zeromq_metadata',
        });
    }

    /**
     * Get Jungfraujoch status
     * Status of the data acquisition
     * @returns broker_status Everything OK
     * @throws ApiError
     */
    public static getStatus(): CancelablePromise<broker_status> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/status',
        });
    }

    /**
     * Get status of FPGA devices
     * @returns fpga_status Everything OK
     * @throws ApiError
     */
    public static getFpgaStatus(): CancelablePromise<fpga_status> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/fpga_status',
        });
    }

    /**
     * Return XFEL pulse IDs for the current data acquisition
     * Return array of XFEL pulse IDs - (-1) if image not recorded
     * @returns number Pulse ID collected
     * @throws ApiError
     */
    public static getXfelPulseId(): CancelablePromise<Array<number>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/xfel/pulse_id',
            errors: {
                404: `Not in XFEL mode or no acquisition recorded`,
            },
        });
    }

    /**
     * Return XFEL event codes for the current data acquisition
     * Return array of XFEL event codes
     * @returns number Event codes collected
     * @throws ApiError
     */
    public static getXfelEventCode(): CancelablePromise<Array<number>> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/xfel/event_code',
            errors: {
                404: `Not in XFEL mode or no acquisition recorded`,
            },
        });
    }

    /**
     * Get detector status
     * Status of the JUNGFRAU detector
     * @returns detector_status Everything OK
     * @throws ApiError
     */
    public static getDetectorStatus(): CancelablePromise<detector_status> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/detector/status',
            errors: {
                404: `Running in "simulator" mode - no detector present`,
                500: `Error encountered when trying to read status`,
            },
        });
    }

    /**
     * Get ROI definitions
     * @returns roi_definitions OK
     * @throws ApiError
     */
    public static getConfigRoi(): CancelablePromise<roi_definitions> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/config/roi',
        });
    }

    /**
     * Upload ROI definitions
     * @param requestBody
     * @returns any Everything OK
     * @throws ApiError
     */
    public static putConfigRoi(
        requestBody?: roi_definitions,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/config/roi',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Input parsing or validation error`,
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Generate background estimate plot
     * Mean intensity for d = 3 - 5 A per image; binning is configurable
     *
     * @param binning Binning of frames for the plot (0 = default binning)
     * @param compression Enable DEFLATE compression of output data.
     * @returns plots Everything OK. Response will be by default compressed with deflate algorithm, if using curl, use --compressed option.
     * @throws ApiError
     */
    public static getPlotBkgEstimate(
        binning?: number,
        compression: boolean = true,
    ): CancelablePromise<plots> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/plot/bkg_estimate',
            query: {
                'binning': binning,
                'compression': compression,
            },
            errors: {
                400: `Input parsing or validation error`,
            },
        });
    }

    /**
     * Generate spot count plot
     * Number of spots per image; binning is configurable
     * @param binning Binning of frames for the plot (0 = default binning)
     * @param compression Enable DEFLATE compression of output data.
     * @returns plots Response will be by default compressed with deflate algorithm, if using curl, use --compressed option.
     * @throws ApiError
     */
    public static getPlotSpotCount(
        binning?: number,
        compression: boolean = true,
    ): CancelablePromise<plots> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/plot/spot_count',
            query: {
                'binning': binning,
                'compression': compression,
            },
            errors: {
                400: `Input parsing or validation error`,
            },
        });
    }

    /**
     * Generate indexing rate plot
     * Image indexing rate; binning is configurable
     * @param binning Binning of frames for the plot (0 = default binning)
     * @param compression Enable DEFLATE compression of output data.
     * @returns plots Response will be by default compressed with deflate algorithm, if using curl, use --compressed option.
     * @throws ApiError
     */
    public static getPlotIndexingRate(
        binning?: number,
        compression: boolean = true,
    ): CancelablePromise<plots> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/plot/indexing_rate',
            query: {
                'binning': binning,
                'compression': compression,
            },
            errors: {
                400: `Input parsing or validation error`,
            },
        });
    }

    /**
     * Generate indexing unit cell length plots
     * Crystal unit cell based on indexing results; binning is configurable
     * @param binning Binning of frames for the plot (0 = default binning)
     * @param compression Enable DEFLATE compression of output data.
     * @returns plots Response will be by default compressed with deflate algorithm, if using curl, use --compressed option.
     * @throws ApiError
     */
    public static getPlotIndexingUnitCell(
        binning?: number,
        compression: boolean = true,
    ): CancelablePromise<plots> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/plot/indexing_unit_cell',
            query: {
                'binning': binning,
                'compression': compression,
            },
            errors: {
                400: `Input parsing or validation error`,
            },
        });
    }

    /**
     * Generate indexing unit cell angle plot
     * Crystal unit cell based on indexing results; binning is configurable
     * @param binning Binning of frames for the plot (0 = default binning)
     * @param compression Enable DEFLATE compression of output data.
     * @returns plots Response will be by default compressed with deflate algorithm, if using curl, use --compressed option.
     * @throws ApiError
     */
    public static getPlotIndexingUnitCellAngle(
        binning?: number,
        compression: boolean = true,
    ): CancelablePromise<plots> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/plot/indexing_unit_cell_angle',
            query: {
                'binning': binning,
                'compression': compression,
            },
            errors: {
                400: `Input parsing or validation error`,
            },
        });
    }

    /**
     * Generate error pixels plot
     * Count of error (mean) and saturated (mean/max) pixels per image; binning is configurable
     * @param binning Binning of frames for the plot (0 = default binning)
     * @param compression Enable DEFLATE compression of output data.
     * @returns plots Response will be by default compressed with deflate algorithm, if using curl, use --compressed option.
     * @throws ApiError
     */
    public static getPlotErrorPixel(
        binning?: number,
        compression: boolean = true,
    ): CancelablePromise<plots> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/plot/error_pixel',
            query: {
                'binning': binning,
                'compression': compression,
            },
            errors: {
                400: `Input parsing or validation error`,
            },
        });
    }

    /**
     * Generate strong pixels plot
     * Count of strong pixels per image (from spot finding); binning is configurable
     * @param binning Binning of frames for the plot (0 = default binning)
     * @param compression Enable DEFLATE compression of output data.
     * @returns plots Response will be by default compressed with deflate algorithm, if using curl, use --compressed option.
     * @throws ApiError
     */
    public static getPlotStrongPixel(
        binning?: number,
        compression: boolean = true,
    ): CancelablePromise<plots> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/plot/strong_pixel',
            query: {
                'binning': binning,
                'compression': compression,
            },
            errors: {
                400: `Input parsing or validation error`,
            },
        });
    }

    /**
     * Generate maximum pixel value plot
     * Provides maximum viable pixel value (excluding overloads and error pixels); binning is configurable and maximum of a bin is returned
     * @param binning Binning of frames for the plot (0 = default binning)
     * @param compression Enable DEFLATE compression of output data.
     * @returns plots Response will be by default compressed with deflate algorithm, if using curl, use --compressed option.
     * @throws ApiError
     */
    public static getPlotMaxValue(
        binning?: number,
        compression: boolean = true,
    ): CancelablePromise<plots> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/plot/max_value',
            query: {
                'binning': binning,
                'compression': compression,
            },
            errors: {
                400: `Input parsing or validation error`,
            },
        });
    }

    /**
     * Generate ROI sum plot
     * Sum of ROI rectangle per image; binning is configurable
     * @param binning Binning of frames for the plot (0 = default binning)
     * @param compression Enable DEFLATE compression of output data.
     * @returns plots Response will be by default compressed with deflate algorithm, if using curl, use --compressed option.
     * @throws ApiError
     */
    public static getPlotRoiSum(
        binning?: number,
        compression: boolean = true,
    ): CancelablePromise<plots> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/plot/roi_sum',
            query: {
                'binning': binning,
                'compression': compression,
            },
            errors: {
                400: `Input parsing or validation error`,
            },
        });
    }

    /**
     * Generate plot of ROI max count
     * Max count of ROI per image; binning is configurable
     * @param binning Binning of frames for the plot (0 = default binning)
     * @param compression Enable DEFLATE compression of output data.
     * @returns plots Response will be by default compressed with deflate algorithm, if using curl, use --compressed option.
     * @throws ApiError
     */
    public static getPlotRoiMaxCount(
        binning?: number,
        compression: boolean = true,
    ): CancelablePromise<plots> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/plot/roi_max_count',
            query: {
                'binning': binning,
                'compression': compression,
            },
            errors: {
                400: `Input parsing or validation error`,
            },
        });
    }

    /**
     * Generate plot of ROI valid pixels
     * Number of pixels within a ROI area; pixels with special values (overload, bad pixel) are excluded; multipixels are counted just once; binning is configurable
     * @param binning Binning of frames for the plot (0 = default binning)
     * @param compression Enable DEFLATE compression of output data.
     * @returns plots Response will be by default compressed with deflate algorithm, if using curl, use --compressed option.
     * @throws ApiError
     */
    public static getPlotRoiValidPixels(
        binning?: number,
        compression: boolean = true,
    ): CancelablePromise<plots> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/plot/roi_valid_pixels',
            query: {
                'binning': binning,
                'compression': compression,
            },
            errors: {
                400: `Input parsing or validation error`,
            },
        });
    }

    /**
     * Generate receiver delay plot
     * Amount of frames the receiver is behind the FPGA for each image - used for internal debugging; binning is configurable
     * @param binning Binning of frames for the plot (0 = default binning)
     * @param compression Enable DEFLATE compression of output data.
     * @returns plots Everything OK
     * @throws ApiError
     */
    public static getPlotReceiverDelay(
        binning?: number,
        compression: boolean = true,
    ): CancelablePromise<plots> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/plot/receiver_delay',
            query: {
                'binning': binning,
                'compression': compression,
            },
            errors: {
                400: `Input parsing or validation error`,
            },
        });
    }

    /**
     * Generate receiver free send buffer plot
     * Amount of send buffers available during frame processing - used for internal debugging; binning is configurable
     * @param binning Binning of frames for the plot (0 = default binning)
     * @param compression Enable DEFLATE compression of output data.
     * @returns plots Everything OK
     * @throws ApiError
     */
    public static getPlotReceiverFreeSendBuffers(
        binning?: number,
        compression: boolean = true,
    ): CancelablePromise<plots> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/plot/receiver_free_send_buffers',
            query: {
                'binning': binning,
                'compression': compression,
            },
            errors: {
                400: `Input parsing or validation error`,
            },
        });
    }

    /**
     * Generate image collection efficiency plot
     * Ratio of collected and expected packets per image; binning is configurable
     * @param binning Binning of frames for the plot (0 = default binning)
     * @param compression Enable DEFLATE compression of output data.
     * @returns plots Everything OK
     * @throws ApiError
     */
    public static getPlotImageCollectionEfficiency(
        binning?: number,
        compression: boolean = true,
    ): CancelablePromise<plots> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/plot/image_collection_efficiency',
            query: {
                'binning': binning,
                'compression': compression,
            },
            errors: {
                400: `Input parsing or validation error`,
            },
        });
    }

    /**
     * Generate plot with number of received packets per image
     * Number of collected packets per image; binning is configurable
     * @param binning Binning of frames for the plot (0 = default binning)
     * @param compression Enable DEFLATE compression of output data.
     * @returns plots Everything OK
     * @throws ApiError
     */
    public static getPlotPacketsReceived(
        binning?: number,
        compression: boolean = true,
    ): CancelablePromise<plots> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/plot/packets_received',
            query: {
                'binning': binning,
                'compression': compression,
            },
            errors: {
                400: `Input parsing or validation error`,
            },
        });
    }

    /**
     * Generate radial integration profile
     * Generate average radial integration profile
     * @param compression Enable DEFLATE compression of output data.
     * @returns plots Everything OK
     * @throws ApiError
     */
    public static getPlotAzimInt(
        compression: boolean = true,
    ): CancelablePromise<plots> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/plot/azim_int',
            query: {
                'compression': compression,
            },
        });
    }

    /**
     * Get general statistics
     * @param compression Enable DEFLATE compression of output data.
     * @returns jfjoch_statistics Everything OK
     * @throws ApiError
     */
    public static getStatistics(
        compression: boolean = true,
    ): CancelablePromise<jfjoch_statistics> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/statistics',
            query: {
                'compression': compression,
            },
        });
    }

    /**
     * Get data collection statistics
     * Results of the last data collection
     * @returns measurement_statistics Everything OK
     * @throws ApiError
     */
    public static getStatisticsDataCollection(): CancelablePromise<measurement_statistics> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/statistics/data_collection',
            errors: {
                404: `No data collection performed so far`,
            },
        });
    }

    /**
     * Get calibration statistics
     * Statistics are provided for each module/storage cell separately
     * @returns calibration_statistics Everything OK
     * @throws ApiError
     */
    public static getStatisticsCalibration(): CancelablePromise<calibration_statistics> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/statistics/calibration',
        });
    }

    /**
     * Get last preview image in JPEG format using custom settings
     * @param requestBody
     * @returns binary Preview image
     * @throws ApiError
     */
    public static postPreviewImageJpeg(
        requestBody?: preview_settings,
    ): CancelablePromise<Blob> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/preview/image.jpeg',
            body: requestBody,
            mediaType: 'application/json',
            errors: {
                400: `Input parsing or validation error`,
                404: `No preview image recorded so far`,
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Get last preview image in JPEG format using default settings
     * @returns binary Preview image
     * @throws ApiError
     */
    public static getPreviewImageJpeg(): CancelablePromise<Blob> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/preview/image.jpeg',
            errors: {
                404: `No preview image recorded so far`,
            },
        });
    }

    /**
     * Get last preview image in TIFF format
     * @returns binary Preview image
     * @throws ApiError
     */
    public static getPreviewImageTiff(): CancelablePromise<Blob> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/preview/image.tiff',
            errors: {
                404: `No preview image recorded so far`,
            },
        });
    }

    /**
     * Get last preview image in TIFF format for calibration with PyFAI/Dioptas
     * Image is reduced to unsigned 16-bit images, all bad pixels are set to 65535 and image is mirrored in vertical direction
     * @returns binary Preview image
     * @throws ApiError
     */
    public static getPreviewCalibrationTiff(): CancelablePromise<Blob> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/preview/calibration.tiff',
            errors: {
                404: `No preview image recorded so far`,
            },
        });
    }

    /**
     * Get mask of the detector (binary)
     * Get full pixel mask of the detector
     * See NXmx standard for meaning of pixel values
     *
     * @returns binary Binary array (4 byte; unsigned)
     * @throws ApiError
     */
    public static getConfigMask(): CancelablePromise<Blob> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/config/mask',
        });
    }

    /**
     * Get user mask of the detector (binary)
     * Get user pixel mask of the detector in the actual detector coordinates: 0 - good pixel, 1 - masked
     * @returns binary User mask in binary format (4 byte; unsigned)
     * @throws ApiError
     */
    public static getConfigUserMask(): CancelablePromise<Blob> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/config/user_mask',
        });
    }

    /**
     * Upload user mask of the detector (binary)
     * Should be in `Idle` state.
     * Upload user mask of the detector - this is for example to account for beam stop shadow or misbehaving regions.
     * If detector is conversion mode the mask can be both in raw (1024x512; stacked modules) or converted coordinates.
     * In the latter case - module gaps are ignored and don't need to be assigned value.
     * Mask is expected as binary array (4-byte; unsigned).
     * 0 - good pixel, other value - masked
     * User mask is stored in NXmx pixel mask (bit 8), as well as used in spot finding and azimuthal integration.
     *
     * @param requestBody
     * @returns any All good
     * @throws ApiError
     */
    public static putConfigUserMask(
        requestBody?: Blob,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/config/user_mask',
            body: requestBody,
            mediaType: 'application/octet-stream',
            errors: {
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Get mask of the detector (TIFF)
     * Get full pixel mask of the detector
     * See NXmx standard for meaning of pixel values
     *
     * @returns binary Pixel mask in TIFF format (4 byte; unsigned)
     * @throws ApiError
     */
    public static getConfigMaskTiff(): CancelablePromise<Blob> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/config/mask.tiff',
        });
    }

    /**
     * Get user mask of the detector (TIFF)
     * Get user pixel mask of the detector in the actual detector coordinates: 0 - good pixel, 1 - masked
     * @returns binary User mask in TIFF format (4 byte; unsigned)
     * @throws ApiError
     */
    public static getConfigUserMaskTiff(): CancelablePromise<Blob> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/config/user_mask.tiff',
        });
    }

    /**
     * Upload user mask of the detector
     * Should be in `Idle` state.
     * Upload user mask of the detector - this is for example to account for beam stop shadow or misbehaving regions.
     * If detector is conversion mode the mask can be both in raw (1024x512; stacked modules) or converted coordinates.
     * In the latter case - module gaps are ignored and don't need to be assigned value.
     * Mask is expected as TIFF (4-byte; unsigned).
     * 0 - good pixel, other value - masked
     * User mask is stored in NXmx pixel mask (bit 8), as well as used in spot finding and azimuthal integration.
     * User mask is not automatically applied - i.e. pixels with user mask will have a valid pixel value in the images.
     *
     * @param requestBody
     * @returns any All good
     * @throws ApiError
     */
    public static putConfigUserMaskTiff(
        requestBody?: Blob,
    ): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'PUT',
            url: '/config/user_mask.tiff',
            body: requestBody,
            mediaType: 'image/tiff',
            errors: {
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Get pedestal in TIFF format
     * @param gainLevel Gain level (0, 1, 2)
     * @param sc Storage cell number
     * @returns binary Calibration image
     * @throws ApiError
     */
    public static getPreviewPedestalTiff(
        gainLevel: number,
        sc?: number,
    ): CancelablePromise<Blob> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/preview/pedestal.tiff',
            query: {
                'gain_level': gainLevel,
                'sc': sc,
            },
            errors: {
                404: `No calibration recorded so far`,
            },
        });
    }

    /**
     * Get Start message in CBOR format
     * Contains metadata for a dataset (e.g., experimental geometry)
     * @returns binary Return start message
     * @throws ApiError
     */
    public static getImageBufferStartCbor(): CancelablePromise<Blob> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/image_buffer/start.cbor',
            errors: {
                404: `No measurement so far`,
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Get image message in CBOR format
     * Contains full image data and metadata. The image must come from the latest data collection.
     * @param imageNumber Image number. If omitted, the image with the highest number in the image buffer will be provided.
     * @returns binary Return image message
     * @throws ApiError
     */
    public static getImageBufferImageCbor(
        imageNumber?: number,
    ): CancelablePromise<Blob> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/image_buffer/image.cbor',
            query: {
                'image_number': imageNumber,
            },
            errors: {
                404: `Image not present in the buffer - either not yet measured or already replaced by a next image.`,
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Clear image buffer
     * Turns off image buffer for the last data collection. Can be only run when Jungfraujoch is not collecting data.
     * @returns any Done
     * @throws ApiError
     */
    public static postImageBufferClear(): CancelablePromise<any> {
        return __request(OpenAPI, {
            method: 'POST',
            url: '/image_buffer/clear',
            errors: {
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Get status of the image buffers
     * Can be run at any stage of Jungfraujoch operation, including during data collection.
     * The status of the image buffer is volatile during data collection - if data collection goes for more images than available buffer slots,
     * then image might be replaced in the buffer between calling /images and /image.cbor.
     *
     * @returns image_buffer_status Done
     * @throws ApiError
     */
    public static getImageBufferStatus(): CancelablePromise<image_buffer_status> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/image_buffer/status',
            errors: {
                500: `Error within Jungfraujoch code - see output message.`,
            },
        });
    }

    /**
     * Get Jungfraujoch version of jfjoch_broker
     * @returns string Release number of Jungfraujoch
     * @throws ApiError
     */
    public static getVersion(): CancelablePromise<string> {
        return __request(OpenAPI, {
            method: 'GET',
            url: '/version',
        });
    }

}
