#!/bin/bash

VERSION=$(<VERSION)
OPENAPI_VERSION=7.8.0
wget https://repo1.maven.org/maven2/org/openapitools/openapi-generator-cli/${OPENAPI_VERSION}/openapi-generator-cli-${OPENAPI_VERSION}.jar -O openapi-generator-cli.jar

java -jar openapi-generator-cli.jar generate -i broker/jfjoch_api.yaml -o python-client/ -g python --git-host=git.psi.ch --git-repo-id jungfraujoch --git-user-id jungfraujoch --additional-properties=packageName=jfjoch_client,packageVersion=$VERSION
cd python-client
sed -i s/"NAME = \"jfjoch-client\""/"NAME = \"jfjoch_client\""/ setup.py
python3 setup.py sdist bdist_wheel
mv dist/* ..