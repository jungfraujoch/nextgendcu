# Contributing

We actively welcome community contributions.
Please contact [Filip Leonarski](mailto:filip.leonarski@psi.ch), if you like to contribute to the project.
