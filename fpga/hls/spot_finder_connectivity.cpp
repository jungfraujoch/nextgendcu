// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: CERN-OHL-S-2.0

#include "hls_jfjoch.h"

void spot_finder_connectivity(hls::stream<ap_axiu<32,1,1,1>> &data_in,
                              hls::stream<ap_axiu<32,1,1,1>> &data_out,
                              hls::stream<ap_uint<32>> &connectivity_out) {
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE axis register both port=data_in
#pragma HLS INTERFACE axis register both port=data_out
#pragma HLS INTERFACE axis register both port=connectivity_out

    ap_uint<RAW_MODULE_COLS> top_line;
    ap_uint<RAW_MODULE_COLS> mid_line;
    ap_uint<RAW_MODULE_COLS> bottom_line;

    ap_axiu<32,1,1,1> val;
    data_in >> val;

    while (!val.user) {
        top_line = 0;
        mid_line = 0;
        bottom_line = 0;

        for (int line = 0; line < RAW_MODULE_LINES; line++) {
#pragma HLS PIPELINE II=32
            for (int col = 0; col < RAW_MODULE_COLS / 32; col++) {
                bottom_line(col * 32 + 31, col * 32) = val.data;
                data_out << val;
                data_in >> val;
            }
            if (line != 0) {
                for (int col = 0; col < RAW_MODULE_COLS / 32; col++) {
                    ap_uint<32> output = 0;
                    for (int i = 0; i < 32; i++) {
                        size_t pos = col * 32 + i;
                        ap_uint<1> connect_mat = 0;
                        if (pos != 0) {
                            connect_mat |= top_line[pos - 1];
                            connect_mat |= mid_line[pos - 1];
                            connect_mat |= bottom_line[pos - 1];
                        }
                        connect_mat |= top_line[pos];
                        connect_mat |= bottom_line[pos];
                        if (pos != RAW_MODULE_COLS - 1) {
                            connect_mat |= top_line[pos + 1];
                            connect_mat |= mid_line[pos + 1];
                            connect_mat |= bottom_line[pos + 1];
                        }
                        output[i] = connect_mat;
                    }
                    connectivity_out << output;
                }
            }
            top_line = mid_line;
            mid_line = bottom_line;
        }
        for (int i = 0; i < 16; i++) {
            data_out << val;
            data_in >> val;
        }

        for (int col = 0; col < RAW_MODULE_COLS / 32; col++) {
#pragma HLS PIPELINE II=1
            ap_uint<32> output = 0;
            for (int i = 0; i < 32; i++) {
                size_t pos = col * 32 + i;
                ap_uint<1> connect_mat = 0;
                if (pos != 0) {
                    connect_mat |= top_line[pos - 1];
                    connect_mat |= mid_line[pos - 1];
                }
                connect_mat |= top_line[pos];
                if (pos != RAW_MODULE_COLS - 1) {
                    connect_mat |= top_line[pos + 1];
                    connect_mat |= mid_line[pos + 1];
                }
                output[i] = connect_mat;
            }
            connectivity_out << output;
        }
    }
    data_out << val;
}
