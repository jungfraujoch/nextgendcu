// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: CERN-OHL-S-2.0

#include "hls_jfjoch.h"

void axis_64_to_512(hls::stream<ap_axiu<64,1,1,1>> &data_in,
                    hls::stream<ap_uint<512>> &data_out) {
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE axis register both port=data_in
#pragma HLS INTERFACE axis register both port=data_out

    ap_axiu<64,1,1,1> packet_64;
    data_in >> packet_64;

    while (!packet_64.user) {
#pragma HLS PIPELINE II=8
        ap_uint<512> val = 0;
        val(63,0) = packet_64.data;
        for (int i = 1; i < 8; i++) {
            data_in >> packet_64;
            val(i * 64 + 63, i * 64) = packet_64.data;
        }
        data_out << val;
        data_in >> packet_64;
    }
}

void axis_32_to_512(hls::stream<ap_axiu<32,1,1,1>> &data_in,
                    hls::stream<ap_uint<512>> &data_out) {
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE axis register both port=data_in
#pragma HLS INTERFACE axis register both port=data_out

    ap_axiu<32,1,1,1> packet_32;
    data_in >> packet_32;

    while (!packet_32.user) {
#pragma HLS PIPELINE II=16
        ap_uint<512> val = 0;
        val(31,0) = packet_32.data;
        for (int i = 1; i < 16; i++) {
            data_in >> packet_32;
            val(i * 32 + 31, i * 32) = packet_32.data;
        }
        data_out << val;
        data_in >> packet_32;
    }
}
