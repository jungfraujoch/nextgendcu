// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: CERN-OHL-S-2.0

#include "hls_jfjoch.h"

void frame_summation_reorder_compl(STREAM_768 &data_in,
                                   STREAM_768 &data_out,
                                   hls::stream<axis_completion > &s_axis_completion,
                                   hls::stream<axis_completion > &m_axis_completion) {
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE axis register both port=data_in
#pragma HLS INTERFACE axis register both port=data_out
#pragma HLS INTERFACE axis register both port=s_axis_completion
#pragma HLS INTERFACE axis register both port=m_axis_completion

    axis_completion completions[MAX_FPGA_SUMMATION * MAX_MODULES_FPGA];
    ap_uint<MAX_FPGA_SUMMATION> completion_mask[MAX_MODULES_FPGA];
    ap_uint<9> completion_count[MAX_MODULES_FPGA]; // must represent 256

    ap_uint<64> curr_frame_number_prefix[MAX_MODULES_FPGA];

    for (int i = 0; i < MAX_MODULES_FPGA; i++) {
        completion_mask[i] = 0;
        completion_count[i] = 0;
        curr_frame_number_prefix[i] = 0;
    }

    packet_768_t packet_in;
    {
#pragma HLS PROTOCOL fixed
        data_in >> packet_in;
        ap_wait();
        data_out << packet_in;
        ap_wait();
    }
    ap_uint<8> sum = ACT_REG_NSUMMATION(packet_in.data); // 0..255

    axis_completion c;
    s_axis_completion >> c;
    while (!c.last) {
#pragma HLS PIPELINE II=16
        ap_uint<64> frame_number_prefix = c.frame_number / (sum + 1);
        ap_uint<8> frame_number_loc = c.frame_number % (sum + 1); // 0..255
        ap_uint<7> module = c.module;
        if (frame_number_prefix > curr_frame_number_prefix[module]) {
            for (int i = 0; i <= sum; i++) {
                axis_completion cmpl = completions[module * MAX_FPGA_SUMMATION + i];
                if (completion_count[module] != sum + 1)
                    cmpl.ignore = 1;
                if (completion_mask[module][i])
                    m_axis_completion << cmpl;
            }
            completions[module * MAX_FPGA_SUMMATION + frame_number_loc] = c;
            completion_mask[module] = 1 << frame_number_loc;
            completion_count[module] = 1;
            curr_frame_number_prefix[module] = frame_number_prefix;
        } else if (frame_number_prefix == curr_frame_number_prefix[module]) {
            completions[module * MAX_FPGA_SUMMATION + frame_number_loc] = c;
            completion_mask[module][frame_number_loc] = 1;
            completion_count[module] += 1;
            curr_frame_number_prefix[module] = frame_number_prefix;
        } else {
            c.ignore = 1;
            m_axis_completion << c;
        }
        s_axis_completion >> c;
    }
    for (int module = 0; module < MAX_MODULES_FPGA; module++) {
#pragma HLS PIPELINE II=16
        for (int i = 0; i <= sum; i++) {
            axis_completion cmpl = completions[module * MAX_FPGA_SUMMATION + i];
            if (completion_count[module] != sum + 1)
                cmpl.ignore = 1;
            if (completion_mask[module][i])
                m_axis_completion << cmpl;
        }
    }

    m_axis_completion << c;

    data_in >> packet_in;
    data_out << packet_in;
}
