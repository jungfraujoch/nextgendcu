// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: CERN-OHL-S-2.0

#include "hls_jfjoch.h"

inline ap_uint<32> count_pixels(ap_uint<32> &in) {
#pragma HLS INLINE
    ap_uint<32> ret = 0;
    for (int i = 0; i < 32; i++)
        ret += in[i];
    return ret;
}

void spot_finder_merge(hls::stream<ap_axiu<32,1,1,1>> &data_in,
                       hls::stream<ap_uint<32>> &connectivity_in,
                       hls::stream<ap_axiu<32,1,1,1>> &data_out,
                       volatile ap_uint<32> &in_min_pix_per_spot) {
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE axis register both port=data_in
#pragma HLS INTERFACE axis register both port=data_out
#pragma HLS INTERFACE axis register both port=connectivity_in
#pragma HLS INTERFACE ap_none register port=in_min_pix_per_spot

    ap_axiu<32,1,1,1> val = data_in.read();
    ap_uint<32> conn;

    while (!val.user) {
        ap_uint<32> min_pix_per_spot = in_min_pix_per_spot;
        ap_uint<32> strong_pixel_count = 0;

        for (int i = 0; i < 16384; i++) {
#pragma HLS PIPELINE II=1
            connectivity_in >> conn;

            if (min_pix_per_spot > 1)
                val.data = val.data & conn;

            strong_pixel_count += count_pixels(val.data);
            data_out << val;
            data_in >> val;
        }

        for (int i = 0; i < 16; i++) {
            if (i == 2)
                val.data = strong_pixel_count;
            data_out << val;
            data_in >> val;
        }
    }
    data_out << val;
}
