// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: CERN-OHL-S-2.0

#ifndef JUNGFRAUJOCH_HLS_JFJOCH_H
#define JUNGFRAUJOCH_HLS_JFJOCH_H

#include "../pcie_driver/jfjoch_fpga.h"

#include <ap_int.h>
#include <cstdint>
#include <cstring>
#include <hls_burst_maxi.h>

#ifndef __SYNTHESIS__
#define ap_wait()
#endif

#ifndef JFJOCH_HLS_NOSYNTH
#include <ap_axi_sdata.h>
#include <hls_stream.h>
#include <hls_math.h>
#else

#include "parallel_stream.h"

#include <cmath>
namespace hls {
    inline bool isfinite(float f) {return std::isfinite(f);}
    // inline float sqrt(float f) {return std::sqrt(f); }
    template <class T> float sqrt(const T &f) {return std::round(std::sqrt(f.to_float())); }
}
#endif

#include "../pcie_driver/jfjoch_fpga.h"

typedef ap_ufixed<16,2, AP_RND_CONV> gainG0_t;
typedef ap_ufixed<16,4, AP_RND_CONV> gainG1_t;
typedef ap_ufixed<16,6, AP_RND_CONV> gainG2_t;
typedef ap_ufixed<16,10, AP_RND_CONV> integration_factor_t;
typedef ap_ufixed<16,9, AP_RND_CONV> xray_d_t;

typedef ap_ufixed<32,12,AP_RND_CONV> one_over_energy_t;

typedef ap_uint<256> hbm256_t;

#ifdef JFJOCH_HLS_NOSYNTH
template<int D,int U,int TI,int TD>
  struct ap_axiu{
    ap_uint<D>       data;
    ap_uint<(D+7)/8> keep;
    ap_uint<(D+7)/8> strb;
    ap_uint<U>       user;
    ap_uint<1>       last;
    ap_uint<TI>      id;
    ap_uint<TD>      dest;
  };
#endif

typedef ap_axiu<512,  1, 1, 1> packet_512_t;
typedef ap_axiu<768,  1, 1, 1> packet_768_t;

typedef hls::stream<packet_512_t> AXI_STREAM;
typedef hls::stream<packet_512_t> STREAM_512;
typedef hls::stream<packet_768_t> STREAM_768;

#define ACT_REG_MODE(x)                   ((x)(32 ,   0)) // 32 bit
#define ACT_REG_ENERGY_KEV_FLOAT(x)       ((x)(63 ,  32)) // 32 bit
#define ACT_REG_NFRAMES(x)                ((x)(95 ,  64)) // 32 bit
#define ACT_REG_NMODULES(x)               ((x)(132, 128)) // 5 bit (0..31)
#define ACT_REG_NSTORAGE_CELLS(x)         ((x)(148, 144)) // 5 bit
#define ACT_REG_NSUMMATION(x)             ((x)(167, 160)) // 8 bit (0..255)
#define ACT_REG_SQRTMULT(x)               ((x)(175, 168)) // 8 bit (0..255)
#define ACT_REG_THRESHOLD(x)              ((x)(255,224)) // 32 bit

struct axis_datamover_ctrl {
    ap_uint<40+64> data;
};

struct axis_addr {
    ap_uint<64> frame_number;
    ap_uint<64> exptime;
    ap_uint<64> timestamp;
    ap_uint<64> bunchid;
    ap_uint<32> debug;
    ap_uint<8> packet_length; // in AXI-Stream beats = 64 byte; possible values 0..255
    ap_uint<5> module;
    ap_uint<9> eth_packet;
    ap_uint<4> det_type;
    ap_uint<1> column;
    ap_uint<1> row;
    ap_uint<4> detector_type;
    ap_uint<1> last;
};

struct axis_completion {
    ap_uint<512> packet_mask;
    ap_uint<64> frame_number;
    ap_uint<64> exptime;
    ap_uint<64> timestamp;
    ap_uint<64> bunchid;
    ap_uint<32> debug;
    ap_uint<16> handle;
    ap_uint<16> packet_count;
    ap_uint<5> module;
    ap_uint<4> detector_type;
    ap_uint<1> last;
    ap_uint<1> ignore;
    ap_uint<1> pedestal;
};

union float_uint32 {
    float f;
    uint32_t u;
};

void setup_datamover (hls::stream<axis_datamover_ctrl> &datamover_cmd_stream, uint64_t address, size_t bytes_to_write);

void axis_64_to_512(hls::stream<ap_axiu<64,1,1,1>> &data_in,
                    hls::stream<ap_uint<512>> &data_out);

void axis_32_to_512(hls::stream<ap_axiu<32,1,1,1>> &data_in,
                    hls::stream<ap_uint<512>> &data_out);

template<int N> ap_uint<N*32> pack32(ap_int<N> in[32]) {
#pragma HLS INLINE
    ap_uint<N*32> out;
    for (int i = 0; i < 32; i++) {
        for (int j = 0; j < N; j++)
            out[i*N+j] = in[i][j];
    }
    return out;
}

template<int N> ap_uint<N*32> pack32(ap_uint<N> in[32]) {
#pragma HLS INLINE
    ap_uint<N*32> out;
    for (int i = 0; i < 32; i++) {
        for (int j = 0; j < N; j++)
            out[i*N+j] = in[i][j];
    }
    return out;
}

template <class T> void unpack_2xhbm_to_32x16bit(const ap_uint<256> in1, const ap_uint<256> in2, T out[32]) {
#pragma HLS INLINE
    for (int i = 0; i < 16; i ++) {
        for (int j = 0; j < 16; j ++) {
            out[i][j] = in1[i*16+j];
            out[i+16][j] = in2[i*16+j];
        }
    }
}

template<int N> void unpack32(const ap_uint<N*32>& in, ap_int<N> out[32]) {
#pragma HLS INLINE
    for (int i = 0; i < 32; i++)
        for (int j = 0; j < N; j++)
            out[i][j] = in[i*N+j];
}

template<int N> void unpack32(const ap_uint<N*32>& in, ap_uint<N> out[32]) {
#pragma HLS INLINE
    for (int i = 0; i < 32; i++)
        for (int j = 0; j < N; j++)
            out[i][j] = in[i*N+j];
}

template<int N> void unpack16(const ap_uint<N*16>& in, ap_uint<N> out[16]) {
#pragma HLS INLINE
    for (int i = 0; i < 16; i++)
        for (int j = 0; j < N; j++)
            out[i][j] = in[i*N+j];
}

template<int N> void unpack64(const ap_uint<N*64>& in, ap_uint<N> out[64]) {
#pragma HLS INLINE
    for (int i = 0; i < 64; i++)
        for (int j = 0; j < N; j++)
            out[i][j] = in[i*N+j];
}

inline void setup_datamover (hls::stream<axis_datamover_ctrl> &datamover_cmd_stream, uint64_t address, size_t bytes_to_write) {
#pragma HLS INLINE
    axis_datamover_ctrl msg;
    msg.data = 0;
    msg.data(22,0) = bytes_to_write;
    msg.data[23] = 1; // INCR burst type
    msg.data[30] = 1; // Assert LAST on the last command
    msg.data(31+64,32) = address;
    datamover_cmd_stream << msg;
}

inline ap_uint<16> get_header_field_16(ap_uint<512> data, size_t position) {
    ap_uint<16> tmp = data(position+15, position);
    ap_uint<16> retval;
    // Swap endian
    retval(15,8) = tmp(7,0);
    retval(7,0) = tmp(15,8);
    return retval;
}

template <class T>
T float_conv(const ap_uint<32> &input) {
#pragma HLS INLINE
    float_uint32 tmp;
    tmp.u = input;
    if (hls::isfinite(tmp.f)) {
        T ret = tmp.f;
        return ret;
    } else {
        T ret = 0;
        return ret;
    }
}

static const uint8_t ECHO_REQUEST  = 0x08;
static const uint8_t ECHO_REPLY    = 0x00;
static const uint8_t PROTOCOL_ICMP = 0x01;
static const uint8_t PROTOCOL_UDP  = 0x11;
static const uint16_t ETHER_IP     = 0x0800;
static const uint16_t ETHER_ARP    = 0x0806;
static const ap_uint<48> MAC_BROADCAST = 0xFFFFFFFFFFFF;
static const ap_uint<32> IPV4_BROADCAST = 0xFFFFFFFF;

static const uint32_t eth_payload_pos = 14 * 8; // 112 bits
static const uint32_t ipv4_payload_pos = eth_payload_pos + 160; // 112 + 160 = 272 bits
static const uint32_t udp_payload_pos = ipv4_payload_pos + 64; // 112 + 160 + 64 = 336 bits (42 bytes)

// Network cores
#define UDP_METADATA_STREAM_WIDTH 48
#define udp_metadata_dest_port(x)     x(15,  0)
#define udp_metadata_payload_size(x)  x(31, 16)
#define udp_metadata_eth_err(x)       x[32]
#define udp_metadata_len_err(x)       x[33]

// Packet stream handling

enum rcv_state_t {RCV_WAIT_FOR_START = 0, RCV_WAIT_FOR_START_LOW = 1, RCV_START = 2, RCV_INIT = 3, RCV_GOOD = 4,
    RCV_FLUSH = 5, RCV_LAST = 6, RCV_FLUSH_IDLE = 7, RCV_IGNORE = 8};


// spot finder definitions
#define SUM_BITWIDTH   37 // 24 + 12 + 1
#define SUM2_BITWIDTH  61 // 2*24 + 12 + 1
#define VALID_BITWIDTH 12 // 12
#define LINES_PER_GO 15

// internal stream within spot finder
struct spot_finder_packet {
    ap_uint<768> data;
    ap_uint<32> mask;
    ap_uint<32> strong_pixel;
    ap_int<32> count_threshold;
    ap_uint<32> snr_threshold;
    ap_uint<1> user;
    ap_uint<1> last;
};

#endif
