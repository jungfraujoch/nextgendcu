// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: CERN-OHL-S-2.0

#include "hls_jfjoch.h"

void pixel_mask(STREAM_768 &data_in,
                STREAM_768 &data_out,
                const uint32_t pixel_mask[MAX_MODULES_FPGA*RAW_MODULE_SIZE/32],
                hls::stream<axis_completion > &s_axis_completion,
                hls::stream<axis_completion > &m_axis_completion) {
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE register both axis port=data_in
#pragma HLS INTERFACE register both axis port=data_out
#pragma HLS INTERFACE register both axis port=m_axis_completion
#pragma HLS INTERFACE register both axis port=s_axis_completion

#pragma HLS INTERFACE mode=m_axi port=pixel_mask bundle=pixel_mask depth=65536 offset=off \
		max_read_burst_length=64 max_write_burst_length=2 latency=5 num_write_outstanding=1 num_read_outstanding=2

    packet_768_t packet;
    {
#pragma HLS PROTOCOL fixed
        data_in >> packet;
        ap_wait();
        data_out << packet;
        ap_wait();
    }

    ap_uint<32> data_collection_mode = ACT_REG_MODE(packet.data);
    ap_uint<1> apply_mask = ((data_collection_mode & MODE_APPLY_PIXEL_MASK) ? 1 : 0);

    axis_completion cmpl;
    s_axis_completion >> cmpl;
    while (!cmpl.last) {
        m_axis_completion << cmpl;
        for (int i = 0; i < RAW_MODULE_SIZE * sizeof(uint16_t) / 64; i++) {
#pragma HLS PIPELINE II=1
            data_in >> packet;

            ap_uint<32> mask = pixel_mask[(cmpl.module * RAW_MODULE_SIZE / 32) + i];
            ap_int<24> in_val[32];
            unpack32(packet.data, in_val);
            for (int j = 0; j < 32 ; j++) {
                if (apply_mask && (mask[j] != 0))
                    in_val[j] = INT24_MIN;
            }
            packet.data = pack32(in_val);

            data_out << packet;
        }
        s_axis_completion >> cmpl;
    }
    m_axis_completion << cmpl;

    data_in >> packet;
    data_out << packet;
}