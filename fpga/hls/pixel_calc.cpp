// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "hls_jfjoch.h"

void pixel_calc(STREAM_768 &data_in,
                STREAM_768 &data_out,
                hls::stream<ap_uint<128>> &calc_out) {
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE register both axis port=data_in
#pragma HLS INTERFACE register both axis port=data_out
#pragma HLS INTERFACE register both axis port=calc_out

    packet_768_t packet;
    {
#pragma HLS PROTOCOL fixed
        data_in >> packet;
        ap_wait();
        data_out << packet;
        ap_wait();
    }

    data_in >> packet;

    while (!packet.user) {
        ap_uint<20> err_count[32];
        ap_uint<20> sat_count[32];
        ap_int<24> max_val[32];
        ap_int<24> min_val[32];

        for (int i = 0; i < 32; i++) {
            err_count[i] = 0;
            sat_count[i] = 0;
            max_val[i] = INT24_MIN;
            min_val[i] = INT24_MAX;
        }

        for (int i = 0; i < RAW_MODULE_SIZE / 32; i++) {
#pragma HLS PIPELINE II=1
            data_out << packet;
            ap_int<24> in_val[32];
            unpack32(packet.data, in_val);

            for (int j = 0; j < 32; j++) {
                if (in_val[j] == INT24_MIN)
                    err_count[j]++;
                else if (in_val[j] == INT24_MAX)
                    sat_count[j]++;
                else {
                    if (in_val[j] > max_val[j])
                        max_val[j] = in_val[j];
                    if (in_val[j] < min_val[j])
                        min_val[j] = in_val[j];
                }

            }

            data_in >> packet;
        }

        ap_uint<32> err_out = err_count[0];
        ap_uint<32> sat_out = sat_count[0];
        ap_int<32> max_out = max_val[0];
        ap_int<32> min_out = min_val[0];
        for (int i = 1; i < 32; i++) {
            err_out += err_count[i];
            sat_out += sat_count[i];
            if (max_val[i] > max_out)
                max_out = max_val[i];
            if (min_val[i] < min_out)
                min_out = min_val[i];
        }
        ap_int<128> out = 0;
        out(0,31) = err_out;
        out(63,32) = sat_out;
        out(95,64) = max_out;
        out(127,96) = min_out;
        calc_out << out;
    }
    data_out << packet;
}
