// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: CERN-OHL-S-2.0

#include "hls_jfjoch.h"

void save_to_hbm_data(STREAM_768 &data_in,
                      STREAM_768 &data_out,
                      hls::stream<ap_axiu<256,1,1,1> > &hbm_out_0,
                      hls::stream<ap_axiu<256,1,1,1> > &hbm_out_1,
                      hls::stream<ap_axiu<256,1,1,1> > &hbm_out_2) {
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE register both axis port=data_in
#pragma HLS INTERFACE register both axis port=data_out
#pragma HLS INTERFACE register both axis port=hbm_out_0
#pragma HLS INTERFACE register both axis port=hbm_out_1
#pragma HLS INTERFACE register both axis port=hbm_out_2

    packet_768_t packet_in;
    {
#pragma HLS PROTOCOL fixed
        data_in >> packet_in;
        ap_wait();
        data_out << packet_in;
        ap_wait();
    }

    data_in >> packet_in;

    Loop_good_packet:
    while (! packet_in.user) {
        // Process one UDP packet per iteration
#pragma HLS PIPELINE II=1

            ap_axiu<256,1,1,1> packet_out;
            packet_out.keep = UINT32_MAX;
            packet_out.strb = UINT32_MAX;
            packet_out.user = 0;
            packet_out.dest = 0;
            packet_out.id = 0;
            packet_out.last = packet_in.last;

            packet_out.data = packet_in.data(255, 0);
            hbm_out_0 << packet_out;

            packet_out.data = packet_in.data(511, 256);
            hbm_out_1 << packet_out;

            packet_out.data = packet_in.data(767, 512);
            hbm_out_2 << packet_out;

            data_in >> packet_in;
    }

    data_out << packet_in;
}