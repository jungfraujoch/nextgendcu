// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: CERN-OHL-S-2.0

#include "hls_jfjoch.h"

void axis_broadcast(STREAM_512 &data_in,
                    STREAM_512 &data_out_0,
                    STREAM_512 &data_out_1) {
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE axis register both port=data_in
#pragma HLS INTERFACE axis register both port=data_out_0
#pragma HLS INTERFACE axis register both port=data_out_1
    packet_512_t packet;
    data_in >> packet;
    data_out_0 << packet;
    data_out_1 << packet;

    data_in >> packet;
    while (!packet.user) {
#pragma HLS PIPELINE II=1
        data_out_0 << packet;
        data_out_1 << packet;
        data_in >> packet;
    }
    data_out_0 << packet;
    data_out_1 << packet;
}
