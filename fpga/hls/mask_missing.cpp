// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: CERN-OHL-S-2.0

#include "hls_jfjoch.h"

void mask_missing(STREAM_768 &data_in,
                 STREAM_768 &data_out,
                 hls::stream<axis_completion > &s_axis_completion,
                 hls::stream<axis_completion > &m_axis_completion) {
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE register both axis port=data_in
#pragma HLS INTERFACE register both axis port=data_out
#pragma HLS INTERFACE register both axis port=m_axis_completion
#pragma HLS INTERFACE register both axis port=s_axis_completion

    packet_768_t packet;
    {
#pragma HLS PROTOCOL fixed
        data_in >> packet;
        ap_wait();
        data_out << packet;
        ap_wait();
    }
    axis_completion cmpl;
    s_axis_completion >> cmpl;
    while (!cmpl.last) {
        m_axis_completion << cmpl;
        for (int i = 0; i < RAW_MODULE_SIZE * sizeof(uint16_t) / 64; i++) {
#pragma HLS PIPELINE II=1
            data_in >> packet;
            if (!cmpl.packet_mask[i / 32]) {
                ap_int<24> in_val[32];
                for (int j = 0; j < 32 ; j++)
                    in_val[j] = INT24_MIN;
                packet.data = pack32(in_val);
            }
            data_out << packet;
        }
        s_axis_completion >> cmpl;
    }
    m_axis_completion << cmpl;

    data_in >> packet;
    data_out << packet;
}