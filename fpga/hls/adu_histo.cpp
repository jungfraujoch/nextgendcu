// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: CERN-OHL-S-2.0

#include "hls_jfjoch.h"

void adu_histo(STREAM_768 &data_in,
               STREAM_768 &data_out,
               hls::stream<ap_uint<512>> &result_out,
               hls::stream<axis_completion > &s_axis_completion,
               hls::stream<axis_completion > &m_axis_completion) {
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE register both axis port=data_in
#pragma HLS INTERFACE register both axis port=data_out
#pragma HLS INTERFACE register both axis port=result_out
#pragma HLS INTERFACE register both axis port=m_axis_completion
#pragma HLS INTERFACE register both axis port=s_axis_completion

    ap_uint<24> count[64][ADU_HISTO_BIN_COUNT]; // log2(256*512*1024/64) = 23
#pragma HLS BIND_STORAGE variable=count type=ram_t2p impl=bram
#pragma HLS ARRAY_PARTITION variable=count type=complete dim=1

    for (int j = 0; j < ADU_HISTO_BIN_COUNT; j++) {
#pragma HLS PIPELINE II=1
        for (int i = 0; i < 64; i++)
            count[i][j] = 0;
    }

    ap_uint<16> in_val[32];
    ap_uint<24> in_val24[32];
    ap_uint<256> bins_0, bins_1;

    packet_768_t packet_in;
    {
#pragma HLS PROTOCOL fixed
        data_in >> packet_in;
        ap_wait();
        data_out << packet_in;
        ap_wait();
    }
    ap_uint<8> sum = ACT_REG_NSUMMATION(packet_in.data); // 0..255


    axis_completion cmpl;
    s_axis_completion >> cmpl;
    while (!cmpl.last) {
        ap_uint<9> sum_local = sum + 1;
        if (cmpl.pedestal)
            sum_local = 1;
        for (int s = 0; s < sum_local; s++) {
            m_axis_completion << cmpl;
            for (int i = 0; i < RAW_MODULE_SIZE / (32 * 2); i++) {
#pragma HLS PIPELINE II=2
                for (int k = 0; k < 2; k++) {
                    data_in >> packet_in;
                    data_out << packet_in;

                    unpack32(packet_in.data, in_val24);

                    for (int j = 0; j < 32; j++) {
                        in_val[j] = in_val24[j](15,0);
                        count[k * 32 + j][in_val[j] / ADU_HISTO_BIN_WIDTH] += (cmpl.packet_mask[i / 64] ? 1 : 0);
                    }
                }
            }
            s_axis_completion >> cmpl;
        }
        for (int i = 0; i < ADU_HISTO_BIN_COUNT / 16; i++) {
#pragma HLS PIPELINE II=8
            ap_uint<512> val = 0;
            for (int k = 0; k < 16; k++) {
                ap_uint<32> tmp = 0;
                for (int j = 0; j < 64; j++) {
                    tmp += count[j][i * 16 + k];
                    count[j][i * 16 + k] = 0;
                }
                val(k * 32 + 31, k * 32) = tmp;
            }
            result_out << val;
        }
    }
    m_axis_completion << cmpl;

    data_in >> packet_in;
    data_out << packet_in;
}
