// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: CERN-OHL-S-2.0

#include "hls_jfjoch.h"

void timer_host(STREAM_512 &data_in, STREAM_512 &data_out,
                volatile uint64_t &stalls,
                volatile uint64_t &beats) {
#pragma HLS INTERFACE register both axis port=data_in
#pragma HLS INTERFACE register both axis port=data_out
#pragma HLS INTERFACE register ap_vld port=stalls
#pragma HLS INTERFACE register ap_vld port=beats
#pragma HLS INTERFACE ap_ctrl_none port=return
    packet_512_t packet_in;

    {
#pragma HLS PROTOCOL fixed
        data_in >> packet_in;
        ap_wait();
        data_out << packet_in;
        ap_wait();
    }
    beats = 0;
    uint64_t stalls_internal = 0;
    stalls = 0;
    uint64_t beats_internal = 0;


    data_in >> packet_in;
    while (!packet_in.user) {
#pragma HLS PIPELINE II=1
        if (data_out.full()) {
            if (stalls_internal < UINT64_MAX)
                stalls_internal++;
        } else {
            data_out << packet_in;
            data_in >> packet_in;
            beats_internal++;
        }
        beats = beats_internal;
        stalls = stalls_internal;
    }
    data_out << packet_in;
}
