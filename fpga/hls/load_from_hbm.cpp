// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: CERN-OHL-S-2.0

#include "hls_jfjoch.h"

void load_from_hbm(STREAM_768 &data_in,
                   STREAM_768 &data_out,
                   hls::stream<axis_completion > &s_axis_completion,
                   hls::stream<axis_completion > &m_axis_completion,
                   hls::stream<ap_uint<16> > &m_axis_free_handles,
                   hls::stream<ap_axiu<256,1,1,1> > &hbm_in_0,
                   hls::stream<ap_axiu<256,1,1,1> > &hbm_in_1,
                   hls::stream<ap_axiu<256,1,1,1> > &hbm_in_2,
                   hls::stream<axis_datamover_ctrl> &datamover_0_cmd,
                   hls::stream<axis_datamover_ctrl> &datamover_1_cmd,
                   hls::stream<axis_datamover_ctrl> &datamover_2_cmd,
                   volatile ap_uint<1> &idle,
                   ap_uint<32> hbm_size_bytes) {
#pragma HLS INTERFACE ap_ctrl_none port=return
#pragma HLS INTERFACE register both axis port=data_in
#pragma HLS INTERFACE register both axis port=data_out
#pragma HLS INTERFACE register both axis port=m_axis_completion
#pragma HLS INTERFACE register both axis port=s_axis_completion
#pragma HLS INTERFACE register both axis port=m_axis_free_handles
#pragma HLS INTERFACE register both axis port=hbm_in_0
#pragma HLS INTERFACE register both axis port=hbm_in_1
#pragma HLS INTERFACE register both axis port=hbm_in_2
#pragma HLS INTERFACE register both axis port=datamover_0_cmd
#pragma HLS INTERFACE register both axis port=datamover_1_cmd
#pragma HLS INTERFACE register both axis port=datamover_2_cmd
#pragma HLS INTERFACE register ap_none port=idle

#pragma HLS INTERFACE mode=ap_none port=hbm_size_bytes

    idle = 1;

    for (ap_uint<16> i = 0; i < hbm_size_bytes * 4 / (RAW_MODULE_SIZE * sizeof(uint16_t)); i++)
        m_axis_free_handles << i;

	packet_768_t  packet;

    {
#pragma HLS PROTOCOL fixed
        data_in >> packet;
        ap_wait();
        data_out << packet;
        ap_wait();
        idle = 0;
        ap_wait();
    }

    ap_uint<32> offset_hbm_0 = 22 * hbm_size_bytes / 32;
    ap_uint<32> offset_hbm_1 = 24 * hbm_size_bytes / 32;
    ap_uint<32> offset_hbm_2 = 26 * hbm_size_bytes / 32;

    axis_completion cmpl;
    s_axis_completion >> cmpl;
    while (!cmpl.last) {
        if (!cmpl.ignore) {
            m_axis_completion << cmpl;

            size_t offset = ((cmpl.handle / 2) * RAW_MODULE_SIZE * sizeof(uint16_t)) / 64;
            if (cmpl.handle % 2 == 1)
                offset += hbm_size_bytes / 32;

            setup_datamover(datamover_0_cmd, (offset_hbm_0 + offset) * 32, RAW_MODULE_SIZE * sizeof(uint16_t) / 2);
            setup_datamover(datamover_1_cmd, (offset_hbm_1 + offset) * 32, RAW_MODULE_SIZE * sizeof(uint16_t) / 2);
            setup_datamover(datamover_2_cmd, (offset_hbm_2 + offset) * 32, RAW_MODULE_SIZE * sizeof(uint16_t) / 2);

            for (int i = 0; i < RAW_MODULE_SIZE * sizeof(uint16_t) / 64; i++) {
#pragma HLS PIPELINE II=1
                ap_axiu<256, 1, 1, 1> packet_in_0;
                ap_axiu<256, 1, 1, 1> packet_in_1;
                ap_axiu<256, 1, 1, 1> packet_in_2;

                hbm_in_0 >> packet_in_0;
                hbm_in_1 >> packet_in_1;
                hbm_in_2 >> packet_in_2;

                packet_768_t packet_out;
                packet_out.data(255, 0) = packet_in_0.data;
                packet_out.data(511, 256) = packet_in_1.data;
                packet_out.data(767, 512) = packet_in_2.data;
                packet_out.last = (i == RAW_MODULE_SIZE * sizeof(uint16_t) / 64 - 1);
                packet_out.id = 0;
                packet_out.dest = 0;
                packet_out.keep = UINT64_MAX;
                packet_out.strb = UINT64_MAX;
                packet_out.user = 0;
                data_out << packet_out;
            }
        }
        m_axis_free_handles << cmpl.handle;
        s_axis_completion >> cmpl;
    }
    m_axis_completion << cmpl;

    m_axis_free_handles << UINT16_MAX;

    data_in >> packet;
    data_out << packet;
    idle = 1;
}
