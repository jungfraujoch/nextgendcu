// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: CERN-OHL-S-2.0

`timescale 1ns / 1ps

module eth_4x10g_leds(
    input stat_rx_status_0,
    input stat_rx_status_1,
    input stat_rx_status_2,
    input stat_rx_status_3,
    output led_green,
    output led_yellow
);
    assign led_green = stat_rx_status_0 && stat_rx_status_1 && stat_rx_status_2 && stat_rx_status_3;
    assign led_yellow = (stat_rx_status_0 || stat_rx_status_1 || stat_rx_status_2 || stat_rx_status_3) && !led_green;
endmodule
