// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: CERN-OHL-S-2.0

// parameters imported from source files
`define JFJOCH_VARIANT 32'h0 
`define JFJOCH_SYNTH_TIME 32'd0

`define JFJOCH_FPGA_MAJOR_VERSION 8'd1
`define JFJOCH_FPGA_MINOR_VERSION 8'd0
`define JFJOCH_FPGA_PATCH_VERSION 8'd0
`define JFJOCH_FPGA_PRERELEASE 64'h31332E6372

`define GIT_SHA1 32'h
`define MAX_MODULES_FPGA 32'd16
`define HBM_SIZE_BYTES 32'h20000000

`define ADDR_AP_CTRL             16'h0000
`define ADDR_MAX_MODULES_FPGA    16'h0004
`define ADDR_JFJOCH_SYNTH_TIME   16'h0008
`define ADDR_GIT_SHA1            16'h000C

`define ADDR_JFJOCH_MAJOR_VER    16'h0010
`define ADDR_JFJOCH_MINOR_VER    16'h0014
`define ADDR_JFJOCH_PATCH_VER    16'h0018
`define ADDR_JFJOCH_VARIANT      16'h001C

`define ADDR_HBM_SIZE            16'h0020
`define ADDR_RUN_COUNTER         16'h0024
`define ADDR_STALLS_HOST_LO      16'h0028
`define ADDR_STALLS_HOST_HI      16'h002C

`define ADDR_STALLS_HBM_LO       16'h0030
`define ADDR_STALLS_HBM_HI       16'h0034
`define ADDR_FIFO_STATUS         16'h0038

`define ADDR_PACKETS_PROC_LO     16'h0040
`define ADDR_PACKETS_PROC_HI     16'h0044
`define ADDR_PACKETS_UDP_LO      16'h0048
`define ADDR_PACKETS_UDP_HI      16'h004C

`define ADDR_PACKETS_SLS_LO      16'h0050
`define ADDR_PACKETS_SLS_HI      16'h0054
`define ADDR_PACKETS_ERR_LEN     16'h0058
`define ADDR_PACKETS_ERR_ETH     16'h005C

`define ADDR_BEATS_HBM_LO        16'h0060
`define ADDR_BEATS_HBM_HI        16'h0064
`define ADDR_BEATS_HOST_LO       16'h0068
`define ADDR_BEATS_HOST_HI       16'h006C

`define ADDR_PULSEID_LO          16'h0070
`define ADDR_PULSEID_HI          16'h0074

`define ADDR_JFJOCH_PRERELEASE_1 16'h0078
`define ADDR_JFJOCH_PRERELEASE_2 16'h007C

`define ADDR_SPOT_FINDER_CNT_THR 16'h0100
`define ADDR_SPOT_FINDER_SNR_THR 16'h0104
`define ADDR_SPOT_FINDER_D_MIN   16'h0108
`define ADDR_SPOT_FINDER_D_MAX   16'h010C
`define ADDR_SPOT_FINDER_MIN_PIX 16'h0110

`define ADDR_MAC_ADDR_LO         16'h0200
`define ADDR_MAC_ADDR_HI         16'h0204
`define ADDR_IPV4_ADDR           16'h0208
`define ADDR_NMODULES            16'h020C

`define ADDR_DATA_COL_MODE       16'h0210
`define ADDR_ENERGY_KEV          16'h0214
`define ADDR_NFRAMES             16'h0218
`define ADDR_NSTORAGE_CELLS      16'h021C

`define ADDR_NSUMMATION          16'h0220
`define ADDR_SQRTMULT            16'h0224
`define ADDR_PXLTHRESHOLD        16'h0228

module action_config
#(parameter C_S_AXI_ADDR_WIDTH = 16,
  parameter C_S_AXI_DATA_WIDTH = 32
)(
    (* X_INTERFACE_INFO = "xilinx.com:signal:clock:1.0 clk CLK" *)
    (* X_INTERFACE_PARAMETER = "ASSOCIATED_BUSIF s_axi, ASSOCIATED_RESET resetn" *)
    input                                   clk                   ,
    (* X_INTERFACE_INFO = "xilinx.com:signal:reset:1.0 resetn RST" *)
    (* X_INTERFACE_PARAMETER = "POLARITY ACTIVE_LOW" *)
    input                                   resetn                 ,

    input  wire [C_S_AXI_ADDR_WIDTH-1:0]   s_axi_AWADDR,
    input  wire                            s_axi_AWVALID,
    output wire                            s_axi_AWREADY,
    input  wire [C_S_AXI_DATA_WIDTH-1:0]   s_axi_WDATA,
    input  wire [C_S_AXI_DATA_WIDTH/8-1:0] s_axi_WSTRB,
    input  wire                            s_axi_WVALID,
    output wire                            s_axi_WREADY,
    output wire [1:0]                      s_axi_BRESP,
    output wire                            s_axi_BVALID,
    input  wire                            s_axi_BREADY,
    input  wire [C_S_AXI_ADDR_WIDTH-1:0]   s_axi_ARADDR,
    input  wire                            s_axi_ARVALID,
    output wire                            s_axi_ARREADY,
    output wire [C_S_AXI_DATA_WIDTH-1:0]   s_axi_RDATA,
    output wire [1:0]                      s_axi_RRESP,
    output wire                            s_axi_RVALID,
    input  wire                            s_axi_RREADY,

    output reg [31:0]                      data_collection_mode     ,
    output reg [47:0]                      fpga_mac_addr            ,
    output reg [31:0]                      fpga_ipv4_addr           ,
    output reg [31:0]                      energy_kev               ,
    output reg [31:0]                      nframes                  ,
    output reg [4:0]                       nmodules                 ,
    output reg [3:0]                       nstorage_cells           ,
    output reg [7:0]                       nsummation               ,
    output reg [7:0]                       sqrtmult                 ,
    output reg [31:0]                      pxlthreshold             ,
    output wire [31:0]                     hbm_size_bytes           ,

    output reg [31:0]                      spot_finder_count_threshold,
    output reg [31:0]                      spot_finder_snr_threshold,
    output reg [31:0]                      spot_finder_d_min        ,
    output reg [31:0]                      spot_finder_d_max        ,
    output reg [31:0]                      spot_finder_min_pix_per_spot,
    
    output reg                             data_collection_start    ,
    output reg                             data_collection_cancel   ,
    input                                  data_collection_idle     ,

    input                                  host_writer_idle       ,
    input                                  load_from_hbm_idle       ,
    input                                  save_to_hbm_idle       ,
    input                                  frame_summation_idle   ,
    input                                  integration_idle       ,
    input                                  stream768to512_idle       ,
    input                                  stream512to768_idle    ,
    input                                  udp_idle               ,
    input                                  sls_detector_idle      ,

    input [2:0]                            host_writer_state      ,

    input                                  calib_data_fifo_empty  ,
    input                                  calib_data_fifo_full   ,
    input                                  calib_addr_fifo_empty  ,
    input                                  calib_addr_fifo_full   ,

    input                                  proc_fifo_empty             ,
    input                                  proc_fifo_full              ,
    input                                  udp_fifo_empty             ,
    input                                  udp_fifo_full              ,
    input                                  c2h_data_fifo_empty   ,
    input                                  c2h_data_fifo_full    ,
    input                                  c2h_cmd_fifo_empty    ,
    input                                  c2h_cmd_fifo_full     ,
    input                                  h2c_data_fifo_empty   ,
    input                                  h2c_data_fifo_full    ,
    input                                  h2c_cmd_fifo_empty    ,
    input                                  h2c_cmd_fifo_full     ,
    input                                  last_data_fifo_empty       ,
    input                                  last_data_fifo_full        ,
    input                                  last_addr_fifo_empty       ,
    input                                  last_addr_fifo_full        ,
    input                                  frame_generator_fifo_empty ,
    input                                  frame_generator_fifo_full  ,
    input                                  eth_in_fifo_empty          ,
    input                                  eth_in_fifo_full           ,
    input                                  hbm_compl_fifo_empty          ,
    input                                  hbm_compl_fifo_full           ,
    input                                  hbm_handles_fifo_empty          ,
    input                                  hbm_handles_fifo_full           ,
    input                                  mailbox_interrupt_0        ,

    input [63:0]                           stalls_hbm                 ,
    input [63:0]                           stalls_host                ,
    input [63:0]                           beats_hbm                 ,
    input [63:0]                           beats_host                ,
    input                                  stalls_hbm_valid           ,
    input                                  stalls_host_valid          ,
    input                                  beats_hbm_valid           ,
    input                                  beats_host_valid          ,
    input [63:0]                           packets_processed          ,
    input                                  packets_processed_valid    ,
    input [63:0]                           packets_udp                ,
    input                                  packets_udp_valid          ,
    input [63:0]                           packets_sls                ,
    input                                  packets_sls_valid          ,

    input [63:0]                           pulseid                    ,
    input                                  pulseid_valid              ,

    input [31:0]                           udp_err_eth                ,
    input                                  udp_err_eth_valid          ,
    input [31:0]                           udp_err_len                ,
    input                                  udp_err_len_valid          ,

    output reg                             clear_counters
);
//------------------------Parameter----------------------
localparam

    WRIDLE                 = 2'd0,
    WRDATA                 = 2'd1,
    WRRESP                 = 2'd2,
    WRRESET                = 2'd3,
    RDIDLE                 = 2'd0,
    RDDATA                 = 2'd1,
    RDRESET                = 2'd2,
    ADDR_BITS              = C_S_AXI_ADDR_WIDTH;

//------------------------Local signal-------------------
    reg  [1:0]                    wstate = WRRESET;
    reg  [1:0]                    wnext;
    reg  [ADDR_BITS-1:0]          waddr;
    wire [31:0]                   wmask;
    wire                          aw_hs;
    wire                          w_hs;
    reg  [1:0]                    rstate = RDRESET;
    reg  [1:0]                    rnext;
    reg  [31:0]                   rdata;
    wire                          ar_hs;
    wire [ADDR_BITS-1:0]          raddr;

// JFJoch signals
    wire [64:0] prerelease;
    assign prerelease = `JFJOCH_FPGA_PRERELEASE;

    reg [31:0] reg_ctrl;

    reg [63:0] reg_stalls_hbm;
    reg [63:0] reg_stalls_host;
    reg [63:0] reg_beats_hbm;
    reg [63:0] reg_beats_host;
    reg [63:0] reg_packets_processed;
    reg [63:0] reg_packets_udp;
    reg [63:0] reg_packets_sls;
    reg [63:0] reg_pulseid;

    reg [31:0] reg_udp_err_len;
    reg [31:0] reg_udp_err_eth;

    reg [31:0] reg_data_acquisition_count;

    reg [31:0] reg_fifo_status;
    reg        reg_data_collection_idle;
//------------------------Instantiation------------------

//------------------------AXI write fsm------------------
assign s_axi_AWREADY = (wstate == WRIDLE);
assign s_axi_WREADY  = (wstate == WRDATA);
assign s_axi_BRESP   = 2'b00;  // OKAY
assign s_axi_BVALID  = (wstate == WRRESP);
assign wmask   = { {8{s_axi_WSTRB[3]}}, {8{s_axi_WSTRB[2]}}, {8{s_axi_WSTRB[1]}}, {8{s_axi_WSTRB[0]}} };
assign aw_hs   = s_axi_AWVALID & s_axi_AWREADY;
assign w_hs    = s_axi_WVALID & s_axi_WREADY;

// wstate
always @(posedge clk) begin
    if (!resetn)
        wstate <= WRRESET;
    else
        wstate <= wnext;
end

// wnext
always @(*) begin
    case (wstate)
        WRIDLE:
            if (s_axi_AWVALID)
                wnext = WRDATA;
            else
                wnext = WRIDLE;
        WRDATA:
            if (s_axi_WVALID)
                wnext = WRRESP;
            else
                wnext = WRDATA;
        WRRESP:
            if (s_axi_BREADY)
                wnext = WRIDLE;
            else
                wnext = WRRESP;
        default:
            wnext = WRIDLE;
    endcase
end

// waddr
always @(posedge clk) begin
    if (aw_hs)
        waddr <= s_axi_AWADDR[ADDR_BITS-1:0];
end

//------------------------AXI read fsm-------------------
assign s_axi_ARREADY = (rstate == RDIDLE);
assign s_axi_RDATA   = rdata;
assign s_axi_RRESP   = 2'b00;  // OKAY
assign s_axi_RVALID  = (rstate == RDDATA);
assign ar_hs   = s_axi_ARVALID & s_axi_ARREADY;
assign raddr   = s_axi_ARADDR[ADDR_BITS-1:0];

// rstate
always @(posedge clk) begin
    if (!resetn)
        rstate <= RDRESET;
    else
        rstate <= rnext;
end

// rnext
always @(*) begin
    case (rstate)
        RDIDLE:
            if (s_axi_ARVALID)
                rnext = RDDATA;
            else
                rnext = RDIDLE;
        RDDATA:
            if (s_axi_RREADY & s_axi_RVALID)
                rnext = RDIDLE;
            else
                rnext = RDDATA;
        default:
            rnext = RDIDLE;
    endcase
end

// rdata
always @(posedge clk) begin
    if (ar_hs) begin
        case (raddr)
            `ADDR_JFJOCH_MAJOR_VER: begin
                rdata <= `JFJOCH_FPGA_MAJOR_VERSION;
            end
            `ADDR_JFJOCH_MINOR_VER: begin
                rdata <= `JFJOCH_FPGA_MINOR_VERSION;
            end
            `ADDR_JFJOCH_PATCH_VER: begin
                rdata <= `JFJOCH_FPGA_PATCH_VERSION;
            end
            `ADDR_AP_CTRL: begin
                rdata <= reg_ctrl;
            end
            `ADDR_MAC_ADDR_HI: begin
                rdata <= fpga_mac_addr[47:32];
            end
            `ADDR_MAC_ADDR_LO: begin
                rdata <= fpga_mac_addr[31:0];
            end
            `ADDR_IPV4_ADDR: begin
                rdata <= fpga_ipv4_addr;
            end 
            `ADDR_DATA_COL_MODE: begin 
                rdata <= data_collection_mode; 
            end 
            `ADDR_ENERGY_KEV: begin 
                rdata <= energy_kev; 
            end 
            `ADDR_NFRAMES: begin 
                rdata <= nframes; 
            end 
            `ADDR_NMODULES: begin 
                rdata <= nmodules; 
            end 
            `ADDR_NSTORAGE_CELLS: begin 
                rdata <= nstorage_cells; 
            end 
            `ADDR_NSUMMATION: begin 
                rdata <= nsummation; 
            end 
            `ADDR_SQRTMULT: begin 
                rdata <= sqrtmult; 
            end 
            `ADDR_PXLTHRESHOLD: begin 
                rdata <= pxlthreshold; 
            end 
            `ADDR_JFJOCH_VARIANT: begin 
                rdata <= `JFJOCH_VARIANT; 
            end 
            `ADDR_GIT_SHA1: begin 
                rdata <= `GIT_SHA1; 
            end 
            `ADDR_JFJOCH_SYNTH_TIME: begin 
                rdata <= `JFJOCH_SYNTH_TIME; 
            end 
            `ADDR_MAX_MODULES_FPGA: begin 
                rdata <= `MAX_MODULES_FPGA; 
            end 
            `ADDR_STALLS_HBM_HI: begin 
                rdata <= reg_stalls_hbm[63:32]; 
            end 
            `ADDR_STALLS_HBM_LO: begin 
                rdata <= reg_stalls_hbm[31:0]; 
            end 
            `ADDR_STALLS_HOST_HI: begin 
                rdata <= reg_stalls_host[63:32]; 
            end 
            `ADDR_STALLS_HOST_LO: begin 
                rdata <= reg_stalls_host[31:0]; 
            end 
            `ADDR_BEATS_HBM_HI: begin 
                rdata <= reg_beats_hbm[63:32]; 
            end 
            `ADDR_BEATS_HBM_LO: begin 
                rdata <= reg_beats_hbm[31:0]; 
            end 
            `ADDR_BEATS_HOST_HI: begin 
                rdata <= reg_beats_host[63:32]; 
            end 
            `ADDR_BEATS_HOST_LO: begin 
                rdata <= reg_beats_host[31:0]; 
            end 
            `ADDR_PACKETS_UDP_HI: begin 
                rdata <= reg_packets_udp[63:32]; 
            end 
            `ADDR_PACKETS_UDP_LO: begin 
                rdata <= reg_packets_udp[31:0]; 
            end 
            `ADDR_PACKETS_SLS_HI: begin 
                rdata <= reg_packets_sls[63:32]; 
            end 
            `ADDR_PACKETS_SLS_LO: begin 
                rdata <= reg_packets_sls[31:0]; 
            end
            `ADDR_PACKETS_PROC_HI: begin
                rdata <= reg_packets_processed[63:32];
            end
            `ADDR_PACKETS_PROC_LO: begin
                rdata <= reg_packets_processed[31:0];
            end
            `ADDR_PULSEID_HI: begin 
                rdata <= reg_pulseid[63:32]; 
            end 
            `ADDR_PULSEID_LO: begin 
                rdata <= reg_pulseid[31:0]; 
            end 
            `ADDR_PACKETS_ERR_LEN: begin 
                rdata <= reg_udp_err_len; 
            end 
            `ADDR_PACKETS_ERR_ETH: begin 
                rdata <= reg_udp_err_eth; 
            end 
            `ADDR_HBM_SIZE: begin 
                rdata <= `HBM_SIZE_BYTES; 
            end 
            `ADDR_FIFO_STATUS: begin 
                rdata <= reg_fifo_status; 
            end 
            `ADDR_SPOT_FINDER_CNT_THR: begin 
                rdata <= spot_finder_count_threshold; 
            end 
            `ADDR_SPOT_FINDER_SNR_THR: begin 
                rdata <= spot_finder_snr_threshold; 
            end 
            `ADDR_SPOT_FINDER_D_MIN: begin 
                rdata <= spot_finder_d_min; 
            end 
            `ADDR_SPOT_FINDER_D_MAX: begin 
                rdata <= spot_finder_d_max; 
            end 
            `ADDR_SPOT_FINDER_MIN_PIX: begin 
                rdata <= spot_finder_min_pix_per_spot; 
            end       
            `ADDR_RUN_COUNTER: begin 
                rdata <= reg_data_acquisition_count; 
            end 
            `ADDR_JFJOCH_PRERELEASE_1: begin 
                rdata <= prerelease[31:0];
            end
            `ADDR_JFJOCH_PRERELEASE_2: begin
                rdata <= prerelease[63:32];
            end
            default:
               rdata <= 32'hffffffff;
        endcase
    end
end

// Constant output

assign hbm_size_bytes = `HBM_SIZE_BYTES;

//------------------------Register logic-----------------

initial data_collection_start = 1'b0;
initial data_collection_cancel = 1'b0;
initial reg_ctrl = 32'b0;

always @(posedge clk) begin
    if (!resetn)
        reg_ctrl         <= 32'b0;
    else begin
        reg_ctrl[0]      <= data_collection_start;
        reg_ctrl[1]      <= reg_data_collection_idle;
        reg_ctrl[2]      <= data_collection_cancel;
        reg_ctrl[3]      <= clear_counters;
        reg_ctrl[4]      <= host_writer_idle;
        reg_ctrl[5]      <= load_from_hbm_idle;
        reg_ctrl[6]      <= save_to_hbm_idle;
        reg_ctrl[7]      <= frame_summation_idle;
        reg_ctrl[8]      <= integration_idle;
        reg_ctrl[9]      <= stream768to512_idle;
        reg_ctrl[12:10]  <= host_writer_state;
        reg_ctrl[13]     <= stream512to768_idle;
        reg_ctrl[16]     <= mailbox_interrupt_0;
    end
end

always @(posedge clk) begin
    if (!resetn) begin
        data_collection_start <= 1'b0;
        reg_data_acquisition_count <= 32'h0;
    end else if (!reg_data_collection_idle)
        data_collection_start <= 1'b0;
    else if (w_hs && waddr == `ADDR_AP_CTRL && s_axi_WSTRB[0] && s_axi_WDATA[0]) begin
        reg_data_acquisition_count <= reg_data_acquisition_count + 32'h1;
        data_collection_start <= 1'b1;
    end
end

always @(posedge clk) begin
    if (!resetn)
        clear_counters <= 1'b0;
    else if (w_hs && waddr == `ADDR_AP_CTRL && s_axi_WSTRB[0])
        clear_counters <= s_axi_WDATA[3];
end

always @(posedge clk) begin
    if (!resetn)
        data_collection_cancel <= 1'b0;
    else if (w_hs && waddr == `ADDR_AP_CTRL && s_axi_WSTRB[0])
        data_collection_cancel <= s_axi_WDATA[2];
end

always @(posedge clk) begin
    if (!resetn)
        reg_data_collection_idle <= 1'b1;
    else begin
        reg_data_collection_idle <= data_collection_idle;
    end
end

always @(posedge clk) begin
    if (!resetn)
        data_collection_mode <= 0;
    else if (reg_data_collection_idle) begin
        if (w_hs && waddr == `ADDR_DATA_COL_MODE)
            data_collection_mode <= (s_axi_WDATA[31:0] & wmask) | (data_collection_mode & !wmask);
    end
end

always @(posedge clk) begin
    if (!resetn)
        fpga_mac_addr[47:32] <= 0;
    else if (reg_data_collection_idle) begin
        if (w_hs && waddr == `ADDR_MAC_ADDR_HI )
            fpga_mac_addr[47:32] <= (s_axi_WDATA[31:0] & wmask) | (fpga_mac_addr[47:32] & !wmask);
    end
end

always @(posedge clk) begin
    if (!resetn)
        fpga_mac_addr[31:0] <= 0;
    else if (reg_data_collection_idle) begin
        if (w_hs && waddr == `ADDR_MAC_ADDR_LO )
            fpga_mac_addr[31:0] <= (s_axi_WDATA[31:0] & wmask) | (fpga_mac_addr[31:0] & !wmask);
    end
end

always @(posedge clk) begin
    if (!resetn)
        fpga_ipv4_addr <= 0;
    else if (reg_data_collection_idle) begin
        if (w_hs && waddr == `ADDR_IPV4_ADDR )
            fpga_ipv4_addr <= (s_axi_WDATA[31:0] & wmask) | (fpga_ipv4_addr & !wmask);
    end
end

always @(posedge clk) begin
    if (!resetn)
        energy_kev <= 0;
    else if (reg_data_collection_idle) begin
        if (w_hs && waddr == `ADDR_ENERGY_KEV )
            energy_kev <= (s_axi_WDATA[31:0] & wmask) | (energy_kev & !wmask);
    end
end

always @(posedge clk) begin
    if (!resetn)
        nframes <= 0;
    else if (reg_data_collection_idle) begin
        if (w_hs && waddr == `ADDR_NFRAMES )
            nframes <= (s_axi_WDATA[31:0] & wmask) | (nframes & !wmask);
    end
end

always @(posedge clk) begin
    if (!resetn)
        nmodules <= 0;
    else if (reg_data_collection_idle) begin
        if (w_hs && waddr == `ADDR_NMODULES )
            nmodules <= (s_axi_WDATA[4:0] & wmask[4:0]) | (nmodules & !wmask[4:0]);
    end
end

always @(posedge clk) begin
    if (!resetn)
        nstorage_cells <= 0;
    else if (reg_data_collection_idle) begin
        if (w_hs && waddr == `ADDR_NSTORAGE_CELLS)
            nstorage_cells <= (s_axi_WDATA[3:0] & wmask[3:0]) | (nstorage_cells & !wmask[3:0]);
    end
end

always @(posedge clk) begin
    if (!resetn)
        nsummation <= 0;
    else if (reg_data_collection_idle) begin
        if (w_hs && waddr == `ADDR_NSUMMATION)
            nsummation <= (s_axi_WDATA[7:0] & wmask[7:0]) | (nsummation & !wmask[7:0]);
    end
end

always @(posedge clk) begin
    if (!resetn)
        sqrtmult <= 0;
    else if (reg_data_collection_idle) begin
        if (w_hs && waddr == `ADDR_SQRTMULT)
            sqrtmult <= (s_axi_WDATA[7:0] & wmask[7:0]) | (sqrtmult & !wmask[7:0]);
    end
end

always @(posedge clk) begin
    if (!resetn)
        pxlthreshold <= 0;
    else if (reg_data_collection_idle) begin
        if (w_hs && waddr == `ADDR_PXLTHRESHOLD)
            pxlthreshold <= (s_axi_WDATA[31:0] & wmask[31:0]) | (sqrtmult & !wmask[31:0]);
    end
end

always @(posedge clk) begin
    if (!resetn)
        spot_finder_snr_threshold <= 0;
    else if (w_hs && waddr == `ADDR_SPOT_FINDER_SNR_THR)
        spot_finder_snr_threshold <= (s_axi_WDATA[31:0] & wmask[31:0]) | (spot_finder_snr_threshold & !wmask[31:0]);
end

always @(posedge clk) begin
    if (!resetn)
        spot_finder_count_threshold <= 0;
    else if (w_hs && waddr == `ADDR_SPOT_FINDER_CNT_THR)
        spot_finder_count_threshold <= (s_axi_WDATA[31:0] & wmask[31:0]) | (spot_finder_count_threshold & !wmask[31:0]);
end

always @(posedge clk) begin
    if (!resetn)
        spot_finder_d_min <= 0;
    else if (w_hs && waddr == `ADDR_SPOT_FINDER_D_MIN)
        spot_finder_d_min <= (s_axi_WDATA[31:0] & wmask[31:0]) | (spot_finder_d_min & !wmask[31:0]);
end

always @(posedge clk) begin
    if (!resetn)
        spot_finder_d_max <= 0;
    else if (w_hs && waddr == `ADDR_SPOT_FINDER_D_MAX)
        spot_finder_d_max <= (s_axi_WDATA[31:0] & wmask[31:0]) | (spot_finder_d_max & !wmask[31:0]);
end

always @(posedge clk) begin
    if (!resetn)
        spot_finder_min_pix_per_spot <= 0;
    else if (w_hs && waddr == `ADDR_SPOT_FINDER_MIN_PIX)
         spot_finder_min_pix_per_spot <= (s_axi_WDATA[31:0] & wmask[31:0]) | ( spot_finder_min_pix_per_spot & !wmask[31:0]);
end

always @ (posedge clk) begin
    if (!resetn)
        begin
            reg_stalls_hbm        <= 0;
            reg_stalls_host       <= 0;
            reg_beats_hbm         <= 0;
            reg_beats_host        <= 0;
            reg_packets_processed <= 0;
            reg_packets_udp       <= 0;
            reg_packets_sls       <= 0;
            reg_pulseid           <= 0;
        end
    else
        begin
            if (stalls_hbm_valid)
                reg_stalls_hbm <= stalls_hbm;
            if (stalls_host_valid)
                reg_stalls_host <= stalls_host;
            if (beats_hbm_valid)
                reg_beats_hbm <= beats_hbm;
            if (beats_host_valid)
                reg_beats_host <= beats_host;
            if (packets_processed_valid)
                reg_packets_processed <= packets_processed;
            if (packets_udp_valid)
                reg_packets_udp <= packets_udp;
            if (packets_sls_valid)
                reg_packets_sls <= packets_sls;
            if (udp_err_len_valid)
                reg_udp_err_len <= udp_err_len;
            if (udp_err_eth_valid)
                reg_udp_err_eth <= udp_err_eth;
            if (pulseid_valid)
                reg_pulseid <= pulseid;
        end
end

// FIFO status
always @(posedge clk) begin
    if (!resetn)
        reg_fifo_status <= 32'h0;
    else
        begin
        reg_fifo_status[0] <= calib_data_fifo_empty;
        reg_fifo_status[1] <= calib_data_fifo_full;
        reg_fifo_status[2] <= calib_addr_fifo_empty;
        reg_fifo_status[3] <= calib_addr_fifo_full;
        reg_fifo_status[4] <= proc_fifo_empty;
        reg_fifo_status[5] <= proc_fifo_full;
        reg_fifo_status[6] <= udp_fifo_empty;
        reg_fifo_status[7] <= udp_fifo_full;
        reg_fifo_status[8] <= c2h_data_fifo_empty;
        reg_fifo_status[9] <= c2h_data_fifo_full;
        reg_fifo_status[10] <= c2h_cmd_fifo_empty;
        reg_fifo_status[11] <= c2h_cmd_fifo_full;
        reg_fifo_status[16] <= last_data_fifo_empty;
        reg_fifo_status[17] <= last_data_fifo_full;
        reg_fifo_status[18] <= last_addr_fifo_empty;
        reg_fifo_status[19] <= last_addr_fifo_full;
        reg_fifo_status[20] <= h2c_data_fifo_empty;
        reg_fifo_status[21] <= h2c_data_fifo_full;
        reg_fifo_status[22] <= h2c_cmd_fifo_empty;
        reg_fifo_status[23] <= h2c_cmd_fifo_full;
        reg_fifo_status[24] <= frame_generator_fifo_empty;
        reg_fifo_status[25] <= frame_generator_fifo_full;
        reg_fifo_status[26] <= eth_in_fifo_full;
        reg_fifo_status[27] <= eth_in_fifo_empty;
        reg_fifo_status[28] <= hbm_compl_fifo_empty;
        reg_fifo_status[29] <= hbm_compl_fifo_full;
        reg_fifo_status[30] <= hbm_handles_fifo_empty;
        reg_fifo_status[31] <= hbm_handles_fifo_full;
        end
end

endmodule
