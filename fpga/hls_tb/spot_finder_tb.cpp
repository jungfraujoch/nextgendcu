// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "../hls_simulation/hls_cores.h"
#include <thread>

int main() {
    size_t nframes = 4;

	int ret = 0;

	STREAM_768 input;
	STREAM_768 output;
	hls::stream<ap_axiu<32, 1, 1, 1>> strong_pixel;
    hls::stream<ap_axiu<32, 1, 1, 1>> mask_in;
    hls::stream<ap_axiu<32, 1, 1, 1>> mask_out;

	ap_int<32> in_photon_count_threshold = 8;
	float_uint32 in_strong_pixel_threshold;
    in_strong_pixel_threshold.f = 4.0f;
    ap_uint<32> local_strong_pixel_threshold = in_strong_pixel_threshold.u;

    std::vector<int32_t> input_frame(nframes * RAW_MODULE_SIZE);
    for (int i = 0; i < RAW_MODULE_SIZE; i++) {
        if (i % RAW_MODULE_COLS == 1023)
            input_frame[i] = INT24_MIN;
        else
            input_frame[i] = i % RAW_MODULE_COLS;
    }

    input << packet_768_t{.user = 0};
    for (int i = 0; i < nframes * RAW_MODULE_SIZE * sizeof(uint16_t) / 64; i++) {
        ap_int<24> tmp[32];
        for (int j = 0; j < 32; j++)
            tmp[j] = input_frame[i * 32 + j];

        input << packet_768_t{.data = pack32(tmp), .user = 0};
        mask_in << ap_axiu<32,1,1,1>{.data = UINT32_MAX, .user = 0};
    }

    mask_in << ap_axiu<32,1,1,1>{.data = 0, .user = 1};

    input << packet_768_t{.user = 1};

	spot_finder(input, mask_in, output, mask_out, strong_pixel, in_photon_count_threshold,local_strong_pixel_threshold);

	if (input.size() != 0)
		ret = 1;
	if (output.size() != nframes * RAW_MODULE_SIZE * sizeof(uint16_t) / 64 + 2)
		ret = 1;
	for (int i = 0; i < nframes * RAW_MODULE_SIZE * sizeof(uint16_t) / 64 + 2; i++)
		output.read();

    if (mask_out.size() != nframes * RAW_MODULE_SIZE * sizeof(uint16_t) / 64 + 1)
        ret = 1;

    for (int i = 0; i < nframes * (RAW_MODULE_SIZE * sizeof(uint16_t) / 64) + 1; i++)
        mask_out.read();

	if (strong_pixel.size() != nframes * (RAW_MODULE_SIZE * sizeof(uint16_t) / 64 + 16) + 1) {
		ret = 1;
	}
	for (int i = 0; i < nframes * (RAW_MODULE_SIZE * sizeof(uint16_t) / 64 + 16) + 1; i++)
		strong_pixel.read();

	if (ret != 0) {
		printf("Test failed  !!!\n");
		ret = 1;
	} else {
		printf("Test passed !\n");
	}

	return ret;
}
