// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "../hls_simulation/hls_cores.h"
#include <thread>

int main() {

    size_t nframes = 12;

    int ret = 0;

    STREAM_768 input;
    STREAM_768 output;
    hls::stream<ap_axiu<64, 1, 1, 1>> integration_result;
    hls::stream<axis_completion> compl_in;
    hls::stream<axis_completion> compl_out;

    ap_uint<256> hbm_0[16384];
    ap_uint<256> hbm_1[16384];
    ap_uint<256> hbm_2[16384];
    ap_uint<256> hbm_3[16384];

    input << packet_768_t{.user = 0};
    for (int f = 0; f < nframes; f++) {
        compl_in << axis_completion{.frame_number = f, .module = 0, .last = 0};
        for (int i = 0; i < RAW_MODULE_SIZE * sizeof(uint16_t) / 64; i++)
            input << packet_768_t{.data = 0, .user = 0};
    }

    compl_in << axis_completion{.last = 1};
    input << packet_768_t{.user = 1};

    ap_uint<1> idle;

    integration(input, output, integration_result, compl_in, compl_out, hbm_0, hbm_1, hbm_2, hbm_3, idle, 0);

    if (input.size() != 0)
        ret = 1;
    if (compl_in.size() != 0)
		ret = 1;

    if (output.size() != nframes * RAW_MODULE_SIZE * sizeof(uint16_t) / 64 + 2)
        ret = 1;
    for (int i = 0; i < nframes * RAW_MODULE_SIZE * sizeof(uint16_t) / 64 + 2; i++)
        output.read();

    if (integration_result.size() != nframes *  FPGA_INTEGRATION_BIN_COUNT+ 1)
        ret = 1;
    for (int i = 0; i < nframes *  FPGA_INTEGRATION_BIN_COUNT + 1; i++)
        integration_result.read();


    if (compl_out.size() != nframes + 1)
        ret = 1;
    for (int i = 0; i < nframes + 1; i++)
    	compl_out.read();

    if (ret != 0) {
        printf("Test failed  !!!\n");
        ret = 1;
    } else {
        printf("Test passed !\n");
    }

    return ret;
}
