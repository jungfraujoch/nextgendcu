// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "../hls_simulation/hls_cores.h"

int main() {
    int32_t threshold = 32;

    bool all_good = true;

    STREAM_768 input;
    STREAM_768 output;

    packet_768_t packet_in;
    packet_in.user = 0;
    packet_in.data = 0;

    ACT_REG_THRESHOLD(packet_in.data) = threshold;
    ACT_REG_MODE(packet_in.data) = MODE_THRESHOLD;

    input << packet_in;

    ap_int<24> values_in[32], values_out[32];
    for (int i = 0; i < 30; i++)
        values_in[i] = i;

    values_in[30] = INT24_MAX;
    values_in[31] = INT24_MIN;

    packet_in.data = pack32(values_in);
    input << packet_in;

    input << packet_768_t{.user = 1};

    pixel_threshold(input, output);

    output.read();
    packet_768_t packet_out = output.read();
    output.read();

    unpack32(packet_out.data, values_out);

    for (int i = 0; i < 32; i++) {
        int32_t value_expected = values_in[i];

        if (values_in[i] == INT24_MIN)
            value_expected = INT24_MIN;
        else if (values_in[i] < threshold)
            value_expected = 0;

        if (values_out[i] != value_expected) {
            all_good = false;
            std::cerr << "Value " << i << " expected: " << value_expected << "  calculated " << values_out[i] << std::endl;
        }
    }

    if (all_good)
        return 0;
    else
        return 1;
}
