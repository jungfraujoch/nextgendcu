// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "../hls_simulation/hls_cores.h"

int main() {
    bool all_good = true;

    STREAM_768 input;
    STREAM_512 output;

    packet_768_t packet_in;
    packet_in.user = 0;
    packet_in.data = 0;

    ACT_REG_MODE(packet_in.data) = MODE_32BIT_OUTPUT;

    input << packet_in;

    ap_int<24> values_in[32];
    ap_int<32> values_out[32];

    for (int i = 0; i < 30; i++)
        values_in[i] = i;

    values_in[30] = INT24_MAX;
    values_in[31] = INT24_MIN;

    packet_in.data = pack32(values_in);
    input << packet_in;

    input << packet_768_t{.user = 1};

    volatile ap_uint<1> idle;

    stream768to512(input, output, idle);

    if (output.size() != 4) {
        std::cerr << "Wrong output size" << std::endl;
        return 1;
    }

    output.read();
    packet_512_t packet_out_1 = output.read();
    packet_512_t packet_out_2 = output.read();
    output.read();

    ap_uint<1024> packet_out = (packet_out_2.data, packet_out_1.data);

    unpack32(packet_out, values_out);

    for (int i = 0; i < 32; i++) {
        int32_t value_expected = values_in[i];
        if (values_in[i] == INT24_MAX)
            value_expected = INT32_MAX;
        if (values_in[i] == INT24_MIN)
            value_expected = INT32_MIN;

        if (values_out[i] != value_expected) {
            all_good = false;
            std::cerr << "Value " << i << " expected: " << value_expected << "  calculated " << values_out[i] << std::endl;
        }
    }

    if (all_good)
        return 0;
    else
        return 1;
}
