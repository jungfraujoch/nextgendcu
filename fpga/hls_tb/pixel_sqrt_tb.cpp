// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "../hls_simulation/hls_cores.h"

int main() {
    bool all_good = true;

    std::vector<uint16_t> sqrtmult_values = {1,2,4,8,16};

    for (int i = 0; i < sqrtmult_values.size(); i++) {
        uint16_t sqrtmult = sqrtmult_values[i];

        STREAM_768 input;
        STREAM_768 output;

        packet_768_t packet_in;
        packet_in.user = 0;
        packet_in.data = 0;

        ACT_REG_SQRTMULT(packet_in.data) = sqrtmult;
        ACT_REG_MODE(packet_in.data) = MODE_SQROOT;

        input << packet_in;

        ap_int<24> values_in[32], values_out[32];
        for (int i = 0; i < 30; i++)
            values_in[i] = i;

        values_in[30] = INT24_MAX;
        values_in[31] = INT24_MIN;


        packet_in.data = pack32(values_in);
        input << packet_in;

        input << packet_768_t{.user = 1};


        pixel_sqrt(input, output);

        output.read();
        packet_768_t packet_out = output.read();
        output.read();

        unpack32(packet_out.data, values_out);

        for (int i = 0; i < 32; i++) {
            double value_expected = std::round(std::sqrt(values_in[i].to_float()) * sqrtmult);
            if (values_in[i] == INT24_MAX)
                value_expected = INT24_MAX;
            if (values_in[i] == INT24_MIN)
                value_expected = INT24_MIN;

            if (values_out[i] != value_expected) {
                all_good = false;
                std::cerr << "Value " << i << " expected: " << value_expected << "  calculated " << values_out[i] << " sqrtmult " << sqrtmult << std::endl;
            }
        }
    }
    if (all_good)
        return 0;
    else
        return 1;
}
