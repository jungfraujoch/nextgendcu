// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "../hls_simulation/hls_cores.h"

int main() {
    bool all_good = true;

    hls::stream<axis_addr> addr_in;
    hls::stream<axis_addr> addr_out;
    STREAM_512 input;
    STREAM_768 output;

    packet_512_t packet_in;
    packet_in.user = 0;
    packet_in.data = 0;

    ACT_REG_MODE(packet_in.data) = 0; // For the moment

    input << packet_in;

    ap_int<16> values_in[32];
    ap_int<24> values_out[32];

    for (int i = 0; i < 30; i++)
        values_in[i] = i;

    values_in[30] = INT16_MAX;
    values_in[31] = INT16_MIN;

    axis_addr addr;

    addr.packet_length = 4;
    addr.last = 0;
    addr_in << addr;

    addr.last = 1;
    addr_in << addr;

    packet_in.data = pack32(values_in);
    for (int i = 0; i < 4; i++)
    	input << packet_in;

    input << packet_512_t{.user = 1};

    volatile ap_uint<1> idle;

    stream512to768(input, output, addr_in, addr_out, idle);

    if (addr_out.size() != 2) {
        std::cerr << "Wrong addr output size" << std::endl;
        return 1;
    }

    addr_out.read();
    addr_out.read();

    if (output.size() != 6) {
        std::cerr << "Wrong output size" << std::endl;
        return 1;
    }

    output.read();
    packet_768_t packet_out_1 = output.read();
    for (int i = 0;i < 3; i++)
    	output.read();
    output.read();

    unpack32(packet_out_1.data, values_out);

    for (int i = 0; i < 32; i++) {
        int32_t value_expected = values_in[i];
        if (values_in[i] == INT16_MAX)
            value_expected = INT24_MAX;
        if (values_in[i] == INT16_MIN)
            value_expected = INT24_MIN;

        if (values_out[i] != value_expected) {
            all_good = false;
            std::cerr << "Value " << i << " expected: " << value_expected << "  calculated " << values_out[i] << std::endl;
        }
    }

    if (all_good)
        return 0;
    else
        return 1;
}
