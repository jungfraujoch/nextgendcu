// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_HLS_CORES_H
#define JFJOCH_HLS_CORES_H

#include "../hls/hls_jfjoch.h"

void ethernet(AXI_STREAM &eth_in,
              AXI_STREAM &ip_out,
              AXI_STREAM &arp_out,
              ap_uint<48> fpga_mac_addr,
              ap_uint<1> enable,
              ap_uint<1> direct,
              uint64_t& counter,
              volatile ap_uint<1> &in_clear_counters);

void ipv4(AXI_STREAM &eth_in,
          AXI_STREAM &udp_out,
          AXI_STREAM &icmp_out,
          ap_uint<32> fpga_ipv4_addr,
          ap_uint<1> direct);

void icmp(AXI_STREAM& eth_in, AXI_STREAM& eth_out, uint64_t& counter,
          volatile ap_uint<1> &in_clear_counters);

void arp(AXI_STREAM &arp_in,
         AXI_STREAM &eth_out,
         ap_uint<48> fpga_mac_addr,
         ap_uint<32> fpga_ipv4_addr,
         ap_uint<1> enable);

void udp(AXI_STREAM &eth_in,
         AXI_STREAM &udp_payload_out,
         hls::stream<ap_uint<UDP_METADATA_STREAM_WIDTH> > &udp_metadata_out,
         uint64_t& counter,
         volatile ap_uint<1> &in_clear_counters,
         volatile ap_uint<1> &idle);

void sls_detector(AXI_STREAM &udp_payload_in,
                  hls::stream<ap_uint<UDP_METADATA_STREAM_WIDTH> > &udp_metadata_in,
                  AXI_STREAM &data_out,
                  hls::stream<axis_addr> &addr_out,
                  uint64_t& counter,
                  uint32_t& counter_eth_error,
                  uint32_t& counter_len_error,
                  uint64_t& pulse_id,
                  volatile ap_uint<1> &in_clear_counters,
                  volatile ap_uint<1> &idle);

// Image modification and analysis cores

void mask_missing(STREAM_768 &data_in,
                  STREAM_768 &data_out,
                  hls::stream<axis_completion > &s_axis_completion,
                  hls::stream<axis_completion > &m_axis_completion);

void adu_histo(STREAM_768 &data_in,
               STREAM_768 &data_out,
               hls::stream<ap_uint<512>> &result_out,
               hls::stream<axis_completion > &s_axis_completion,
               hls::stream<axis_completion > &m_axis_completion);

void jf_conversion(STREAM_768 &data_in, STREAM_768 &data_out,
                   hls::stream<axis_completion > &s_axis_completion,
                   hls::stream<axis_completion > &m_axis_completion,
                   hls::burst_maxi<hbm256_t> d_hbm_p0, hls::burst_maxi<hbm256_t> d_hbm_p1,
                   hls::burst_maxi<hbm256_t> d_hbm_p2, hls::burst_maxi<hbm256_t> d_hbm_p3,
                   hls::burst_maxi<hbm256_t> d_hbm_p4, hls::burst_maxi<hbm256_t> d_hbm_p5,
                   hls::burst_maxi<hbm256_t> d_hbm_p6, hls::burst_maxi<hbm256_t> d_hbm_p7,
                   hls::burst_maxi<hbm256_t> d_hbm_p8, hls::burst_maxi<hbm256_t> d_hbm_p9,
                   hls::burst_maxi<hbm256_t> d_hbm_p10, hls::burst_maxi<hbm256_t> d_hbm_p11,
                   ap_uint<32> hbm_size_bytes);

void spot_finder(STREAM_768 &data_in,
                 hls::stream<ap_axiu<32,1,1,1>> &mask_in,
                 STREAM_768 &data_out,
                 hls::stream<ap_axiu<32,1,1,1>> &mask_out,
                 hls::stream<ap_axiu<32,1,1,1>> &strong_pixel_out,
                 volatile ap_int<32> &in_count_threshold,
                 volatile ap_uint<32> &in_snr_threshold);

void integration(STREAM_768 &data_in,
                 STREAM_768 &data_out,
                 hls::stream<ap_axiu<64,1,1,1>> &result_out,
                 hls::stream<axis_completion > &s_axis_completion,
                 hls::stream<axis_completion > &m_axis_completion,
                 ap_uint<256> *d_hbm_p0,
                 ap_uint<256> *d_hbm_p1,
                 ap_uint<256> *d_hbm_p2,
                 ap_uint<256> *d_hbm_p3,
                 volatile ap_uint<1> &idle,
                 ap_uint<32> hbm_size_bytes);

void data_collection_fsm(AXI_STREAM &eth_in,
                         STREAM_512 &data_out,
                         hls::stream<axis_addr> &addr_in,
                         hls::stream<axis_addr> &addr_out,
                         volatile ap_uint<1> &in_run,
                         volatile ap_uint<1> &in_cancel,
                         volatile ap_uint<1> &out_idle,
                         ap_uint<32> mode,
                         ap_uint<32> energy_kev,
                         ap_uint<32> nframes,
                         ap_uint<5>  nmodules,
                         ap_uint<4>  nstorage_cells,
                         ap_uint<8> nsummation,
                         ap_uint<8> sqrtmult,
                         ap_uint<32> pxlthreshold,
                         volatile rcv_state_t  &data_collection_state);

void host_writer(STREAM_512 &data_in,
                 hls::stream<ap_uint<512>> &adu_histo_in,
                 hls::stream<ap_uint<512>> &integration_in,
                 hls::stream<ap_uint<512>> &spot_finder_in,
                 hls::stream<ap_uint<256>> &roi_count_in,
                 hls::stream<ap_uint<128>> &pixel_count_in,
                 hls::stream<axis_completion > &s_axis_completion,
                 hls::stream<ap_axiu<512,1,1,1> > &host_memory_out,
                 hls::stream<axis_datamover_ctrl> &datamover_out_cmd,
                 hls::stream<ap_uint<32> > &s_axis_work_request,
                 hls::stream<ap_uint<32> > &m_axis_completion,
                 const uint64_t *dma_address_table,
                 volatile uint64_t &packets_processed,
                 volatile ap_uint<1> &idle,
                 volatile ap_uint<1> &in_cancel,
                 volatile ap_uint<3> &state);

void load_from_hbm(STREAM_768 &data_in,
                   STREAM_768 &data_out,
                   hls::stream<axis_completion > &s_axis_completion,
                   hls::stream<axis_completion > &m_axis_completion,
                   hls::stream<ap_uint<16> > &m_axis_free_handles,
                   hls::stream<ap_axiu<256,1,1,1> > &hbm_in_0,
                   hls::stream<ap_axiu<256,1,1,1> > &hbm_in_1,
                   hls::stream<ap_axiu<256,1,1,1> > &hbm_in_2,
                   hls::stream<axis_datamover_ctrl> &datamover_0_cmd,
                   hls::stream<axis_datamover_ctrl> &datamover_1_cmd,
                   hls::stream<axis_datamover_ctrl> &datamover_2_cmd,
                   volatile ap_uint<1> &idle,
                   ap_uint<32> hbm_size_bytes);

void save_to_hbm(hls::stream<axis_addr> &addr_in,
                 hls::stream<axis_completion > &m_axis_completion,
                 hls::stream<ap_uint<16>> &s_axis_free_handles,
                 hls::stream<axis_datamover_ctrl> &datamover_0_cmd,
                 hls::stream<axis_datamover_ctrl> &datamover_1_cmd,
                 hls::stream<axis_datamover_ctrl> &datamover_2_cmd,
                 volatile ap_uint<1> &idle,
                 ap_uint<32> hbm_size_bytes);

void save_to_hbm_data(STREAM_768 &data_in,
                      STREAM_768 &data_out,
                      hls::stream<ap_axiu<256,1,1,1> > &hbm_out_0,
                      hls::stream<ap_axiu<256,1,1,1> > &hbm_out_1,
                      hls::stream<ap_axiu<256,1,1,1> > &hbm_out_2);

void timer_host(STREAM_512 &data_in,
                STREAM_512 &data_out,
                volatile uint64_t &stalls,
                volatile uint64_t &beats);

// Helper cores - not part of main "data" path

int frame_generator(STREAM_512 &data_out,
                    ap_uint<256> *d_hbm_p0,
                    ap_uint<256> *d_hbm_p1,
                    ap_uint<32> hbm_size_bytes,
                    ap_uint<48> src_mac_addr,
                    ap_uint<32> src_ipv4_addr,
                    volatile ap_uint<1> &in_cancel,
                    const FrameGeneratorConfig &config);

int load_calibration(ap_uint<256> *d_hbm_p0,
                     ap_uint<256> *d_hbm_p1,
                     uint32_t pixel_mask[MAX_MODULES_FPGA*RAW_MODULE_SIZE/32],
                     const LoadCalibrationConfig &config,
                     ap_uint<32> hbm_size_bytes,
                     hls::stream<axis_datamover_ctrl> &datamover_in_cmd,
                     hls::stream<ap_axiu<512,1,1,1> > &host_memory_in,
                     const uint64_t *dma_address_table);

void frame_summation(STREAM_768 &data_in, STREAM_768 &data_out,
                     hls::stream<axis_completion > &s_axis_completion,
                     hls::stream<axis_completion > &m_axis_completion,
                     volatile ap_uint<1> &idle);

void frame_summation_reorder_compl(STREAM_768 &data_in,
                                   STREAM_768 &data_out,
                                   hls::stream<axis_completion > &s_axis_completion,
                                   hls::stream<axis_completion > &m_axis_completion);

void stream768to512(STREAM_768 &data_in, STREAM_512 &data_out,
                    volatile ap_uint<1> &idle);

void spot_finder_connectivity(hls::stream<ap_axiu<32,1,1,1>> &data_in,
                              hls::stream<ap_axiu<32,1,1,1>> &data_out,
                              hls::stream<ap_uint<32>> &connectivity_out);

void spot_finder_merge(hls::stream<ap_axiu<32,1,1,1>> &data_in,
                       hls::stream<ap_uint<32>> &connectivity_in,
                       hls::stream<ap_axiu<32,1,1,1>> &data_out,
                       volatile ap_uint<32> &in_min_pix_per_spot);

void spot_finder_mask(STREAM_768 &data_in,
                      STREAM_768 &data_out,
                      hls::stream<ap_axiu<32,1,1,1>> &mask_out,
                      hls::stream<axis_completion > &s_axis_completion,
                      hls::stream<axis_completion > &m_axis_completion,
                      ap_uint<256> *d_hbm_p0,
                      ap_uint<256> *d_hbm_p1,
                      volatile ap_uint<32> &in_min_d_value,
                      volatile ap_uint<32> &in_max_d_value,
                      ap_uint<32> hbm_size_bytes);

void eiger_reorder(STREAM_768 &data_in,
                   STREAM_768 &data_out,
                   hls::stream<axis_completion > &s_axis_completion,
                   hls::stream<axis_completion > &m_axis_completion);

void roi_calc(STREAM_768 &data_in,
              STREAM_768 &data_out,
              hls::stream<ap_uint<256>> &roi_out,
              hls::stream<axis_completion > &s_axis_completion,
              hls::stream<axis_completion > &m_axis_completion,
              ap_uint<256> *d_hbm_p0,
              ap_uint<256> *d_hbm_p1,
              ap_uint<32> hbm_size_bytes);

void pixel_sqrt(STREAM_768 &data_in,
                STREAM_768 &data_out);

void pixel_threshold(STREAM_768 &data_in,
                     STREAM_768 &data_out);

void stream512to768(STREAM_512 &data_in,
                    STREAM_768 &data_out,
                    hls::stream<axis_addr> &addr_in,
                    hls::stream<axis_addr> &addr_out,
                    volatile ap_uint<1> &idle);

void pixel_mask(STREAM_768 &data_in,
                STREAM_768 &data_out,
                const uint32_t pixel_mask[MAX_MODULES_FPGA*RAW_MODULE_SIZE/32],
                hls::stream<axis_completion > &s_axis_completion,
                hls::stream<axis_completion > &m_axis_completion);

void pixel_calc(STREAM_768 &data_in,
                STREAM_768 &data_out,
                hls::stream<ap_uint<128>> &calc_out);

#endif //JFJOCH_HLS_CORES_H
