// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_HLSDEVICE_H
#define JFJOCH_HLSDEVICE_H

#include <future>
#include <vector>

#include "../pcie_driver/jfjoch_fpga.h"

struct HLSDeviceImpl;

class HLSDevice {
    // Hide all HLS simulation structures here
    std::unique_ptr<HLSDeviceImpl> impl_;

    std::mutex completion_mutex;
    uint32_t completion_count;

    std::thread action_thread;
    std::future<void> frame_generator_future;
    std::vector<uint64_t> dma_address_table;
    uint64_t eth_packets = 0;
    uint64_t icmp_packets = 0;
    uint64_t udp_packets = 0;
    uint64_t sls_packets = 0;
    uint32_t udp_len_err = 0;
    uint32_t udp_eth_err = 0;

    uint64_t current_pulse_id = 0;
    uint32_t run_counter = 0;

    DataCollectionConfig cfg;
    SpotFinderParameters spot_finder_parameters;

    const uint64_t mac_addr = 0xCCAA11223344;
    const uint32_t ipv4_addr = 0x0132010A;
    volatile bool idle;

    void FrameGeneratorFuture(FrameGeneratorConfig config);
    void HLSMainThread();
public:
    explicit HLSDevice(std::vector<DeviceOutput *> &device_buffer);
    ~HLSDevice();
    void SendPacket(char *buffer, int len, uint8_t user = 0);
    void CreateJFPacket(uint64_t frame_number, uint32_t eth_packet,
                        uint32_t module_number, const uint16_t *data, int8_t adjust_axis = 0, uint8_t user = 0);
    void CreateEIGERPacket(uint64_t frame_number, uint32_t eth_packet,
                           uint32_t module_number, uint32_t col, uint32_t row,
                           const uint16_t *data);
    void CreateFinalPacket();
    void CreateXfelBunchIDPacket(double bunchid, uint32_t event_code);
    
    DataCollectionStatus GetDataCollectionStatus() const ;
    void Cancel() ;

    void HW_ReadActionRegister(DataCollectionConfig *job);
    void HW_WriteActionRegister(const DataCollectionConfig *job);

    void FPGA_StartAction();
    void FPGA_EndAction();
    bool HW_IsIdle() const;
    bool HW_ReadMailbox(uint32_t *values);
    bool HW_SendWorkRequest(uint32_t handle);
    void HW_LoadCalibration(const LoadCalibrationConfig &config);
    void HW_SetSpotFinderParameters(const SpotFinderParameters &params);
    void HW_RunInternalGenerator(const FrameGeneratorConfig &config) ;

    uint64_t GetSLSPackets();
    uint64_t GetUDPPackets();
};


#endif //JFJOCH_HLSDEVICE_H
