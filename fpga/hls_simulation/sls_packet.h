// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_SLS_PACKET_H
#define JUNGFRAUJOCH_SLS_PACKET_H

#include <cstdint>
#define BUNCHID_MAGICN (0x48f271c7)

struct __attribute__((packed, aligned(2))) jf_udp_payload {
    uint64_t framenum;
    uint32_t exptime; // x 1e-7 sec
    uint32_t packetnum;
    uint64_t bunchid;
    uint64_t timestamp;
    uint16_t moduleID;
    uint16_t row;
    uint16_t column;
    uint16_t detSpec2;
    uint32_t debug;
    uint16_t roundRobin;
    uint8_t detectortype;
    uint8_t headerVersion;
    uint16_t data[4096];
};

struct __attribute__((packed, aligned(2))) jf_raw_packet {
    char dest_mac[6];
    char sour_mac[6];
    uint16_t ether_type;
    uint16_t ipv4_header_h;
    uint16_t ipv4_header_total_length;
    uint16_t ipv4_header_identification;
    uint16_t ipv4_header_flags_frag;
    uint16_t ipv4_header_ttl_protocol;
    uint16_t ipv4_header_checksum;
    uint32_t ipv4_header_sour_ip;
    uint32_t ipv4_header_dest_ip;
    uint16_t udp_sour_port;
    uint16_t udp_dest_port;
    uint16_t udp_length;
    uint16_t udp_checksum;
    jf_udp_payload jf;
};

struct __attribute__((packed, aligned(2))) eiger_udp_payload {
    uint64_t framenum;
    uint32_t exptime; // x 1e-7 sec
    uint32_t packetnum;
    uint64_t bunchid;
    uint64_t timestamp;
    uint16_t moduleID;
    uint16_t row;
    uint16_t column;
    uint16_t detSpec2;
    uint32_t debug;
    uint16_t roundRobin;
    uint8_t detectortype;
    uint8_t headerVersion;
    uint16_t data[2048];
};

struct __attribute__((packed, aligned(2))) eiger_raw_packet {
    char dest_mac[6];
    char sour_mac[6];
    uint16_t ether_type;
    uint16_t ipv4_header_h;
    uint16_t ipv4_header_total_length;
    uint16_t ipv4_header_identification;
    uint16_t ipv4_header_flags_frag;
    uint16_t ipv4_header_ttl_protocol;
    uint16_t ipv4_header_checksum;
    uint32_t ipv4_header_sour_ip;
    uint32_t ipv4_header_dest_ip;
    uint16_t udp_sour_port;
    uint16_t udp_dest_port;
    uint16_t udp_length;
    uint16_t udp_checksum;
    eiger_udp_payload eiger;
};

struct __attribute__((packed, aligned(2))) bunchid_payload {
    char empty_bytes[6];
    uint32_t magicn[3];
    uint32_t bunchid_lsb[2];
    uint32_t bunchid_msb[2];
    uint32_t montrig;
    uint32_t evt_ctr;
};

struct __attribute__((packed, aligned(2))) bunchid_raw_packet {
    char dest_mac[6];
    char sour_mac[6];
    uint16_t ether_type;
    uint16_t ipv4_header_h;
    uint16_t ipv4_header_total_length;
    uint16_t ipv4_header_identification;
    uint16_t ipv4_header_flags_frag;
    uint16_t ipv4_header_ttl_protocol;
    uint16_t ipv4_header_checksum;
    uint32_t ipv4_header_sour_ip;
    uint32_t ipv4_header_dest_ip;
    uint16_t udp_sour_port;
    uint16_t udp_dest_port;
    uint16_t udp_length;
    uint16_t udp_checksum;
    bunchid_payload payload;
};

#endif //JUNGFRAUJOCH_SLS_PACKET_H
