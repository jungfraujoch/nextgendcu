# SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
# SPDX-License-Identifier: CERN-OHL-S-2.0

# Jungfraujoch specific
set_false_path -to [get_pins jfjoch_pcie_i/*/network_stack/network_config_0/*/reg_eth_stat_rx_status_1_reg/D]

# From 100G example design
set_false_path -to [get_pins -leaf -of_objects [get_cells -hier *cdc_to* -filter {is_sequential}] -filter {NAME=~*cmac_cdc*/*/D}]

set_max_delay -from [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ */mac_100g*/*/channel_inst/*_CHANNEL_PRIM_INST/RXOUTCLK}]] -to [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ */mac_100g*/*/channel_inst/*_CHANNEL_PRIM_INST/TXOUTCLK}]] -datapath_only 3.103
set_max_delay -from [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ */mac_100g*/*/channel_inst/*_CHANNEL_PRIM_INST/TXOUTCLK}]] -to [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ */mac_100g*/*/channel_inst/*_CHANNEL_PRIM_INST/RXOUTCLK}]] -datapath_only 3.103
set_max_delay -from [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ */mac_100g*/*/channel_inst/*_CHANNEL_PRIM_INST/RXOUTCLK}]] -to [get_clocks clk_out2_jfjoch_pcie_clk_wiz_0_0] -datapath_only 3.103
set_max_delay -from [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ */mac_100g*/*/channel_inst/*_CHANNEL_PRIM_INST/TXOUTCLK}]] -to [get_clocks clk_out2_jfjoch_pcie_clk_wiz_0_0] -datapath_only 3.103
set_max_delay -from [get_clocks clk_out2_jfjoch_pcie_clk_wiz_0_0] -to [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ */mac_100g*/*/channel_inst/*_CHANNEL_PRIM_INST/RXOUTCLK}]] -datapath_only 10.000
set_max_delay -from [get_clocks clk_out2_jfjoch_pcie_clk_wiz_0_0] -to [get_clocks -of_objects [get_pins -hierarchical -filter {NAME =~ */mac_100g*/*/channel_inst/*_CHANNEL_PRIM_INST/TXOUTCLK}]] -datapath_only 10.000

create_waiver -type CDC -id {CDC-11} -user "cmac" -tags "10930"\
-desc "The CDC-11 warning is waived as fan-out is expected for this stat signal"\
-to [get_pins -of [get_cells -hier -filter {name =~ */*stat_rx_aligned/*cdc_to*}] -filter {name =~ *D}]
