# SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
# SPDX-License-Identifier: CERN-OHL-S-2.0

# Jungfraujoch specific
set_false_path -to [get_pins jfjoch_pcie_i/*/packet_proc*/network_stack/network_config*/*/reg_eth_stat_rx_status_1_reg/D]

# From 10G/25G example design
set_false_path -to [get_cells -hierarchical -filter {NAME =~ */i_*_axi_if_top/*/i_*_syncer/*meta_reg*}]
set_false_path -to [get_cells -hierarchical -filter {NAME =~ */i_*_SYNC*/*stretch_reg*}]

set_false_path -to [get_cells -hierarchical -filter {NAME=~ */i*syncer/*d2_cdc_to*}]

set_max_delay  10.000 -datapath_only -from [get_pins -of [get_cells -hier -filter { name =~ */pktgen_enable_int_reg*}] -filter { name =~ *C } ]\
-to [get_pins -of [get_cells -hier -filter { name =~ */*_sync_pkt_gen_enable/s_out_d2_cdc_to_reg*}] -filter { name =~ *D } ] -quiet
set_max_delay  10.000 -datapath_only -from [get_pins -of [get_cells -hier -filter { name =~ */i_RX_WD_ALIGN/align_status_reg[*]*}] -filter { name =~ *C } ]\
-to [get_pins -of [get_cells -hier -filter { name =~ */s_out_d2_cdc_to_reg*}] -filter { name =~ *D } ] -quiet
set_max_delay -from [get_pins -of [get_cells -hier -filter { name =~ */rx_errors_int_reg*}] -filter { name =~ *C } ]\
-to [get_pins -of [get_cells -hier -filter { name =~ */s_out_d2_cdc_to_reg[*]*}] -filter { name =~ *D } ] 10.000 -datapath_only -quiet

set_max_delay -from [get_pins -of [get_cells -hier -filter { name =~ */mode_switch_reg*}] -filter { name =~ *C } ] 10.000 -datapath_only -quiet
set_max_delay -from [get_pins -of [get_cells -hier -filter { name =~ */pipe_reg*}] -filter { name =~ *C } ] 2.5 -datapath_only -quiet
set_max_delay 10.000 -datapath_only -from [get_pins -of [get_cells -hier -filter { name =~ */i_pif_registers/ctl_*x_opcode_*}] -filter { name =~ *C } ]  -quiet

create_waiver -quiet -type CDC -id {CDC-11} -user "xxv_ethernet" -desc "The align status signal is synced with different syncers where fan-out is expected and so can be waived" -tags "11999"\
-from [get_pins -of [get_cells -hier -filter {name =~ */i_RX_WD_ALIGN/align_status_reg*}] -filter {name =~ *C}]\
-to [get_pins -of [get_cells -hier -filter {name =~ */s_out_d2_cdc_to_reg*}] -filter {name =~ *D}]

create_waiver -quiet -type CDC -id {CDC-11} -user "xxv_ethernet" -desc "The reset signal is synced with different syncers where fan-out is expected and so can be waived" -tags "11999"\
-from [get_pins -of [get_cells -hier -filter {name =~ */rx_reset_done_async_r*}] -filter {name =~ *C}]\
-to [get_pins -of [get_cells -hier -filter {name =~ */s_out_d2_cdc_to_reg*}] -filter {name =~ *D}]

create_waiver -quiet -type CDC -id {CDC-11} -user "xxv_ethernet" -desc "The align status signal is synced with different syncers where fan-out is expected and so can be waived" -tags "11999"\
-from [get_pins -of [get_cells -hier -filter {name =~ */i_RX_WD_ALIGN/align_status_reg*}] -filter {name =~ *C}]\
-to [get_pins -of [get_cells -hier -filter {name =~ */i_*_TRAFFIC_GENERATOR/i_*_PKT_CHK/rx_block_lock_led_*d_reg*}] -filter {name =~ *D}]

create_waiver -quiet -type CDC -id {CDC-2} -user "xxv_ethernet" -desc "The align status signal is synced with different syncers where fan-out is expected and so can be waived" -tags "11999"\
-from [get_pins -of [get_cells -hier -filter {name =~ */i_RX_WD_ALIGN/align_status_reg*}] -filter {name =~ *C}]\
-to [get_pins -of [get_cells -hier -filter {name =~ */i_*_TRAFFIC_GENERATOR/i_*_PKT_CHK/rx_block_lock_led_*d_reg*}] -filter {name =~ *D}]

create_waiver -quiet -type CDC -id {CDC-1} -user "xxv_ethernet" -desc "The align status signal is synced with different syncers where fan-out is expected and so can be waived" -tags "11999"\
-from [get_pins -of [get_cells -hier -filter {name =~ */i_RX_DELETE_FCS/*_d*_reg*}] -filter {name =~ *C}]\
-to [get_pins -of [get_cells -hier -filter {name =~ */i_*_TRAFFIC_GENERATOR/*_reg*}] -filter {name =~ *}]

create_waiver -quiet -type CDC -id {CDC-11} -user "xxv_ethernet" -desc "The align status signal is synced with different syncers where fan-out is expected and so can be waived" -tags "11999"\
-from [get_pins -of [get_cells -hier -filter {name =~ */i_RX_WD_ALIGN/align_status_reg*}] -filter {name =~ *C}]\
-to [get_pins -of [get_cells -hier -filter {name =~ */i_*_TRAF_CHK*/rx_block_lock_led_*d_reg*}] -filter {name =~ *D}]

create_waiver -quiet -type CDC -id {CDC-1} -user "xxv_ethernet" -desc "The align status signal is synced with different syncers where fan-out is expected and so can be waived" -tags "11999"\
-from [get_pins -of [get_cells -hier -filter {name =~ */i_RX_WD_ALIGN/align_status_reg*}] -filter {name =~ *C}]\
-to [get_pins -of [get_cells -hier -filter {name =~ */i_*_TRAF_CHK*/rx_block_lock_led_*d_reg*}] -filter {name =~ *D}]

create_waiver -quiet -type CDC -id {CDC-2} -user "xxv_ethernet" -desc "The align status signal is synced with different syncers where fan-out is expected and so can be waived" -tags "11999"\
-from [get_pins -of [get_cells -hier -filter {name =~ */i_RX_WD_ALIGN/align_status_reg*}] -filter {name =~ *C}]\
-to [get_pins -of [get_cells -hier -filter {name =~ */i_*_TRAF_CHK*/rx_block_lock_led_*d_reg*}] -filter {name =~ *D}]

create_waiver -quiet -type CDC -id {CDC-1} -user "xxv_ethernet" -desc "The align status signal is synced with different syncers where fan-out is expected and so can be waived" -tags "11999"\
-from [get_pins -of [get_cells -hier -filter {name =~ */i_RX_DECODER/data_*_reg*}] -filter {name =~ *C}]\
-to [get_pins -of [get_cells -hier -filter {name =~ */i_*_TRAF_CHK*/*_reg*}] -filter {name =~ *}]



set_max_delay -datapath_only -from [get_pins -of [get_cells -hier -filter { name =~ */*_axi_if_top/i_pif_registers/ctl_rsfec_enable_r_reg*}] -filter { name =~ *C }] 10.000 -quiet

set_max_delay -datapath_only -from [get_pins -of [get_cells -hier -filter { name =~ */*_axi_if_top/i_pif_registers/ctl_*x_max_packet_len_out_reg*}] -filter { name =~ *C }] 10.000 -quiet

set_max_delay -from [get_pins -of [get_cells -hier -filter { name =~ */inst/*x_*bit_retiming_sync_serdes*/data_out_*d*}] -filter { name =~ *C } ] 10.000 -datapath_only -quiet


# For rx-status
set_false_path -to [get_pins jfjoch_pcie_i/*/network_stack/network_config_0/*/reg_eth_stat_rx_status_1_reg/D]
