#!/bin/bash
# Taken from https://schneide.blog/2015/08/10/packaging-kernel-modulesdrivers-using-dkms/

VERSION=1.0.0-rc.31

occurrences=`/usr/sbin/dkms status | grep jfjoch | grep ${VERSION} | wc -l`

if [ ! occurrences > 0 ]; then
    /usr/sbin/dkms add -m jfjoch -v ${VERSION}
fi
/usr/sbin/dkms build -m jfjoch -v ${VERSION}
/usr/sbin/dkms install -m jfjoch -v ${VERSION}

exit 0