// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_JFJOCH_FPGA_H
#define JUNGFRAUJOCH_JFJOCH_FPGA_H

#ifdef __KERNEL__
typedef __u8 uint8_t;
typedef __u32 uint32_t;
typedef __u64 uint64_t;
#else
#include <cstdint>
#endif

#define RAW_MODULE_LINES        (512L)
#define RAW_MODULE_COLS         (1024L)
#define RAW_MODULE_SIZE         (RAW_MODULE_LINES * RAW_MODULE_COLS)

#define FPGA_BUFFER_LOCATION_SIZE (RAW_MODULE_SIZE * sizeof(short) * 4) // account for space for data processing results and 32-bit frames

#define DELAY_FRAMES_STOP_AND_QUIT 5

#define GAIN_G0_MULTIPLIER 32
#define GAIN_G1_MULTIPLIER (-1)
#define GAIN_G2_MULTIPLIER (-1)

#define JFJOCH_FPGA_REVISION 5

#define JFJOCH_FPGA_VARIANT_100G        0
#define JFJOCH_FPGA_VARIANT_8x10G       1

#define MODE_CONV                    0x0001L
#define MODE_32BIT_OUTPUT            0x0002L
#define MODE_UNSIGNED                0x0004L
#define MODE_SQROOT                  0x0008L
#define MODE_EIGER_32BIT             0x0010L
#define MODE_EIGER_8BIT              0x0020L
#define MODE_FIXG1                   0x0080L
#define MODE_THRESHOLD               0x0100L
#define MODE_8BIT_OUTPUT             0x0200L
#define MODE_APPLY_PIXEL_MASK        0x0400L

#define LOAD_CALIBRATION_OK              (0)
#define LOAD_CALIBRATION_ERR_HOST_ADDR   (1)
#define LOAD_CALIBRATION_ERR_DEST        (2)
#define LOAD_CALIBRATION_ERR_MODULE      (3)

#define HANDLE_START               (65534)
#define HANDLE_END                 (65535)
#define DATA_COLLECTION_ID_PURGE   (65535)

#define INT_PKT_GEN_DEBUG       0x0
#define INT_PKT_GEN_BUNCHID     0xCACACACACA
#define INT_PKT_GEN_EXPTTIME    10000

#define FPGA_INTEGRATION_BIN_COUNT 1024

#define MAX_MODULES_FPGA  32
#define MAX_FPGA_SUMMATION 256

#define ADU_HISTO_BIN_WIDTH 32
#define ADU_HISTO_BIN_COUNT (65536/ ADU_HISTO_BIN_WIDTH)

#define DMA_DESCRIPTORS_PER_MODULE 6

#define FPGA_ROI_COUNT    64

#define INT24_MAX (8388607)
#define INT24_MIN (-8388608)

// INT32_MAX is written explicitly, as the constant is not present in kernel
#define MAX_FRAMES (2147483647)

#pragma pack(push)
#pragma pack(4)

#define LOAD_CALIBRATION_DEST_GAIN_G0                0
#define LOAD_CALIBRATION_DEST_GAIN_G1                1
#define LOAD_CALIBRATION_DEST_GAIN_G2                2
#define LOAD_CALIBRATION_DEST_PEDESTAL_G0            3
#define LOAD_CALIBRATION_DEST_PEDESTAL_G1            4
#define LOAD_CALIBRATION_DEST_PEDESTAL_G2            5
#define LOAD_CALIBRATION_DEST_INTEGRATION_MAP        6
#define LOAD_CALIBRATION_DEST_INTEGRATION_WEIGHTS    7
#define LOAD_CALIBRATION_DEST_FRAME_GEN              8
#define LOAD_CALIBRATION_DEST_SPOT_FINDER_RES_MAP    9
#define LOAD_CALIBRATION_DEST_ROI_CALC              10
#define LOAD_CALIBRATION_DEST_PXL_MASK              11

#define SLS_DETECTOR_TYPE_JUNGFRAU               (0x3)
#define SLS_DETECTOR_TYPE_EIGER                  (0x1)

#define NET_IF_FRAME_GENERATOR                    0xF

struct LoadCalibrationConfig {
    uint32_t handle;
};

struct DataCollectionConfig {
    uint32_t nmodules; // Number of modules for data collection minus one (0 = 1 module, 1 = 2 modules, ..., 31 = 32 modules)
    uint32_t mode; // see MODE_* in common/Definitions.h; upper 16-bit of the mode are data_collection_id, that is returned with completion numbers
    float energy_kev; // One over energy in keV (this is bit-to-bit float value, use float+uint32_t union to assign the value)
    uint32_t nframes; // Number of frames for data collection
    uint32_t nstorage_cells; // Number of storage cells minus one (0 = 1SC, 1 = 2SC, ..., 15 = 16SC)
    uint32_t nsummation; // Summation of frames minus one (0 = no summation, 1 = 2 frames, 2 = 3 frames, ..., 255 = 256 frames)
    uint32_t sqrtmult; // Multiplication factor (minus one) of pixel value BEFORE taking square root function - should be square of the coeff used in other work
    int32_t pxlthreshold; // pixels with values lower than threshold at set to zero
};

struct DataCollectionStatus {
    uint32_t ctrl_reg;
    uint32_t max_modules;
    int32_t  jfjoch_synth_time; // unit time is signed
    uint32_t git_sha1;

    uint32_t jfjoch_fpga_major_ver;
    uint32_t jfjoch_fpga_minor_ver;
    uint32_t jfjoch_fpga_patch_ver;
    uint32_t jfjoch_fpga_variant;

    uint32_t hbm_size_bytes;
    uint32_t run_counter;
    uint64_t pipeline_stalls_host;
    uint64_t pipeline_stalls_hbm;
    uint32_t fifo_status;
    uint32_t reserved_0;
    uint64_t packets_processed;
    uint64_t packets_udp;
    uint64_t packets_sls;
    uint32_t udp_err_len;
    uint32_t udp_err_eth;
    uint64_t pipeline_beats_hbm;
    uint64_t pipeline_beats_host;
    double current_pulseid;
    char jfjoch_fpga_prerelease_ver[12];
};

struct NetworkStatus {
    uint32_t jfjoch_net_magic;
    uint32_t jfjoch_net_mode;
    uint64_t mac_addr;
    uint32_t ipv4_addr;
    uint32_t stat_rx_status;
    uint64_t packets_eth;
    uint64_t packets_icmp;
};

struct DeviceStatus {
    uint32_t mailbox_status_reg;
    uint32_t mailbox_err_reg;
    uint32_t mailbox_interrupt_status;

    uint32_t fpga_temp_C;

    uint32_t fpga_pcie_12V_I_mA;
    uint32_t fpga_pcie_3p3V_I_mA;
    uint32_t fpga_pcie_12V_V_mV;
    uint32_t fpga_pcie_3p3V_V_mV;

    uint32_t pcie_h2c_descriptors;
    uint32_t pcie_c2h_descriptors;
    uint32_t pcie_h2c_beats;
    uint32_t pcie_c2h_beats;

    uint32_t pcie_h2c_status;
    uint32_t pcie_c2h_status;

    uint32_t pcie_user_interrupt_mask;
    uint32_t pcie_user_interrupt_request;
    uint32_t pcie_user_interrupt_pending;
    uint32_t pcie_dma_interrupt_mask;
    uint32_t pcie_dma_interrupt_request;
    uint32_t pcie_dma_interrupt_pending;

    uint32_t hbm_0_temp_C;
    uint32_t hbm_1_temp_C;

    uint32_t qsfp_cage_0_temp_C;
    uint32_t qsfp_cage_1_temp_C;

    uint32_t work_compl_fifo_avail;
    uint32_t eth_link_status;
    uint32_t eth_link_count;
    int32_t  active_handles;

    uint64_t packets_udp;
    uint64_t packets_sls;

    uint64_t fpga_default_mac_addr;
    uint64_t fpga_default_mac_addrs_count;
    char     serial_number[64];
    char     device_number[64];
    char     fpga_firmware_version[64];
    bool     idle;

    uint8_t pcie_link_speed;
    uint8_t pcie_link_width;
};

struct FrameGeneratorConfig {
    uint64_t dest_mac_addr; // Use the same as source address
    uint32_t dest_ipv4_addr; // Use the same as source address
    double pulse_id;
    uint32_t frames; // Number of frames
    uint32_t modules; // Number of modules (1-32)
    uint32_t exptime;
    uint32_t debug;
    uint32_t detector_type; // EIGER = 1, JUNGFRAU = 3
    uint32_t images_in_memory; // value minus one
    uint32_t eiger_bit_depth; // 8, 16, 32
};

struct RegisterConfig {
    uint32_t addr;
    uint32_t val;
};

struct I2COperation {
    uint8_t dev_addr;
    uint8_t reg_addr;
    uint8_t content;
};

struct SpotFinderParameters {
    int32_t count_threshold;
    float snr_threshold;
    float min_d;
    float max_d;
    uint32_t min_pix_per_spot;
};

struct FPGAIPAddress {
    uint32_t addr; // network order (little endian)
    uint32_t if_number;
};

struct FPGAMacAddress {
    uint64_t addr; // network order (little endian)
    uint32_t if_number;
};

struct FPGANetworkMode {
    uint32_t mode;
    uint32_t if_number;
};
#pragma pack(pop)

#pragma pack(push)
#pragma pack(1)

struct IntegrationResult {
    uint32_t count;
    float sum;
};

struct SpotFindingResult {
    char strong_pixel[RAW_MODULE_SIZE / 8];
    int32_t count_threshold;
    float snr_threshold;
    int32_t strong_pixel_count;
    int32_t reserved[13];
};

struct ModuleStatistics {
    uint64_t frame_number;
    uint64_t timestamp;
    double pulse_id;
    uint32_t detector_type;
    int32_t max_value;
    int32_t load_calibration_destination; // reused as min value
    uint32_t err_pixels; // number of pixels with 0xffff value
    uint32_t exptime;
    uint32_t debug;
    uint32_t pedestal;
    uint32_t packet_count;
    uint32_t module_number;
    uint32_t saturated_pixels; // number of pixels with 0xc000 value
    uint64_t packet_mask[8]; // This is reserved for 256 packets/module
};

struct ROICount {
    int64_t sum;
    uint64_t sum2;
    float sum_x_weighted;
    float sum_y_weighted;
    uint32_t good_pixels;
    int32_t max_value;
};

struct DeviceOutput {
    int16_t pixels[RAW_MODULE_SIZE * 2]; // accommodate 32-bit output
    struct SpotFindingResult spot_finding_result;
    struct IntegrationResult integration_result[FPGA_INTEGRATION_BIN_COUNT];
    uint32_t adu_histogram[ADU_HISTO_BIN_COUNT];
    struct ROICount roi_counts[FPGA_ROI_COUNT];
    struct ModuleStatistics module_statistics;
};

#pragma pack(pop)

#endif //JUNGFRAUJOCH_JFJOCH_FPGA_H
