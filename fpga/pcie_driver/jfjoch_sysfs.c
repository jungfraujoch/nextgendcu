// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <linux/sysfs.h>
#include <linux/device.h>
#include <linux/pci.h>

#include "jfjoch_drv.h"

static ssize_t hbm0_temp_degC_show(struct device *dev,
                                   struct device_attribute *attr,
                                   char *buf) {
    struct miscdevice* miscdev = dev_get_drvdata(dev);
    struct jfjoch_drvdata* drvdata = container_of(miscdev, struct jfjoch_drvdata, miscdev);

    unsigned int val = ioread32(drvdata->bar0 + CMS_OFFSET + ADDR_CMS_HBM_TEMP1_INS_REG);
    return scnprintf(buf, PAGE_SIZE, "%d", val);
}

static ssize_t hbm1_temp_degC_show(struct device *dev,
                                   struct device_attribute *attr,
                                   char *buf) {
    struct miscdevice* miscdev = dev_get_drvdata(dev);
    struct jfjoch_drvdata* drvdata = container_of(miscdev, struct jfjoch_drvdata, miscdev);

    unsigned int val = ioread32(drvdata->bar0 + CMS_OFFSET + ADDR_CMS_HBM_TEMP2_INS_REG);
    return scnprintf(buf, PAGE_SIZE, "%d", val);
}

static ssize_t qsfp0_temp_degC_show(struct device *dev,
                                    struct device_attribute *attr,
                                    char *buf) {
    struct miscdevice* miscdev = dev_get_drvdata(dev);
    struct jfjoch_drvdata* drvdata = container_of(miscdev, struct jfjoch_drvdata, miscdev);

    unsigned int val = ioread32(drvdata->bar0 + CMS_OFFSET + ADDR_CMS_CAGE_TEMP0_INS_REG);
    return scnprintf(buf, PAGE_SIZE, "%d", val);
}

static ssize_t qsfp1_temp_degC_show(struct device *dev,
                                    struct device_attribute *attr,
                                    char *buf) {
    struct miscdevice* miscdev = dev_get_drvdata(dev);
    struct jfjoch_drvdata* drvdata = container_of(miscdev, struct jfjoch_drvdata, miscdev);

    unsigned int val = ioread32(drvdata->bar0 + CMS_OFFSET + ADDR_CMS_CAGE_TEMP1_INS_REG);
    return scnprintf(buf, PAGE_SIZE, "%d", val);
}

static ssize_t fpga_temp_degC_show(struct device *dev,
                                   struct device_attribute *attr,
                                   char *buf) {
    struct miscdevice* miscdev = dev_get_drvdata(dev);
    struct jfjoch_drvdata* drvdata = container_of(miscdev, struct jfjoch_drvdata, miscdev);

    unsigned int val = ioread32(drvdata->bar0 + CMS_OFFSET + ADDR_CMS_FPGA_TEMP_INS_REG);
    return scnprintf(buf, PAGE_SIZE, "%d", val);
}

static ssize_t fpga_power_mW_show(struct device *dev,
                                  struct device_attribute *attr,
                                  char *buf) {
    struct miscdevice* miscdev = dev_get_drvdata(dev);
    struct jfjoch_drvdata* drvdata = container_of(miscdev, struct jfjoch_drvdata, miscdev);

    u32 fpga_pcie_3p3V_I_mA  = ioread32(drvdata->bar0 + CMS_OFFSET + ADDR_CMS_3V3PEX_I_IN_INS_REG);
    u32 fpga_pcie_12V_I_mA   = ioread32(drvdata->bar0 + CMS_OFFSET + ADDR_CMS_12VPEX_I_IN_INS_REG);
    u32 fpga_pcie_3p3V_V_mV  = ioread32(drvdata->bar0 + CMS_OFFSET + ADDR_CMS_3V3_PEX_INS_REG);
    u32 fpga_pcie_12V_V_mV   = ioread32(drvdata->bar0 + CMS_OFFSET + ADDR_CMS_12V_PEX_INS_REG);
    u32 power_mW = (fpga_pcie_3p3V_I_mA * fpga_pcie_3p3V_V_mV + fpga_pcie_12V_I_mA * fpga_pcie_12V_V_mV) / 1000;
    return scnprintf(buf, PAGE_SIZE, "%d", power_mW);
}

static ssize_t status_show(struct device *dev,
                           struct device_attribute *attr,
                           char *buf) {
    struct miscdevice* miscdev = dev_get_drvdata(dev);
    struct jfjoch_drvdata* drvdata = container_of(miscdev, struct jfjoch_drvdata, miscdev);

    unsigned int val = ioread32(drvdata->bar0 + ACTION_CONFIG_OFFSET + ADDR_CTRL_REGISTER);
    return scnprintf(buf, PAGE_SIZE, "%d", val);
}

static ssize_t eth_aligned_show(struct device *dev,
                                struct device_attribute *attr,
                                char *buf) {
    struct miscdevice* miscdev = dev_get_drvdata(dev);
    struct jfjoch_drvdata* drvdata = container_of(miscdev, struct jfjoch_drvdata, miscdev);
    return scnprintf(buf, PAGE_SIZE, "0x%02x", jfjoch_network_check_link(drvdata));
}

static ssize_t git_sha1_show(struct device *dev,
                             struct device_attribute *attr,
                             char *buf) {
    struct miscdevice* miscdev = dev_get_drvdata(dev);
    struct jfjoch_drvdata* drvdata = container_of(miscdev, struct jfjoch_drvdata, miscdev);
    return scnprintf(buf, PAGE_SIZE, "%x", drvdata->git_sha1);
}

static ssize_t max_modules_show(struct device *dev,
                                struct device_attribute *attr,
                                char *buf) {
    struct miscdevice* miscdev = dev_get_drvdata(dev);
    struct jfjoch_drvdata* drvdata = container_of(miscdev, struct jfjoch_drvdata, miscdev);
    return scnprintf(buf, PAGE_SIZE, "%d", drvdata->max_modules);
}

static ssize_t stalls_show(struct device *dev,
                           struct device_attribute *attr,
                           char *buf) {
    struct miscdevice* miscdev = dev_get_drvdata(dev);
    struct jfjoch_drvdata* drvdata = container_of(miscdev, struct jfjoch_drvdata, miscdev);

    u64 val0 = ioread32(drvdata->bar0 + ACTION_CONFIG_OFFSET + ADDR_STALLS_HBM_LO);
    u64 val1 = ioread32(drvdata->bar0 + ACTION_CONFIG_OFFSET + ADDR_STALLS_HBM_HI);
    u64 val = (val1 << 32) | val0;
    return scnprintf(buf, PAGE_SIZE, "%lld", val);
}

static ssize_t nbuffers_show(struct device *dev,
                             struct device_attribute *attr,
                             char *buf) {
    return scnprintf(buf, 25, "%d", nbuffer);
}

static ssize_t active_handles_show(struct device *dev,
                                   struct device_attribute *attr,
                                   char *buf) {
    struct miscdevice* miscdev = dev_get_drvdata(dev);
    struct jfjoch_drvdata* drvdata = container_of(miscdev, struct jfjoch_drvdata, miscdev);
    return scnprintf(buf, 25, "%d", atomic_read(&drvdata->active_handles));
}

static ssize_t version_show(struct device *dev,
                             struct device_attribute *attr,
                             char *buf) {
    struct miscdevice* miscdev = dev_get_drvdata(dev);
    struct jfjoch_drvdata* drvdata = container_of(miscdev, struct jfjoch_drvdata, miscdev);
    return scnprintf(buf, 64, "%s", drvdata->fpga_version);
}

static ssize_t revision_show(struct device *dev,
                             struct device_attribute *attr,
                             char *buf) {
    struct miscdevice* miscdev = dev_get_drvdata(dev);
    struct jfjoch_drvdata* drvdata = container_of(miscdev, struct jfjoch_drvdata, miscdev);
    return scnprintf(buf, 25, "%d", drvdata->revision);
}

static ssize_t fpga_serial_number_show(struct device *dev,
                                       struct device_attribute *attr,
                                       char *buf) {
    struct miscdevice* miscdev = dev_get_drvdata(dev);
    struct jfjoch_drvdata* drvdata = container_of(miscdev, struct jfjoch_drvdata, miscdev);
    return scnprintf(buf, 25, "%s", drvdata->fpga_serial_number);
}

static ssize_t pcie_link_show(struct device *dev,
                              struct device_attribute *attr,
                              char *buf) {
    struct miscdevice* miscdev = dev_get_drvdata(dev);
    struct jfjoch_drvdata* drvdata = container_of(miscdev, struct jfjoch_drvdata, miscdev);
    u16 pcie_link_status = jfjoch_get_pcie_link_status(drvdata);
    u32 pcie_link_speed = pcie_link_status & PCI_EXP_LNKSTA_CLS;
    u32 pcie_link_width = (pcie_link_status & PCI_EXP_LNKSTA_NLW) >> PCI_EXP_LNKSTA_NLW_SHIFT;
    return scnprintf(buf, 25, "Gen%ux%u", pcie_link_speed, pcie_link_width);
}

static struct device_attribute device_attrs[] = {
        __ATTR_RO(hbm0_temp_degC),
        __ATTR_RO(hbm1_temp_degC),
        __ATTR_RO(qsfp0_temp_degC),
        __ATTR_RO(qsfp1_temp_degC),
        __ATTR_RO(fpga_temp_degC),
        __ATTR_RO(fpga_power_mW),
        __ATTR_RO(nbuffers),
        __ATTR_RO(stalls),
        __ATTR_RO(status),
        __ATTR_RO(git_sha1),
        __ATTR_RO(revision),
        __ATTR_RO(max_modules),
        __ATTR_RO(eth_aligned),
        __ATTR_RO(active_handles),
        __ATTR_RO(fpga_serial_number),
        __ATTR_RO(version),
        __ATTR_RO(pcie_link)
};

int jfjoch_register_sysfs(struct jfjoch_drvdata *drvdata) {
    int i, err;

    if (!drvdata)
        return -EINVAL;

    for (i = 0; i < ARRAY_SIZE(device_attrs); i++) {
        err = device_create_file(drvdata->miscdev.this_device, &device_attrs[i]);
        if (err) {
            dev_err(drvdata->miscdev.this_device, "Failed to create sysfs file");
            break;
        }
    }

    if (err) {
        while (--i >= 0)
            device_remove_file(drvdata->miscdev.this_device,
                               &device_attrs[i]);
    }
    return err;
}

void jfjoch_unregister_sysfs(struct jfjoch_drvdata *drvdata) {
    int i;

    for (i = 0; i < ARRAY_SIZE(device_attrs); i++)
        device_remove_file(drvdata->miscdev.this_device, &device_attrs[i]);
}
