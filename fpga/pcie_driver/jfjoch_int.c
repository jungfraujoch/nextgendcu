// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "jfjoch_drv.h"

static irqreturn_t jfjoch_irq_compl(int irq, void *data) {
    struct jfjoch_drvdata *drvdata = data;
    u32 empty, handle, output, data_collection_id;
    // For interrupt handler, it is guaranteed that same interrupt is masked
    // Kfifo doesn't need synchronization for writers if single writer
    // No need of spinlock

    // acknowledge interrupt
    iowrite32(MAILBOX_INTERRUPT_RIT, drvdata->bar0 + MAILBOX_OFFSET + ADDR_MAILBOX_IS);

    empty = ioread32(drvdata->bar0 + MAILBOX_OFFSET + ADDR_MAILBOX_STATUS) & MAILBOX_EMPTY;
    while (!empty) {
        output = ioread32(drvdata->bar0 + MAILBOX_OFFSET + ADDR_MAILBOX_RDDATA);
        handle = output & 0xFFFF;
        data_collection_id = (output >> 16) & 0xFFFF;

        // Purge messages should not go into the work_compl FIFO
        if (data_collection_id != DATA_COLLECTION_ID_PURGE)
            kfifo_put(&drvdata->work_compl, output);

        // Decrease count of active handles - don't include START and END (these have only completion, no request)
        if ((handle != HANDLE_START) && (handle != HANDLE_END))
            atomic_dec(&drvdata->active_handles);

        empty = ioread32(drvdata->bar0 + MAILBOX_OFFSET + ADDR_MAILBOX_STATUS) & MAILBOX_EMPTY;
    }
    wake_up_interruptible_all(&drvdata->work_compl_wait_queue);
    return IRQ_HANDLED;
}

int jfjoch_setup_pcie_interrupt(struct pci_dev *pdev) {
    int ret;
    struct jfjoch_drvdata *drvdata = pci_get_drvdata(pdev);

    ret = pci_alloc_irq_vectors(pdev, 1, 1, PCI_IRQ_MSIX);
    if (ret != 1)
        return -ENOSPC;
    ret = pci_request_irq(pdev, 0, jfjoch_irq_compl, NULL, drvdata, "Jungraujoch work completion ready" );
    if (ret) {
        pci_free_irq_vectors(pdev);
        return ret;
    }

    // Set Mailbox receive interrupt threshold to 0 (i.e. interrupt if receive queue elements > 0)
    iowrite32(0    , drvdata->bar0 + MAILBOX_OFFSET + ADDR_MAILBOX_RIT);
    // Set Mailbox receive interrupt to enable
    iowrite32(MAILBOX_INTERRUPT_RIT , drvdata->bar0 + MAILBOX_OFFSET + ADDR_MAILBOX_IE);

    // Enable user interrupt lines 1 and 2 in XDMA core
    iowrite32((1<<1), drvdata->bar0 + PCIE_OFFSET + ADDR_XDMA_USER_IE_MASK);

    return 0;
}

void jfjoch_free_pcie_interrupt(struct  pci_dev *pdev) {
    struct jfjoch_drvdata *drvdata = pci_get_drvdata(pdev);

    pci_free_irq(pdev, 0, drvdata);
    pci_free_irq_vectors(pdev);
}