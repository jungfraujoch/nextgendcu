// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include <linux/delay.h>

#include "jfjoch_drv.h"

#define AMD_I2C_CTRL_REG               (0x100)
#define AMD_I2C_STATUS_REG             (0x104)
#define AMD_I2C_TX_FIFO                (0x108)
#define AMD_I2C_RX_FIFO                (0x10C)
#define AMD_I2C_RX_FIFO_PIRQ           (0x120)

#define AMD_I2C_TX_FIFO_START            (0x100)
#define AMD_I2C_TX_FIFO_STOP             (0x200)

#define AMD_I2C_CTRL_REG_I2C_ENABLE      (0x01)
#define AMD_I2C_CTRL_REG_TX_FIFO_RST     (0x02)

#define AMD_I2C_STATUS_REG_BUS_BUSY      (1<<2)
#define AMD_I2C_STATUS_REG_TX_FIFO_EMPTY (1<<7)
#define AMD_I2C_STATUS_REG_RX_FIFO_EMPTY (1<<6)

static int i2c_init(struct jfjoch_drvdata *drvdata, size_t i2c_offset) {
    struct device *const dev = &drvdata->pdev->dev;
    u32 status_reg;

    // Set the RX_FIFO depth to one byte
    iowrite32(0xF, drvdata->bar0 + i2c_offset + AMD_I2C_RX_FIFO_PIRQ);
    // Reset TX FIFO
    iowrite32(AMD_I2C_CTRL_REG_TX_FIFO_RST, drvdata->bar0 + i2c_offset + AMD_I2C_CTRL_REG);
    // Enable I2C
    iowrite32(AMD_I2C_CTRL_REG_I2C_ENABLE, drvdata->bar0 + i2c_offset + AMD_I2C_CTRL_REG);

    status_reg = ioread32(drvdata->bar0 + i2c_offset + AMD_I2C_STATUS_REG);
    if (status_reg != (AMD_I2C_STATUS_REG_RX_FIFO_EMPTY | AMD_I2C_STATUS_REG_TX_FIFO_EMPTY))
        dev_err(dev, "I2C init result: %x", status_reg);

    return 0;
}

static int i2c_wait_while_rx_fifo_empty(struct jfjoch_drvdata *drvdata, size_t i2c_offset) {
    struct device *const dev = &drvdata->pdev->dev;
    int i = 0;
    u32 status_reg = ioread32(drvdata->bar0 + i2c_offset + AMD_I2C_STATUS_REG);

    while (i < 1000) {
        if ((status_reg & AMD_I2C_STATUS_REG_RX_FIFO_EMPTY) == 0 )
            return 0;

        udelay(10);
        status_reg = ioread32(drvdata->bar0 + i2c_offset + AMD_I2C_STATUS_REG);
        i++;
    }

    dev_err(dev, "I2C read: RX FIFO empty after 10 ms");
    return -EIO;
}

static int i2c_wait_while_busy(struct jfjoch_drvdata *drvdata, size_t i2c_offset) {
    struct device *const dev = &drvdata->pdev->dev;
    int i = 0;
    u32 status_reg = ioread32(drvdata->bar0 + i2c_offset + AMD_I2C_STATUS_REG);
    while (i < 1000) {
        if ((status_reg & AMD_I2C_STATUS_REG_BUS_BUSY) == 0)
            return 0;

        udelay(10);
        i++;
        status_reg = ioread32(drvdata->bar0 + i2c_offset + AMD_I2C_STATUS_REG);
    }

    dev_err(dev, "I2C op: bus busy after 10 ms");
    return -EIO;
}

static int i2c_write(struct jfjoch_drvdata *drvdata, size_t i2c_offset, const struct I2COperation *operation) {
    struct device *const dev = &drvdata->pdev->dev;
    u32 status_reg;
    int err;

    // Disable I2C
    iowrite32(0, drvdata->bar0 + i2c_offset + AMD_I2C_CTRL_REG);
    // Enable I2C
    iowrite32(AMD_I2C_CTRL_REG_I2C_ENABLE, drvdata->bar0 + i2c_offset + AMD_I2C_CTRL_REG);

    status_reg = ioread32(drvdata->bar0 + i2c_offset + AMD_I2C_STATUS_REG);
    if (((status_reg & AMD_I2C_STATUS_REG_BUS_BUSY) != 0 )
        || ((status_reg & AMD_I2C_STATUS_REG_RX_FIFO_EMPTY) == 0 )
        || ((status_reg & AMD_I2C_STATUS_REG_TX_FIFO_EMPTY) == 0 )) {
        dev_err(dev, "I2C not ready to write SR: %x", status_reg);
        return -EIO;
    }

    iowrite32(AMD_I2C_TX_FIFO_START | operation->dev_addr, drvdata->bar0 + i2c_offset + AMD_I2C_TX_FIFO);
    iowrite32(operation->reg_addr , drvdata->bar0 + i2c_offset + AMD_I2C_TX_FIFO);
    iowrite32(AMD_I2C_TX_FIFO_STOP | operation->content, drvdata->bar0 + i2c_offset + AMD_I2C_TX_FIFO);

    err = i2c_wait_while_busy(drvdata, i2c_offset);
    if (err != 0)
        return err;
    return 0;
}

static int i2c_write_reg_addr(struct jfjoch_drvdata *drvdata, size_t i2c_offset, const struct I2COperation *operation) {
    struct device *const dev = &drvdata->pdev->dev;
    u32 status_reg;
    int err;

    // Disable I2C
    iowrite32(0, drvdata->bar0 + i2c_offset + AMD_I2C_CTRL_REG);
    // Enable I2C
    iowrite32(AMD_I2C_CTRL_REG_I2C_ENABLE, drvdata->bar0 + i2c_offset + AMD_I2C_CTRL_REG);

    status_reg = ioread32(drvdata->bar0 + i2c_offset + AMD_I2C_STATUS_REG);
    if (((status_reg & AMD_I2C_STATUS_REG_BUS_BUSY) != 0 )
        || ((status_reg & AMD_I2C_STATUS_REG_RX_FIFO_EMPTY) == 0 )
        || ((status_reg & AMD_I2C_STATUS_REG_TX_FIFO_EMPTY) == 0 )) {
        dev_err(dev, "I2C not ready to write SR: %x", status_reg);
        return -EIO;
    }

    iowrite32(operation->dev_addr | AMD_I2C_TX_FIFO_START, drvdata->bar0 + i2c_offset + AMD_I2C_TX_FIFO);
    iowrite32(operation->reg_addr | AMD_I2C_TX_FIFO_STOP, drvdata->bar0 + i2c_offset + AMD_I2C_TX_FIFO);

    err = i2c_wait_while_busy(drvdata, i2c_offset);
    if (err != 0)
        return err;

    return 0;
}

static int i2c_read(struct jfjoch_drvdata *drvdata, size_t i2c_offset, struct I2COperation *operation) {
    struct device *const dev = &drvdata->pdev->dev;
    int err;
    u32 status_reg;

    // Disable I2C
    iowrite32(0, drvdata->bar0 + i2c_offset + AMD_I2C_CTRL_REG);
    // Enable I2C
    iowrite32(AMD_I2C_CTRL_REG_I2C_ENABLE, drvdata->bar0 + i2c_offset + AMD_I2C_CTRL_REG);

    status_reg = ioread32(drvdata->bar0 + i2c_offset + AMD_I2C_STATUS_REG);
    if (((status_reg & AMD_I2C_STATUS_REG_BUS_BUSY) != 0 )
        || ((status_reg & AMD_I2C_STATUS_REG_RX_FIFO_EMPTY) == 0 )
        || ((status_reg & AMD_I2C_STATUS_REG_TX_FIFO_EMPTY) == 0 )) {
        dev_err(dev, "I2C not ready to read SR: %x", status_reg);
        return -EIO;
    }

    iowrite32(operation->dev_addr | 0x1 | AMD_I2C_TX_FIFO_START, drvdata->bar0 + i2c_offset + AMD_I2C_TX_FIFO);
    iowrite32(0x1 | AMD_I2C_TX_FIFO_STOP, drvdata->bar0 + i2c_offset + AMD_I2C_TX_FIFO);

    err = i2c_wait_while_rx_fifo_empty(drvdata, i2c_offset);
    if (err != 0)
        return err;

    operation->content = ioread32(drvdata->bar0 + i2c_offset + AMD_I2C_RX_FIFO);

    err = i2c_wait_while_busy(drvdata, i2c_offset);
    if (err != 0)
        return err;
    return 0;
}

int jfjoch_i2c_si5394_init(struct jfjoch_drvdata *drvdata) {
    return i2c_init(drvdata, I2C_SI5394_OFFSET);
}

int jfjoch_i2c_si5394_read(struct jfjoch_drvdata *drvdata, struct I2COperation *operation) {
    int tmp = i2c_write_reg_addr(drvdata, I2C_SI5394_OFFSET, operation);
    if (tmp != 0)
        return tmp;
    return i2c_read(drvdata, I2C_SI5394_OFFSET, operation);
}

int jfjoch_i2c_si5394_write(struct jfjoch_drvdata *drvdata, const struct I2COperation *operation) {
    return i2c_write(drvdata, I2C_SI5394_OFFSET, operation);
}
