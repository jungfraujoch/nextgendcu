// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "jfjoch_drv.h"

int jfjoch_setup_pcie_for_dma(struct pci_dev *pdev) {
    struct device *const dev = &pdev->dev;

    // Set DMA masks
    int err = dma_set_mask(dev, DMA_BIT_MASK(64));
    if (err) {
        dev_err(dev, "Failed to set 64-bit DMA mask (%d)\n", err);
        return err;
    }

    err = dma_set_mask_and_coherent(dev, DMA_BIT_MASK(64));
    if (err) {
        dev_err(dev, "Failed to set 64-bit DMA consistent mask (%d)\n", err);
        return err;
    }

    // Enable bus master for the device
    pci_set_master(pdev);
    return 0;
}

int jfjoch_map_cfg_bar(struct pci_dev *pdev) {
    struct jfjoch_drvdata *drvdata = pci_get_drvdata(pdev);
    struct device *const dev = &pdev->dev;

    // Request BAR 0 to map
    int err = pci_request_region(pdev, 0, "jfjoch");
    if (err) {
        dev_err(dev, "Unable to request PCI memory I/O addresses\n");
        return err;
    }

    // Map BAR 0 for configuration
    drvdata->bar0 = pci_iomap(pdev, 0, 0);
    if (drvdata->bar0 == NULL) {
        dev_err(dev, "Unable to map PCI memory I/O addresses\n");
        pci_release_region(pdev, 0); // Release requested region
        return -ENOMEM;
    }
    return 0;
}

int jfjoch_check_version(struct pci_dev *pdev) {
    struct device *const dev = &pdev->dev;
    struct jfjoch_drvdata *drvdata = pci_get_drvdata(pdev);

    // Check if PCIe revision ID is matching the driver
    drvdata->revision = pdev->revision;

    if (drvdata->revision != JFJOCH_FPGA_REVISION) {
        dev_err(dev, "Jungfraujoch FPGA is flashed with design made with revision %x, while driver is compiled with revision %x.\n",
                drvdata->revision, JFJOCH_FPGA_REVISION);
        dev_err(dev, "Given FPGA releases have braking changes in FPGA-driver interface (register map, data structure size, etc.)\n");
        dev_err(dev, "it is not safe and not supported at the moment to operate multiple releases with one driver.\n");
        return -EINVAL;
    }
    return 0;
}
