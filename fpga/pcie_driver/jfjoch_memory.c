// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "jfjoch_drv.h"

#include <linux/module.h>
#include <linux/string.h>
#include <linux/version.h>

int nbuffer = 512;
module_param(nbuffer, int, 0);

int jfjoch_alloc_phys_continuous_buf(struct pci_dev *pdev) {
    struct jfjoch_drvdata *drvdata = pci_get_drvdata(pdev);
    struct device *const dev = &pdev->dev;
    int i;

    drvdata->nbuf = 0;
    drvdata->bufs = kzalloc(sizeof(struct jfjoch_buf) * nbuffer, GFP_KERNEL);

    if (drvdata->bufs == NULL) {
        dev_err(dev, "Failed to allocate local structure\n");
        return -ENOMEM;
    }

    for (i = 0; i < nbuffer; i++) {
        dma_addr_t bus_addr;
        void *tmp = dma_alloc_coherent(&pdev->dev, FPGA_BUFFER_LOCATION_SIZE, &bus_addr, GFP_KERNEL);

        if (tmp == NULL) {
            dev_err(dev, "Failed to allocate %d PCI consistent buffer\n", i);
            jfjoch_free_phys_continuous_buf(pdev);
            return -ENOMEM;
        }

        // Save DMA address to address table on FPGA
        iowrite32(PCI_DMA_L(bus_addr), drvdata->bar0 + ADDRESS_TABLE_OFFSET + i * 2 * 4);
        iowrite32(PCI_DMA_H(bus_addr), drvdata->bar0 + ADDRESS_TABLE_OFFSET + (i * 2 + 1) * 4);

        drvdata->bufs[i].kernel_address = tmp;
        drvdata->bufs[i].dma_address = bus_addr;
        drvdata->nbuf++;
    }

    return 0;
}

void jfjoch_free_phys_continuous_buf(struct pci_dev *pdev) {
    int i;
    struct jfjoch_drvdata *drvdata = pci_get_drvdata(pdev);
    for (i = 0; i < drvdata->nbuf; i++) {
        if (drvdata->bufs[i].kernel_address != NULL) {
            dma_free_coherent(&pdev->dev,
                              FPGA_BUFFER_LOCATION_SIZE,
                              drvdata->bufs[i].kernel_address,
                              drvdata->bufs[i].dma_address);
        }
    }
    kfree(drvdata->bufs);
}

int jfjoch_cdev_mmap(struct file *file, struct vm_area_struct *vma) {
    unsigned long offset, buffer_number, len;

    struct jfjoch_drvdata *drvdata = container_of(file->private_data, struct jfjoch_drvdata, miscdev);

    if (vma->vm_flags & VM_EXEC) {
        return -EINVAL;
    }

    // In newer kernel use:
#if LINUX_VERSION_CODE <= KERNEL_VERSION(6,3,0)
    vma->vm_flags |= VM_DONTEXPAND | VM_DONTDUMP | VM_DONTCOPY | VM_LOCKED;
#else
     vm_flags_set(vma, (VM_DONTEXPAND | VM_DONTDUMP | VM_DONTCOPY | VM_LOCKED));
#endif
    // Don't allow region to be expanded (i.e. via mremap() in user mode), to be copied on fork, or dumped, or swapped

    // pgoff determines which buffer to use
    offset = vma->vm_pgoff << PAGE_SHIFT;

    // Reset offset to zero
    vma->vm_pgoff = 0;

    // Offset must be multiple of buffer size
    if (offset % FPGA_BUFFER_LOCATION_SIZE)
        return -EINVAL;

    // Buffer number must be within allocated range
    buffer_number = offset / FPGA_BUFFER_LOCATION_SIZE;
    if (buffer_number >= drvdata->nbuf)
        return -EINVAL;

    // Size of mapped region must be exactly one buffer size
    len = vma->vm_end - vma->vm_start;
    if (len != FPGA_BUFFER_LOCATION_SIZE)
        return -EINVAL;

    return dma_mmap_coherent(&drvdata->pdev->dev,
                             vma,
                             drvdata->bufs[buffer_number].kernel_address,
                             drvdata->bufs[buffer_number].dma_address,
                             FPGA_BUFFER_LOCATION_SIZE);
}
