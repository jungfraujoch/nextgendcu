#!/bin/bash

# Copyright (2019-2024) Paul Scherrer Institute

VERSION=1.0.0-rc.31

mkdir -p /usr/src/jfjoch-${VERSION}/src
cp dkms.conf /usr/src/jfjoch-${VERSION}
cp *.c *.h Makefile /usr/src/jfjoch-${VERSION}/src

dkms add -m jfjoch -v ${VERSION}
dkms install -m jfjoch -v ${VERSION}
