// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_JFJOCH_IOCTL_H
#define JUNGFRAUJOCH_JFJOCH_IOCTL_H

#include "jfjoch_fpga.h"

#ifdef __KERNEL__
#include <uapi/asm-generic/ioctl.h>
#else
#include <sys/ioctl.h>
#endif

#define IOCTL_JFJOCH_MAGIC 0xE1

#define IOCTL_JFJOCH_START           _IO (IOCTL_JFJOCH_MAGIC, 0)
#define IOCTL_JFJOCH_STATUS          _IOR(IOCTL_JFJOCH_MAGIC, 1, struct DataCollectionStatus)
#define IOCTL_JFJOCH_READ_CONFIG     _IOR(IOCTL_JFJOCH_MAGIC, 2, struct DataCollectionConfig)
#define IOCTL_JFJOCH_SET_CONFIG      _IOW(IOCTL_JFJOCH_MAGIC, 3, struct DataCollectionConfig)
#define IOCTL_JFJOCH_CANCEL          _IO (IOCTL_JFJOCH_MAGIC, 4)
#define IOCTL_JFJOCH_READ_WC_MBOX    _IOR(IOCTL_JFJOCH_MAGIC, 5, uint32_t)
#define IOCTL_JFJOCH_SEND_WR         _IOW(IOCTL_JFJOCH_MAGIC, 6, uint32_t)
#define IOCTL_JFJOCH_BUF_COUNT       _IOR(IOCTL_JFJOCH_MAGIC, 7, uint32_t)
// 8 is reserved
#define IOCTL_JFJOCH_GET_MAC         _IOWR(IOCTL_JFJOCH_MAGIC, 9, struct FPGAMacAddress)
#define IOCTL_JFJOCH_ISIDLE          _IOR(IOCTL_JFJOCH_MAGIC, 10, uint32_t)
#define IOCTL_JFJOCH_GET_DEV_STATUS  _IOR(IOCTL_JFJOCH_MAGIC, 11, struct DeviceStatus)
#define IOCTL_JFJOCH_END             _IO (IOCTL_JFJOCH_MAGIC, 12)
#define IOCTL_JFJOCH_RESET           _IO (IOCTL_JFJOCH_MAGIC, 13)
#define IOCTL_JFJOCH_NUMA            _IOR(IOCTL_JFJOCH_MAGIC, 14, uint32_t)
#define IOCTL_JFJOCH_CLR_CNTRS       _IO (IOCTL_JFJOCH_MAGIC, 15)
// 16 is reserved
#define IOCTL_JFJOCH_SET_IPV4        _IOW(IOCTL_JFJOCH_MAGIC, 17, struct FPGAIPAddress)
#define IOCTL_JFJOCH_GET_IPV4        _IOWR(IOCTL_JFJOCH_MAGIC, 18, struct FPGAIPAddress)
// 19-21 are reserved
#define IOCTL_JFJOCH_RUN_FRAME_GEN   _IOW(IOCTL_JFJOCH_MAGIC, 23, struct FrameGeneratorConfig)
// 24-25 are reserved
#define IOCTL_JFJOCH_READ_REGISTER   _IOWR(IOCTL_JFJOCH_MAGIC, 26, struct RegisterConfig )
#define IOCTL_JFJOCH_SET_SPOTFIN_PAR _IOW(IOCTL_JFJOCH_MAGIC, 27, struct SpotFinderParameters)
// 28-29 are reserved
#define IOCTL_JFJOCH_GET_SPOTFIN_PAR _IOR(IOCTL_JFJOCH_MAGIC, 30, struct SpotFinderParameters)
#define IOCTL_JFJOCH_LOAD_CALIB      _IOW(IOCTL_JFJOCH_MAGIC, 31, struct LoadCalibrationConfig)
#define IOCTL_JFJOCH_I2C_CLK_READ    _IOR(IOCTL_JFJOCH_MAGIC, 32, struct I2COperation)
#define IOCTL_JFJOCH_I2C_CLK_WRITE   _IOW(IOCTL_JFJOCH_MAGIC, 33, struct I2COperation)
#define IOCTL_JFJOCH_NET_STATUS      _IOWR(IOCTL_JFJOCH_MAGIC, 34, struct NetworkStatus)
#define IOCTL_JFJOCH_GET_REVISION    _IOR(IOCTL_JFJOCH_MAGIC, 35, uint32_t)
#define IOCTL_JFJOCH_GET_NET_MODE    _IOWR(IOCTL_JFJOCH_MAGIC, 36, struct FPGANetworkMode)
#define IOCTL_JFJOCH_SET_NET_MODE    _IOW(IOCTL_JFJOCH_MAGIC, 37, struct FPGANetworkMode)
#define IOCTL_JFJOCH_ENABLE_CMAC_2   _IO(IOCTL_JFJOCH_MAGIC, 38)

#endif //JUNGFRAUJOCH_JFJOCH_IOCTL_H
