// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "jfjoch_drv.h"
#include "jfjoch_ioctl.h"

long jfjoch_cdev_ioctl(struct file *file, unsigned int cmd, unsigned long arg) {
    struct jfjoch_drvdata *drvdata = container_of(file->private_data, struct jfjoch_drvdata, miscdev);
    struct DataCollectionStatus status;
    struct DataCollectionConfig config;
    struct DeviceStatus env_params;
    struct FrameGeneratorConfig frame_generator_config;
    struct RegisterConfig reg_config;
    struct SpotFinderParameters spot_finder_parameters;
    struct LoadCalibrationConfig load_calibration_config;
    struct I2COperation i2c_operation;
    struct NetworkStatus network_status;
    struct FPGAIPAddress ip_address;
    struct FPGAMacAddress mac_address;
    struct FPGANetworkMode net_mode;
    u32 exchange[16];
    int err;

    switch (cmd) {
        case IOCTL_JFJOCH_START:
            if (!(file->f_mode & FMODE_WRITE))
                return -EACCES;
            jfjoch_start(drvdata);
            return 0;
        case IOCTL_JFJOCH_END:
            if (!(file->f_mode & FMODE_WRITE))
                return -EACCES;
            jfjoch_end(drvdata);
            return 0;
        case IOCTL_JFJOCH_CANCEL:
            if (!(file->f_mode & FMODE_WRITE))
                return -EACCES;
            jfjoch_cancel(drvdata);
            return 0;
        case IOCTL_JFJOCH_STATUS:
            jfjoch_get_status(drvdata, &status);
            if (copy_to_user((char *) arg, &status, sizeof(struct DataCollectionStatus)) != 0)
                return -EFAULT;
            return 0;
        case IOCTL_JFJOCH_READ_CONFIG:
            jfjoch_get_config(drvdata, &config);
            if (copy_to_user((char *) arg, &config, sizeof(struct DataCollectionConfig)) != 0)
                return -EFAULT;
            return 0;
        case IOCTL_JFJOCH_SET_CONFIG:
            if (!(file->f_mode & FMODE_WRITE))
                return -EACCES;
            if (copy_from_user(&config, (char *) arg, sizeof(struct DataCollectionConfig)) != 0)
                return -EFAULT;
            return jfjoch_set_config(drvdata, &config);
        case IOCTL_JFJOCH_LOAD_CALIB:
            if (!(file->f_mode & FMODE_WRITE))
                return -EACCES;
            if (copy_from_user(&load_calibration_config, (char *) arg, sizeof(struct LoadCalibrationConfig)) != 0)
                return -EFAULT;
            return jfjoch_load_calibration(drvdata, &load_calibration_config);
        case IOCTL_JFJOCH_GET_DEV_STATUS:
            jfjoch_get_env_data(drvdata, &env_params);
            if (copy_to_user((char *) arg, &env_params, sizeof(struct DeviceStatus)) != 0)
                return -EFAULT;
            return 0;
        case IOCTL_JFJOCH_SEND_WR:
            if (!(file->f_mode & FMODE_WRITE))
                return -EACCES;
            if (copy_from_user(exchange, (char *) arg, sizeof(u32)) != 0)
                return -EFAULT;
            return jfjoch_send_wr(drvdata, exchange[0]);
        case IOCTL_JFJOCH_READ_WC_MBOX:
            // The function changes state of the FPGA, so require write permissions (while it is technically reading)
            if (!(file->f_mode & FMODE_WRITE))
                return -EACCES;
            err = jfjoch_read_wc(drvdata,exchange);
            if (err)
                return err;
            if (copy_to_user((char *) arg, exchange, sizeof(u32)) != 0)
                return -EFAULT;
            return 0;
        case IOCTL_JFJOCH_BUF_COUNT:
            if (copy_to_user((char *) arg, &nbuffer, sizeof(int32_t)) != 0)
                return -EFAULT;
            return 0;
        case IOCTL_JFJOCH_GET_MAC:
            if (copy_from_user(&mac_address, (char *) arg, sizeof(struct FPGAMacAddress)) != 0)
                return -EFAULT;
            jfjoch_get_mac_addr(drvdata, mac_address.if_number, &mac_address.addr);
            if (copy_to_user((char *) arg, &mac_address, sizeof(struct FPGAMacAddress)) != 0)
                return -EFAULT;
            return 0;
        case IOCTL_JFJOCH_SET_IPV4:
            if (!(file->f_mode & FMODE_WRITE))
                return -EACCES;
            if (copy_from_user(&ip_address, (char *) arg, sizeof(struct FPGAIPAddress)) != 0)
                return -EFAULT;
            err = jfjoch_set_ipv4_addr(drvdata, ip_address.if_number, &ip_address.addr);
            return err;
        case IOCTL_JFJOCH_GET_IPV4:
            if (copy_from_user(&ip_address, (char *) arg, sizeof(struct FPGAIPAddress)) != 0)
                return -EFAULT;
            err = jfjoch_get_ipv4_addr(drvdata, ip_address.if_number, &ip_address.addr);
            if (err != 0)
                return err;
            if (copy_to_user((char *) arg, &ip_address, sizeof(struct FPGAIPAddress)) != 0)
                return -EFAULT;
            return 0;
        case IOCTL_JFJOCH_SET_SPOTFIN_PAR:
            if (!(file->f_mode & FMODE_WRITE))
                return -EACCES;
            if (copy_from_user(&spot_finder_parameters, (char *) arg, sizeof(struct SpotFinderParameters)) != 0)
                return -EFAULT;
            jfjoch_set_spot_finder_parameters(drvdata, &spot_finder_parameters);
            return 0;
        case IOCTL_JFJOCH_GET_SPOTFIN_PAR:
            jfjoch_get_spot_finder_parameters(drvdata, &spot_finder_parameters);
            if (copy_to_user((char *) arg, &spot_finder_parameters, sizeof(struct SpotFinderParameters)) != 0)
                return -EFAULT;
            return 0;
        case IOCTL_JFJOCH_ISIDLE:
            jfjoch_is_idle(drvdata, exchange);
            if (copy_to_user((char *) arg, exchange, sizeof(int32_t)) != 0)
                return -EFAULT;
            return 0;
        case IOCTL_JFJOCH_RESET:
            if (!(file->f_mode & FMODE_WRITE))
                return -EACCES;
            jfjoch_reset(drvdata);
            return 0;
        case IOCTL_JFJOCH_NUMA:
            exchange[0] = drvdata->pdev->dev.numa_node;
            if (copy_to_user((char *) arg, exchange, sizeof(int32_t)) != 0)
                return -EFAULT;
            return 0;
        case IOCTL_JFJOCH_CLR_CNTRS:
            jfjoch_clr_net_counters(drvdata);
            return 0;
        case IOCTL_JFJOCH_RUN_FRAME_GEN:
            if (!(file->f_mode & FMODE_WRITE))
                return -EACCES;
            if (copy_from_user(&frame_generator_config, (char *) arg, sizeof(struct FrameGeneratorConfig)) != 0)
                return -EFAULT;
            return jfjoch_run_frame_gen(drvdata, &frame_generator_config);
        case IOCTL_JFJOCH_READ_REGISTER:
            // This is the most powerful option from security point of view and can only be performed by root
            if (!capable(CAP_SYS_ADMIN))
                return -EACCES;
            if (copy_from_user(&reg_config, (char *) arg, sizeof(struct RegisterConfig)) != 0)
                return -EFAULT;
            reg_config.val = jfjoch_read_register(drvdata, reg_config.addr);
            if (copy_to_user((char *) arg, &reg_config, sizeof(struct RegisterConfig)) != 0)
                return -EFAULT;
            return 0;
        case IOCTL_JFJOCH_NET_STATUS:
            if (copy_from_user(&network_status, (char *) arg, sizeof(struct NetworkStatus)) != 0)
                return -EFAULT;
            err = jfjoch_get_net_cfg(drvdata, network_status.jfjoch_net_magic, &network_status);
            if (err)
                return err;
            if (copy_to_user((char *) arg, &network_status, sizeof(struct NetworkStatus)) != 0)
                return -EFAULT;
            return 0;
        case IOCTL_JFJOCH_GET_REVISION:
            if (copy_to_user((char *) arg, &(drvdata->revision), sizeof(u32)) != 0)
                return -EFAULT;
            return 0;
        case IOCTL_JFJOCH_GET_NET_MODE:
            if (copy_from_user(&net_mode, (char *) arg, sizeof(struct FPGANetworkMode)) != 0)
                return -EFAULT;
            err = jfjoch_get_net_mode(drvdata, net_mode.if_number, &net_mode.mode);
            if (err != 0)
                return err;
            if (copy_to_user((char *) arg, &net_mode, sizeof(struct FPGANetworkMode)) != 0)
                return -EFAULT;
            return 0;
        case IOCTL_JFJOCH_SET_NET_MODE:
            if (!(file->f_mode & FMODE_WRITE))
                return -EACCES;
            if (copy_from_user(&net_mode, (char *) arg, sizeof(struct FPGANetworkMode)) != 0)
                return -EFAULT;
            err = jfjoch_set_net_mode(drvdata, net_mode.if_number, &net_mode.mode);
            return err;
        case IOCTL_JFJOCH_ENABLE_CMAC_2:
            if (!(file->f_mode & FMODE_WRITE))
                return -EACCES;
            return jfjoch_cmac_100g_enable(drvdata, 1);
        case IOCTL_JFJOCH_I2C_CLK_READ:
            if (copy_from_user(&i2c_operation, (char *) arg, sizeof(struct I2COperation)) != 0)
                return -EFAULT;
            err = jfjoch_i2c_si5394_read(drvdata, &i2c_operation) != 0;
            if (err)
                return err;
            if (copy_to_user((char *) arg, &i2c_operation, sizeof(struct I2COperation)) != 0)
                return -EFAULT;
            return 0;
        case IOCTL_JFJOCH_I2C_CLK_WRITE:
            if (!(file->f_mode & FMODE_WRITE))
                return -EACCES;
            if (copy_from_user(&i2c_operation, (char *) arg, sizeof(struct I2COperation)) != 0)
                return -EFAULT;
            return jfjoch_i2c_si5394_write(drvdata, &i2c_operation);
        default:
            return -ENOTTY;
    }
}