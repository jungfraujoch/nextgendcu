#!/bin/bash
# Taken from https://schneide.blog/2015/08/10/packaging-kernel-modulesdrivers-using-dkms/

VERSION=1.0.0-rc.31

/usr/sbin/dkms remove -m jfjoch -v ${VERSION} --all

exit 0
