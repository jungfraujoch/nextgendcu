// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "jfjoch_drv.h"
#include <linux/random.h>

void jfjoch_setup_network(struct jfjoch_drvdata *drvdata) {
    if (drvdata->fpga_variant == JFJOCH_FPGA_VARIANT_100G)
        jfjoch_cmac_100g_enable(drvdata, 0);

    // enable counters
    iowrite32(0, drvdata->bar0 + ACTION_CONFIG_OFFSET);
}

int jfjoch_cmac_100g_enable(struct jfjoch_drvdata *drvdata, u32 net_if) {
    u64 ptr = MAC_100G_0_OFFSET;

    if (drvdata->fpga_variant != JFJOCH_FPGA_VARIANT_100G)
        return -EINVAL;

    if (net_if != 0)
        return -EINVAL;

    // Enable RS_FEC
    iowrite32(0x03, drvdata->bar0 + ptr + 0x107C);

    // Restart TX and RX cores + deassert resets
    iowrite32((1 << 31) + (1 << 30), drvdata->bar0 + ptr + 0x0004);
    iowrite32(0x0, drvdata->bar0 + ptr + 0x0004);

    // TX Send RFI
    iowrite32(0x10, drvdata->bar0 + ptr + 0x000C);

    // Enable RX
    iowrite32(0x1, drvdata->bar0 + ptr + 0x0014);

    // start communication
    iowrite32(0x1, drvdata->bar0 + ptr + 0x000C);

    return 0;
}

u32 jfjoch_network_check_link(struct jfjoch_drvdata *drvdata) {
    int i;
    unsigned int val = 0;

    for (i = 0; i < drvdata->eth_links; i++) {
        if (ioread32((drvdata->bar0) + NET_CFG_OFFSET + i * 0x10000  + ADDR_NET_RX_STATUS) != 0)
            val |= (1UL<<i);
    }
    return val;
}