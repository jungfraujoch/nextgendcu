// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_JFJOCH_DRV_H
#define JUNGFRAUJOCH_JFJOCH_DRV_H

#include <linux/pci.h>
#include <linux/cdev.h>
#include <linux/mutex.h>
#include <linux/spinlock.h>
#include <linux/mm.h>
#include <linux/miscdevice.h>
#include <linux/kfifo.h>
#include <linux/wait.h>

#include "jfjoch_fpga.h"

// From Xilinx XDMA
/* obtain the 32 most significant (high) bits of a 32-bit or 64-bit address */
#define PCI_DMA_H(addr) ((addr >> 16) >> 16)
/* obtain the 32 least significant (low) bits of a 32-bit or 64-bit address */
#define PCI_DMA_L(addr) (addr & 0xffffffffUL)

/* bits of the SG DMA control register */
#define XDMA_CTRL_RUN_STOP			    (1UL << 0)
#define XDMA_CTRL_IE_DESC_STOPPED		(1UL << 1)
#define XDMA_CTRL_IE_DESC_COMPLETED		(1UL << 2)
#define XDMA_CTRL_IE_DESC_ALIGN_MISMATCH	(1UL << 3)
#define XDMA_CTRL_IE_MAGIC_STOPPED		(1UL << 4)
#define XDMA_CTRL_IE_IDLE_STOPPED		(1UL << 6)
#define XDMA_CTRL_IE_READ_ERROR			(0x1FUL << 9)
#define XDMA_CTRL_IE_WRITE_ERROR	    (0x1FUL << 14)
#define XDMA_CTRL_IE_DESC_ERROR			(0x1FUL << 19)
#define XDMA_CTRL_NON_INCR_ADDR			(1UL << 25)
#define XDMA_CTRL_POLL_MODE_WB			(1UL << 26)
#define XDMA_CTRL_STM_MODE_WB			(1UL << 27)

#define ADDR_XDMA_USER_IE_MASK          0x2004
#define ADDR_XDMA_DMA_IE_MASK           0x2010
#define ADDR_XDMA_USER_IR               0x2040
#define ADDR_XDMA_DMA_IR                0x2044
#define ADDR_XDMA_USER_IP               0x2048
#define ADDR_XDMA_DMA_IP                0x204C

// Offset for BAR #0 for action configuration
#define ACTION_CONFIG_OFFSET     (0x010000)
#define MAILBOX_OFFSET           (0x030000)
#define LOAD_CALIBRATION_OFFSET  (0x060000)
#define CMS_OFFSET               (0x0C0000)
#define MAC_100G_0_OFFSET        (0x100000)
#define PCIE_OFFSET              (0x090000)
#define FRAME_GEN_OFFSET         (0x080000)
#define ADDRESS_TABLE_OFFSET     (0x400000)
#define MAC_10G_0_OFFSET         (0x100000)
#define MAC_10G_1_OFFSET         (0x110000)
#define MAC_10G_2_OFFSET         (0x120000)
#define MAC_10G_3_OFFSET         (0x130000)
#define MAC_10G_4_OFFSET         (0x140000)
#define MAC_10G_5_OFFSET         (0x150000)
#define MAC_10G_6_OFFSET         (0x160000)
#define MAC_10G_7_OFFSET         (0x170000)

#define NET_CFG_OFFSET           (0x200000)

#define I2C_SI5394_OFFSET        (0x0A0000)

// Action config
#define ADDR_CTRL_REGISTER       0x0000
#define ADDR_GIT_SHA1            0x000C
#define ADDR_ACTION_TYPE         0x0010
#define ADDR_RELEASE_LEVEL       0x0014

#define ADDR_MAX_MODULES_FPGA    0x0020
#define ADDR_MODS_INT_PKT_GEN    0x0024
#define ADDR_STALLS_HOST_LO      0x0028
#define ADDR_STALLS_HOST_HI      0x002C
#define ADDR_STALLS_HBM_LO       0x0030
#define ADDR_STALLS_HBM_HI       0x0034
#define ADDR_FIFO_STATUS         0x0038

#define ADDR_PACKETS_PROC_LO     0x0040
#define ADDR_PACKETS_PROC_HI     0x0044
#define ADDR_PACKETS_ETH_LO      0x0048
#define ADDR_PACKETS_ETH_HI      0x004C
#define ADDR_PACKETS_ICMP_LO     0x0050
#define ADDR_PACKETS_ICMP_HI     0x0054
#define ADDR_PACKETS_UDP_LO      0x0058
#define ADDR_PACKETS_UDP_HI      0x005C
#define ADDR_PACKETS_SLS_LO      0x0060
#define ADDR_PACKETS_SLS_HI      0x0064
#define ADDR_PACKETS_ERR_LEN     0x0068
#define ADDR_PACKETS_ERR_ETH     0x006C

#define ADDR_NMODULES            0x020C
#define ADDR_DATA_COL_MODE       0x0210
#define ADDR_ONE_OVER_ENERGY     0x0214
#define ADDR_NFRAMES             0x0218
#define ADDR_NSTORAGE_CELLS      0x021C

#define ADDR_SPOT_FINDER_THRESHOLD  0x0100
#define ADDR_SPOT_FINDER_SNR        0x0104

// Network config
#define ADDR_NET_MODE            0x0004
#define ADDR_NET_MAC_ADDR_LO     0x0008
#define ADDR_NET_MAC_ADDR_HI     0x000C
#define ADDR_NET_IPV4_ADDR       0x0010
#define ADDR_NET_RX_STATUS       0x0014

#define ADDR_MAILBOX_WRDATA      0x00
#define ADDR_MAILBOX_RDDATA      0x08
#define ADDR_MAILBOX_STATUS      0x10
#define ADDR_MAILBOX_ERR         0x14
#define ADDR_MAILBOX_SIT         0x18
#define ADDR_MAILBOX_RIT         0x1C
#define ADDR_MAILBOX_IS          0x20
#define ADDR_MAILBOX_IE          0x24
#define ADDR_MAILBOX_CTRL        0x2C

#define MAILBOX_INTERRUPT_RIT    (1 << 1)
#define MAILBOX_EMPTY            (1 << 0)
#define MAILBOX_FULL             (1 << 1)
#define MAILBOX_STA              (1 << 2)
#define MAILBOX_RTA              (1 << 3)

#define CTRL_REGISTER_IDLE               (1<<1u)

#define ADDR_LOAD_CALIBRATION_CTRL    (LOAD_CALIBRATION_OFFSET | 0x000000)
#define ADDR_LOAD_CALIBRATION_RETURN  (LOAD_CALIBRATION_OFFSET | 0x000010)
#define ADDR_LOAD_CALIBRATION_HANDLE  (LOAD_CALIBRATION_OFFSET | 0x000018)

#define ADDR_FRAME_GEN_CTRL           (FRAME_GEN_OFFSET | 0x000000)
#define ADDR_FRAME_GEN_RETURN         (FRAME_GEN_OFFSET | 0x000010)
#define ADDR_FRAME_GEN_CONFIG         (FRAME_GEN_OFFSET | 0x000018)

#define JFJOCH_DMA_SETTINGS (XDMA_CTRL_RUN_STOP | XDMA_CTRL_IE_DESC_ALIGN_MISMATCH | XDMA_CTRL_IE_DESC_ERROR | XDMA_CTRL_IE_READ_ERROR \
                             | XDMA_CTRL_IE_WRITE_ERROR | XDMA_CTRL_IE_DESC_COMPLETED | XDMA_CTRL_STM_MODE_WB)

#define ADDR_CMS_CONTROL_REG          0x028018
#define ADDR_CMS_MB_RESETN_REG        0x020000
#define ADDR_CMS_FPGA_TEMP_INS_REG    0x028100 // in C
#define ADDR_CMS_3V3PEX_I_IN_INS_REG  0x028280 // in mA
#define ADDR_CMS_12VPEX_I_IN_INS_REG  0x0280D0 // in mA
#define ADDR_CMS_12V_PEX_INS_REG      0x028028 // in mV
#define ADDR_CMS_3V3_PEX_INS_REG      0x028034 // in mV
#define ADDR_CMS_HBM_TEMP1_INS_REG    0x028268 // in C
#define ADDR_CMS_HBM_TEMP2_INS_REG    0x0282BC // in C
#define ADDR_CMS_CAGE_TEMP0_INS_REG   0x028178 // in C
#define ADDR_CMS_CAGE_TEMP1_INS_REG   0x028184 // in C
#define ADDR_CMS_HOST_STATUS2_REG     0x02830C

#define MAX_FPGA_BUFFER   8192

extern int nbuffer;

struct jfjoch_buf {
    dma_addr_t           dma_address;
    void                *kernel_address;
};

struct jfjoch_drvdata {
    struct miscdevice     miscdev;
    struct pci_dev       *pdev;
    unsigned int          nbuf;
    struct jfjoch_buf    *bufs;
    char                 *bar0;
    char                  name[256];
    u32                   max_modules;
    u32                   git_sha1;
    u32                   revision;
    char                  fpga_version[64];
    u32                   fpga_variant;
    u32                   eth_links;
    u64                   default_mac;
    u32                   number_of_seq_mac_addrs;
    char                  fpga_serial_number[64];
    spinlock_t            file_write_open_count_spinlock;
    int                   file_write_open_count; // ensure

    // AXI mailbox requires to check if full before writing anything - this obviously must be atomic
    // assuming this is quick operation
    spinlock_t            work_request_submit_spinlock;

    // KFIFO has separate synchronization for read and write - i.e. one writer and multiple readers need mutex/spinlock
    // only for reading
    //
    // work_compl is not protected for put operation => this only happens in ISR
    // atomicity is guaranteed by the kernel (interrupt is masked during its own ISR)
    // however getting things from the queue happens in system call and can be executed in parallel
    // therefore this part is protected by mutex (it is assumed that waiting for interrupts can take seconds,
    // process has to be able to sleep while holding the lock => spinlock would not work)
    struct mutex          work_compl_read_mutex;
    DECLARE_KFIFO(work_compl, u32, MAX_FPGA_BUFFER); // protected by work_compl_read_mutex
    wait_queue_head_t     work_compl_wait_queue; // used for read completion queue method to wait for interrupt, ISR has wake-up call
    atomic_t              active_handles;
};

int jfjoch_register_misc_dev(struct pci_dev *pdev);
void jfjoch_unregister_misc_dev(struct pci_dev *pdev);

int jfjoch_alloc_phys_continuous_buf(struct pci_dev *pdev);
void jfjoch_free_phys_continuous_buf(struct pci_dev *pdev);

int jfjoch_setup_pcie_for_dma(struct pci_dev *pdev);
int jfjoch_map_cfg_bar(struct pci_dev *pdev);
int jfjoch_check_version(struct pci_dev *pdev);
int jfjoch_setup_pcie_interrupt(struct  pci_dev *pdev);
void jfjoch_free_pcie_interrupt(struct  pci_dev *pdev);

long jfjoch_cdev_ioctl(struct file *file, unsigned int cmd, unsigned long arg);
int jfjoch_cdev_mmap(struct file *file, struct vm_area_struct *vma);
int jfjoch_cdev_release(struct inode *inode, struct file *file);
int jfjoch_cdev_open(struct inode *inode, struct file *file);

void jfjoch_setup_cms(struct jfjoch_drvdata *drvdata);
void jfjoch_setup_network(struct jfjoch_drvdata *drvdata);

void jfjoch_start(struct jfjoch_drvdata *drvdata);
void jfjoch_cancel(struct jfjoch_drvdata *drvdata);
void jfjoch_end(struct jfjoch_drvdata *drvdata);

int jfjoch_send_wr(struct jfjoch_drvdata *drvdata, u32 handle);
int jfjoch_read_wc(struct jfjoch_drvdata *drvdata, u32 *output);
int jfjoch_set_config(struct jfjoch_drvdata *drvdata, const struct DataCollectionConfig *config);
void jfjoch_get_config(struct jfjoch_drvdata *drvdata, struct DataCollectionConfig *config);
void jfjoch_get_status(struct jfjoch_drvdata *drvdata, struct DataCollectionStatus *status);
int jfjoch_load_calibration(struct jfjoch_drvdata *drvdata, struct LoadCalibrationConfig *config);
int jfjoch_run_frame_gen(struct jfjoch_drvdata *drvdata, struct FrameGeneratorConfig *config);
void jfjoch_set_spot_finder_parameters(struct jfjoch_drvdata *drvdata, struct SpotFinderParameters *params);
void jfjoch_get_spot_finder_parameters(struct jfjoch_drvdata *drvdata, struct SpotFinderParameters *params);

int jfjoch_get_net_cfg(struct jfjoch_drvdata *drvdata, u32 net_if, struct NetworkStatus *status);
int jfjoch_set_mac_addr(struct jfjoch_drvdata *drvdata, u32 net_if, u64 *mac_addr);
int jfjoch_get_mac_addr(struct jfjoch_drvdata *drvdata, u32 net_if, u64 *mac_addr);
int jfjoch_set_ipv4_addr(struct jfjoch_drvdata *drvdata, u32 net_if, const u32 *addr);
int jfjoch_get_ipv4_addr(struct jfjoch_drvdata *drvdata, u32 net_if, u32 *addr);
int jfjoch_set_net_mode(struct jfjoch_drvdata *drvdata, u32 net_if, const u32 *net_mode);
int jfjoch_get_net_mode(struct jfjoch_drvdata *drvdata, u32 net_if, u32 *net_mode);

u64 jfjoch_read_cms_default_config(struct jfjoch_drvdata *drvdata);

void jfjoch_clr_net_counters(struct jfjoch_drvdata *drvdata);

void jfjoch_is_idle(struct jfjoch_drvdata *drvdata, uint32_t *output);

void jfjoch_get_env_data(struct jfjoch_drvdata *drvdata, struct DeviceStatus *env_params);

void jfjoch_reset(struct jfjoch_drvdata *drvdata);

uint32_t jfjoch_read_register(struct jfjoch_drvdata *drvdata, uint32_t addr);

int jfjoch_register_sysfs(struct jfjoch_drvdata *drvdata);
void jfjoch_unregister_sysfs(struct jfjoch_drvdata *drvdata);
int jfjoch_cmac_100g_enable(struct jfjoch_drvdata *drvdata, u32 net_if);
u32 jfjoch_network_check_link(struct jfjoch_drvdata *drvdata);

int jfjoch_i2c_si5394_init(struct jfjoch_drvdata *drvdata);
int jfjoch_i2c_si5394_read(struct jfjoch_drvdata *drvdata, struct I2COperation *operation);
int jfjoch_i2c_si5394_write(struct jfjoch_drvdata *drvdata, const struct I2COperation *operation);

u16 jfjoch_get_pcie_link_status(struct jfjoch_drvdata *drvdata);

static const struct file_operations jfjoch_cdev_fileops = {
        .mmap           = jfjoch_cdev_mmap,
        .unlocked_ioctl = jfjoch_cdev_ioctl,
        .open           = jfjoch_cdev_open,
        .release        = jfjoch_cdev_release,
        .llseek         = no_llseek
};

#endif //JUNGFRAUJOCH_JFJOCH_DRV_H
