# SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
# SPDX-License-Identifier: CERN-OHL-S-2.0

# Hierarchical cell: packet_proc
proc create_hier_cell_packet_proc { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2092 -severity "ERROR" "create_hier_cell_packet_proc() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 eth_in

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 eth_out

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 s_axi_cfg

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 udp_out


  # Create pins
  create_bd_pin -dir I aresetn
  create_bd_pin -dir I eth_clk
  create_bd_pin -dir I eth_reset
  create_bd_pin -dir I refclk200
  create_bd_pin -dir I stat_rx_status

  # Create instance: axis_clock_converter_rx, and set properties
  set axis_clock_converter_rx [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_clock_converter:1.1 axis_clock_converter_rx ]

  # Create instance: axis_clock_converter_tx, and set properties
  set axis_clock_converter_tx [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_clock_converter:1.1 axis_clock_converter_tx ]

  # Create instance: axis_dwidth_converter_rx, and set properties
  set axis_dwidth_converter_rx [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_dwidth_converter:1.1 axis_dwidth_converter_rx ]
  set_property -dict [ list \
   CONFIG.M_TDATA_NUM_BYTES {64} \
 ] $axis_dwidth_converter_rx

  # Create instance: axis_dwidth_converter_tx, and set properties
  set axis_dwidth_converter_tx [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_dwidth_converter:1.1 axis_dwidth_converter_tx ]
  set_property -dict [ list \
   CONFIG.M_TDATA_NUM_BYTES {8} \
 ] $axis_dwidth_converter_tx

  # Create instance: network_stack
  create_hier_cell_network_stack $hier_obj network_stack

  # Create instance: util_vector_logic_0, and set properties
  set util_vector_logic_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_0 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {not} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_notgate.png} \
 ] $util_vector_logic_0

  set_property -dict [list CONFIG.DIRECT {1}] [get_bd_cells network_stack/network_config_0]

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins udp_out] [get_bd_intf_pins network_stack/udp_out]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins eth_out] [get_bd_intf_pins axis_clock_converter_tx/M_AXIS]
  connect_bd_intf_net -intf_net Conn3 [get_bd_intf_pins eth_in] [get_bd_intf_pins axis_clock_converter_rx/S_AXIS]
  connect_bd_intf_net -intf_net Conn4 [get_bd_intf_pins s_axi_cfg] [get_bd_intf_pins network_stack/s_axi_cfg]
  connect_bd_intf_net -intf_net axis_clock_converter_rx_M_AXIS [get_bd_intf_pins axis_clock_converter_rx/M_AXIS] [get_bd_intf_pins axis_dwidth_converter_rx/S_AXIS]
  connect_bd_intf_net -intf_net axis_dwidth_converter_tx_M_AXIS [get_bd_intf_pins axis_clock_converter_tx/S_AXIS] [get_bd_intf_pins axis_dwidth_converter_tx/M_AXIS]
  connect_bd_intf_net -intf_net eth_in_1 [get_bd_intf_pins axis_dwidth_converter_rx/M_AXIS] [get_bd_intf_pins network_stack/eth_in]
  connect_bd_intf_net -intf_net network_stack_eth_out [get_bd_intf_pins axis_dwidth_converter_tx/S_AXIS] [get_bd_intf_pins network_stack/eth_out]

  # Create port connections
  connect_bd_net -net aresetn_1 [get_bd_pins aresetn] [get_bd_pins axis_clock_converter_rx/m_axis_aresetn] [get_bd_pins axis_clock_converter_tx/s_axis_aresetn] [get_bd_pins axis_dwidth_converter_rx/aresetn] [get_bd_pins axis_dwidth_converter_tx/aresetn] [get_bd_pins network_stack/ap_rst_n] [get_bd_pins network_stack/resetn]
  connect_bd_net -net eth_clk_1 [get_bd_pins eth_clk] [get_bd_pins axis_clock_converter_rx/s_axis_aclk] [get_bd_pins axis_clock_converter_tx/m_axis_aclk]
  connect_bd_net -net eth_reset_1 [get_bd_pins eth_reset] [get_bd_pins util_vector_logic_0/Op1]
  connect_bd_net -net eth_resetn_1 [get_bd_pins axis_clock_converter_rx/s_axis_aresetn] [get_bd_pins axis_clock_converter_tx/m_axis_aresetn] [get_bd_pins util_vector_logic_0/Res]
  connect_bd_net -net refclk200_1 [get_bd_pins refclk200] [get_bd_pins axis_clock_converter_rx/m_axis_aclk] [get_bd_pins axis_clock_converter_tx/s_axis_aclk] [get_bd_pins axis_dwidth_converter_rx/aclk] [get_bd_pins axis_dwidth_converter_tx/aclk] [get_bd_pins network_stack/axiclk]
  connect_bd_net -net network_stack_stat_rx_status_1 [get_bd_pins stat_rx_status] [get_bd_pins network_stack/stat_rx_status]

  # Restore current instance
  current_bd_instance $oldCurInst
}