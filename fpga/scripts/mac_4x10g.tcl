# SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
# SPDX-License-Identifier: CERN-OHL-S-2.0

# Hierarchical cell: mac_4x10g
proc create_hier_cell_mac_4x10g { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2092 -severity "ERROR" "create_hier_cell_mac_4x10g() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 m_axis

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:gt_rtl:1.0 qsfp1

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 qsfp_ref

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 s_axi


  # Create pins
  create_bd_pin -dir I -type rst axi_aresetn
  create_bd_pin -dir O led_green
  create_bd_pin -dir O led_yellow
  create_bd_pin -dir I -type clk refclk200
  create_bd_pin -dir I -type clk refclk100

  # Create instance: axis_interconnect_0, and set properties
  set axis_interconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_interconnect:2.1 axis_interconnect_0 ]
  set_property -dict [ list \
   CONFIG.ARB_ALGORITHM {3} \
   CONFIG.ARB_ON_MAX_XFERS {0} \
   CONFIG.ARB_ON_NUM_CYCLES {4} \
   CONFIG.ARB_ON_TLAST {1} \
   CONFIG.ENABLE_ADVANCED_OPTIONS {0} \
   CONFIG.M00_FIFO_DEPTH {256} \
   CONFIG.NUM_MI {1} \
   CONFIG.NUM_SI {4} \
   CONFIG.S00_FIFO_DEPTH {512} \
   CONFIG.S00_FIFO_MODE {1} \
   CONFIG.S00_HAS_REGSLICE {1} \
   CONFIG.S01_FIFO_DEPTH {512} \
   CONFIG.S01_FIFO_MODE {1} \
   CONFIG.S01_HAS_REGSLICE {1} \
   CONFIG.S02_FIFO_DEPTH {512} \
   CONFIG.S02_FIFO_MODE {1} \
   CONFIG.S02_HAS_REGSLICE {1} \
   CONFIG.S03_FIFO_DEPTH {512} \
   CONFIG.S03_FIFO_MODE {1} \
   CONFIG.S03_HAS_REGSLICE {1} \
 ] $axis_interconnect_0

  # Create instance: eth_4x10g_leds_0, and set properties
  set block_name eth_4x10g_leds
  set block_cell_name eth_4x10g_leds_0
  if { [catch {set eth_4x10g_leds_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $eth_4x10g_leds_0 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }

  # Create instance: packet_proc_0
  create_hier_cell_packet_proc $hier_obj packet_proc_0

  # Create instance: packet_proc_1
  create_hier_cell_packet_proc $hier_obj packet_proc_1

  # Create instance: packet_proc_2
  create_hier_cell_packet_proc $hier_obj packet_proc_2

  # Create instance: packet_proc_3
  create_hier_cell_packet_proc $hier_obj packet_proc_3

  # Create instance: smartconnect_0, and set properties
  set smartconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:smartconnect:1.0 smartconnect_0 ]
  set_property -dict [ list \
   CONFIG.NUM_CLKS {2} \
   CONFIG.NUM_MI {8} \
   CONFIG.NUM_SI {1} \
 ] $smartconnect_0

  # Create instance: util_vector_logic_4, and set properties
  set util_vector_logic_4 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_vector_logic:2.0 util_vector_logic_4 ]
  set_property -dict [ list \
   CONFIG.C_OPERATION {not} \
   CONFIG.C_SIZE {1} \
   CONFIG.LOGO_FILE {data/sym_notgate.png} \
 ] $util_vector_logic_4

  # Create instance: xlconstant_0, and set properties
  set xlconstant_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 xlconstant_0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {b101} \
   CONFIG.CONST_WIDTH {3} \
 ] $xlconstant_0

  # Create instance: xxv_ethernet_0, and set properties
  set xxv_ethernet_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xxv_ethernet:4.1 xxv_ethernet_0 ]
  set_property -dict [ list \
   CONFIG.BASE_R_KR {BASE-R} \
   CONFIG.ENABLE_GT_BOARD_INTERFACE {1} \
   CONFIG.ENABLE_PIPELINE_REG {1} \
   CONFIG.GT_GROUP_SELECT {Quad_X0Y7} \
   CONFIG.GT_REF_CLK_FREQ {161.1328125} \
   CONFIG.INCLUDE_AXI4_INTERFACE {1} \
   CONFIG.INCLUDE_STATISTICS_COUNTERS {0} \
   CONFIG.LANE1_GT_LOC {X0Y28} \
   CONFIG.LANE2_GT_LOC {X0Y29} \
   CONFIG.LANE3_GT_LOC {X0Y30} \
   CONFIG.LANE4_GT_LOC {X0Y31} \
   CONFIG.LINE_RATE {10} \
   CONFIG.NUM_OF_CORES {4} \
 ] $xxv_ethernet_0

  # Create instance: zero, and set properties
  set zero [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 zero ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $zero

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins qsfp_ref] [get_bd_intf_pins xxv_ethernet_0/gt_ref_clk]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins qsfp1] [get_bd_intf_pins xxv_ethernet_0/gt_serial_port]
  connect_bd_intf_net -intf_net Conn3 [get_bd_intf_pins s_axi] [get_bd_intf_pins smartconnect_0/S00_AXI]
  connect_bd_intf_net -intf_net S00_AXIS_1 [get_bd_intf_pins axis_interconnect_0/S00_AXIS] [get_bd_intf_pins packet_proc_0/udp_out]
  connect_bd_intf_net -intf_net S01_AXIS_1 [get_bd_intf_pins axis_interconnect_0/S01_AXIS] [get_bd_intf_pins packet_proc_1/udp_out]
  connect_bd_intf_net -intf_net S02_AXIS_1 [get_bd_intf_pins axis_interconnect_0/S02_AXIS] [get_bd_intf_pins packet_proc_2/udp_out]
  connect_bd_intf_net -intf_net S03_AXIS_1 [get_bd_intf_pins axis_interconnect_0/S03_AXIS] [get_bd_intf_pins packet_proc_3/udp_out]
  connect_bd_intf_net -intf_net axis_interconnect_0_M00_AXIS [get_bd_intf_pins m_axis] [get_bd_intf_pins axis_interconnect_0/M00_AXIS]
  connect_bd_intf_net -intf_net packet_proc_0_eth_out [get_bd_intf_pins packet_proc_0/eth_out] [get_bd_intf_pins xxv_ethernet_0/axis_tx_0]
  connect_bd_intf_net -intf_net packet_proc_1_eth_out [get_bd_intf_pins packet_proc_1/eth_out] [get_bd_intf_pins xxv_ethernet_0/axis_tx_1]
  connect_bd_intf_net -intf_net packet_proc_2_eth_out [get_bd_intf_pins packet_proc_2/eth_out] [get_bd_intf_pins xxv_ethernet_0/axis_tx_2]
  connect_bd_intf_net -intf_net packet_proc_3_eth_out [get_bd_intf_pins packet_proc_3/eth_out] [get_bd_intf_pins xxv_ethernet_0/axis_tx_3]
  connect_bd_intf_net -intf_net smartconnect_0_M00_AXI [get_bd_intf_pins smartconnect_0/M00_AXI] [get_bd_intf_pins xxv_ethernet_0/s_axi_0]
  connect_bd_intf_net -intf_net smartconnect_0_M01_AXI [get_bd_intf_pins smartconnect_0/M01_AXI] [get_bd_intf_pins xxv_ethernet_0/s_axi_1]
  connect_bd_intf_net -intf_net smartconnect_0_M02_AXI [get_bd_intf_pins smartconnect_0/M02_AXI] [get_bd_intf_pins xxv_ethernet_0/s_axi_2]
  connect_bd_intf_net -intf_net smartconnect_0_M03_AXI [get_bd_intf_pins smartconnect_0/M03_AXI] [get_bd_intf_pins xxv_ethernet_0/s_axi_3]
  connect_bd_intf_net -intf_net smartconnect_0_M04_AXI [get_bd_intf_pins packet_proc_0/s_axi_cfg] [get_bd_intf_pins smartconnect_0/M04_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M05_AXI [get_bd_intf_pins packet_proc_1/s_axi_cfg] [get_bd_intf_pins smartconnect_0/M05_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M06_AXI [get_bd_intf_pins packet_proc_2/s_axi_cfg] [get_bd_intf_pins smartconnect_0/M06_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M07_AXI [get_bd_intf_pins packet_proc_3/s_axi_cfg] [get_bd_intf_pins smartconnect_0/M07_AXI]
  connect_bd_intf_net -intf_net xxv_ethernet_0_axis_rx_0 [get_bd_intf_pins packet_proc_0/eth_in] [get_bd_intf_pins xxv_ethernet_0/axis_rx_0]
  connect_bd_intf_net -intf_net xxv_ethernet_0_axis_rx_1 [get_bd_intf_pins packet_proc_3/eth_in] [get_bd_intf_pins xxv_ethernet_0/axis_rx_3]
  connect_bd_intf_net -intf_net xxv_ethernet_0_axis_rx_2 [get_bd_intf_pins packet_proc_1/eth_in] [get_bd_intf_pins xxv_ethernet_0/axis_rx_1]
  connect_bd_intf_net -intf_net xxv_ethernet_0_axis_rx_3 [get_bd_intf_pins packet_proc_2/eth_in] [get_bd_intf_pins xxv_ethernet_0/axis_rx_2]

  # Create port connections
  connect_bd_net -net dclk_1 [get_bd_pins refclk100] [get_bd_pins smartconnect_0/aclk] [get_bd_pins xxv_ethernet_0/dclk] [get_bd_pins xxv_ethernet_0/s_axi_aclk_0] [get_bd_pins xxv_ethernet_0/s_axi_aclk_1] [get_bd_pins xxv_ethernet_0/s_axi_aclk_2] [get_bd_pins xxv_ethernet_0/s_axi_aclk_3]
  connect_bd_net -net eth_4x10g_leds_0_led_green [get_bd_pins led_green] [get_bd_pins eth_4x10g_leds_0/led_green]
  connect_bd_net -net eth_4x10g_leds_0_led_yellow [get_bd_pins led_yellow] [get_bd_pins eth_4x10g_leds_0/led_yellow]
  connect_bd_net -net eth_reset_0 [get_bd_pins packet_proc_0/eth_reset] [get_bd_pins xxv_ethernet_0/user_rx_reset_0]
  connect_bd_net -net eth_reset_1 [get_bd_pins packet_proc_1/eth_reset] [get_bd_pins xxv_ethernet_0/user_rx_reset_1]
  connect_bd_net -net eth_reset_2 [get_bd_pins packet_proc_2/eth_reset] [get_bd_pins xxv_ethernet_0/user_rx_reset_2]
  connect_bd_net -net eth_reset_3 [get_bd_pins packet_proc_3/eth_reset] [get_bd_pins xxv_ethernet_0/user_rx_reset_3]
  connect_bd_net -net refclk200_1 [get_bd_pins refclk200] [get_bd_pins axis_interconnect_0/S00_AXIS_ACLK] [get_bd_pins axis_interconnect_0/ACLK] [get_bd_pins axis_interconnect_0/M00_AXIS_ACLK] [get_bd_pins axis_interconnect_0/S01_AXIS_ACLK] [get_bd_pins axis_interconnect_0/S02_AXIS_ACLK] [get_bd_pins axis_interconnect_0/S03_AXIS_ACLK] [get_bd_pins packet_proc_0/refclk200] [get_bd_pins packet_proc_1/refclk200] [get_bd_pins packet_proc_2/refclk200] [get_bd_pins packet_proc_3/refclk200] [get_bd_pins smartconnect_0/aclk1]
  connect_bd_net -net s_axi_aresetn_0_1 [get_bd_pins axi_aresetn] [get_bd_pins axis_interconnect_0/ARESETN] [get_bd_pins axis_interconnect_0/M00_AXIS_ARESETN] [get_bd_pins axis_interconnect_0/S00_AXIS_ARESETN] [get_bd_pins axis_interconnect_0/S01_AXIS_ARESETN] [get_bd_pins axis_interconnect_0/S02_AXIS_ARESETN] [get_bd_pins axis_interconnect_0/S03_AXIS_ARESETN] [get_bd_pins packet_proc_0/aresetn] [get_bd_pins packet_proc_1/aresetn] [get_bd_pins packet_proc_2/aresetn] [get_bd_pins packet_proc_3/aresetn] [get_bd_pins smartconnect_0/aresetn] [get_bd_pins util_vector_logic_4/Op1] [get_bd_pins xxv_ethernet_0/s_axi_aresetn_0] [get_bd_pins xxv_ethernet_0/s_axi_aresetn_1] [get_bd_pins xxv_ethernet_0/s_axi_aresetn_2] [get_bd_pins xxv_ethernet_0/s_axi_aresetn_3]
  connect_bd_net -net util_vector_logic_4_Res [get_bd_pins util_vector_logic_4/Res] [get_bd_pins xxv_ethernet_0/sys_reset]
  connect_bd_net -net xlconstant_0_dout [get_bd_pins xlconstant_0/dout] [get_bd_pins xxv_ethernet_0/rxoutclksel_in_0] [get_bd_pins xxv_ethernet_0/rxoutclksel_in_1] [get_bd_pins xxv_ethernet_0/rxoutclksel_in_2] [get_bd_pins xxv_ethernet_0/rxoutclksel_in_3] [get_bd_pins xxv_ethernet_0/txoutclksel_in_0] [get_bd_pins xxv_ethernet_0/txoutclksel_in_1] [get_bd_pins xxv_ethernet_0/txoutclksel_in_2] [get_bd_pins xxv_ethernet_0/txoutclksel_in_3]
  connect_bd_net -net xxv_ethernet_0_stat_rx_status_0 [get_bd_pins eth_4x10g_leds_0/stat_rx_status_0] [get_bd_pins xxv_ethernet_0/stat_rx_status_0] [get_bd_pins packet_proc_0/stat_rx_status]
  connect_bd_net -net xxv_ethernet_0_stat_rx_status_1 [get_bd_pins eth_4x10g_leds_0/stat_rx_status_1] [get_bd_pins xxv_ethernet_0/stat_rx_status_1] [get_bd_pins packet_proc_1/stat_rx_status]
  connect_bd_net -net xxv_ethernet_0_stat_rx_status_2 [get_bd_pins eth_4x10g_leds_0/stat_rx_status_2] [get_bd_pins xxv_ethernet_0/stat_rx_status_2] [get_bd_pins packet_proc_2/stat_rx_status]
  connect_bd_net -net xxv_ethernet_0_stat_rx_status_3 [get_bd_pins eth_4x10g_leds_0/stat_rx_status_3] [get_bd_pins xxv_ethernet_0/stat_rx_status_3] [get_bd_pins packet_proc_3/stat_rx_status]
  connect_bd_net -net xxv_ethernet_0_tx_clk_out_0 [get_bd_pins packet_proc_0/eth_clk] [get_bd_pins xxv_ethernet_0/rx_core_clk_0] [get_bd_pins xxv_ethernet_0/tx_clk_out_0]
  connect_bd_net -net xxv_ethernet_0_tx_clk_out_1 [get_bd_pins packet_proc_1/eth_clk] [get_bd_pins xxv_ethernet_0/rx_core_clk_1] [get_bd_pins xxv_ethernet_0/tx_clk_out_1]
  connect_bd_net -net xxv_ethernet_0_tx_clk_out_2 [get_bd_pins packet_proc_2/eth_clk] [get_bd_pins xxv_ethernet_0/rx_core_clk_2] [get_bd_pins xxv_ethernet_0/tx_clk_out_2]
  connect_bd_net -net xxv_ethernet_0_tx_clk_out_3 [get_bd_pins packet_proc_3/eth_clk] [get_bd_pins xxv_ethernet_0/rx_core_clk_3] [get_bd_pins xxv_ethernet_0/tx_clk_out_3]
  connect_bd_net -net zero_dout [get_bd_pins xxv_ethernet_0/gtwiz_reset_rx_datapath_0] [get_bd_pins xxv_ethernet_0/gtwiz_reset_rx_datapath_1] [get_bd_pins xxv_ethernet_0/gtwiz_reset_rx_datapath_2] [get_bd_pins xxv_ethernet_0/gtwiz_reset_rx_datapath_3] [get_bd_pins xxv_ethernet_0/gtwiz_reset_tx_datapath_0] [get_bd_pins xxv_ethernet_0/gtwiz_reset_tx_datapath_1] [get_bd_pins xxv_ethernet_0/gtwiz_reset_tx_datapath_2] [get_bd_pins xxv_ethernet_0/gtwiz_reset_tx_datapath_3] [get_bd_pins xxv_ethernet_0/qpllreset_in_0] [get_bd_pins xxv_ethernet_0/rx_reset_0] [get_bd_pins xxv_ethernet_0/rx_reset_1] [get_bd_pins xxv_ethernet_0/rx_reset_2] [get_bd_pins xxv_ethernet_0/rx_reset_3] [get_bd_pins xxv_ethernet_0/tx_reset_0] [get_bd_pins xxv_ethernet_0/tx_reset_1] [get_bd_pins xxv_ethernet_0/tx_reset_2] [get_bd_pins xxv_ethernet_0/tx_reset_3] [get_bd_pins zero/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}
