# SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
# SPDX-License-Identifier: CERN-OHL-S-2.0

# Hierarchical cell: jungfraujoch
proc create_hier_cell_jungfraujoch { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2092 -severity "ERROR" "create_hier_cell_jungfraujoch() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p0

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p1

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p2

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p3

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p4

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p5

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p6

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p7

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p8

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p9

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p10

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p11

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p12

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p13

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p14

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p15

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p16

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p17

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p18

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p19

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p20

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p21

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p22

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p23

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p24

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p25

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p26

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p27

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 m_axis_c2h_data

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 m_axis_c2h_datamover_cmd

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 m_axis_h2c_datamover_cmd

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 s_axi

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 s_axis_h2c_data

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 udp_in


  # Create pins
  create_bd_pin -dir O -type intr Interrupt_0
  create_bd_pin -dir I -type rst ap_rst_n
  create_bd_pin -dir I -type clk axi_clk
  create_bd_pin -dir I -type rst axi_rst_n

  # Create instance: action_config_0, and set properties
  set block_name action_config
  set block_cell_name action_config_0
  if { [catch {set action_config_0 [create_bd_cell -type module -reference $block_name $block_cell_name] } errmsg] } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2095 -severity "ERROR" "Unable to add referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   } elseif { $action_config_0 eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2096 -severity "ERROR" "Unable to referenced block <$block_name>. Please add the files for ${block_name}'s definition into the project."}
     return 1
   }

  # Create instance: axi_bram_ctrl_calibration_addr_0, and set properties
  set axi_bram_ctrl_calibration_addr_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.1 axi_bram_ctrl_calibration_addr_0 ]
  set_property -dict [ list \
   CONFIG.PROTOCOL {AXI4LITE} \
   CONFIG.READ_LATENCY {4} \
   CONFIG.SINGLE_PORT_BRAM {1} \
   CONFIG.USE_ECC {0} \
 ] $axi_bram_ctrl_calibration_addr_0

  # Create instance: axi_bram_ctrl_calibration_addr_1, and set properties
  set axi_bram_ctrl_calibration_addr_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.1 axi_bram_ctrl_calibration_addr_1 ]
  set_property -dict [ list \
   CONFIG.DATA_WIDTH {32} \
   CONFIG.PROTOCOL {AXI4} \
   CONFIG.READ_LATENCY {4} \
   CONFIG.SINGLE_PORT_BRAM {1} \
 ] $axi_bram_ctrl_calibration_addr_1

  # Create instance: axi_bram_ctrl_pixel_mask_0, and set properties
  set axi_bram_ctrl_pixel_mask_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_bram_ctrl:4.1 axi_bram_ctrl_pixel_mask_0 ]
  set_property -dict [ list \
   CONFIG.READ_LATENCY {4} \
   CONFIG.SINGLE_PORT_BRAM {1} \
 ] $axi_bram_ctrl_pixel_mask_0

  # Create instance: axis_addr_fifo_0, and set properties
  set axis_addr_fifo_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_addr_fifo_0 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {64} \
   CONFIG.FIFO_MEMORY_TYPE {block} \
   CONFIG.HAS_AEMPTY {1} \
   CONFIG.HAS_AFULL {1} \
 ] $axis_addr_fifo_0

  # Create instance: axis_addr_fifo_1, and set properties
  set axis_addr_fifo_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_addr_fifo_1 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
   CONFIG.FIFO_MEMORY_TYPE {block} \
 ] $axis_addr_fifo_1

  # Create instance: axis_adu_histo_result_fifo, and set properties
  set axis_adu_histo_result_fifo [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_adu_histo_result_fifo ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {1024} \
 ] $axis_adu_histo_result_fifo

  # Create instance: axis_compl_fifo_0, and set properties
  set axis_compl_fifo_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_compl_fifo_0 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {1024} \
   CONFIG.HAS_AEMPTY {1} \
   CONFIG.HAS_AFULL {1} \
 ] $axis_compl_fifo_0

  # Create instance: axis_compl_fifo_1, and set properties
  set axis_compl_fifo_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_compl_fifo_1 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
 ] $axis_compl_fifo_1

  # Create instance: axis_data_fifo_0, and set properties
  set axis_data_fifo_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_0 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {32768} \
   CONFIG.FIFO_MEMORY_TYPE {ultra} \
 ] $axis_data_fifo_0

  # Create instance: axis_data_fifo_1, and set properties
  set axis_data_fifo_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_1 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {256} \
   CONFIG.FIFO_MEMORY_TYPE {block} \
   CONFIG.HAS_AEMPTY {1} \
   CONFIG.HAS_AFULL {1} \
 ] $axis_data_fifo_1

  # Create instance: axis_data_fifo_2, and set properties
  set axis_data_fifo_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_2 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {256} \
 ] $axis_data_fifo_2

  # Create instance: axis_data_fifo_3, and set properties
  set axis_data_fifo_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_3 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {256} \
 ] $axis_data_fifo_3

  # Create instance: axis_data_fifo_4, and set properties
  set axis_data_fifo_4 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_4 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {32768} \
   CONFIG.FIFO_MEMORY_TYPE {ultra} \
   CONFIG.HAS_AEMPTY {1} \
   CONFIG.HAS_AFULL {1} \
 ] $axis_data_fifo_4

  # Create instance: axis_data_fifo_c2h_cmd, and set properties
  set axis_data_fifo_c2h_cmd [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_c2h_cmd ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {32} \
   CONFIG.FIFO_MEMORY_TYPE {auto} \
   CONFIG.HAS_AEMPTY {1} \
   CONFIG.HAS_AFULL {1} \
 ] $axis_data_fifo_c2h_cmd

  # Create instance: axis_data_fifo_c2h_data, and set properties
  set axis_data_fifo_c2h_data [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_c2h_data ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {1024} \
   CONFIG.FIFO_MEMORY_TYPE {auto} \
   CONFIG.HAS_AEMPTY {1} \
   CONFIG.HAS_AFULL {1} \
 ] $axis_data_fifo_c2h_data

  # Create instance: axis_data_fifo_h2c_cmd, and set properties
  set axis_data_fifo_h2c_cmd [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_h2c_cmd ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {4096} \
   CONFIG.HAS_AEMPTY {1} \
   CONFIG.HAS_AFULL {1} \
 ] $axis_data_fifo_h2c_cmd

  # Create instance: axis_data_fifo_h2c_data, and set properties
  set axis_data_fifo_h2c_data [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_h2c_data ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {256} \
   CONFIG.FIFO_MEMORY_TYPE {block} \
   CONFIG.HAS_AEMPTY {1} \
   CONFIG.HAS_AFULL {1} \
   CONFIG.TDATA_NUM_BYTES {64} \
 ] $axis_data_fifo_h2c_data

  # Create instance: axis_data_fifo_udp_1, and set properties
  set axis_data_fifo_udp_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_udp_1 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {256} \
   CONFIG.FIFO_MEMORY_TYPE {auto} \
 ] $axis_data_fifo_udp_1

  # Create instance: axis_data_fifo_udp_metadata_0, and set properties
  set axis_data_fifo_udp_metadata_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_udp_metadata_0 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
 ] $axis_data_fifo_udp_metadata_0

  # Create instance: axis_interconnect_0, and set properties
  set axis_interconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_interconnect:2.1 axis_interconnect_0 ]
  set_property -dict [ list \
   CONFIG.ARB_ALGORITHM {3} \
   CONFIG.ARB_ON_MAX_XFERS {0} \
   CONFIG.ARB_ON_NUM_CYCLES {1} \
   CONFIG.ARB_ON_TLAST {1} \
   CONFIG.M00_HAS_REGSLICE {1} \
   CONFIG.NUM_MI {1} \
   CONFIG.NUM_SI {2} \
   CONFIG.S00_FIFO_DEPTH {1024} \
   CONFIG.S00_FIFO_MODE {1} \
   CONFIG.S00_HAS_REGSLICE {1} \
   CONFIG.S01_FIFO_DEPTH {1024} \
   CONFIG.S01_FIFO_MODE {1} \
   CONFIG.S01_HAS_REGSLICE {1} \
 ] $axis_interconnect_0

  # Create instance: axis_register_slice_addr_0, and set properties
  set axis_register_slice_addr_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice:1.1 axis_register_slice_addr_0 ]
  set_property -dict [ list \
   CONFIG.REG_CONFIG {16} \
 ] $axis_register_slice_addr_0

  # Create instance: axis_register_slice_data_0, and set properties
  set axis_register_slice_data_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice:1.1 axis_register_slice_data_0 ]
  set_property -dict [ list \
   CONFIG.REG_CONFIG {16} \
 ] $axis_register_slice_data_0

  # Create instance: axis_register_slice_data_in_0, and set properties
  set axis_register_slice_data_in_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice:1.1 axis_register_slice_data_in_0 ]
  set_property -dict [ list \
   CONFIG.REG_CONFIG {16} \
 ] $axis_register_slice_data_in_0

  # Create instance: axis_register_slice_host_mem, and set properties
  set axis_register_slice_host_mem [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice:1.1 axis_register_slice_host_mem ]
  set_property -dict [ list \
   CONFIG.REG_CONFIG {16} \
 ] $axis_register_slice_host_mem

  # Create instance: axis_register_slice_udp, and set properties
  set axis_register_slice_udp [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice:1.1 axis_register_slice_udp ]
  set_property -dict [ list \
   CONFIG.REG_CONFIG {16} \
 ] $axis_register_slice_udp

  # Create instance: axis_udp_addr_fifo_0, and set properties
  set axis_udp_addr_fifo_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_udp_addr_fifo_0 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {64} \
 ] $axis_udp_addr_fifo_0

  # Create instance: axis_udp_fifo_0, and set properties
  set axis_udp_fifo_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_udp_fifo_0 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {256} \
   CONFIG.FIFO_MEMORY_TYPE {block} \
   CONFIG.HAS_AEMPTY {1} \
   CONFIG.HAS_AFULL {1} \
 ] $axis_udp_fifo_0

  # Create instance: axis_udp_in_fifo, and set properties
  set axis_udp_in_fifo [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_udp_in_fifo ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {1024} \
   CONFIG.FIFO_MEMORY_TYPE {auto} \
   CONFIG.HAS_AEMPTY {1} \
   CONFIG.HAS_AFULL {1} \
 ] $axis_udp_in_fifo

  # Create instance: data_collection_fsm_0, and set properties
  set data_collection_fsm_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:data_collection_fsm:1.0 data_collection_fsm_0 ]

  # Create instance: dma_address_table, and set properties
  set dma_address_table [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 dma_address_table ]
  set_property -dict [ list \
   CONFIG.Assume_Synchronous_Clk {true} \
   CONFIG.EN_SAFETY_CKT {false} \
   CONFIG.Enable_B {Use_ENB_Pin} \
   CONFIG.Memory_Type {True_Dual_Port_RAM} \
   CONFIG.Operating_Mode_A {NO_CHANGE} \
   CONFIG.Operating_Mode_B {NO_CHANGE} \
   CONFIG.PRIM_type_to_Implement {URAM} \
   CONFIG.Port_A_Write_Rate {50} \
   CONFIG.Port_B_Clock {100} \
   CONFIG.Port_B_Enable_Rate {100} \
   CONFIG.Port_B_Write_Rate {50} \
   CONFIG.READ_LATENCY_A {4} \
   CONFIG.READ_LATENCY_B {4} \
   CONFIG.Use_Byte_Write_Enable {true} \
   CONFIG.Use_RSTB_Pin {true} \
 ] $dma_address_table

  # Create instance: frame_generator
  create_hier_cell_frame_generator $hier_obj frame_generator

  # Create instance: hbm_cache
  create_hier_cell_hbm_cache $hier_obj hbm_cache

  # Create instance: host_writer_0, and set properties
  set host_writer_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:host_writer:1.0 host_writer_0 ]

  # Create instance: image_processing
  create_hier_cell_image_processing $hier_obj image_processing

  # Create instance: load_calibration_0, and set properties
  set load_calibration_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:load_calibration:1.0 load_calibration_0 ]

  # Create instance: mailbox_0, and set properties
  set mailbox_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:mailbox:2.1 mailbox_0 ]
  set_property -dict [ list \
   CONFIG.C_IMPL_STYLE {1} \
   CONFIG.C_INTERCONNECT_PORT_1 {4} \
   CONFIG.C_MAILBOX_DEPTH {8192} \
 ] $mailbox_0

  # Create instance: one, and set properties
  set one [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 one ]

  # Create instance: pixel_mask_mem_0, and set properties
  set pixel_mask_mem_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:blk_mem_gen:8.4 pixel_mask_mem_0 ]
  set_property -dict [ list \
   CONFIG.Assume_Synchronous_Clk {true} \
   CONFIG.EN_SAFETY_CKT {false} \
   CONFIG.Enable_B {Always_Enabled} \
   CONFIG.Memory_Type {Single_Port_RAM} \
   CONFIG.Operating_Mode_A {WRITE_FIRST} \
   CONFIG.Operating_Mode_B {WRITE_FIRST} \
   CONFIG.PRIM_type_to_Implement {URAM} \
   CONFIG.Port_B_Clock {0} \
   CONFIG.Port_B_Enable_Rate {0} \
   CONFIG.Port_B_Write_Rate {0} \
   CONFIG.READ_LATENCY_A {4} \
   CONFIG.READ_LATENCY_B {4} \
   CONFIG.Use_RSTB_Pin {false} \
 ] $pixel_mask_mem_0

  # Create instance: sls_detector_0, and set properties
  set sls_detector_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:sls_detector:1.0 sls_detector_0 ]

  # Create instance: smartconnect_0, and set properties
  set smartconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:smartconnect:1.0 smartconnect_0 ]
  set_property -dict [ list \
   CONFIG.NUM_CLKS {1} \
   CONFIG.NUM_MI {5} \
   CONFIG.NUM_SI {1} \
 ] $smartconnect_0

  # Create instance: smartconnect_1, and set properties
  set smartconnect_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:smartconnect:1.0 smartconnect_1 ]

  # Create instance: smartconnect_2, and set properties
  set smartconnect_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:smartconnect:1.0 smartconnect_2 ]

  # Create instance: smartconnect_3, and set properties
  set smartconnect_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:smartconnect:1.0 smartconnect_3 ]
  set_property -dict [ list \
   CONFIG.NUM_SI {2} \
 ] $smartconnect_3

  # Create instance: smartconnect_4, and set properties
  set smartconnect_4 [ create_bd_cell -type ip -vlnv xilinx.com:ip:smartconnect:1.0 smartconnect_4 ]

  # Create instance: stream512to768_0, and set properties
  set stream512to768_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:stream512to768:1.0 stream512to768_0 ]

  # Create instance: timer_hbm, and set properties
  set timer_hbm [ create_bd_cell -type ip -vlnv psi.ch:hls:timer_host:1.0 timer_hbm ]

  # Create instance: timer_host, and set properties
  set timer_host [ create_bd_cell -type ip -vlnv psi.ch:hls:timer_host:1.0 timer_host ]

  # Create instance: udp_0, and set properties
  set udp_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:udp:1.0 udp_0 ]

  # Create interface connections
  connect_bd_intf_net -intf_net S_AXIS_1 [get_bd_intf_pins s_axis_h2c_data] [get_bd_intf_pins axis_data_fifo_h2c_data/S_AXIS]
  connect_bd_intf_net -intf_net axi_bram_ctrl_calibration_addr_0_BRAM_PORTA [get_bd_intf_pins axi_bram_ctrl_calibration_addr_0/BRAM_PORTA] [get_bd_intf_pins dma_address_table/BRAM_PORTA]
  connect_bd_intf_net -intf_net axi_bram_ctrl_calibration_addr_1_BRAM_PORTA [get_bd_intf_pins axi_bram_ctrl_calibration_addr_1/BRAM_PORTA] [get_bd_intf_pins dma_address_table/BRAM_PORTB]
  connect_bd_intf_net -intf_net axi_bram_ctrl_pixel_mask_0_BRAM_PORTA [get_bd_intf_pins axi_bram_ctrl_pixel_mask_0/BRAM_PORTA] [get_bd_intf_pins pixel_mask_mem_0/BRAM_PORTA]
  connect_bd_intf_net -intf_net axis_addr_fifo_0_M_AXIS [get_bd_intf_pins axis_addr_fifo_0/M_AXIS] [get_bd_intf_pins axis_register_slice_addr_0/S_AXIS]
  connect_bd_intf_net -intf_net axis_addr_fifo_1_M_AXIS [get_bd_intf_pins axis_addr_fifo_1/M_AXIS] [get_bd_intf_pins hbm_cache/addr_in]
  connect_bd_intf_net -intf_net axis_adu_histo_result_fifo_M_AXIS [get_bd_intf_pins axis_adu_histo_result_fifo/M_AXIS] [get_bd_intf_pins host_writer_0/adu_histo_in]
  connect_bd_intf_net -intf_net axis_compl_fifo_0_M_AXIS [get_bd_intf_pins axis_compl_fifo_0/M_AXIS] [get_bd_intf_pins image_processing/s_axis_completion]
  connect_bd_intf_net -intf_net axis_compl_fifo_1_M_AXIS [get_bd_intf_pins axis_compl_fifo_1/M_AXIS] [get_bd_intf_pins host_writer_0/s_axis_completion]
  connect_bd_intf_net -intf_net axis_data_fifo_0_M_AXIS [get_bd_intf_pins axis_data_fifo_0/M_AXIS] [get_bd_intf_pins timer_hbm/data_in]
  connect_bd_intf_net -intf_net axis_data_fifo_1_M_AXIS [get_bd_intf_pins axis_data_fifo_1/M_AXIS] [get_bd_intf_pins axis_register_slice_data_0/S_AXIS]
  connect_bd_intf_net -intf_net axis_data_fifo_2_M_AXIS [get_bd_intf_pins axis_data_fifo_2/M_AXIS] [get_bd_intf_pins hbm_cache/data_in]
  connect_bd_intf_net -intf_net axis_data_fifo_3_M_AXIS [get_bd_intf_pins axis_data_fifo_3/M_AXIS] [get_bd_intf_pins timer_host/data_in]
  connect_bd_intf_net -intf_net axis_data_fifo_4_M_AXIS [get_bd_intf_pins axis_data_fifo_4/M_AXIS] [get_bd_intf_pins host_writer_0/data_in]
  connect_bd_intf_net -intf_net axis_data_fifo_c2h_cmd_M_AXIS [get_bd_intf_pins m_axis_c2h_datamover_cmd] [get_bd_intf_pins axis_data_fifo_c2h_cmd/M_AXIS]
  connect_bd_intf_net -intf_net axis_data_fifo_c2h_data_M_AXIS [get_bd_intf_pins m_axis_c2h_data] [get_bd_intf_pins axis_data_fifo_c2h_data/M_AXIS]
  connect_bd_intf_net -intf_net axis_data_fifo_h2c_cmd_M_AXIS [get_bd_intf_pins m_axis_h2c_datamover_cmd] [get_bd_intf_pins axis_data_fifo_h2c_cmd/M_AXIS]
  connect_bd_intf_net -intf_net axis_data_fifo_h2c_data_M_AXIS [get_bd_intf_pins axis_data_fifo_h2c_data/M_AXIS] [get_bd_intf_pins axis_register_slice_data_in_0/S_AXIS]
  connect_bd_intf_net -intf_net axis_data_fifo_udp_1_M_AXIS [get_bd_intf_pins axis_data_fifo_udp_1/M_AXIS] [get_bd_intf_pins sls_detector_0/udp_payload_in]
  connect_bd_intf_net -intf_net axis_data_fifo_udp_metadata_0_M_AXIS [get_bd_intf_pins axis_data_fifo_udp_metadata_0/M_AXIS] [get_bd_intf_pins sls_detector_0/udp_metadata_in]
  connect_bd_intf_net -intf_net axis_eth_in_fifo_M_AXIS [get_bd_intf_pins axis_udp_in_fifo/M_AXIS] [get_bd_intf_pins udp_0/eth_in]
  connect_bd_intf_net -intf_net axis_interconnect_0_M00_AXIS [get_bd_intf_pins axis_interconnect_0/M00_AXIS] [get_bd_intf_pins axis_udp_in_fifo/S_AXIS]
  connect_bd_intf_net -intf_net axis_register_slice_addr_0_M_AXIS [get_bd_intf_pins axis_register_slice_addr_0/M_AXIS] [get_bd_intf_pins stream512to768_0/addr_in]
  connect_bd_intf_net -intf_net axis_register_slice_data_0_M_AXIS [get_bd_intf_pins axis_register_slice_data_0/M_AXIS] [get_bd_intf_pins stream512to768_0/data_in]
  connect_bd_intf_net -intf_net axis_register_slice_data_in_0_M_AXIS1 [get_bd_intf_pins axis_register_slice_data_in_0/M_AXIS] [get_bd_intf_pins load_calibration_0/host_memory_in]
  connect_bd_intf_net -intf_net axis_register_slice_host_mem_M_AXIS [get_bd_intf_pins axis_data_fifo_c2h_data/S_AXIS] [get_bd_intf_pins axis_register_slice_host_mem/M_AXIS]
  connect_bd_intf_net -intf_net axis_register_slice_udp_M_AXIS [get_bd_intf_pins axis_register_slice_udp/M_AXIS] [get_bd_intf_pins data_collection_fsm_0/eth_in]
  connect_bd_intf_net -intf_net axis_udp_addr_fifo_0_M_AXIS [get_bd_intf_pins axis_udp_addr_fifo_0/M_AXIS] [get_bd_intf_pins data_collection_fsm_0/addr_in]
  connect_bd_intf_net -intf_net axis_udp_fifo_0_M_AXIS [get_bd_intf_pins axis_register_slice_udp/S_AXIS] [get_bd_intf_pins axis_udp_fifo_0/M_AXIS]
  connect_bd_intf_net -intf_net data_collection_fsm_0_addr_out [get_bd_intf_pins axis_addr_fifo_0/S_AXIS] [get_bd_intf_pins data_collection_fsm_0/addr_out]
  connect_bd_intf_net -intf_net data_collection_fsm_0_data_out [get_bd_intf_pins axis_data_fifo_0/S_AXIS] [get_bd_intf_pins data_collection_fsm_0/data_out]
  connect_bd_intf_net -intf_net data_in_1 [get_bd_intf_pins hbm_cache/data_out] [get_bd_intf_pins image_processing/data_in]
  connect_bd_intf_net -intf_net frame_generator_M_AXIS [get_bd_intf_pins axis_interconnect_0/S01_AXIS] [get_bd_intf_pins frame_generator/M_AXIS]
  connect_bd_intf_net -intf_net frame_generator_m_axi_d_hbm_p20 [get_bd_intf_pins m_axi_d_hbm_p20] [get_bd_intf_pins frame_generator/m_axi_d_hbm_p0]
  connect_bd_intf_net -intf_net frame_generator_m_axi_d_hbm_p21 [get_bd_intf_pins m_axi_d_hbm_p21] [get_bd_intf_pins frame_generator/m_axi_d_hbm_p1]
  connect_bd_intf_net -intf_net hbm_cache_m_axi_d_hbm_p22 [get_bd_intf_pins m_axi_d_hbm_p22] [get_bd_intf_pins hbm_cache/m_axi_d_hbm_p0]
  connect_bd_intf_net -intf_net hbm_cache_m_axi_d_hbm_p23 [get_bd_intf_pins m_axi_d_hbm_p23] [get_bd_intf_pins hbm_cache/m_axi_d_hbm_p1]
  connect_bd_intf_net -intf_net hbm_cache_m_axi_d_hbm_p24 [get_bd_intf_pins m_axi_d_hbm_p24] [get_bd_intf_pins hbm_cache/m_axi_d_hbm_p2]
  connect_bd_intf_net -intf_net hbm_cache_m_axi_d_hbm_p25 [get_bd_intf_pins m_axi_d_hbm_p25] [get_bd_intf_pins hbm_cache/m_axi_d_hbm_p3]
  connect_bd_intf_net -intf_net hbm_cache_m_axi_d_hbm_p26 [get_bd_intf_pins m_axi_d_hbm_p26] [get_bd_intf_pins hbm_cache/m_axi_d_hbm_p4]
  connect_bd_intf_net -intf_net hbm_cache_m_axi_d_hbm_p27 [get_bd_intf_pins m_axi_d_hbm_p27] [get_bd_intf_pins hbm_cache/m_axi_d_hbm_p5]
  connect_bd_intf_net -intf_net hbm_cache_m_axis_completion [get_bd_intf_pins axis_compl_fifo_0/S_AXIS] [get_bd_intf_pins hbm_cache/m_axis_completion]
  connect_bd_intf_net -intf_net host_writer_0_datamover_out_cmd [get_bd_intf_pins axis_data_fifo_c2h_cmd/S_AXIS] [get_bd_intf_pins host_writer_0/datamover_out_cmd]
  connect_bd_intf_net -intf_net host_writer_0_host_memory_out [get_bd_intf_pins axis_register_slice_host_mem/S_AXIS] [get_bd_intf_pins host_writer_0/host_memory_out]
  connect_bd_intf_net -intf_net host_writer_0_m_axi_dma_address_table [get_bd_intf_pins host_writer_0/m_axi_dma_address_table] [get_bd_intf_pins smartconnect_3/S01_AXI]
  connect_bd_intf_net -intf_net host_writer_0_m_axis_completion [get_bd_intf_pins host_writer_0/m_axis_completion] [get_bd_intf_pins mailbox_0/S1_AXIS]
  connect_bd_intf_net -intf_net image_processing_M_AXIS [get_bd_intf_pins host_writer_0/spot_finder_in] [get_bd_intf_pins image_processing/spot_finder_out]
  connect_bd_intf_net -intf_net image_processing_M_AXIS1 [get_bd_intf_pins host_writer_0/integration_in] [get_bd_intf_pins image_processing/integration_result_out]
  connect_bd_intf_net -intf_net image_processing_data_out6 [get_bd_intf_pins axis_data_fifo_3/S_AXIS] [get_bd_intf_pins image_processing/data_out]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p0 [get_bd_intf_pins image_processing/m_axi_d_hbm_p0] [get_bd_intf_pins smartconnect_1/S00_AXI]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p1 [get_bd_intf_pins m_axi_d_hbm_p1] [get_bd_intf_pins image_processing/m_axi_d_hbm_p1]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p2 [get_bd_intf_pins image_processing/m_axi_d_hbm_p2] [get_bd_intf_pins smartconnect_2/S00_AXI]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p3 [get_bd_intf_pins m_axi_d_hbm_p3] [get_bd_intf_pins image_processing/m_axi_d_hbm_p3]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p4 [get_bd_intf_pins m_axi_d_hbm_p4] [get_bd_intf_pins image_processing/m_axi_d_hbm_p4]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p5 [get_bd_intf_pins m_axi_d_hbm_p5] [get_bd_intf_pins image_processing/m_axi_d_hbm_p5]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p6 [get_bd_intf_pins m_axi_d_hbm_p6] [get_bd_intf_pins image_processing/m_axi_d_hbm_p6]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p7 [get_bd_intf_pins m_axi_d_hbm_p7] [get_bd_intf_pins image_processing/m_axi_d_hbm_p7]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p8 [get_bd_intf_pins m_axi_d_hbm_p8] [get_bd_intf_pins image_processing/m_axi_d_hbm_p8]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p9 [get_bd_intf_pins m_axi_d_hbm_p9] [get_bd_intf_pins image_processing/m_axi_d_hbm_p9]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p10 [get_bd_intf_pins m_axi_d_hbm_p10] [get_bd_intf_pins image_processing/m_axi_d_hbm_p10]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p11 [get_bd_intf_pins m_axi_d_hbm_p11] [get_bd_intf_pins image_processing/m_axi_d_hbm_p11]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p12 [get_bd_intf_pins m_axi_d_hbm_p12] [get_bd_intf_pins image_processing/m_axi_d_hbm_p12]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p13 [get_bd_intf_pins m_axi_d_hbm_p13] [get_bd_intf_pins image_processing/m_axi_d_hbm_p13]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p14 [get_bd_intf_pins m_axi_d_hbm_p14] [get_bd_intf_pins image_processing/m_axi_d_hbm_p14]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p15 [get_bd_intf_pins m_axi_d_hbm_p15] [get_bd_intf_pins image_processing/m_axi_d_hbm_p15]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p16 [get_bd_intf_pins m_axi_d_hbm_p16] [get_bd_intf_pins image_processing/m_axi_d_hbm_p16]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p17 [get_bd_intf_pins m_axi_d_hbm_p17] [get_bd_intf_pins image_processing/m_axi_d_hbm_p17]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p18 [get_bd_intf_pins m_axi_d_hbm_p18] [get_bd_intf_pins image_processing/m_axi_d_hbm_p18]
  connect_bd_intf_net -intf_net image_processing_m_axi_d_hbm_p19 [get_bd_intf_pins m_axi_d_hbm_p19] [get_bd_intf_pins image_processing/m_axi_d_hbm_p19]
  connect_bd_intf_net -intf_net image_processing_m_axi_pixel_mask [get_bd_intf_pins image_processing/m_axi_pixel_mask] [get_bd_intf_pins smartconnect_4/S01_AXI]
  connect_bd_intf_net -intf_net image_processing_m_axis_completion3 [get_bd_intf_pins axis_compl_fifo_1/S_AXIS] [get_bd_intf_pins image_processing/m_axis_completion]
  connect_bd_intf_net -intf_net image_processing_result_out [get_bd_intf_pins axis_adu_histo_result_fifo/S_AXIS] [get_bd_intf_pins image_processing/result_out]
  connect_bd_intf_net -intf_net image_processing_roi_calc_out [get_bd_intf_pins host_writer_0/roi_count_in] [get_bd_intf_pins image_processing/roi_calc_out]
  connect_bd_intf_net -intf_net image_processing_pixel_calc_out [get_bd_intf_pins host_writer_0/pixel_calc_in] [get_bd_intf_pins image_processing/pixel_calc_out]
  connect_bd_intf_net -intf_net load_calibration_0_datamover_in_cmd [get_bd_intf_pins axis_data_fifo_h2c_cmd/S_AXIS] [get_bd_intf_pins load_calibration_0/datamover_in_cmd]
  connect_bd_intf_net -intf_net load_calibration_0_m_axi_d_hbm_p0 [get_bd_intf_pins load_calibration_0/m_axi_d_hbm_p0] [get_bd_intf_pins smartconnect_1/S01_AXI]
  connect_bd_intf_net -intf_net load_calibration_0_m_axi_d_hbm_p1 [get_bd_intf_pins load_calibration_0/m_axi_d_hbm_p1] [get_bd_intf_pins smartconnect_2/S01_AXI]
  connect_bd_intf_net -intf_net load_calibration_0_m_axi_dma_address_table [get_bd_intf_pins load_calibration_0/m_axi_dma_address_table] [get_bd_intf_pins smartconnect_3/S00_AXI]
  connect_bd_intf_net -intf_net load_calibration_0_m_axi_pixel_mask [get_bd_intf_pins load_calibration_0/m_axi_pixel_mask] [get_bd_intf_pins smartconnect_4/S00_AXI]
  connect_bd_intf_net -intf_net mailbox_0_M1_AXIS [get_bd_intf_pins host_writer_0/s_axis_work_request] [get_bd_intf_pins mailbox_0/M1_AXIS]
  connect_bd_intf_net -intf_net s_axi_1 [get_bd_intf_pins s_axi] [get_bd_intf_pins smartconnect_0/S00_AXI]
  connect_bd_intf_net -intf_net sls_detector_0_addr_out [get_bd_intf_pins axis_udp_addr_fifo_0/S_AXIS] [get_bd_intf_pins sls_detector_0/addr_out]
  connect_bd_intf_net -intf_net sls_detector_0_data_out [get_bd_intf_pins axis_udp_fifo_0/S_AXIS] [get_bd_intf_pins sls_detector_0/data_out]
  connect_bd_intf_net -intf_net smartconnect_0_M00_AXI [get_bd_intf_pins action_config_0/s_axi] [get_bd_intf_pins smartconnect_0/M00_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M01_AXI [get_bd_intf_pins mailbox_0/S0_AXI] [get_bd_intf_pins smartconnect_0/M01_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M02_AXI [get_bd_intf_pins load_calibration_0/s_axi_control] [get_bd_intf_pins smartconnect_0/M02_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M03_AXI [get_bd_intf_pins frame_generator/s_axi_control] [get_bd_intf_pins smartconnect_0/M03_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M04_AXI [get_bd_intf_pins axi_bram_ctrl_calibration_addr_0/S_AXI] [get_bd_intf_pins smartconnect_0/M04_AXI]
  connect_bd_intf_net -intf_net smartconnect_1_M00_AXI [get_bd_intf_pins m_axi_d_hbm_p0] [get_bd_intf_pins smartconnect_1/M00_AXI]
  connect_bd_intf_net -intf_net smartconnect_2_M00_AXI [get_bd_intf_pins m_axi_d_hbm_p2] [get_bd_intf_pins smartconnect_2/M00_AXI]
  connect_bd_intf_net -intf_net smartconnect_3_M00_AXI [get_bd_intf_pins axi_bram_ctrl_calibration_addr_1/S_AXI] [get_bd_intf_pins smartconnect_3/M00_AXI]
  connect_bd_intf_net -intf_net smartconnect_4_M00_AXI [get_bd_intf_pins axi_bram_ctrl_pixel_mask_0/S_AXI] [get_bd_intf_pins smartconnect_4/M00_AXI]
  connect_bd_intf_net -intf_net stream512to768_0_data_out [get_bd_intf_pins axis_data_fifo_2/S_AXIS] [get_bd_intf_pins stream512to768_0/data_out]
  connect_bd_intf_net -intf_net stream512to768_addr_out_0 [get_bd_intf_pins axis_addr_fifo_1/S_AXIS] [get_bd_intf_pins stream512to768_0/addr_out]
  connect_bd_intf_net -intf_net timer_hbm_data_out [get_bd_intf_pins axis_data_fifo_1/S_AXIS] [get_bd_intf_pins timer_hbm/data_out]
  connect_bd_intf_net -intf_net timer_host_data_out [get_bd_intf_pins axis_data_fifo_4/S_AXIS] [get_bd_intf_pins timer_host/data_out]
  connect_bd_intf_net -intf_net udp_0_udp_metadata_out [get_bd_intf_pins axis_data_fifo_udp_metadata_0/S_AXIS] [get_bd_intf_pins udp_0/udp_metadata_out]
  connect_bd_intf_net -intf_net udp_0_udp_payload_out [get_bd_intf_pins axis_data_fifo_udp_1/S_AXIS] [get_bd_intf_pins udp_0/udp_payload_out]
  connect_bd_intf_net -intf_net udp_in_1 [get_bd_intf_pins udp_in] [get_bd_intf_pins axis_interconnect_0/S00_AXIS]

  # Create port connections
  connect_bd_net -net action_config_0_clear_counters [get_bd_pins action_config_0/clear_counters] [get_bd_pins sls_detector_0/in_clear_counters] [get_bd_pins udp_0/in_clear_counters]
  connect_bd_net -net action_config_0_data_collection_cancel [get_bd_pins action_config_0/data_collection_cancel] [get_bd_pins data_collection_fsm_0/in_cancel] [get_bd_pins frame_generator/in_cancel] [get_bd_pins host_writer_0/in_cancel]
  connect_bd_net -net action_config_0_data_collection_fsm_start [get_bd_pins action_config_0/data_collection_start] [get_bd_pins data_collection_fsm_0/in_run]
  connect_bd_net -net action_config_0_data_collection_mode [get_bd_pins action_config_0/data_collection_mode] [get_bd_pins data_collection_fsm_0/mode]
  connect_bd_net -net action_config_0_energy_kev [get_bd_pins action_config_0/energy_kev] [get_bd_pins data_collection_fsm_0/energy_kev]
  connect_bd_net -net action_config_0_fpga_ipv4_addr [get_bd_pins action_config_0/fpga_ipv4_addr] [get_bd_pins frame_generator/src_ipv4_addr]
  connect_bd_net -net action_config_0_fpga_mac_addr [get_bd_pins action_config_0/fpga_mac_addr] [get_bd_pins frame_generator/src_mac_addr]
  connect_bd_net -net action_config_0_frames_per_trigger [get_bd_pins action_config_0/nframes] [get_bd_pins data_collection_fsm_0/nframes]
  connect_bd_net -net action_config_0_hbm_size_bytes [get_bd_pins action_config_0/hbm_size_bytes] [get_bd_pins frame_generator/hbm_size_bytes] [get_bd_pins hbm_cache/hbm_size_bytes] [get_bd_pins image_processing/hbm_size_bytes] [get_bd_pins load_calibration_0/hbm_size_bytes]
  connect_bd_net -net action_config_0_nmodules [get_bd_pins action_config_0/nmodules] [get_bd_pins data_collection_fsm_0/nmodules]
  connect_bd_net -net action_config_0_nstorage_cells [get_bd_pins action_config_0/nstorage_cells] [get_bd_pins data_collection_fsm_0/nstorage_cells]
  connect_bd_net -net action_config_0_nsummation [get_bd_pins action_config_0/nsummation] [get_bd_pins data_collection_fsm_0/nsummation]
  connect_bd_net -net action_config_0_pxlthreshold [get_bd_pins action_config_0/pxlthreshold] [get_bd_pins data_collection_fsm_0/pxlthreshold]
  connect_bd_net -net action_config_0_spot_finder_count_threshold [get_bd_pins action_config_0/spot_finder_count_threshold] [get_bd_pins image_processing/in_count_threshold]
  connect_bd_net -net action_config_0_spot_finder_d_max [get_bd_pins action_config_0/spot_finder_d_max] [get_bd_pins image_processing/in_max_d_value]
  connect_bd_net -net action_config_0_spot_finder_d_min [get_bd_pins action_config_0/spot_finder_d_min] [get_bd_pins image_processing/in_min_d_value]
  connect_bd_net -net action_config_0_spot_finder_snr_threshold [get_bd_pins action_config_0/spot_finder_snr_threshold] [get_bd_pins image_processing/in_snr_threshold]
  connect_bd_net -net action_config_0_sqrtmult [get_bd_pins action_config_0/sqrtmult] [get_bd_pins data_collection_fsm_0/sqrtmult]
  connect_bd_net -net ap_clk_1 [get_bd_pins axi_clk] [get_bd_pins action_config_0/clk] [get_bd_pins axi_bram_ctrl_calibration_addr_0/s_axi_aclk] [get_bd_pins axi_bram_ctrl_calibration_addr_1/s_axi_aclk] [get_bd_pins axi_bram_ctrl_pixel_mask_0/s_axi_aclk] [get_bd_pins axis_addr_fifo_0/s_axis_aclk] [get_bd_pins axis_addr_fifo_1/s_axis_aclk] [get_bd_pins axis_adu_histo_result_fifo/s_axis_aclk] [get_bd_pins axis_compl_fifo_0/s_axis_aclk] [get_bd_pins axis_compl_fifo_1/s_axis_aclk] [get_bd_pins axis_data_fifo_0/s_axis_aclk] [get_bd_pins axis_data_fifo_1/s_axis_aclk] [get_bd_pins axis_data_fifo_2/s_axis_aclk] [get_bd_pins axis_data_fifo_3/s_axis_aclk] [get_bd_pins axis_data_fifo_4/s_axis_aclk] [get_bd_pins axis_data_fifo_c2h_cmd/s_axis_aclk] [get_bd_pins axis_data_fifo_c2h_data/s_axis_aclk] [get_bd_pins axis_data_fifo_h2c_cmd/s_axis_aclk] [get_bd_pins axis_data_fifo_h2c_data/s_axis_aclk] [get_bd_pins axis_data_fifo_udp_1/s_axis_aclk] [get_bd_pins axis_data_fifo_udp_metadata_0/s_axis_aclk] [get_bd_pins axis_interconnect_0/ACLK] [get_bd_pins axis_interconnect_0/M00_AXIS_ACLK] [get_bd_pins axis_interconnect_0/S00_AXIS_ACLK] [get_bd_pins axis_interconnect_0/S01_AXIS_ACLK] [get_bd_pins axis_register_slice_addr_0/aclk] [get_bd_pins axis_register_slice_data_0/aclk] [get_bd_pins axis_register_slice_data_in_0/aclk] [get_bd_pins axis_register_slice_host_mem/aclk] [get_bd_pins axis_register_slice_udp/aclk] [get_bd_pins axis_udp_addr_fifo_0/s_axis_aclk] [get_bd_pins axis_udp_fifo_0/s_axis_aclk] [get_bd_pins axis_udp_in_fifo/s_axis_aclk] [get_bd_pins data_collection_fsm_0/ap_clk] [get_bd_pins frame_generator/axi_clk] [get_bd_pins hbm_cache/axi_clk] [get_bd_pins host_writer_0/ap_clk] [get_bd_pins image_processing/axi_clk] [get_bd_pins load_calibration_0/ap_clk] [get_bd_pins mailbox_0/M1_AXIS_ACLK] [get_bd_pins mailbox_0/S0_AXI_ACLK] [get_bd_pins mailbox_0/S1_AXIS_ACLK] [get_bd_pins sls_detector_0/ap_clk] [get_bd_pins smartconnect_0/aclk] [get_bd_pins smartconnect_1/aclk] [get_bd_pins smartconnect_2/aclk] [get_bd_pins smartconnect_3/aclk] [get_bd_pins smartconnect_4/aclk] [get_bd_pins stream512to768_0/ap_clk] [get_bd_pins timer_hbm/ap_clk] [get_bd_pins timer_host/ap_clk] [get_bd_pins udp_0/ap_clk]
  connect_bd_net -net axis_addr_fifo_0_almost_empty [get_bd_pins action_config_0/calib_addr_fifo_empty] [get_bd_pins axis_addr_fifo_0/almost_empty]
  connect_bd_net -net axis_addr_fifo_0_almost_full [get_bd_pins action_config_0/calib_addr_fifo_full] [get_bd_pins axis_addr_fifo_0/almost_full]
  connect_bd_net -net axis_compl_fifo_1_almost_empty [get_bd_pins action_config_0/last_addr_fifo_empty] [get_bd_pins axis_compl_fifo_0/almost_empty]
  connect_bd_net -net axis_compl_fifo_1_almost_full [get_bd_pins action_config_0/last_addr_fifo_full] [get_bd_pins axis_compl_fifo_0/almost_full]
  connect_bd_net -net axis_data_fifo_10_almost_empty [get_bd_pins action_config_0/last_data_fifo_empty] [get_bd_pins axis_data_fifo_4/almost_empty]
  connect_bd_net -net axis_data_fifo_10_almost_full [get_bd_pins action_config_0/last_data_fifo_full] [get_bd_pins axis_data_fifo_4/almost_full]
  connect_bd_net -net axis_data_fifo_1_almost_empty [get_bd_pins action_config_0/calib_data_fifo_empty] [get_bd_pins axis_data_fifo_1/almost_empty]
  connect_bd_net -net axis_data_fifo_1_almost_full [get_bd_pins action_config_0/calib_data_fifo_full] [get_bd_pins axis_data_fifo_1/almost_full]
  connect_bd_net -net axis_data_fifo_c2h_cmd_almost_empty [get_bd_pins action_config_0/c2h_cmd_fifo_empty] [get_bd_pins axis_data_fifo_c2h_cmd/almost_empty]
  connect_bd_net -net axis_data_fifo_c2h_cmd_almost_full [get_bd_pins action_config_0/c2h_cmd_fifo_full] [get_bd_pins axis_data_fifo_c2h_cmd/almost_full]
  connect_bd_net -net axis_data_fifo_c2h_data_almost_empty [get_bd_pins action_config_0/c2h_data_fifo_empty] [get_bd_pins axis_data_fifo_c2h_data/almost_empty]
  connect_bd_net -net axis_data_fifo_c2h_data_almost_full [get_bd_pins action_config_0/c2h_data_fifo_full] [get_bd_pins axis_data_fifo_c2h_data/almost_full]
  connect_bd_net -net axis_data_fifo_h2c_cmd_almost_empty [get_bd_pins action_config_0/h2c_cmd_fifo_empty] [get_bd_pins axis_data_fifo_h2c_cmd/almost_empty]
  connect_bd_net -net axis_data_fifo_h2c_cmd_almost_full [get_bd_pins action_config_0/h2c_cmd_fifo_full] [get_bd_pins axis_data_fifo_h2c_cmd/almost_full]
  connect_bd_net -net axis_data_fifo_h2c_data_almost_empty [get_bd_pins action_config_0/h2c_data_fifo_empty] [get_bd_pins axis_data_fifo_h2c_data/almost_empty]
  connect_bd_net -net axis_data_fifo_h2c_data_almost_full [get_bd_pins action_config_0/h2c_data_fifo_full] [get_bd_pins axis_data_fifo_h2c_data/almost_full]
  connect_bd_net -net axis_eth_in_fifo_almost_empty [get_bd_pins action_config_0/eth_in_fifo_empty] [get_bd_pins axis_udp_in_fifo/almost_empty]
  connect_bd_net -net axis_eth_in_fifo_almost_full [get_bd_pins action_config_0/eth_in_fifo_full] [get_bd_pins axis_udp_in_fifo/almost_full]
  connect_bd_net -net axis_udp_fifo_0_almost_empty [get_bd_pins action_config_0/udp_fifo_empty] [get_bd_pins axis_udp_fifo_0/almost_empty]
  connect_bd_net -net axis_udp_fifo_0_almost_full [get_bd_pins action_config_0/udp_fifo_full] [get_bd_pins axis_udp_fifo_0/almost_full]
  connect_bd_net -net data_collection_fsm_0_out_idle_V [get_bd_pins action_config_0/data_collection_idle] [get_bd_pins data_collection_fsm_0/out_idle]
  connect_bd_net -net frame_generator_almost_empty [get_bd_pins action_config_0/frame_generator_fifo_empty] [get_bd_pins frame_generator/almost_empty]
  connect_bd_net -net frame_generator_almost_full [get_bd_pins action_config_0/frame_generator_fifo_full] [get_bd_pins frame_generator/almost_full]
  connect_bd_net -net frame_summation_0_idle [get_bd_pins action_config_0/frame_summation_idle] [get_bd_pins image_processing/frame_summation_idle]
  connect_bd_net -net hbm_cache_almost_empty [get_bd_pins action_config_0/hbm_handles_fifo_empty] [get_bd_pins hbm_cache/hbm_handle_fifo_empty]
  connect_bd_net -net hbm_cache_almost_empty1 [get_bd_pins action_config_0/hbm_compl_fifo_empty] [get_bd_pins hbm_cache/compl_fifo_empty]
  connect_bd_net -net hbm_cache_almost_full [get_bd_pins action_config_0/hbm_handles_fifo_full] [get_bd_pins hbm_cache/hbm_handle_fifo_full]
  connect_bd_net -net hbm_cache_almost_full1 [get_bd_pins action_config_0/hbm_compl_fifo_full] [get_bd_pins hbm_cache/compl_fifo_full]
  connect_bd_net -net host_writer_0_idle [get_bd_pins action_config_0/host_writer_idle] [get_bd_pins host_writer_0/idle]
  connect_bd_net -net host_writer_0_packets_processed [get_bd_pins action_config_0/packets_processed] [get_bd_pins host_writer_0/packets_processed]
  connect_bd_net -net host_writer_0_packets_processed_ap_vld [get_bd_pins action_config_0/packets_processed_valid] [get_bd_pins host_writer_0/packets_processed_ap_vld]
  connect_bd_net -net host_writer_0_state [get_bd_pins action_config_0/host_writer_state] [get_bd_pins host_writer_0/state]
  connect_bd_net -net image_processing_almost_empty [get_bd_pins action_config_0/proc_fifo_empty] [get_bd_pins image_processing/proc_fifo_empty]
  connect_bd_net -net image_processing_almost_full [get_bd_pins action_config_0/proc_fifo_full] [get_bd_pins image_processing/proc_fifo_full]
  connect_bd_net -net in_min_pix_per_spot_1 [get_bd_pins action_config_0/spot_finder_min_pix_per_spot] [get_bd_pins image_processing/in_min_pix_per_spot]
  connect_bd_net -net integration_0_idle [get_bd_pins action_config_0/integration_idle] [get_bd_pins image_processing/integration_idle]
  connect_bd_net -net load_from_hbm_0_idle [get_bd_pins action_config_0/load_from_hbm_idle] [get_bd_pins hbm_cache/load_from_hbm_idle]
  connect_bd_net -net mailbox_0_Interrupt_0 [get_bd_pins Interrupt_0] [get_bd_pins action_config_0/mailbox_interrupt_0] [get_bd_pins mailbox_0/Interrupt_0]
  connect_bd_net -net one_dout [get_bd_pins one/dout] [get_bd_pins stream512to768_0/ap_start]
  connect_bd_net -net reset_axi [get_bd_pins axi_rst_n] [get_bd_pins action_config_0/resetn] [get_bd_pins axis_addr_fifo_0/s_axis_aresetn] [get_bd_pins axis_addr_fifo_1/s_axis_aresetn] [get_bd_pins axis_adu_histo_result_fifo/s_axis_aresetn] [get_bd_pins axis_compl_fifo_0/s_axis_aresetn] [get_bd_pins axis_compl_fifo_1/s_axis_aresetn] [get_bd_pins axis_data_fifo_0/s_axis_aresetn] [get_bd_pins axis_data_fifo_1/s_axis_aresetn] [get_bd_pins axis_data_fifo_2/s_axis_aresetn] [get_bd_pins axis_data_fifo_3/s_axis_aresetn] [get_bd_pins axis_data_fifo_4/s_axis_aresetn] [get_bd_pins axis_data_fifo_c2h_cmd/s_axis_aresetn] [get_bd_pins axis_data_fifo_c2h_data/s_axis_aresetn] [get_bd_pins axis_data_fifo_h2c_cmd/s_axis_aresetn] [get_bd_pins axis_data_fifo_h2c_data/s_axis_aresetn] [get_bd_pins axis_data_fifo_udp_1/s_axis_aresetn] [get_bd_pins axis_data_fifo_udp_metadata_0/s_axis_aresetn] [get_bd_pins axis_interconnect_0/ARESETN] [get_bd_pins axis_interconnect_0/M00_AXIS_ARESETN] [get_bd_pins axis_interconnect_0/S00_AXIS_ARESETN] [get_bd_pins axis_interconnect_0/S01_AXIS_ARESETN] [get_bd_pins axis_register_slice_addr_0/aresetn] [get_bd_pins axis_register_slice_data_0/aresetn] [get_bd_pins axis_register_slice_data_in_0/aresetn] [get_bd_pins axis_register_slice_host_mem/aresetn] [get_bd_pins axis_register_slice_udp/aresetn] [get_bd_pins axis_udp_addr_fifo_0/s_axis_aresetn] [get_bd_pins axis_udp_fifo_0/s_axis_aresetn] [get_bd_pins axis_udp_in_fifo/s_axis_aresetn] [get_bd_pins frame_generator/axi_rst_n] [get_bd_pins hbm_cache/axi_rst_n] [get_bd_pins image_processing/axi_rst_n] [get_bd_pins smartconnect_0/aresetn] [get_bd_pins smartconnect_1/aresetn] [get_bd_pins smartconnect_2/aresetn] [get_bd_pins smartconnect_3/aresetn] [get_bd_pins smartconnect_4/aresetn]
  connect_bd_net -net reset_hls [get_bd_pins ap_rst_n] [get_bd_pins axi_bram_ctrl_calibration_addr_0/s_axi_aresetn] [get_bd_pins axi_bram_ctrl_calibration_addr_1/s_axi_aresetn] [get_bd_pins axi_bram_ctrl_pixel_mask_0/s_axi_aresetn] [get_bd_pins data_collection_fsm_0/ap_rst_n] [get_bd_pins frame_generator/ap_rst_n] [get_bd_pins hbm_cache/ap_rst_n] [get_bd_pins host_writer_0/ap_rst_n] [get_bd_pins image_processing/ap_rst_n] [get_bd_pins load_calibration_0/ap_rst_n] [get_bd_pins mailbox_0/S0_AXI_ARESETN] [get_bd_pins sls_detector_0/ap_rst_n] [get_bd_pins stream512to768_0/ap_rst_n] [get_bd_pins timer_hbm/ap_rst_n] [get_bd_pins timer_host/ap_rst_n] [get_bd_pins udp_0/ap_rst_n]
  connect_bd_net -net save_to_hbm_0_idle [get_bd_pins action_config_0/save_to_hbm_idle] [get_bd_pins hbm_cache/save_to_hbm_idle]
  connect_bd_net -net sls_detector_0_counter [get_bd_pins action_config_0/packets_sls] [get_bd_pins sls_detector_0/counter]
  connect_bd_net -net sls_detector_0_counter_ap_vld [get_bd_pins action_config_0/packets_sls_valid] [get_bd_pins sls_detector_0/counter_ap_vld]
  connect_bd_net -net sls_detector_0_counter_eth_error [get_bd_pins action_config_0/udp_err_eth] [get_bd_pins sls_detector_0/counter_eth_error]
  connect_bd_net -net sls_detector_0_counter_eth_error_ap_vld [get_bd_pins action_config_0/udp_err_eth_valid] [get_bd_pins sls_detector_0/counter_eth_error_ap_vld]
  connect_bd_net -net sls_detector_0_counter_len_error [get_bd_pins action_config_0/udp_err_len] [get_bd_pins sls_detector_0/counter_len_error]
  connect_bd_net -net sls_detector_0_counter_len_error_ap_vld [get_bd_pins action_config_0/udp_err_len_valid] [get_bd_pins sls_detector_0/counter_len_error_ap_vld]
  connect_bd_net -net sls_detector_0_idle [get_bd_pins action_config_0/sls_detector_idle] [get_bd_pins sls_detector_0/idle]
  connect_bd_net -net sls_detector_0_pulse_id [get_bd_pins action_config_0/pulseid] [get_bd_pins sls_detector_0/pulse_id]
  connect_bd_net -net sls_detector_0_pulse_id_ap_vld [get_bd_pins action_config_0/pulseid_valid] [get_bd_pins sls_detector_0/pulse_id_ap_vld]
  connect_bd_net -net stream512to768_0_idle [get_bd_pins action_config_0/stream512to768_idle] [get_bd_pins stream512to768_0/idle]
  connect_bd_net -net stream768to512_0_idle [get_bd_pins action_config_0/stream768to512_idle] [get_bd_pins image_processing/stream768to512_idle]
  connect_bd_net -net timer_hbm_beats [get_bd_pins action_config_0/beats_hbm] [get_bd_pins timer_hbm/beats]
  connect_bd_net -net timer_hbm_beats_ap_vld [get_bd_pins action_config_0/beats_hbm_valid] [get_bd_pins timer_hbm/beats_ap_vld]
  connect_bd_net -net timer_hbm_stalls [get_bd_pins action_config_0/stalls_hbm] [get_bd_pins timer_hbm/stalls]
  connect_bd_net -net timer_hbm_stalls_ap_vld [get_bd_pins action_config_0/stalls_hbm_valid] [get_bd_pins timer_hbm/stalls_ap_vld]
  connect_bd_net -net timer_host_beats [get_bd_pins action_config_0/beats_host] [get_bd_pins timer_host/beats]
  connect_bd_net -net timer_host_beats_ap_vld [get_bd_pins action_config_0/beats_host_valid] [get_bd_pins timer_host/beats_ap_vld]
  connect_bd_net -net timer_host_stalls [get_bd_pins action_config_0/stalls_host] [get_bd_pins timer_host/stalls]
  connect_bd_net -net timer_host_stalls_ap_vld [get_bd_pins action_config_0/stalls_host_valid] [get_bd_pins timer_host/stalls_ap_vld]
  connect_bd_net -net udp_0_counter [get_bd_pins action_config_0/packets_udp] [get_bd_pins udp_0/counter]
  connect_bd_net -net udp_0_counter_ap_vld [get_bd_pins action_config_0/packets_udp_valid] [get_bd_pins udp_0/counter_ap_vld]
  connect_bd_net -net udp_0_idle [get_bd_pins action_config_0/udp_idle] [get_bd_pins udp_0/idle]

  # Restore current instance
  current_bd_instance $oldCurInst
}
