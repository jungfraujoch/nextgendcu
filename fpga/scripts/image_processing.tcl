# SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
# SPDX-License-Identifier: CERN-OHL-S-2.0

# Hierarchical cell: image_processing
proc create_hier_cell_image_processing { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2092 -severity "ERROR" "create_hier_cell_image_processing() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 data_in

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 data_out

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 integration_result_out

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p0

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p1

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p2

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p3

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p4

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p5

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p6

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p7

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p8

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p9

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p10

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p11

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p12

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p13

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p14

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p15

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p16

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p17

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p18

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p19

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_pixel_mask

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 m_axis_completion

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 pixel_calc_out

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 result_out

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 roi_calc_out

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 s_axis_completion

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 spot_finder_out


  # Create pins
  create_bd_pin -dir I -type rst ap_rst_n
  create_bd_pin -dir I -type clk axi_clk
  create_bd_pin -dir I -type rst axi_rst_n
  create_bd_pin -dir O -from 0 -to 0 frame_summation_idle
  create_bd_pin -dir I -from 31 -to 0 -type data hbm_size_bytes
  create_bd_pin -dir I -from 31 -to 0 -type data in_count_threshold
  create_bd_pin -dir I -from 31 -to 0 -type data in_max_d_value
  create_bd_pin -dir I -from 31 -to 0 -type data in_min_d_value
  create_bd_pin -dir I -from 31 -to 0 -type data in_min_pix_per_spot
  create_bd_pin -dir I -from 31 -to 0 -type data in_snr_threshold
  create_bd_pin -dir O -from 0 -to 0 integration_idle
  create_bd_pin -dir O proc_fifo_empty
  create_bd_pin -dir O proc_fifo_full
  create_bd_pin -dir O -from 0 -to 0 stream768to512_idle

  # Create instance: adu_histo_0, and set properties
  set adu_histo_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:adu_histo:1.0 adu_histo_0 ]

  # Create instance: axis_32_to_512_0, and set properties
  set axis_32_to_512_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:axis_32_to_512:1.0 axis_32_to_512_0 ]

  # Create instance: axis_64_to_512_0, and set properties
  set axis_64_to_512_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:axis_64_to_512:1.0 axis_64_to_512_0 ]

  # Create instance: axis_compl_fifo_0, and set properties
  set axis_compl_fifo_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_compl_fifo_0 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
   CONFIG.HAS_AEMPTY {0} \
   CONFIG.HAS_AFULL {0} \
 ] $axis_compl_fifo_0

  # Create instance: axis_compl_fifo_1, and set properties
  set axis_compl_fifo_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_compl_fifo_1 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
   CONFIG.HAS_AEMPTY {0} \
   CONFIG.HAS_AFULL {0} \
 ] $axis_compl_fifo_1

  # Create instance: axis_compl_fifo_2, and set properties
  set axis_compl_fifo_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_compl_fifo_2 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
 ] $axis_compl_fifo_2

  # Create instance: axis_compl_fifo_3, and set properties
  set axis_compl_fifo_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_compl_fifo_3 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
 ] $axis_compl_fifo_3

  # Create instance: axis_compl_fifo_4, and set properties
  set axis_compl_fifo_4 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_compl_fifo_4 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
 ] $axis_compl_fifo_4

  # Create instance: axis_compl_fifo_5, and set properties
  set axis_compl_fifo_5 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_compl_fifo_5 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
 ] $axis_compl_fifo_5

  # Create instance: axis_compl_fifo_6, and set properties
  set axis_compl_fifo_6 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_compl_fifo_6 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
 ] $axis_compl_fifo_6

  # Create instance: axis_compl_fifo_7, and set properties
  set axis_compl_fifo_7 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_compl_fifo_7 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
 ] $axis_compl_fifo_7

  # Create instance: axis_compl_fifo_8, and set properties
  set axis_compl_fifo_8 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_compl_fifo_8 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
 ] $axis_compl_fifo_8

  # Create instance: axis_data_fifo_0, and set properties
  set axis_data_fifo_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_0 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16384} \
   CONFIG.FIFO_MEMORY_TYPE {ultra} \
 ] $axis_data_fifo_0

  # Create instance: axis_data_fifo_1, and set properties
  set axis_data_fifo_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_1 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {256} \
 ] $axis_data_fifo_1

  # Create instance: axis_data_fifo_2, and set properties
  set axis_data_fifo_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_2 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {256} \
 ] $axis_data_fifo_2

  # Create instance: axis_data_fifo_3, and set properties
  set axis_data_fifo_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_3 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {256} \
 ] $axis_data_fifo_3

  # Create instance: axis_data_fifo_4, and set properties
  set axis_data_fifo_4 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_4 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {256} \
 ] $axis_data_fifo_4

  # Create instance: axis_data_fifo_5, and set properties
  set axis_data_fifo_5 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_5 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {256} \
 ] $axis_data_fifo_5

  # Create instance: axis_data_fifo_6, and set properties
  set axis_data_fifo_6 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_6 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {256} \
 ] $axis_data_fifo_6

  # Create instance: axis_data_fifo_7, and set properties
  set axis_data_fifo_7 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_7 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {256} \
 ] $axis_data_fifo_7

  # Create instance: axis_data_fifo_8, and set properties
  set axis_data_fifo_8 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_8 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {256} \
   CONFIG.HAS_AEMPTY {1} \
   CONFIG.HAS_AFULL {1} \
 ] $axis_data_fifo_8

  # Create instance: axis_data_fifo_9, and set properties
  set axis_data_fifo_9 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_9 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {256} \
 ] $axis_data_fifo_9

  # Create instance: axis_data_fifo_11, and set properties
  set axis_data_fifo_11 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_11 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {512} \
 ] $axis_data_fifo_11

  # Create instance: axis_data_fifo_12, and set properties
  set axis_data_fifo_12 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_12 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {512} \
 ] $axis_data_fifo_12

  # Create instance: axis_data_fifo_13, and set properties
  set axis_data_fifo_13 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_13 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {512} \
 ] $axis_data_fifo_13

  # Create instance: axis_data_fifo_14, and set properties
  set axis_data_fifo_14 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_14 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {512} \
 ] $axis_data_fifo_14

  # Create instance: axis_data_fifo_10, and set properties
  set axis_data_fifo_10 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_10 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {64} \
 ] $axis_data_fifo_10

  # Create instance: axis_data_spot_finder_mask_0, and set properties
  set axis_data_spot_finder_mask_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_spot_finder_mask_0 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {512} \
 ] $axis_data_spot_finder_mask_0

  # Create instance: axis_data_spot_finder_mask_1, and set properties
  set axis_data_spot_finder_mask_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_spot_finder_mask_1 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {64} \
 ] $axis_data_spot_finder_mask_1

  # Create instance: axis_integration_result_fifo_0, and set properties
  set axis_integration_result_fifo_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_integration_result_fifo_0 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {256} \
 ] $axis_integration_result_fifo_0

  # Create instance: axis_integration_result_fifo_1, and set properties
  set axis_integration_result_fifo_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_integration_result_fifo_1 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {512} \
 ] $axis_integration_result_fifo_1

  # Create instance: axis_pixel_calc_fifo_0, and set properties
  set axis_pixel_calc_fifo_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_pixel_calc_fifo_0 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
 ] $axis_pixel_calc_fifo_0

  # Create instance: axis_register_slice_data_1, and set properties
  set axis_register_slice_data_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice:1.1 axis_register_slice_data_1 ]
  set_property -dict [ list \
   CONFIG.REG_CONFIG {16} \
 ] $axis_register_slice_data_1

  # Create instance: axis_register_slice_data_2, and set properties
  set axis_register_slice_data_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice:1.1 axis_register_slice_data_2 ]
  set_property -dict [ list \
   CONFIG.REG_CONFIG {16} \
 ] $axis_register_slice_data_2

  # Create instance: axis_register_slice_data_3, and set properties
  set axis_register_slice_data_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice:1.1 axis_register_slice_data_3 ]
  set_property -dict [ list \
   CONFIG.REG_CONFIG {16} \
 ] $axis_register_slice_data_3

  # Create instance: axis_roi_calc_result_fifo_0, and set properties
  set axis_roi_calc_result_fifo_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_roi_calc_result_fifo_0 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {512} \
 ] $axis_roi_calc_result_fifo_0

  # Create instance: axis_spot_finder_conn_fifo_0, and set properties
  set axis_spot_finder_conn_fifo_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_spot_finder_conn_fifo_0 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {2048} \
 ] $axis_spot_finder_conn_fifo_0

  # Create instance: axis_spot_finder_fifo_0, and set properties
  set axis_spot_finder_fifo_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_spot_finder_fifo_0 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {32} \
 ] $axis_spot_finder_fifo_0

  # Create instance: axis_spot_finder_fifo_1, and set properties
  set axis_spot_finder_fifo_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_spot_finder_fifo_1 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {2048} \
 ] $axis_spot_finder_fifo_1

  # Create instance: axis_spot_finder_fifo_2, and set properties
  set axis_spot_finder_fifo_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_spot_finder_fifo_2 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {128} \
 ] $axis_spot_finder_fifo_2

  # Create instance: axis_spot_finder_fifo_3, and set properties
  set axis_spot_finder_fifo_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_spot_finder_fifo_3 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {4096} \
   CONFIG.FIFO_MEMORY_TYPE {ultra} \
 ] $axis_spot_finder_fifo_3

  # Create instance: eiger_reorder_0, and set properties
  set eiger_reorder_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:eiger_reorder:1.0 eiger_reorder_0 ]

  # Create instance: frame_summation_0, and set properties
  set frame_summation_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:frame_summation:1.0 frame_summation_0 ]

  # Create instance: integration_0, and set properties
  set integration_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:integration:1.0 integration_0 ]

  # Create instance: jf_conversion_0, and set properties
  set jf_conversion_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:jf_conversion:1.0 jf_conversion_0 ]

  # Create instance: mask_missing_0, and set properties
  set mask_missing_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:mask_missing:1.0 mask_missing_0 ]

  # Create instance: one, and set properties
  set one [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 one ]

  # Create instance: pixel_calc_0, and set properties
  set pixel_calc_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:pixel_calc:1.0 pixel_calc_0 ]

  # Create instance: pixel_mask_0, and set properties
  set pixel_mask_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:pixel_mask:1.0 pixel_mask_0 ]

  # Create instance: pixel_sqrt_0, and set properties
  set pixel_sqrt_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:pixel_sqrt:1.0 pixel_sqrt_0 ]

  # Create instance: pixel_threshold_0, and set properties
  set pixel_threshold_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:pixel_threshold:1.0 pixel_threshold_0 ]

  # Create instance: roi_calc_0, and set properties
  set roi_calc_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:roi_calc:1.0 roi_calc_0 ]

  # Create instance: spot_finder_0, and set properties
  set spot_finder_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:spot_finder:1.0 spot_finder_0 ]

  # Create instance: spot_finder_1, and set properties
  set spot_finder_1 [ create_bd_cell -type ip -vlnv psi.ch:hls:spot_finder:1.0 spot_finder_1 ]

  # Create instance: spot_finder_connecti_0, and set properties
  set spot_finder_connecti_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:spot_finder_connectivity:1.0 spot_finder_connecti_0 ]

  # Create instance: spot_finder_mask_0, and set properties
  set spot_finder_mask_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:spot_finder_mask:1.0 spot_finder_mask_0 ]

  # Create instance: spot_finder_merge_0, and set properties
  set spot_finder_merge_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:spot_finder_merge:1.0 spot_finder_merge_0 ]

  # Create instance: stream768to512_0, and set properties
  set stream768to512_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:stream768to512:1.0 stream768to512_0 ]

  # Create interface connections
  connect_bd_intf_net -intf_net Conn1 [get_bd_intf_pins spot_finder_out] [get_bd_intf_pins axis_spot_finder_fifo_3/M_AXIS]
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins m_axi_pixel_mask] [get_bd_intf_pins pixel_mask_0/m_axi_pixel_mask]
  connect_bd_intf_net -intf_net Conn3 [get_bd_intf_pins integration_result_out] [get_bd_intf_pins axis_integration_result_fifo_1/M_AXIS]
  connect_bd_intf_net -intf_net adu_histo_0_data_out [get_bd_intf_pins adu_histo_0/data_out] [get_bd_intf_pins axis_data_fifo_1/S_AXIS]
  connect_bd_intf_net -intf_net adu_histo_0_m_axis_completion [get_bd_intf_pins adu_histo_0/m_axis_completion] [get_bd_intf_pins axis_compl_fifo_1/S_AXIS]
  connect_bd_intf_net -intf_net adu_histo_0_result_out [get_bd_intf_pins result_out] [get_bd_intf_pins adu_histo_0/result_out]
  connect_bd_intf_net -intf_net axis_32_to_512_0_data_out [get_bd_intf_pins axis_32_to_512_0/data_out] [get_bd_intf_pins axis_spot_finder_fifo_3/S_AXIS]
  connect_bd_intf_net -intf_net axis_64_to_512_0_data_out [get_bd_intf_pins axis_64_to_512_0/data_out] [get_bd_intf_pins axis_integration_result_fifo_1/S_AXIS]
  connect_bd_intf_net -intf_net axis_compl_fifo_0_M_AXIS [get_bd_intf_pins adu_histo_0/s_axis_completion] [get_bd_intf_pins axis_compl_fifo_0/M_AXIS]
  connect_bd_intf_net -intf_net axis_compl_fifo_0_S_AXIS [get_bd_intf_pins s_axis_completion] [get_bd_intf_pins axis_compl_fifo_0/S_AXIS]
  connect_bd_intf_net -intf_net axis_compl_fifo_1_M_AXIS [get_bd_intf_pins axis_compl_fifo_1/M_AXIS] [get_bd_intf_pins mask_missing_0/s_axis_completion]
  connect_bd_intf_net -intf_net axis_compl_fifo_2_M_AXIS [get_bd_intf_pins axis_compl_fifo_2/M_AXIS] [get_bd_intf_pins eiger_reorder_0/s_axis_completion]
  connect_bd_intf_net -intf_net axis_compl_fifo_3_M_AXIS [get_bd_intf_pins axis_compl_fifo_3/M_AXIS] [get_bd_intf_pins pixel_mask_0/s_axis_completion]
  connect_bd_intf_net -intf_net axis_compl_fifo_4_M_AXIS [get_bd_intf_pins axis_compl_fifo_4/M_AXIS] [get_bd_intf_pins jf_conversion_0/s_axis_completion]
  connect_bd_intf_net -intf_net axis_compl_fifo_5_M_AXIS [get_bd_intf_pins axis_compl_fifo_5/M_AXIS] [get_bd_intf_pins frame_summation_0/s_axis_completion]
  connect_bd_intf_net -intf_net axis_compl_fifo_6_M_AXIS [get_bd_intf_pins axis_compl_fifo_6/M_AXIS] [get_bd_intf_pins integration_0/s_axis_completion]
  connect_bd_intf_net -intf_net axis_compl_fifo_7_M_AXIS [get_bd_intf_pins axis_compl_fifo_7/M_AXIS] [get_bd_intf_pins spot_finder_mask_0/s_axis_completion]
  connect_bd_intf_net -intf_net axis_compl_fifo_8_M_AXIS [get_bd_intf_pins axis_compl_fifo_8/M_AXIS] [get_bd_intf_pins roi_calc_0/s_axis_completion]
  connect_bd_intf_net -intf_net axis_data_fifo_0_M_AXIS [get_bd_intf_pins adu_histo_0/data_in] [get_bd_intf_pins axis_data_fifo_0/M_AXIS]
  connect_bd_intf_net -intf_net axis_data_fifo_11_M_AXIS [get_bd_intf_pins axis_data_fifo_11/M_AXIS] [get_bd_intf_pins roi_calc_0/data_in]
  connect_bd_intf_net -intf_net axis_data_fifo_12_M_AXIS [get_bd_intf_pins axis_data_fifo_12/M_AXIS] [get_bd_intf_pins pixel_calc_0/data_in]
  connect_bd_intf_net -intf_net axis_data_fifo_13_M_AXIS [get_bd_intf_pins axis_data_fifo_13/M_AXIS] [get_bd_intf_pins pixel_sqrt_0/data_in]
  connect_bd_intf_net -intf_net axis_data_fifo_14_M_AXIS [get_bd_intf_pins axis_data_fifo_14/M_AXIS] [get_bd_intf_pins stream768to512_0/data_in]
  connect_bd_intf_net -intf_net axis_data_fifo_1_M_AXIS [get_bd_intf_pins axis_data_fifo_1/M_AXIS] [get_bd_intf_pins mask_missing_0/data_in]
  connect_bd_intf_net -intf_net axis_data_fifo_2_M_AXIS [get_bd_intf_pins axis_data_fifo_2/M_AXIS] [get_bd_intf_pins eiger_reorder_0/data_in]
  connect_bd_intf_net -intf_net axis_data_fifo_3_M_AXIS [get_bd_intf_pins axis_data_fifo_3/M_AXIS] [get_bd_intf_pins pixel_mask_0/data_in]
  connect_bd_intf_net -intf_net axis_data_fifo_4_M_AXIS [get_bd_intf_pins axis_data_fifo_4/M_AXIS] [get_bd_intf_pins jf_conversion_0/data_in]
  connect_bd_intf_net -intf_net axis_data_fifo_5_M_AXIS [get_bd_intf_pins axis_data_fifo_5/M_AXIS] [get_bd_intf_pins axis_register_slice_data_1/S_AXIS]
  connect_bd_intf_net -intf_net axis_data_fifo_6_M_AXIS [get_bd_intf_pins axis_data_fifo_6/M_AXIS] [get_bd_intf_pins frame_summation_0/data_in]
  connect_bd_intf_net -intf_net axis_data_fifo_7_M_AXIS [get_bd_intf_pins axis_data_fifo_7/M_AXIS] [get_bd_intf_pins integration_0/data_in]
  connect_bd_intf_net -intf_net axis_data_fifo_8_M_AXIS [get_bd_intf_pins axis_data_fifo_8/M_AXIS] [get_bd_intf_pins spot_finder_mask_0/data_in]
  connect_bd_intf_net -intf_net axis_data_fifo_9_M_AXIS [get_bd_intf_pins axis_data_fifo_9/M_AXIS] [get_bd_intf_pins spot_finder_0/data_in]
  connect_bd_intf_net -intf_net axis_data_fifo_10_M_AXIS [get_bd_intf_pins axis_data_fifo_10/M_AXIS] [get_bd_intf_pins spot_finder_1/data_in]
  connect_bd_intf_net -intf_net axis_data_spot_finder_mask_0_M_AXIS [get_bd_intf_pins axis_data_spot_finder_mask_0/M_AXIS] [get_bd_intf_pins spot_finder_0/mask_in]
  connect_bd_intf_net -intf_net axis_data_spot_finder_mask_1_M_AXIS [get_bd_intf_pins axis_data_spot_finder_mask_1/M_AXIS] [get_bd_intf_pins spot_finder_1/mask_in]
  connect_bd_intf_net -intf_net axis_integration_result_fifo_0_M_AXIS [get_bd_intf_pins axis_64_to_512_0/data_in] [get_bd_intf_pins axis_integration_result_fifo_0/M_AXIS]
  connect_bd_intf_net -intf_net axis_pixel_calc_fifo_0_M_AXIS [get_bd_intf_pins pixel_calc_out] [get_bd_intf_pins axis_pixel_calc_fifo_0/M_AXIS]
  connect_bd_intf_net -intf_net axis_register_slice_data_1_M_AXIS [get_bd_intf_pins axis_register_slice_data_1/M_AXIS] [get_bd_intf_pins pixel_threshold_0/data_in]
  connect_bd_intf_net -intf_net axis_register_slice_data_2_M_AXIS [get_bd_intf_pins axis_data_fifo_7/S_AXIS] [get_bd_intf_pins axis_register_slice_data_2/M_AXIS]
  connect_bd_intf_net -intf_net axis_register_slice_data_3_M_AXIS [get_bd_intf_pins data_out] [get_bd_intf_pins axis_register_slice_data_3/M_AXIS]
  connect_bd_intf_net -intf_net axis_roi_calc_result_fifo_0_M_AXIS [get_bd_intf_pins roi_calc_out] [get_bd_intf_pins axis_roi_calc_result_fifo_0/M_AXIS]
  connect_bd_intf_net -intf_net axis_spot_finder_conn_fifo_0_M_AXIS [get_bd_intf_pins axis_spot_finder_conn_fifo_0/M_AXIS] [get_bd_intf_pins spot_finder_merge_0/connectivity_in]
  connect_bd_intf_net -intf_net axis_spot_finder_fifo_0_M_AXIS [get_bd_intf_pins axis_spot_finder_fifo_0/M_AXIS] [get_bd_intf_pins spot_finder_connecti_0/data_in]
  connect_bd_intf_net -intf_net axis_spot_finder_fifo_1_M_AXIS [get_bd_intf_pins axis_spot_finder_fifo_1/M_AXIS] [get_bd_intf_pins spot_finder_merge_0/data_in]
  connect_bd_intf_net -intf_net axis_spot_finder_fifo_2_M_AXIS [get_bd_intf_pins axis_32_to_512_0/data_in] [get_bd_intf_pins axis_spot_finder_fifo_2/M_AXIS]
  connect_bd_intf_net -intf_net data_in_1 [get_bd_intf_pins data_in] [get_bd_intf_pins axis_data_fifo_0/S_AXIS]
  connect_bd_intf_net -intf_net eiger_reorder_0_data_out [get_bd_intf_pins axis_data_fifo_3/S_AXIS] [get_bd_intf_pins eiger_reorder_0/data_out]
  connect_bd_intf_net -intf_net eiger_reorder_0_m_axis_completion [get_bd_intf_pins axis_compl_fifo_3/S_AXIS] [get_bd_intf_pins eiger_reorder_0/m_axis_completion]
  connect_bd_intf_net -intf_net frame_summation_0_data_out [get_bd_intf_pins axis_register_slice_data_2/S_AXIS] [get_bd_intf_pins frame_summation_0/data_out]
  connect_bd_intf_net -intf_net frame_summation_0_m_axis_completion [get_bd_intf_pins axis_compl_fifo_6/S_AXIS] [get_bd_intf_pins frame_summation_0/m_axis_completion]
  connect_bd_intf_net -intf_net integration_0_data_out [get_bd_intf_pins axis_data_fifo_8/S_AXIS] [get_bd_intf_pins integration_0/data_out]
  connect_bd_intf_net -intf_net integration_0_m_axi_d_hbm_p0 [get_bd_intf_pins m_axi_d_hbm_p12] [get_bd_intf_pins integration_0/m_axi_d_hbm_p0]
  connect_bd_intf_net -intf_net integration_0_m_axi_d_hbm_p1 [get_bd_intf_pins m_axi_d_hbm_p13] [get_bd_intf_pins integration_0/m_axi_d_hbm_p1]
  connect_bd_intf_net -intf_net integration_0_m_axi_d_hbm_p2 [get_bd_intf_pins m_axi_d_hbm_p14] [get_bd_intf_pins integration_0/m_axi_d_hbm_p2]
  connect_bd_intf_net -intf_net integration_0_m_axi_d_hbm_p3 [get_bd_intf_pins m_axi_d_hbm_p15] [get_bd_intf_pins integration_0/m_axi_d_hbm_p3]
  connect_bd_intf_net -intf_net integration_0_m_axis_completion [get_bd_intf_pins axis_compl_fifo_7/S_AXIS] [get_bd_intf_pins integration_0/m_axis_completion]
  connect_bd_intf_net -intf_net integration_0_result_out [get_bd_intf_pins axis_integration_result_fifo_0/S_AXIS] [get_bd_intf_pins integration_0/result_out]
  connect_bd_intf_net -intf_net jf_conversion_0_data_out [get_bd_intf_pins axis_data_fifo_5/S_AXIS] [get_bd_intf_pins jf_conversion_0/data_out]
  connect_bd_intf_net -intf_net jf_conversion_0_m_axi_d_hbm_p0 [get_bd_intf_pins m_axi_d_hbm_p0] [get_bd_intf_pins jf_conversion_0/m_axi_d_hbm_p0]
  connect_bd_intf_net -intf_net jf_conversion_0_m_axi_d_hbm_p1 [get_bd_intf_pins m_axi_d_hbm_p1] [get_bd_intf_pins jf_conversion_0/m_axi_d_hbm_p1]
  connect_bd_intf_net -intf_net jf_conversion_0_m_axi_d_hbm_p2 [get_bd_intf_pins m_axi_d_hbm_p2] [get_bd_intf_pins jf_conversion_0/m_axi_d_hbm_p2]
  connect_bd_intf_net -intf_net jf_conversion_0_m_axi_d_hbm_p3 [get_bd_intf_pins m_axi_d_hbm_p3] [get_bd_intf_pins jf_conversion_0/m_axi_d_hbm_p3]
  connect_bd_intf_net -intf_net jf_conversion_0_m_axi_d_hbm_p4 [get_bd_intf_pins m_axi_d_hbm_p4] [get_bd_intf_pins jf_conversion_0/m_axi_d_hbm_p4]
  connect_bd_intf_net -intf_net jf_conversion_0_m_axi_d_hbm_p5 [get_bd_intf_pins m_axi_d_hbm_p5] [get_bd_intf_pins jf_conversion_0/m_axi_d_hbm_p5]
  connect_bd_intf_net -intf_net jf_conversion_0_m_axi_d_hbm_p6 [get_bd_intf_pins m_axi_d_hbm_p6] [get_bd_intf_pins jf_conversion_0/m_axi_d_hbm_p6]
  connect_bd_intf_net -intf_net jf_conversion_0_m_axi_d_hbm_p7 [get_bd_intf_pins m_axi_d_hbm_p7] [get_bd_intf_pins jf_conversion_0/m_axi_d_hbm_p7]
  connect_bd_intf_net -intf_net jf_conversion_0_m_axi_d_hbm_p8 [get_bd_intf_pins m_axi_d_hbm_p8] [get_bd_intf_pins jf_conversion_0/m_axi_d_hbm_p8]
  connect_bd_intf_net -intf_net jf_conversion_0_m_axi_d_hbm_p9 [get_bd_intf_pins m_axi_d_hbm_p9] [get_bd_intf_pins jf_conversion_0/m_axi_d_hbm_p9]
  connect_bd_intf_net -intf_net jf_conversion_0_m_axi_d_hbm_p10 [get_bd_intf_pins m_axi_d_hbm_p10] [get_bd_intf_pins jf_conversion_0/m_axi_d_hbm_p10]
  connect_bd_intf_net -intf_net jf_conversion_0_m_axi_d_hbm_p11 [get_bd_intf_pins m_axi_d_hbm_p11] [get_bd_intf_pins jf_conversion_0/m_axi_d_hbm_p11]
  connect_bd_intf_net -intf_net jf_conversion_0_m_axis_completion [get_bd_intf_pins axis_compl_fifo_5/S_AXIS] [get_bd_intf_pins jf_conversion_0/m_axis_completion]
  connect_bd_intf_net -intf_net mask_missing_0_data_out [get_bd_intf_pins axis_data_fifo_2/S_AXIS] [get_bd_intf_pins mask_missing_0/data_out]
  connect_bd_intf_net -intf_net mask_missing_0_m_axis_completion [get_bd_intf_pins axis_compl_fifo_2/S_AXIS] [get_bd_intf_pins mask_missing_0/m_axis_completion]
  connect_bd_intf_net -intf_net pixel_calc_0_calc_out [get_bd_intf_pins axis_pixel_calc_fifo_0/S_AXIS] [get_bd_intf_pins pixel_calc_0/calc_out]
  connect_bd_intf_net -intf_net pixel_calc_0_data_out [get_bd_intf_pins axis_data_fifo_13/S_AXIS] [get_bd_intf_pins pixel_calc_0/data_out]
  connect_bd_intf_net -intf_net pixel_mask_0_data_out [get_bd_intf_pins axis_data_fifo_4/S_AXIS] [get_bd_intf_pins pixel_mask_0/data_out]
  connect_bd_intf_net -intf_net pixel_mask_0_m_axis_completion [get_bd_intf_pins axis_compl_fifo_4/S_AXIS] [get_bd_intf_pins pixel_mask_0/m_axis_completion]
  connect_bd_intf_net -intf_net pixel_sqrt_0_data_out [get_bd_intf_pins axis_data_fifo_14/S_AXIS] [get_bd_intf_pins pixel_sqrt_0/data_out]
  connect_bd_intf_net -intf_net pixel_threshold_0_data_out [get_bd_intf_pins axis_data_fifo_6/S_AXIS] [get_bd_intf_pins pixel_threshold_0/data_out]
  connect_bd_intf_net -intf_net roi_calc_0_data_out [get_bd_intf_pins axis_data_fifo_12/S_AXIS] [get_bd_intf_pins roi_calc_0/data_out]
  connect_bd_intf_net -intf_net roi_calc_0_m_axi_d_hbm_p0 [get_bd_intf_pins m_axi_d_hbm_p18] [get_bd_intf_pins roi_calc_0/m_axi_d_hbm_p0]
  connect_bd_intf_net -intf_net roi_calc_0_m_axi_d_hbm_p1 [get_bd_intf_pins m_axi_d_hbm_p19] [get_bd_intf_pins roi_calc_0/m_axi_d_hbm_p1]
  connect_bd_intf_net -intf_net roi_calc_0_m_axis_completion [get_bd_intf_pins m_axis_completion] [get_bd_intf_pins roi_calc_0/m_axis_completion]
  connect_bd_intf_net -intf_net roi_calc_0_roi_out [get_bd_intf_pins axis_roi_calc_result_fifo_0/S_AXIS] [get_bd_intf_pins roi_calc_0/roi_out]
  connect_bd_intf_net -intf_net spot_finder_0_data_out [get_bd_intf_pins axis_data_fifo_10/S_AXIS] [get_bd_intf_pins spot_finder_0/data_out]
  connect_bd_intf_net -intf_net spot_finder_0_mask_out [get_bd_intf_pins axis_data_spot_finder_mask_1/S_AXIS] [get_bd_intf_pins spot_finder_0/mask_out]
  connect_bd_intf_net -intf_net spot_finder_1_data_out [get_bd_intf_pins axis_data_fifo_11/S_AXIS] [get_bd_intf_pins spot_finder_1/data_out]
  connect_bd_intf_net -intf_net spot_finder_1_strong_pixel_out [get_bd_intf_pins axis_spot_finder_fifo_0/S_AXIS] [get_bd_intf_pins spot_finder_1/strong_pixel_out]
  connect_bd_intf_net -intf_net spot_finder_connecti_0_connectivity_out [get_bd_intf_pins axis_spot_finder_conn_fifo_0/S_AXIS] [get_bd_intf_pins spot_finder_connecti_0/connectivity_out]
  connect_bd_intf_net -intf_net spot_finder_connecti_0_data_out [get_bd_intf_pins axis_spot_finder_fifo_1/S_AXIS] [get_bd_intf_pins spot_finder_connecti_0/data_out]
  connect_bd_intf_net -intf_net spot_finder_mask_0_data_out [get_bd_intf_pins axis_data_fifo_9/S_AXIS] [get_bd_intf_pins spot_finder_mask_0/data_out]
  connect_bd_intf_net -intf_net spot_finder_mask_0_m_axi_d_hbm_p0 [get_bd_intf_pins m_axi_d_hbm_p16] [get_bd_intf_pins spot_finder_mask_0/m_axi_d_hbm_p0]
  connect_bd_intf_net -intf_net spot_finder_mask_0_m_axi_d_hbm_p1 [get_bd_intf_pins m_axi_d_hbm_p17] [get_bd_intf_pins spot_finder_mask_0/m_axi_d_hbm_p1]
  connect_bd_intf_net -intf_net spot_finder_mask_0_m_axis_completion [get_bd_intf_pins axis_compl_fifo_8/S_AXIS] [get_bd_intf_pins spot_finder_mask_0/m_axis_completion]
  connect_bd_intf_net -intf_net spot_finder_mask_0_mask_out [get_bd_intf_pins axis_data_spot_finder_mask_0/S_AXIS] [get_bd_intf_pins spot_finder_mask_0/mask_out]
  connect_bd_intf_net -intf_net spot_finder_merge_0_data_out [get_bd_intf_pins axis_spot_finder_fifo_2/S_AXIS] [get_bd_intf_pins spot_finder_merge_0/data_out]
  connect_bd_intf_net -intf_net stream768to512_0_data_out [get_bd_intf_pins axis_register_slice_data_3/S_AXIS] [get_bd_intf_pins stream768to512_0/data_out]

  # Create port connections
  connect_bd_net -net ap_rst_n_1 [get_bd_pins ap_rst_n] [get_bd_pins adu_histo_0/ap_rst_n] [get_bd_pins axis_32_to_512_0/ap_rst_n] [get_bd_pins axis_64_to_512_0/ap_rst_n] [get_bd_pins eiger_reorder_0/ap_rst_n] [get_bd_pins frame_summation_0/ap_rst_n] [get_bd_pins integration_0/ap_rst_n] [get_bd_pins jf_conversion_0/ap_rst_n] [get_bd_pins mask_missing_0/ap_rst_n] [get_bd_pins pixel_calc_0/ap_rst_n] [get_bd_pins pixel_mask_0/ap_rst_n] [get_bd_pins pixel_sqrt_0/ap_rst_n] [get_bd_pins pixel_threshold_0/ap_rst_n] [get_bd_pins roi_calc_0/ap_rst_n] [get_bd_pins spot_finder_0/ap_rst_n] [get_bd_pins spot_finder_1/ap_rst_n] [get_bd_pins spot_finder_connecti_0/ap_rst_n] [get_bd_pins spot_finder_mask_0/ap_rst_n] [get_bd_pins spot_finder_merge_0/ap_rst_n] [get_bd_pins stream768to512_0/ap_rst_n]
  connect_bd_net -net axi_clk_1 [get_bd_pins axi_clk] [get_bd_pins adu_histo_0/ap_clk] [get_bd_pins axis_32_to_512_0/ap_clk] [get_bd_pins axis_64_to_512_0/ap_clk] [get_bd_pins axis_compl_fifo_0/s_axis_aclk] [get_bd_pins axis_compl_fifo_1/s_axis_aclk] [get_bd_pins axis_compl_fifo_2/s_axis_aclk] [get_bd_pins axis_compl_fifo_3/s_axis_aclk] [get_bd_pins axis_compl_fifo_4/s_axis_aclk] [get_bd_pins axis_compl_fifo_5/s_axis_aclk] [get_bd_pins axis_compl_fifo_6/s_axis_aclk] [get_bd_pins axis_compl_fifo_7/s_axis_aclk] [get_bd_pins axis_compl_fifo_8/s_axis_aclk] [get_bd_pins axis_data_fifo_0/s_axis_aclk] [get_bd_pins axis_data_fifo_1/s_axis_aclk] [get_bd_pins axis_data_fifo_11/s_axis_aclk] [get_bd_pins axis_data_fifo_12/s_axis_aclk] [get_bd_pins axis_data_fifo_13/s_axis_aclk] [get_bd_pins axis_data_fifo_14/s_axis_aclk] [get_bd_pins axis_data_fifo_2/s_axis_aclk] [get_bd_pins axis_data_fifo_3/s_axis_aclk] [get_bd_pins axis_data_fifo_4/s_axis_aclk] [get_bd_pins axis_data_fifo_5/s_axis_aclk] [get_bd_pins axis_data_fifo_6/s_axis_aclk] [get_bd_pins axis_data_fifo_7/s_axis_aclk] [get_bd_pins axis_data_fifo_8/s_axis_aclk] [get_bd_pins axis_data_fifo_9/s_axis_aclk] [get_bd_pins axis_data_fifo_10/s_axis_aclk] [get_bd_pins axis_data_spot_finder_mask_0/s_axis_aclk] [get_bd_pins axis_data_spot_finder_mask_1/s_axis_aclk] [get_bd_pins axis_integration_result_fifo_0/s_axis_aclk] [get_bd_pins axis_integration_result_fifo_1/s_axis_aclk] [get_bd_pins axis_pixel_calc_fifo_0/s_axis_aclk] [get_bd_pins axis_register_slice_data_1/aclk] [get_bd_pins axis_register_slice_data_2/aclk] [get_bd_pins axis_register_slice_data_3/aclk] [get_bd_pins axis_roi_calc_result_fifo_0/s_axis_aclk] [get_bd_pins axis_spot_finder_conn_fifo_0/s_axis_aclk] [get_bd_pins axis_spot_finder_fifo_0/s_axis_aclk] [get_bd_pins axis_spot_finder_fifo_1/s_axis_aclk] [get_bd_pins axis_spot_finder_fifo_2/s_axis_aclk] [get_bd_pins axis_spot_finder_fifo_3/s_axis_aclk] [get_bd_pins eiger_reorder_0/ap_clk] [get_bd_pins frame_summation_0/ap_clk] [get_bd_pins integration_0/ap_clk] [get_bd_pins jf_conversion_0/ap_clk] [get_bd_pins mask_missing_0/ap_clk] [get_bd_pins pixel_calc_0/ap_clk] [get_bd_pins pixel_mask_0/ap_clk] [get_bd_pins pixel_sqrt_0/ap_clk] [get_bd_pins pixel_threshold_0/ap_clk] [get_bd_pins roi_calc_0/ap_clk] [get_bd_pins spot_finder_0/ap_clk] [get_bd_pins spot_finder_1/ap_clk] [get_bd_pins spot_finder_connecti_0/ap_clk] [get_bd_pins spot_finder_mask_0/ap_clk] [get_bd_pins spot_finder_merge_0/ap_clk] [get_bd_pins stream768to512_0/ap_clk]
  connect_bd_net -net axi_rst_n_1 [get_bd_pins axi_rst_n] [get_bd_pins axis_compl_fifo_0/s_axis_aresetn] [get_bd_pins axis_compl_fifo_1/s_axis_aresetn] [get_bd_pins axis_compl_fifo_2/s_axis_aresetn] [get_bd_pins axis_compl_fifo_3/s_axis_aresetn] [get_bd_pins axis_compl_fifo_4/s_axis_aresetn] [get_bd_pins axis_compl_fifo_5/s_axis_aresetn] [get_bd_pins axis_compl_fifo_6/s_axis_aresetn] [get_bd_pins axis_compl_fifo_7/s_axis_aresetn] [get_bd_pins axis_compl_fifo_8/s_axis_aresetn] [get_bd_pins axis_data_fifo_0/s_axis_aresetn] [get_bd_pins axis_data_fifo_1/s_axis_aresetn] [get_bd_pins axis_data_fifo_11/s_axis_aresetn] [get_bd_pins axis_data_fifo_12/s_axis_aresetn] [get_bd_pins axis_data_fifo_13/s_axis_aresetn] [get_bd_pins axis_data_fifo_14/s_axis_aresetn] [get_bd_pins axis_data_fifo_2/s_axis_aresetn] [get_bd_pins axis_data_fifo_3/s_axis_aresetn] [get_bd_pins axis_data_fifo_4/s_axis_aresetn] [get_bd_pins axis_data_fifo_5/s_axis_aresetn] [get_bd_pins axis_data_fifo_6/s_axis_aresetn] [get_bd_pins axis_data_fifo_7/s_axis_aresetn] [get_bd_pins axis_data_fifo_8/s_axis_aresetn] [get_bd_pins axis_data_fifo_9/s_axis_aresetn] [get_bd_pins axis_data_fifo_10/s_axis_aresetn] [get_bd_pins axis_data_spot_finder_mask_0/s_axis_aresetn] [get_bd_pins axis_data_spot_finder_mask_1/s_axis_aresetn] [get_bd_pins axis_integration_result_fifo_0/s_axis_aresetn] [get_bd_pins axis_integration_result_fifo_1/s_axis_aresetn] [get_bd_pins axis_pixel_calc_fifo_0/s_axis_aresetn] [get_bd_pins axis_register_slice_data_1/aresetn] [get_bd_pins axis_register_slice_data_2/aresetn] [get_bd_pins axis_register_slice_data_3/aresetn] [get_bd_pins axis_roi_calc_result_fifo_0/s_axis_aresetn] [get_bd_pins axis_spot_finder_conn_fifo_0/s_axis_aresetn] [get_bd_pins axis_spot_finder_fifo_0/s_axis_aresetn] [get_bd_pins axis_spot_finder_fifo_1/s_axis_aresetn] [get_bd_pins axis_spot_finder_fifo_2/s_axis_aresetn] [get_bd_pins axis_spot_finder_fifo_3/s_axis_aresetn]
  connect_bd_net -net axis_data_fifo_8_almost_empty [get_bd_pins proc_fifo_empty] [get_bd_pins axis_data_fifo_8/almost_empty]
  connect_bd_net -net axis_data_fifo_8_almost_full [get_bd_pins proc_fifo_full] [get_bd_pins axis_data_fifo_8/almost_full]
  connect_bd_net -net frame_summation_0_idle [get_bd_pins frame_summation_idle] [get_bd_pins frame_summation_0/idle]
  connect_bd_net -net hbm_size_bytes_1 [get_bd_pins hbm_size_bytes] [get_bd_pins integration_0/hbm_size_bytes] [get_bd_pins jf_conversion_0/hbm_size_bytes] [get_bd_pins roi_calc_0/hbm_size_bytes] [get_bd_pins spot_finder_mask_0/hbm_size_bytes]
  connect_bd_net -net in_count_threshold_1 [get_bd_pins in_count_threshold] [get_bd_pins spot_finder_0/in_count_threshold] [get_bd_pins spot_finder_1/in_count_threshold]
  connect_bd_net -net in_max_d_value_1 [get_bd_pins in_max_d_value] [get_bd_pins spot_finder_mask_0/in_max_d_value]
  connect_bd_net -net in_min_d_value_1 [get_bd_pins in_min_d_value] [get_bd_pins spot_finder_mask_0/in_min_d_value]
  connect_bd_net -net in_min_pix_per_spot_1 [get_bd_pins in_min_pix_per_spot] [get_bd_pins spot_finder_merge_0/in_min_pix_per_spot]
  connect_bd_net -net in_snr_threshold_1 [get_bd_pins in_snr_threshold] [get_bd_pins spot_finder_0/in_snr_threshold] [get_bd_pins spot_finder_1/in_snr_threshold]
  connect_bd_net -net integration_0_idle [get_bd_pins integration_idle] [get_bd_pins integration_0/idle]
  connect_bd_net -net one_1 [get_bd_pins frame_summation_0/ap_start] [get_bd_pins integration_0/ap_start] [get_bd_pins one/dout] [get_bd_pins spot_finder_0/ap_start] [get_bd_pins spot_finder_0/strong_pixel_out_TREADY] [get_bd_pins spot_finder_1/ap_start] [get_bd_pins spot_finder_1/mask_out_TREADY] [get_bd_pins stream768to512_0/ap_start]
  connect_bd_net -net stream768to512_0_idle [get_bd_pins stream768to512_idle] [get_bd_pins stream768to512_0/idle]

  # Restore current instance
  current_bd_instance $oldCurInst
}