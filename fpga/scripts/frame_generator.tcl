# SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
# SPDX-License-Identifier: CERN-OHL-S-2.0

# Hierarchical cell: frame_generator
proc create_hier_cell_frame_generator { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2092 -severity "ERROR" "create_hier_cell_frame_generator() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 M_AXIS

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p0

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p1

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 s_axi_control


  # Create pins
  create_bd_pin -dir O almost_empty
  create_bd_pin -dir O almost_full
  create_bd_pin -dir I -type rst ap_rst_n
  create_bd_pin -dir I -type clk axi_clk
  create_bd_pin -dir I -type rst axi_rst_n
  create_bd_pin -dir I -from 31 -to 0 -type data hbm_size_bytes
  create_bd_pin -dir I -from 0 -to 0 -type data in_cancel
  create_bd_pin -dir I -from 31 -to 0 -type data src_ipv4_addr
  create_bd_pin -dir I -from 47 -to 0 -type data src_mac_addr

  # Create instance: axis_frame_generator_fifo_0, and set properties
  set axis_frame_generator_fifo_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_frame_generator_fifo_0 ]
  set_property -dict [ list \
   CONFIG.HAS_AEMPTY {1} \
   CONFIG.HAS_AFULL {1} \
 ] $axis_frame_generator_fifo_0

  # Create instance: axis_frame_generator_fifo_1, and set properties
  set axis_frame_generator_fifo_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_frame_generator_fifo_1 ]
  set_property -dict [ list \
   CONFIG.HAS_AEMPTY {0} \
   CONFIG.HAS_AFULL {0} \
 ] $axis_frame_generator_fifo_1

  # Create instance: axis_register_slice_0, and set properties
  set axis_register_slice_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_register_slice:1.1 axis_register_slice_0 ]
  set_property -dict [ list \
   CONFIG.REG_CONFIG {12} \
 ] $axis_register_slice_0

  # Create instance: frame_generator_0, and set properties
  set frame_generator_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:frame_generator:1.0 frame_generator_0 ]

  # Create instance: network_stack
  create_hier_cell_network_stack $hier_obj network_stack

  # Create instance: one, and set properties
  set one [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 one ]

  # Create instance: smartconnect_0, and set properties
  set smartconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:smartconnect:1.0 smartconnect_0 ]
  set_property -dict [ list \
   CONFIG.NUM_MI {2} \
   CONFIG.NUM_SI {1} \
 ] $smartconnect_0

  # Create interface connections
  connect_bd_intf_net -intf_net Conn2 [get_bd_intf_pins m_axi_d_hbm_p0] [get_bd_intf_pins frame_generator_0/m_axi_d_hbm_p0]
  connect_bd_intf_net -intf_net Conn3 [get_bd_intf_pins m_axi_d_hbm_p1] [get_bd_intf_pins frame_generator_0/m_axi_d_hbm_p1]
  connect_bd_intf_net -intf_net axis_frame_generator_fifo_0_M_AXIS [get_bd_intf_pins axis_frame_generator_fifo_0/M_AXIS] [get_bd_intf_pins network_stack/eth_in]
  connect_bd_intf_net -intf_net axis_frame_generator_fifo_1_M_AXIS [get_bd_intf_pins axis_frame_generator_fifo_1/M_AXIS] [get_bd_intf_pins axis_register_slice_0/S_AXIS]
  connect_bd_intf_net -intf_net axis_register_slice_0_M_AXIS [get_bd_intf_pins M_AXIS] [get_bd_intf_pins axis_register_slice_0/M_AXIS]
  connect_bd_intf_net -intf_net frame_generator_0_data_out [get_bd_intf_pins axis_frame_generator_fifo_0/S_AXIS] [get_bd_intf_pins frame_generator_0/data_out]
  connect_bd_intf_net -intf_net network_stack_udp_out [get_bd_intf_pins axis_frame_generator_fifo_1/S_AXIS] [get_bd_intf_pins network_stack/udp_out]
  connect_bd_intf_net -intf_net s_axi_control_1 [get_bd_intf_pins s_axi_control] [get_bd_intf_pins smartconnect_0/S00_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M00_AXI [get_bd_intf_pins network_stack/s_axi_cfg] [get_bd_intf_pins smartconnect_0/M00_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M01_AXI [get_bd_intf_pins frame_generator_0/s_axi_control] [get_bd_intf_pins smartconnect_0/M01_AXI]

  # Create port connections
  connect_bd_net -net ap_rst_n_1 [get_bd_pins ap_rst_n] [get_bd_pins frame_generator_0/ap_rst_n] [get_bd_pins network_stack/ap_rst_n] [get_bd_pins smartconnect_0/aresetn]
  connect_bd_net -net axi_clk_1 [get_bd_pins axi_clk] [get_bd_pins axis_frame_generator_fifo_0/s_axis_aclk] [get_bd_pins axis_frame_generator_fifo_1/s_axis_aclk] [get_bd_pins axis_register_slice_0/aclk] [get_bd_pins frame_generator_0/ap_clk] [get_bd_pins network_stack/axiclk] [get_bd_pins smartconnect_0/aclk]
  connect_bd_net -net axi_rst_n_1 [get_bd_pins axi_rst_n] [get_bd_pins axis_frame_generator_fifo_0/s_axis_aresetn] [get_bd_pins axis_frame_generator_fifo_1/s_axis_aresetn] [get_bd_pins axis_register_slice_0/aresetn] [get_bd_pins network_stack/resetn]
  connect_bd_net -net axis_frame_generator_fifo_0_almost_empty [get_bd_pins almost_empty] [get_bd_pins axis_frame_generator_fifo_0/almost_empty]
  connect_bd_net -net axis_frame_generator_fifo_0_almost_full [get_bd_pins almost_full] [get_bd_pins axis_frame_generator_fifo_0/almost_full]
  connect_bd_net -net one_1 [get_bd_pins network_stack/eth_out_tready] [get_bd_pins one/dout] [get_bd_pins network_stack/stat_rx_status]
  connect_bd_net -net hbm_size_bytes_1 [get_bd_pins hbm_size_bytes] [get_bd_pins frame_generator_0/hbm_size_bytes]
  connect_bd_net -net in_cancel_1 [get_bd_pins in_cancel] [get_bd_pins frame_generator_0/in_cancel]
  connect_bd_net -net src_ipv4_addr_1 [get_bd_pins src_ipv4_addr] [get_bd_pins frame_generator_0/src_ipv4_addr]
  connect_bd_net -net src_mac_addr_1 [get_bd_pins src_mac_addr] [get_bd_pins frame_generator_0/src_mac_addr]

  set_property -dict [list CONFIG.DIRECT {1}] [get_bd_cells network_stack/network_config_0]

  # Restore current instance
  current_bd_instance $oldCurInst
}
