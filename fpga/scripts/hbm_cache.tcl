# SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
# SPDX-License-Identifier: CERN-OHL-S-2.0

# Hierarchical cell: hbm_cache
proc create_hier_cell_hbm_cache { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2092 -severity "ERROR" "create_hier_cell_hbm_cache() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 addr_in

  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:axis_rtl:1.0 data_in

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 data_out

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p0

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p1

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p2

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p3

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p4

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:aximm_rtl:1.0 m_axi_d_hbm_p5

  create_bd_intf_pin -mode Master -vlnv xilinx.com:interface:axis_rtl:1.0 m_axis_completion


  # Create pins
  create_bd_pin -dir I -type rst ap_rst_n
  create_bd_pin -dir I -type clk axi_clk
  create_bd_pin -dir I -type rst axi_rst_n
  create_bd_pin -dir O compl_fifo_empty
  create_bd_pin -dir O compl_fifo_full
  create_bd_pin -dir O hbm_handle_fifo_empty
  create_bd_pin -dir O hbm_handle_fifo_full
  create_bd_pin -dir I -from 31 -to 0 -type data hbm_size_bytes
  create_bd_pin -dir O load_from_hbm_idle
  create_bd_pin -dir O save_to_hbm_idle

  # Create instance: axi_datamover_0, and set properties
  set axi_datamover_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_datamover:5.1 axi_datamover_0 ]
  set_property -dict [ list \
   CONFIG.c_addr_width {64} \
   CONFIG.c_dummy {1} \
   CONFIG.c_enable_mm2s {1} \
   CONFIG.c_include_mm2s {Full} \
   CONFIG.c_include_mm2s_stsfifo {true} \
   CONFIG.c_m_axi_mm2s_data_width {256} \
   CONFIG.c_m_axi_mm2s_id_width {6} \
   CONFIG.c_m_axi_s2mm_id_width {6} \
   CONFIG.c_m_axis_mm2s_tdata_width {256} \
   CONFIG.c_mm2s_btt_used {23} \
   CONFIG.c_mm2s_burst_size {16} \
   CONFIG.c_mm2s_include_sf {true} \
   CONFIG.c_s2mm_btt_used {23} \
   CONFIG.c_single_interface {0} \
 ] $axi_datamover_0

  # Create instance: axi_datamover_1, and set properties
  set axi_datamover_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_datamover:5.1 axi_datamover_1 ]
  set_property -dict [ list \
   CONFIG.c_addr_width {64} \
   CONFIG.c_dummy {1} \
   CONFIG.c_enable_mm2s {1} \
   CONFIG.c_include_mm2s {Full} \
   CONFIG.c_include_mm2s_stsfifo {true} \
   CONFIG.c_m_axi_mm2s_data_width {256} \
   CONFIG.c_m_axi_mm2s_id_width {6} \
   CONFIG.c_m_axi_s2mm_id_width {6} \
   CONFIG.c_m_axis_mm2s_tdata_width {256} \
   CONFIG.c_mm2s_btt_used {23} \
   CONFIG.c_mm2s_burst_size {16} \
   CONFIG.c_mm2s_include_sf {true} \
   CONFIG.c_s2mm_btt_used {23} \
   CONFIG.c_single_interface {0} \
 ] $axi_datamover_1

  # Create instance: axi_datamover_2, and set properties
  set axi_datamover_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_datamover:5.1 axi_datamover_2 ]
  set_property -dict [ list \
   CONFIG.c_addr_width {64} \
   CONFIG.c_dummy {1} \
   CONFIG.c_enable_mm2s {1} \
   CONFIG.c_include_mm2s {Full} \
   CONFIG.c_include_mm2s_stsfifo {true} \
   CONFIG.c_m_axi_mm2s_data_width {256} \
   CONFIG.c_m_axi_mm2s_id_width {6} \
   CONFIG.c_m_axi_s2mm_id_width {6} \
   CONFIG.c_m_axis_mm2s_tdata_width {256} \
   CONFIG.c_mm2s_btt_used {23} \
   CONFIG.c_mm2s_burst_size {16} \
   CONFIG.c_mm2s_include_sf {true} \
   CONFIG.c_s2mm_btt_used {23} \
   CONFIG.c_single_interface {0} \
 ] $axi_datamover_2

  # Create instance: axis_compl_fifo_0, and set properties
  set axis_compl_fifo_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_compl_fifo_0 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {2048} \
   CONFIG.FIFO_MEMORY_TYPE {ultra} \
   CONFIG.HAS_AEMPTY {0} \
   CONFIG.HAS_AFULL {0} \
 ] $axis_compl_fifo_0

  # Create instance: axis_compl_fifo_1, and set properties
  set axis_compl_fifo_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_compl_fifo_1 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {256} \
   CONFIG.FIFO_MEMORY_TYPE {auto} \
   CONFIG.HAS_AEMPTY {1} \
   CONFIG.HAS_AFULL {1} \
 ] $axis_compl_fifo_1

  # Create instance: axis_data_fifo_2, and set properties
  set axis_data_fifo_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_2 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
 ] $axis_data_fifo_2

  # Create instance: axis_data_fifo_3, and set properties
  set axis_data_fifo_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_3 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
 ] $axis_data_fifo_3

  # Create instance: axis_data_fifo_4, and set properties
  set axis_data_fifo_4 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_data_fifo_4 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
 ] $axis_data_fifo_4

  # Create instance: axis_datamover_cmd_fifo_0, and set properties
  set axis_datamover_cmd_fifo_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_datamover_cmd_fifo_0 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
 ] $axis_datamover_cmd_fifo_0

  # Create instance: axis_datamover_cmd_fifo_1, and set properties
  set axis_datamover_cmd_fifo_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_datamover_cmd_fifo_1 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
 ] $axis_datamover_cmd_fifo_1

  # Create instance: axis_datamover_cmd_fifo_2, and set properties
  set axis_datamover_cmd_fifo_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_datamover_cmd_fifo_2 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
 ] $axis_datamover_cmd_fifo_2

  # Create instance: axis_datamover_cmd_fifo_3, and set properties
  set axis_datamover_cmd_fifo_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_datamover_cmd_fifo_3 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
 ] $axis_datamover_cmd_fifo_3

  # Create instance: axis_datamover_cmd_fifo_4, and set properties
  set axis_datamover_cmd_fifo_4 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_datamover_cmd_fifo_4 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
 ] $axis_datamover_cmd_fifo_4

  # Create instance: axis_datamover_cmd_fifo_5, and set properties
  set axis_datamover_cmd_fifo_5 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_datamover_cmd_fifo_5 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {16} \
 ] $axis_datamover_cmd_fifo_5

  # Create instance: axis_datamover_fifo_0, and set properties
  set axis_datamover_fifo_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_datamover_fifo_0 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {2048} \
 ] $axis_datamover_fifo_0

  # Create instance: axis_datamover_fifo_1, and set properties
  set axis_datamover_fifo_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_datamover_fifo_1 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {2048} \
 ] $axis_datamover_fifo_1

  # Create instance: axis_datamover_fifo_2, and set properties
  set axis_datamover_fifo_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_datamover_fifo_2 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {2048} \
 ] $axis_datamover_fifo_2

  # Create instance: axis_datamover_fifo_3, and set properties
  set axis_datamover_fifo_3 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_datamover_fifo_3 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {2048} \
 ] $axis_datamover_fifo_3

  # Create instance: axis_datamover_fifo_4, and set properties
  set axis_datamover_fifo_4 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_datamover_fifo_4 ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {2048} \
 ] $axis_datamover_fifo_4

   # Create instance: axis_datamover_fifo_5, and set properties
   set axis_datamover_fifo_5 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_datamover_fifo_5 ]
   set_property -dict [ list \
    CONFIG.FIFO_DEPTH {2048} \
  ] $axis_datamover_fifo_5

  # Create instance: axis_hbm_handles_fifo, and set properties
  set axis_hbm_handles_fifo [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_data_fifo:2.0 axis_hbm_handles_fifo ]
  set_property -dict [ list \
   CONFIG.FIFO_DEPTH {2048} \
   CONFIG.HAS_AEMPTY {1} \
   CONFIG.HAS_AFULL {1} \
 ] $axis_hbm_handles_fifo

  # Create instance: frame_summation_reor_0, and set properties
  set frame_summation_reor_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:frame_summation_reorder_compl:1.0 frame_summation_reor_0 ]

  # Create instance: load_from_hbm_0, and set properties
  set load_from_hbm_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:load_from_hbm:1.0 load_from_hbm_0 ]

  # Create instance: one, and set properties
  set one [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 one ]

  # Create instance: save_to_hbm_0, and set properties
  set save_to_hbm_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:save_to_hbm:1.0 save_to_hbm_0 ]

  # Create instance: save_to_hbm_data_0, and set properties
  set save_to_hbm_data_0 [ create_bd_cell -type ip -vlnv psi.ch:hls:save_to_hbm_data:1.0 save_to_hbm_data_0 ]

  # Create interface connections
  connect_bd_intf_net -intf_net axi_datamover_0_M_AXI_MM2S_1 [get_bd_intf_pins m_axi_d_hbm_p0] [get_bd_intf_pins axi_datamover_0/M_AXI_MM2S]
  connect_bd_intf_net -intf_net axi_datamover_0_M_AXI_S2MM_1 [get_bd_intf_pins m_axi_d_hbm_p1] [get_bd_intf_pins axi_datamover_0/M_AXI_S2MM]
  connect_bd_intf_net -intf_net axi_datamover_1_M_AXI_MM2S_1 [get_bd_intf_pins m_axi_d_hbm_p2] [get_bd_intf_pins axi_datamover_1/M_AXI_MM2S]
  connect_bd_intf_net -intf_net axi_datamover_1_M_AXI_S2MM_1 [get_bd_intf_pins m_axi_d_hbm_p3] [get_bd_intf_pins axi_datamover_1/M_AXI_S2MM]
  connect_bd_intf_net -intf_net axi_datamover_2_M_AXI_MM2S_1 [get_bd_intf_pins m_axi_d_hbm_p4] [get_bd_intf_pins axi_datamover_2/M_AXI_MM2S]
  connect_bd_intf_net -intf_net axi_datamover_2_M_AXI_S2MM_1 [get_bd_intf_pins m_axi_d_hbm_p5] [get_bd_intf_pins axi_datamover_2/M_AXI_S2MM]
  connect_bd_intf_net -intf_net m_axis_completion_1 [get_bd_intf_pins m_axis_completion] [get_bd_intf_pins load_from_hbm_0/m_axis_completion]
  connect_bd_intf_net -intf_net Conn17 [get_bd_intf_pins addr_in] [get_bd_intf_pins save_to_hbm_0/addr_in]
  connect_bd_intf_net -intf_net Conn18 [get_bd_intf_pins data_in] [get_bd_intf_pins save_to_hbm_data_0/data_in]
  connect_bd_intf_net -intf_net axi_datamover_0_M_AXIS_MM2S [get_bd_intf_pins axi_datamover_0/M_AXIS_MM2S] [get_bd_intf_pins axis_datamover_fifo_3/S_AXIS]
  connect_bd_intf_net -intf_net axi_datamover_1_M_AXIS_MM2S [get_bd_intf_pins axi_datamover_1/M_AXIS_MM2S] [get_bd_intf_pins axis_datamover_fifo_4/S_AXIS]
  connect_bd_intf_net -intf_net axi_datamover_2_M_AXIS_MM2S [get_bd_intf_pins axi_datamover_2/M_AXIS_MM2S] [get_bd_intf_pins axis_datamover_fifo_5/S_AXIS]
  connect_bd_intf_net -intf_net axis_compl_fifo_0_M_AXIS [get_bd_intf_pins axis_compl_fifo_0/M_AXIS] [get_bd_intf_pins frame_summation_reor_0/s_axis_completion]
  connect_bd_intf_net -intf_net axis_compl_fifo_1_M_AXIS [get_bd_intf_pins axis_compl_fifo_1/M_AXIS] [get_bd_intf_pins load_from_hbm_0/s_axis_completion]
  connect_bd_intf_net -intf_net axis_data_fifo_2_M_AXIS [get_bd_intf_pins axis_data_fifo_2/M_AXIS] [get_bd_intf_pins frame_summation_reor_0/data_in]
  connect_bd_intf_net -intf_net axis_data_fifo_3_M_AXIS [get_bd_intf_pins axis_data_fifo_3/M_AXIS] [get_bd_intf_pins load_from_hbm_0/data_in]
  connect_bd_intf_net -intf_net axis_data_fifo_4_M_AXIS [get_bd_intf_pins axis_data_fifo_4/M_AXIS] [get_bd_intf_pins data_out]
  connect_bd_intf_net -intf_net axis_datamover_cmd_fifo_3_M_AXIS [get_bd_intf_pins axi_datamover_0/S_AXIS_MM2S_CMD] [get_bd_intf_pins axis_datamover_cmd_fifo_3/M_AXIS]
  connect_bd_intf_net -intf_net axis_datamover_cmd_fifo_4_M_AXIS [get_bd_intf_pins axi_datamover_1/S_AXIS_MM2S_CMD] [get_bd_intf_pins axis_datamover_cmd_fifo_4/M_AXIS]
  connect_bd_intf_net -intf_net axis_datamover_cmd_fifo_5_M_AXIS [get_bd_intf_pins axi_datamover_2/S_AXIS_MM2S_CMD] [get_bd_intf_pins axis_datamover_cmd_fifo_5/M_AXIS]
  connect_bd_intf_net -intf_net axis_datamover_fifo_0_M_AXIS [get_bd_intf_pins axi_datamover_0/S_AXIS_S2MM] [get_bd_intf_pins axis_datamover_fifo_0/M_AXIS]
  connect_bd_intf_net -intf_net axis_datamover_fifo_1_M_AXIS [get_bd_intf_pins axi_datamover_1/S_AXIS_S2MM] [get_bd_intf_pins axis_datamover_fifo_1/M_AXIS]
  connect_bd_intf_net -intf_net axis_datamover_fifo_2_M_AXIS [get_bd_intf_pins axi_datamover_2/S_AXIS_S2MM] [get_bd_intf_pins axis_datamover_fifo_2/M_AXIS]
  connect_bd_intf_net -intf_net axis_datamover_cmd_fifo_0_M_AXIS [get_bd_intf_pins axi_datamover_0/S_AXIS_S2MM_CMD] [get_bd_intf_pins axis_datamover_cmd_fifo_0/M_AXIS]
  connect_bd_intf_net -intf_net axis_datamover_cmd_fifo_1_M_AXIS [get_bd_intf_pins axi_datamover_1/S_AXIS_S2MM_CMD] [get_bd_intf_pins axis_datamover_cmd_fifo_1/M_AXIS]
  connect_bd_intf_net -intf_net axis_datamover_cmd_fifo_2_M_AXIS [get_bd_intf_pins axi_datamover_2/S_AXIS_S2MM_CMD] [get_bd_intf_pins axis_datamover_cmd_fifo_2/M_AXIS]
  connect_bd_intf_net -intf_net axis_datamover_fifo_3_M_AXIS1 [get_bd_intf_pins axis_datamover_fifo_3/M_AXIS] [get_bd_intf_pins load_from_hbm_0/hbm_in_0]
  connect_bd_intf_net -intf_net axis_datamover_fifo_4_M_AXIS1 [get_bd_intf_pins axis_datamover_fifo_4/M_AXIS] [get_bd_intf_pins load_from_hbm_0/hbm_in_1]
  connect_bd_intf_net -intf_net axis_datamover_fifo_5_M_AXIS1 [get_bd_intf_pins axis_datamover_fifo_5/M_AXIS] [get_bd_intf_pins load_from_hbm_0/hbm_in_2]
  connect_bd_intf_net -intf_net axis_hbm_handles_fifo_M_AXIS [get_bd_intf_pins axis_hbm_handles_fifo/M_AXIS] [get_bd_intf_pins save_to_hbm_0/s_axis_free_handles]
  connect_bd_intf_net -intf_net frame_summation_reor_0_data_out [get_bd_intf_pins axis_data_fifo_3/S_AXIS] [get_bd_intf_pins frame_summation_reor_0/data_out]
  connect_bd_intf_net -intf_net frame_summation_reor_0_m_axis_completion [get_bd_intf_pins axis_compl_fifo_1/S_AXIS] [get_bd_intf_pins frame_summation_reor_0/m_axis_completion]
  connect_bd_intf_net -intf_net load_from_hbm_0_data_out [get_bd_intf_pins axis_data_fifo_4/S_AXIS] [get_bd_intf_pins load_from_hbm_0/data_out]
  connect_bd_intf_net -intf_net load_from_hbm_0_datamover_0_cmd [get_bd_intf_pins axis_datamover_cmd_fifo_3/S_AXIS] [get_bd_intf_pins load_from_hbm_0/datamover_0_cmd]
  connect_bd_intf_net -intf_net load_from_hbm_0_datamover_1_cmd [get_bd_intf_pins axis_datamover_cmd_fifo_4/S_AXIS] [get_bd_intf_pins load_from_hbm_0/datamover_1_cmd]
  connect_bd_intf_net -intf_net load_from_hbm_0_datamover_2_cmd [get_bd_intf_pins axis_datamover_cmd_fifo_5/S_AXIS] [get_bd_intf_pins load_from_hbm_0/datamover_2_cmd]
  connect_bd_intf_net -intf_net load_from_hbm_0_m_axis_free_handles [get_bd_intf_pins axis_hbm_handles_fifo/S_AXIS] [get_bd_intf_pins load_from_hbm_0/m_axis_free_handles]
  connect_bd_intf_net -intf_net save_to_hbm_0_data_out [get_bd_intf_pins axis_data_fifo_2/S_AXIS] [get_bd_intf_pins save_to_hbm_data_0/data_out]
  connect_bd_intf_net -intf_net save_to_hbm_0_datamover_0_cmd [get_bd_intf_pins axis_datamover_cmd_fifo_0/S_AXIS] [get_bd_intf_pins save_to_hbm_0/datamover_0_cmd]
  connect_bd_intf_net -intf_net save_to_hbm_0_datamover_1_cmd [get_bd_intf_pins axis_datamover_cmd_fifo_1/S_AXIS] [get_bd_intf_pins save_to_hbm_0/datamover_1_cmd]
  connect_bd_intf_net -intf_net save_to_hbm_0_datamover_2_cmd [get_bd_intf_pins axis_datamover_cmd_fifo_2/S_AXIS] [get_bd_intf_pins save_to_hbm_0/datamover_2_cmd]
  connect_bd_intf_net -intf_net save_to_hbm_0_hbm_out_0 [get_bd_intf_pins axis_datamover_fifo_0/S_AXIS] [get_bd_intf_pins save_to_hbm_data_0/hbm_out_0]
  connect_bd_intf_net -intf_net save_to_hbm_0_hbm_out_1 [get_bd_intf_pins axis_datamover_fifo_1/S_AXIS] [get_bd_intf_pins save_to_hbm_data_0/hbm_out_1]
  connect_bd_intf_net -intf_net save_to_hbm_0_hbm_out_2 [get_bd_intf_pins axis_datamover_fifo_2/S_AXIS] [get_bd_intf_pins save_to_hbm_data_0/hbm_out_2]
  connect_bd_intf_net -intf_net save_to_hbm_0_m_axis_completion [get_bd_intf_pins axis_compl_fifo_0/S_AXIS] [get_bd_intf_pins save_to_hbm_0/m_axis_completion]

  # Create port connections
  connect_bd_net -net ap_rst_n_1 [get_bd_pins ap_rst_n] [get_bd_pins axi_datamover_0/m_axi_mm2s_aresetn] [get_bd_pins axi_datamover_0/m_axi_s2mm_aresetn] [get_bd_pins axi_datamover_0/m_axis_mm2s_cmdsts_aresetn] [get_bd_pins axi_datamover_0/m_axis_s2mm_cmdsts_aresetn] [get_bd_pins axi_datamover_1/m_axi_mm2s_aresetn] [get_bd_pins axi_datamover_1/m_axi_s2mm_aresetn] [get_bd_pins axi_datamover_1/m_axis_mm2s_cmdsts_aresetn] [get_bd_pins axi_datamover_1/m_axis_s2mm_cmdsts_aresetn] [get_bd_pins axi_datamover_2/m_axi_mm2s_aresetn] [get_bd_pins axi_datamover_2/m_axi_s2mm_aresetn] [get_bd_pins axi_datamover_2/m_axis_mm2s_cmdsts_aresetn] [get_bd_pins axi_datamover_2/m_axis_s2mm_cmdsts_aresetn] [get_bd_pins frame_summation_reor_0/ap_rst_n] [get_bd_pins load_from_hbm_0/ap_rst_n] [get_bd_pins save_to_hbm_0/ap_rst_n] [get_bd_pins save_to_hbm_data_0/ap_rst_n]
  connect_bd_net -net axi_clk_1 [get_bd_pins axi_clk] [get_bd_pins axi_datamover_0/m_axi_mm2s_aclk] [get_bd_pins axi_datamover_0/m_axi_s2mm_aclk] [get_bd_pins axi_datamover_0/m_axis_mm2s_cmdsts_aclk] [get_bd_pins axi_datamover_0/m_axis_s2mm_cmdsts_awclk] [get_bd_pins axi_datamover_1/m_axi_mm2s_aclk] [get_bd_pins axi_datamover_1/m_axi_s2mm_aclk] [get_bd_pins axi_datamover_1/m_axis_mm2s_cmdsts_aclk] [get_bd_pins axi_datamover_1/m_axis_s2mm_cmdsts_awclk] [get_bd_pins axi_datamover_2/m_axi_mm2s_aclk] [get_bd_pins axi_datamover_2/m_axi_s2mm_aclk] [get_bd_pins axi_datamover_2/m_axis_mm2s_cmdsts_aclk] [get_bd_pins axi_datamover_2/m_axis_s2mm_cmdsts_awclk] [get_bd_pins axis_compl_fifo_0/s_axis_aclk] [get_bd_pins axis_compl_fifo_1/s_axis_aclk] [get_bd_pins axis_data_fifo_2/s_axis_aclk] [get_bd_pins axis_data_fifo_3/s_axis_aclk] [get_bd_pins axis_data_fifo_4/s_axis_aclk] [get_bd_pins axis_datamover_cmd_fifo_0/s_axis_aclk] [get_bd_pins axis_datamover_cmd_fifo_1/s_axis_aclk] [get_bd_pins axis_datamover_cmd_fifo_2/s_axis_aclk] [get_bd_pins axis_datamover_cmd_fifo_3/s_axis_aclk] [get_bd_pins axis_datamover_cmd_fifo_4/s_axis_aclk] [get_bd_pins axis_datamover_cmd_fifo_5/s_axis_aclk] [get_bd_pins axis_datamover_fifo_0/s_axis_aclk] [get_bd_pins axis_datamover_fifo_1/s_axis_aclk] [get_bd_pins axis_datamover_fifo_2/s_axis_aclk] [get_bd_pins axis_datamover_fifo_3/s_axis_aclk] [get_bd_pins axis_datamover_fifo_4/s_axis_aclk] [get_bd_pins axis_datamover_fifo_5/s_axis_aclk] [get_bd_pins axis_hbm_handles_fifo/s_axis_aclk] [get_bd_pins frame_summation_reor_0/ap_clk] [get_bd_pins load_from_hbm_0/ap_clk] [get_bd_pins save_to_hbm_0/ap_clk] [get_bd_pins save_to_hbm_data_0/ap_clk]
  connect_bd_net -net axi_rst_n_1 [get_bd_pins axi_rst_n] [get_bd_pins axis_compl_fifo_0/s_axis_aresetn] [get_bd_pins axis_compl_fifo_1/s_axis_aresetn] [get_bd_pins axis_data_fifo_2/s_axis_aresetn] [get_bd_pins axis_data_fifo_3/s_axis_aresetn] [get_bd_pins axis_data_fifo_4/s_axis_aresetn] [get_bd_pins axis_datamover_cmd_fifo_0/s_axis_aresetn] [get_bd_pins axis_datamover_cmd_fifo_1/s_axis_aresetn] [get_bd_pins axis_datamover_cmd_fifo_2/s_axis_aresetn] [get_bd_pins axis_datamover_cmd_fifo_3/s_axis_aresetn] [get_bd_pins axis_datamover_cmd_fifo_4/s_axis_aresetn] [get_bd_pins axis_datamover_cmd_fifo_5/s_axis_aresetn] [get_bd_pins axis_datamover_fifo_0/s_axis_aresetn] [get_bd_pins axis_datamover_fifo_1/s_axis_aresetn] [get_bd_pins axis_datamover_fifo_2/s_axis_aresetn] [get_bd_pins axis_datamover_fifo_3/s_axis_aresetn] [get_bd_pins axis_datamover_fifo_4/s_axis_aresetn] [get_bd_pins axis_datamover_fifo_5/s_axis_aresetn] [get_bd_pins axis_hbm_handles_fifo/s_axis_aresetn]
  connect_bd_net -net axis_compl_fifo_1_almost_empty [get_bd_pins compl_fifo_empty] [get_bd_pins axis_compl_fifo_1/almost_empty]
  connect_bd_net -net axis_compl_fifo_1_almost_full [get_bd_pins compl_fifo_full] [get_bd_pins axis_compl_fifo_1/almost_full]
  connect_bd_net -net axis_hbm_handles_fifo_almost_empty [get_bd_pins hbm_handle_fifo_empty] [get_bd_pins axis_hbm_handles_fifo/almost_empty]
  connect_bd_net -net axis_hbm_handles_fifo_almost_full [get_bd_pins hbm_handle_fifo_full] [get_bd_pins axis_hbm_handles_fifo/almost_full]
  connect_bd_net -net hbm_size_bytes_1 [get_bd_pins hbm_size_bytes] [get_bd_pins load_from_hbm_0/hbm_size_bytes] [get_bd_pins save_to_hbm_0/hbm_size_bytes]
  connect_bd_net -net load_from_hbm_0_idle [get_bd_pins load_from_hbm_idle] [get_bd_pins load_from_hbm_0/idle]
  connect_bd_net -net one_dout_1 [get_bd_pins axi_datamover_0/m_axis_mm2s_sts_tready] [get_bd_pins axi_datamover_0/m_axis_s2mm_sts_tready] [get_bd_pins axi_datamover_1/m_axis_mm2s_sts_tready] [get_bd_pins axi_datamover_1/m_axis_s2mm_sts_tready] [get_bd_pins axi_datamover_2/m_axis_mm2s_sts_tready] [get_bd_pins axi_datamover_2/m_axis_s2mm_sts_tready] [get_bd_pins one/dout]
  connect_bd_net -net save_to_hbm_0_idle [get_bd_pins save_to_hbm_idle] [get_bd_pins save_to_hbm_0/idle]

  # Restore current instance
  current_bd_instance $oldCurInst
}
