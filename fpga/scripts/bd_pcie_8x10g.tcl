# SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
# SPDX-License-Identifier: CERN-OHL-S-2.0

################################################################
# This is a generated script based on design: jfjoch_pcie
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2022.2
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   catch {common::send_gid_msg -ssname BD::TCL -id 2041 -severity "ERROR" "This script was generated using Vivado <$scripts_vivado_version> and is being run in <$current_vivado_version> of Vivado. Please run the script in Vivado <$scripts_vivado_version> then open the design in Vivado <$current_vivado_version>. Upgrade the design by running \"Tools => Report => Report IP Status...\", then run write_bd_tcl to create an updated script."}

   return 1
}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source jfjoch_pcie_script.tcl


# The design that will be created by this Tcl script contains the following 
# module references:
# gen_xdma_descriptor, action_config

# Please add the sources of those modules before sourcing this Tcl script.

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xcvu35p-fsvh2104-2-e
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name jfjoch_pcie

# If you do not already have an existing IP Integrator design open,
# you can create a design using the following command:
#    create_bd_design $design_name

# Creating design if needed
set errMsg ""
set nRet 0

set cur_design [current_bd_design -quiet]
set list_cells [get_bd_cells -quiet]

if { ${design_name} eq "" } {
   # USE CASES:
   #    1) Design_name not set

   set errMsg "Please set the variable <design_name> to a non-empty value."
   set nRet 1

} elseif { ${cur_design} ne "" && ${list_cells} eq "" } {
   # USE CASES:
   #    2): Current design opened AND is empty AND names same.
   #    3): Current design opened AND is empty AND names diff; design_name NOT in project.
   #    4): Current design opened AND is empty AND names diff; design_name exists in project.

   if { $cur_design ne $design_name } {
      common::send_gid_msg -ssname BD::TCL -id 2001 -severity "INFO" "Changing value of <design_name> from <$design_name> to <$cur_design> since current design is empty."
      set design_name [get_property NAME $cur_design]
   }
   common::send_gid_msg -ssname BD::TCL -id 2002 -severity "INFO" "Constructing design in IPI design <$cur_design>..."

} elseif { ${cur_design} ne "" && $list_cells ne "" && $cur_design eq $design_name } {
   # USE CASES:
   #    5) Current design opened AND has components AND same names.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 1
} elseif { [get_files -quiet ${design_name}.bd] ne "" } {
   # USE CASES:
   #    6) Current opened design, has components, but diff names, design_name exists in project.
   #    7) No opened design, design_name exists in project.

   set errMsg "Design <$design_name> already exists in your project, please set the variable <design_name> to another value."
   set nRet 2

} else {
   # USE CASES:
   #    8) No opened design, design_name not in project.
   #    9) Current opened design, has components, but diff names, design_name not in project.

   common::send_gid_msg -ssname BD::TCL -id 2003 -severity "INFO" "Currently there is no design <$design_name> in project, so creating one..."

   create_bd_design $design_name

   common::send_gid_msg -ssname BD::TCL -id 2004 -severity "INFO" "Making design <$design_name> as current_bd_design."
   current_bd_design $design_name

}

common::send_gid_msg -ssname BD::TCL -id 2005 -severity "INFO" "Currently the variable <design_name> is equal to \"$design_name\"."

if { $nRet != 0 } {
   catch {common::send_gid_msg -ssname BD::TCL -id 2006 -severity "ERROR" $errMsg}
   return $nRet
}

set bCheckIPsPassed 1
##################################################################
# CHECK IPs
##################################################################
set bCheckIPs 1
if { $bCheckIPs == 1 } {
   set list_check_ips "\
xilinx.com:ip:axi_iic:2.1\
xilinx.com:ip:axi_intc:4.1\
xilinx.com:ip:axi_quad_spi:3.2\
xilinx.com:ip:clk_wiz:6.0\
xilinx.com:ip:cms_subsystem:4.0\
xilinx.com:ip:xlconstant:1.1\
xilinx.com:ip:proc_sys_reset:5.0\
xilinx.com:ip:smartconnect:1.0\
xilinx.com:ip:util_ds_buf:2.2\
xilinx.com:ip:xlconcat:2.1\
xilinx.com:ip:axi_protocol_converter:2.1\
xilinx.com:ip:axi_register_slice:2.1\
xilinx.com:ip:hbm:1.0\
xilinx.com:ip:util_vector_logic:2.0\
xilinx.com:ip:axi_bram_ctrl:4.1\
xilinx.com:ip:axis_data_fifo:2.0\
xilinx.com:ip:axis_register_slice:1.1\
psi.ch:hls:data_collection_fsm:1.0\
xilinx.com:ip:blk_mem_gen:8.4\
psi.ch:hls:frame_generator:1.0\
psi.ch:hls:host_writer:1.0\
psi.ch:hls:load_calibration:1.0\
xilinx.com:ip:mailbox:2.1\
psi.ch:hls:timer_host:1.0\
xilinx.com:ip:axis_dwidth_converter:1.1\
xilinx.com:ip:xxv_ethernet:4.1\
xilinx.com:ip:axi_firewall:1.2\
xilinx.com:ip:axis_clock_converter:1.1\
xilinx.com:ip:xdma:4.1\
xilinx.com:ip:axi_datamover:5.1\
psi.ch:hls:frame_summation_reorder_compl:1.0\
psi.ch:hls:load_from_hbm:1.0\
psi.ch:hls:save_to_hbm:1.0\
psi.ch:hls:save_to_hbm_data:1.0\
psi.ch:hls:adu_histo:1.0\
psi.ch:hls:axis_32_to_512:1.0\
psi.ch:hls:axis_64_to_512:1.0\
psi.ch:hls:eiger_reorder:1.0\
psi.ch:hls:frame_summation:1.0\
psi.ch:hls:integration:1.0\
psi.ch:hls:jf_conversion:1.0\
psi.ch:hls:mask_missing:1.0\
psi.ch:hls:pixel_mask:1.0\
psi.ch:hls:pixel_sqrt:1.0\
psi.ch:hls:pixel_threshold:1.0\
psi.ch:hls:roi_calc:1.0\
psi.ch:hls:spot_finder:1.0\
psi.ch:hls:spot_finder_connectivity:1.0\
psi.ch:hls:spot_finder_mask:1.0\
psi.ch:hls:spot_finder_merge:1.0\
psi.ch:hls:stream768to512:1.0\
psi.ch:hls:arp:1.0\
xilinx.com:ip:axis_switch:1.1\
psi.ch:hls:ethernet:1.0\
psi.ch:hls:icmp:1.0\
psi.ch:hls:ipv4:1.0\
psi.ch:hls:sls_detector:1.0\
psi.ch:hls:udp:1.0\
"

   set list_ips_missing ""
   common::send_msg_id "BD_TCL-006" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

   foreach ip_vlnv $list_check_ips {
      set ip_obj [get_ipdefs -all $ip_vlnv]
      if { $ip_obj eq "" } {
         lappend list_ips_missing $ip_vlnv
      }
   }

   if { $list_ips_missing ne "" } {
      catch {common::send_msg_id "BD_TCL-115" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
      set bCheckIPsPassed 0
   }

}

##################################################################
# CHECK Modules
##################################################################
set bCheckModules 1
if { $bCheckModules == 1 } {
   set list_check_mods "\
action_config\
gen_xdma_descriptor\
eth_4x10g_leds\
"

   set list_mods_missing ""
   common::send_msg_id "BD_TCL-006" "INFO" "Checking if the following modules exist in the project's sources: $list_check_mods ."

   foreach mod_vlnv $list_check_mods {
      if { [can_resolve_reference $mod_vlnv] == 0 } {
         lappend list_mods_missing $mod_vlnv
      }
   }

   if { $list_mods_missing ne "" } {
      catch {common::send_msg_id "BD_TCL-115" "ERROR" "The following module(s) are not found in the project: $list_mods_missing" }
      common::send_msg_id "BD_TCL-008" "INFO" "Please add source files for the missing module(s) above."
      set bCheckIPsPassed 0
   }
}

if { $bCheckIPsPassed != 1 } {
  common::send_msg_id "BD_TCL-1003" "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 3
}

##################################################################
# DESIGN PROCs
##################################################################

# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2090 -severity "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_gid_msg -ssname BD::TCL -id 2091 -severity "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set i2c_si5394 [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:iic_rtl:1.0 i2c_si5394 ]

  set pcie0_mgt [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:pcie_7x_mgt_rtl:1.0 pcie0_mgt ]

  set pcie0_ref [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 pcie0_ref ]

  set qsfp0 [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:gt_rtl:1.0 qsfp0 ]

  set qsfp0_ref [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 qsfp0_ref ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {161132812} \
   ] $qsfp0_ref

  set qsfp1 [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:gt_rtl:1.0 qsfp1 ]

  set qsfp1_ref [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 qsfp1_ref ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {161132812} \
   ] $qsfp1_ref

  set ref100 [ create_bd_intf_port -mode Slave -vlnv xilinx.com:interface:diff_clock_rtl:1.0 ref100 ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {100000000} \
   ] $ref100

  set satellite_uart_0 [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:uart_rtl:1.0 satellite_uart_0 ]


  # Create ports
  set hbm_cattrip [ create_bd_port -dir O -from 0 -to 0 hbm_cattrip ]
  set pcie_perstn [ create_bd_port -dir I -type rst pcie_perstn ]
  set qsfp0_led_busy [ create_bd_port -dir O -from 0 -to 0 qsfp0_led_busy ]
  set qsfp0_led_conn_green [ create_bd_port -dir O qsfp0_led_conn_green ]
  set qsfp0_led_conn_yellow [ create_bd_port -dir O -from 0 -to 0 qsfp0_led_conn_yellow ]
  set qsfp1_led_busy [ create_bd_port -dir O -from 0 -to 0 qsfp1_led_busy ]
  set qsfp1_led_conn_green [ create_bd_port -dir O qsfp1_led_conn_green ]
  set qsfp1_led_conn_yellow [ create_bd_port -dir O qsfp1_led_conn_yellow ]
  set satellite_gpio_0 [ create_bd_port -dir I -from 3 -to 0 -type intr satellite_gpio_0 ]
  set_property -dict [ list \
   CONFIG.PortWidth {4} \
   CONFIG.SENSITIVITY {EDGE_RISING} \
 ] $satellite_gpio_0

  # Create instance: axi_iic_0, and set properties
  set axi_iic_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_iic:2.1 axi_iic_0 ]

  # Create instance: axi_intc_0, and set properties
  set axi_intc_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_intc:4.1 axi_intc_0 ]
  set_property -dict [ list \
   CONFIG.C_IRQ_CONNECTION {1} \
   CONFIG.C_NUM_SW_INTR {2} \
 ] $axi_intc_0

  # Create instance: axi_quad_spi_0, and set properties
  set axi_quad_spi_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_quad_spi:3.2 axi_quad_spi_0 ]
  set_property -dict [ list \
   CONFIG.C_FIFO_DEPTH {256} \
   CONFIG.C_SCK_RATIO {2} \
   CONFIG.C_SPI_MEMORY {2} \
   CONFIG.C_SPI_MODE {2} \
   CONFIG.C_USE_STARTUP {1} \
   CONFIG.C_USE_STARTUP_INT {1} \
 ] $axi_quad_spi_0

  # Create instance: axis_interconnect_0, and set properties
  set axis_interconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axis_interconnect:2.1 axis_interconnect_0 ]
  set_property -dict [ list \
   CONFIG.ARB_ALGORITHM {3} \
   CONFIG.ARB_ON_MAX_XFERS {0} \
   CONFIG.ARB_ON_NUM_CYCLES {4} \
   CONFIG.ARB_ON_TLAST {1} \
   CONFIG.M00_FIFO_DEPTH {256} \
   CONFIG.NUM_MI {1} \
   CONFIG.NUM_SI {2} \
   CONFIG.S00_FIFO_DEPTH {512} \
   CONFIG.S00_FIFO_MODE {1} \
   CONFIG.S00_HAS_REGSLICE {1} \
   CONFIG.S01_FIFO_DEPTH {512} \
   CONFIG.S01_FIFO_MODE {1} \
   CONFIG.S01_HAS_REGSLICE {1} \
 ] $axis_interconnect_0

  # Create instance: clk_wiz_0, and set properties
  set clk_wiz_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz:6.0 clk_wiz_0 ]
  set_property -dict [ list \
   CONFIG.CLKOUT1_JITTER {153.164} \
   CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {50.000} \
   CONFIG.CLKOUT2_JITTER {134.506} \
   CONFIG.CLKOUT2_PHASE_ERROR {154.678} \
   CONFIG.CLKOUT2_USED {true} \
   CONFIG.CLKOUT3_JITTER {119.392} \
   CONFIG.CLKOUT3_PHASE_ERROR {154.678} \
   CONFIG.CLKOUT3_REQUESTED_OUT_FREQ {200.000} \
   CONFIG.CLKOUT3_USED {true} \
   CONFIG.MMCM_CLKOUT0_DIVIDE_F {24.000} \
   CONFIG.MMCM_CLKOUT1_DIVIDE {12} \
   CONFIG.MMCM_CLKOUT2_DIVIDE {6} \
   CONFIG.NUM_OUT_CLKS {3} \
   CONFIG.PRIM_SOURCE {Single_ended_clock_capable_pin} \
   CONFIG.RESET_PORT {reset} \
   CONFIG.RESET_TYPE {ACTIVE_HIGH} \
   CONFIG.USE_LOCKED {false} \
   CONFIG.USE_RESET {false} \
 ] $clk_wiz_0

  # Create instance: cms_subsystem_0, and set properties
  set cms_subsystem_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:cms_subsystem:4.0 cms_subsystem_0 ]

  # Create instance: hbm_infrastructure
  create_hier_cell_hbm_infrastructure [current_bd_instance .] hbm_infrastructure

  # Create instance: jungfraujoch
  create_hier_cell_jungfraujoch [current_bd_instance .] jungfraujoch

  # Create instance: mac_4x10g
  create_hier_cell_mac_4x10g [current_bd_instance .] mac_4x10g

  # Create instance: mac_4x10g_1
  create_hier_cell_mac_4x10g [current_bd_instance .] mac_4x10g_1

  # Create instance: one, and set properties
  set one [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 one ]

  # Create instance: pcie_dma_0
  create_hier_cell_pcie_dma_0 [current_bd_instance .] pcie_dma_0

  # Create instance: proc_sys_reset_pcie_0, and set properties
  set proc_sys_reset_pcie_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 proc_sys_reset_pcie_0 ]
  set_property -dict [ list \
   CONFIG.C_EXT_RST_WIDTH {1} \
 ] $proc_sys_reset_pcie_0

  # Create instance: proc_sys_reset_refclk, and set properties
  set proc_sys_reset_refclk [ create_bd_cell -type ip -vlnv xilinx.com:ip:proc_sys_reset:5.0 proc_sys_reset_refclk ]
  set_property -dict [ list \
   CONFIG.C_EXT_RST_WIDTH {1} \
 ] $proc_sys_reset_refclk

  # Create instance: smartconnect_0, and set properties
  set smartconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:smartconnect:1.0 smartconnect_0 ]
  set_property -dict [ list \
   CONFIG.NUM_CLKS {4} \
   CONFIG.NUM_MI {8} \
   CONFIG.NUM_SI {1} \
 ] $smartconnect_0

  # Create instance: util_ds_buf_0, and set properties
  set util_ds_buf_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf:2.2 util_ds_buf_0 ]

  # Create instance: xlconcat_intc, and set properties
  set xlconcat_intc [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_intc ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {2} \
 ] $xlconcat_intc

  # Create instance: xlconcat_irq, and set properties
  set xlconcat_irq [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat:2.1 xlconcat_irq ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {2} \
 ] $xlconcat_irq

  # Create instance: zero, and set properties
  set zero [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant:1.1 zero ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $zero

  # Create interface connections
  connect_bd_intf_net -intf_net S01_AXIS_1 [get_bd_intf_pins axis_interconnect_0/S01_AXIS] [get_bd_intf_pins mac_4x10g/m_axis]
  connect_bd_intf_net -intf_net axi_iic_0_IIC [get_bd_intf_ports i2c_si5394] [get_bd_intf_pins axi_iic_0/IIC]
  connect_bd_intf_net -intf_net axis_interconnect_0_M00_AXIS [get_bd_intf_pins axis_interconnect_0/M00_AXIS] [get_bd_intf_pins jungfraujoch/udp_in]
  connect_bd_intf_net -intf_net cms_subsystem_0_satellite_uart [get_bd_intf_ports satellite_uart_0] [get_bd_intf_pins cms_subsystem_0/satellite_uart]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p0 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_0] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p0]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p1 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_1] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p1]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p2 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_2] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p2]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p3 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_3] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p3]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p4 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_4] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p4]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p5 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_5] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p5]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p6 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_6] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p6]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p7 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_7] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p7]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p8 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_8] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p8]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p9 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_9] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p9]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p10 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_10] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p10]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p11 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_11] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p11]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p12 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_12] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p12]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p13 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_13] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p13]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p14 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_14] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p14]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p15 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_15] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p15]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p16 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_16] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p16]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p17 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_17] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p17]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p18 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_18] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p18]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p19 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_19] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p19]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p20 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_20] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p20]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p21 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_21] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p21]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p22 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_22] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p22]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p23 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_23] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p23]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p24 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_24] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p24]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p25 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_25] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p25]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p26 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_26] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p26]
  connect_bd_intf_net -intf_net jungfraujoch_m_axi_d_hbm_p27 [get_bd_intf_pins hbm_infrastructure/s_axi_hbm_27] [get_bd_intf_pins jungfraujoch/m_axi_d_hbm_p27]
  connect_bd_intf_net -intf_net jungfraujoch_m_axis_c2h_data [get_bd_intf_pins jungfraujoch/m_axis_c2h_data] [get_bd_intf_pins pcie_dma_0/s_axis_c2h_data]
  connect_bd_intf_net -intf_net jungfraujoch_m_axis_c2h_datamover_cmd [get_bd_intf_pins jungfraujoch/m_axis_c2h_datamover_cmd] [get_bd_intf_pins pcie_dma_0/s_axis_c2h_cmd]
  connect_bd_intf_net -intf_net jungfraujoch_m_axis_h2c_datamover_cmd [get_bd_intf_pins jungfraujoch/m_axis_h2c_datamover_cmd] [get_bd_intf_pins pcie_dma_0/s_axis_h2c_cmd]
  connect_bd_intf_net -intf_net mac_4x10g1_m_axis [get_bd_intf_pins axis_interconnect_0/S00_AXIS] [get_bd_intf_pins mac_4x10g_1/m_axis]
  connect_bd_intf_net -intf_net mac_4x10g1_qsfp1 [get_bd_intf_ports qsfp0] [get_bd_intf_pins mac_4x10g_1/qsfp1]
  connect_bd_intf_net -intf_net mac_4x10g_qsfp1 [get_bd_intf_ports qsfp1] [get_bd_intf_pins mac_4x10g/qsfp1]
  connect_bd_intf_net -intf_net pcie0_ref_1 [get_bd_intf_ports pcie0_ref] [get_bd_intf_pins pcie_dma_0/pcie_refclk]
  connect_bd_intf_net -intf_net pcie_dma_0_M_AXI [get_bd_intf_pins pcie_dma_0/m_axi_ctrl] [get_bd_intf_pins smartconnect_0/S00_AXI]
  connect_bd_intf_net -intf_net pcie_dma_0_pcie0_mgt [get_bd_intf_ports pcie0_mgt] [get_bd_intf_pins pcie_dma_0/pcie_mgt]
  connect_bd_intf_net -intf_net qsfp0_ref_1 [get_bd_intf_ports qsfp0_ref] [get_bd_intf_pins mac_4x10g_1/qsfp_ref]
  connect_bd_intf_net -intf_net qsfp1_ref_1 [get_bd_intf_ports qsfp1_ref] [get_bd_intf_pins mac_4x10g/qsfp_ref]
  connect_bd_intf_net -intf_net ref100_1 [get_bd_intf_ports ref100] [get_bd_intf_pins util_ds_buf_0/CLK_IN_D]
  connect_bd_intf_net -intf_net s_axis_h2c_data_1 [get_bd_intf_pins jungfraujoch/s_axis_h2c_data] [get_bd_intf_pins pcie_dma_0/m_axis_h2c_data]
  connect_bd_intf_net -intf_net smartconnect_0_M00_AXI [get_bd_intf_pins jungfraujoch/s_axi] [get_bd_intf_pins smartconnect_0/M00_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M01_AXI [get_bd_intf_pins cms_subsystem_0/s_axi_ctrl] [get_bd_intf_pins smartconnect_0/M01_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M02_AXI [get_bd_intf_pins mac_4x10g_1/s_axi] [get_bd_intf_pins smartconnect_0/M02_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M03_AXI [get_bd_intf_pins axi_quad_spi_0/AXI_LITE] [get_bd_intf_pins smartconnect_0/M03_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M04_AXI [get_bd_intf_pins axi_intc_0/s_axi] [get_bd_intf_pins smartconnect_0/M04_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M05_AXI [get_bd_intf_pins pcie_dma_0/s_axi_dma_ctrl] [get_bd_intf_pins smartconnect_0/M05_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M06_AXI [get_bd_intf_pins mac_4x10g/s_axi] [get_bd_intf_pins smartconnect_0/M06_AXI]
  connect_bd_intf_net -intf_net smartconnect_0_M07_AXI [get_bd_intf_pins axi_iic_0/S_AXI] [get_bd_intf_pins smartconnect_0/M07_AXI]

  # Create port connections
  connect_bd_net -net axi_clk_1 [get_bd_pins clk_wiz_0/clk_in1] [get_bd_pins pcie_dma_0/axi_aclk] [get_bd_pins proc_sys_reset_pcie_0/slowest_sync_clk] [get_bd_pins smartconnect_0/aclk3]
  connect_bd_net -net axi_intc_0_irq [get_bd_pins axi_intc_0/irq] [get_bd_pins xlconcat_irq/In0]
  connect_bd_net -net axi_quad_spi_0_ip2intc_irpt [get_bd_pins axi_quad_spi_0/ip2intc_irpt] [get_bd_pins xlconcat_intc/In0]
  connect_bd_net -net cms_subsystem_0_interrupt_host [get_bd_pins cms_subsystem_0/interrupt_host] [get_bd_pins xlconcat_intc/In1]
  connect_bd_net -net hbm_infrastructure_hbm_temp_trip_1 [get_bd_ports hbm_cattrip] [get_bd_pins cms_subsystem_0/interrupt_hbm_cattrip] [get_bd_pins hbm_infrastructure/hbm_cattrip]
  connect_bd_net -net hbm_infrastructure_hbm_temperature_0 [get_bd_pins cms_subsystem_0/hbm_temp_1] [get_bd_pins hbm_infrastructure/hbm_temperature_0]
  connect_bd_net -net hbm_infrastructure_hbm_temperature_1 [get_bd_pins cms_subsystem_0/hbm_temp_2] [get_bd_pins hbm_infrastructure/hbm_temperature_1]
  connect_bd_net -net jungfraujoch_Interrupt_0 [get_bd_pins jungfraujoch/Interrupt_0] [get_bd_pins xlconcat_irq/In1]
  connect_bd_net -net mac_4x10g1_led_green [get_bd_ports qsfp0_led_conn_green] [get_bd_pins mac_4x10g_1/led_green]
  connect_bd_net -net mac_4x10g1_led_yellow [get_bd_ports qsfp0_led_conn_yellow] [get_bd_pins mac_4x10g_1/led_yellow]
  connect_bd_net -net mac_4x10g_led_green [get_bd_ports qsfp1_led_conn_green] [get_bd_pins mac_4x10g/led_green]
  connect_bd_net -net mac_4x10g_led_yellow [get_bd_ports qsfp1_led_conn_yellow] [get_bd_pins mac_4x10g/led_yellow]
  connect_bd_net -net net_refclk50 [get_bd_pins axi_iic_0/s_axi_aclk] [get_bd_pins axi_intc_0/s_axi_aclk] [get_bd_pins axi_quad_spi_0/s_axi_aclk] [get_bd_pins clk_wiz_0/clk_out1] [get_bd_pins cms_subsystem_0/aclk_ctrl] [get_bd_pins proc_sys_reset_refclk/slowest_sync_clk] [get_bd_pins smartconnect_0/aclk2]
  connect_bd_net -net net_refclk100 [get_bd_pins axi_quad_spi_0/ext_spi_clk] [get_bd_pins clk_wiz_0/clk_out2] [get_bd_pins hbm_infrastructure/refclk100] [get_bd_pins smartconnect_0/aclk1] [get_bd_pins mac_4x10g/refclk100] [get_bd_pins mac_4x10g_1/refclk100]
  connect_bd_net -net net_refclk200 [get_bd_pins axis_interconnect_0/ACLK] [get_bd_pins axis_interconnect_0/M00_AXIS_ACLK] [get_bd_pins axis_interconnect_0/S00_AXIS_ACLK] [get_bd_pins axis_interconnect_0/S01_AXIS_ACLK] [get_bd_pins clk_wiz_0/clk_out3] [get_bd_pins hbm_infrastructure/axi_clk] [get_bd_pins jungfraujoch/axi_clk] [get_bd_pins mac_4x10g/refclk200] [get_bd_pins mac_4x10g_1/refclk200] [get_bd_pins pcie_dma_0/refclk200] [get_bd_pins smartconnect_0/aclk]
  connect_bd_net -net one_dout [get_bd_pins cms_subsystem_0/aresetn_ctrl] [get_bd_pins one/dout] [get_bd_pins proc_sys_reset_pcie_0/dcm_locked] [get_bd_pins proc_sys_reset_refclk/dcm_locked]
  connect_bd_net -net pcie_dma_0_axi_aresetn [get_bd_pins pcie_dma_0/axi_aresetn] [get_bd_pins proc_sys_reset_pcie_0/ext_reset_in] [get_bd_pins proc_sys_reset_refclk/ext_reset_in] [get_bd_pins smartconnect_0/aresetn]
  connect_bd_net -net pcie_perstn_1 [get_bd_ports pcie_perstn] [get_bd_pins pcie_dma_0/pcie_perstn]
  connect_bd_net -net proc_sys_reset_pcie_0_interconnect_aresetn [get_bd_pins pcie_dma_0/axi_clk_resetn] [get_bd_pins proc_sys_reset_pcie_0/interconnect_aresetn]
  connect_bd_net -net proc_sys_reset_refclk_peripheral_aresetn [get_bd_pins axi_iic_0/s_axi_aresetn] [get_bd_pins axi_intc_0/s_axi_aresetn] [get_bd_pins axi_quad_spi_0/s_axi_aresetn] [get_bd_pins axis_interconnect_0/ARESETN] [get_bd_pins axis_interconnect_0/M00_AXIS_ARESETN] [get_bd_pins axis_interconnect_0/S00_AXIS_ARESETN] [get_bd_pins axis_interconnect_0/S01_AXIS_ARESETN] [get_bd_pins hbm_infrastructure/axi_resetn] [get_bd_pins jungfraujoch/ap_rst_n] [get_bd_pins mac_4x10g/axi_aresetn] [get_bd_pins mac_4x10g_1/axi_aresetn] [get_bd_pins proc_sys_reset_refclk/peripheral_aresetn]
  connect_bd_net -net refclk100_1 [get_bd_pins hbm_infrastructure/hbmclk100] [get_bd_pins util_ds_buf_0/IBUF_OUT]
  connect_bd_net -net resetn_1 [get_bd_pins jungfraujoch/axi_rst_n] [get_bd_pins pcie_dma_0/refclk200_resetn] [get_bd_pins proc_sys_reset_refclk/interconnect_aresetn]
  connect_bd_net -net satellite_gpio_0_1 [get_bd_ports satellite_gpio_0] [get_bd_pins cms_subsystem_0/satellite_gpio]
  connect_bd_net -net xlconcat_0_dout [get_bd_pins pcie_dma_0/usr_irq_req] [get_bd_pins xlconcat_irq/dout]
  connect_bd_net -net xlconcat_irq_dout [get_bd_pins axi_intc_0/intr] [get_bd_pins xlconcat_intc/dout]
  connect_bd_net -net zero_dout [get_bd_ports qsfp0_led_busy] [get_bd_ports qsfp1_led_busy] [get_bd_pins axi_quad_spi_0/usrcclkts] [get_bd_pins zero/dout]

  # Create address segments
  assign_bd_address -offset 0x00010000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs jungfraujoch/action_config_0/s_axi/reg0] -force
  assign_bd_address -offset 0x00030000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs jungfraujoch/mailbox_0/S0_AXI/Reg] -force
  assign_bd_address -offset 0x00040000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs axi_quad_spi_0/AXI_LITE/Reg] -force
  assign_bd_address -offset 0x00050000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs axi_intc_0/S_AXI/Reg] -force
  assign_bd_address -offset 0x00060000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs jungfraujoch/load_calibration_0/s_axi_control/Reg] -force
  assign_bd_address -offset 0x00070000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs pcie_dma_0/axi_firewall_0/S_AXI_CTL/Control] -force
  assign_bd_address -offset 0x00080000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs jungfraujoch/frame_generator/frame_generator_0/s_axi_control/Reg] -force
  assign_bd_address -offset 0x002F0000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs jungfraujoch/frame_generator/network_stack/network_config_0/s_axi/reg0] -force
  assign_bd_address -offset 0x00090000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs pcie_dma_0/xdma_0/S_AXI_LITE/CTL0] -force
  assign_bd_address -offset 0x000C0000 -range 0x00040000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs cms_subsystem_0/s_axi_ctrl/Mem] -force
  assign_bd_address -offset 0x000A0000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs axi_iic_0/S_AXI/Reg] -force
  assign_bd_address -offset 0x00100000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs mac_4x10g/xxv_ethernet_0/s_axi_0/Reg] -force
  assign_bd_address -offset 0x00110000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs mac_4x10g/xxv_ethernet_0/s_axi_1/Reg] -force
  assign_bd_address -offset 0x00120000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs mac_4x10g/xxv_ethernet_0/s_axi_2/Reg] -force
  assign_bd_address -offset 0x00130000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs mac_4x10g/xxv_ethernet_0/s_axi_3/Reg] -force
  assign_bd_address -offset 0x00140000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs mac_4x10g_1/xxv_ethernet_0/s_axi_0/Reg] -force
  assign_bd_address -offset 0x00150000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs mac_4x10g_1/xxv_ethernet_0/s_axi_1/Reg] -force
  assign_bd_address -offset 0x00160000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs mac_4x10g_1/xxv_ethernet_0/s_axi_2/Reg] -force
  assign_bd_address -offset 0x00170000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs mac_4x10g_1/xxv_ethernet_0/s_axi_3/Reg] -force
  assign_bd_address -offset 0x00200000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs mac_4x10g/packet_proc_0/network_stack/network_config_0/s_axi/reg0] -force
  assign_bd_address -offset 0x00210000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs mac_4x10g/packet_proc_1/network_stack/network_config_0/s_axi/reg0] -force
  assign_bd_address -offset 0x00220000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs mac_4x10g/packet_proc_2/network_stack/network_config_0/s_axi/reg0] -force
  assign_bd_address -offset 0x00230000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs mac_4x10g/packet_proc_3/network_stack/network_config_0/s_axi/reg0] -force
  assign_bd_address -offset 0x00240000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs mac_4x10g_1/packet_proc_0/network_stack/network_config_0/s_axi/reg0] -force
  assign_bd_address -offset 0x00250000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs mac_4x10g_1/packet_proc_1/network_stack/network_config_0/s_axi/reg0] -force
  assign_bd_address -offset 0x00260000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs mac_4x10g_1/packet_proc_2/network_stack/network_config_0/s_axi/reg0] -force
  assign_bd_address -offset 0x00270000 -range 0x00010000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs mac_4x10g_1/packet_proc_3/network_stack/network_config_0/s_axi/reg0] -force
  assign_bd_address -offset 0x00400000 -range 0x00080000 -target_address_space [get_bd_addr_spaces pcie_dma_0/xdma_0/M_AXI_LITE] [get_bd_addr_segs jungfraujoch/axi_bram_ctrl_calibration_addr_0/S_AXI/Mem0] -force
  assign_bd_address -offset 0x00000000 -range 0x00080000 -target_address_space [get_bd_addr_spaces jungfraujoch/load_calibration_0/Data_m_axi_handle_to_dma_address] [get_bd_addr_segs jungfraujoch/axi_bram_ctrl_calibration_addr_1/S_AXI/Mem0] -force
  assign_bd_address -offset 0x00000000 -range 0x00200000 -target_address_space [get_bd_addr_spaces jungfraujoch/image_processing/pixel_mask_0/Data_m_axi_pixel_mask] [get_bd_addr_segs jungfraujoch/axi_bram_ctrl_pixel_mask_0/S_AXI/Mem0] -force
  assign_bd_address -offset 0x00000000 -range 0x00200000 -target_address_space [get_bd_addr_spaces jungfraujoch/load_calibration_0/Data_m_axi_pixel_mask] [get_bd_addr_segs jungfraujoch/axi_bram_ctrl_pixel_mask_0/S_AXI/Mem0] -force
  assign_bd_address

  set_property -dict [list CONFIG.GT_GROUP_SELECT {Quad_X0Y6} CONFIG.LANE1_GT_LOC {X0Y24} CONFIG.LANE2_GT_LOC {X0Y25} CONFIG.LANE3_GT_LOC {X0Y26} CONFIG.LANE4_GT_LOC {X0Y27}] [get_bd_cells mac_4x10g_1/xxv_ethernet_0]

  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
  validate_bd_design
  save_bd_design
}
# End of create_root_design()

##################################################################
# MAIN FLOW
##################################################################

create_root_design ""
