#!/bin/bash

HLS_IP_DIR=$PWD/action/ip

rm -rf action

# Create directories
mkdir -p action/hdl
mkdir -p action/ip
mkdir -p action/scripts

for i in hls/psi_ch*.zip
do
  x=${i##hls/psi_ch_}
  y=${x%%.zip}
  unzip -qq -d action/ip/$y $i
done

cp ${SRC_DIR}/hdl/*.v action/hdl

# Update action type and release level based on jfjoch_fpga.h
REVISION=`grep "#define JFJOCH_FPGA_REVISION" ${SRC_DIR}/pcie_driver/jfjoch_fpga.h | awk '{print $3}'`

MAX_MODULES_FPGA=`grep "#define MAX_MODULES_FPGA" ${SRC_DIR}/pcie_driver/jfjoch_fpga.h  |tr -s " " |cut -f3 -d" "`

git describe --match=NeVeRmAtCh --always --abbrev=8
if [ $? -eq 0 ]; then
  GIT_SHA1=`git describe --match=NeVeRmAtCh --always --abbrev=8`
else
  GIT_SHA1=0
fi

SRC="define JFJOCH_VARIANT 32'h.*"
DST="define JFJOCH_VARIANT 32'h${VARIANT}"
sed -i "s/$SRC/$DST/" action/hdl/action_config.v

SRC="define GIT_SHA1 32'h.*"
DST="define GIT_SHA1 32'h${GIT_SHA1}"
sed -i "s/$SRC/$DST/" action/hdl/action_config.v

TIME_EPOCH=`date +%s`

SRC="define JFJOCH_SYNTH_TIME 32'd*"
DST="define JFJOCH_SYNTH_TIME 32'd${TIME_EPOCH}"
sed -i "s/$SRC/$DST/" action/hdl/action_config.v

SRC="define MAX_MODULES_FPGA 32'd.*"
DST="define MAX_MODULES_FPGA 32'd${MAX_MODULES_FPGA}"
sed -i "s/$SRC/$DST/" action/hdl/action_config.v

cat <<EOF > action/scripts/pcie_release.tcl
set pcie_subsystem_id ${VARIANT}
set pcie_revision ${REVISION}
EOF
