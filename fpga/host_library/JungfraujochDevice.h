// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_JUNGFRAUJOCHDEVICE_H
#define JUNGFRAUJOCH_JUNGFRAUJOCHDEVICE_H

#include <string>
#include "../pcie_driver/jfjoch_fpga.h"

struct JungfraujochDeviceCompletion {
    uint16_t data_collection_id;
    uint16_t buffer_id;
};

class JungfraujochDevice {
    int fd;
public:
    JungfraujochDevice(const std::string& device_name, bool write_access);
    ~JungfraujochDevice();

    // Starts data collection
    void Start();
    // Cancels data collection
    void Cancel();
    // Cancels data collection + disables DMA
    void End();
    // Checks if action is idle
    bool IsIdle() const;

    // Gets current status
    DataCollectionStatus GetDataCollectionStatus() const;
    // Gets current env. parameters
    DeviceStatus GetDeviceStatus() const;

    // Clears network counters
    void ClearNetworkCounters();

    // Sets configuration
    void SetConfig(const DataCollectionConfig &config);
    // Get configuration
    DataCollectionConfig GetConfig() const;

    // Resets FPGA - not safe at the moment
    void Reset();

    // Returns current NUMA node
    uint32_t GetNumaNode() const;

    // Read work completion
    // returns true if there was completion in the mailbox - output is then saved
    // returns false if mailbox was empty - output is invalid
    // More user-friendly function, as it returns data structure
    bool ReadWorkCompletion(JungfraujochDeviceCompletion &completion);

    // Read work completion
    // returns true if there was completion in the mailbox - output is then saved
    // returns false if mailbox was empty - output is invalid
    // output is coded as single 32-bit integer:
    // bits (0..15) = buffer location that has completed data
    // bits (31..16) = data_collection_id
    bool ReadWorkCompletion(uint32_t *output);

    // Sends work request of buffer location id
    // returns true if there was space in the mailbox
    // returns false if mailbox was full
    bool SendWorkRequest(uint32_t id);

    // Returns MAC address in network order
    uint64_t GetMACAddress(uint32_t interface = 0) const;
    // Returns IPv4 address in network order
    uint32_t GetIPv4Address(uint32_t interface = 0) const;
    // Set IPv4 address
    // input has to be in network order
    void SetIPv4Address(uint32_t input, uint32_t interface = 0);

    // Start internal frame generator
    // Doesn't wait for the result
    void RunFrameGenerator(const FrameGeneratorConfig &config);

    // Read any FPGA register
    // Can only be done by root user
    uint32_t ReadRegister(uint32_t addr) const;

    // Load calibration parameters
    // Function is synchronous - it will return when loading is done
    void LoadCalibration(const LoadCalibrationConfig &config);

    void SetSpotFinderParameters(const SpotFinderParameters &params);
    SpotFinderParameters GetSpotFinderParameters();

    // Get number of kernel buffers
    uint32_t GetBufferCount() const;
    // Map kernel buffer ( id must be less than GetBufferCount() ) to user virtual memory space
    DeviceOutput *MapKernelBuffer(uint32_t buffer_id);

    // Unmap kernel buffer, using the pointer returned by MapKernelBuffer()
    void UnmapKernelBuffer(DeviceOutput* val);

    uint32_t GetRevision();

    NetworkStatus GetNetworkStatus(uint32_t interface);
    uint32_t GetNetworkMode(uint32_t interface);
    void SetNetworkMode(uint32_t mode, uint32_t interface);

    uint8_t ReadClockConfig(uint8_t addr);
    void WriteClockConfig(uint8_t addr, uint8_t val);
};

#endif //JUNGFRAUJOCH_JUNGFRAUJOCHDEVICE_H
