# Jungfraujoch

Application to receive data from the PSI JUNGFRAU and EIGER detectors.

All documentation is now placed in [docs/](docs/) subdirectory and for the current version hosted on 
[Jungfraujoch Read The Docs page](https://jungfraujoch.readthedocs.io).