// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_COMPRESSIONALGORITHMENUM_H
#define JUNGFRAUJOCH_COMPRESSIONALGORITHMENUM_H

enum class CompressionAlgorithm {BSHUF_LZ4, BSHUF_ZSTD, BSHUF_ZSTD_RLE, NO_COMPRESSION};

#endif //JUNGFRAUJOCH_COMPRESSIONALGORITHMENUM_H
