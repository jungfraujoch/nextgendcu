// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "NonePusher.h"

void NonePusher::StartDataCollection(StartMessage &message) {}

bool NonePusher::EndDataCollection(const EndMessage &message) {
    return true;
}

bool NonePusher::SendImage(const uint8_t *image_data, size_t image_size, int64_t image_number) {
    return true;
}

bool NonePusher::SendCalibration(const CompressedImage &message) {
    return true;
}

void NonePusher::Finalize() {}


