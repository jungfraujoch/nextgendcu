// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_NONEPUSHER_H
#define JFJOCH_NONEPUSHER_H

#include "ImagePusher.h"

class NonePusher : public ImagePusher {
public:
    void StartDataCollection(StartMessage &message) override;
    bool EndDataCollection(const EndMessage &message) override;
    bool SendImage(const uint8_t *image_data, size_t image_size, int64_t image_number) override;
    bool SendCalibration(const CompressedImage &message) override;
    void Finalize() override;
};


#endif //JFJOCH_NONEPUSHER_H
