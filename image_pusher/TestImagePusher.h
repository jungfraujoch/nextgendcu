// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_TESTIMAGEPUSHER_H
#define JUNGFRAUJOCH_TESTIMAGEPUSHER_H

#include <mutex>

#include "ImagePusher.h"
#include "../common/Logger.h"
#include "../common/DiffractionExperiment.h"
#include "../jungfrau/JFCalibration.h"
#include "../jungfrau/JFModuleGainCalibration.h"

class TestImagePusher : public ImagePusher {
    mutable std::mutex m;
    std::vector<uint8_t> receiver_generated_image;
    int64_t image_id;
    bool correct_sequence = true;
    bool is_running = false;
    size_t frame_counter = 0;
    std::optional<DataMessage> data_message;
public:
    explicit TestImagePusher(int64_t image_number);
    void StartDataCollection(StartMessage& message)  override;
    bool EndDataCollection(const EndMessage& message) override;
    bool SendCalibration(const CompressedImage& message) override;
    bool SendImage(const uint8_t *image_data, size_t image_size, int64_t image_number) override;

    bool CheckImage(const DiffractionExperiment &x,
                         const std::vector<uint16_t> &raw_reference_image,
                         const JFCalibration &calibration,
                         Logger &logger);
    [[nodiscard]] bool CheckSequence() const;
    [[nodiscard]] const std::vector<uint8_t> &GetImage() const;
    [[nodiscard]] size_t GetCounter() const;
};


#endif //JUNGFRAUJOCH_TESTIMAGEPUSHER_H
