// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "CBORFilePusher.h"
#include <fstream>
#include "spdlog/fmt/fmt.h"

void CBORFilePusher::StartDataCollection(StartMessage &message) {
    std::unique_lock ul(m);
    dataset_number++;
    img_number = 0;

    size_t approx_size = 1024*1024;
    for (const auto &x : message.pixel_mask)
        approx_size += x.size;

    std::vector<uint8_t> serialization_buffer(approx_size);
    CBORStream2Serializer serializer(serialization_buffer.data(), serialization_buffer.size());
    serializer.SerializeSequenceStart(message);

    write(fmt::format("dataset{:03d}_start.cbor", dataset_number),
                      serialization_buffer.data(), serializer.GetBufferSize());
}

bool CBORFilePusher::EndDataCollection(const EndMessage &message) {
    std::unique_lock ul(m);
    size_t approx_size = 1024*1024;

    std::vector<uint8_t> serialization_buffer(approx_size);
    CBORStream2Serializer serializer(serialization_buffer.data(), serialization_buffer.size());
    serializer.SerializeSequenceEnd(message);

    write(fmt::format("dataset{:03d}_end.cbor", dataset_number),
          serialization_buffer.data(), serializer.GetBufferSize());
    return true;
}

bool CBORFilePusher::SendImage(const uint8_t *image_data, size_t image_size, int64_t image_number) {
    std::unique_lock ul(m);

    write(fmt::format("dataset{:03d}_img{:06d}.cbor", dataset_number, img_number), image_data, image_size);
    img_number++;
    return true;
}

bool CBORFilePusher::SendCalibration(const CompressedImage &message) {
    return true;
}

void CBORFilePusher::write(const std::string &filename, const void *data, size_t data_size) {
    std::ofstream file(filename.c_str(), std::ios::out | std::ios::binary | std::ios::trunc);
    file.write((char *) data, data_size);
    file.close();
}
