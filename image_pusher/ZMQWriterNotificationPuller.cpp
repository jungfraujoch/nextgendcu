// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "ZMQWriterNotificationPuller.h"
#include "nlohmann/json.hpp"

ZMQWriterNotificationPuller::ZMQWriterNotificationPuller(const std::string &addr, std::chrono::milliseconds timeout)
        : socket(ZMQSocketType::Pull) {
    socket.ReceiveTimeout(timeout);
    socket.Bind(addr);
}

std::optional<ZMQWriterNotificationOutput> ZMQWriterNotificationPuller::Receive(uint64_t run_number, const std::string &run_name) {
    ZMQMessage msg;
    // Loop to ensure that messages with wrong run_number or run_name are filtered
    while (socket.Receive(msg)) {
        nlohmann::json j;
        try {
            j = nlohmann::json::parse(std::string((char *) msg.data(), msg.size()));
        } catch (const std::exception& e) {
            throw JFJochException(JFJochExceptionCategory::JSON,
                                  "Notification message parsing error: " + std::string(e.what()));
        }
        if ((j["run_number"] == run_number) && (j["run_name"] == run_name)) {
            ZMQWriterNotificationOutput ret{};
            ret.ok = j["ok"];
            ret.processed_images = j["processed_images"];
            ret.socket_number = j["socket_number"];
            return ret;
        }
    }
    return {};
}

std::string ZMQWriterNotificationPuller::GetEndpointName() {
    return socket.GetEndpointName();
}
