// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_DUMPCBORTOFILEPUSHER_H
#define JUNGFRAUJOCH_DUMPCBORTOFILEPUSHER_H

#include "ImagePusher.h"
#include <mutex>

class CBORFilePusher : public ImagePusher {
    std::mutex m;
    uint64_t dataset_number = 0;
    uint64_t img_number = 0;
    static void write(const std::string &filename, const void *data, size_t data_size);
public:
    void StartDataCollection(StartMessage &message) override;
    bool EndDataCollection(const EndMessage &message) override;
    bool SendImage(const uint8_t *image_data, size_t image_size, int64_t image_number) override;
    bool SendCalibration(const CompressedImage &message) override;
};


#endif //JUNGFRAUJOCH_DUMPCBORTOFILEPUSHER_H
