// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_IMAGEPUSHER_H
#define JUNGFRAUJOCH_IMAGEPUSHER_H

#include <cstdint>
#include <vector>

#include "../common/DiffractionExperiment.h"
#include "../common/DiffractionSpot.h"
#include "../frame_serialize/CBORStream2Serializer.h"
#include "../frame_serialize/JFJochMessages.h"
#include "../common/ZeroCopyReturnValue.h"
#include "../common/Logger.h"

void PrepareCBORImage(DataMessage& message,
                      const DiffractionExperiment &experiment,
                      void *image, size_t image_size);

class ImagePusher {
public:
    virtual void StartDataCollection(StartMessage& message) = 0;
    virtual bool EndDataCollection(const EndMessage& message) = 0; // Non-blocking
    virtual bool SendImage(const uint8_t *image_data, size_t image_size,  int64_t image_number) = 0;
    virtual void SendImage(ZeroCopyReturnValue &z);
    virtual bool SendCalibration(const CompressedImage& message) = 0;
    virtual void Finalize(); // Ensure that all streams are closed, can throw exception
    virtual std::string GetWriterNotificationSocketAddress() const;
    virtual ~ImagePusher() = default;
};


#endif //JUNGFRAUJOCH_IMAGEPUSHER_H
