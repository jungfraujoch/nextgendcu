// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_ZMQWRITERNOTIFICATIONPULLER_H
#define JFJOCH_ZMQWRITERNOTIFICATIONPULLER_H

#include <optional>

#include "../common/ZMQWrappers.h"

struct ZMQWriterNotificationOutput {
    bool ok;
    uint64_t socket_number;
    uint64_t processed_images;
};

class ZMQWriterNotificationPuller {
    ZMQSocket socket;

public:
    explicit ZMQWriterNotificationPuller(const std::string& addr, std::chrono::milliseconds timeout = std::chrono::seconds(15));
    std::string GetEndpointName();
    std::optional<ZMQWriterNotificationOutput> Receive(uint64_t run_number, const std::string &run_name);
};


#endif //JFJOCH_ZMQWRITERNOTIFICATIONPULLER_H
