// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "ImagePusher.h"

void PrepareCBORImage(DataMessage& message,
                      const DiffractionExperiment &experiment,
                      void *image, size_t image_size) {
    message.image.data = (uint8_t *) image;
    message.image.size = image_size;
    message.image.xpixel = experiment.GetXPixelsNum();
    message.image.ypixel = experiment.GetYPixelsNum();
    message.image.pixel_depth_bytes = experiment.GetByteDepthImage();
    message.image.pixel_is_signed = experiment.IsPixelSigned();
    message.image.pixel_is_float = false;
    message.image.algorithm = experiment.GetCompressionAlgorithm();
    message.image.channel = "default";
}

void ImagePusher::Finalize() {}

std::string ImagePusher::GetWriterNotificationSocketAddress() const {
    return "";
}

void ImagePusher::SendImage(ZeroCopyReturnValue &z) {
    SendImage((uint8_t *) z.GetImage(), z.GetImageSize(), z.GetImageNumber());
    z.release();
}
