// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_ZMQSTREAM2PUSHER_H
#define JUNGFRAUJOCH_ZMQSTREAM2PUSHER_H

#include <mutex>

#include "ImagePusher.h"
#include "../common/ZMQWrappers.h"
#include "../preview/PreviewCounter.h"
#include "ZMQWriterNotificationPuller.h"

class ZMQStream2Pusher : public ImagePusher {
    std::vector<uint8_t> serialization_buffer;
    CBORStream2Serializer serializer;

    std::vector<std::unique_ptr<ZMQSocket>> socket;

    std::unique_ptr<ZMQWriterNotificationPuller> writer_notification_socket;

    int64_t images_per_file = 1;
    uint64_t run_number = 0;
    std::string run_name;
public:
    explicit ZMQStream2Pusher(const std::vector<std::string>& addr,
                              std::optional<int32_t> send_buffer_high_watermark = {},
                              std::optional<int32_t> send_buffer_size = {});

    ZMQStream2Pusher& WriterNotificationSocket(const std::string& addr);

    std::vector<std::string> GetAddress();

    // Strictly serial, as order of these is important
    void StartDataCollection(StartMessage& message) override;
    bool EndDataCollection(const EndMessage& message) override;
    bool SendCalibration(const CompressedImage& message) override;

    // Thread-safe
    void SendImage(ZeroCopyReturnValue &z) override;
    bool SendImage(const uint8_t *image_data, size_t image_size, int64_t image_number) override;

    void Finalize() override;
    std::string GetWriterNotificationSocketAddress() const override;
};

#endif //JUNGFRAUJOCH_ZMQSTREAM2PUSHER_H
