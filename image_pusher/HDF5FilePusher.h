
// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_HDF5FILEPUSHER_H
#define JFJOCH_HDF5FILEPUSHER_H

#include <future>

#include "ImagePusher.h"
#include "../writer/HDF5Writer.h"
#include "../common/ThreadSafeFIFO.h"

struct HDF5FilePusherQueueElement {
    const uint8_t *image_data;
    size_t image_size;
    int64_t image_number;
    ZeroCopyReturnValue *z;
    bool end;
};

class HDF5FilePusher : public ImagePusher {
    std::unique_ptr<HDF5Writer> writer;
    std::mutex m;
    std::future<void> writer_future;
    ThreadSafeFIFO<HDF5FilePusherQueueElement> writer_queue;
public:
    // Thread safety: StartDataCollection, EndDataCollection and SendCalibration must run poorly in serial context
    // SendImage can be executed in parallel
    void StartDataCollection(StartMessage &message) override;
    bool EndDataCollection(const EndMessage &message) override;
    bool SendImage(const uint8_t *image_data, size_t image_size, int64_t image_number) override;
    void SendImage(ZeroCopyReturnValue &z) override;
    bool SendCalibration(const CompressedImage &message) override;

    void WriterThread();
};


#endif //JFJOCH_HDF5FILEPUSHER_H
