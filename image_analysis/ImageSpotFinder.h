// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCH_IMAGESPOTFINDER_H
#define JFJOCH_IMAGESPOTFINDER_H


#include <cstddef>
#include <vector>

#include "../common/DiffractionExperiment.h"
#include "SpotFindingSettings.h"
#include "StrongPixelSet.h"

// This is "slow" spot finder for image-based analysis
// To complement "fast" PSI detector module based spot finder
// This one is expected to be used in cases, where images are already assembled
// and it aims for 100 ms execution

class ImageSpotFinder {
    size_t width;
    size_t height;
    std::vector<float> resolution_map;
    int NBX = 15;
public:
    ImageSpotFinder(const DiffractionGeometry &geom, size_t width, size_t height);
    ImageSpotFinder(std::vector<float> &resolution_map, size_t width, size_t height);
    ImageSpotFinder& PixelMask(std::vector<uint32_t> &pixel_mask);
    void nbx(int input);
    std::vector<DiffractionSpot> Run(const int32_t *input,
                                     const SpotFindingSettings &settings) const;
};

#endif //JFJOCH_IMAGESPOTFINDER_H
