// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#include "AzimuthalIntegration.h"
#include "../common/JFJochException.h"
#include "../common/RawToConvertedGeometry.h"
#include "../common/DiffractionGeometry.h"

AzimuthalIntegration::AzimuthalIntegration(const DiffractionExperiment& experiment, const AzimuthalIntegrationMapping &mapping) :
        pixel_to_bin(experiment.GetPixelsNum(), UINT16_MAX),
        corrections(experiment.GetPixelsNum(), 1.0),
        nbins(mapping.GetBinNumber()),
        sum(mapping.GetBinNumber(), 0),
        sum2(mapping.GetBinNumber(), 0),
        count(mapping.GetBinNumber(), 0)
{
    auto mapping_raw = mapping.GetPixelToBin();
    RawToConvertedGeometry(experiment, pixel_to_bin.data(), mapping_raw.data());

    std::vector<float> corrections_raw(experiment.GetModulesNum() * RAW_MODULE_SIZE);
    for (int i = 0; i < experiment.GetModulesNum(); i++)
        experiment.CalcAzIntCorrRawCoord(corrections_raw.data() + i * RAW_MODULE_SIZE, i);

    RawToConvertedGeometry(experiment, corrections.data(), corrections_raw.data());
}

void AzimuthalIntegration::Process(const int16_t *data, size_t npixel) {
    if (npixel != pixel_to_bin.size())
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "Mismatch in size of pixel-to-bin mapping and image");

    for (int i = 0; i < npixel; i++) {
        auto bin = pixel_to_bin[i];
        auto value  = data[i] * corrections[i];
        if ((data[i] > INT16_MIN + 4) && (data[i] < INT16_MAX - 4) && (bin < nbins)) {
            sum[bin]   += value;
            sum2[bin]  += value * value;
            count[bin] += 1;
        }
    }
}

void AzimuthalIntegration::GetResult(std::vector<double> &result) const {
    result.resize(nbins);

    for (int i = 0; i < nbins; i++) {
        if (count[i] > 0)
            result[i] = sum[i] / count[i];
        else
            result[i] = 0;
    }
}
