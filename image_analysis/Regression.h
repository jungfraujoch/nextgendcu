// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_REGRESSION_H
#define JUNGFRAUJOCH_REGRESSION_H

#include <vector>
#include <utility>
#include "../common/JFJochException.h"

struct RegressionResult {
    double intercept;
    double slope;
};

inline RegressionResult regression(std::vector<float> &x, std::vector<float> &y) {
    if (x.size() != y.size())
        throw JFJochException(JFJochExceptionCategory::InputParameterInvalid,
                              "Regression: mismatch between input parameter size");

    double x_sum = 0.0;
    double y_sum = 0.0;
    double xy_sum = 0.0;
    double x2_sum = 0.0;

    for (int i = 0; i < x.size(); i++) {
        x_sum += x[i];
        y_sum += y[i];
        x2_sum += x[i] * x[i];
        xy_sum += x[i] * y[i];
    }

    double denominator = x.size() * x2_sum - x_sum * x_sum;
    double intercept = (y_sum * x2_sum - x_sum * xy_sum) / denominator;
    double slope = (x.size() * xy_sum - x_sum * y_sum) / denominator;
    return {
        .intercept = intercept,
            .slope = slope
    };
};

#endif //JUNGFRAUJOCH_REGRESSION_H
