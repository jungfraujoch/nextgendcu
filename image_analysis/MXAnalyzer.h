// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_MXANALYZER_H
#define JUNGFRAUJOCH_MXANALYZER_H

#include "../common/DiffractionExperiment.h"
#include "IndexerWrapper.h"
#include "StrongPixelSet.h"

class MXAnalyzer {
    mutable std::mutex read_from_cpu_mutex;
    const DiffractionExperiment &experiment;
    std::unique_ptr<IndexerWrapper> indexer;
    bool find_spots = false;
    std::vector<DiffractionSpot> spots;
    constexpr static const float spot_distance_threshold_pxl = 2.0f;

    std::vector<float> arr_mean;
    std::vector<float> arr_sttdev;
    std::vector<uint32_t> arr_valid_count;
    std::vector<uint32_t> arr_strong_pixel;
public:
    const std::vector<float> &GetCPUMean() const;
    const std::vector<float> &GetCPUStdDev() const;
    const std::vector<uint32_t> &GetCPUValidCount() const;
    const std::vector<uint32_t> &GetCPUStrongPixel() const;

    explicit MXAnalyzer(const DiffractionExperiment& experiment);

    void ReadFromFPGA(const DeviceOutput* output,
                      const SpotFindingSettings& settings,
                      size_t module_number);

    void ReadFromCPU(DeviceOutput *output,
                     const SpotFindingSettings &settings,
                     size_t module_number);

    void ReadFromCPU(const int16_t *image,
                     const SpotFindingSettings& settings,
                     size_t module_number);

    void Process(DataMessage &message,
                 const SpotFindingSettings& settings);

    uint32_t FilterSpotsInPowderRings(const std::vector<DiffractionSpot> &spots_filter,
                                      std::vector<DiffractionSpot> &spots_out,
                                      int64_t min_spot_count_ring);
};


#endif //JUNGFRAUJOCH_MXANALYZER_H
