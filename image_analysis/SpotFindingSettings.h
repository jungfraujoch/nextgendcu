// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_SPOTFINDINGSETTINGS_H
#define JUNGFRAUJOCH_SPOTFINDINGSETTINGS_H

#include <cstdint>

struct SpotFindingSettings {
    bool enable = true;
    float signal_to_noise_threshold = 3;  // STRONG_PIXEL in XDS
    int64_t photon_count_threshold = 10;  // Threshold in photon counts
    int64_t min_pix_per_spot = 2;         // Minimum pixels per spot
    int64_t max_pix_per_spot = 50;        // Maximum pixels per spot
    float high_resolution_limit = 2.0;
    float low_resolution_limit = 50.0;

    bool filter_spots_powder_ring = false;
    int64_t min_spot_count_powder_ring = 20;

    bool indexing = true;
    float indexing_tolerance = 0.1;
};

#endif //JUNGFRAUJOCH_SPOTFINDINGSETTINGS_H
