// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_AZIMUTHALINTEGRATIONMAPPING_H
#define JUNGFRAUJOCH_AZIMUTHALINTEGRATIONMAPPING_H

#include <optional>
#include "../common/DiffractionExperiment.h"

class AzimuthalIntegrationMapping {
    float low_q, high_q, q_spacing;
    std::vector<float> bin_to_q;
    std::vector<uint16_t> pixel_to_bin;
    uint16_t max_bin_number;
    void Setup();
    explicit AzimuthalIntegrationMapping(const AzimuthalIntegrationSettings &settings);
public:
    // Raw geometry
    explicit AzimuthalIntegrationMapping(const DiffractionExperiment& experiment);
    // Standard geometry
    AzimuthalIntegrationMapping(const DiffractionGeometry& geom, int64_t width, int64_t height,
                                const AzimuthalIntegrationSettings& settings);

    [[nodiscard]] uint16_t GetBinNumber() const;
    [[nodiscard]] const std::vector<uint16_t>& GetPixelToBin() const;
    [[nodiscard]] const std::vector<float> &GetBinToQ() const;
    [[nodiscard]] float QToBin(float q) const;
};


#endif //JUNGFRAUJOCH_AZIMUTHALINTEGRATIONMAPPING_H
