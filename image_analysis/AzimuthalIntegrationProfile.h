// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_AZIMUTHALINTEGRATIONPROFILE_H
#define JUNGFRAUJOCH_AZIMUTHALINTEGRATIONPROFILE_H

#include <vector>
#include <mutex>
#include "../common/Plot.h"

#include "AzimuthalIntegrationMapping.h"
#include "../acquisition_device/AcquisitionDevice.h"

class AzimuthalIntegrationProfile {
    mutable std::mutex m;
    std::vector<float> sum;
    std::vector<uint64_t> count;
    std::vector<float> bin_to_q;
    std::string title;
public:
    explicit AzimuthalIntegrationProfile(const AzimuthalIntegrationMapping &mapping);
    void Clear(const AzimuthalIntegrationMapping &mapping);
    void SetTitle(const std::string& input);
    void Add(const DeviceOutput &result);
    void Add(const std::vector<float> &sum, const std::vector<uint32_t> &count);
    std::vector<float> GetResult() const;
    float GetMeanValueOfBins(uint16_t min_bin, uint16_t max_bin) const;
    MultiLinePlot GetPlot() const;
    AzimuthalIntegrationProfile& operator+=(const AzimuthalIntegrationProfile& profile); // Not thread safe
};

#endif //JUNGFRAUJOCH_AZIMUTHALINTEGRATIONPROFILE_H
