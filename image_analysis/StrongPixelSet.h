// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_STRONGPIXELSET_H
#define JUNGFRAUJOCH_STRONGPIXELSET_H

#include <unordered_map>
#include <unordered_set>
#include <mutex>
#include "../common/DiffractionExperiment.h"
#include "../common/Coord.h"
#include "../common/DiffractionSpot.h"
#include "../acquisition_device/AcquisitionDevice.h"
#include "SpotFindingSettings.h"

struct strong_pixel {
    uint16_t col;
    uint16_t line;
    int32_t counts;
};

void FilterSpotsByResolution(const DiffractionExperiment& experiment,
                             const std::vector<DiffractionSpot> &input,
                             std::vector<DiffractionSpot> &output);

void FilterSpotsByCount(const DiffractionExperiment& experiment,
                        const std::vector<DiffractionSpot> &input,
                        std::vector<DiffractionSpot> &output);

class StrongPixelSet {
    std::vector<strong_pixel> pixels;
    std::vector<uint16_t> L;

    static const constexpr size_t max_strong_pixel_per_module = 4000;
    static const constexpr uint32_t xpixel = RAW_MODULE_COLS;
    static const constexpr uint32_t ypixel = RAW_MODULE_LINES;

    uint32_t strong_pixel_count;

    uint16_t find_root(uint16_t e);
    uint16_t make_union(uint16_t e1, uint16_t e2);
    std::vector<DiffractionSpot> sparseccl();
public:
    void ReadFPGAOutput(const DiffractionExperiment& experiment,
                        const DeviceOutput& output);

    StrongPixelSet();
    void AddStrongPixel(uint16_t col, uint16_t line, int32_t photons = 1);
    void FindSpots(const DiffractionExperiment &experiment, const SpotFindingSettings &settings, std::vector<DiffractionSpot> &spots,
                                uint16_t module_number);
    void FindSpotsImage(const SpotFindingSettings &settings, std::vector<DiffractionSpot> &spots);
    uint32_t GetStrongPixelCount() const;
};


#endif //JUNGFRAUJOCH_STRONGPIXELSET_H
