// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_AZIMUTHALINTEGRATION_H
#define JUNGFRAUJOCH_AZIMUTHALINTEGRATION_H

#include <vector>
#include <cstdint>
#include <cmath>

#include "../common/DiffractionExperiment.h"
#include "AzimuthalIntegrationMapping.h"

class AzimuthalIntegration {
    std::vector<uint16_t> pixel_to_bin;
    std::vector<float> corrections;
    const uint16_t nbins;
    std::vector<double> sum;
    std::vector<double> sum2;
    std::vector<int64_t> count;
public:
    AzimuthalIntegration(const DiffractionExperiment& experiment, const AzimuthalIntegrationMapping& mapping);
    void Process(const int16_t *data, size_t npixel);
    void GetResult(std::vector<double> &result) const;
};


#endif //JUNGFRAUJOCH_AZIMUTHALINTEGRATION_H
