// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_CRYSTALLATTICE_H
#define JUNGFRAUJOCH_CRYSTALLATTICE_H

#include <vector>
#include "../common/Coord.h"
#include "../common/UnitCell.h"

class CrystalLattice {
    Coord vec[3];
public:
    CrystalLattice() = default;
    explicit CrystalLattice(const UnitCell &cell);

    [[nodiscard]] Coord &Vec0();
    [[nodiscard]] Coord &Vec1();
    [[nodiscard]] Coord &Vec2();
    [[nodiscard]] const Coord &Vec0() const;
    [[nodiscard]] const Coord &Vec1() const;
    [[nodiscard]] const Coord &Vec2() const;
    [[nodiscard]] UnitCell GetUnitCell() const;
    [[nodiscard]] std::vector<float> GetVector() const;
};


#endif //JUNGFRAUJOCH_CRYSTALLATTICE_H
