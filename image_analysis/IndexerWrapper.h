// SPDX-FileCopyrightText: 2024 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JUNGFRAUJOCH_INDEXERWRAPPER_H
#define JUNGFRAUJOCH_INDEXERWRAPPER_H

#include <vector>
#include <memory>

#include "../common/Coord.h"
#include "CrystalLattice.h"
#include "../common/Definitions.h"

struct IndexerWrapperImpl;

struct IndexingResult {
    CrystalLattice l;
    std::vector<uint8_t> indexed_spot;
};

class IndexerWrapper {
    std::unique_ptr<IndexerWrapperImpl> impl_;

    constexpr const static uint32_t viable_cell_min_spots = 9;
public:
    IndexerWrapper();
    ~IndexerWrapper();
    IndexerWrapper(const IndexerWrapper &i) = delete;
    const IndexerWrapper& operator=(const IndexerWrapper &i) = delete;
    void Setup(const UnitCell &cell);
    std::vector<IndexingResult> Run(const std::vector<Coord> &coord, float indexing_threshold, int nspots = -1);
};

#endif //JUNGFRAUJOCH_INDEXERWRAPPER_H
