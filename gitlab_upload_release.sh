#!/bin/bash

#
# Copyright (2019-2024) Paul Scherrer Institute
#

export FPGA_VERSION=1.0.0-rc.31
export PACKAGE_VERSION_SEM=`head -n1 VERSION`
export PACKAGE_VERSION=${PACKAGE_VERSION_SEM//-/_}
export PACKAGE_VERSION_PYTHON=${PACKAGE_VERSION_SEM//-rc./rc}
export PACKAGE_VERSION_PYTHON=${PACKAGE_VERSION_PYTHON//-alpha./a}
export PACKAGE_VERSION_PYTHON=${PACKAGE_VERSION_PYTHON//-beta./b}

export PACKAGE_REGISTRY_URL="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/jungfraujoch/${PACKAGE_VERSION_SEM}"

wget https://gitlab.psi.ch/api/v4/projects/1560/packages/generic/jungfraujoch/${FPGA_VERSION}/jfjoch_fpga_pcie_100g.mcs
wget https://gitlab.psi.ch/api/v4/projects/1560/packages/generic/jungfraujoch/${FPGA_VERSION}/jfjoch_fpga_pcie_8x10g.mcs

curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file jfjoch-driver-dkms-${PACKAGE_VERSION}-1.el8.noarch.rpm "${PACKAGE_REGISTRY_URL}/jfjoch-driver-dkms-${PACKAGE_VERSION}-1.el8.noarch.rpm"
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file jfjoch-writer-${PACKAGE_VERSION}-1.el8.x86_64.rpm "${PACKAGE_REGISTRY_URL}/jfjoch-writer-${PACKAGE_VERSION}-1.el8.x86_64.rpm"
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file jfjoch-viewer-${PACKAGE_VERSION}-1.el8.x86_64.rpm "${PACKAGE_REGISTRY_URL}/jfjoch-viewer-${PACKAGE_VERSION}-1.el8.x86_64.rpm"
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file jfjoch-${PACKAGE_VERSION}-1.el8.x86_64.rpm "${PACKAGE_REGISTRY_URL}/jfjoch-${PACKAGE_VERSION}-1.el8.x86_64.rpm"
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file jfjoch_frontend.tar.gz "${PACKAGE_REGISTRY_URL}/jfjoch_frontend.tar.gz"
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file jfjoch_fpga_pcie_100g.mcs "${PACKAGE_REGISTRY_URL}/jfjoch_fpga_pcie_100g.mcs"
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file jfjoch_fpga_pcie_8x10g.mcs "${PACKAGE_REGISTRY_URL}/jfjoch_fpga_pcie_8x10g.mcs"
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file jfjoch_client-${PACKAGE_VERSION_PYTHON}.tar.gz "${PACKAGE_REGISTRY_URL}/jfjoch_client-${PACKAGE_VERSION_PYTHON}.tar.gz"
curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file jfjoch_client-${PACKAGE_VERSION_PYTHON}-py3-none-any.whl "${PACKAGE_REGISTRY_URL}/jfjoch_client-${PACKAGE_VERSION_PYTHON}-py3-none-any.whl"

release-cli create --name "Release $PACKAGE_VERSION_SEM" --tag-name $PACKAGE_VERSION_SEM \
  --assets-link "{\"name\":\"jfjoch_frontend.tar.gz\",\"url\":\"${PACKAGE_REGISTRY_URL}/jfjoch_frontend.tar.gz\"}" \
  --assets-link "{\"name\":\"jfjoch_fpga_pcie_8x10g.mcs\",\"url\":\"${PACKAGE_REGISTRY_URL}/jfjoch_fpga_pcie_8x10g.mcs\"}" \
  --assets-link "{\"name\":\"jfjoch_fpga_pcie_100g.mcs\",\"url\":\"${PACKAGE_REGISTRY_URL}/jfjoch_fpga_pcie_100g.mcs\"}" \
  --assets-link "{\"name\":\"jfjoch-${PACKAGE_VERSION}-1.el8.x86_64.rpm\",\"url\":\"${PACKAGE_REGISTRY_URL}/jfjoch-${PACKAGE_VERSION}-1.el8.x86_64.rpm\",\"link_type\":\"package\"}" \
  --assets-link "{\"name\":\"jfjoch-writer-${PACKAGE_VERSION}-1.el8.x86_64.rpm\",\"url\":\"${PACKAGE_REGISTRY_URL}/jfjoch-writer-${PACKAGE_VERSION}-1.el8.x86_64.rpm\",\"link_type\":\"package\"}" \
  --assets-link "{\"name\":\"jfjoch-viewer-${PACKAGE_VERSION}-1.el8.x86_64.rpm\",\"url\":\"${PACKAGE_REGISTRY_URL}/jfjoch-viewer-${PACKAGE_VERSION}-1.el8.x86_64.rpm\",\"link_type\":\"package\"}" \
  --assets-link "{\"name\":\"jfjoch-driver-dkms-${PACKAGE_VERSION}-1.el8.noarch.rpm\",\"url\":\"${PACKAGE_REGISTRY_URL}/jfjoch-driver-dkms-${PACKAGE_VERSION}-1.el8.noarch.rpm\",\"link_type\":\"package\"}" \
  --assets-link "{\"name\":\"jfjoch_client-${PACKAGE_VERSION_PYTHON}-py3-none-any.whl\",\"url\":\"${PACKAGE_REGISTRY_URL}/jfjoch_client-${PACKAGE_VERSION_PYTHON}-py3-none-any.whl\",\"link_type\":\"package\"}" \
  --assets-link "{\"name\":\"jfjoch_client-${PACKAGE_VERSION_PYTHON}.tar.gz\",\"url\":\"${PACKAGE_REGISTRY_URL}/jfjoch_client-${PACKAGE_VERSION_PYTHON}.tar.gz\",\"link_type\":\"package\"}"

if [ -n "$CI_PYPI_TOKEN" ]; then
  rm -rf dist/
  mkdir dist/
  cp jfjoch_client-${PACKAGE_VERSION_PYTHON}-py3-none-any.whl jfjoch_client-${PACKAGE_VERSION_PYTHON}.tar.gz dist/
  twine upload dist/* -u __token__ -p $CI_PYPI_TOKEN --skip-existing
fi
