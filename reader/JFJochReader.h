// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCHIMAGEREADER_H
#define JFJOCHIMAGEREADER_H

#include <unordered_set>
#include <map>

#include "../frame_serialize/JFJochMessages.h"
#include "../common/Plot.h"
#include "../common/DiffractionGeometry.h"

struct JFJochReaderDataset {
    size_t number_of_images;
    size_t image_size_x;
    size_t image_size_y;
    int64_t saturation_value;
    int64_t bit_depth_image;

    DiffractionGeometry geom;

    float frame_time;
    float count_time;

    std::optional<int64_t> error_value;

    std::string jfjoch_release;
    std::string instrument_name;
    std::string source_name;
    std::string detector_name;
    std::optional<float> attenuator_transmission;
    std::optional<float> total_flux;

    std::vector<float> az_int_bin_to_q;

    std::vector<uint32_t> spot_count;
    std::vector<uint8_t> indexing_result;
    std::vector<float> bkg_estimate;
    std::vector<float> efficiency;
    std::vector<int64_t> max_value;
    std::vector<std::string> roi;
    std::vector<std::vector<int64_t>> roi_sum;
    std::vector<std::vector<int64_t>> roi_sum_sq;
    std::vector<std::vector<int64_t>> roi_max;
    std::vector<std::vector<int64_t>> roi_npixel;
    std::vector<uint32_t> pixel_mask;
};

struct JFJochReaderImage {
    std::vector<int32_t> image;
    std::vector<SpotToSave> spots;
    std::vector<float> az_int;
    std::vector<float> az_int_bin_to_q;

    int64_t number;

    std::unordered_set<int64_t> saturated_pixel;
    std::unordered_set<int64_t> error_pixel;
    std::map<int32_t, int32_t> valid_pixel;

    std::shared_ptr<JFJochReaderDataset> dataset;
};


class JFJochReader {
public:
    virtual ~JFJochReader() = default;

    [[nodiscard]] virtual std::shared_ptr<JFJochReaderDataset> GetStartMessage() const = 0;
    [[nodiscard]] virtual uint64_t GetNumberOfImages() const = 0;

    virtual void LoadImage(int64_t image_number) = 0;
    virtual std::shared_ptr<JFJochReaderImage> CopyImage() = 0;
    virtual void Close() = 0;
};

#endif //JFJOCHIMAGEREADER_H
