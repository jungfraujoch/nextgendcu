ADD_LIBRARY(JFJochReader STATIC
        JFJochReader.cpp JFJochReader.h
        JFJochHDF5Reader.cpp
        JFJochHDF5Reader.h
)

TARGET_LINK_LIBRARIES(JFJochReader JFJochCommon JFJochZMQ JFJochLogger JFJochHDF5Wrappers CBORStream2FrameSerialize)
