// SPDX-FileCopyrightText: 2025 Filip Leonarski, Paul Scherrer Institute <filip.leonarski@psi.ch>
// SPDX-License-Identifier: GPL-3.0-only

#ifndef JFJOCHHDF5IMAGEREADER_H
#define JFJOCHHDF5IMAGEREADER_H

#include "JFJochReader.h"
#include "../writer/HDF5Objects.h"

constexpr static int32_t GAP_PXL_VALUE = INT32_MIN + 1;
constexpr static int32_t ERROR_PXL_VALUE = INT32_MIN;
constexpr static int32_t SATURATED_PXL_VALUE = INT32_MAX;

class JFJochHDF5Reader : public JFJochReader {
    mutable std::mutex master_file_mutex;
    std::unique_ptr<HDF5ReadOnlyFile> master_file;

    bool legacy_format = false;
    size_t images_per_file = 1;
    size_t number_of_images = 0;

    mutable std::mutex start_message_mutex;
    std::shared_ptr<JFJochReaderDataset> start_message;
    void SetStartMessage(std::shared_ptr<JFJochReaderDataset> val);

    mutable std::mutex current_image_mutex;
    std::shared_ptr<JFJochReaderImage> current_image;

    std::shared_ptr<JFJochReaderImage> LoadImageInternal(int64_t image_number);

    void LoadImageDataset(std::vector<int32_t> &output,
                          const std::string &name,
                          hsize_t number,
                          hsize_t width,
                          hsize_t height);
public:
    ~JFJochHDF5Reader() override = default;

    void ReadFile(const std::string& filename);

    std::shared_ptr<JFJochReaderDataset> GetStartMessage() const override;

    void LoadImage(int64_t image_number) override;
    std::shared_ptr<JFJochReaderImage> CopyImage() override;

    uint64_t GetNumberOfImages() const override;
    void Close() override;
};


#endif //JFJOCHHDF5IMAGEREADER_H
