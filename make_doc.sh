#!/bin/bash

python3.11 -m venv tmp_venv/
source tmp_venv/bin/activate

pip install -r docs/requirements.txt

sphinx-build docs public

rm -rf tmp_venv/
